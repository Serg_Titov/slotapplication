<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="by.minsk.miroha.entities.databases.SlotName"%>
<%@ page import="by.minsk.miroha.entities.report.Multipliers"%>
<%@ page import="by.minsk.miroha.entities.report.SlotPoint"%>
<%@ page import="by.minsk.miroha.report.SlotDBReport"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html lang="en" pageEncoding="UTF-8">
<head>
    <meta charset="UTF-8">
    <title>Загрузка информации в локальную базу</title>
</head>
<style>
    #menu {
        margin-left: 30%;
        position: absolute;
        top: 2em;
    }
    .menu-li {
        display: inline;
        margin: -2.5;
    }
    .menu-ul {
        list-style: none;
        margin: 0;
        padding-left: 0;
    }
    .menu-a {
        text-decoration: none;
        padding: 1em;
        background: #f5f5f5;
        border: 1px solid #b19891;
        color: #695753;
    }
</style>
<body>
    <div id="menu">
        <nav>
            <ul class="menu-ul">
                <li class="menu-li"><a class="menu-a" href='/SlotApplication/'>Home</a></li>
                <li class="menu-li"><a class="menu-a" href='/SlotApplication/tools'>Tools</a></li>
                <li class="menu-li"><a class="menu-a" href='/SlotApplication/slots'>Slots</a></li>
                <li class="menu-li"><a class="menu-a" href='/SlotApplication/jackpots'>Jackpots</a></li>
            </ul>
        </nav>
    </div>
    <div class="message"><h2>Загрузка базы из файла с расчетом ее показателей</h2></div>
    <form:form action="/SlotApplication/calc-home-db" method="post" modelAttribute="slotName">

        <form:label path="slotName">Slot: </form:label>
        <form:input path="slotName" value="${slotName.slotName}"/> <br>

        <form:label path="provider">Provider: </form:label>
        <form:input path="provider" value="${slotName.provider}"/> <br>
        <br>
        <br>
        <label path="error">${error}</label><br>
        <br>
        <div style="visibility: ${uploadDisabled};">
            <form:button >Загрузить</form:button>
        </div>
    </form:form>
    <br>
    <button onclick="location.href='/SlotApplication/DeleteHome'" ${deleteDisabled}>Очистить базу</button>
    <br>
    <label>${deleted}</label>
    <br>
    <br>
    <button onclick="location.href='/SlotApplication/'">Назад</button>
    <br><br>
    <div>${uploaded}</div><br><br>
<%  SlotDBReport report = (SlotDBReport) request.getAttribute("report");
    if(report != null){
%>      <label>Slot: ${report.slotInformation.slot}</label><br>
        <label>Provider: ${report.slotInformation.provider}</label><br><br>
        <label>Total spins in database: ${report.commonSlotInformation.spinAmount}</label><br>
        <label>Max win in database: ${report.commonSlotInformation.maxWin}</label><br>
        <label>RTP: ${report.commonSlotInformation.rtp}%</label><br>
        <label>Volatility: ${report.commonSlotInformation.volatility}</label><br>
        <label>Бонус выпадает раз в ${report.commonSlotInformation.bonusFrequency} симуляций</label><br>
        <label>Доля выигрышей с бонусами: ${report.commonSlotInformation.bonusWinsRate}</label><br>
        <label>Доля выигрышей без бонусов: ${report.commonSlotInformation.winsWithoutBonusRate}</label><br>
        <label>Средний выигрыш в бонусной игре: ${report.commonSlotInformation.averageBonusWin}</label><br><br>
        <label>Частота выпадения множителей в базе данных:</label><br>
<%      for (Multipliers multiplier : report.getMultipliers()){
%>          <label> Множитель больше <%=multiplier.getMultiplier()%>: раз в <%=multiplier.getMultiplierCount()%></label> симуляций.<br>
<%
        }
        if(!report.getSlotPoints().isEmpty()){
%>
            Для данного слота следующие точки: <br>
            <% for(SlotPoint slotPoint : report.getSlotPoints()){
            %>
                SpinLimit = <%=slotPoint.getSpinLimit()%>; Result = <%=slotPoint.getPointResult()%>
                <br>
            <%  }
        }
    }
%>
    </body>
</html>