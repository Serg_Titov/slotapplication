<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Jackpot</title>
</head>
<style>
    #menu {
        margin-left: 30%;
        margin-top: 2%;
    }
    .menu-li {
        display: inline;
        margin: -2.5;
    }
    .menu-ul {
        list-style: none;
        margin: 0;
        padding-left: 0;
    }
    .menu-a {
        text-decoration: none;
        padding: 1em;
        background: #f5f5f5;
        border: 1px solid #b19891;
        color: #695753;
    }
    .labelHeader{
        margin-left: 35%;
        margin-top: 3%;
        font-size: 18pt;
    }
    .common-block{
        display: inline-flex;
    }
    .top-hblock{
        margin-left: 10px;
        top: 0;
    }
    .common-jackpot-value{
        border: 1px solid #b19891;
        height: 70px;
        width: 350px;
    }
    .big-text{
        font-size: 15pt;
    }
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script>
    function loadPage(){
        let fullParams = window.location.search.replace('?','').split('&');
        let slotName;
        if(fullParams.length > 0){
            let slotParams = fullParams[0].split('=');
            if(slotParams.length > 1){
                slotName = slotParams[1];
            }
        }
        let siteName;
        if(fullParams.length > 1){
            let siteParams = fullParams[1].split('=');
            if(siteParams.length > 1){
                siteName = siteParams[1];
            }
        }

        let request = {
                        slot : slotName,
                        site : siteName
                      };
        $.ajax({
            type: 'POST',
            url: '/SlotApplication/jackpot',
            data: request,
            success: function(data){
                document.getElementById("slotName").innerText = data.slotName + " ";
                document.getElementById("siteName").innerText = data.site.shortUrl;
                document.getElementById("currentJackpot").innerText = data.currentJackpot;
                document.getElementById("statusLabel").innerText = data.status;
                document.getElementById("lastPayedOutJackpot").innerText = data.lastPayedOutJackpot;
                document.getElementById("maxPayedOutJackpot").innerText = data.maxPayedOutJackpot;
                document.getElementById("minPayedOutJackpot").innerText = data.minPayedOutJackpot;

                for(let grown of data.grownValues){
                    let periodSuffix = "";
                    if(grown.timePeriod === "DAY"){
                        periodSuffix = "Day";
                    }
                    if(grown.timePeriod === "MONTH"){
                        periodSuffix = "Month";
                    }
                    if(grown.timePeriod === "YEAR"){
                        periodSuffix = "Year";
                    }
                    document.getElementById("grown"+periodSuffix).innerText = grown.grownValue;
                }
            }
        });
    }
</script>
<script>
    function getGraphsPoints(){
        let fullParams = window.location.search.replace('?','').split('&');
        let slotName;
        if(fullParams.length > 0){
            let slotParams = fullParams[0].split('=');
            if(slotParams.length > 1){
                slotName = slotParams[1];
            }
        }
        let siteName;
        if(fullParams.length > 1){
            let siteParams = fullParams[1].split('=');
            if(siteParams.length > 1){
                siteName = siteParams[1];
            }
        }
        let dateStart = document.getElementById("dateStart");
        let dateEnd = document.getElementById("dateEnd");
        let periodType = document.getElementById("periodType");
        let request = { slotName: slotName,
                        site: siteName,
                        dateStart : dateStart.value,
                        dateEnd : dateEnd.value,
                        increaseType : periodType.value
        };
        $.ajax({
            type: 'POST',
            url: '/SlotApplication/get-jackpot-graph',
            data: request,
            success:  function(data){
                let graphPlace = document.getElementById("grownJackpot");
                while (graphPlace.firstChild) {
                    graphPlace.removeChild(graphPlace.firstChild);
                }

                let graphIncreasePlace = document.getElementById("increaseJackpot");
                while (graphIncreasePlace.firstChild) {
                    graphIncreasePlace.removeChild(graphIncreasePlace.firstChild);
                }

                let graphPlaceDiv = document.createElement("div");
                graphPlaceDiv.id = "graphPlace";
                graphPlace.appendChild(graphPlaceDiv);

                let graphIncreasePlaceDiv = document.createElement("div");
                graphIncreasePlaceDiv.id = "graphIncreasePlace";
                graphIncreasePlace.appendChild(graphIncreasePlaceDiv);

                if(data != null && data.graphPoints != null && data.graphPoints.length > 0){
                    for(let i = 0;  i< data.graphPoints.length; i++){
                        let pointLabel = document.createElement("label");
                        pointLabel.innerText = data.graphPoints[i].jackpotValue + " - " + data.graphPoints[i].date;
                        graphPlaceDiv.appendChild(pointLabel);
                        let brComponent = document.createElement("br");
                        graphPlaceDiv.appendChild(brComponent);
                    }
                }

                if(data != null && data.increasePoints != null && data.increasePoints.length > 0){
                    for(let i = 0;  i< data.increasePoints.length; i++){
                        let increaseLabel = document.createElement("label");
                        increaseLabel.innerText = data.increasePoints[i].jackpotValue + " - " + data.increasePoints[i].date;
                        graphIncreasePlaceDiv.appendChild(increaseLabel);
                        let brComponent = document.createElement("br");
                        graphIncreasePlaceDiv.appendChild(brComponent);
                    }
                }
            }
        });
    }
</script>
<body>
    <script>loadPage()</script>
    <div id="menu">
        <nav>
            <ul class="menu-ul">
              <li class="menu-li"><a class="menu-a" href='/SlotApplication/'>Home</a></li>
              <li class="menu-li"><a class="menu-a" href='/SlotApplication/tools'>Tools</a></li>
              <li class="menu-li"><a class="menu-a" href='/SlotApplication/slots'>Slots</a></li>
              <li class="menu-li"><a class="menu-a" href='/SlotApplication/jackpots'>Jackpots</a></li>
            </ul>
        </nav>
    </div>

    <div class="labelHeader">
        <label  id="slotName"></label><label id="siteName"></label>
    </div>
    <br>
    <div>
        <div class="common-block">
            <div class="top-hblock">
                <div class="common-jackpot-value">
                    <div>
                        <label id="currentJackpot" class="big-text">-</label>
                    </div>
                    <div>
                        <label>Текущее значение</label>
                    </div>
                </div>
                <br>
                <div class="common-jackpot-value">
                    <div>
                        <label id="statusLabel" class="big-text">-</label>
                    </div>
                    <div>
                        <label>Статус</label>
                    </div>
                </div>
                <br>
                <div class="common-jackpot-value">
                    <div>
                        <label>Последний разыгранный</label>
                    </div>
                    <div>
                        <label id="lastPayedOutJackpot" class="big-text">-</label>
                    </div>
                </div>
                <br>
                <div class="common-jackpot-value">
                    <div>
                        <label>Максимальный разыгранный джекпот</label>
                    </div>
                    <div>
                        <label id="maxPayedOutJackpot" class="big-text">-</label>
                    </div>
                </div>
                <br>
                <div class="common-jackpot-value">
                    <div>
                        <label>Минимальный разыгранный джекпот</label>
                    </div>
                    <div>
                        <label id="minPayedOutJackpot" class="big-text">-</label>
                    </div>
                </div>
                <br>
                <div class="common-jackpot-value">
                    <div>
                        <label class="big-text">AVG GROWN 24H</label>
                    </div>
                    <div>
                        <label id="grownDay" class="big-text">-</label>
                    </div>
                </div>
                <br>
                <div class="common-jackpot-value">
                    <div>
                        <label class="big-text">AVG GROWN MONTH</label>
                    </div>
                    <div>
                        <label id="grownMonth" class="big-text">-</label>
                    </div>
                </div>
                <br>
                <div class="common-jackpot-value">
                    <div>
                        <label class="big-text">AVG GROWN YEAR</label>
                    </div>
                    <div>
                        <label id="grownYear" class="big-text">-</label>
                    </div>
                </div>
                <br>
            </div>
            <div class="top-hblock">
                <div>
                    <div id="graphPeriod">
                        <div>
                            <label>Начало периода</label>
                            <input id="dateStart" type="date"/>
                        </div>
                        <div>
                            <label>Конец периода</label>
                            <input id="dateEnd" type="date"/>
                        </div>
                        <div>
                            <label>Период</label>
                            <select id="periodType">
                                <option value="days" selected>Дни</option>
                                <option value="weeks">Недели</option>
                                <option value="months">Месяцы</option>
                            </select>
                        </div>
                        <button onclick="getGraphsPoints()">Обновить</button>
                    </div>
                    <script>getGraphsPoints()</script>
                    <div>
                        <label class="big-text">График роста джекпота (за все время)</label>
                        <div id="grownJackpot">
                        </div>
                    </div>
                    <br/>
                    <div>
                        <label class="big-text">График прироста джекпота</label>
                        <div id="increaseJackpot">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>