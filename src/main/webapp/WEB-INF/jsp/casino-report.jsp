<%@ page import="java.util.List"%>
<%@ page import="by.minsk.miroha.report.CommonCasinoReport"%>
<%@ page import="by.minsk.miroha.entities.front.ProviderAmount"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en" pageEncoding="UTF-8">
<head>
    <meta charset="UTF-8">
    <title>Slots analytics</title>
    <style>
        #menu {
            margin-left: 30%;
        }
        .menu-li {
            display: inline;
            margin: -2.5;
        }
        .menu-ul {
            list-style: none;
            margin: 0;
            padding-left: 0;
        }
        .menu-a {
            text-decoration: none;
            padding: 1em;
            background: #f5f5f5;
            border: 1px solid #b19891;
            color: #695753;
        }
        .table{
            border-collapse: collapse;
            border: 1px solid grey;
        }
        .tr {
           border: 1px solid grey;
        }
        .td {
           border: 1px solid grey;
        }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script>
        window.page = 0;
        window.field = "provider";
        window.type = "asc";
        function createTable(){
            let table = document.createElement("table");
            table.id="slotTable";
            table.classList.add("table");
            let trHeader = document.createElement("tr");
            trHeader.id = "tableHeader";
            trHeader.classList.add("tr");
            table.appendChild(trHeader);
            document.getElementById("slotTablePlace").appendChild(table);

            let tdHeaderProvider = document.createElement("td");
            tdHeaderProvider.id = "provider";
            tdHeaderProvider.classList.add("td");
            tdHeaderProvider.classList.add("even");
            tdHeaderProvider.onclick = sortTable;
            trHeader.appendChild(tdHeaderProvider);
            let headerLabelProvider = document.createElement("label");
            tdHeaderProvider.appendChild(headerLabelProvider);
            headerLabelProvider.innerText = "Provider";

            let tdHeaderSlot = document.createElement("td");
            tdHeaderSlot.id = "slot";
            tdHeaderSlot.classList.add("td");
            tdHeaderSlot.classList.add("even");
            tdHeaderSlot.onclick = sortTable;
            trHeader.appendChild(tdHeaderSlot);
            let headerLabelSlot = document.createElement("label");
            tdHeaderSlot.appendChild(headerLabelSlot);
            headerLabelSlot.innerText = "Slot";

            let tdHeaderRTP = document.createElement("td");
            tdHeaderRTP.id = "rtp";
            tdHeaderRTP.classList.add("td");
            tdHeaderRTP.classList.add("even");
            tdHeaderRTP.onclick = sortTable;
            trHeader.appendChild(tdHeaderRTP);
            let headerLabelRTP = document.createElement("label");
            tdHeaderRTP.appendChild(headerLabelRTP);
            headerLabelRTP.innerText = "Rtp";

            let tdHeaderVolatility = document.createElement("td");
            tdHeaderVolatility.id = "volatility";
            tdHeaderVolatility.classList.add("td");
            tdHeaderVolatility.classList.add("even");
            tdHeaderVolatility.onclick = sortTable;
            trHeader.appendChild(tdHeaderVolatility);
            let headerLabelVolatility = document.createElement("label");
            tdHeaderVolatility.appendChild(headerLabelVolatility);
            headerLabelVolatility.innerText = "Volatility";

            let tdHeaderX100 = document.createElement("td");
            tdHeaderX100.id = "x100";
            tdHeaderX100.classList.add("td");
            tdHeaderX100.classList.add("even");
            tdHeaderX100.onclick = sortTable;
            trHeader.appendChild(tdHeaderX100);
            let headerLabelX100 = document.createElement("label");
            tdHeaderX100.appendChild(headerLabelX100);
            headerLabelX100.innerText = "x100";

            let tdHeaderX1000 = document.createElement("td");
            tdHeaderX1000.id = "x1000";
            tdHeaderX1000.classList.add("td");
            tdHeaderX1000.classList.add("even");
            tdHeaderX1000.onclick = sortTable;
            trHeader.appendChild(tdHeaderX1000);
            let headerLabelX1000 = document.createElement("label");
            tdHeaderX1000.appendChild(headerLabelX1000);
            headerLabelX1000.innerText = "x1000";

            let tdHeaderX5000 = document.createElement("td");
            tdHeaderX5000.id = "x5000";
            tdHeaderX5000.classList.add("td");
            tdHeaderX5000.classList.add("even");
            tdHeaderX5000.onclick = sortTable;
            trHeader.appendChild(tdHeaderX5000);
            let headerLabelX5000 = document.createElement("label");
            tdHeaderX5000.appendChild(headerLabelX5000);
            headerLabelX5000.innerText = "x5000";

            let tdHeaderX10000 = document.createElement("td");
            tdHeaderX10000.id = "x10000";
            tdHeaderX10000.classList.add("td");
            tdHeaderX10000.classList.add("even");
            tdHeaderX10000.onclick = sortTable;
            trHeader.appendChild(tdHeaderX10000);
            let headerLabelX10000 = document.createElement("label");
            tdHeaderX10000.appendChild(headerLabelX10000);
            headerLabelX10000.innerText = "x10000";

            let tdHeaderX20000 = document.createElement("td");
            tdHeaderX20000.id = "x20000";
            tdHeaderX20000.classList.add("td");
            tdHeaderX20000.classList.add("even");
            tdHeaderX20000.onclick = sortTable;
            trHeader.appendChild(tdHeaderX20000);
            let headerLabelX20000 = document.createElement("label");
            tdHeaderX20000.appendChild(headerLabelX20000);
            headerLabelX20000.innerText = "x20000";

            let tdHeaderX50000 = document.createElement("td");
            tdHeaderX50000.id = "x50000";
            tdHeaderX50000.classList.add("td");
            tdHeaderX50000.classList.add("even");
            tdHeaderX50000.onclick = sortTable;
            trHeader.appendChild(tdHeaderX50000);
            let headerLabelX50000 = document.createElement("label");
            tdHeaderX50000.appendChild(headerLabelX50000);
            headerLabelX50000.innerText = "x50000";

            let tdHeaderDetails = document.createElement("td");
            trHeader.appendChild(tdHeaderDetails);
        }
    </script>
    <script>
        function getLines(){
            let request = {
                             page: window.page,
                             field : window.field,
                             type : window.type
                          };
            $.ajax({
                type: 'POST',
                url: '/SlotApplication/get-slots-table',
                data: request,
                success: function(data){
                    let table = document.getElementById("slotTable");

                    for(let i = 0; i<data.table.length; i++){
                        let trBody = document.createElement("tr");
                         trBody.classList.add("tr");
                         trBody.classList.add("tr-body");
                         table.appendChild(trBody);

                         let tdProvider = document.createElement("td");
                         tdProvider.classList.add("td");
                         trBody.appendChild(tdProvider);
                         let labelProvider = document.createElement("label");
                         tdProvider.appendChild(labelProvider);
                         labelProvider.innerText = data.table[i].provider;

                         let tdSlot = document.createElement("td");
                         tdSlot.classList.add("td");
                         trBody.appendChild(tdSlot);
                         let labelSlot = document.createElement("label");
                         tdSlot.appendChild(labelSlot);
                         labelSlot.innerText = data.table[i].slot.cleanSlot;

                         let tdRtp = document.createElement("td");
                         tdRtp.classList.add("td");
                         trBody.appendChild(tdRtp);
                         let labelRtp = document.createElement("label");
                         tdRtp.appendChild(labelRtp);
                         labelRtp.innerText = data.table[i].rtp;

                         let tdVolatility = document.createElement("td");
                         tdVolatility.classList.add("td");
                         trBody.appendChild(tdVolatility);
                         let labelVolatility = document.createElement("label");
                         tdVolatility.appendChild(labelVolatility);
                         labelVolatility.innerText = data.table[i].volatility;

                         let tdX100 = document.createElement("td");
                         tdX100.classList.add("td");
                         trBody.appendChild(tdX100);
                         let labelX100 = document.createElement("label");
                         tdX100.appendChild(labelX100);
                         labelX100.innerText = data.table[i].x100;

                         let tdX1000 = document.createElement("td");
                         tdX1000.classList.add("td");
                         trBody.appendChild(tdX1000);
                         let labelX1000 = document.createElement("label");
                         tdX1000.appendChild(labelX1000);
                         labelX1000.innerText = data.table[i].x1000;

                         let tdX5000 = document.createElement("td");
                         tdX5000.classList.add("td");
                         trBody.appendChild(tdX5000);
                         let labelX5000 = document.createElement("label");
                         tdX5000.appendChild(labelX5000);
                         labelX5000.innerText = data.table[i].x5000;

                         let tdX10000 = document.createElement("td");
                         tdX10000.classList.add("td");
                         trBody.appendChild(tdX10000);
                         let labelX10000 = document.createElement("label");
                         tdX10000.appendChild(labelX10000);
                         labelX10000.innerText = data.table[i].x10000;

                         let tdX20000 = document.createElement("td");
                         tdX20000.classList.add("td");
                         trBody.appendChild(tdX20000);
                         let labelX20000 = document.createElement("label");
                         tdX20000.appendChild(labelX20000);
                         labelX20000.innerText = data.table[i].x20000;

                         let tdX50000 = document.createElement("td");
                         tdX50000.classList.add("td");
                         trBody.appendChild(tdX50000);
                         let labelX50000 = document.createElement("label");
                         tdX50000.appendChild(labelX50000);
                         labelX50000.innerText = data.table[i].x50000;

                         let tdDetails = document.createElement("td");
                         tdDetails.classList.add("td");
                         trBody.appendChild(tdDetails);
                         let aDetails = document.createElement("a");
                         tdDetails.appendChild(aDetails);
                         aDetails.innerText = "Перейти";
                         aDetails.setAttribute("href", "/SlotApplication/slot?id="+data.table[i].slotId);
                    }
                    if(data.table.length < 10){
                        document.getElementById("nextButton").style.display = "none";
                    }
                }
            });
        }
    </script>
    <script>
        function sortTable(){
            let sortField = $(this).attr('id');
            let classes = $(this).attr('class');
            let cssClasses = classes.split(' ');
            window.page = 0;
            window.field = sortField;
            if (cssClasses.includes("even")){
                window.type = "asc";
                $(this).removeClass("even");
            } else {
                window.type = "desc";
                $(this).addClass("even");
            }
            let slotTable = document.getElementById("slotTable");
            if (slotTable !== null){
                $('.tr-body').remove();
            }
            document.getElementById("nextButton").style.display = "block";
            getLines();
        }
    </script>
    <script>
        function next(){
            page++;
            getLines();
        }
    </script>
</head>
<body>
    <div id="menu">
        <nav>
            <ul class="menu-ul">
              <li class="menu-li"><a class="menu-a" href='/SlotApplication/'>Home</a></li>
              <li class="menu-li"><a class="menu-a" href='/SlotApplication/tools'>Tools</a></li>
              <li class="menu-li"><a class="menu-a" href='/SlotApplication/slots'>Slots</a></li>
              <li class="menu-li"><a class="menu-a" href='/SlotApplication/jackpots'>Jackpots</a></li>
            </ul>
        </nav>
    </div>
    <%  CommonCasinoReport report = (CommonCasinoReport) request.getAttribute("report");
        List<ProviderAmount> topProviders = report.getTopProviders();
        String top1Provider = "";
        int top1ProviderSlots=0;
        if(topProviders.size()>0){
            top1Provider = topProviders.get(0).getProvider();
            top1ProviderSlots = topProviders.get(0).getSlots();
        }
        String top2Provider = "";
        int top2ProviderSlots=0;
        if(topProviders.size()>1){
            top2Provider = topProviders.get(1).getProvider();
            top2ProviderSlots = topProviders.get(1).getSlots();
        }
        String top3Provider = "";
        int top3ProviderSlots=0;
        if(topProviders.size()>2){
            top3Provider = topProviders.get(2).getProvider();
            top3ProviderSlots = topProviders.get(2).getSlots();
        }

        List<String> newSlots = report.getNewSlots();
        String newSlot1 = "";
        if(newSlots.size()>0){
            newSlot1 = newSlots.get(0);
        }
        String newSlot2 = "";
        if(newSlots.size()>1){
            newSlot2 = newSlots.get(1);
        }
        String newSlot3 = "";
        if(newSlots.size()>2){
            newSlot3 = newSlots.get(2);
        }
    %>
    <div id="totalSLotsAmount">
        <label>Total slots ${report.totalSlots}</label>
    </div>

    <div id="providersAmount">
        <label>Providers ${report.providers} </label>
    </div>

    <div id="totalSpinAmount">
        <label>Spins ${report.totalSpins} </label>
    </div>

    <div id="topProviders">
        <div id="top1Provider"><label>Provider Top-1: <%=top1Provider%> - <%=top1ProviderSlots%> слотов</label></div>
        <div id="top2Provider"><label>Provider Top-2: <%=top2Provider%> - <%=top2ProviderSlots%> слотов</label></div>
        <div id="top3Provider"><label>Provider Top-3: <%=top3Provider%> - <%=top3ProviderSlots%> слотов</label></div>
    </div>

    <div id="newSlots">
        <label>NEW SLOTS:</label>
        <div id="newSlot1"><label><%=newSlot1%></label></div>
        <div id="newSlot2"><label><%=newSlot2%></label></div>
        <div id="newSlot3"><label><%=newSlot3%></label></div>
    </div>
    <br>
    <div id="slotTablePlace">
    </div>
    <div>
        <button id="nextButton" onclick="next()">Ещё</button>
    </div>
     <script>createTable()</script>
     <script>getLines()</script>
</body>
</html>