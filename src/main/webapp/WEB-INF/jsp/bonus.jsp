<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>Bonuses</title>
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
      href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800;900&family=Oswald&display=swap"
      rel="stylesheet"
    />
    <style>
      html {
        height: 100%;
        scroll-behavior: smooth;
      }
      * {
        box-sizing: border-box;
      }
      body {
        margin: 0;
        font-family: "Montserrat", sans-serif;
        font-size: 14px;
        background-color: #181818;
      }

      /*общий grid*/
      .grid {
        display: grid;
        grid-template-columns: 0.5fr 1fr 2fr 1fr 0.5fr;
        grid-template-rows: auto 1fr auto;
        justify-content: center;
        align-content: flex-start;
        justify-items: center;
        box-sizing: border-box;
        min-height: 100%;
        min-width: 320px;
      }

      /*общий flex*/
      .flex {
        display: flex;
        justify-content: center;
        flex-flow: row wrap;
      }
      /*flex-контейнер*/
      .flex-row {
        display: flex;
        flex-flow: row wrap;
        max-width: 1200px;
        margin: 0 auto;
        justify-content: center;
      }
      /*flex-контейнер*/
      .flex-column {
        display: flex;
        max-width: 1200px; /*ширина контента*/
        flex-flow: column nowrap;
        margin: 0 auto;
        align-items: center;
      }

      header {
        grid-column: 3/4;
        grid-row: 1/2;
        width: 100%;
        /*height: 100vh;*/
        color: #fff;
      }

      nav {
        width: 100%;
        box-sizing: border-box;
        display: flex;
        flex-flow: row nowrap;
        height: 80px;
        justify-content: space-around;
        transition: background-color 0.5s;
      }

      ul.topnav {
        display: flex;
        list-style-type: none;
        width: 100%;
        justify-content: center;
        align-items: center;
        margin: 0;
        padding: 0;
      }

      ul.topnav li {
        text-align: center;
        background: #262626;
        border-radius: 2px 2px;
        height: auto;
        perspective: 900px;
      }

      /*style for rotate point of menu*/
      .roll-link {
        height: auto;
        display: block;
        width: 90px;
        transform-style: preserve-3d;
      }

      .roll-link .front {
        height: 40px;
        background: rgb(255, 184, 0);
        color: #262626;
      }

      .roll-link .back {
        opacity: 0;
        height: 40px;
        display: block;
        background: #262626;
        color: #f5f5f5;
        transform: translate3d(0, 0, -40px) rotate3d(1, 0, 0, 90deg);
      }

      .topnav li:first-child .roll-link .front,
      .topnav li:first-child .roll-link .back {
        border-radius: 5px 0px 0px 5px;
      }

      .topnav li:last-child .roll-link .front,
      .topnav li:last-child .roll-link .back {
        border-radius: 0px 5px 5px 0px;
      }

      .roll-link > a {
        display: block;
        position: absolute;
        width: 90px;
        text-decoration: none;
        padding: 1em;
        transition: all 0.3s ease-out;
        transform-origin: 50% 0%;
      }

      .topnav .roll-link:hover .front {
        transform: translate3d(0, 40px, 0) rotate3d(1, 0, 0, -90deg);
        backface-visibility: hidden;
        perspective: 1000;
        opacity: 0;
      }

      .topnav .roll-link:hover .back {
        transform: rotate3d(1, 0, 0, 0deg);
        backface-visibility: hidden;
        perspective: 1000;
        opacity: 1;
      }

      nav .btn-menu {
        display: none;
      }

      main {
        grid-column: 2/5;
        grid-row: 2/3;
        width: 100%;
      }

      .wrap-report-block {
        width: 100%;
        display: flex;
        align-items: center;
        justify-content: center;
        flex-direction: column;
        margin-top: 50px;
        /*background-color: #695753;*/
        border-radius: 10px;
        padding: 5px;
        transition: 0.3s;
        /*height: 0;
    opacity: 0;*/
        pointer-events: visible;
      }
      .multi-title {
        position: relative;
        margin-bottom: 50px;
        font-family: "Oswald", sans-serif;
        font-weight: normal;
        font-size: 35px;
        line-height: 65px;
        text-transform: uppercase;
        color: #ffffff;
        /*background-color: #695753;*/
      }

      .multi-title span {
        font-family: "Oswald", sans-serif;
        color: #ffb800;
      }

      .report-block {
        /*width: calc(100% + 40px);*/
        width: 100%;
        display: flex;
        align-items: stretch;
        justify-content: center;
        /*background-color: #695753;*/
      }

      .report-block .left {
        width: 100%;
        max-width: 394px;
        margin: 0 10px;
      }

      .report-block .right {
        width: 100%;
        max-width: 807px;
        margin: 0 10px;
      }

      .report-block .item {
        padding: 15px 20px;
        background: #181818;
        border: 2px solid #333333;
        border-radius: 10px;
        margin-bottom: 20px;
        min-height: 70px;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
      }

      .report-block .item.item-row {
        -webkit-box-orient: horizontal;
        -webkit-box-direction: normal;
        flex-direction: row;
        -webkit-box-align: stretch;
        align-items: stretch;
      }

      .report-block .item .item-text-row-info {
        width: 100%;
        display: flex;
        -webkit-box-pack: justify;
        justify-content: space-between;
        -webkit-box-align: center;
        align-items: center;
      }

      .report-block .item .item-text-row-info .wrap {
        display: flex;
        -webkit-box-align: center;
        align-items: center;
      }

      .report-block .item .item-text-row-info .wrap span {
        font-weight: 300;
        font-size: 16px;
        line-height: 24px;
        color: rgb(255, 255, 255);
      }

      .report-block .item .item-text-row-info .price {
        display: flex;
        -webkit-box-align: center;
        align-items: center;
      }

      .report-block .item .item-text-row-info .price span {
        margin-right: 10px;
        display: block;
        font-weight: 300;
        font-size: 16px;
        line-height: 24px;
        color: rgb(255, 255, 255);
      }

      .report-block .item .line {
        border: 1px solid #333333;
        margin: 0px 30px;
      }
      .report-block .item .line-row {
        margin: 28px 0;
        border: 1px solid #333333;
        width: 100%;
      }

      .report-block .item .item-text-row {
        width: 100%;
        display: flex;
        justify-content: space-between;
        align-items: center;
      }

      .report-block .item .item-text-row .title {
        display: flex;
        align-items: center;
        text-align: left;
      }

      .report-block .item .item-text-row .title span {
        font-weight: 300;
        font-size: 16px;
        line-height: 90%;
        color: #ffffff;
      }

      .report-block .item .item-text-row p {
        font-weight: 600;
        font-size: 16px;
        line-height: 22px;
        text-align: right;
        color: #ffb800;
      }

      .report-block .item .row-winrate {
        width: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
      }

      .report-block .item .row-winrate .title {
        margin-bottom: 33px;
        font-weight: 600;
        font-size: 16px;
        line-height: 22px;
        text-align: center;
        color: #ffffff;
      }

      .report-block .item .row-winrate .blocks {
        width: 100%;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        flex-direction: column;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        -webkit-box-align: stretch;
        -ms-flex-align: stretch;
        align-items: stretch;
      }

      .report-block .item .row-winrate .blocks .block {
        padding: 0 10px;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        flex-direction: row;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        border-top: 1px solid #333333;
      }

      .report-block .item .row-winrate .blocks .block:first-child {
        border: none;
      }

      .blocks .block span {
        display: block;
        margin-bottom: 10px;
        font-weight: 600;
        font-size: 16px;
        line-height: 20px;
        text-align: center;
        color: #ffffff;
        flex: 1;
      }

      .blocks .block span.block-title {
        font-weight: 400;
        font-size: 14px;
      }

      .blocks .block p {
        font-weight: 500;
        font-size: 16px;
        line-height: 20px;
        text-align: center;
        color: #ffb800;
        flex: 1;
        padding: 0px 5px;
      }

      .blocks .block p.sim {
        font-size: 14px;
        font-weight: 400;
        margin: 5px 0px;
      }

      .report-block .item .bonus {
        width: 100%;
        display: flex;
        -webkit-box-pack: center;
        justify-content: center;
        -webkit-box-align: center;
        align-items: center;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        flex-direction: column;
      }

      .report-block .item .bonus .title {
        margin-top: 15px;
        margin-bottom: 30px;
        font-weight: 600;
        font-size: 16px;
        line-height: 22px;
        text-align: center;
        color: rgb(255, 255, 255);
      }

      .report-block .item .bonus .block {
        margin-bottom: 45px;
        width: 175px;
        height: 156px;
        display: flex;
        -webkit-box-pack: center;
        justify-content: center;
        -webkit-box-align: center;
        align-items: center;
        position: relative;
      }

      .report-block .item .bonus .block .bonus-speed {
        position: absolute;
        transform: rotate(-140deg);
        transform-origin: 50% 64%;
        margin-top: 0px;
        transition: all 0.3s ease 0s;
      }

      .report-block .item .bonus .block span {
        position: absolute;
        bottom: -25px;
        font-weight: 500;
        font-size: 30px;
        line-height: 37px;
        text-align: center;
        color: rgb(255, 184, 0);
      }

      .report-block .item .forms {
        width: 100%;
        display: flex;
        justify-content: space-between;
        align-items: flex-end;
        flex-wrap: wrap;
        margin-top: 10px;
        margin-bottom: 15px;
      }

      .report-block .item .forms .forms-item {
        width: calc(50% - 20px);
        margin-bottom: 15px;
        display: flex;
        align-items: flex-start;
        flex-direction: column;
        position: relative;
        pointer-events: none;
      }

      .report-block .item .forms .forms-item span {
        display: block;
        margin-bottom: 5px;
        font-weight: normal;
        font-size: 12px;
        line-height: 15px;
        color: #ffffff;
        text-align: left;
        width: 100%;
      }

      .report-block .item .forms .forms-item input {
        background: rgba(255, 255, 255, 0.1);
        border-radius: 50px;
        border: none;
        padding: 10px 15px;
        font-weight: normal;
        font-size: 12px;
        width: 100%;
        line-height: 15px;
        color: #ffffff;
        text-transform: capitalize;
      }

      .report-block .item .title-info {
        margin: 10px 0 15px 0;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        text-align: center;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        width: 100%;
      }
      .report-block .item .title-info span {
        font-weight: 500;
        font-size: 16px;
        line-height: 22px;
        text-align: center;
        color: #ffffff;
      }

      .title-block .item .title-info span {
        font-weight: 500;
        font-size: 18px;
        line-height: 22px;
        text-align: center;
        color: #ffffff;
      }

      .report-block .item .wrap-chart-line {
        width: 100%;
        position: relative;
        min-height: 274px;
      }

      .report-block .item .chart-line {
        width: 100%;
      }

      .report-block .item .chart-line-blocks {
        width: 100%;
        height: 100%;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        -webkit-box-align: stretch;
        -ms-flex-align: stretch;
        align-items: stretch;
        position: absolute;
        left: 0;
        top: 0;
        overflow: hidden;
      }

      .report-block .item .chart-line-blocks .block-wrap {
        width: 100%;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: end;
        -ms-flex-align: end;
        align-items: flex-end;
        -webkit-box-pack: start;
        -ms-flex-pack: start;
        justify-content: flex-start;
        position: relative;
      }
      .report-block .item .chart-line-blocks .block-wrap .block {
        width: 100%;
        min-width: 100px;
        min-height: 255px;
        margin-right: 6px;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: justify;
        -ms-flex-pack: justify;
        justify-content: space-between;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        padding: 20px 0;
        -webkit-transition: 0.3s linear;
        transition: 0.3s linear;
        -webkit-transform: translate(0, 0);
        transform: translate(0, 0);
        position: relative;
        background-color: #262626;
        border-radius: 10px;
      }

      .report-block .item .chart-line-blocks .block-wrap .block::before {
        content: "";
        width: 100%;
        min-height: 255px;
        position: absolute;
        left: 0;
        bottom: 0;
        z-index: -1;
        background-color: #262626;
        border-radius: 10px;
        -webkit-transition: 0.3s;
        transition: 0.3s;
      }

      .report-block .item .chart-line-blocks .block-wrap .block:hover::before {
        content: "";
        transform: translateY(-15px);
      }

      .report-block .row-item {
        width: 100%;
        margin-left: -10px;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: stretch;
        -ms-flex-align: stretch;
        align-items: stretch;
      }

      .report-block .row-item .item {
        width: 100%;
        margin: 0 10px;
        margin-bottom: 20px !important;
        padding: 30px 20px;
      }

      .report-block .item .chart-radius {
        width: 100%;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: justify;
        -ms-flex-pack: justify;
        justify-content: space-between;
        -webkit-box-align: stretch;
        -ms-flex-align: stretch;
        align-items: stretch;
      }

      .report-block .item .chart-radius .chart {
        width: 110px;
        height: 110px;
        min-width: 110px;
      }

      .report-block .item .chart-radius .chart canvas {
        width: 100%;
        height: 100%;
      }

      .report-block .item .chart-radius .line {
        margin: 0 37px;
      }

      .report-block .item .chart-radius .text {
        width: auto;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: start;
        -ms-flex-align: start;
        align-items: flex-start;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        text-align: left;
      }
      .report-block .item .chart-radius .text span {
        display: block;
        margin-bottom: 16px;
        font-weight: 600;
        font-size: 16px;
        line-height: 22px;
        color: #ffffff;
      }

      .report-block .item .chart-radius .text .plus {
        margin-bottom: 6px;
        font-weight: 500;
        font-size: 14px;
        line-height: 17px;
        color: #ffeec2;
      }
      .report-block .item .chart-radius .text .zero {
        font-weight: 500;
        font-size: 14px;
        line-height: 17px;
        color: #ffb800;
      }

      .report-block .item .chart-custom {
        width: 100%;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: stretch;
        -ms-flex-align: stretch;
        align-items: stretch;
        margin-bottom: 15px;
        -webkit-transition: 0.3s;
        transition: 0.3s;
        overflow: hidden;
      }
      .report-block .item .chart-custom .block {
        width: 100%;
        max-width: 130px;
        min-width: 20px;
        margin-right: 24px;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: end;
        -ms-flex-pack: end;
        justify-content: flex-end;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        -webkit-transition: 0.3s;
        transition: 0.3s;
      }
      .report-block .item .chart-custom .block .fail-row {
        margin-bottom: 11px;
        font-weight: 500;
        font-size: 12px;
        line-height: 14px;
        text-align: center;
        color: #959595;
        opacity: 0;
        -webkit-transition: 0.3s linear;
        transition: 0.3s linear;
      }

      .report-block .item .chart-custom .block .block-lines {
        width: 100%;
        margin-bottom: 10px;
      }
      .report-block .item .chart-custom .block .block-lines .block-lines-item {
        width: 100%;
        height: 3px;
        margin-top: 2px;
        background: #ffb800;
        border-radius: 5px;
      }
      .report-block .item .chart-custom .block .count {
        font-weight: bold;
        font-size: 14px;
        line-height: 16px;
        text-align: center;
        color: rgba(255, 255, 255, 0.6);
        margin-bottom: 10px;
        -webkit-transition: 0.3s;
        transition: 0.3s;
      }
      .report-block .item .chart-custom .block .frequency {
        font-weight: 500;
        font-size: 12px;
        line-height: 14px;
        text-align: center;
        color: #959595;
        opacity: 0;
        -webkit-transition: 0.3s linear;
        transition: 0.3s linear;
      }
      .report-block .item .chart-custom .block.activeOpacity {
        opacity: 0.3;
      }

      .report-block .item .chart-custom .block.active {
        opacity: 1;
      }

      .report-block .item .chart-custom .block:last-child {
        margin-right: 0;
      }

      .report-block .item .chart-custom .block:hover .fail-row,
      .report-block .item .chart-custom .block:hover .frequency {
        opacity: 1;
      }

      .report-block .item .chart-custom .block:hover .count {
        color: #fff;
      }

      .report-block .item .chart-radius .chart canvas {
        box-sizing: border-box;
        display: block;
        height: 110px;
        width: 110px;
      }

      .report-block .item.chart-block-hid {
        overflow: hidden;
      }

     
      
      .report-block .item .chart-line-blocks .block-wrap .block .top {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        text-align: center;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        -webkit-transition: 0.5s;
        transition: 0.5s;
      }

      .report-block .item .chart-line-blocks .block-wrap .block:hover {
        padding-top: 5px;
      }
      .report-block .item .chart-line-blocks .block-wrap .block .top span {
        font-weight: bold;
        font-size: 16px;
        line-height: 20px;
        text-align: center;
        color: rgba(255, 255, 255, 0.6);
        -webkit-transition: 0.3s linear;
        transition: 0.3s linear;
      }
      .report-block .item .chart-line-blocks .block-wrap .block .top p {
        font-weight: 500;
        font-size: 12px;
        line-height: 15px;
        text-align: center;
        color: rgba(255, 255, 255, 0.6);
        -webkit-transition: 0.3s linear;
        transition: 0.3s linear;
      }

      .report-block .item .chart-line-blocks .block-wrap .block:hover .top p {
        color: rgba(255, 255, 255, 1);
      }

      .report-block .item .chart-line-blocks .block-wrap .block .bottom {
        font-weight: bold;
        font-size: 14px;
        line-height: 17px;
        text-align: center;
        color: rgba(255, 255, 255, 0.6);
        -webkit-transition: 0.3s linear;
        transition: 0.3s linear;
      }
      .report-block .item .chart-line-blocks .block-wrap .block .linear,
      .report-block .item .chart-line-blocks .block-wrap .block .linear-zero {
        width: 0;
        position: absolute;
        left: 50%;
        bottom: 20%;
        height: 2px;
        background: #ffb800;
        z-index: 6;
        -webkit-transform-origin: 0% 0%;
        transform-origin: 0% 0%;
      }
      .report-block .item .chart-line-blocks .block-wrap .block.lst {
        margin-right: 0;
      }

      .report-block .item .chart-line-blocks .block-wrap .linePos {
        position: absolute;
        bottom: 20%;
        z-index: 10;
        pointer-events: none;
        height: 6px;
        width: 6px;
        border-radius: 50%;
        background: #ffffff;
        -webkit-transition: 0.3s;
        transition: 0.3s;
      }
      .i-info {
        cursor: pointer;
        display: block;
        color: #262626;
        padding-left: 5px;
      }
      .report-block .item .bonus .info {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        margin-bottom: 15px;
      }

      .report-block .item .bonus .info span {
        display: block;
        margin-right: 10px;
        font-weight: 500;
        font-size: 16px;
        line-height: 20px;
        color: rgba(255, 255, 255, 0.5);
      }
      .pointer {
        width: auto;
        position: fixed;
        left: 0;
        z-index: 1000;
        min-width: 140px;
        height: auto;
        max-width: 330px;
        padding: 16px 10px;
        background: #404040;
        box-shadow: 0px 0px 10px rgb(0 0 0 / 25%);
        border-radius: 10px;
        display: -webkit-inline-box;
        display: -ms-inline-flexbox;
        display: inline-flex;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        opacity: 0;
        pointer-events: none;
        -webkit-transition: opacity 0.3s linear;
        transition: opacity 0.3s linear;
      }
      .pointer.active {
        opacity: 1;
        pointer-events: visible;
      }
      .pointer.activeLeft {
        opacity: 1;
        pointer-events: visible;
      }

      .pointer.activeLeft::before {
        left: unset;
        right: -10px;
        -webkit-transform: rotate(180deg);
        transform: rotate(180deg);
        background-position: right center;
      }

      .pointer.activeBottom {
        opacity: 1;
        left: 50%;
        -webkit-transform: translate(-50%, 0);
        transform: translate(-50%, 0);
        pointer-events: visible;
        min-width: 250px;
      }

      .pointer.activeBottom::before {
        left: unset;
        top: -15px;
        right: unset;
        -webkit-transform: rotate(90deg);
        transform: rotate(90deg);
        background-position: top center;
      }
      .pointer::before {
        content: "";
        width: 10px;
        height: 20px;
        position: absolute;
        left: -10px;
        z-index: -1;
        background: url(https://bonusev.com/images/arrow-pointer.svg) left
          center no-repeat;
        background-size: contain;
      }

      .pointer .logos-row {
        width: auto;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
      }
      .pointer .logos-row .item-text {
        width: 100%;
        max-width: 280px;
        padding: 5px 10px;
      }
      .pointer .logos-row .item-text span {
        display: block;
        margin-bottom: 20px;
        font-weight: normal;
        font-size: 16px;
        line-height: 20px;
        color: #ffffff;
      }
      .pointer .logos-row .item-text a {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: start;
        -ms-flex-pack: start;
        justify-content: flex-start;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        font-weight: 600;
        font-size: 16px;
        line-height: 20px;
        text-transform: uppercase;
        color: #ffb800;
        -webkit-transition: 0.3s;
        transition: 0.3s;
      }
      .pointer .logos-row .item-text a:hover {
        color: #ffd56a;
      }

      .pointer .logos-row .item-text a:hover svg path {
        stroke: #ffd56a;
      }

      .pointer .logos-row .item-text a:active {
        color: #ffb800;
      }

      .pointer .logos-row .item-text a:active svg path {
        stroke: #ffb800;
      }
      .pointer .logos-row .item-text a svg {
        margin-left: 20px;
        -webkit-transition: 0.3s;
        transition: 0.3s;
      }
      .pointer .logos-row .item-text a svg path {
        -webkit-transition: 0.3s;
        transition: 0.3s;
      }
      .report-block .item .item-text-row .title .img {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        margin-right: 15px;
      }
      .report-block .item .item-text-row-info .wrap .img {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        margin-right: 30px;
      }

      @media (max-width: 500px) {
        .grid {
          grid-template-columns: 0.5fr 0.5fr 2fr 0.5fr 0.5fr;
        }
        header {
          grid-column: 2/5;
        }
        nav {
          flex-direction: row;
          height: auto;
          margin-top: 5px;
          justify-content: start;
        }

        nav .btn-menu {
          display: block;
          color: #f5f5f5;
          float: right;
          margin-right: 10px;
        }

        ul.topnav {
          display: none;
        }
        ul.topnav li {
          width: 90%;
          height: 40px;
        }

        .topnav li a {
          width: 100%;
          padding: 0.5em;
        }
        .topnav li:first-child .roll-link .front,
      .topnav li:first-child .roll-link .back {
        border-radius: 5px 5px 0px 0px;
      }

      .topnav li:last-child .roll-link .front,
      .topnav li:last-child .roll-link .back {
        border-radius: 0px 0px 5px 5px;
      }

        .roll-link {
          width: 100%;
        }

        .topnav.responsive {
          display: block;
        }
      }

      @media (max-width: 1040px) {
        .report-block,
        .report-block .row-item {
          flex-direction: column;
        }
      }
    </style>
  </head>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
  <body>
    <!-- Грид-контейенер общий -->
    <div class="grid">
      <!--Header-->
      <header>
        <!--блок навгации-->
        <nav class="navigation">
          <!-- Кнопка раскрытия меню -->
          <a href="javascript:void(0)" class="btn-menu">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="24"
              height="24"
              viewBox="0 0 24 24"
              style="fill:white"
            >
              <path
                d="M3.5,7 C3.22385763,7 3,6.77614237 3,6.5 C3,6.22385763 3.22385763,6 3.5,6 L20.5,6 C20.7761424,6 21,6.22385763 21,6.5 C21,6.77614237 20.7761424,7 20.5,7 L3.5,7 Z M3.5,12 C3.22385763,12 3,11.7761424 3,11.5 C3,11.2238576 3.22385763,11 3.5,11 L20.5,11 C20.7761424,11 21,11.2238576 21,11.5 C21,11.7761424 20.7761424,12 20.5,12 L3.5,12 Z M3.5,17 C3.22385763,17 3,16.7761424 3,16.5 C3,16.2238576 3.22385763,16 3.5,16 L20.5,16 C20.7761424,16 21,16.2238576 21,16.5 C21,16.7761424 20.7761424,17 20.5,17 L3.5,17 Z"
              />
            </svg>
          </a>
          <ul class="topnav" id="myTopnav">
            <li>
              <div class="roll-link">
                <a href="/SlotApplication/" class="front">Home</a>
                <a href="/SlotApplication/" class="back">Home</a>
              </div>
            </li>
            <li>
              <div class="roll-link">
                <a href="/SlotApplication/tools" class="front">Tools</a>
                <a href="/SlotApplication/tools" class="back">Tools</a>
              </div>
            </li>
            <li>
              <div class="roll-link">
                <a href="/SlotApplication/slots" class="front">Slots</a>
                <a href="/SlotApplication/slots" class="back">Slots</a>
              </div>
            </li>
            <li>
              <div class="roll-link">
                <a href="/SlotApplication/jackpots" class="front">Jackpots</a>
                <a href="/SlotApplication/jackpots" class="back">Jackpots</a>
              </div>
            </li>
            <li>
              <div class="roll-link">
                <a href="/SlotApplication/sites" class="front">Sites</a>
                <a href="/SlotApplication/sites" class="back">Sites</a>
              </div>
            </li>
          </ul>
        </nav>
        <!--/ блок навгации-->
      </header>
      <!--/Header-->

      <!--main-->
      <main>
        <div class="wrap-report-block">
          <div class="multi-title">ИТОГОВЫЙ <span>ОТЧЕТ</span></div>
          <div class="report-block">
            <div class="left">
              <div class="item">
                <div class="item-text-row">
                  <div class="title">
                    <div class="img">
                      <img src="https://bonusev.com/images/i-1.svg" />
                    </div>
                    <span>Средний бонусный выигрыш</span>
                  </div>
                  <p class="average-win"></p>
                </div>
              </div>
              <div class="item">
                <div class="item-text-row">
                  <div class="title">
                    <div class="img">
                      <img src="https://bonusev.com/images/i-2.svg" />
                    </div>
                    <span>Количество спинов </span>
                  </div>
                  <p class="bet-counter"></p>
                </div>
              </div>
              <div class="item">
                <div class="row-winrate">
                  <div class="title">Самые крупные Profit и их вероятности</div>
                  <div class="blocks blocks-top">
                    <div class="block">
                      <span></span><span class="block-title">Сумма</span
                      ><span class="block-title">Частота</span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="bonus">
                  <div class="title">Дисперсия бонуса</div>
                  <div class="block">
                    <img src="https://bonusev.com/images/bonus.svg" alt />
                    <div class="bonus-speed">
                      <img
                        src="https://bonusev.com/images/bonus-speed.svg"
                        alt
                      />
                    </div>
                    <span class="bonus-volatility"></span>
                  </div>
                  <div class="info">
                    <span>Инфо</span>
                    <div
                      class="i-info"
                      data-type="text"
                      data-content="bonusVolatility"
                    >
                      <img src="https://bonusev.com/images/i-info.svg" />
                    </div>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="forms"></div>
              </div>
            </div>
            <div class="right">
              <div class="item item-row">
                <div class="item-text-row-info">
                  <div class="wrap">
                    <div class="img">
                      <img src="https://bonusev.com/images/i-3.svg" />
                    </div>
                    <span>EV игрока</span>
                  </div>
                  <div class="price">
                    <span class="ev-player"></span>
                    <div
                      class="i-info"
                      data-type="text"
                      data-content="evPlayer"
                    >
                      <img src="https://bonusev.com/images/i-info.svg" />
                    </div>
                  </div>
                </div>
                <div class="line"></div>
                <div class="item-text-row-info">
                  <div class="wrap">
                    <div class="img">
                      <img src="https://bonusev.com/images/i-3.svg" />
                    </div>
                    <span>EV казино</span>
                  </div>
                  <div class="price">
                    <span class="ev-casino"></span>
                    <div
                      class="i-info"
                      data-type="text"
                      data-content="evCasino"
                    >
                      <img src="https://bonusev.com/images/i-info.svg" />
                    </div>
                  </div>
                </div>
              </div>
              <div class="item item-row">
                <div class="item-text-row-info">
                  <div class="wrap">
                    <div class="img">
                      <img src="https://bonusev.com/images/i-4.svg" />
                    </div>
                    <span>ROI игрока</span>
                  </div>
                  <div class="price">
                    <span class="roi-player"></span>
                    <div
                      class="i-info"
                      data-type="text"
                      data-content="roiPlayer"
                    >
                      <img src="https://bonusev.com/images/i-info.svg" />
                    </div>
                  </div>
                </div>
                <div class="line"></div>
                <div class="item-text-row-info">
                  <div class="wrap">
                    <div class="img">
                      <img src="https://bonusev.com/images/i-4.svg" />
                    </div>
                    <span>ROI казино</span>
                  </div>
                  <div class="price">
                    <span class="roi-casino"></span>
                    <div
                      class="i-info"
                      data-type="text"
                      data-content="roiCasino"
                    >
                      <img src="https://bonusev.com/images/i-info.svg" />
                    </div>
                  </div>
                </div>
              </div>
              <div class="item chart-block-hid">
                <div class="title-info">
                  <span> Вероятность увеличить депозит в [X] раз </span>
                  <div
                    class="i-info"
                    data-type="text"
                    data-content="depositMultipliers"
                  >
                    <img src="https://bonusev.com/images/i-info.svg" />
                  </div>
                </div>
                <div class="wrap-chart-line">
                  <div class="chart-line">
                    <div class="chart-line-blocks">
                      <div class="block-wrap">
                        <div class="block">
                          <div class="top">
                            <span></span>
                            <p>симуляций</p>
                          </div>
                          <div class="bottom">x1 dep</div>
                          <div class="linear"></div>
                        </div>
                        <div class="block">
                          <div class="top">
                            <span></span>
                            <p>симуляций</p>
                          </div>
                          <div class="bottom">x2 dep</div>
                          <div class="linear"></div>
                        </div>
                        <div class="block">
                          <div class="top">
                            <span></span>
                            <p>симуляций</p>
                          </div>
                          <div class="bottom">x5 dep</div>
                          <div class="linear"></div>
                        </div>
                        <div class="block">
                          <div class="top">
                            <span></span>
                            <p>симуляций</p>
                          </div>
                          <div class="bottom">x10 dep</div>
                          <div class="linear"></div>
                        </div>
                        <div class="block">
                          <div class="top">
                            <span></span>
                            <p>симуляций</p>
                          </div>
                          <div class="bottom">x20 dep</div>
                          <div class="linear"></div>
                        </div>
                        <div class="block">
                          <div class="top">
                            <span></span>
                            <p>симуляций</p>
                          </div>
                          <div class="bottom">x50 dep</div>
                          <div class="linear"></div>
                        </div>
                        <div class="block lst">
                          <div class="top">
                            <span></span>
                            <p>симуляций</p>
                          </div>
                          <div class="bottom">x100 dep</div>
                          <div class="linear"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row-item">
                <div class="item">
                  <div class="chart-radius">
                    <div class="chart">
                      <canvas id="chartRadius"></canvas>
                    </div>
                    <div class="line"></div>
                    <div class="text">
                      <span>
                        Вероятность <br />
                        отыграть бонус
                      </span>
                      <div class="plus profit-probability"></div>
                      <div class="zero non-profit-probability"></div>
                    </div>
                  </div>
                </div>
                <div class="item">
                  <div class="item-text-row mod-text">
                    <div class="title">
                      <div class="img">
                        <img src="https://bonusev.com/images/i-5.svg" />
                      </div>
                      <span> MAX серия выигрышей </span>
                    </div>
                    <p class="longest-profit-series"></p>
                  </div>
                  <div class="line-row"></div>
                  <div class="item-text-row mod-text">
                    <div class="title">
                      <div class="img">
                        <img src="https://bonusev.com/images/i-6.svg" />
                      </div>
                      <span> MAX серия проигрышей </span>
                    </div>
                    <p class="longest-non-profit-series"></p>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="title-info">
                  <span> Шансы проиграть [N] депозитов подряд </span>
                  <div class="i-info" data-type="text" data-content="series">
                    <img src="https://bonusev.com/images/i-info.svg" />
                  </div>
                </div>
                <div class="chart-custom non-profit-series">
                  <div class="block">
                    <div class="fail-row">Частота</div>
                    <div class="count count-ser"></div>
                    <div class="block-lines">
                      <div class="block-lines-item"></div>
                    </div>
                    <div class="count">5</div>
                    <div class="frequency">Проигрыши подряд</div>
                  </div>
                  <div class="block">
                    <div class="fail-row">Частота</div>
                    <div class="count count-ser"></div>
                    <div class="block-lines">
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                    </div>
                    <div class="count">10</div>
                    <div class="frequency">Проигрыши подряд</div>
                  </div>
                  <div class="block">
                    <div class="fail-row">Частота</div>
                    <div class="count count-ser"></div>
                    <div class="block-lines">
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                    </div>
                    <div class="count">25</div>
                    <div class="frequency">Проигрыши подряд</div>
                  </div>
                  <div class="block">
                    <div class="fail-row">Частота</div>
                    <div class="count count-ser"></div>
                    <div class="block-lines">
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                    </div>
                    <div class="count">50</div>
                    <div class="frequency">Проигрыши подряд</div>
                  </div>
                  <div class="block">
                    <div class="fail-row">Частота</div>
                    <div class="count count-ser"></div>
                    <div class="block-lines">
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                    </div>
                    <div class="count">100</div>
                    <div class="frequency">Проигрыши подряд</div>
                  </div>
                  <div class="block">
                    <div class="fail-row">Частота</div>
                    <div class="count count-ser"></div>
                    <div class="block-lines">
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                    </div>
                    <div class="count">250</div>
                    <div class="frequency">Проигрыши подряд</div>
                  </div>
                  <div class="block">
                    <div class="fail-row">Частота</div>
                    <div class="count count-ser"></div>
                    <div class="block-lines">
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                      <div class="block-lines-item"></div>
                    </div>
                    <div class="count">500</div>
                    <div class="frequency">Проигрыши подряд</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
      <!-- /main -->

      <!-- footer -->
      <footer></footer>
      <!-- /footer -->
    </div>
    <div class="pointer" data-pointer="text">
      <div class="logos-row">
        <div class="item-text">
          <span> Подсказка </span>
          <!--<a href="#tarif">
            ВЫБРАТЬ ТАРИФ
            <svg
              width="14"
              height="14"
              viewBox="0 0 14 14"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M1 13L13 1M13 1H1M13 1V13"
                stroke="#FFB800"
                stroke-width="2"
              />
            </svg>
          </a>-->
        </div>
      </div>
    </div>
    <script>

      const leftBlock = document.querySelector(".left");
      const rightBlock = document.querySelector(".right");
      const averageWin = document.querySelector(".average-win");
      const betCounter = document.querySelector(".bet-counter");
      const topProfits = document.querySelector(".blocks");
      const formInput = document.querySelector(".forms");
      const bonusVolatility = document.querySelector(".bonus-volatility");
      const bonusSpeed = document.querySelector(".bonus-speed");
      const evPlayer = document.querySelector(".ev-player");
      const evCasino = document.querySelector(".ev-casino");
      const roiPlayer = document.querySelector(".roi-player");
      const roiCasino = document.querySelector(".roi-casino");
      const plus = document.querySelector(".plus");
      const zero = document.querySelector(".zero");
      const profitSeries = document.querySelector(".longest-profit-series");
      const nonProfitSeries = document.querySelector(
        ".longest-non-profit-series"
      );
      const nonProfitBlocks = document.querySelectorAll(
        ".non-profit-series .block"
      );
      const nonProfitSeriesSection =
        document.querySelector(".non-profit-series");

      let request = { url: window.location.href };
      $.ajax({
        type: "POST",
        url: "/SlotApplication/get-bonus",
        data: request,
        success: function (data) {
          drawReport(data);
        },
      });

      function drawReport(data) {
        console.log(data);
        averageWin.textContent = data.commonSimInformation.averageWin;
        betCounter.textContent = data.commonSimInformation.betCounter;
        let bonusVolatilityValue = data.commonSimInformation.bonusVolatility;
        bonusVolatility.textContent = parseFloat(bonusVolatilityValue.toFixed(2));
        if (bonusVolatilityValue <= 100){
        bonusSpeed.style.transform =
          "rotate(" +
          (-140 +
            (280 / 100) *
              parseFloat(bonusVolatilityValue)) +
          "deg)";
        } else {
          bonusSpeed.style.transform =
          "rotate(" +
          (-140 +
            (280 / 100) *
              100) +
          "deg)";
        }
        evPlayer.textContent = parseFloat(
          data.commonSimInformation.ev.toFixed(1)
        );
        evCasino.textContent = -parseFloat(
          data.commonSimInformation.ev.toFixed(1)
        );
        roiPlayer.textContent = parseFloat(
          data.commonSimInformation.roi.toFixed(1)
        );
        roiCasino.textContent = -parseFloat(
          data.commonSimInformation.roi.toFixed(1)
        );
        plus.textContent =
          "В плюс " +
          parseFloat(data.commonSimInformation.profitProbability.toFixed(1));
        zero.textContent =
          "В ноль " +
          parseFloat(data.commonSimInformation.nonProfitProbability.toFixed(1));
        profitSeries.textContent =
          data.commonSimInformation.longestProfitSeries;
        nonProfitSeries.textContent =
          data.commonSimInformation.longestNonProfitSeries;

        data.topProfits.forEach((item) => {
          let block = document.createElement("div");
          block.classList.add("block");
          topProfits.append(block);
          block.innerHTML =
            "<span>Топ-" +
            item.name +
            "</span><p>" +
            item.profit.toLocaleString("ru-RU") +
            '</p><p class="sim">раз в ' +
            item.frequency.toLocaleString("ru-RU") +
            " симуляций</p>";
        });

        //Входящие данные
        const provider = {
          name: "Провайдер",
          value: data.slotInformation.provider,
        };
        const slot = { name: "Слот", value: data.slotInformation.slot };
        const deposit = {
          name: "Депозит",
          value: data.slotInput.deposit.toLocaleString("ru-RU"),
        };
        const bonus = {
          name: "Бонус",
          value: data.slotInput.bonus.toLocaleString("ru-RU"),
        };
        const bet = {
          name: "Ставка",
          value: data.slotInput.bet.toLocaleString("ru-RU"),
        };
        const waggerType = {
          name: "Wagger Type",
          value: data.slotInput.waggerType,
        };
        const wagger = { name: "Wagger", value: data.slotInput.wagger };
        const sim = {
          name: "Симуляции",
          value: data.slotInput.sim.toLocaleString("ru-RU"),
        };
        const withoutBonus = {
          name: "Результат без учета бонуса",
          value: data.slotInput.withoutBonus == true ? "Да" : "Нет",
        };
        const taxes = {
          name: "Налог",
          value: data.slotInput.taxes.toLocaleString("ru-RU"),
        };
        const profitLimit = {
          name: "Максимальное значение выигрыша одной симуляции",
          value: data.slotInput.profitLimit.toLocaleString("ru-RU"),
        };
        const bonusParts = {
          name: "Количество частей бонуса",
          value: data.slotInput.bonusParts,
        };
        const arrInput = [
          provider,
          slot,
          deposit,
          bonus,
          bet,
          sim,
          wagger,
          waggerType,
          withoutBonus,
          taxes,
          profitLimit,
          bonusParts,
        ];

        arrInput.forEach((element) => {
          let itemForm = document.createElement("div");
          itemForm.classList.add("forms-item");
          formInput.append(itemForm);
          itemForm.innerHTML =
            "<span>" +
            element.name +
            '</span> <input type="text" value="' +
            element.value +
            '">';
        });

        //Блок "Шансы проиграть Х депозиоов подряд"
        let arrNonProfit = [];
        data.nonProfitSeries.forEach(item => {
          arrNonProfit.push(item.series);
        });

        const dataNonProfit = [
          {
            ser: arrNonProfit[0],
            line: arrNonProfit[0] ? (arrNonProfit[6] ? arrNonProfit[0] * 40/arrNonProfit[6] : (arrNonProfit[5] ? arrNonProfit[0] * 40/arrNonProfit[5] : (arrNonProfit[4] ? arrNonProfit[0] * 40/arrNonProfit[4] : (arrNonProfit[3] ? arrNonProfit[0] * 40/arrNonProfit[3] : (arrNonProfit[2] ? arrNonProfit[0] * 40/arrNonProfit[2] : (arrNonProfit[1] ? arrNonProfit[0] * 40/arrNonProfit[1] : 40)))))) : undefined,
            count: 5,
          },
          {
            ser: arrNonProfit[1],
            line: arrNonProfit[1] ? (arrNonProfit[6] ? arrNonProfit[1] * 40/arrNonProfit[6] + 1 : (arrNonProfit[5] ? arrNonProfit[1] * 40/arrNonProfit[5] + 1 : (arrNonProfit[4] ? arrNonProfit[1] * 40/arrNonProfit[4] + 1 : (arrNonProfit[3] ? arrNonProfit[1] * 40/arrNonProfit[3] + 1 : (arrNonProfit[2] ? arrNonProfit[1] * 40/arrNonProfit[2] + 1 : 40))))) : undefined,
            count: 10
          },
          {
            ser: arrNonProfit[2],
            line: arrNonProfit[2] ? (arrNonProfit[6] ? arrNonProfit[2] * 40/arrNonProfit[6] + 2: (arrNonProfit[5] ? arrNonProfit[2] * 40/arrNonProfit[5] + 2 : (arrNonProfit[4] ? arrNonProfit[2] * 40/arrNonProfit[4] + 2 : (arrNonProfit[3] ? arrNonProfit[2] * 40/arrNonProfit[3] + 2 : 40)))) : undefined,
            count: 25
          },
          {
            ser: arrNonProfit[3],
            line: arrNonProfit[3] ? (arrNonProfit[6] ? arrNonProfit[3] * 40/arrNonProfit[6] + 3 : (arrNonProfit[5] ? arrNonProfit[3] * 40/arrNonProfit[5] + 3 : (arrNonProfit[4] ? arrNonProfit[3] * 40/arrNonProfit[4] : 40))) : undefined,
            count: 50
          },
          {
            ser: arrNonProfit[4],
            line: arrNonProfit[4] ? (arrNonProfit[6] ? arrNonProfit[4] * 40/arrNonProfit[6] : (arrNonProfit[5] ? arrNonProfit[4] * 40/arrNonProfit[5] : 40)) : undefined,
            count: 100
          },
          {
            ser: arrNonProfit[5],
            line: arrNonProfit[5] ? (arrNonProfit[6] ? arrNonProfit[5] * 40/arrNonProfit[6] : 40) : undefined,
            count: 250
          },
          {
            ser: arrNonProfit[6],
            line: arrNonProfit[6] ? 40 : undefined,
            count: 500
          },
        ]


        const profitLineBlocks = document.querySelector('.non-profit-series');

        const profitLineFunc = (dataBlocks) => {
          if(profitLineBlocks){
            profitLineBlocks.innerHTML = '';

            dataBlocks.forEach((item, index, arr) => {
              if (item.ser){
                let elem = '<div class="block"><div class="fail-row">Частота</div><div class="count count-ser">' 
                  + item.ser 
                  + '</div><div class="block-lines"></div><div class="count">' 
                  + item.count 
                  + '</div><div class="frequency">Проигрыши подряд</div></div>';
                  profitLineBlocks.insertAdjacentHTML('beforeend', elem);
                }
            });
           
            profitLineBlocks.querySelectorAll('.block-lines').forEach((item, index, arr) => {
              
              for (let i=0; i< dataBlocks[index].line; i++){
                
                let elemLine = '<div class="block-lines-item"></div>';
                item.insertAdjacentHTML('beforeend', elemLine);
              }
              
            })
          }
        };

        profitLineFunc(dataNonProfit);


        /*arrNonProfit.forEach((elem, i, arr) => {
          let elemBlock = nonProfitBlocks[i].querySelector(".count-ser");
          elemBlock.textContent = elem;
        });*/

        nonProfitSeriesSection.addEventListener("mouseenter", (e) => {
          nonProfitBlocks.forEach((item) => {
            item.classList.add("activeOpacity");
            item.addEventListener("mouseenter", () => {
              item.classList.add("active");
            });
            item.addEventListener("mouseleave", () => {
              item.classList.remove("active");
            });
          });
        });
        nonProfitSeriesSection.addEventListener("mouseleave", (e) => {
          nonProfitBlocks.forEach((del) => del.classList.remove("active"));
          nonProfitBlocks.forEach((del) =>
            del.classList.remove("activeOpacity")
          );
        });

        //Блок Вероятность отыграть бонус
        const plusValue = data.commonSimInformation.profitProbability;
        const zeroValue = data.commonSimInformation.nonProfitProbability;

        const myChartRadius = new ChartRadius({
          canvas: myCanvas,
          data: {
            plus: plusValue,
            empty1: 2,
            zero: zeroValue,
            empty2: 2,
          },
          colors: ["#FFEEC2", "#181818", "#FFB800", "#181818"],
          hole: 0.6,
        });

        myChartRadius.draw();

        const aarDepositMultipliers = [];
        data.depositMultipliers.forEach(item => {
          aarDepositMultipliers.push(item.depositMultiplierFrequency);
        })

        const dataBlocksValue = [
          {
            sim: aarDepositMultipliers[0],
            dep: aarDepositMultipliers[0] ? (aarDepositMultipliers[6] ? aarDepositMultipliers[0]*100/aarDepositMultipliers[6] : (aarDepositMultipliers[5] ? aarDepositMultipliers[0]*100/aarDepositMultipliers[5] : (aarDepositMultipliers[4] ? aarDepositMultipliers[0]*100/aarDepositMultipliers[4] : (aarDepositMultipliers[3] ? aarDepositMultipliers[0]*100/aarDepositMultipliers[3] : (aarDepositMultipliers[2] ? aarDepositMultipliers[0]*100/aarDepositMultipliers[2] : (aarDepositMultipliers[1] ? aarDepositMultipliers[0]*100/aarDepositMultipliers[1] : 120)))))) : undefined,
            depLine: 1,
            linePos: 77,
          },
          {
            sim: aarDepositMultipliers[1],
            dep: aarDepositMultipliers[1] ? (aarDepositMultipliers[6] ? aarDepositMultipliers[1]*100/aarDepositMultipliers[6] +5 : (aarDepositMultipliers[5] ? aarDepositMultipliers[1]*100/aarDepositMultipliers[5] : (aarDepositMultipliers[4] ? aarDepositMultipliers[1]*100/aarDepositMultipliers[4] : (aarDepositMultipliers[3] ? aarDepositMultipliers[1]*100/aarDepositMultipliers[3] : (aarDepositMultipliers[2] ? aarDepositMultipliers[1]*100/aarDepositMultipliers[2] : 120))))) : undefined,
            depLine: 2,
            linePos: 50,
          },
          {
            sim: aarDepositMultipliers[2],
            dep: aarDepositMultipliers[2] ? (aarDepositMultipliers[6] ? aarDepositMultipliers[2]*100/aarDepositMultipliers[6] + 10: (aarDepositMultipliers[5] ? aarDepositMultipliers[2]*100/aarDepositMultipliers[5] : (aarDepositMultipliers[4] ? aarDepositMultipliers[2]*100/aarDepositMultipliers[4] : (aarDepositMultipliers[3] ? aarDepositMultipliers[2]*100/aarDepositMultipliers[3] : 120)))) : undefined,
            depLine: 5,
            linePos: 55,
          },
          {
            sim: aarDepositMultipliers[3],
            dep: aarDepositMultipliers[3] ? (aarDepositMultipliers[6] ? aarDepositMultipliers[3]*100/aarDepositMultipliers[6] +15 : (aarDepositMultipliers[5] ? aarDepositMultipliers[3]*100/aarDepositMultipliers[5] + 10 : (aarDepositMultipliers[4] ? aarDepositMultipliers[3]*100/aarDepositMultipliers[4] + 10 : 120))) : undefined,
            depLine: 10,
            linePos: 55,
          },
          {
            sim: aarDepositMultipliers[4],
            dep: aarDepositMultipliers[4] ? (aarDepositMultipliers[6] ? aarDepositMultipliers[4]*100/aarDepositMultipliers[6] + 20 : (aarDepositMultipliers[5] ? aarDepositMultipliers[4]*100/aarDepositMultipliers[5] + 15 : 120)) : undefined,
            depLine: 20,
            linePos: 60,
          },
          {
            sim: aarDepositMultipliers[5],
            dep: aarDepositMultipliers[5] ? (aarDepositMultipliers[6] ? aarDepositMultipliers[5]*100/aarDepositMultipliers[6] + 25 :  120) : undefined,
            depLine: 50,
            linePos: 65,
          },
          {
            sim: aarDepositMultipliers[6],
            dep: aarDepositMultipliers[6] ? 120 : undefined,
            depLine: 100,
            linePos: 80,
          },
        ];
        charLineFunc(dataBlocksValue);
      }

      //canvas для блока "Вероятность отыграть бонус"
      const myCanvas = document.getElementById("chartRadius");
      myCanvas.width = 220;
      myCanvas.height = 220;

      function drawPieSlice(
        ctx,
        centerX,
        centerY,
        radius,
        startAngle,
        endAngle,
        color
      ) {
        ctx.fillStyle = color;
        ctx.beginPath();
        ctx.moveTo(centerX, centerY);
        ctx.arc(centerX, centerY, radius, startAngle, endAngle);
        ctx.closePath();
        ctx.fill();
      }

      const ChartRadius = function (options) {
        this.options = options;
        this.canvas = options.canvas;
        this.ctx = this.canvas.getContext("2d");
        this.colors = options.colors;

        this.draw = function () {
          let total_value = 0;
          let color_index = 0;
          for (let value in this.options.data) {
            let val = this.options.data[value];
            total_value += val;
          }

          let start_angle = 55;
          for (value in this.options.data) {
            let val = this.options.data[value];
            let slice_angle = (2 * Math.PI * val) / total_value;

            drawPieSlice(
              this.ctx,
              this.canvas.width / 2,
              this.canvas.height / 2,
              Math.min(this.canvas.width / 2, this.canvas.height / 2),
              start_angle,
              start_angle + slice_angle,
              this.colors[color_index % this.colors.length]
            );

            start_angle += slice_angle;
            color_index++;
          }

          if (this.options.hole) {
            drawPieSlice(
              this.ctx,
              this.canvas.width / 2,
              this.canvas.height / 2,
              this.options.hole *
                Math.min(this.canvas.width / 2, this.canvas.height / 2),
              0,
              2 * Math.PI,
              "#181818"
            );
          }
        };
      };

      //Блок "Вероятность увеличить депозит"
      const chartLineBlocks = document.querySelector(
        ".chart-line-blocks .block-wrap"
      );

      const charLineFunc = (dataBlocks) => {
        if (chartLineBlocks) {
          chartLineBlocks.innerHTML = "";

          let countLength = 0;

          dataBlocks.forEach((item, index, arr) => {
            countLength = arr.length - index;

            if (item.sim != undefined){
            const element =
              '<div class="block ' +
              (arr.length - 1 === index ? "lst" : "") +
              '" style="z-index:' +
              countLength +
              ';"><div class="top"><span>' +
              (item.sim == 0 || item.sim == "166 666" ? "-" : item.sim) +
              '</span><p>симуляций</p></div><div class="bottom">x' +
              item.depLine +
              'dep</div><div class="linear"></div></div>';
            chartLineBlocks.insertAdjacentHTML("beforeend", element);
              }
          });

          chartLineBlocks
            .querySelectorAll(".block")
            .forEach((item, index, arr) => {
              countLength = arr.length - index;
              const elementPos =
                '<div class="linePos" style="left:' +
                (item.offsetLeft + item.offsetWidth / 2 - 3) +
                "px; bottom: calc((20% + " +
                dataBlocks[index].dep +
                'px) - 6px);"></div>';
              chartLineBlocks.insertAdjacentHTML("beforeend", elementPos);
            });

          const linePosItems = document.querySelectorAll(".linePos");
          const topItems = document.querySelectorAll(".top span");
          const bottomItems = document.querySelectorAll(".bottom");

          chartLineBlocks.querySelectorAll(".block").forEach((el, index) => {
            el.addEventListener("mouseenter", () => {
              linePosItems[index].style.transform = "scale(2)";
              linePosItems[index].style.background = "#FFB800";
              topItems[index].style.color = "#FFB800";
              bottomItems[index].style.color = "#FFB800";
            });
            el.addEventListener("mouseleave", () => {
              linePosItems[index].style.transform = "scale(1)";
              linePosItems[index].style.background = "#fff";
              topItems[index].style.color = "rgba(255, 255, 255, 0.6)";
              bottomItems[index].style.color = "rgba(255, 255, 255, 0.6)";
            });
          });

          linePosItems.forEach((item, index, arr) => {
            let dot1 = linePosItems[index];
            let dot2 = linePosItems[index + 1] ? linePosItems[index + 1] : null;

            if (dot1 && dot2) {
              let choords1 = dot1.getBoundingClientRect();
              let choords2 = dot2.getBoundingClientRect();

              // get choords
              let x1 = choords1.left;
              let y1 = choords1.top;
              let x2 = choords2.left;
              let y2 = choords2.top;

              // line width
              let line = document.querySelectorAll(".linear")[index];
              const calcPos = dataBlocks[index].dep;

              line.style.bottom = "calc(20% + " + calcPos + "px)";
              let hypotenuse = getChoordsWidth(x1, y1, x2, y2);

              let leg = x2 - x1;
              let angle = Math.acos(leg / hypotenuse);

              line.style.width = hypotenuse + "px";
              line.style.transform =
                dot1.offsetTop > dot2.offsetTop
                  ? "rotate(-" + angle + "rad)"
                  : "rotate(" + angle + "rad)";

              function getChoordsWidth(x1, y1, x2, y2) {
                let a1 = Math.pow(x2 - x1, 2);
                let a2 = Math.pow(y2 - y1, 2);
                let res = a1 + a2;

                return Math.sqrt(res);
              }
            }
          });
        }
      };

      //Блок с подсказкой
      const promptsLink = "/SlotApplication/get-casino-prompts";
      fetch(promptsLink)
        .then((response) => response.json())
        .then((data) => {
          saveData(data);
        })
        .catch((error) => console.error("Ошибка получения данных" + error));

      let dataPrompts = {};
      function saveData(_data) {
        return (dataPrompts = _data);
        console.log(dataPrompts);
      }

      const iInfo = document.querySelectorAll(".i-info"),
        pointer = document.querySelector(".pointer");

      if (iInfo) {
        window.addEventListener("scroll", (e) => {
          const findPointer = document.querySelectorAll("[data-pointer]");

          findPointer.forEach((item) => {
            item.classList.remove("active");
            item.classList.remove("activeLeft");
            item.classList.remove("activeBottom");
          });

          iInfo.forEach((del) => del.classList.remove("active-visible"));
        });
        iInfo.forEach((item) => {
          item.addEventListener("click", () => {
            const rect = item.getBoundingClientRect();

            const findPointer = document.querySelector(
              '[data-pointer="' + item.dataset.type + '"]'
            );
            const findPointerAll = document.querySelectorAll("[data-pointer]");

            let dataContent = item.dataset.content;
            drawPrompts(dataPrompts);

            function drawPrompts(data) {
              for (key in data) {
                if (key == dataContent) {
                  let pointerContent = pointer.querySelector(".item-text");
                  pointerContent.innerHTML = "<span>" + data[key] + "</span>";
                  pointer.innerHTML =
                    '<div class="logos-row"><div class="item-text"><span>' +
                    data[key] +
                    "</span></div></div>";
                }
              }
            }

            if (findPointer) {
              iInfo.forEach((del) => del.classList.remove("active-visible"));
              item.classList.add("active-visible");

              if (document.body.offsetWidth > 1080) {
                findPointerAll.forEach((item) => {
                  item.classList.remove("active");
                  item.classList.remove("activeLeft");
                });
                if (
                  rect.left + findPointer.offsetWidth + rect.width >
                  document.body.offsetWidth
                ) {
                  findPointer.style.left =
                    rect.right -
                    findPointer.offsetWidth -
                    rect.width -
                    20 +
                    "px";
                  findPointer.classList.add("activeLeft");
                  findPointer.style.top =
                    rect.top -
                    findPointer.offsetHeight / 2 +
                    rect.height / 2 +
                    "px";
                } else {
                  findPointer.style.left = rect.left + rect.width + 20 + "px";
                  findPointer.classList.add("active");

                  findPointer.style.top =
                    rect.top -
                    findPointer.offsetHeight / 2 +
                    rect.height / 2 +
                    "px";
                }
              } else {
                findPointerAll.forEach((item) => {
                  item.classList.remove("activeBottom");
                });
                findPointer.classList.add("activeBottom");
                findPointer.style.top = rect.top + rect.height + 20 + "px";
              }
            }
          });
        });
        document.body.addEventListener("click", (e) => {
          if (!e.target.closest(".i-info")) {
            const findPointer = document.querySelectorAll("[data-pointer]");

            findPointer.forEach((item) => {
              item.classList.remove("active");
              item.classList.remove("activeLeft");
              item.classList.remove("activeBottom");
            });

            iInfo.forEach((del) => del.classList.remove("active-visible"));
          }
        });
      }

      // открытие меню навигации
      const btnMenu = document.querySelector(".btn-menu");
      btnMenu.addEventListener("click", toggleMenu);

      function toggleMenu() {
        var x = document.getElementById("myTopnav");
        if (x.className === "topnav") {
          x.className += " responsive";
        } else {
          x.className = "topnav";
        }
      }
      
    </script>
  </body>
</html>
