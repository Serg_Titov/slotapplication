<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en" pageEncoding="UTF-8">
  <head>
    <title>Casino Brands</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
      href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800;900&display=swap"
      rel="stylesheet"
    />
  </head>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
  <style>
    html {
      height: 100%;
    }
    * {
      box-sizing: border-box;
    }
    body {
      margin: 0;
      font-family: "Montserrat", sans-serif;
      font-size: 14px;
    }

    /*общий grid*/
    .grid {
      display: grid;
      grid-template-columns: 1fr .5fr 2fr .5fr 1fr;
      grid-template-rows: auto 1fr auto;
      justify-content: center;
      align-content: flex-start;
      justify-items: center;
      box-sizing: border-box;
      min-height: 100%;
      min-width: 320px;
    }

    /*общий flex*/
    .flex {
      display: flex;
      justify-content: center;
      flex-flow: row wrap;
    }
    /*flex-контейнер*/
    .flex-row {
      display: flex;
      flex-flow: row wrap;
      max-width: 1200px;
      margin: 0 auto;
      justify-content: center;
    }
    /*flex-контейнер*/
    .flex-column {
      display: flex;
      max-width: 1200px; /*ширина контента*/
      flex-flow: column nowrap;
      margin: 0 auto;
      align-items: center;
    }

    header {
      grid-column: 3/4;
      grid-row: 1/2;
      width: 100%;
      /*height: 100vh;*/
      color: #fff;
    }

    nav {
      width: 100%;
      box-sizing: border-box;
      display: flex;
      flex-flow: row nowrap;
      height: 80px;
      justify-content: space-around;
      transition: background-color 0.5s;
    }

    ul.topnav {
      display: flex;
      list-style-type: none;
      width: 100%;
      justify-content: center;
      align-items: center;
      margin: 0;
      padding: 0;
    }

    ul.topnav li {
      text-align: center;
      background: #f5f5f5;
      border-radius: 2px 2px;
      height: auto;
      perspective: 900px;
    }
    /*style for rotate point of menu*/
    .roll-link {
      height: auto;
      display: block;
      width: 90px;
      transform-style: preserve-3d;
    }

    .roll-link .front {
      height: 40px;
      background: #f5f5f5;
      color: #695753;
    }

    .roll-link .back {
      opacity: 0;
      height: 40px;
      display: block;
      background: #b19891;
      color: #f5f5f5;
      transform: translate3d(0, 0, -40px) rotate3d(1, 0, 0, 90deg);
    }

    .roll-link > a {
      display: block;
      position: absolute;
      width: 90px;
      text-decoration: none;
      padding: 1em;
      transition: all 0.3s ease-out;
      transform-origin: 50% 0%;
    }

    .topnav .roll-link:hover .front {
      transform: translate3d(0, 40px, 0) rotate3d(1, 0, 0, -90deg);
      backface-visibility: hidden;
      perspective: 1000;
      opacity: 0;
    }

    .topnav .roll-link:hover .back {
      transform: rotate3d(1, 0, 0, 0deg);
      backface-visibility: hidden;
      perspective: 1000;
      opacity: 1;
    }

    nav .btn-menu {
      display: none;
    }

    aside {
      grid-column: 1/2;
      grid-row: 2/3;
      width: 100%;
      margin-top: 65px;
      display: flex;
      flex-direction: column;
      align-items: flex-end;
    }

    main {
      grid-column: 2/5;
      grid-row: 2/3;
      width: 100%;
      /*height: 100vh;*/
      /*color: #fff;*/
    }
    .pagination,
    .brands {
      align-items: flex-start;
    }
    .pagination{
      margin-top: 35px;
    }
    .alphabet {
      width: 100%;
      padding-left: 0;
      list-style-type: none;
    }
    .alphabet li {
      text-align: center;
      border-radius: 2px 2px;
    }
    .alphabet a {
      display: block;
      padding: 5px;
      list-style-type: none;
      color: #695753;
      text-decoration: none;
    }

    .alphabet li:hover {
      background-color: #695753;
      color: #f5f5f5;
    }
    .alphabet a:hover {
      color: #f5f5f5;
    }
    .item-wrap{
      width: 200px;
      display: flex;
      margin-top: 5px;
    }
    .brands-filter,
    .item-link{
      width: 90%;
      height: 30px;
      border: none;
      background: #f5f5f5;
      outline: none;
      padding: 5px;
      color: #695753;
      font-family: "Montserrat", sans-serif;
    }
    .brands-filter:hover,
    .item-link:hover{
      color: #f5f5f5;
      background: #b19891;
    }
    .brands-filter option:hover{
      background: #f5f5f5;
      color: #695753;
    }
    .brands-filter{
      display: none;
    }
    .brands>div{
      width: 100%;
      position: relative;
      padding: 10px 0 10px 0;
    }
    .item-link {
      text-decoration: none;
      padding-left: 10px;
    }
    #new-sites{
      display: none;
    }

    .sign{
      position: absolute;
      top: 17px;
      left: 5px;
      /*padding-left: 50px;*/
      /*margin: 5px 0px 5px 0px;*/
      color: #695753;
    }
    .sign~div{
      border-top: solid 2px #b1989130;
    }

    .brands-list{
      justify-content: flex-start;
      padding-left: 30px;
    }

    .brands-list li {
      list-style-type: none;
      width: 250px;
      margin: 5px;
      box-sizing: border-box;
    }

    .brands-list li a {
      color: #695753;
      text-decoration: none;
    }
    .brands-list li a:hover {
      background-color: #695753;
      color: #f5f5f5;
      padding: 3px;
      border-radius: 2px 2px;
    }
    .brands-list li.hide{
      display: none;
    }
    .block{
      min-height: 49px;
    }

    /* Preloader */

.preloader {
  background-color: rgba(251, 251, 251, .1);
  color: #fff;
  display: none;
  left: 0;
  position: fixed;
  top: 0;
  height: 100%;
  width: 100%;
}
.preloader.active {
  display: block;
}
#status {
  width: 200px;
  height: 200px;
  position: absolute;
  left: 50%;
  /* centers the loading animation horizontally one the screen */
  top: 50%;
  /* centers the loading animation vertically one the screen */
  background-image: url(https://raw.githubusercontent.com/niklausgerber/PreLoadMe/master/img/status.gif);
  /* path to your loading animation */
  background-repeat: no-repeat;
  background-position: center;
  margin: -100px 0 0 -100px;
  /* is width and height divided by two */
}


    @media (max-width: 500px) {
      .grid {
      grid-template-columns: .5fr 0.5fr 2fr 0.5fr .5fr;
      }
      header {
        grid-column: 2/5;
      }
      nav {
        flex-direction: row;
        height: auto;
        margin-top: 5px;
        justify-content: start;
      }

      nav .btn-menu {
        display: block;
        color: #695753;
        float: right;
        margin-right: 10px;
      }

      ul.topnav {
        display: none;
      }
      ul.topnav li {
        width: 90%;
        height: 40px;
      }

      .roll-link {
        width: 100%;
      }

      .topnav.responsive {
        display: block;
      }

      .alphabet {
        margin-top: 10px;
        width: 80%;
      }
    }
  </style>
  <body>
    <!-- Грид-контейенер общий -->
    <div class="grid">
      <!--Header-->
      <header>
        <!--блок навгации-->
        <nav class="navigation">
          <!-- Кнопка раскрытия меню -->
          <a href="javascript:void(0)" class="btn-menu" >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="24"
              height="24"
              viewBox="0 0 24 24"
            >
              <path
                d="M3.5,7 C3.22385763,7 3,6.77614237 3,6.5 C3,6.22385763 3.22385763,6 3.5,6 L20.5,6 C20.7761424,6 21,6.22385763 21,6.5 C21,6.77614237 20.7761424,7 20.5,7 L3.5,7 Z M3.5,12 C3.22385763,12 3,11.7761424 3,11.5 C3,11.2238576 3.22385763,11 3.5,11 L20.5,11 C20.7761424,11 21,11.2238576 21,11.5 C21,11.7761424 20.7761424,12 20.5,12 L3.5,12 Z M3.5,17 C3.22385763,17 3,16.7761424 3,16.5 C3,16.2238576 3.22385763,16 3.5,16 L20.5,16 C20.7761424,16 21,16.2238576 21,16.5 C21,16.7761424 20.7761424,17 20.5,17 L3.5,17 Z"
              />
            </svg>
          </a>
          <ul class="topnav" id="myTopnav">
            <li>
              <div class="roll-link">
                <a href="/SlotApplication/" class="front">Home</a>
                <a href="/SlotApplication/" class="back">Home</a>
              </div>
            </li>
            <li>
              <div class="roll-link">
                <a href="/SlotApplication/tools" class="front">Tools</a>
                <a href="/SlotApplication/tools" class="back">Tools</a>
              </div>
            </li>
            <li>
              <div class="roll-link">
                <a href="/SlotApplication/slots" class="front">Slots</a>
                <a href="/SlotApplication/slots" class="back">Slots</a>
              </div>
            </li>
            <li>
              <div class="roll-link">
                <a href="/SlotApplication/jackpots" class="front">Jackpots</a>
                <a href="/SlotApplication/jackpots" class="back">Jackpots</a>
              </div>
            </li>
            <li>
              <div class="roll-link">
                <a href="/SlotApplication/sites" class="front">Sites</a>
                <a href="/SlotApplication/sites" class="back">Sites</a>
              </div>
            </li>
          </ul>
        </nav>
        <!--/ блок навгации-->
      </header>
      <!--/Header-->

      <!--sidebar-->
      <aside>
        <div class="item-wrap">
          <select class="brands-filter">
            <option value="default">Select license</option>
          </select>
        </div>
        <div class="item-wrap">
          <a href="/SlotApplication/new-sites" id="new-sites" class="item-link" >New Sites</a>
        </div>
      </aside>
      <!--/sidebar-->

      <!--main-->
      <main>
        <div class="preloader">
          <div id="status">&nbsp;</div>
        </div>
        <!--блок пагинации-->
        <div class="pagination">
          <ul class="alphabet flex-row"></ul>
        </div>
        <!--/ блок пагинации-->

        <!--блок список брендов-->
        <div class="brands flex-column"></div>
        <!--/ блок список брендов-->
      </main>
      <!-- /main -->

      <!-- footer -->
      <footer></footer>
      <!-- /footer -->
    </div>

    <script>
      const alphabet = [
        "O-9",
        "A",
        "B",
        "C",
        "D",
        "E",
        "F",
        "G",
        "H",
        "I",
        "J",
        "K",
        "L",
        "M",
        "N",
        "O",
        "P",
        "Q",
        "R",
        "S",
        "T",
        "U",
        "V",
        "W",
        "X",
        "Y",
        "Z",
      ];
      let alphabetList = document.querySelector(".alphabet");
      let containerBrands = document.querySelector(".brands");
      
      const brandsLink = "/SlotApplication/get-all-brands";
      $('.preloader').addClass('active');


  /*const xhr = new XMLHttpRequest();

  xhr.open("GET", brandsLink);
  xhr.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
  xhr.responseType = "json";

  xhr.onload = function() {
    if (xhr.status !== 200) {
      console.error(`Ошибка ${xhr.status}: ${xhr.response}`);
    } else {
      drawBrands(xhr.response);
    }
  };

  xhr.onerror = function() { // происходит, только когда запрос совсем не получилось выполнить
    console.error(`Ошибка соединения`);
  };

  xhr.send();*/


      fetch(brandsLink)
        .then((response) => response.json())
        .then((data) => {
          drawBrands(data);
        })
        .catch((error) => console.error("Ошибка получения данных" + error));

      //let brandsList = document.querySelector(".brands-list");


      function drawBrands(data) {
        console.log(data);
        $('.preloader').removeClass('active');
          alphabet.forEach(function (item, i, arr) {
          alphabetList.innerHTML +=
            '<li><a href="#' + item + '">' + item + "</a></li>";
          containerBrands.innerHTML +=
            '<div class="block"><div id="' +
            item +
            '" class="sign">' +
            item +
            '</div><div><ul class="brands-list flex-row"></ul></div></div>';
          });
      

      let signes = document.querySelectorAll(".sign");
      let numberSign = document.querySelector("#O-9");
      let arrBrands = [];
      let arr = [];
      let arrDB = data.brands;
      let arrLicenses = data.licenses;
      let selectLicenses = document.querySelector('.brands-filter');
      selectLicenses.style.display = 'block';

    
        for (let i = 0; i < arrLicenses.length; i++){
            let option = document.createElement('option');
            option.value = arrLicenses[i];
            option.innerText = arrLicenses[i];
            selectLicenses.append(option);
  
          }
      

      
      for (let i = 0; i < signes.length; i++) {
        for (let k = 0; k < arrDB.length; k++) {
          if (signes[i].textContent == arrDB[k].brand.charAt(0)) {
            let nextElem = signes[i].nextSibling;
            let elem = nextElem.querySelector(".brands-list");
            elem.innerHTML += '<li><a href="/SlotApplication/brand?name=' + arrDB[k].brand+ '" data-license="' + arrDB[k].licenses.join(',') + '">' + arrDB[k].brand + "</a></li>";
            arr.push(arrDB[k].brand);
          }
        }
      }

      for (let i = 0; i < arrDB.length; i++) {
        if (!arr.includes(arrDB[i].brand)) {
          let nextElem = numberSign.nextSibling;
          let elem = nextElem.querySelector(".brands-list");
          elem.innerHTML += '<li><a href="/SlotApplication/brand?name=' + arrDB[i].brand + '" data-license="' + arrDB[i].licenses.join(',') + '">' + arrDB[i].brand + "</a></li>";
          arrBrands.push(arrDB[i].brand);
        }
      }


      let newSitesbtn = document.querySelector('#new-sites');
      newSitesbtn.style.display = 'block';  
      }



      //фильтр по лицензии
      let selectLicenses = document.querySelector('.brands-filter');
      selectLicenses.addEventListener('change', function() {
      let value = selectLicenses.value;
      console.log(value);
      let arrElem = document.querySelectorAll('[data-license]');
      let arrData = [];
      let notData = [];

      for (let i = 0; i < arrElem.length; i++){
        let elemData = arrElem[i].dataset.license.split(',');
        const chekValue = (elem) => elem == value;

        if (value == 'default'){
          arrElem[i].parentNode.classList.remove('hide');
        } else if (elemData.some(chekValue)){
          arrElem[i].parentNode.classList.remove('hide');
          arrData.push(arrElem[i]);
        } else {
          arrElem[i].parentNode.classList.add('hide');
          notData.push(arrElem[i]);
        }
      }

      console.log(arrData);
      console.log(notData);

      })

      /*function checkBlock(){
      let divBlock = document.querySelectorAll('.block');
      let arrLi = [];
      for(let i=0; i<divBlock.length; i++){
        let divLi = divBlock[i].querySelectorAll('li');
        for(let k=0; k<divLi.length; k++){
          if(divLi[k].style.display = 'block'){
            console.log(divLi[k]);
            //arrLi.push(divLi[k]);
          }
        }
        console.log(arrLi);
      }
    }*/


      // открытие меню навигации
      const btnMenu = document.querySelector('.btn-menu')
      btnMenu.addEventListener('click', toggleMenu)

      function toggleMenu() {
        var x = document.getElementById("myTopnav");
        if (x.className === "topnav") {
          x.className += " responsive";
        } else {
          x.className = "topnav";
        }
      }
    </script>
  </body>
</html>
