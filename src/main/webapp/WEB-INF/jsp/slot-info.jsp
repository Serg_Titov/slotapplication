<%@ page import="by.minsk.miroha.entities.front.SlotInputFront" %>
<%@ page import="by.minsk.miroha.report.SlotInputReport"%>
<%@ page import="by.minsk.miroha.entities.report.TopProfits"%>
<%@ page import="by.minsk.miroha.entities.report.DepositMultiplier"%>
<%@ page import="by.minsk.miroha.entities.report.NonProfitSeries"%>
<%@ page import="by.minsk.miroha.entities.SlotInputError" %>
<%@ page import="java.nio.charset.StandardCharsets" %>
<%@ page import="by.minsk.miroha.entities.PairProfit"%>
<%@ page import="by.minsk.miroha.entities.PairSequence"%>
<%@ page import="by.minsk.miroha.entities.enums.WaggerType"%>
<%@ page import="java.util.List"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html lang="en" pageEncoding="UTF-8">
<head>
    <meta charset="UTF-8">
    <title>Информация о слоте</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <style>
        #menu {
            margin-left: 30%;
            position: absolute;
            top: 2em;
        }
        .menu-li {
            display: inline;
            margin: -2.5;
        }
        .menu-ul {
            list-style: none;
            margin: 0;
            padding-left: 0;
        }
        .menu-a {
            text-decoration: none;
            padding: 1em;
            background: #f5f5f5;
            border: 1px solid #b19891;
            color: #695753;
        }
            #inputContainer {
              height: 250;
              display: flex;
              flex-direction: column;
              justify-content: space-between;
            }

            #advanceSettings{
                display: none;
            }

            .prompt{
                display: none;
            }

            .prompt-label:hover + .prompt {
                display: block;
            }
            .hidden{
                display: none;
            }
            .text-input{
                text-align: right
            }
    </style>
    <script>
        function getSlotInputValues(){
            let depositC = document.getElementById("deposit");
            let bonusC = document.getElementById("bonus");
            let withoutBonusC = document.getElementById("withoutBonus");
            let taxesC = document.getElementById("taxes");
            let providerFeeC = document.getElementById("providerFee");
            let paymentFeeC = document.getElementById("paymentFee");
            let platformFeeC = document.getElementById("platformFee");
            let betC = document.getElementById("bet");
            let maxProfitC = document.getElementById("maxProfit");
            let waggerTypeC = document.getElementById("waggerType");
            let waggerC = document.getElementById("wagger");
            let bonusPartC = document.getElementById("bonusPart");
            let simC = document.getElementById("sim");
            let manualC = document.getElementById("manual");
            let accuracyC = document.getElementById("accuracy");
            let slotInputFrontO = {
                deposit: depositC.value,
                bonus: bonusC.value,
                withoutBonus: withoutBonusC.checked,
                taxes: taxesC.value,
                providerFee: providerFeeC.value,
                paymentFee: paymentFeeC.value,
                platformFee: platformFeeC.value,
                bet: betC.value,
                profitLimit: maxProfitC.value,
                waggerType: waggerTypeC.value,
                wagger: waggerC.value,
                bonusParts: bonusPartC.value,
                sim: simC.value,
                manual: manualC.checked,
                accuracy: accuracyC.value,
                performType: document.location.search
            };
            window.slotInputFront = slotInputFrontO;
        }
    </script>
    <script>
        function getReport(){
            hasReport=0;

            getSlotInputValues();
            let expectedTimeFromServer;
            $.ajax({
                type: 'POST',
                url: '/SlotApplication/expectedTime',
                data: window.slotInputFront,
                async: false,
                success: function(data){
                    expectedTimeFromServer = data;
                    if (data !== 0 && hasReport === 0){
                        let lowBorder = Math.floor(data/60);
                        if (document.getElementById("expectedTimeLabel") === null){
                            let expectedTimeLabel = document.createElement("label");
                            expectedTimeLabel.setAttribute("id",  "expectedTimeLabel");
                            if (data < 60) {
                                expectedTimeLabel.innerText = "Ожидаемое время работы программы составляет менее 1 минуты";
                            } else {
                                expectedTimeLabel.innerText = "Ожидаемое время работы программы составляет " + Math.floor(data/60) + "-" + (Math.floor(data/60)+1) + " минут";
                            }
                            document.getElementById("expectedTime").appendChild(expectedTimeLabel);
                        } else {
                            if (data < 60) {
                                document.getElementById("expectedTimeLabel").innerText = "Ожидаемое время работы программы составляет менее 1 минуты";
                            } else {
                                document.getElementById("expectedTimeLabel").innerText = "Ожидаемое время работы программы составляет " + Math.floor(data/60) + "-" + (Math.floor(data/60)+1) + " минут";
                            }
                        }
                    }
                }
            });

            let performConfirm;
            if (expectedTimeFromServer > 600){
                performConfirm = confirm("Время ожидания расчета займет более чем " +
                    (Math.floor(expectedTimeFromServer/60)) + " минут. Уверены, что желаете продолжить ?");
            } else {
                performConfirm = true;
            }

            if (performConfirm){
                if (version === 'admin'){
                    document.getElementById("reportPlace").style.display = 'block';
                    document.getElementById("exactEVReport").style.display = 'none';
                }
                let stopwatchDiv = document.getElementById("stopwatch");
                stopwatchDiv.style.visibility = 'visible';
                StartStop();
                $.ajax({
                    type: 'POST',
                    url: '/SlotApplication/slot-info',
                    data: window.slotInputFront,
                    success: function(data){
                        let emptyString = document.createElement("br");
                        let reportContainer = document.getElementById("report");
                        let reportPlace = document.getElementById("reportPlace");

                        let waitingToDelete = document.getElementById("waitingLabel");
                        if (waitingToDelete !== null){
                            waitingToDelete.remove();
                        }

                        if (data.error !== null){
                            let errorMessageDiv = document.getElementById("error-message-div");
                            let errorLabel = document.createElement("label");
                            errorLabel.setAttribute("id", "error-message");
                            errorLabel.innerText = data.error;
                            errorMessageDiv.appendChild(errorLabel);
                        } else {
                            let slotLabel = document.getElementById("slotLabel");
                            slotLabel.innerText = "Слот: " + data.slotInformation.slot;

                            let providerLabel = document.getElementById("providerLabel");
                            providerLabel.innerText = "Провайдер: " + data.slotInformation.provider;

                            if (version !== "casino"){
                                let profitLabel = document.getElementById("profitLabel");
                                profitLabel.innerText = "PROFIT: " + data.commonSimInformation.profit;
                            }

                            let evLabel = document.getElementById("evLabel");
                            evLabel.innerText = "EV: " + data.commonSimInformation.ev;

                            let roiLabel = document.getElementById("roiLabel");
                            roiLabel.innerText = "ROI: " + data.commonSimInformation.roi;

                            let bonusDispersionLabel = document.getElementById("bonusVolatilityLabel");
                            bonusDispersionLabel.innerText = "Дисперсия бонуса: " + data.commonSimInformation.bonusVolatility;

                            let positiveProfitLabel = document.getElementById("positiveProfitLabel");
                            positiveProfitLabel.innerText = "Вероятность получить Profit больше 0 : " + data.commonSimInformation.profitProbability;

                            let nullProfitLabel = document.getElementById("nullProfitLabel");
                            nullProfitLabel.innerText = "Вероятность получить Profit  0 : " + data.commonSimInformation.nonProfitProbability;

                            let averageWinLabel = document.getElementById("averageWin");
                            if (data.commonSimInformation.averageWin === 0){
                                averageWinLabel.innerText = "";
                            } else {
                                averageWinLabel.innerText = "Средний бонусный выигрыш : " + data.commonSimInformation.averageWin;
                            }

                            let betCounterLabel = document.getElementById("betCounter");
                            if (data.commonSimInformation.betCounter === 0){
                                betCounterLabel.innerText = "";
                            } else {
                                betCounterLabel.innerText = "Количество спинов : " + data.commonSimInformation.betCounter;
                            }

                            let topProfitsLabel = document.getElementById("topProfitsLabel");
                            topProfitsLabel.innerText = "Самые крупные  Profit и их вероятности:";

                            let topProfitsDiv = document.getElementById("topProfitsDiv");
                            while (topProfitsDiv.hasChildNodes()) {
                                topProfitsDiv.removeChild(topProfitsDiv.firstChild);
                            }
                            for(let topProfits of data.topProfits){
                                let topProfitsLabel = document.createElement("label");
                                topProfitsLabel.innerText = "TOP-" + topProfits.name + ": сумма = " +
                                    topProfits.profit + "; частота: раз в " + topProfits.frequency + " симуляций";
                                topProfitsDiv.appendChild(topProfitsLabel);
                                let newEmptyString = document.createElement("br");
                                topProfitsDiv.appendChild(newEmptyString);
                            }

                            let depositMultiplierLabel = document.getElementById("depositMultiplierLabel");
                            depositMultiplierLabel.innerText = "Отношение выигрыша к депозиту:";

                            let depositMultiplierDiv = document.getElementById("depositMultiplierDiv");
                            while (depositMultiplierDiv.hasChildNodes()) {
                                depositMultiplierDiv.removeChild(depositMultiplierDiv.firstChild);
                            }
                            for(let depositMultiplier of data.depositMultipliers){
                                let depositMultiplierLabel = document.createElement("label");
                                depositMultiplierLabel.innerText = "Коэффициент выигрыша " + depositMultiplier.depositMultiplier +
                                    ": раз в " + depositMultiplier.depositMultiplierFrequency + " симуляций.";
                                depositMultiplierDiv.appendChild(depositMultiplierLabel);
                                let newEmptyString = document.createElement("br");
                                depositMultiplierDiv.appendChild(newEmptyString);
                            }

                            let seriesDiv = document.getElementById("series");
                            while (seriesDiv.hasChildNodes()) {
                                seriesDiv.removeChild(seriesDiv.firstChild);
                            }
                            for(let nonProfitSeries of data.nonProfitSeries){
                                let nonProfitSeriesLabel = document.createElement("label");
                                nonProfitSeriesLabel.innerText = "Серии из  Profit = " + nonProfitSeries.seriesName +
                                    " раз подряд: раз в " + nonProfitSeries.series + " симуляций.";
                                seriesDiv.appendChild(nonProfitSeriesLabel);
                                let newEmptyString = document.createElement("br");
                                seriesDiv.appendChild(newEmptyString);
                            }

                            let longestNonProfitLabel = document.getElementById("longestNonProfitLabel");
                            longestNonProfitLabel.innerText = "Самая долгая безвыигрышная серия: " + data.commonSimInformation.longestNonProfitSeries;

                            let longestNonProfitFrequencyLabel = document.getElementById("longestNonProfitFrequencyLabel");
                            longestNonProfitFrequencyLabel.innerText = "Частота самой долгой безвыигрышной серии: " + data.commonSimInformation.longestNonProfitFrequency;

                            let longestProfitLabel = document.getElementById("longestProfitLabel");
                            longestProfitLabel.innerText = "Самая долгая выигрышная серия: " + data.commonSimInformation.longestProfitSeries;

                            let longestProfitFrequencyLabel = document.getElementById("longestProfitFrequencyLabel");
                            longestProfitFrequencyLabel.innerText = "Частота самой долгой выигрышной серии: " + data.commonSimInformation.longestProfitFrequency;

                        }
                        StartStop();
                        ClearСlock();
                        stopwatchDiv.style.visibility = 'hidden';
                        document.getElementById("expectedTime").innerText="";
                        hasReport=1;
                    }
                });

                let errorMessageLabel = document.getElementById("error-message");
                if (errorMessageLabel !== null){
                    errorMessageLabel.remove();
                }

                let waitingLabel = document.createElement("label");
                waitingLabel.setAttribute("id",  "waitingLabel");
                waitingLabel.innerText = "Происходит расчет. Ожидайте..."
                let successDiv = document.getElementById("message");
                successDiv.appendChild(waitingLabel);
            }
        }
    </script>
    <script>
        function hideMaxProfit(){
            let maxProfitDiv = document.getElementById("maxProfitDiv");
            let hideButton = document.getElementById("hideButton");
            let maxProfit = document.getElementById("maxProfit");
            if (hideButton.innerText === "Добавить ограничение максимального выигрыша"){
                hideButton.innerText = "Убрать ограничение максимального выигрыша";
                maxProfitDiv.style.visibility = 'visible';
            } else {
                if (maxProfit.value !== null){
                    maxProfit.value = 0
                }
                hideButton.innerText = "Добавить ограничение максимального выигрыша";
                maxProfitDiv.style.visibility = 'hidden';
            }
        }
        </script>
    <script>
        function hideBonusPart(){
            let bonusPartDiv = document.getElementById("bonusPartDiv");
            let hideButton = document.getElementById("hideBonusButton");
            let bonusPart = document.getElementById("bonusPart");
            if (hideButton.innerText === "Бонус отыгрывается частями"){
                hideButton.innerText = "Не отыгрывать бонус частями";
                bonusPartDiv.style.visibility = 'visible';
            } else {
                if (bonusPart.value !== null){
                    bonusPart.value = 0
                }
                hideButton.innerText = "Бонус отыгрывается частями";
                bonusPartDiv.style.visibility = 'hidden';
            }
        }
    </script>
    <script>
        function getPrompts(){
            $.ajax({
                type: 'GET',
                url: '/SlotApplication/get-prompts',
                success: function(data){
                    let depositPromptLabel = document.getElementById("depositPrompt");
                    depositPromptLabel.innerText = data.depositPrompt;

                    let bonusPromptLabel = document.getElementById("bonusPrompt");
                    bonusPromptLabel.innerText = data.bonusPrompt;

                    let providerFeePromptLabel = document.getElementById("providerFeePrompt");
                    providerFeePromptLabel.innerText = data.providerFeePrompt;

                    let paymentFeePromptLabel = document.getElementById("paymentFeePrompt");
                    paymentFeePromptLabel.innerText = data.paymentFeePrompt;

                    let platformFeePromptLabel = document.getElementById("platformFeePrompt");
                    platformFeePromptLabel.innerText = data.platformFeePrompt;

                    let betPromptLabel = document.getElementById("betPrompt");
                    betPromptLabel.innerText = data.betPrompt;

                    let waggerTypePromptLabel = document.getElementById("waggerTypePrompt");
                    waggerTypePromptLabel.innerText = data.waggerTypePrompt;

                    let waggerPromptLabel = document.getElementById("waggerPrompt");
                    waggerPromptLabel.innerText = data.waggerPrompt;

                    let simulationsPromptLabel = document.getElementById("simulationsPrompt");
                    simulationsPromptLabel.innerText = data.simulationsPrompt;

                    let withoutBonusPromptLabel = document.getElementById("withoutBonusPrompt");
                    withoutBonusPromptLabel.innerText = data.withoutBonusPrompt;

                    let manualPromptLabel = document.getElementById("manualPrompt");
                    manualPromptLabel.innerText = data.manualPrompt;

                    let taxesPromptLabel = document.getElementById("taxesPrompt");
                    taxesPromptLabel.innerText = data.taxesPrompt;

                    let maxProfitPromptLabel = document.getElementById("maxProfitPrompt");
                    maxProfitPromptLabel.innerText = data.maxProfitPrompt;

                    let bonusPartPromptLabel = document.getElementById("bonusPartPrompt");
                    bonusPartPromptLabel.innerText = data.bonusPartPrompt;

                    let accuracyPromptLabel = document.getElementById("accuracyPrompt");
                    accuracyPromptLabel.innerText = data.accuracyPrompt;
                }
            });
        }
    </script>
        <script>
            function enabledPerform(){

                $.ajax({
                    type: 'GET',
                    url: '/SlotApplication/perform-enabled',
                    success: function(data){
                        enabledPerformHandling(data, "perform");
                    },
                    error: function(error){
                        enabledPerformError("perform");
                        enabledPerformError("performDemo");
                    }
                });
            };
        </script>
        <script>
            function enabledPerformHandling(data, label){
                if(data){
                    let performButton = document.getElementById(label);
                    performButton.disabled  = false;
                } else {
                    enabledPerformError(label);
                }
            }
        </script>
        <script>
            function enabledPerformError(label){
                let performButton = document.getElementById(label);
                performButton.disabled = true;
                let performDiv = document.getElementById("performDisabled");
                if (performDiv.children.length === 0){
                    let disabledPerform = document.createElement("label");
                    disabledPerform.innerText = "Сперва необходимо загрузить базу из файла";
                    performDiv.appendChild(disabledPerform);
                    let redirectionMenu = document.getElementById("redirection");
                    redirectionMenu.style.visibility = "visible";
                }
            }
        </script>
        <script>
            var base = 60;
            var clocktimer,dateObj,dh,dm,ds,ms;
            var readout='';
            var h=1,m=1,tm=1,s=0,ts=0,ms=0,init=0;

            //функция для очистки поля
            function ClearСlock() {
                clearTimeout(clocktimer);
                h=1;m=1;tm=1;s=0;ts=0;ms=0;
                init=0;
                readout='00:00:00';
                document.MyForm.stopwatch.value=readout;
            }

            //функция для старта секундомера
            function StartTIME() {
                var cdateObj = new Date();
                var t = (cdateObj.getTime() - dateObj.getTime())-(s*1000);
                if (t>999) { s++; }
                if (s>=(m*base)) {
                    ts=0;
                    m++;
                } else {
                    ts=parseInt((ms/100)+s);
                    if(ts>=base) { ts=ts-((m-1)*base); }
                }
                if (m>(h*base)) {
                    tm=1;
                    h++;
                } else {
                    tm=parseInt((ms/100)+m);
                    if(tm>=base) { tm=tm-((h-1)*base); }
                }
                    ms = Math.round(t/10);
                    if (ms>99) {ms=0;}
                    if (ms==0) {ms='00';}
                    if (ms>0&&ms<=9) { ms = '0'+ms; }
                    if (ts>0) { ds = ts; if (ts<10) { ds = '0'+ts; }} else { ds = '00'; }
                    dm=tm-1;
                    if (dm>0) { if (dm<10) { dm = '0'+dm; }} else { dm = '00'; }
                    dh=h-1;
                    if (dh>0) { if (dh<10) { dh = '0'+dh; }} else { dh = '00'; }
                    readout = dh + ':' + dm + ':' + ds ;
                    document.MyForm.stopwatch.value = readout;
                    clocktimer = setTimeout("StartTIME()",1000);
            }

            //Функция запуска и остановки
            function StartStop() {
                if (init==0){
                    ClearСlock();
                    dateObj = new Date();
                    StartTIME();
                    init=1;
                } else {
                    clearTimeout(clocktimer);
                    init=0;
                }
            }
        </script>
        <script>
            function hideTaxes(){
                window.hasReport=0;
                $.ajax({
                    type: 'GET',
                    url: '/SlotApplication/version',
                    data: {path: document.location.search},
                    success: function(data){
                        window.version =  data;
                        if(data === "casino"){
                            document.getElementById("taxesDiv").style.visibility = "hidden";
                            document.getElementById("taxesDiv").disabled = true;
                        } else{
                            document.getElementById("casinoFees").style.visibility = "hidden";
                            document.getElementById("casinoFees").disabled = true;
                        }
                        if (data === 'admin'){
                            document.getElementById("accuracyDiv").style.display = 'block';
                            let performsDiv = document.getElementById("performsDiv");
                            let evPerform = document.createElement("button");
                            evPerform.innerText = "Найти точный EV";
                            evPerform.setAttribute('onclick', 'getExactEv()');
                            performsDiv.appendChild(evPerform);
                        }
                    }
                });
            }
        </script>
        <script>
            function getExactEv(){
                getSlotInputValues();

                let stopwatchDiv = document.getElementById("stopwatch");
                stopwatchDiv.style.visibility = 'visible';
                StartStop();

                let waitingLabel = document.createElement("label");
                waitingLabel.setAttribute("id",  "waitingLabel");
                waitingLabel.innerText = "Происходит расчет. Ожидайте..."
                let successDiv = document.getElementById("message");
                successDiv.appendChild(waitingLabel);

                $.ajax({
                    type: 'POST',
                    url: '/SlotApplication/exact-EV',
                    data: window.slotInputFront,
                    success: function(data){
                        document.getElementById("reportPlace").style.display = 'none';
                        document.getElementById("exactEVReport").style.display = 'block';

                        let errorMessageLabel = document.getElementById("error-ev-message");
                        if (errorMessageLabel !== null){
                            errorMessageLabel.remove();
                        }

                        let waitingToDelete = document.getElementById("waitingLabel");
                        if (waitingToDelete !== null){
                            waitingToDelete.remove();
                        }

                        StartStop();
                        ClearСlock();
                        stopwatchDiv.style.visibility = 'hidden';
                        document.getElementById("expectedTime").innerText="";

                        if (data.error !== null){
                            let errorMessageDiv = document.getElementById("error-ev-div");
                            let errorLabel = document.createElement("label");
                            errorLabel.setAttribute("id", "error-ev-message");
                            errorLabel.innerText = data.error;
                            errorMessageDiv.appendChild(errorLabel);
                        } else {

                            document.getElementById("evValue").innerText="Точное EV = " + data.ev;
                            document.getElementById("simulationsEV").innerText="Количество симуляций для достижения точного EV = " + data.simulations;
                        }
                    }
                });
            }
        </script>
        <script>
            function advanceSettings(){
                let advanceSettingsDiv = document.getElementById("advanceSettings");
                if (advanceSettingsDiv.style.display === 'none' || advanceSettingsDiv.style.display === ''){
                    advanceSettingsDiv.style.display = 'block';
                } else {
                    advanceSettingsDiv.style.display = 'none';
                }
            }
        </script>
</head>
<body>
    <script>enabledPerform();</script>
    <script> getPrompts();</script>
    <div id="menu">
        <nav>
            <ul class="menu-ul">
                <li class="menu-li"><a class="menu-a" href='/SlotApplication/'>Home</a></li>
                <li class="menu-li"><a class="menu-a" href='/SlotApplication/tools'>Tools</a></li>
                <li class="menu-li"><a class="menu-a" href='/SlotApplication/slots'>Slots</a></li>
                <li class="menu-li"><a class="menu-a" href='/SlotApplication/jackpots'>Jackpots</a></li>
            </ul>
        </nav>
    </div>
<%  SlotInputError errorInput = (SlotInputError) request.getAttribute("errorInput");
    SlotInputReport report = (SlotInputReport) request.getAttribute("report");
    List<PairProfit> top5 = (List<PairProfit>) request.getAttribute("profitProbability");
    List<String> providers = (List<String>) request.getAttribute("providers");
    if (errorInput==null){
        if (report == null){
%>
            <div id="inputContainer">
                <div>
                    <label>Депозит: </label> <input id="deposit"/><label class="prompt-label">    ?   </label><label id="depositPrompt" class="prompt"></label>
                </div>

                <div>
                    <label>Бонус: </label><input id="bonus"/><label class="prompt-label">  ?  </label><label id="bonusPrompt" class="prompt"></label>
                </div>

                <div id="casinoFees">
                    <div>
                        <label>Provider Fee: </label><input id="providerFee" value="0"/><label class="prompt-label">  ?  </label><label id="providerFeePrompt" class="prompt"></label>
                    </div>
                    <div>
                        <label>  Payment Fee: </label><input id="paymentFee" value="0"/><label class="prompt-label">  ?  </label><label id="paymentFeePrompt" class="prompt"></label>
                    </div>
                    <div>
                        <label>  Platform Fee: </label><input id="platformFee" value="0"/><label class="prompt-label">  ?  </label><label id="platformFeePrompt" class="prompt"></label>
                    </div>
                </div>

                <div>
                    <label>Ставка: </label> <input id="bet"/><label class="prompt-label">  ?  </label><label id="betPrompt" class="prompt"></label>
                </div>

                <div>
                    <label>Wagger type </label>
                    <select id="waggerType">
        <%          for (String waggerType : (List<String>) request.getAttribute("waggerTypes")){
        %>              <option><%=waggerType%></option>
        <%          }
        %>          </select>
                    <label class="prompt-label">  ?  </label><label id="waggerTypePrompt" class="prompt"></label>
                </div>

                <div>
                    <label>Wagger: </label> <input id="wagger"/><label class="prompt-label">  ?  </label><label id="waggerPrompt" class="prompt"></label>
                </div>

                <div>
                    <label>Симуляции: </label><input id="sim"/><label class="prompt-label">  ?  </label><label id="simulationsPrompt" class="prompt"></label>
                </div>
            </div>

            <div id="inputSettings">
                <button id="settingsButton" onclick="advanceSettings()">Расширенные настройки</button>
                <div id="advanceSettings">
                    <div>
                        <label>  Результат без учета бонуса:</label><input type="checkbox" id="withoutBonus"/><label class="prompt-label">  ?  </label><label id="withoutBonusPrompt" class="prompt"></label>
                    </div>

                    <div>
                        <label>  Обращаться в БД:</label><input type="checkbox" id="manual" checked/><label class="prompt-label">  ?  </label><label id="manualPrompt" class="prompt"></label>
                    </div>

                    <div id="taxesDiv">
                        <label>Налог: </label><input id="taxes" value="0"/><label class="prompt-label">  ?  </label><label id="taxesPrompt" class="prompt"></label>
                    </div>

                    <div id="accuracyDiv" class="hidden">
                        <label>Точность расчета точного EV в % (от 0 до 100)</label><input id="accuracy" class="text-input" value="95"/><label class="prompt-label">  ?  </label><label id="accuracyPrompt" class="prompt"></label>
                    </div>

                    <div>
                        <button id="hideButton" onclick="hideMaxProfit()">Добавить ограничение максимального выигрыша</button>
                        <div id="maxProfitDiv" style="visibility: hidden">
                            <label>   Максимальное значение выигрыша одной симуляции</label>
                            <input id="maxProfit"/><label class="prompt-label">  ?  </label><label id="maxProfitPrompt" class="prompt"></label>
                        </div>
                    </div>

                    <div>
                        <button id="hideBonusButton" onclick="hideBonusPart()">Бонус отыгрывается частями</button>
                        <div id="bonusPartDiv" style="visibility: hidden"><label>   Количество частей бонуса</label>
                            <input id="bonusPart"/><label class="prompt-label">  ?  </label><label id="bonusPartPrompt" class="prompt"></label>
                        </div>
                    </div>
                </div>
            </div>

<%      }
    } %>
<br>
<div id="performsDiv">
    <button id="perform" onclick="getReport()">Выполнить</button>
</div>


<div id="performDisabled"></div>
<div id="redirection" style="visibility:hidden">
    <button onclick="location.href='/SlotApplication/upload'">Загрузить слот</button>
    <button onclick="location.href='/SlotApplication/calc-home-db'">Рассчитать слот</button>
    <button onclick="location.href='/SlotApplication/report'">Рассчитать бонус из базы</button>
</div>
<br>
<button onclick="location.href='/SlotApplication/'">Вернуться в меню</button>
<br>
<br>

<div id="message"></div>
<div style="visibility: hidden" id="stopwatch">
    <form name=MyForm>
        <input name=stopwatch size=10 value="00:00:00">
    </form>
    <div id="expectedTime"></div><br>
</div>
<div id="exactEVReport">
    <div id="error-ev-div"></div>
    <label id="evValue"></label><br>
    <label id="simulationsEV"></label>
</div>

<div id="reportPlace">
    <div id="error-message-div"></div>
    <div id="report">
        <label id="slotLabel"></label><br>
        <label id="providerLabel"></label><br><br>
        <label id="profitLabel"></label><br>
        <label id="evLabel"></label><br>
        <label id="roiLabel"></label><br>
        <label id="bonusVolatilityLabel"></label><br>
        <label id="positiveProfitLabel"></label><br>
        <label id="nullProfitLabel"></label><br>
        <label id="averageWin"></label><br>
        <label id="betCounter"></label><br><br>

        <label id="topProfitsLabel"></label><br>
        <div id="topProfitsDiv"></div><br>

        <label id="depositMultiplierLabel"></label>
        <div id="depositMultiplierDiv"></div><br>

        <div id="series"></div><br>

        <label id="longestNonProfitLabel"></label><br>
        <label id="longestNonProfitFrequencyLabel"></label><br><br>
        <label id="longestProfitLabel"></label><br>
        <label id="longestProfitFrequencyLabel"></label><br><br>
    </div>
</div>
<script>hideTaxes()</script>
</body>
</html>