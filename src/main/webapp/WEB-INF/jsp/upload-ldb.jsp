<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="by.minsk.miroha.entities.databases.SlotName"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html lang="en" pageEncoding="UTF-8">
<head>
    <meta charset="UTF-8">
    <title>Загрузка информации в локальную базу</title>
</head>
<style>
    #menu {
        margin-left: 30%;
        position: absolute;
        top: 2em;
    }
    .menu-li {
        display: inline;
        margin: -2.5;
    }
    .menu-ul {
        list-style: none;
        margin: 0;
        padding-left: 0;
    }
    .menu-a {
        text-decoration: none;
        padding: 1em;
        background: #f5f5f5;
        border: 1px solid #b19891;
        color: #695753;
    }
</style>
<body>
    <div id="menu">
        <nav>
            <ul class="menu-ul">
                <li class="menu-li"><a class="menu-a" href='/SlotApplication/'>Home</a></li>
                <li class="menu-li"><a class="menu-a" href='/SlotApplication/tools'>Tools</a></li>
                <li class="menu-li"><a class="menu-a" href='/SlotApplication/slots'>Slots</a></li>
                <li class="menu-li"><a class="menu-a" href='/SlotApplication/jackpots'>Jackpots</a></li>
            </ul>
        </nav>
    </div>
    <div class="message"><h2>Загрузка базы из файла без расчета ее показателей</h2></div>
    <form:form action="/SlotApplication/upload" method="post" modelAttribute="slotName">

        <form:label path="slotName">Slot: </form:label>
        <form:input path="slotName" value="${slotName.slotName}"/> <br>

        <form:label path="provider">Provider: </form:label>
        <form:input path="provider" value="${slotName.provider}"/> <br>
        <br>
        <br>
        <label path="error">${error}</label><br>
        <br>
        <div style="visibility: ${uploadDisabled};">
            <form:button >Загрузить</form:button>
        </div>
    </form:form>
    <br>
    <button onclick="location.href='/SlotApplication/deleteHome'" ${deleteDisabled}>Очистить базу</button>
    <br>
    <label>${deleted}</label>
    <br>
    <br>
    <button onclick="location.href='/SlotApplication/'">Назад</button>
    <br><br>
    <div>${uploaded}</div>
    </body>
</html>