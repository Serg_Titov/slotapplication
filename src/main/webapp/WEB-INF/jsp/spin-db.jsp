<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="by.minsk.miroha.entities.front.SpinnerFront"%>
<%@ page import="java.util.List"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html lang="en" pageEncoding="UTF-8">
<head>
    <meta charset="UTF-8">
    <title>Статистика спинов</title>
    <style>
        #menu {
            margin-left: 30%;
            position: absolute;
            top: 2em;
        }
        .menu-li {
            display: inline;
            margin: -2.5;
        }
        .menu-ul {
            list-style: none;
            margin: 0;
            padding-left: 0;
        }
        .menu-a {
            text-decoration: none;
            padding: 1em;
            background: #f5f5f5;
            border: 1px solid #b19891;
            color: #695753;
        }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script>

        function createGame(){
            let provider = document.getElementById("provider");
            let container = document.getElementById("games");
            let gameSelect = document.getElementById("game");
            if (gameSelect !== null){
                gameSelect.remove();
            }
            $.ajax({
                type: 'POST',
                url: '/SlotApplication/get-games',
                data: { provider: provider.value },
                success: function(data){
                    let gamesSelect = document.createElement("select");
                    gamesSelect.setAttribute("id",  "game");
                    for(let game of data){
                        let option = document.createElement("option");
                        option.value = game.rawSlot;
                        option.text = game.cleanSlot;
                        gamesSelect.appendChild(option);
                    }
                    container.appendChild(gamesSelect);
                }
            });
        };
    </script>
    <script>
        function perform(){
            let spinNumberC = document.getElementById("spinNumber");
            let simNumberC = document.getElementById("simNumber");
            let providerC = document.getElementById("provider");
            let gameC = document.getElementById("game");
            let spinnerFront = {
                provider: providerC.value,
                game: gameC.value,
                spinLimit: spinNumberC.value,
                simulations: simNumberC.value,
                result: ""
            };

            $.ajax({
                type: 'POST',
                url: '/SlotApplication/spinner-db',
                data: spinnerFront,
                success: function(data){
                    let waitingToDelete = document.getElementById("waitingLabel");
                    if (waitingToDelete !== null){
                        waitingToDelete.remove();
                    }

                    let resultLabel = document.getElementById("result");
                    resultLabel.innerText = "Результат,% : " + data.result;
                }

            });
            let waitingLabel = document.createElement("label");
            waitingLabel.setAttribute("id",  "waitingLabel");
            waitingLabel.innerText = "Происходит расчет. Ожидайте..."
            let successDiv = document.getElementById("message");
            successDiv.appendChild(waitingLabel);
        }
    </script>
</head>
<body>
<%  List<String> providers = (List<String>) request.getAttribute("providers");
%>
    <div id="menu">
        <nav>
            <ul class="menu-ul">
                <li class="menu-li"><a class="menu-a" href='/SlotApplication/'>Home</a></li>
                <li class="menu-li"><a class="menu-a" href='/SlotApplication/tools'>Tools</a></li>
                <li class="menu-li"><a class="menu-a" href='/SlotApplication/slots'>Slots</a></li>
                <li class="menu-li"><a class="menu-a" href='/SlotApplication/jackpots'>Jackpots</a></li>
            </ul>
        </nav>
    </div>
    <label>Выберите провайдера: <label>
    <select id="provider" onchange="createGame()">
<%  for(String provider : providers){
%>      <option><%=provider%></option>
<%  }
%>  </select>
    <br>
    <label>Выберите игру: <label>
    <div id="games"></div>
    <script> createGame();</script>
    <br>
    <label>Количество спинов: </label>
    <input type="text" id="spinNumber" value="${spinnerFront.spinLimit}"/><br>

    <label>Количество симуляций: </label>
    <input type="text" id="simNumber" value="${spinnerFront.simulations}"/><br>

    <br>
    <button onclick="perform()" id="perform">Рассчитать введенный спин</button><br>
    <small>Sim * Spin = 1 000 000 000 будет выполняться около 5 мин</small><br>
    <small>Sim * Spin = 10 000 000 000 будет выполняться около 35 мин</small><br>
    <br>
    <div id="performDisabled"></div>
    <div id="redirection" style="visibility:hidden">
        <button onclick="location.href='/SlotApplication/upload'">Загрузить слот</button>
        <button onclick="location.href='/SlotApplication/calc-home-db'">Рассчитать слот</button>
    </div>
    <label id="result"></label>
    <div id="message"></div>
<br>
<button onclick="location.href='/SlotApplication/'">Вернуться в меню</button>
</body>
</html>