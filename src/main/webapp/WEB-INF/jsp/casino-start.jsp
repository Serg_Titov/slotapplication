<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en" pageEncoding="UTF-8">
<head>
    <meta charset="UTF-8">
    <title>Bonuses & slots analytics</title>
    <style>
        .header {
            text-align: center
        }
        .div-line{
            display:inline-block;
            padding:0 10px;
        }
        .center{
            margin-left: 5%
        }
        .simple-input{
            width: 50px
        }
        .text-input{
            width: 110px
        }
        .button-center{
            margin-left: 13%
        }
        .input-data{
            display: none
        }
        .prompt{
            display: none;
        }

        .prompt-label:hover + .prompt {
            display: block;
        }
        #report{
            display: none;
        }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script>
        function getSlotInputValues(){
            let depositC = document.getElementById("deposit");
            let bonusC = document.getElementById("bonus");
            let slotInputFrontO = {
                deposit: depositC.value,
                bonus: bonusC.value
            };
            window.slotInputFront = slotInputFrontO;
        }
    </script>
    <script>
        function getCasinoReport(){
            getSlotInputValues();
            let stopwatchDiv = document.getElementById("stopwatch");
            stopwatchDiv.style.visibility = 'visible';
            StartStop();

            let waitingLabel = document.getElementById("waitingLabel");
            if (waitingLabel === null){
                waitingLabel = document.createElement("label");
                waitingLabel.setAttribute("id",  "waitingLabel");
                let successDiv = document.getElementById("message");
                successDiv.appendChild(waitingLabel);
            }
            waitingLabel.innerText = "Происходит расчет. Ожидайте...";

            $.ajax({
                type: 'POST',
                url: '/SlotApplication/casino',
                data: window.slotInputFront,
                success: function(data){
                    let emptyString = document.createElement("br");
                    let reportContainer = document.getElementById("report");
                    reportContainer.style.display = 'block';
                    let reportPlace = document.getElementById("reportPlace");

                    if (data.error !== null){
                        let errorMessageDiv = document.getElementById("error-message-div");
                        let errorLabel = document.createElement("label");
                        errorLabel.setAttribute("id", "error-message");
                        errorLabel.innerText = data.error;
                        errorMessageDiv.appendChild(errorLabel);
                    } else {
                        let averageWinLabel = document.getElementById("averageWin");
                        averageWinLabel.innerText = "Средний бонусный выигрыш: " + data.commonSimInformation.averageWin;

                        let evPlayerLabel = document.getElementById("evPlayerLabel");
                        evPlayerLabel.innerText = "EV Игрока: " + data.commonSimInformation.ev;

                        let evCasinoLabel = document.getElementById("evCasinoLabel");
                        evCasinoLabel.innerText = "EV Казино: " + (-1*data.commonSimInformation.ev) + "*";

                        let spinAmountLabel = document.getElementById("spinAmountLabel");
                        spinAmountLabel.innerText = "Количество спинов: " + data.commonSimInformation.betCounter;

                        let roiPlayerLabel = document.getElementById("roiPlayerLabel");
                        roiPlayerLabel.innerText = "ROI Игрока: " + data.commonSimInformation.roi;

                        let roiCasinoLabel = document.getElementById("roiCasinoLabel");
                        roiCasinoLabel.innerText = "ROI Казино: " + (-1*data.commonSimInformation.roi) + "*";

                        let bonusDispersionLabel = document.getElementById("bonusVolatilityLabel");
                        bonusDispersionLabel.innerText = "Волатильность бонуса: " + data.commonSimInformation.bonusVolatility;

                        let positiveProfitLabel = document.getElementById("positiveProfitLabel");
                        positiveProfitLabel.innerText = "Вероятность получить Profit больше 0 : " + data.commonSimInformation.profitProbability;

                        let nullProfitLabel = document.getElementById("nullProfitLabel");
                        nullProfitLabel.innerText = "Вероятность получить Profit  0 : " + data.commonSimInformation.nonProfitProbability;

                        let topProfitsLabel = document.getElementById("topProfitsLabel");
                        topProfitsLabel.innerText = "Самые большие победы";

                        let topProfitsDiv = document.getElementById("topProfitsDiv");
                        while (topProfitsDiv.hasChildNodes()) {
                            topProfitsDiv.removeChild(topProfitsDiv.firstChild);
                        }
                        for(let i = 0; i<4; i++){
                            let topProfitsLabel = document.createElement("label");
                            topProfitsLabel.innerText = "TOP-" + data.topProfits[i].name + ": " +
                                data.topProfits[i].profit;
                            topProfitsDiv.appendChild(topProfitsLabel);
                            let newEmptyString = document.createElement("br");
                            topProfitsDiv.appendChild(newEmptyString);
                        }

                        let depositMultiplierLabel = document.getElementById("depositMultiplierLabel");
                        depositMultiplierLabel.innerText = "Вероятность увеличить депозит в [X] раз";

                         let depositMultiplierDiv = document.getElementById("depositMultiplierDiv");
                         while (depositMultiplierDiv.hasChildNodes()) {
                             depositMultiplierDiv.removeChild(depositMultiplierDiv.firstChild);
                         }
                         for(let depositMultiplier of data.depositMultipliers){
                             let depositMultiplierLabel = document.createElement("label");
                             depositMultiplierLabel.innerText = "Коэффициент выигрыша " + depositMultiplier.depositMultiplier +
                                 ": раз в " + depositMultiplier.depositMultiplierFrequency + " симуляций.";
                             depositMultiplierDiv.appendChild(depositMultiplierLabel);
                             let newEmptyString = document.createElement("br");
                             depositMultiplierDiv.appendChild(newEmptyString);
                         }

                         let longestProfitLabel = document.getElementById("longestProfitLabel");
                         longestProfitLabel.innerText = "MAX серия выигрышей : " + data.commonSimInformation.longestProfitSeries;

                         let longestNonProfitLabel = document.getElementById("longestNonProfitLabel");
                         longestNonProfitLabel.innerText = "MAX серия проигрышей : " + data.commonSimInformation.longestNonProfitSeries;

                        let seriesLabel = document.getElementById("seriesLabel");
                        seriesLabel.innerText = "Шансы проиграть [N] депозитов подряд";

                         let seriesDiv = document.getElementById("seriesDiv");
                         while (seriesDiv.hasChildNodes()) {
                             seriesDiv.removeChild(seriesDiv.firstChild);
                         }
                         for(let i = 0; i<5; i++){
                             let seriesValueLabel = document.createElement("label");
                             seriesValueLabel.innerText = data.nonProfitSeries[i].seriesName +
                                 ": " + data.nonProfitSeries[i].series;
                             seriesDiv.appendChild(seriesValueLabel);
                             let newEmptyString = document.createElement("br");
                             seriesDiv.appendChild(newEmptyString);
                         }

                         document.getElementById("inputData").style.display = 'block';
                         let inputDepositLabel = document.getElementById("inputDeposit");
                         let selectDeposit = document.getElementById("deposit");
                         inputDepositLabel.innerText = "Депозит - " + selectDeposit.value;

                         let inputBonusLabel = document.getElementById("inputBonus");
                         let selectBonus = document.getElementById("bonus");
                         inputBonusLabel.innerText = "Бонус - " + selectBonus.value + "% (депозита)";

                         let casinoMessageLabel = document.getElementById("casinoMessage");
                         casinoMessageLabel.innerText = "* - Данный расчет без учёта коммиссий. Для расчета с учетом коммисий необходимо приобрести подписку";
                         getPrompts();
                    }
                    StartStop();
                    ClearСlock();
                    stopwatchDiv.style.visibility = 'hidden';
                    document.getElementById("waitingLabel").innerText = "";
                },
                error: function(error){
                    StartStop();
                    ClearСlock();
                    stopwatchDiv.style.visibility = 'hidden';
                    document.getElementById("waitingLabel").innerText = "Что-то пошло не так...";
                }
            });
        }
    </script>
    <script>
        function getPrompts(){
            $.ajax({
                type: 'GET',
                url: '/SlotApplication/get-casino-prompts',
                success: function(data){
                    let evPlayerPromptLabel = document.getElementById("evPlayerPrompt");
                    evPlayerPromptLabel.innerText = data.evPlayer;

                    let evCasinoPromptLabel = document.getElementById("evCasinoPrompt");
                    evCasinoPromptLabel.innerText = data.evCasino;

                    let roiPlayerPromptLabel = document.getElementById("roiPlayerPrompt");
                    roiPlayerPromptLabel.innerText = data.roiPlayer;

                    let roiCasinoPromptLabel = document.getElementById("roiCasinoPrompt");
                    roiCasinoPromptLabel.innerText = data.roiCasino;

                    let bonusVolatilityPromptLabel = document.getElementById("bonusVolatilityPrompt");
                    bonusVolatilityPromptLabel.innerText = data.bonusVolatility;

                    let depositMultiplierPromptLabel = document.getElementById("depositMultiplierPrompt");
                    depositMultiplierPromptLabel.innerText = data.depositMultipliers;

                    let seriesPromptLabel = document.getElementById("seriesPrompt");
                    seriesPromptLabel.innerText = data.series;
                }
            });
        }
    </script>
    <script>
        var base = 60;
           var clocktimer,dateObj,dh,dm,ds,ms;
           var readout='';
           var h=1,m=1,tm=1,s=0,ts=0,ms=0,init=0;

           //функция для очистки поля
           function ClearСlock() {
                clearTimeout(clocktimer);
                h=1;m=1;tm=1;s=0;ts=0;ms=0;
                init=0;
                readout='00:00:00';
                document.MyForm.stopwatch.value=readout;
           }

           //функция для старта секундомера
           function StartTIME() {
                var cdateObj = new Date();
                var t = (cdateObj.getTime() - dateObj.getTime())-(s*1000);
                if (t>999) { s++; }
                if (s>=(m*base)) {
                        ts=0;
                        m++;
                } else {
                        ts=parseInt((ms/100)+s);
                        if(ts>=base) { ts=ts-((m-1)*base); }
                }
                if (m>(h*base)) {
                        tm=1;
                        h++;
                } else {
                        tm=parseInt((ms/100)+m);
                        if(tm>=base) { tm=tm-((h-1)*base); }
                }
                ms = Math.round(t/10);
                if (ms>99) {ms=0;}
                if (ms==0) {ms='00';}
                if (ms>0&&ms<=9) { ms = '0'+ms; }
                if (ts>0) { ds = ts; if (ts<10) { ds = '0'+ts; }} else { ds = '00'; }
                dm=tm-1;
                if (dm>0) { if (dm<10) { dm = '0'+dm; }} else { dm = '00'; }
                dh=h-1;
                if (dh>0) { if (dh<10) { dh = '0'+dh; }} else { dh = '00'; }
                readout = dh + ':' + dm + ':' + ds ;
                document.MyForm.stopwatch.value = readout;
                clocktimer = setTimeout("StartTIME()",1000);
           }

           //Функция запуска и остановки
           function StartStop() {
                if (init==0){
                        ClearСlock();
                        dateObj = new Date();
                        StartTIME();
                        init=1;
                } else {
                        clearTimeout(clocktimer);
                        init=0;
                }
           }
    </script>
</head>
<body>
    <div>
        <h2 class="header">Попробуйте сами</h2>
    </div>
    <div>
        <div class='div-line'>
            <label>Выберите сумму депозита</label>
            <select id='deposit'>
                <option>100</option>
                <option>150</option>
                <option>200</option>
                <option>250</option>
                <option>300</option>
            </select>
        </div>
        <div class='div-line'>
            <label>Выберите % бонуса за депозит</label>
            <select id='bonus'>
                <option>50</option>
                <option>75</option>
                <option>100</option>
                <option>125</option>
            </select>
        </div>
    </div>
    <br>
    <div class='center'>
        <label>Для изменения всех параметров требуется подписка</label>
    </div>
    <br>
    <div>
        <div class='div-line'>
            <label>Ставка</label>
            <input type='text' id='bet' value='5' class='simple-input' disabled/>
        </div>
        <div class='div-line'>
            <label>Отыгрыш</label>
            <input type='text' id='wagger' value='45' class='simple-input' disabled/>
        </div>
        <div class='div-line'>
            <label>Тип отыгрыша</label>
            <input type='text' id='waggerType' value='B' class='simple-input' disabled/>
        </div>
    </div>
    <br>
    <div>
        <div class='div-line'>
            <label>Провайдер</label>
            <input type='text' id='provider' value='Pragmatic' class='text-input' disabled/>
        </div>
        <div class='div-line'>
            <label>Слот</label>
            <input type='text' id='slot' value='The Dog House' class='text-input' disabled/>
        </div>
        <div class='div-line'>
            <button disabled>Расширенные параметры</button>
        </div>
    </div>
    <br>
    <div>
        <button class='button-center' onclick="getCasinoReport()">Рассчитать</button>
    </div><br>
    <div id="message"></div>
    <div style="visibility: hidden" id="stopwatch">
        <form name=MyForm>
            <input name=stopwatch size=10 value="00:00:00">
        </form>
    </div>
    <div id="reportPlace">
        <div id="error-message-div"></div>
        <div id="report">
            <label id="averageWin"></label><br>
            <label id="evPlayerLabel"></label><label class="prompt-label">  ?  </label><label id="evPlayerPrompt" class="prompt"></label><br>
            <label id="evCasinoLabel"></label><label class="prompt-label">  ?  </label><label id="evCasinoPrompt" class="prompt"></label><br>
            <label id="spinAmountLabel"></label><br>
            <label id="roiPlayerLabel"></label><label class="prompt-label">  ?  </label><label id="roiPlayerPrompt" class="prompt"></label><br>
            <label id="roiCasinoLabel"></label><label class="prompt-label">  ?  </label><label id="roiCasinoPrompt" class="prompt"></label><br>
            <label id="bonusVolatilityLabel"></label><label class="prompt-label">  ?  </label><label id="bonusVolatilityPrompt" class="prompt"></label><br>
            <label id="positiveProfitLabel"></label><br>
            <label id="nullProfitLabel"></label><br><br>

            <label id="topProfitsLabel"></label><br>
            <div id="topProfitsDiv"></div><br><br>

            <label id="depositMultiplierLabel"></label><label class="prompt-label">  ?  </label><label id="depositMultiplierPrompt" class="prompt"></label><br>
            <div id="depositMultiplierDiv"></div><br><br>

            <div id="inputData" class="input-data">
                <label id="inputProvider">Провайдер - Pragmatic</label><br>
                <label id="inputSlot">Слот - The Dog House</label><br>
                <label id="inputDeposit"></label><br>
                <label id="inputBonus"></label><br>
                <label id="inputMaxProfit">Огран. макс. выигрыша - не ограничен</label><br>
                <label id="inputBet">Ставка - 5</label><br>
                <label id="inputBonusType">Механика бонуса - Reload</label><br>
                <label id="inputWagger">Вагер - 45 B</label><br>
            </div><br>

            <label id="longestProfitLabel"></label><br>
            <label id="longestNonProfitLabel"></label><br>
            <label id="seriesLabel"></label><label class="prompt-label">  ?  </label><label id="seriesPrompt" class="prompt"></label><br>
            <div id="seriesDiv"></div><br><br>

            <label id="casinoMessage"></label>
        </div>
    </div>
    <a href="/SlotApplication/casino-report">Общая информация о слотах</a>
</body>
</html>