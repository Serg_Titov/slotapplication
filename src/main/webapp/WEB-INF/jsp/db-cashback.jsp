<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.List"%>
<html lang="en" pageEncoding="UTF-8">
    <head>
        <meta charset="UTF-8">
        <title>Расчет кэшбека из базы</title>
        <style>
            #menu {
                margin-left: 30%;
                position: absolute;
                top: 2em;
            }
            .menu-li {
                display: inline;
                margin: -2.5;
            }
            .menu-ul {
                list-style: none;
                margin: 0;
                padding-left: 0;
            }
            .menu-a {
                text-decoration: none;
                padding: 1em;
                background: #f5f5f5;
                border: 1px solid #b19891;
                color: #695753;
            }
        </style>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
        <script>
            function createGame(){
                let provider = document.getElementById("provider");
                let container = document.getElementById("games");
                let gameSelect = document.getElementById("game");
                if (gameSelect !== null){
                    gameSelect.remove();
                }
                $.ajax({
                    type: 'POST',
                    url: '/SlotApplication/get-games',
                    data: { provider: provider.value },
                    success: function(data){
                        let reportPlace = document.getElementById("provider");
                        let gamesSelect = document.createElement("select");
                        gamesSelect.setAttribute("id",  "game");
                        for(let game of data){
                            let option = document.createElement("option");
                            option.value = game.rawSlot;
                            option.text = game.cleanSlot;
                            gamesSelect.appendChild(option);
                        }
                        container.appendChild(gamesSelect);
                    }
                });
            };
        </script>
        <script>
                function upload(){
                    let providerC = document.getElementById("provider");
                    let gameC = document.getElementById("game");
                    let cashbackC = document.getElementById("cashbackValue");
                    let simAmountC = document.getElementById("simAmount");
                    let slotNameO = {
                        slotName: gameC.value,
                        provider: providerC.value,
                        cashbackValue: cashbackC.value,
                        simAmount: simAmountC.value
                    };

                    let messageLabelOld = document.getElementById("messageLabel");
                    if (messageLabelOld !== null){
                        messageLabelOld.remove();
                    }

                    $.ajax({
                         type: 'POST',
                         url: '/SlotApplication/db-cashback',
                         data: slotNameO,
                         success: function(data){
                            console.log(data);
                            let messageDiv = document.getElementById("message");
                            let messageLabel = document.createElement("label");
                            messageLabel.setAttribute("id", "messageLabel");
                            messageLabel.innerText = data;
                            messageDiv.appendChild(messageLabel);

                            let waitingToDelete = document.getElementById("waitingLabel");
                            if (waitingToDelete !== null){
                                waitingToDelete.remove();
                            }
                         },
                         error: function(data){
                             let messageDiv = document.getElementById("message");
                             let messageLabel = document.createElement("label");
                             messageLabel.setAttribute("id", "messageLabel");
                             messageLabel.innerText = JSON.stringify(data);
                             messageDiv.appendChild(messageLabel);

                             let waitingToDelete = document.getElementById("waitingLabel");
                             if (waitingToDelete !== null){
                                 waitingToDelete.remove();
                             }
                         }
                    });
                    let waitingLabel = document.createElement("label");
                    waitingLabel.setAttribute("id",  "waitingLabel");
                    waitingLabel.innerText = "Происходит расчет. Ожидайте..."
                    let successDiv = document.getElementById("message");
                    successDiv.appendChild(waitingLabel);
                }
        </script>
    </head>
    <body>
    <%  List<String> providers = (List<String>) request.getAttribute("providers");
    %>
        <label>Выберите провайдера: <label>
        <select id="provider" onchange="createGame()">
    <%      for(String provider : providers){
    %>          <option><%=provider%></option>
    <%      }
    %>  </select>
         <br>
         <label>Выберите игру: <label>
         <div id="games"></div>
         <script> createGame();</script>
        <br>
        <label>Введите размер кэшбека </label>
        <input id="cashbackValue" value="0.15"/>
        <br><br>
        <label>Введите количество симуляций </label>
        <input id="simAmount" />
        <br>
        <br>
        <button id="upload" onclick="upload()">Рассчитать</button>
        <br>
        <br>
        <button onclick="location.href='/SlotApplication/'">Вернуться в меню</button>
        <br><br>
        <div id="message"></div><br><br>
    </body>
</html>