<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="by.minsk.miroha.entities.databases.SlotName"%>
<%@ page import="java.util.List"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html lang="en" pageEncoding="UTF-8">
    <head>
        <meta charset="UTF-8">
        <title>Рассчет всех слотов провайдера</title>
        <style>
            #menu {
                margin-left: 30%;
                position: absolute;
                top: 2em;
            }
            .menu-li {
                display: inline;
                margin: -2.5;
            }
            .menu-ul {
                list-style: none;
                margin: 0;
                padding-left: 0;
            }
            .menu-a {
                text-decoration: none;
                padding: 1em;
                background: #f5f5f5;
                border: 1px solid #b19891;
                color: #695753;
            }
        </style>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
        <script>
            function calcAllSlots(){
                let provider = document.getElementById("provider");
                $.ajax({
                    type: 'POST',
                    url: '/SlotApplication/calc-provider',
                    data: { provider: provider.value },
                    success: function(data){
                        let resultLabel = document.getElementById("resultMessage");
                        resultLabel.innerText = data;

                        let waitingToDelete = document.getElementById("waitingLabel");
                        if (waitingToDelete !== null){
                            waitingToDelete.remove();
                        }
                    }
                });
                let waitingLabel = document.createElement("label");
                waitingLabel.setAttribute("id",  "waitingLabel");
                waitingLabel.innerText = "Происходит расчет. Ожидайте..."
                let successDiv = document.getElementById("message");
                successDiv.appendChild(waitingLabel);
            }
        </script>
    </head>
    <body>
    <%  List<String> providers = (List<String>) request.getAttribute("providers");
    %>
        <div id="menu">
            <nav>
                <ul class="menu-ul">
                  <li class="menu-li"><a class="menu-a" href='/SlotApplication/'>Home</a></li>
                  <li class="menu-li"><a class="menu-a" href='/SlotApplication/tools'>Tools</a></li>
                  <li class="menu-li"><a class="menu-a" href='/SlotApplication/slots'>Slots</a></li>
                  <li class="menu-li"><a class="menu-a" href='/SlotApplication/jackpots'>Jackpots</a></li>
                </ul>
            </nav>
        </div>
        <label>Выберите провайдера: <label>
        <select id="provider">
    <%      for(String provider : providers){
    %>          <option><%=provider%></option>
    <%      }
    %>  </select>
        <br><br>
        <br>
        <button id="upload" onclick="calcAllSlots()">Рассчитать</button>
        <br>
        <br>
        <button onclick="location.href='/SlotApplication/'">Вернуться в меню</button>
        <br>
        <br>
        <div id="message">
            <label id="resultMessage"></label>
        </div>


    </body>
</html>