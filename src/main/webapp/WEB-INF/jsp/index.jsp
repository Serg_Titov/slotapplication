<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>Bonuses & slots analytics</title>
  </head>
  <style>
      li {
        display: inline;
        margin: -2.5;
      }
      ul {
        list-style: none;
        margin: 0;
        padding-left: 0;
      }
      a {
        text-decoration: none;
        padding: 1em;
        background: #f5f5f5;
        border: 1px solid #b19891;
        color: #695753;
      }
      #menu {
        position: absolute;
        top: 2em;
        left: 35%;
      }
  </style>
  <body>
    <div id="menu">
      <nav>
        <ul>
          <li><a href='/SlotApplication/'>Home</a></li>
          <li><a href='/SlotApplication/tools'>Tools</a></li>
          <li><a href='/SlotApplication/slots'>Slots</a></li>
          <li><a href='/SlotApplication/jackpots'>Jackpots</a></li>
          <li><a href='/SlotApplication/sites'>Sites</a></li>
        </ul>
      </nav>
    </div>
  </body>
</html>