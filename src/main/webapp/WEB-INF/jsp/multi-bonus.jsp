<html lang="en" pageEncoding="UTF-8">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<head>
    <meta charset="UTF-8">
    <title>Массовый расчет бонусов</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <style>
        #menu {
            margin-left: 30%;
            position: absolute;
            top: 2em;
        }
        .menu-li {
            display: inline;
            margin: -2.5;
        }
        .menu-ul {
            list-style: none;
            margin: 0;
            padding-left: 0;
        }
        .menu-a {
            text-decoration: none;
            padding: 1em;
            background: #f5f5f5;
            border: 1px solid #b19891;
            color: #695753;
        }
        .advanceSettings{
            display: none;
        }
    </style>
    <script>
        function loadInputs(){
            window.inputCounter = 0;
            $.ajax({
                type: 'GET',
                url: '/SlotApplication/get-selects',
                async: false,
                success: function(data){
                    window.providers = data.providers;
                    window.waggerTypes = data.waggerTypes;
                }
            });
        }
    </script>
    <script>
        function createSlotInput(){
            let inputPlace = document.getElementById("inputPlace");
            let inputDiv = document.createElement("div");
            inputDiv.id = "slotInput" + window.inputCounter;
            inputPlace.appendChild(inputDiv);

            let providerDiv = document.createElement("div");
            inputDiv.appendChild(providerDiv);

            let providerLabel = document.createElement("label");
            providerLabel.innerText = "Выберите провайдера: ";
            providerDiv.appendChild(providerLabel);

            let providerSelect = document.createElement("select");
            providerSelect.id = "provider" + window.inputCounter;
            for(let provider of window.providers){
                let providerOption = document.createElement("option");
                providerOption.value = provider;
                providerOption.text = provider;
                providerSelect.appendChild(providerOption);
            }
            providerSelect.addEventListener('change', function createGame(e){
                let idCounter = e.target.id.slice(8);
                let provider = document.getElementById("provider" + idCounter);
                let container = document.getElementById("games" + idCounter);
                let gameSelect = document.getElementById("game" + idCounter);
                if (gameSelect !== null){
                    gameSelect.remove();
                }
                $.ajax({
                    type: 'POST',
                    url: '/SlotApplication/get-games',
                    data: { provider: provider.value },
                    success: function(data){
                        let gamesSelect = document.createElement("select");
                        gamesSelect.setAttribute("id",  "game" + idCounter);
                        for(let game of data){
                            let option = document.createElement("option");
                            option.value = game;
                            option.text = game;
                            gamesSelect.appendChild(option);
                        }
                        container.appendChild(gamesSelect);
                    }
                });
            });
            providerDiv.appendChild(providerSelect);

            let gameCommonDiv = document.createElement("div");
            inputDiv.appendChild(gameCommonDiv);

            let gameLabel = document.createElement("label");
            gameLabel.innerText = "Выберите слот: ";
            gameCommonDiv.appendChild(gameLabel);

            let gameDiv = document.createElement("div");
            gameDiv.id = "games" + window.inputCounter;
            gameCommonDiv.appendChild(gameDiv);
            createGame(window.inputCounter);

            let depositDiv = document.createElement("div");
            inputDiv.appendChild(depositDiv);

            let depositLabel = document.createElement("label");
            depositLabel.innerText = "Депозит: ";
            depositDiv.appendChild(depositLabel);

            let depositInput = document.createElement("input");
            depositInput.id = "deposit" + window.inputCounter;
            depositDiv.appendChild(depositInput);

            let bonusDiv = document.createElement("div");
            inputDiv.appendChild(bonusDiv);

            let bonusLabel = document.createElement("label");
            bonusLabel.innerText = "Бонус: ";
            bonusDiv.appendChild(bonusLabel);

            let bonusInput = document.createElement("input");
            bonusInput.id = "bonus" + window.inputCounter;
            bonusDiv.appendChild(bonusInput);

            let betDiv = document.createElement("div");
            inputDiv.appendChild(betDiv);

            let betLabel = document.createElement("label");
            betLabel.innerText = "Ставка: ";
            betDiv.appendChild(betLabel);

            let betInput = document.createElement("input");
            betInput.id = "bet" + window.inputCounter;
            betDiv.appendChild(betInput);

            let waggerTypeDiv = document.createElement("div");
            inputDiv.appendChild(waggerTypeDiv);

            let waggerTypeLabel = document.createElement("label");
            waggerTypeLabel.innerText = "Wagger type : ";
            waggerTypeDiv.appendChild(waggerTypeLabel);

            let waggerTypeSelect = document.createElement("select");
            waggerTypeSelect.id = "waggerType" + window.inputCounter;
            for(let waggerType of window.waggerTypes){
                let waggerTypeOption = document.createElement("option");
                waggerTypeOption.value = waggerType;
                waggerTypeOption.text = waggerType;
                waggerTypeSelect.appendChild(waggerTypeOption);
            }
            waggerTypeDiv.appendChild(waggerTypeSelect);

            let waggerDiv = document.createElement("div");
            inputDiv.appendChild(waggerDiv);

            let waggerLabel = document.createElement("label");
            waggerLabel.innerText = "Wagger: ";
            waggerDiv.appendChild(waggerLabel);

            let waggerInput = document.createElement("input");
            waggerInput.id = "wagger" + window.inputCounter;
            waggerDiv.appendChild(waggerInput);

            let simDiv = document.createElement("div");
            inputDiv.appendChild(simDiv);

            let simLabel = document.createElement("label");
            simLabel.innerText = "Симуляции: ";
            simDiv.appendChild(simLabel);

            let simInput = document.createElement("input");
            simInput.id = "sim" + window.inputCounter;
            simDiv.appendChild(simInput);

            let inputSettingsDiv = document.createElement("div");
            inputSettingsDiv.id = "inputSettings"  + window.inputCounter;
            inputDiv.appendChild(inputSettingsDiv);

            let advancedSettingsButton = document.createElement("button");
            advancedSettingsButton.innerText = 'Расширенные настройки';
            advancedSettingsButton.id = 'settingsButton' + window.inputCounter;
            advancedSettingsButton.addEventListener('click', function (e){
                let idCounter = e.target.id.slice(14);
                let advanceSettingsDiv = document.getElementById("advanceSettings"+idCounter);
                if (advanceSettingsDiv.style.display === 'none' || advanceSettingsDiv.style.display === ''){
                    advanceSettingsDiv.style.display = 'block';
                } else {
                    advanceSettingsDiv.style.display = 'none';
                }
            });
            inputSettingsDiv.appendChild(advancedSettingsButton);

            let advancedSettingsDiv = document.createElement("div");
            advancedSettingsDiv.id = "advanceSettings"+ window.inputCounter;
            advancedSettingsDiv.classList.add('advanceSettings');
            inputSettingsDiv.appendChild(advancedSettingsDiv);

            let withoutBonusDiv = document.createElement("div");
            advancedSettingsDiv.appendChild(withoutBonusDiv);

            let withoutBonusLabel = document.createElement("label");
            withoutBonusLabel.innerText = "Результат без учета бонуса: ";
            withoutBonusDiv.appendChild(withoutBonusLabel);

            let withoutBonusInput = document.createElement("input");
            withoutBonusInput.setAttribute('type', 'checkbox');
            withoutBonusInput.id = "withoutBonus" + window.inputCounter;
            withoutBonusDiv.appendChild(withoutBonusInput);

            let manualDiv = document.createElement("div");
            advancedSettingsDiv.appendChild(manualDiv);

            let manualLabel = document.createElement("label");
            manualLabel.innerText = "Обращаться в БД: ";
            manualDiv.appendChild(manualLabel);

            let manualInput = document.createElement("input");
            manualInput.setAttribute('type', 'checkbox');
            manualInput.setAttribute('checked', 'checked');
            manualInput.id = "manual" + window.inputCounter;
            manualDiv.appendChild(manualInput);

            let taxesDiv = document.createElement("div");
            advancedSettingsDiv.appendChild(taxesDiv);

            let taxesLabel = document.createElement("label");
            taxesLabel.innerText = "Налог: ";
            taxesDiv.appendChild(taxesLabel);

            let taxesInput = document.createElement("input");
            taxesInput.id = "taxes" + window.inputCounter;
            taxesInput.value = 0;
            taxesDiv.appendChild(taxesInput);

            let maxProfitCommonDiv = document.createElement("div");
            advancedSettingsDiv.appendChild(maxProfitCommonDiv);

            let maxProfitButton = document.createElement("button");
            maxProfitButton.innerText = 'Добавить ограничение максимального выигрыша';
            maxProfitButton.id = 'hideButton' + window.inputCounter;
            maxProfitButton.addEventListener('click', function (e){
                let idCounter = e.target.id.slice(10);
                let maxProfitDiv = document.getElementById("maxProfitDiv"+idCounter);
                let hideButton = document.getElementById("hideButton"+idCounter);
                let maxProfit = document.getElementById("maxProfit"+idCounter);
                if (hideButton.innerText === "Добавить ограничение максимального выигрыша"){
                    hideButton.innerText = "Убрать ограничение максимального выигрыша";
                    maxProfitDiv.style.visibility = 'visible';
                } else {
                    if (maxProfit.value !== null){
                        maxProfit.value = 0
                    }
                    hideButton.innerText = "Добавить ограничение максимального выигрыша";
                    maxProfitDiv.style.visibility = 'hidden';
                }
            });
            maxProfitCommonDiv.appendChild(maxProfitButton);

            let maxProfitDiv = document.createElement("div");
            maxProfitDiv.id = 'maxProfitDiv' + window.inputCounter;
            maxProfitDiv.style.visibility = 'hidden';
            maxProfitCommonDiv.appendChild(maxProfitDiv);

            let maxProfitLabel = document.createElement("label");
            maxProfitLabel.innerText = "Максимальное значение выигрыша одной симуляции";
            maxProfitDiv.appendChild(maxProfitLabel);

            let maxProfitInput = document.createElement("input");
            maxProfitInput.id = "maxProfit" + window.inputCounter;
            maxProfitDiv.appendChild(maxProfitInput);

            let bonusPartCommonDiv = document.createElement("div");
            advancedSettingsDiv.appendChild(bonusPartCommonDiv);

            let hideBonusButton = document.createElement("button");
            hideBonusButton.innerText = 'Бонус отыгрывается частями';
            hideBonusButton.id = 'hideBonusButton' + window.inputCounter;
            hideBonusButton.addEventListener('click', function (e){
                let idCounter = e.target.id.slice(15);
                let bonusPartDiv = document.getElementById("bonusPartDiv"+idCounter);
                let hideButton = document.getElementById("hideBonusButton"+idCounter);
                let bonusPart = document.getElementById("bonusPart"+idCounter);
                if (hideButton.innerText === "Бонус отыгрывается частями"){
                    hideButton.innerText = "Не отыгрывать бонус частями";
                    bonusPartDiv.style.visibility = 'visible';
                } else {
                    if (bonusPart.value !== null){
                        bonusPart.value = 0
                    }
                    hideButton.innerText = "Бонус отыгрывается частями";
                    bonusPartDiv.style.visibility = 'hidden';
                }
            });
            bonusPartCommonDiv.appendChild(hideBonusButton);

            let bonusPartDiv = document.createElement("div");
            bonusPartDiv.id = 'bonusPartDiv' + window.inputCounter;
            bonusPartDiv.style.visibility = 'hidden';
            bonusPartCommonDiv.appendChild(bonusPartDiv);

            let bonusPartLabel = document.createElement("label");
            bonusPartLabel.innerText = "Количество частей бонуса";
            bonusPartDiv.appendChild(bonusPartLabel);

            let bonusPartInput = document.createElement("input");
            bonusPartInput.id = "bonusPart" + window.inputCounter;
            bonusPartDiv.appendChild(bonusPartInput);

            let br1 = document.createElement("br");
            inputDiv.appendChild(br1);

            let reportPlace = document.createElement("div");
            reportPlace.id = "reportPlace" + window.inputCounter;
            inputDiv.appendChild(reportPlace);

            let waitingLabel = document.createElement("label");
            waitingLabel.id = "waitingLabel" + window.inputCounter;
            waitingLabel.innerText = "Происходит расчет. Ожидайте...";
            waitingLabel.style.display = 'none';
            reportPlace.appendChild(waitingLabel);

            let br2 = document.createElement("br");
            inputDiv.appendChild(br2);
            let br3 = document.createElement("br");
            inputDiv.appendChild(br3);

            if(window.inputCounter > 0){
                let prevInput = window.inputCounter - 1;

                depositInput.value = document.getElementById("deposit"+prevInput).value;
                bonusInput.value = document.getElementById("bonus"+prevInput).value;
                betInput.value = document.getElementById("bet"+prevInput).value;
                waggerTypeSelect.value = document.getElementById("waggerType"+prevInput).value;
                waggerInput.value = document.getElementById("wagger"+prevInput).value;
                simInput.value = document.getElementById("sim"+prevInput).value;
                withoutBonusInput.checked = document.getElementById("withoutBonus"+prevInput).checked;
                manualInput.checked = document.getElementById("manual"+prevInput).checked;
                taxesInput.value = document.getElementById("taxes"+prevInput).value;
                maxProfitInput.value = document.getElementById("maxProfit"+prevInput).value;
                bonusPartInput.value = document.getElementById("bonusPart"+prevInput).value;
            }
        }
    </script>
    <script>
        function createGame(idCounter){
            let provider = document.getElementById("provider" + idCounter);
            let container = document.getElementById("games" + idCounter);
            let gameSelect = document.getElementById("game" + idCounter);
            if (gameSelect !== null){
                gameSelect.remove();
            }
            $.ajax({
                type: 'POST',
                url: '/SlotApplication/get-games',
                data: { provider: provider.value },
                success: function(data){
                    let gamesSelect = document.createElement("select");
                    gamesSelect.setAttribute("id",  "game" + idCounter);
                    for(let game of data){
                        let option = document.createElement("option");
                        option.value = game;
                        option.text = game;
                        gamesSelect.appendChild(option);
                    }
                    container.appendChild(gamesSelect);
                }
            });
        }
    </script>
   <script>
    function addSlotInput(){
        window.inputCounter++;
        createSlotInput();
    }
   </script>
    <script>
        function perform(){
            for(let i = 0; i <= window.inputCounter; i++){
                let providerC = document.getElementById("provider"+i);
                let gameC = document.getElementById("game"+i);
                let depositC = document.getElementById("deposit"+i);
                let bonusC = document.getElementById("bonus"+i);
                let withoutBonusC = document.getElementById("withoutBonus"+i);
                let taxesC = document.getElementById("taxes"+i);
                let betC = document.getElementById("bet"+i);
                let maxProfitC = document.getElementById("maxProfit"+i);
                let waggerTypeC = document.getElementById("waggerType"+i);
                let waggerC = document.getElementById("wagger"+i);
                let bonusPartC = document.getElementById("bonusPart"+i);
                let simC = document.getElementById("sim"+i);
                let manualC = document.getElementById("manual"+i);
                let slotInputFront = {
                    deposit: depositC.value,
                    bonus: bonusC.value,
                    withoutBonus: withoutBonusC.checked,
                    taxes: taxesC.value,
                    providerFee: '0',
                    paymentFee: '0',
                    platformFee: '0',
                    bet: betC.value,
                    profitLimit: maxProfitC.value,
                    waggerType: waggerTypeC.value,
                    wagger: waggerC.value,
                    bonusParts: bonusPartC.value,
                    sim: simC.value,
                    manual: manualC.checked,
                    provider: providerC.value,
                    game: gameC.value,
                    accuracy: '95',
                    performType: '?version=admin'
                };
                document.getElementById("waitingLabel"+i).style.display='block';
                $.ajax({
                    type: 'POST',
                    url: '/SlotApplication/report',
                    data:  slotInputFront,
                    success: function(data){
                        console.log(JSON.stringify(data));
                        document.getElementById("waitingLabel"+i).style.display='none';
                        let reportPlace = document.getElementById("reportPlace"+i);
                        if (data.error !== null){
                            let evLabelPrev = document.getElementById("ev"+i);
                            if (evLabelPrev === null){
                                let errorLabel = document.createElement("label");
                                errorLabel.id = 'ev'+i;
                                errorLabel.innerText = data.error;
                                reportPlace.appendChild(errorLabel);
                            } else {
                                evLabelPrev.innerText = "EV: "+ data.commonSimInformation.ev;
                            }
                        } else{
                            let evLabelPrev = document.getElementById("ev"+i);
                            if (evLabelPrev === null){
                                let evLabel = document.createElement("label");
                                evLabel.id = 'ev'+i;
                                evLabel.innerText = "EV: "+ data.commonSimInformation.ev;
                                reportPlace.appendChild(evLabel);
                            } else {
                                evLabelPrev.innerText = "EV: "+ data.commonSimInformation.ev;
                            }
                        }
                    }
                });
            }
        }
    </script>
</head>
<body>
    <div id="menu">
        <nav>
            <ul class="menu-ul">
                <li class="menu-li"><a class="menu-a" href='/SlotApplication/'>Home</a></li>
                <li class="menu-li"><a class="menu-a" href='/SlotApplication/tools'>Tools</a></li>
                <li class="menu-li"><a class="menu-a" href='/SlotApplication/slots'>Slots</a></li>
                <li class="menu-li"><a class="menu-a" href='/SlotApplication/jackpots'>Jackpots</a></li>
            </ul>
        </nav>
    </div>
    <script>loadInputs();</script>
    <div id="inputPlace"></div>
    <script>createSlotInput();</script>
    <br><br><br>
    <button id="addSlotInput" onclick='addSlotInput()'>Добавить бонус</button><br><br>
    <button id="perform" onclick="perform()">Рассчитать</button><br>
    <button onclick="location.href='/SlotApplication/'">Вернуться в меню</button>
</body>
</html>