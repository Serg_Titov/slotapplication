<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="by.minsk.miroha.entities.front.report.SlotReportFront"%>
<%@ page import="by.minsk.miroha.entities.report.CommonSlotInformation"%>
<%@ page import="by.minsk.miroha.entities.report.Multipliers"%>
<%@ page import="by.minsk.miroha.entities.front.report.SlotPointFront"%>
<%@ page import="java.util.List"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en" pageEncoding="UTF-8">
<head>
    <meta charset="UTF-8">
    <title>Slot information</title>
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
      href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800;900&family=Oswald&display=swap"
      rel="stylesheet"
    />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
  <style>
    #menu {
        margin-left: 30%;
    }
    .menu-li {
        display: inline;
        margin: -2.5;
    }
    .menu-ul {
        list-style: none;
        margin: 0;
        padding-left: 0;
    }
    .menu-a {
        text-decoration: none;
        padding: 1em;
        background: #f5f5f5;
        border: 1px solid #b19891;
        color: #695753;
    }
    /* Works on Firefox */
    .chart-block-hid {
      scrollbar-width: thin;
      scrollbar-color: #262626 ;
    }
    /* Works on Chrome, Edge, and Safari */
    .chart-block-hid::-webkit-scrollbar {
      width: 12px;
      height: 12px;
    }
    .chart-block-hid::-webkit-scrollbar-thumb {
      background-color: rgb(255, 184, 0);
      border-radius: 10px;
      border: 4px solid #181818;
    }

    html {
          height: 100%;
          scroll-behavior: smooth;
        }
        * {
          box-sizing: border-box;
        }
        body {
          margin: 0;
          font-family: 'Montserrat', sans-serif;
          font-size: 14px;
          background-color: #181818;
        }

        /*общий grid*/
        .grid {
          display: grid;
          grid-template-columns: .5fr 1fr 2fr 1fr .5fr;
          grid-template-rows: auto 1fr auto;
          justify-content: center;
          align-content: flex-start;
          justify-items: center;
          box-sizing: border-box;
          min-height: 100%;
          min-width: 320px;
        }

        /*общий flex*/
        .flex {
          display: flex;
          justify-content: center;
          flex-flow: row wrap;
        }
        /*flex-контейнер*/
        .flex-row {
          display: flex;
          flex-flow: row nowrap;
          /*max-width: 1200px;
          margin: 0 auto;
          justify-content: center;*/
          
        }
        /*flex-контейнер*/
        .flex-column {
          display: flex;
          max-width: 1200px; /*ширина контента*/
          flex-flow: column nowrap;
          margin: 0 auto;
          align-items: center;
        }
        .hidden{
          display: none;
        }

        header {
          grid-column: 3/4;
          grid-row: 1/2;
          width: 100%;
          /*height: 100vh;*/
          color: #fff;
        }

        nav {
          width: 100%;
          box-sizing: border-box;
          display: flex;
          flex-flow: row nowrap;
          height: 80px;
          justify-content: space-around;
          transition: background-color 0.5s;
        }

        button{
          font-family: 'Montserrat', sans-serif;
        }

        ul.topnav {
          display: flex;
          list-style-type: none;
          width: 100%;
          justify-content: center;
          align-items: center;
          margin: 0;
          padding: 0;
        }

        ul.topnav li {
          text-align: center;
          background: #262626;
          border-radius: 2px 2px;
          height: auto;
          perspective: 900px;
        }
        
        /*style for rotate point of menu*/
        .roll-link {
          height: auto;
          display: block;
          width: 90px;
          transform-style: preserve-3d;
        }

        .roll-link .front {
          height: 40px;
          background: rgb(255, 184, 0);
          color: #262626;
        }

        .roll-link .back {
          opacity: 0;
          height: 40px;
          display: block;
          background: #262626;
          color: #f5f5f5;
          transform: translate3d(0, 0, -40px) rotate3d(1, 0, 0, 90deg);
        }

        .topnav li:first-child .roll-link .front,
        .topnav li:first-child .roll-link .back {
          border-radius: 5px 0px 0px 5px;
        }

        .topnav li:last-child .roll-link .front,
        .topnav li:last-child .roll-link .back {
          border-radius: 0px 5px 5px 0px;
        }

        .roll-link > a {
          display: block;
          position: absolute;
          width: 90px;
          text-decoration: none;
          padding: 1em;
          transition: all 0.3s ease-out;
          transform-origin: 50% 0%;
        }

        .topnav .roll-link:hover .front {
          transform: translate3d(0, 40px, 0) rotate3d(1, 0, 0, -90deg);
          backface-visibility: hidden;
          perspective: 1000;
          opacity: 0;
        }

        .topnav .roll-link:hover .back {
          transform: rotate3d(1, 0, 0, 0deg);
          backface-visibility: hidden;
          perspective: 1000;
          opacity: 1;
        }
        nav .btn-menu {
          display: none;
        }
        main {
          grid-column: 2/5;
          grid-row: 2/3;
          width: 100%;
        }
        #report{
          color: #f5f5f5;
        }
        .main-title{
          display: flex;
          flex-direction: column;
          align-items: center;
          color: #f5f5f5;
          margin-bottom: 50px;
          font-family: "Oswald", sans-serif;
        font-weight: normal;
        font-size: 24px;
        line-height: 34px;
        text-transform: uppercase;
        }
        .slot-label{
          color: rgb(255, 184, 0);
        }
        .wrap-report-block {
        width: 100%;
        display: flex;
        align-items: center;
        justify-content: center;
        flex-direction: column;
        margin-top: 50px;
        /*background-color: #695753;*/
        border-radius: 10px;
        padding: 5px;
        transition: 0.3s;
        /*height: 0;
    opacity: 0;*/
        pointer-events: visible;
      }
      .multi-title {
        position: relative;
        margin-bottom: 50px;
        font-family: "Oswald", sans-serif;
        font-weight: normal;
        font-size: 35px;
        line-height: 65px;
        text-transform: uppercase;
        color: #ffffff;
        /*background-color: #695753;*/
      }

      .multi-title span {
        font-family: "Oswald", sans-serif;
        color: #ffb800;
      }

      .report-block {
        /*width: calc(100% + 40px);*/
        width: 100%;
        display: flex;
        align-items: stretch;
        justify-content: center;
        /*background-color: #695753;*/
      }
      .report-block .left {
        width: 100%;
        max-width: 394px;
        margin: 0 10px;

      }
      .report-block .right {
        width: 100%;
        max-width: 855px;
        margin: 0 10px;

      }
      .report-block .item {
        padding: 15px 20px;
        background: #181818;
        border: 2px solid #333333;
        border-radius: 10px;
        margin-bottom: 20px;
        min-height: 70px;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
      }
      .report-block .item:nth-child(even) {
        transform: translateX(150%);
        animation: ani-left 1s forwards;
      }

      @keyframes ani-left {
        0% {transform: translateX(150%);}
        100% {transform: translateY(0);}
      }
      .report-block .item:nth-child(odd) {
        transform: translateX(-150%);
        animation: ani-right 1s forwards;
      }

      @keyframes ani-right {
        0% {transform: translateX(-150%);}
        100% {transform: translateY(0);}
      }
      .report-block .right .item:nth-child(1){
        min-width: 495px;
      }
      .report-block .item .title-info {
        margin: 10px 0 15px 0;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        text-align: center;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        width: 100%;
    }

      .report-block .item .title-info span {
        font-weight: 500;
        font-size: 16px;
        line-height: 22px;
        text-align: center;
        color: #ffffff;
    }

      .report-block .item.item-row {
        -webkit-box-orient: horizontal;
        -webkit-box-direction: normal;
        flex-direction: row;
        -webkit-box-align: stretch;
        align-items: stretch;
      }
      .report-block .item .item-text-row {
        width: 100%;
        display: flex;
        justify-content: space-between;
        align-items: center;
      }
      

      .report-block .item .item-text-row .title,
      .item-text-column .title {
        display: flex;
        align-items: center;
        text-align: left;
      }

      .report-block .item .item-text-row .title span,
      .item-text-column span {
        font-weight: 300;
        font-size: 16px;
        line-height: 90%;
        color: #ffffff;
      }

      .report-block .item .item-text-row p,
      .item-text-column p {
        font-weight: 600;
        font-size: 16px;
        line-height: 22px;
        text-align: right;
        color: #ffb800;
        padding-left: 5px;
      }
      .report-block .item .item-text-row p span{
        display: block;
      }
      .item-text-column{
        width: 100%;
        min-width: 170px;
        display: flex;
        flex-direction: column;
        justify-content: space-between;
        align-items: center;
      }
      .report-block .item .item-text-row-info {
        width: 100%;
        display: flex;
        -webkit-box-pack: justify;
        justify-content: space-between;
        -webkit-box-align: center;
        align-items: center;
      }

      .report-block .item .item-text-row-info .nomination {
        display: flex;
        -webkit-box-align: center;
        align-items: center;
      }

      .report-block .item .item-text-row-info .nomination span {
        font-weight: 300;
        font-size: 16px;
        line-height: 24px;
        color: rgb(255, 255, 255);
      }

      .report-block .item .item-text-row-info .nomination-value {
        display: flex;
        -webkit-box-align: center;
        align-items: center;
      }

      .report-block .item .item-text-row-info .nomination-value span {
        margin-right: 10px;
        display: block;
        font-weight: 300;
        font-size: 16px;
        line-height: 24px;
        color: rgb(255, 184, 0);
        padding-left: 10px;
      }

      .report-block .item .line {
        border: 1px solid #333333;
        margin: 0px 30px;
      }
      .report-block .item .line-row {
        margin: 28px 0;
        border: 1px solid #333333;
        width: 100%;
      }
      .report-block .item .circle-wrap,
      .report-block .item .bonus,
      .report-block .rtp {
        width: 100%;
        display: flex;
        -webkit-box-pack: center;
        justify-content: center;
        -webkit-box-align: center;
        align-items: center;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        flex-direction: column;
      }

      .report-block .item .circle-wrap .title,
      .report-block .item .bonus .title,
      .report-block .item .rtp .title {
        margin-top: 15px;
        margin-bottom: 30px;
        font-weight: 600;
        font-size: 16px;
        line-height: 22px;
        text-align: center;
        color: rgb(255, 255, 255);
      }

      .report-block .item .circle-wrap .block,
      .report-block .item .bonus .block {
        margin-bottom: 45px;
        width: 175px;
        height: 156px;
        display: flex;
        -webkit-box-pack: center;
        justify-content: center;
        -webkit-box-align: center;
        align-items: center;
        position: relative;
      }
      .report-block .item .bonus .block .speed {
        position: absolute;
        transform: rotate(-140deg);
        transform-origin: 50% 64%;
        margin-top: 0px;
        transition: transform 1s ease-in-out;
      }
      .report-block .item .bonus .block span {
        position: absolute;
        bottom: -25px;
        font-weight: 500;
        font-size: 30px;
        line-height: 37px;
        text-align: center;
        color: rgb(255, 184, 0);
      }
      .circle-wrap .block .block-circle{
        border: solid 2px rgb(255, 184, 0);
        position: relative;
        border-radius: 50% 50%; 
        width: 150px;
        height: 150px;
        align-items: center;
        box-shadow: 0 0 0 0 rgba(255, 184, 0, 1);
        transform: scale(1);
        animation: pulse 2s infinite;
      }
      @keyframes pulse {
      0% {
        transform: scale(0.95);
        box-shadow: 0 0 0 0 rgba(255, 184, 0, 0.7);
      }

      70% {
        transform: scale(1);
        box-shadow: 0 0 0 10px rgba(255, 184, 0, 0);
      }

      100% {
        transform: scale(0.95);
        box-shadow: 0 0 0 0 rgba(255, 184, 0, 0);
      }
    }
.circle-wrap .block .total-spins,
.circle-wrap .block .max-win{
        color: #f5f5f5;
        font-size: 24px;
        position: absolute;
        text-shadow: 0px 0px 20px orange;
      }

.circle_percent {
  font-size:150px; 
  width:1em; 
  height:1em; 
  position: relative; 
  background: #333333; 
  border-radius:50%; 
  overflow:hidden; 
  display:inline-block; 
  margin-bottom: 45px;
  }
  .circle_percent_value{
    color: transparent;
    font-size: 1px;
  }
.circle_inner {
  position: absolute; 
  left: 0; 
  top: 0; 
  width: 1em; 
  height: 1em; 
  clip:rect(0 1em 1em .5em);}
.round_per {
  position: absolute; 
  left: 0; top: 0; 
  width: 1em; 
  height: 1em; 
  background: rgb(255, 184, 0); 
  clip:rect(0 1em 1em .5em); 
  transform:rotate(180deg); 
  transition:1.05s;}
.percent_more .circle_inner {
  clip:rect(0 .5em 1em 0em);}
.percent_more:after {
  position: absolute; 
  left: .5em; 
  top:0em; 
  right: 0; 
  bottom: 0; 
  background: rgb(255, 184, 0); 
  content:'';}
.circle_inbox {
  position: absolute; 
  top: 10px; 
  left: 10px; 
  right: 10px; 
  bottom: 10px; 
  background: #262626; 
  z-index:3; 
  border-radius: 50%;}
.percent_text {
  position: absolute; 
  font-size: 30px; 
  left: 50%; 
  top: 50%; 
  transform: translate(-50%,-50%); 
  z-index: 3;
  color: #f5f5f5;
}
.report-block .row-item {
        width: 100%;
        margin-left: -10px;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: stretch;
        -ms-flex-align: stretch;
        align-items: stretch;
      }

      .report-block .row-item .item {
        width: 100%;
        margin: 0 10px;
        margin-bottom: 20px !important;
        padding: 30px 20px;
      }

      .report-block .item .chart-radius {
        width: 100%;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: justify;
        -ms-flex-pack: justify;
        justify-content: center;
        -webkit-box-align: stretch;
        -ms-flex-align: stretch;
        align-items: stretch;
      }

      .report-block .item .chart-radius .chart { 
        width: 100%;
        
      }

      .report-block .item .chart-radius .chart canvas {
        width: 100%;
        height: 100%;
      }

      .report-block .item .chart-radius .line {
        margin: 0 37px;
      }

      .report-block .item .chart-radius .text {
        width: auto;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        align-items: center;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        text-align: left;
      }
      .report-block .item .chart-radius .text>span {
        display: block;
        margin-bottom: 16px;
        font-weight: 500;
        font-size: 16px;
        line-height: 22px;
        color: #ffffff;
      }

      .report-block .item .chart-radius .text .plus {
        margin-bottom: 6px;
        font-weight: 500;
        font-size: 14px;
        line-height: 17px;
        color: #ffeec2;
      }
      .report-block .item .chart-radius .text .zero {
        font-weight: 500;
        font-size: 14px;
        line-height: 17px;
        color: #ffb800;
      }

      .report-block .item.chart-block-hid {
        overflow-x: hidden;
        padding: 15px 10px;
      }
      .report-block .item .wrap-chart-line {
        width: 100%;
        position: relative;
        min-height: 374px;
      }
      .report-block .item .chart-line {
        width: 100%;
      }
      .report-block .item .chart-line-blocks {
        width: 100%;
        height: 100%;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        -webkit-box-align: stretch;
        -ms-flex-align: stretch;
        align-items: stretch;
        /*position: absolute;
        left: 50%;
        top: 0;
        overflow: hidden;
        transform: translateX(-50%);*/
      }

      .report-block .item .chart-line-blocks .block-wrap {
        /*width: 648px;*/
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: end;
        -ms-flex-align: end;
        align-items: flex-end;
        -webkit-box-pack: start;
        -ms-flex-pack: start;
        justify-content: flex-start;
        position: relative;
      }
      .block-wrap .block {
        width: 100%;
        min-width: 125px;
        min-height: 355px;
        margin-right: 6px;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: justify;
        -ms-flex-pack: justify;
        justify-content: space-between;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        padding: 20px 0;
        -webkit-transition: 0.3s linear;
        transition: 0.3s linear;
        -webkit-transform: translate(0, 0);
        transform: translate(0, 0);
        position: relative;
      }
      

      .block-wrap .block::before {
        content: "";
        width: 100%;
        min-height: 355px;
        position: absolute;
        left: 0;
        bottom: 0;
        z-index: -1;
        background: #262626;
        border-radius: 10px;
        -webkit-transition: 0.3s;
        transition: 0.3s;
      }
      .report-block .item .chart-line-blocks .block-wrap .block .top {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        text-align: center;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        -webkit-transition: 0.5s;
        transition: 0.5s;
      }

      .report-block .item .chart-line-blocks .block-wrap .block:hover {
        padding-top: 5px;
      }
      .block-wrap .block .top span {
        font-weight: bold;
        font-size: 16px;
        line-height: 20px;
        text-align: center;
        color: rgba(255, 255, 255, 0.6);
        -webkit-transition: 0.3s linear;
        transition: 0.3s linear;
      }
      .report-block .item .chart-line-blocks .block-wrap .block .top p {
        font-weight: 500;
        font-size: 12px;
        line-height: 15px;
        text-align: center;
        color: rgba(255, 255, 255, 0.6);
        -webkit-transition: 0.3s linear;
        transition: 0.3s linear;
      }

      .report-block .item .chart-line-blocks .block-wrap .block:hover .top p {
        color: rgba(255, 255, 255, 1);
      }

       .block-wrap .block .bottom {
        font-weight: bold;
        font-size: 14px;
        line-height: 17px;
        text-align: center;
        color: rgba(255, 255, 255, 0.6);
        -webkit-transition: 0.3s linear;
        transition: 0.3s linear;
      }
      .report-block .item .chart-line-blocks .block-wrap .block .linear,
      .report-block .item .chart-line-blocks .block-wrap .block .linear-zero {
        width: 0;
        position: absolute;
        left: 50%;
        bottom: 20%;
        height: 2px;
        background: #ffb800;
        z-index: 6;
        -webkit-transform-origin: 0% 0%;
        transform-origin: 0% 0%;
      }
      .report-block .item .chart-line-blocks .block-wrap .block.lst {
        margin-right: 0;
      }

      .report-block .item .chart-line-blocks .block-wrap .linePos {
        position: absolute;
        bottom: 20%;
        z-index: 10;
        pointer-events: none;
        height: 6px;
        width: 6px;
        border-radius: 50%;
        background: #ffffff;
        -webkit-transition: 0.3s;
        transition: 0.3s;
        z-index: 100;
      }
      .block-wrap .block.compressed,
      .block-wrap .block.compressed::before{
        min-width: 75px;
        min-height: 255px;
      }
      .block-wrap .block.max-compressed,
      .block-wrap .block.max-compressed::before{
        min-width: 55px;
        min-height: 255px;
        margin-right: 3px;
      }
      .block-wrap .block.max-compressed .top span,
      .block-wrap .block.max-compressed .bottom {
        font-weight: 500;
        font-size: 12px;
      }

        @media (max-width: 500px) {
      header {
        grid-column: 2/5;
      }
      nav {
        flex-direction: row;
        height: auto;
        margin-top: 5px;
        justify-content: start;
      }
      nav .btn-menu {
        display: block;
        color: #f5f5f5;
        float: right;
        margin-right: 10px;
      }

      ul.topnav {
        display: none;
      }
      ul.topnav li {
        width: 90%;
        height: 40px;
      }

      .topnav li a {
        width: 100%;
        padding: 0.5em;
      }
      .topnav li:first-child .roll-link .front,
      .topnav li:first-child .roll-link .back {
        border-radius: 5px 5px 0px 0px;
      }

      .topnav li:last-child .roll-link .front,
      .topnav li:last-child .roll-link .back {
        border-radius: 0px 0px 5px 5px;
      }

      .roll-link {
        width: 100%;
      }

      .topnav.responsive {
        display: block;
      }
    }
    @media (max-width: 1040px) {
        .report-block,
        .report-block .row-item {
          flex-direction: column;
        }
      }
  </style>
  </head>
  <body>
    <%  SlotReportFront report = (SlotReportFront) request.getAttribute("report");
    %>

        <!-- Грид-контейенер общий -->
        <div class="grid">
            <!--Header-->
            <header>
              <!--блок навгации-->
              <nav class="navigation">
                <!-- Кнопка раскрытия меню -->
                <a href="javascript:void(0)" class="btn-menu">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                  >
                    <path
                      d="M3.5,7 C3.22385763,7 3,6.77614237 3,6.5 C3,6.22385763 3.22385763,6 3.5,6 L20.5,6 C20.7761424,6 21,6.22385763 21,6.5 C21,6.77614237 20.7761424,7 20.5,7 L3.5,7 Z M3.5,12 C3.22385763,12 3,11.7761424 3,11.5 C3,11.2238576 3.22385763,11 3.5,11 L20.5,11 C20.7761424,11 21,11.2238576 21,11.5 C21,11.7761424 20.7761424,12 20.5,12 L3.5,12 Z M3.5,17 C3.22385763,17 3,16.7761424 3,16.5 C3,16.2238576 3.22385763,16 3.5,16 L20.5,16 C20.7761424,16 21,16.2238576 21,16.5 C21,16.7761424 20.7761424,17 20.5,17 L3.5,17 Z"
                    />
                  </svg>
                </a>
                <ul class="topnav" id="myTopnav">
                  <li>
                    <div class="roll-link">
                      <a href="/SlotApplication/" class="front">Home</a>
                      <a href="/SlotApplication/" class="back">Home</a>
                    </div>
                  </li>
                  <li>
                    <div class="roll-link">
                      <a href="/SlotApplication/tools" class="front">Tools</a>
                      <a href="/SlotApplication/tools" class="back">Tools</a>
                    </div>
                  </li>
                  <li>
                    <div class="roll-link">
                      <a href="/SlotApplication/slots" class="front">Slots</a>
                      <a href="/SlotApplication/slots" class="back">Slots</a>
                    </div>
                  </li>
                  <li>
                    <div class="roll-link">
                      <a href="/SlotApplication/jackpots" class="front">Jackpots</a>
                      <a href="/SlotApplication/jackpots" class="back">Jackpots</a>
                    </div>
                  </li>
                  <li>
                    <div class="roll-link">
                      <a href="/SlotApplication/sites" class="front">Sites</a>
                      <a href="/SlotApplication/sites" class="back">Sites</a>
                    </div>
                  </li>
                </ul>
              </nav>
              <!--/ блок навгации-->
            </header>
            <!--/Header-->
      
            <!--main-->
            <main>
              



    <!--<div id="menu">
        <nav>
            <ul class="menu-ul">
              <li class="menu-li"><a class="menu-a" href='/SlotApplication/'>Home</a></li>
              <li class="menu-li"><a class="menu-a" href='/SlotApplication/tools'>Tools</a></li>
              <li class="menu-li"><a class="menu-a" href='/SlotApplication/slots'>Slots</a></li>
              <li class="menu-li"><a class="menu-a" href='/SlotApplication/jackpots'>Jackpots</a></li>
            </ul>
        </nav>
    </div>-->
    <div class="wrap-report-block">
      <div class="main-title">
          <div class="nomination-value">
            <span class="provider-label">${report.slotInformation.provider}</span>
          </div>
          <div class="nomination-value">
            <span class="slot-label">${report.slotInformation.slot}</span>
          </div>
        </div>
      </div>
        <div class="report-block">
              
              <div class="left">
                <div class="item">
                  <div class="item-text-row">
                    <div class="title">
                      <span>Total spins in database</span>
                    </div>
                    <p class="numeric">${report.commonSlotInformation.spinAmount}</p>
                  </div>
                </div>
                <div class="item">
                  <div class="item-text-row">
                    <div class="title">
                      <span>Max win in database</span>
                    </div>
                    <p class="numeric">${report.commonSlotInformation.maxWin}</p>
                  </div>
                </div>
                <div class="item">
                  <div class="item-text-row">
                    <div class="title">
                      <span>Бонус выпадает раз в</span>
                    </div>
                    <p><span class="roundup">${report.commonSlotInformation.bonusFrequency} </span>спинов</p>
                  </div>
                </div>
                <%      if (report.getCommonSlotInformation().getAverageBonusWin() != 0){
                  %>                            
                <div class="item">
                  <div class="item-text-row">
                    <div class="title">
                      <span>Средний выигрыш в бонусной игре</span>
                    </div>
                    <p class="roundup">${report.commonSlotInformation.averageBonusWin}</p>
                  </div>
                </div>
                <%      }
                  %>
                <div class="item">
                  <div class="rtp">
                  <div class="title">RTP</div>
                  <div class="circle_percent">
                    
                    <div class="circle_inner">
                      <div class="round_per"></div>
                    </div>
                  </div>
                </div>
                </div>
                <div class="item">
                  <div class="bonus">
                    <div class="title">Volatility</div>
                    <div class="block">
                      <img src="https://bonusev.com/images/bonus.svg" alt />
                      <div class="speed">
                        <img
                          src="https://bonusev.com/images/bonus-speed.svg"
                          alt
                        />
                      </div>
                      <span class="volatility">${report.commonSlotInformation.volatility}</span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="right">
                
                <!--<div class="item item-row">
                  <div class="circle-wrap">
                    <div class="title">Total spins in database</div>
                    <div class="block">
                      <div class="block-circle"></div>
                      <div class="total-spins numeric">${report.commonSlotInformation.spinAmount}</div>
                    </div>
                  </div>
                  <div class="circle-wrap">
                    <div class="title">Max win in database</div>
                    <div class="block">
                      <div class="block-circle"></div>
                      <div class="max-win numeric">${report.commonSlotInformation.maxWin}</div>
                    </div>
                  </div>
                </div>-->
      

              <div class="item">
                <div class="chart-radius">
                  <div class="chart">
                    <div class="text">
                      <span>
                        Распределение выигрышей
                      </span>
                      </div>
                    <canvas id="chart-pie"></canvas>
                  </div>
                  <!--<div class="line"></div>
                  <div class="text">
                    <span>
                      Распределение выигрышей
                    </span>
                    <div class="plus bonus-wins-rate">Доля выигрышей с бонусами: <span class="roundup">${report.commonSlotInformation.bonusWinsRate}</span></div>
                    <div class="zero wins-without-bonus-rate">Доля выигрышей без бонусов: <span class="roundup">${report.commonSlotInformation.winsWithoutBonusRate}</span></div>
                  </div>-->
                </div>
              </div>

              


              <div class="item chart-block-hid">
                <div class="title-info">
                  <span> Частота выпадения множителей в базе данных </span>
                  
                </div>
                <div class="wrap-chart-line">
                  <div class="chart-line">
                    <div class="chart-line-blocks">
                      <div class="block-wrap">
                        <%      for(Multipliers multiplier : report.getMultipliers()){
                          %>          <div class="block blockDM">
                            <div class="top">
                              <span class="multipliers-frequency"><%=multiplier.getMultiplierCount()%></span>
                              <p>спинов</p>
                            </div>
                            <div class="bottom multiplier"><%=multiplier.getMultiplier()%></div>
                            <div class="linear"></div>
                          </div> 
                          <%      }
                          %>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="item">
                <div class="title-info">
                  <span>  Вероятность остаться в плюсе </span>
                </div>
                <div>
                  <canvas id="myChart" width="600" height="400"></canvas>
                </div>
              </div>
             
            </div>
            


    

    </div>
    <div hidden id="report">
      <label id="slotLabel">Слот: ${report.slotInformation.slot}</label><br>
      <label id="providerLabel">Провайдер: ${report.slotInformation.provider}</label><br><br>
      <label id="totalSpins">Total spins in database: ${report.commonSlotInformation.spinAmount}</label><br><br>
      <label id="maxWin">Max win in database: ${report.commonSlotInformation.maxWin}</label><br>
      <label id="rtp">RTP: <span>${report.commonSlotInformation.rtp}</span></label><br>
      <label id="bonusVolatility">Volatility: ${report.commonSlotInformation.volatility}</label><br>
      <label id="bonusFrequency">Бонус выпадает раз в ${report.commonSlotInformation.bonusFrequency} симуляций</label><br>
      <label id="bonusWinsRate">Доля выигрышей с бонусами:<span> ${report.commonSlotInformation.bonusWinsRate}</span></label><br>
      <label id="winsWithoutBonusRate">Доля выигрышей без бонусов: <span>${report.commonSlotInformation.winsWithoutBonusRate}</span></label><br>
<%      if (report.getCommonSlotInformation().getAverageBonusWin() != 0){
%>          <label id="averageBonusWin">Средний выигрыш в бонусной игре: ${report.commonSlotInformation.averageBonusWin}</label><br>
<%      }
%>
      <br><label id="multipliersFrequency">Частота выпадения множителей в базе данных:</label><br>
<%      for(Multipliers multiplier : report.getMultipliers()){
%>          <label>Множитель больше <%=multiplier.getMultiplier()%>: раз в <%=multiplier.getMultiplierCount()%> симуляций.</label><br>
<%      }
%>
<br>
      <label id="slotPointsLabel">Для данного слота следующие точки: </label><br>
<%      for(SlotPointFront point : report.getSlotPoints()){
%>          <label class="spin-limit"> <%=point.getSpinLimit()%> </label>; <label class="result"> <%=point.getPointResult()%> </label><br>
<%      }
%>
  
    </div>
    </div>


</main>
<!-- /main -->

<!-- footer -->
<footer></footer>
<!-- /footer -->
</div>
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>


<script>
  //slotPointsLabel
  let spinLimits = document.querySelectorAll('.spin-limit');//SpinLimit = 
  let results = document.querySelectorAll('.result');//Result = 
  const labelsX = [];
  const labelsY = [];
  labelsXScale = [];
  labelsYScale = [];
  spinLimits.forEach((item) => {
    labelsX.push(parseFloat(item.textContent));
  })
  results.forEach((item) => {
    labelsY.push(parseFloat((item.textContent).replace(/\s/g, "").replace(",", ".")));
  })
  
  labelsX.forEach((item) => {
     let counter = (item + '').split('').reduce((x, y) => x + (y === '0'), 0);
     if (counter >= 3 && counter < 6){
       item = item + '';
       item = item.slice(0, -3);
       item = item + 'K';
     }
     if (counter >= 6){
      item = item + '';
       item = item.slice(0, -6);
       item = item + 'M';
     }
     labelsXScale.push(item);
  });


  const data = {
    labels: labelsXScale,
    datasets: [{
      label: '',
      backgroundColor: '#262626',
      borderColor: 'rgb(255, 184, 0)',
      data: labelsY,
     
    }]
  };
  const chartOptions = {
    plugins: {
      legend: {
        display: false
      },
      tooltip: {
        titleColor: 'rgb(255, 184, 0)',
        displayColors: false,
        borderWidth: 10,
                callbacks: {
                  title: function(context) {
                    return 'SpinLimit ' + context[0].label;
                },
                 label: function(context){
                  let label = context.dataset.label || '';

                        if (label) {
                            label += ': ';
                        }
                        if (context.parsed.y !== null) {
                            label += 'Result ' + context.parsed.y + '%';
                        }

                        return label;
                 },
                 labelTextColor: function(context) {
                        return '#f5f5f5';
                    }
                  }
                }
    },
    scales: {
      x: {
        title: {
          display: true,
          text: 'SpinLimit'
        }, 
      },
      y: {
        title: {
          display: true,
          text: 'Result %'
        },
        ticks: {
            callback: function(value){return value + "%"}
         }, 
      },
    },
  }

  const config = {
    type: 'line',
    data: data,
    options: chartOptions,
  };

  const myChart = new Chart(
    document.getElementById('myChart'),
    config
  );

  //bonus-volatility
    const volatility = document.querySelector(".volatility").textContent;
  const speed = document.querySelector(".speed");
  setTimeout(() => {
  if (volatility <= 100){
        speed.style.transform =
          "rotate(" +
          (-140 +
            (280 / 100) *
              parseFloat(volatility)) +
          "deg)";
        } else {
          speed.style.transform =
          "rotate(" +
          (-140 +
            (280 / 100) *
              100) +
          "deg)";
        }
      },1000);


      
      
      //rtp
      $(".circle_percent").each(function() {
        let $this = $(this),
        $value = parseFloat(($('#rtp span').text()).replace(/\s/g, "").replace(",", "."));
        console.log(parseFloat(($('#rtp span').text()).replace(/\s/g, "").replace(",", ".")));
        $dataDeg = $value * 3.6,
        $round = $this.find(".round_per");
        $round.css("transform", "rotate(" + parseInt($dataDeg + 180) + "deg)");	
        $this.append('<div class="circle_inbox"><span class="percent_text"></span></div>');
        $this.prop('Counter', 0).animate({Counter: $value},
        {
          duration: 2000, 
          easing: 'swing', 
          step: function () {
                  $this.find(".percent_text").text(this.Counter.toFixed(2) +"%");
              }
          });
        if($value >= 51){
          $round.css("transform", "rotate(" + 360 + "deg)");
          setTimeout(function(){
            $this.addClass("percent_more");
          },1000);
          setTimeout(function(){
            $round.css("transform", "rotate(" + parseInt($dataDeg + 180) + "deg)");
          },1000);
        } 
      });

    
            

        //Увеличить депозит в Х раз

        const aarDepositMultipliers = document.querySelectorAll('.multipliers-frequency');
        const blockDM = document.querySelectorAll('.blockDM');
        let dataDM = [];
        blockDM.forEach((elem,i,arr) => {
          let DMValue = +elem.querySelector('.multipliers-frequency').textContent;
          let DMMultiplier = +elem.querySelector('.multiplier').textContent;
          dataDM.push( {
          sim: DMValue,
          dep: DMValue,
          depLine: DMMultiplier,
          linePos:60
          });
        })
        console.log(dataDM);
        if(dataDM.length > 6){
          for (let i = 0; i<dataDM.length; i++){
          console.log(dataDM[i].dep);
          console.log(dataDM[dataDM.length -1].dep);
          dataDM[i].dep = dataDM[i].dep ? (dataDM[i].dep*100/dataDM[dataDM.length -1].dep) : undefined;
        }
        } else {
          for (let i = 0; i<dataDM.length; i++){
            console.log(dataDM[i].dep);
            console.log(dataDM[dataDM.length -1].dep);
            dataDM[i].dep = dataDM[i].dep ? (dataDM[i].dep*200/dataDM[dataDM.length -1].dep) : undefined;
          }
        }
        console.log(dataDM);


        let arrFrecuency = [];
        aarDepositMultipliers.forEach((item) => {
          let formatItem = +item.textContent;
          arrFrecuency.push(formatItem);
        })    
        const dataBlocksValue = [
          {
            sim: arrFrecuency[0] ,
            dep: arrFrecuency[0] ? (arrFrecuency[4] ? arrFrecuency[0]*100/arrFrecuency[4] : (arrFrecuency[3] ? arrFrecuency[0]*100/arrFrecuency[3] : (arrFrecuency[2] ? arrFrecuency[0]*100/arrFrecuency[2] : (arrFrecuency[1] ? arrFrecuency[0]*100/arrFrecuency[1] : 120)))) : undefined,
            depLine: 10,
            linePos: 77,
          },
          {
            sim: arrFrecuency[1] ,
            dep: arrFrecuency[1] ? (arrFrecuency[4] ? arrFrecuency[1]*100/arrFrecuency[4] +10 : (arrFrecuency[3] ? arrFrecuency[1]*100/arrFrecuency[3] + 10 : (arrFrecuency[2] ? arrFrecuency[1]*100/arrFrecuency[2] + 10 : 120))) : undefined,
            depLine: 25,
            linePos: 50,
          },
          {
            sim: arrFrecuency[2] ,
            dep: arrFrecuency[2] ? (arrFrecuency[4] ? arrFrecuency[2]*100/arrFrecuency[4] + 15 : (arrFrecuency[3] ? arrFrecuency[2]*100/arrFrecuency[3] + 15 : 120)) : undefined,
            depLine: 50,
            linePos: 55,
          },
          {
            sim: arrFrecuency[3] ,
            dep: arrFrecuency[3] ? (arrFrecuency[4] ? arrFrecuency[3]*100/arrFrecuency[4] + 20 :  120) : undefined,
            depLine: 100,
            linePos: 55,
          },
          {
            sim: arrFrecuency[4], 
            dep: arrFrecuency[4] ? 120 : undefined,
            depLine: 500,
            linePos: 60,
          },
        ];
        const chartLineBlocks = document.querySelector(
        ".chart-line-blocks .block-wrap"
      );

      const charLineFunc = (dataBlocks) => {
        if (chartLineBlocks) {
          chartLineBlocks.innerHTML = "";

          let countLength = 0;

          dataBlocks.forEach((item, index, arr) => {
            countLength = arr.length - index;

            if (arr.length > 6){
              document.querySelector('.wrap-chart-line').style.minHeight = '274px';
            }
            if(arr.length > 10){
              document.querySelector('.right').style.maxWidth = '890px';
            }
            if(arr.length > 15){
              document.querySelector('.chart-block-hid').style.overflowX = 'scroll';
              document.querySelector('.chart-line-blocks').style.justifyContent = 'flex-start';
            }
            console.log(dataBlocks);
            if (item.sim != undefined){
            const element =
              '<div class="block ' +
              (arr.length - 1 === index ? "lst" : "") + " " +
              (arr.length > 6 && arr.length <= 10 ? "compressed" : "") +
              (arr.length > 10  ? "max-compressed" : "") +
              '" style="z-index:' +
              countLength +
              ';"><div class="top"><span>' +
              (item.sim == 0 || item.sim == "166 666" ? "-" : item.sim) +
              '</span><p>спинов</p></div><div class="bottom">x' +
              item.depLine +
              '</div><div class="linear"></div></div>';
            chartLineBlocks.insertAdjacentHTML("beforeend", element);
              }
          });

          chartLineBlocks
            .querySelectorAll(".block")
            .forEach((item, index, arr) => {
              countLength = arr.length - index;
              const elementPos =
                '<div class="linePos" style="left:' +
                (item.offsetLeft + item.offsetWidth / 2 - 3) +
                "px; bottom: calc((20% + " +
                dataBlocks[index].dep +
                'px) - 2px);"></div>';
              chartLineBlocks.insertAdjacentHTML("beforeend", elementPos);
            });

          const linePosItems = document.querySelectorAll(".linePos");
          const topItems = document.querySelectorAll(".top span");
          const bottomItems = document.querySelectorAll(".bottom");

          chartLineBlocks.querySelectorAll(".block").forEach((el, index) => {
            el.addEventListener("mouseenter", () => {
              linePosItems[index].style.transform = "scale(2)";
              linePosItems[index].style.background = "#FFB800";
              topItems[index].style.color = "#FFB800";
              bottomItems[index].style.color = "#FFB800";
            });
            el.addEventListener("mouseleave", () => {
              linePosItems[index].style.transform = "scale(1)";
              linePosItems[index].style.background = "#fff";
              topItems[index].style.color = "rgba(255, 255, 255, 0.6)";
              bottomItems[index].style.color = "rgba(255, 255, 255, 0.6)";
            });
          });

          linePosItems.forEach((item, index, arr) => {
            let dot1 = linePosItems[index];
            let dot2 = linePosItems[index + 1] ? linePosItems[index + 1] : null;

            if (dot1 && dot2) {
              let choords1 = dot1.getBoundingClientRect();
              let choords2 = dot2.getBoundingClientRect();

              // get choords
              let x1 = choords1.left;
              let y1 = choords1.top;
              let x2 = choords2.left;
              let y2 = choords2.top;

              // line width
              let line = document.querySelectorAll(".linear")[index];
              const calcPos = dataBlocks[index].dep;

              line.style.bottom = "calc(20% + " + calcPos + "px)";
              let hypotenuse = getChoordsWidth(x1, y1, x2, y2);

              let leg = x2 - x1;
              let angle = Math.acos(leg / hypotenuse);

              line.style.width = hypotenuse + "px";
              line.style.transform =
                dot1.offsetTop > dot2.offsetTop
                  ? "rotate(-" + angle + "rad)"
                  : "rotate(" + angle + "rad)";

              function getChoordsWidth(x1, y1, x2, y2) {
                let a1 = Math.pow(x2 - x1, 2);
                let a2 = Math.pow(y2 - y1, 2);
                let res = a1 + a2;

                return Math.sqrt(res);
              }
            }
          });
        }
      };
        
        //charLineFunc(dataBlocksValue);
        charLineFunc(dataDM);
    
        //Распределение выигрышей chart-pie

const plusValue = parseFloat(document.querySelector('#bonusWinsRate span').textContent).toFixed(2);
        const zeroValue = parseFloat(document.querySelector('#winsWithoutBonusRate span').textContent).toFixed(2);
        console.log(plusValue, zeroValue);



        const dataPie = {
  labels: [
    'Доля выигрышей с бонусами',
    'Доля выигрышей без бонусов',
  ],
  datasets: [{
    label: 'Распределение выигрышей',
    data: [plusValue, zeroValue],
    backgroundColor: [
      '#ffeec2',
      '#ffb800',
    ],
    borderAlign: 'center',
    borderWidth: 0,
    
    borderColor:'#181818',
    hoverOffset: 3
  }]
};
const pieOptions = {
  layout: {
    padding:10
  },
  //responsive: true,
  //maintainAspectRatio: true,
  aspectRatio: 4,
  plugins: {
    legend: {
      position: 'right',
      align: 'center',
      //maxWidth: 500,
      labels: {
        boxWidth: 20,
        generateLabels: (chart) => {
          let data = chart.data;
        if (data.labels.length && data.datasets.length) {
          return data.labels.map(function(label, i) {
            let meta = chart.getDatasetMeta(0);
            let ds = data.datasets[0];
            let fill = ds.backgroundColor[i];
            let stroke = ds.borderColor[i];
            let bw = ds.borderWidth[i];
              return {
              // And finally : 
              text: label + ' ' + ds.data[i] + '       ',
              fillStyle: fill,
              strokeStyle: stroke,
              lineWidth: bw,
              hidden: isNaN(ds.data[i]) || meta.data[i].hidden,
              index: i
            };
          });
        }
        return [];
        },
                  font: {
                      size: 14,
                      color: '#ffffff',
                      weight: '500',
                  }
              }
    }
  },
}

const configPie = {
  type: 'doughnut',
  data: dataPie,
  options: pieOptions,
};
const myPie = new Chart(
    document.getElementById('chart-pie'),
    configPie
  );
  Chart.defaults.font.family = "Montserrat";
 
  


      //форматирование чисел
      $('.numeric').each(function(){
        $this = (parseFloat($(this).text())).toLocaleString("ru-RU");
        $(this).text($this);
      });

      $('.roundup').each(function(){
        $this = (parseFloat($(this).text())).toFixed(2);
        $(this).text($this);
      });
    // открытие меню навигации
      const btnMenu = document.querySelector('.btn-menu')
      btnMenu.addEventListener('click', toggleMenu)

      function toggleMenu() {
        var x = document.getElementById("myTopnav");
        if (x.className === "topnav") {
          x.className += " responsive";
        } else {
          x.className = "topnav";
        }
      }
    </script>
  </body>
</html>