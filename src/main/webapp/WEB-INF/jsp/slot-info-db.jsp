<%@ page import="by.minsk.miroha.entities.front.SlotInputFront" %>
<%@ page import="by.minsk.miroha.report.SlotInputReport"%>
<%@ page import="by.minsk.miroha.entities.report.TopProfits"%>
<%@ page import="by.minsk.miroha.entities.report.Multipliers"%>
<%@ page import="by.minsk.miroha.entities.report.DepositMultiplier"%>
<%@ page import="by.minsk.miroha.entities.report.NonProfitSeries"%>
<%@ page import="by.minsk.miroha.entities.report.SlotPoint"%>
<%@ page import="by.minsk.miroha.entities.SlotInputError" %>
<%@ page import="by.minsk.miroha.entities.common.Provider"%>
<%@ page import="java.nio.charset.StandardCharsets" %>
<%@ page import="by.minsk.miroha.entities.PairProfit"%>
<%@ page import="by.minsk.miroha.entities.PairSequence"%>
<%@ page import="by.minsk.miroha.entities.enums.WaggerType"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Map"%>
<%@ page import="java.util.Set"%>
<%@ page import="java.util.Collections"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html lang="en" pageEncoding="UTF-8">
<head>
    <meta charset="UTF-8">
    <title>Информация о слоте</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <style>
        #menu {
          position: absolute;
          top: 2em;
          margin-left: 30%;
        }
        .menu-li {
          display: inline;
          margin: -2.5;
        }
        .menu-ul {
          list-style: none;
          margin: 0;
          padding-left: 0;
        }
        .menu-a {
          text-decoration: none;
          padding: 1em;
          background: #f5f5f5;
          border: 1px solid #b19891;
          color: #695753;
        }
    
        #inputContainer {
          display: flex;
          flex-direction: column;
          justify-content: space-between;
        }

        /*#advanceSettings{
          display: none;
        }*/

        .prompt{
          display: none;
        }

        .prompt-label:hover + .prompt {
          display: block;
        }
        
        .text-input{
          text-align: right
        }

        html {
          height: 100%;
          scroll-behavior: smooth;
        }
        * {
          box-sizing: border-box;
        }
        body {
          margin: 0;
          font-family: 'Montserrat', sans-serif;
          font-size: 14px;
          background-color: #181818;
        }

        /*общий grid*/
        .grid {
          display: grid;
          grid-template-columns: 0.5fr 1fr 2fr 1fr 0.5fr;
          grid-template-rows: auto 1fr auto;
          justify-content: center;
          align-content: flex-start;
          justify-items: center;
          box-sizing: border-box;
          min-height: 100%;
          min-width: 320px;
        }

        /*общий flex*/
        .flex {
          display: flex;
          justify-content: center;
          flex-flow: row wrap;
        }
        /*flex-контейнер*/
        .flex-row {
          display: flex;
          flex-flow: row wrap;
          max-width: 1200px;
          margin: 0 auto;
          justify-content: center;
        }
        /*flex-контейнер*/
        .flex-column {
          display: flex;
          max-width: 1200px; /*ширина контента*/
          flex-flow: column nowrap;
          margin: 0 auto;
          align-items: center;
        }
        

        header {
          grid-column: 3/4;
          grid-row: 1/2;
          width: 100%;
          /*height: 100vh;*/
          color: #fff;
        }

        nav {
          width: 100%;
          box-sizing: border-box;
          display: flex;
          flex-flow: row nowrap;
          height: 80px;
          justify-content: space-around;
          transition: background-color 0.5s;
        }

        button{
          font-family: 'Montserrat', sans-serif;
        }

        ul.topnav {
          display: flex;
          list-style-type: none;
          width: 100%;
          justify-content: center;
          align-items: center;
          margin: 0;
          padding: 0;
        }

        ul.topnav li {
          text-align: center;
          background: #262626;
          border-radius: 2px 2px;
          height: auto;
          perspective: 900px;
        }
        
        /*style for rotate point of menu*/
        .roll-link {
          height: auto;
          display: block;
          width: 90px;
          transform-style: preserve-3d;
        }

        .roll-link .front {
          height: 40px;
          background: rgb(255, 184, 0);
          color: #262626;
        }

        .roll-link .back {
          opacity: 0;
          height: 40px;
          display: block;
          background: #262626;
          color: #f5f5f5;
          transform: translate3d(0, 0, -40px) rotate3d(1, 0, 0, 90deg);
        }

        .topnav li:first-child .roll-link .front,
        .topnav li:first-child .roll-link .back {
          border-radius: 5px 0px 0px 5px;
        }

        .topnav li:last-child .roll-link .front,
        .topnav li:last-child .roll-link .back {
          border-radius: 0px 5px 5px 0px;
        }

        .roll-link > a {
          display: block;
          position: absolute;
          width: 90px;
          text-decoration: none;
          padding: 1em;
          transition: all 0.3s ease-out;
          transform-origin: 50% 0%;
        }

        .topnav .roll-link:hover .front {
          transform: translate3d(0, 40px, 0) rotate3d(1, 0, 0, -90deg);
          backface-visibility: hidden;
          perspective: 1000;
          opacity: 0;
        }

        .topnav .roll-link:hover .back {
          transform: rotate3d(1, 0, 0, 0deg);
          backface-visibility: hidden;
          perspective: 1000;
          opacity: 1;
        }

        nav .btn-menu {
          display: none;
        }

        main {
          grid-column: 2/5;
          grid-row: 2/3;
          width: 100%;
        }

        .wrapper{
          display: flex;
          justify-content: center;
        }
        .calculate-block {
          width: 100%;
          max-width: 700px;
          padding: 50px;
          /*background: #181818;*/
          border: 2px solid rgb(51, 51, 51);
          border-radius: 20px;
          margin-bottom: 10px;
          margin-top: 50px;
        }
        .calculate-block .form {
          width: 100%;
        }
        .calculate-block .form .form-drop {
          width: 100%;
          display: -webkit-box;
          display: -ms-flexbox;
          display: flex;
          -webkit-box-pack: justify;
          -ms-flex-pack: justify;
          justify-content: space-between;
          -webkit-box-align: center;
          -ms-flex-align: center;
          align-items: center;
          margin-bottom: 10px;
          transition: all 0.4s ease;
        }

        .calculate-block .form .dropdown-settings .form-drop{
          justify-content: flex-start;
        }
        .calculate-block .form .form-drop .form-drop-name {
          font-weight: normal;
          font-size: 16px;
          line-height: 22px;
          color: rgb(255, 255, 255);
          transition: all 0.4s ease;
          flex: 4;
        }
        .bonus-wrap{
          max-height: 0px;
          overflow: hidden;
        }
        .bonus-wrap .btn{
          background-color: #181818;
    border: solid 2px;
    border-color: rgb(255, 184, 0);
    color: #f5f5f5;
        }
        .calculate-block .form .form-drop .drop-wrap,
        .bonus-link .drop-wrap{
          min-width: 290px;
          min-height: 50px;
          height: 100%;
          position: relative;
        }
        .calculate-block .form .form-drop .block-value,
        .stopwatch {
          min-width: 290px;
          display: -webkit-box;
          display: -ms-flexbox;
          display: flex;
          -webkit-box-pack: justify;
          -ms-flex-pack: justify;
          justify-content: space-between;
          -webkit-box-align: center;
          -ms-flex-align: center;
          align-items: center;
          background: #262626;
          border-radius: 25px;
          padding: 14px 25px;
          transition: all 0.4s ease;
          cursor: pointer;
        }
        .calculate-block .form .dropdown-settings .form-drop .block-value{
          background: #181818;
          min-width: 200px;
          margin: 0px 10px;
        }

        .block-value.warning{
          border: solid 1px red;
        }
        .dropdown-settings .i-info{
          padding: 0;
        }
        .calculate-block .form .form-drop .block-value input,
        .stopwatch input{
          width: 100%;
          background-color: transparent;
          border: none;
          font-weight: normal;
          font-size: 16px;
          line-height: 22px;
          color: rgb(255, 255, 255);
          outline: none;
          text-align: start;
          font-family: 'Montserrat', sans-serif;
        }
        .calculate-block .form .form-drop .block-value input:-webkit-autofill {
          transition: all 5000s ease-in-out 0s;
        }

        .calculate-block .form .form-drop .drop,
        .bonus-link .drop {
          min-width: 290px;
          position: absolute;
          display: -webkit-box;
          display: -ms-flexbox;
          display: flex;
          justify-content: flex-end;
          align-items: center;
          padding: 0px 25px 0px 0px;
          flex-direction: row;
          border-radius: 25px;
          background: #262626;
          overflow: hidden;
          -webkit-transition: 0.3s;
          transition: 0.3s;
          color: #f5f5f5;
          height: 46.5px;
        }
        .calculate-block .form .form-drop .drop .drop-top,
        .bonus-link .drop-top {
          width: 100%;
          display: -webkit-box;
          display: -ms-flexbox;
          display: flex;
          -webkit-box-pack: justify;
          -ms-flex-pack: justify;
          justify-content: space-between;
          -webkit-box-align: center;
          -ms-flex-align: center;
          align-items: center;
          cursor: pointer;
          padding: 14px 25px;
          -webkit-transition: 0.3s;
          transition: 0.3s;
          max-height: 50px;
          background-color: transparent;
          outline: none;
          border: none;
          font-family: 'Montserrat', sans-serif;
          color: #f5f5f5;
        }
        .bonus-link .drop-top option{
          background-color: #262626;
        }
        .calculate-block .form .form-drop .drop.active .drop-content {
          padding: 14px 25px;
          height: 100%;
          min-height: 160px;
          max-height: 160px;
        }

        .calculate-block .form .form-drop .drop .drop-content {
          width: 100%;
          height: 0;
          min-height: 0;
          max-height: 0;
          overflow: hidden;
          overflow-y: auto;
          padding: 0 25px;
          position: relative;
          left: 0;
          top: 100%;
          z-index: 2;
          -webkit-transition: 0.3s;
          transition: 0.3s;
        }
        .calculate-block .form .form-drop .drop  .item,
        .bonus-link .item {
          cursor: pointer;
          margin-bottom: 14px;
          font-weight: 500;
          font-size: 16px;
          line-height: 22px;
          color: #f5f5f5;
          background-color: #262626;
          -webkit-transition: 0.3s linear;
          transition: 0.3s linear;
          text-align: left;
        }
        .calculate-block .form .btn {
          margin: 20px 0px;
          width: 100%;
          padding: 20px;
          font-size: 16px;
        }
        .input-settings{
          position: relative;
        }
        .btn {
          display: flex;
          -webkit-box-align: center;
          -webkit-box-pack: center;
          justify-content: center;
          padding: 18px 30px;
          align-items: center;
          font-weight: 600;
          font-size: 14px;
          line-height: 17px;
          text-transform: uppercase;
          color: rgb(34, 34, 34);
          background: rgb(255, 184, 0);
          border-radius: 50px;
          transition: all 0.3s ease 0s;
          cursor: pointer;
          border: none;
        }
        .btn:disabled{
          opacity: .3;
        }
        .btn-set,
        .btn-bonus{
          display: flex;
          width: 100%;
          -webkit-box-align: center;
          -webkit-box-pack: center;
          justify-content: center;
          padding: 20px 30px;
          align-items: center;
          font-weight: 400;
          font-size: 16px;
          line-height: 17px;
          color: #f5f5f5;
          background: #181818;
          border-radius: 20px 20px 20px 20px;
          transition: all 0.3s ease 0s;
          cursor: pointer;
          border: none;
        }
        .btn-set:hover, .btn-set.active,
        .btn-bonus:hover{
          background-color: rgb(255, 184, 0);
          color: #262626;
          font-weight: 600;
        }
        .btn-set.active{
          border-radius: 20px 20px 0px 0px;
        }
        .btn-set:before {
          position: absolute;
          display: block;
          content: "❯";
          font-size: 1em;
          color: #fff;
          top: 20px;
          right: 100px;
          transform: rotate(0deg);
          transition: all 0.35s;
        }
        .btn-set:hover:before{
          color: #262626;
        }
        .btn-set.active:before{
          transform: rotate(90deg);
          color: #262626;
        }
        .btn-ev-perform{
          background-color: transparent;
          border: solid 2px rgb(255, 184, 0);
          color: rgb(255, 184, 0);
        }
        .dropdown-settings{
          max-height: 0;
          padding: 0 1em;
          color: #fff;
          background: #262626;
          transition: all 0.5s ease;
          border-radius: 0px 0px 20px 20px;
          overflow: hidden;
        }
        .dropdown-settings.active, 
        .bonus-wrap.active{
          max-height: 100vh;
          padding: 20px 10px 10px 10px;
        }

        input[type="checkbox"] { display: none; }

        input[type="checkbox"] + label {
          display: block;
          position: relative;
          padding-left: 35px;
          margin-bottom: 20px;
          /*font: 14px/20px 'Open Sans', Arial, sans-serif;*/
          color: #ddd;
          cursor: pointer;
          flex: 1;
        }
        input[type="checkbox"] + label:before {
          content: '';
          display: block;
          width: 10px;
          height: 10px;
          border: 2px solid rgb(255, 184, 0);
          position: absolute;
          left: 0;
          top: 5px;
          opacity: .6;
          -webkit-transition: all .12s, border-color .08s;
          transition: all .12s, border-color .08s;
        }
        input[type="checkbox"]:checked + label:before {
          width: 6px;
          top: 0px;
          left: 5px;
          border-radius: 0;
          opacity: 1;
          border-top-color: transparent;
          border-left-color: transparent;
          -webkit-transform: rotate(45deg);
          transform: rotate(45deg);
        }    
        .check{
          display: flex;
          flex-flow: row nowrap;
          justify-content: space-between;
          width: 70%;
        }
        .accordion {
          width: 100%;
          /*max-width: 100%;*/
          margin: 0 auto;
          padding: 10px;
        }

        .accordion-item {
          position: relative;
        }
        .accordion-item .heading.active {
          color: #fff;
          opacity: 1;
        }
        .accordion-item .active .icon {
          background: #262626;
        }
        .accordion-item .active .icon:before {
          background: rgb(255, 184, 0);
        }
        .accordion-item .heading.active .icon:after {
          width: 0;
        }
        .accordion-item .heading {
          display: block;
          text-decoration: none;
          background-color: #262626;
          color: #f5f5f5;
          font-weight: 400;
          font-size: 16px;
          position: relative;
          padding: 10px 0px 10px 20px;
          transition: 0.3s ease-in-out;
          border: none;
          opacity: .5;
          text-align: start;
        }

        .accordion-item .heading:hover {
          color: #fff;
          opacity: 1;
        }
        .accordion-item .heading:hover .icon:before, 
        .accordion-item .heading:hover .icon:after {
          background:rgb(255, 184, 0);
        }
        .accordion-item .icon {
          display: block;
          position: absolute;
          top: 50%;
          left: 0;
          width: 2px;
          height: 10px;
          /*border: 2px solid rgb(255, 184, 0);*/
          /*border-radius: 3px;*/
          transform: translateY(-50%);
          font-size: 14px;
        }
        .accordion-item .icon:before, 
        .accordion-item .icon:after {
          content: "";
          width: 10px;
          height: 2px;
          background: rgb(255, 184, 0);
          position: absolute;
          /*border-radius: 3px;*/
          left: 50%;
          top: 50%;
          transition: 0.3s ease-in-out;
          transform: translate(-50%, -50%);
        }
        .accordion-item .icon:after {
          transform: translate(-50%, -50%) rotate(90deg);
          z-index: -1;
        }
        .form .accordion-item .content {
          max-height: 0;
          overflow: hidden;
        }
        .calculate-block .form .accordion-item .content label{
          font-size: 14px;
        }
        .form .accordion-item .heading.active + .content {
          max-height: 100vh;
        }
        .bonus-link div{
          margin: 10px 0px;
        }
        .bonus-link .btn-bonus{
          width: 60%;
          margin-top: 10px;
        }
        .bonus-link label{
          color: #f5f5f5;
          margin-top: 10px;
        }
        .error-message-div{
          margin-top: -25px;
          padding-bottom: 25px;
        }
        .message{
          color:#f5f5f5;
          font-weight: 500;
          font-size: 16px;
        }
        .error-message{
          color:#ff0000;
          font-weight: 500;
          font-size: 16px;
        }
        .btn-back{
          padding: 15px 40px;
          background: transparent;
          border:none;
          color: #f5f5f5;
          font-size: 14px;
          position: relative;
          transition: all .4s cubic-bezier(0.645, 0.045, 0.355, 1);
          cursor: pointer;
          display: block;
          margin: 10px auto;
          border-radius: 50px;
          font-family: 'Montserrat', sans-serif;
          font-weight: 500;
        }

        .btn-back::after,
        .btn-back::before{
          content: "";
          position: absolute;
          top: 50%;
          right: 0px;
          transform: translateY(-50%);
          opacity: 0;
          transition: all .4s cubic-bezier(0.645, 0.045, 0.355, 1);
        }

        .btn-back::after{
          width: 20px;
          height: 2px;
          background: #262626;
          transform: translateX(-3px);
          margin-top: 0px;
        }

        .btn-back::before{
          content: "";
          transform: rotate(-135deg) translateX(50%);
          width: 8px;
          height: 8px;
          background: transparent;
          border-left: 2px solid #262626;
          border-bottom: 2px solid #262626;
          margin-top: -.5px;
          margin-right: -1px;
        }


        .btn-back:hover{
          padding: 15px 60px 15px 20px;
          background-color: rgb(255, 184, 0);
          color: #262626;
        }

        .btn-back:hover:after,
        .btn-back:hover:before{
          opacity: 1;
          right: 15px;
        }


        .wrap-report-block {
        width: 100%;
        display: flex;
        align-items: center;
        justify-content: center;
        flex-direction: column;
        margin-top: 50px;
        /*background-color: #695753;*/
        border-radius: 10px;
        padding: 5px;
        transition: 0.3s;
        /*height: 0;
    opacity: 0;*/
        pointer-events: visible;
      }
      .multi-title {
        position: relative;
        margin-bottom: 50px;
        font-family: "Oswald", sans-serif;
        font-weight: normal;
        font-size: 35px;
        line-height: 65px;
        text-transform: uppercase;
        color: #ffffff;
        /*background-color: #695753;*/
      }

      .multi-title span {
        font-family: "Oswald", sans-serif;
        color: #ffb800;
      }

      .report-block {
        width: calc(100% + 40px);
        width: 100%;
        display: flex;
        align-items: stretch;
        justify-content: center;
        /*background-color: #695753;*/
      }

      .report-block .left {
        width: 100%;
        max-width: 394px;
        margin: 0 10px;
      }

      .report-block .right {
        width: 100%;
        max-width: 855px;
        margin: 0 10px;
      }

      .report-block .item,
      .exact-ev-report .item{
        padding: 15px 20px;
        background: #181818;
        border: 2px solid #333333;
        border-radius: 10px;
        margin-bottom: 20px;
        min-height: 70px;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
      }

      .report-block .item.item-row,
      .exact-ev-report .item-row {
        -webkit-box-orient: horizontal;
        -webkit-box-direction: normal;
        flex-direction: row;
        -webkit-box-align: stretch;
        align-items: stretch;
      }

      .report-block .item .item-text-row-info,
      .exact-ev-report .item-text-row-info {
        width: 100%;
        display: flex;
        -webkit-box-pack: justify;
        justify-content: space-between;
        -webkit-box-align: center;
        align-items: center;
      }

      .report-block .item .item-text-row-info .wrap {
        display: flex;
        -webkit-box-align: center;
        align-items: center;
      }

      .report-block .item .item-text-row-info .wrap span {
        font-weight: 300;
        font-size: 16px;
        line-height: 24px;
        color: rgb(255, 255, 255);
      }

      .report-block .item .item-text-row-info .price {
        display: flex;
        -webkit-box-align: center;
        align-items: center;
      }

      .report-block .item .item-text-row-info .price span {
        margin-right: 10px;
        display: block;
        font-weight: 300;
        font-size: 16px;
        line-height: 24px;
        color: rgb(255, 255, 255);
      }

      .report-block .item .line,
      .exact-ev-report .line{
        border: 1px solid #333333;
        margin: 0px 30px;
      }
      .report-block .item .line-row {
        margin: 28px 0;
        border: 1px solid #333333;
        width: 100%;
      }

      .report-block .item .item-text-row {
        width: 100%;
        display: flex;
        justify-content: space-between;
        align-items: center;
      }

      .report-block .item .item-text-row .title {
        display: flex;
        align-items: center;
        text-align: left;
      }

      .report-block .item .item-text-row .title span {
        font-weight: 300;
        font-size: 16px;
        line-height: 90%;
        color: #ffffff;
      }

      .report-block .item .item-text-row p {
        font-weight: 600;
        font-size: 16px;
        line-height: 22px;
        text-align: right;
        color: #ffb800;
      }

      .report-block .item .row-winrate {
        width: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
      }

      .report-block .item .row-winrate .title {
        margin-bottom: 33px;
        font-weight: 600;
        font-size: 16px;
        line-height: 22px;
        text-align: center;
        color: #ffffff;
      }

      .report-block .item .row-winrate .blocks {
        width: 100%;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        flex-direction: column;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        -webkit-box-align: stretch;
        -ms-flex-align: stretch;
        align-items: stretch;
      }

      .report-block .item .row-winrate .blocks .block {
        padding: 0 10px;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        flex-direction: row;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        border-top: 1px solid #333333;
      }

      .report-block .item .row-winrate .blocks .block:first-child {
        border: none;
      }

      .blocks .block span {
        display: block;
        margin-bottom: 10px;
        font-weight: 600;
        font-size: 16px;
        line-height: 20px;
        text-align: center;
        color: #ffffff;
        flex: 1;
      }

      .blocks .block span.block-title {
        font-weight: 400;
        font-size: 14px;
      }

      .blocks .block p {
        font-weight: 500;
        font-size: 16px;
        line-height: 20px;
        text-align: center;
        color: #ffb800;
        flex: 1;
        padding: 0px 5px;
      }

      .blocks .block p.sim {
        font-size: 14px;
        font-weight: 400;
        margin: 5px 0px;
      }

      .report-block .item .bonus {
        width: 100%;
        display: flex;
        -webkit-box-pack: center;
        justify-content: center;
        -webkit-box-align: center;
        align-items: center;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        flex-direction: column;
      }

      .report-block .item .bonus .title {
        margin-top: 15px;
        margin-bottom: 30px;
        font-weight: 600;
        font-size: 16px;
        line-height: 22px;
        text-align: center;
        color: rgb(255, 255, 255);
      }

      .report-block .item .bonus .block {
        margin-bottom: 45px;
        width: 175px;
        height: 156px;
        display: flex;
        -webkit-box-pack: center;
        justify-content: center;
        -webkit-box-align: center;
        align-items: center;
        position: relative;
      }

      .report-block .item .bonus .block .bonus-speed {
        position: absolute;
        transform: rotate(-140deg);
        transform-origin: 50% 64%;
        margin-top: 0px;
        transition: transform 1s ease-in-out;
      }

      .report-block .item .bonus .block span {
        position: absolute;
        bottom: -25px;
        font-weight: 500;
        font-size: 30px;
        line-height: 37px;
        text-align: center;
        color: rgb(255, 184, 0);
      }

      .report-block .item .forms {
        width: 100%;
        display: flex;
        justify-content: space-between;
        align-items: flex-end;
        flex-wrap: wrap;
        margin-top: 10px;
        margin-bottom: 15px;
      }

      .report-block .item .forms .forms-item {
        width: calc(50% - 20px);
        margin-bottom: 15px;
        display: flex;
        align-items: flex-start;
        flex-direction: column;
        position: relative;
        pointer-events: none;
      }

      .report-block .item .forms .forms-item span {
        display: block;
        margin-bottom: 5px;
        font-weight: normal;
        font-size: 12px;
        line-height: 15px;
        color: #ffffff;
        text-align: left;
        width: 100%;
      }

      .report-block .item .forms .forms-item input {
        background: rgba(255, 255, 255, 0.1);
        border-radius: 50px;
        border: none;
        padding: 10px 15px;
        font-weight: normal;
        font-size: 12px;
        width: 100%;
        line-height: 15px;
        color: #ffffff;
        text-transform: capitalize;
      }

      .report-block .item .title-info {
        margin: 10px 0 15px 0;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        text-align: center;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        width: 100%;
      }
      .report-block .item .title-info span {
        font-weight: 500;
        font-size: 16px;
        line-height: 22px;
        text-align: center;
        color: #ffffff;
      }

      .title-block .item .title-info span {
        font-weight: 500;
        font-size: 18px;
        line-height: 22px;
        text-align: center;
        color: #ffffff;
      }

     
      .report-block .row-item {
        width: 100%;
        margin-left: -10px;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: stretch;
        -ms-flex-align: stretch;
        align-items: stretch;
      }

      .report-block .row-item .item {
        width: 100%;
        margin: 0 10px;
        margin-bottom: 20px !important;
        padding: 30px 20px;
      }

      .report-block .item .chart-radius {
        width: 100%;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: justify;
        -ms-flex-pack: justify;
        justify-content: space-between;
        -webkit-box-align: stretch;
        -ms-flex-align: stretch;
        align-items: stretch;
      }

      .report-block .item .chart-radius .chart {
        width: 110px;
        height: 110px;
        min-width: 110px;
      }

      .report-block .item .chart-radius .chart canvas {
        width: 100%;
        height: 100%;
      }

      .report-block .item .chart-radius .line {
        margin: 0 37px;
      }

      .report-block .item .chart-radius .text {
        width: auto;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: start;
        -ms-flex-align: start;
        align-items: flex-start;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        text-align: left;
      }
      .report-block .item .chart-radius .text span {
        display: block;
        margin-bottom: 16px;
        font-weight: 600;
        font-size: 16px;
        line-height: 22px;
        color: #ffffff;
      }

      .report-block .item .chart-radius .text .plus {
        margin-bottom: 6px;
        font-weight: 500;
        font-size: 14px;
        line-height: 17px;
        color: #ffeec2;
      }
      .report-block .item .chart-radius .text .zero {
        font-weight: 500;
        font-size: 14px;
        line-height: 17px;
        color: #ffb800;
      }

      .report-block .item .chart-custom {
        width: 100%;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: stretch;
        -ms-flex-align: stretch;
        align-items: stretch;
        margin-bottom: 15px;
        -webkit-transition: 0.3s;
        transition: 0.3s;
        overflow: hidden;
      }
      .report-block .item .chart-custom .block {
        width: 100%;
        max-width: 130px;
        min-width: 20px;
        margin-right: 24px;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: end;
        -ms-flex-pack: end;
        justify-content: flex-end;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        -webkit-transition: 0.3s;
        transition: 0.3s;
      }
      .report-block .item .chart-custom .block .fail-row {
        margin-bottom: 11px;
        font-weight: 500;
        font-size: 12px;
        line-height: 14px;
        text-align: center;
        color: #959595;
        opacity: 0;
        -webkit-transition: 0.3s linear;
        transition: 0.3s linear;
      }
      .report-block .item .chart-custom .block .count {
        font-weight: bold;
        font-size: 14px;
        line-height: 16px;
        text-align: center;
        color: rgba(255, 255, 255, 0.6);
        margin-bottom: 10px;
        -webkit-transition: 0.3s;
        transition: 0.3s;
      }
      .report-block .item .chart-custom .block .block-lines {
        width: 100%;
        margin-bottom: 10px;
      }
      .report-block .item .chart-custom .block .block-lines .block-lines-item {
        width: 100%;
        height: 3px;
        margin-top: 2px;
        background: #ffb800;
        border-radius: 5px;
      }
      .report-block .item .chart-custom .block .count {
        font-weight: bold;
        font-size: 14px;
        line-height: 16px;
        text-align: center;
        color: rgba(255, 255, 255, 0.6);
        margin-bottom: 10px;
        -webkit-transition: 0.3s;
        transition: 0.3s;
      }
      .report-block .item .chart-custom .block .frequency {
        font-weight: 500;
        font-size: 12px;
        line-height: 14px;
        text-align: center;
        color: #959595;
        opacity: 0;
        -webkit-transition: 0.3s linear;
        transition: 0.3s linear;
      }
      .report-block .item .chart-custom .block.activeOpacity {
        opacity: 0.3;
      }

      .report-block .item .chart-custom .block.active {
        opacity: 1;
      }

      .report-block .item .chart-custom .block:last-child {
        margin-right: 0;
      }

      .report-block .item .chart-custom .block:hover .fail-row,
      .report-block .item .chart-custom .block:hover .frequency {
        opacity: 1;
      }

      .report-block .item .chart-custom .block:hover .count {
        color: #fff;
      }

      .report-block .item .chart-radius .chart canvas {
        box-sizing: border-box;
        display: block;
        height: 110px;
        width: 110px;
      }

      .report-block .item.chart-block-hid {
        overflow: hidden;
        padding: 15px 10px;
      }
      .report-block .item .wrap-chart-line {
        width: 100%;
        position: relative;
        min-height: 374px;
      }
      .report-block .item .chart-line {
        width: 100%;
      }
      .report-block .item .chart-line-blocks {
        width: 100%;
        height: 100%;
        /*min-width: 763px;*/
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        -webkit-box-align: stretch;
        -ms-flex-align: stretch;
        align-items: stretch;
        /*position: absolute;
        left: 0;
        top: 0;
        overflow-x: scroll;*/
      }
      /*::-webkit-scrollbar {
        width: 0;
      }*/

      .report-block .item .chart-line-blocks .block-wrap {
        /*width: 100%;*/
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: end;
        -ms-flex-align: end;
        align-items: flex-end;
        -webkit-box-pack: start;
        -ms-flex-pack: start;
        justify-content: flex-start;
        position: relative;
      }
      .block-wrap .block {
        width: 100%;
        min-width: 125px;
        min-height: 355px;
        margin-right: 6px;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: justify;
        -ms-flex-pack: justify;
        justify-content: space-between;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        padding: 20px 0;
        -webkit-transition: 0.3s linear;
        transition: 0.3s linear;
        -webkit-transform: translate(0, 0);
        transform: translate(0, 0);
        position: relative;
      }
      .block-wrap .block::before {
        content: "";
        width: 100%;
        /*max-width: 104px;*/
        min-height: 355px;
        position: absolute;
        left: 0;
        bottom: 0;
        z-index: -1;
        background: #262626;
        border-radius: 10px;
        -webkit-transition: 0.3s;
        transition: 0.3s;
      }
      .report-block .item .chart-line-blocks .block-wrap .block .top {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        text-align: center;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        -webkit-transition: 0.5s;
        transition: 0.5s;
      }

      .report-block .item .chart-line-blocks .block-wrap .block:hover {
        padding-top: 5px;
      }
      .block-wrap .block .top span {
        font-weight: bold;
        font-size: 16px;
        line-height: 20px;
        text-align: center;
        color: rgba(255, 255, 255, 0.6);
        -webkit-transition: 0.3s linear;
        transition: 0.3s linear;
      }
      .report-block .item .chart-line-blocks .block-wrap .block .top p {
        font-weight: 500;
        font-size: 12px;
        line-height: 15px;
        text-align: center;
        color: rgba(255, 255, 255, 0.6);
        -webkit-transition: 0.3s linear;
        transition: 0.3s linear;
      }

      .report-block .item .chart-line-blocks .block-wrap .block:hover .top p {
        color: rgba(255, 255, 255, 1);
      }

      .block-wrap .block .bottom {
        font-weight: bold;
        font-size: 14px;
        line-height: 17px;
        text-align: center;
        color: rgba(255, 255, 255, 0.6);
        -webkit-transition: 0.3s linear;
        transition: 0.3s linear;
      }
      .report-block .item .chart-line-blocks .block-wrap .block .linear,
      .report-block .item .chart-line-blocks .block-wrap .block .linear-zero {
        width: 0;
        position: absolute;
        left: 50%;
        bottom: 20%;
        height: 2px;
        background: #ffb800;
        z-index: 6;
        -webkit-transform-origin: 0% 0%;
        transform-origin: 0% 0%;
      }
      .report-block .item .chart-line-blocks .block-wrap .block.lst {
        margin-right: 0;
      }

      .report-block .item .chart-line-blocks .block-wrap .linePos {
        position: absolute;
        bottom: 20%;
        z-index: 10;
        pointer-events: none;
        height: 6px;
        width: 6px;
        border-radius: 50%;
        background: #ffffff;
        -webkit-transition: 0.3s;
        transition: 0.3s;
        z-index: 100;
      }
      .block-wrap .block.compressed,
      .block-wrap .block.compressed::before{
        min-width: 75px;
        min-height: 255px;
      }
      .block-wrap .block.max-compressed,
      .block-wrap .block.max-compressed::before{
        min-width: 55px;
        min-height: 255px;
        margin-right: 3px;
      }
      .block-wrap .block.max-compressed .top span,
      .block-wrap .block.max-compressed .bottom {
        font-weight: 500;
        font-size: 12px;
      }
      .i-info {
        cursor: pointer;
        display: block;
        color: #262626;
        padding-left: 5px;
      }
      .report-block .item .bonus .info {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        margin-bottom: 15px;
      }

      .report-block .item .bonus .info span {
        display: block;
        margin-right: 10px;
        font-weight: 500;
        font-size: 16px;
        line-height: 20px;
        color: rgba(255, 255, 255, 0.5);
      }
      .pointer {
        width: auto;
        position: fixed;
        left: 0;
        z-index: 1000;
        min-width: 140px;
        height: auto;
        max-width: 330px;
        padding: 16px 10px;
        background: #404040;
        box-shadow: 0px 0px 10px rgb(0 0 0 / 25%);
        border-radius: 10px;
        display: -webkit-inline-box;
        display: -ms-inline-flexbox;
        display: inline-flex;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        opacity: 0;
        pointer-events: none;
        -webkit-transition: opacity 0.3s linear;
        transition: opacity 0.3s linear;
      }
      .pointer.active {
        opacity: 1;
        pointer-events: visible;
      }
      .pointer.activeLeft {
        opacity: 1;
        pointer-events: visible;
      }

      .pointer.activeLeft::before {
        left: unset;
        right: -10px;
        -webkit-transform: rotate(180deg);
        transform: rotate(180deg);
        background-position: right center;
      }

      .pointer.activeBottom {
        opacity: 1;
        left: 50%;
        -webkit-transform: translate(-50%, 0);
        transform: translate(-50%, 0);
        pointer-events: visible;
        min-width: 250px;
      }

      .pointer.activeBottom::before {
        left: unset;
        top: -15px;
        right: unset;
        -webkit-transform: rotate(90deg);
        transform: rotate(90deg);
        background-position: top center;
      }
      .pointer::before {
        content: "";
        width: 10px;
        height: 20px;
        position: absolute;
        left: -10px;
        z-index: -1;
        background: url(https://bonusev.com/images/arrow-pointer.svg) left
          center no-repeat;
        background-size: contain;
      }

      .pointer .logos-row {
        width: auto;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
      }
      .pointer .logos-row .item-text {
        width: 100%;
        max-width: 280px;
        padding: 5px 10px;
      }
      .pointer .logos-row .item-text span {
        display: block;
        margin-bottom: 20px;
        font-weight: normal;
        font-size: 16px;
        line-height: 20px;
        color: #ffffff;
      }
      .pointer .logos-row .item-text a {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: start;
        -ms-flex-pack: start;
        justify-content: flex-start;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        font-weight: 600;
        font-size: 16px;
        line-height: 20px;
        text-transform: uppercase;
        color: #ffb800;
        -webkit-transition: 0.3s;
        transition: 0.3s;
      }
      .pointer .logos-row .item-text a:hover {
        color: #ffd56a;
      }

      .pointer .logos-row .item-text a:hover svg path {
        stroke: #ffd56a;
      }

      .pointer .logos-row .item-text a:active {
        color: #ffb800;
      }

      .pointer .logos-row .item-text a:active svg path {
        stroke: #ffb800;
      }
      .pointer .logos-row .item-text a svg {
        margin-left: 20px;
        -webkit-transition: 0.3s;
        transition: 0.3s;
      }
      .pointer .logos-row .item-text a svg path {
        -webkit-transition: 0.3s;
        transition: 0.3s;
      }
      .report-block .item .item-text-row .title .img {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        margin-right: 15px;
      }
      .report-block .item .item-text-row-info .wrap .img {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        margin-right: 30px;
      }
      .stopwatch-wrapper{
        padding-bottom: 20px;
      } 
      .stopwatch{
        height: 70px;
        justify-content: center;
        min-width: 100px;
        margin-top: 20px;
      }
      .stopwatch form{
        margin: 0;
      }
      .stopwatch input{
        text-align: center;
      }
      .stopwatch .expected-time{
        color: #f5f5f5;
      }
      .report{
        display: none;
      }
      .exact-ev-report{
        width: 70%;
        color:#f5f5f5;
        font-weight: 500;
        font-size: 16px;
        border-radius: 5px 5px;
        padding: 20px 0px;
      }
      .exact-ev-report .item{
        width: 100%;
      }
      .exact-ev-report .item.hidden{
      display: none;
    }
      .exact-ev-report .item-text-row-info{
        justify-content: space-evenly;
      }
      .exact-ev-report .item span{
        color: rgb(255, 184, 0);
      }

      .calculate-block .form .form-drop.hidden{
          display: none;
        }
    .hidden{
      display: none;
    }
        
    @media (max-width: 500px) {
      header {
        grid-column: 2/5;
      }
      nav {
        flex-direction: row;
        height: auto;
        margin-top: 5px;
        justify-content: start;
      }
      nav .btn-menu {
        display: block;
        color: #f5f5f5;
        float: right;
        margin-right: 10px;
      }

      ul.topnav {
        display: none;
      }
      ul.topnav li {
        width: 90%;
        height: 40px;
      }

      .topnav li a {
        width: 100%;
        padding: 0.5em;
      }
      .topnav li:first-child .roll-link .front,
      .topnav li:first-child .roll-link .back {
        border-radius: 5px 5px 0px 0px;
      }

      .topnav li:last-child .roll-link .front,
      .topnav li:last-child .roll-link .back {
        border-radius: 0px 0px 5px 5px;
      }

      .roll-link {
        width: 100%;
      }

      .topnav.responsive {
        display: block;
      }
    }
    

    @media (max-width: 1040px) {
      .report-block,
      .report-block .row-item {
        flex-direction: column;
      }
    }
    @media (max-width: 800px){
      .grid {
        grid-template-columns: .5fr 0.5fr 2fr 0.5fr .5fr;
      }
      .calculate-block{
        padding: 20px;
      }
      .calculate-block .form .form-drop {
        flex-direction: column;
        align-items: flex-start;
      }
      .calculate-block .form .form-drop .drop-wrap {
        min-width: 200px;
        width: 100%;
      }
      .calculate-block .form .form-drop .drop{
        min-width: 200px;
        width: 100%;
      }

      .calculate-block .form .form-drop .block-value{
        width: 100%;
        min-width: 200px;
      }
      .calculate-block .form .dropdown-settings .form-drop .block-value{
        width: 95%;
      }
      .calculate-block .form .form-drop .form-drop-name {
        font-size: 14px;
        margin:0px 6px 6px 6px;
      }

      .calculate-block .form .dropdown-settings .form-drop:nth-child(1),
      .calculate-block .form .dropdown-settings .form-drop:nth-child(2){
        flex-direction: row;
        align-items: center;
      }
      .calculate-block .form .dropdown-settings .content.form-drop {
        flex-direction: column;
        align-items: flex-start;
      }

      .btn-set:before{
        right: 15%;
      }
    }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>

</head>
<body>

    <!-- Грид-контейенер общий -->
    <div class="grid">
        <!--Header-->
        <header>
          <!--блок навгации-->
          <nav class="navigation">
            <!-- Кнопка раскрытия меню -->
            <a href="javascript:void(0)" class="btn-menu" >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="24"
                height="24"
                viewBox="0 0 24 24"
                style="fill:white"
              >
                <path
                  d="M3.5,7 C3.22385763,7 3,6.77614237 3,6.5 C3,6.22385763 3.22385763,6 3.5,6 L20.5,6 C20.7761424,6 21,6.22385763 21,6.5 C21,6.77614237 20.7761424,7 20.5,7 L3.5,7 Z M3.5,12 C3.22385763,12 3,11.7761424 3,11.5 C3,11.2238576 3.22385763,11 3.5,11 L20.5,11 C20.7761424,11 21,11.2238576 21,11.5 C21,11.7761424 20.7761424,12 20.5,12 L3.5,12 Z M3.5,17 C3.22385763,17 3,16.7761424 3,16.5 C3,16.2238576 3.22385763,16 3.5,16 L20.5,16 C20.7761424,16 21,16.2238576 21,16.5 C21,16.7761424 20.7761424,17 20.5,17 L3.5,17 Z"
                />
              </svg>
            </a>
            <ul class="topnav" id="myTopnav">
              <li>
                <div class="roll-link">
                  <a href="/SlotApplication/" class="front">Home</a>
                  <a href="/SlotApplication/" class="back">Home</a>
                </div>
              </li>
              <li>
                <div class="roll-link">
                  <a href="/SlotApplication/tools" class="front">Tools</a>
                  <a href="/SlotApplication/tools" class="back">Tools</a>
                </div>
              </li>
              <li>
                <div class="roll-link">
                  <a href="/SlotApplication/slots" class="front">Slots</a>
                  <a href="/SlotApplication/slots" class="back">Slots</a>
                </div>
              </li>
              <li>
                <div class="roll-link">
                  <a href="/SlotApplication/jackpots" class="front">Jackpots</a>
                  <a href="/SlotApplication/jackpots" class="back">Jackpots</a>
                </div>
              </li>
              <li>
                <div class="roll-link">
                  <a href="/SlotApplication/sites" class="front">Sites</a>
                  <a href="/SlotApplication/sites" class="back">Sites</a>
                </div>
              </li>
            </ul>
          </nav>
          <!--/ блок навгации-->
        </header>
        <!--/Header-->
  
        <!--main-->
        <main>
            <div class="wrapper">
            <div class="calculate-block">
                <div class="form">
            <%  SlotInputError errorInput = (SlotInputError) request.getAttribute("errorInput");
            SlotInputReport report = (SlotInputReport) request.getAttribute("report");
            List<PairProfit> top5 = (List<PairProfit>) request.getAttribute("profitProbability");
              List<Provider> providers = (List<Provider>) request.getAttribute("providers");
            if (errorInput==null){
                if (report == null){
                    
        %>          <div class="form-drop">
            <label class="form-drop-name">Выберите провайдера: </label>
            <div class="drop-wrap">
                <div class="drop">
                    <select id="provider"  class="drop-top">
        <%          for(Provider provider : providers){
          %>              <option id="<%=provider.getRawProvider()%>" class="item"><%=provider.getCleanProvider()%></option>
        <%          }
        %>          </select>
    
        <div class="i-info" data-type="text" data-content="providerPrompt">
            <img src="https://bonusev.com/images/i-info.svg" />
          </div>
        <!--<label class="prompt-label">  ?  </label>
        <label id="providerPrompt" class="prompt"></label>-->
    </div>
    </div>
    </div>
        
                    <div id="inputContainer">
                        <div class="form-drop">
                            <label class="form-drop-name">Выберите слот: </label>
                            <div class="drop-wrap">
                            <div id="games" class="drop">
                                <div class="i-info" data-type="text" data-content="gamesPrompt">
                                    <img src="https://bonusev.com/images/i-info.svg" />
                                  </div>
                            <!--<label class="prompt-label">  ?  </label>
                            <label id="gamesPrompt" class="prompt"></label>-->
                        </div>
                        </div>
                        </div>
                        
        
                        <div class="form-drop">
                            <label class="form-drop-name">Депозит: </label>
                            <div class="block-value">
                            <input id="deposit"/ class="required">
                            <div class="i-info" data-type="text" data-content="depositPrompt">
                                <img src="https://bonusev.com/images/i-info.svg" />
                              </div>
                            <!--<label class="prompt-label">  ?  </label>
                            <label id="depositPrompt" class="prompt"></label>-->
                        </div> 
                        </div>
        
                        <div class="form-drop">
                            <label class="form-drop-name">Бонус: </label> 
                            <div class="block-value">
                            <input id="bonus" class="required"/>
                            <div class="i-info" data-type="text" data-content="bonusPrompt">
                                <img src="https://bonusev.com/images/i-info.svg" />
                              </div>
                            <!--<label class="prompt-label">  ?  </label>
                            <label id="bonusPrompt" class="prompt"></label>-->
                            </div>
                        </div>
        
                        <div id="casinoFees">
                            <div class="form-drop">
                                <label class="form-drop-name">Provider Fee: </label>
                                <div class="block-value">
                                <input id="providerFee" value="0"/>
                                <div class="i-info" data-type="text" data-content="providerFeePrompt">
                                    <img src="https://bonusev.com/images/i-info.svg" />
                                  </div>
                                <!--<label class="prompt-label">  ?  </label>
                                <label id="providerFeePrompt" class="prompt"></label>-->
                                </div>
                            </div>
                            <div class="form-drop">
                                <label class="form-drop-name">  Payment Fee: </label>
                                <div class="block-value">
                                <input id="paymentFee" value="0"/>
                                <div class="i-info" data-type="text" data-content="paymentFeePrompt">
                                    <img src="https://bonusev.com/images/i-info.svg" />
                                  </div>
                                <!--<label class="prompt-label">  ?  </label>
                                <label id="paymentFeePrompt" class="prompt"></label>-->
                                </div>
                            </div>
                            <div class="form-drop">
                                <label class="form-drop-name">  Platform Fee: </label>
                                <div class="block-value">
                                <input id="platformFee" value="0"/>
                                <div class="i-info" data-type="text" data-content="platformFeePrompt">
                                    <img src="https://bonusev.com/images/i-info.svg" />
                                  </div>
                                <!--<label class="prompt-label">  ?  </label>
                                <label id="platformFeePrompt" class="prompt"></label>-->
                                </div>
                            </div>
                        </div>
        
                        <div class="form-drop">
                            <label class="form-drop-name">Ставка: </label> 
                            <div class="block-value">
                            <input id="bet" class="required"/>
                            <div class="i-info" data-type="text" data-content="betPrompt">
                                <img src="https://bonusev.com/images/i-info.svg" />
                              </div>
                            <!--<label class="prompt-label">  ?  </label>
                            <label id="betPrompt" class="prompt"></label>-->
                            </div>
                        </div>
        
                        <div class="form-drop">
                            <label class="form-drop-name">Wagger type </label>
                            <div class="drop-wrap">
                                <div class="drop">
                            <select id="waggerType" class="drop-top">
                <%          for (String waggerType : (List<String>) request.getAttribute("waggerTypes")){
                %>              <option class="item"><%=waggerType%></option>
                <%          }
                %>          </select>
                <div class="i-info" data-type="text" data-content="waggerTypePrompt">
                    <img src="https://bonusev.com/images/i-info.svg" />
                  </div>
                            <!--<label class="prompt-label">  ?  </label>
                            <label id="waggerTypePrompt" class="prompt"></label>-->
                        </div>
                        </div>
                        </div>
        
                        <div class="form-drop">
                            <label class="form-drop-name">Wagger: </label> 
                            <div class="block-value">
                            <input id="wagger" class="required"/>
                            <div class="i-info" data-type="text" data-content="waggerPrompt">
                                <img src="https://bonusev.com/images/i-info.svg" />
                              </div>
                            <!--<label class="prompt-label">  ?  </label>
                            <label id="waggerPrompt" class="prompt"></label>-->
                            </div>
                        </div>
        
                        <div class="form-drop">
                            <label class="form-drop-name">Симуляции: </label>
                            <div class="block-value">
                            <input id="sim" class="required input-range" type="text" data-max="10000000"/>
                            <div class="i-info" data-type="text" data-content="simulationsPrompt">
                                <img src="https://bonusev.com/images/i-info.svg" />
                              </div>
                            <!--<label class="prompt-label">  ?  </label>
                            <label id="simulationsPrompt" class="prompt"></label>-->
                            </div>
                        </div>
                    </div>
        
                    <div id="inputSettings" class="input-settings">
                        <button id="settingsButton" class="btn-set">Расширенные настройки</button>
                        <div id="advanceSettings" class="dropdown-settings">
                            <div class="form-drop">
                                <div class="check">
                                <label class="form-drop-name">  Результат без учета бонуса</label>
                                <input type="checkbox" id="withoutBonus"/>
                                <label for="withoutBonus"></label>
                            </div>
                            <div class="i-info" data-type="text" data-content="withoutBonusPrompt">
                                <img src="https://bonusev.com/images/i-info.svg" />
                              </div>
                                <!--<label class="prompt-label">  ?  </label>
                                <label id="withoutBonusPrompt" class="prompt"></label>-->
                                
                            </div>
        
                            <div class="form-drop">
                                <div class="check"> 
                                <label class="form-drop-name">  Обращаться в БД</label>
                                <input type="checkbox" id="manual" checked/>
                                <label for="manual"></label>
                                </div>
                                <div class="i-info" data-type="text" data-content="manualPrompt">
                                    <img src="https://bonusev.com/images/i-info.svg" />
                                  </div>
                                <!--<label class="prompt-label">  ?  </label>
                                <label id="manualPrompt" class="prompt"></label>-->
                                
                            </div>
        
                            <div id="taxesDiv" class="form-drop">
                                <label class="form-drop-name">Налог: </label>
                                <div class="block-value">
                                <input id="taxes" value="0"/>
                                <div class="i-info" data-type="text" data-content="taxesPrompt">
                                    <img src="https://bonusev.com/images/i-info.svg" />
                                  </div>
                                <!--<label class="prompt-label">  ?  </label>
                                <label id="taxesPrompt" class="prompt"></label>-->
                                </div>
                            </div>
        
                            <div id="accuracyDiv" class="hidden form-drop">
                                <label class="form-drop-name">Точность расчета точного EV в % (от 0 до 100)</label>
                                <div class="block-value">
                                <input id="accuracy" class="text-input" value="95"/>
                                <div class="i-info" data-type="text" data-content="accuracyPrompt">
                                    <img src="https://bonusev.com/images/i-info.svg" />
                                  </div>
                                <!--<label class="prompt-label">  ?  </label>
                                <label id="accuracyPrompt" class="prompt"></label>-->
                                </div>
                            </div>
        <div class="accordion">
                            <div class="accordion-item">
                                <button id="hideButton"  class="heading"><div class="icon"></div> <div class="title">Добавить ограничение максимального выигрыша</div></button>
                                <div id="maxProfitDiv"  class="content form-drop ">
                                    <label class="form-drop-name">   Максимальное значение выигрыша одной симуляции</label>
                                    <div class="block-value">
                                    <input id="maxProfit"/>
                                    <div class="i-info" data-type="text" data-content="maxProfitPrompt">
                                        <img src="https://bonusev.com/images/i-info.svg" />
                                      </div>
                                    <!--<label class="prompt-label">  ?  </label>
                                    <label id="maxProfitPrompt" class="prompt"></label>-->
                                    </div>
                                </div>
                            </div>

                            <div class="accordion-item">
                              <button id="hideButton"  class="heading"><div class="icon"></div> <div class="title">Добавить ограничение одного спина</div></button>
                              <div id="spinLimitDiv"  class="content form-drop ">
                                  <label class="form-drop-name">   Ограничение одного спина</label>
                                  <div class="block-value">
                                  <input id="spinLimit"/>
                                  <div class="i-info" data-type="text" data-content="spinLimit">
                                      <img src="https://bonusev.com/images/i-info.svg" />
                                    </div>
                                  <!--<label class="prompt-label">  ?  </label>
                                  <label id="maxProfitPrompt" class="prompt"></label>-->
                                  </div>
                              </div>
                          </div>
        
                            <div class="accordion-item">
                                <button id="hideBonusButton"  class="heading"><div class="icon"></div> <div class="title">Бонус отыгрывается частями</div></button>
                                <div id="bonusPartDiv"  class="content form-drop">
                                    <label class="form-drop-name">   Количество частей бонуса</label>
                                    <div class="block-value">
                                    <input id="bonusPart"/>
                                    <div class="i-info" data-type="text" data-content="bonusPartPrompt">
                                        <img src="https://bonusev.com/images/i-info.svg" />
                                      </div>
                                    <!--<label class="prompt-label">  ?  </label>
                                    <label id="bonusPartPrompt" class="prompt"></label>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="bonus-wrap">
                        <button id="hideLinkButton" disabled  class="btn">Привязать бонус к сайту</button>
                        <div id="bonusLinkDiv" class="hidden bonus-link flex-column">
                          <div class="drop-wrap">
                            <div class="drop">
                            <select id="brandSelect"  class="drop-top"></select>
                          </div>
                        </div>
                          <div class="drop-wrap">
                            <div class="drop">
                             <select id="siteSelect" class="drop-top"></select>
                             </div>
                             </div>
                              <button id="linkBonus"  class="btn-bonus">Привязать</button>
                              <label id="linkMessage"></label>
                        </div>
                    </div>
        <%      }
            } %>
        <div id="performsDiv">
            <button id="perform"  class="btn">Выполнить</button>
        </div>
        <button onclick="location.href='/SlotApplication/'" class="btn-back">Вернуться в меню</button>
               
    </div>
    </div>
</div>
        <div class="stopwatch-wrapper flex-column">
        <div id="message" class="message"></div>
        <div class="stopwatch flex-column hidden" id="stopwatch">
            <form name=MyForm>
                <input name=stopwatch size=10 value="00:00:00">
            </form>
            <div id="expectedTime" class="expected-time"></div><br>
        </div>
      </div>
        <div id="exactEVReport" class="exact-ev-report flex-column hidden">
            <div id="error-ev-div"></div>
            <div class="item item-row">
              <div class="item-text-row-info">
              <label id="evValue"></label>
            </div>
              <div class="line"></div>
              <div class="item-text-row-info">
              <label id="accuracyEV"></label>
            </div>
            </div> 
            <div class="item">
            <label id="simulationsEV"></label>
          </div>
        </div>

<!--Report-->
<div id="reportPlace">
  <div id="error-message-div" class="flex-row error-message-div"></div>
  <div id="report" class="report">
      <!--<label id="timer"></label>
      <label id="slotLabel"></label><br>
      <label id="providerLabel"></label><br><br>
      <label id="profitLabel"></label><br>
      <label id="evLabel"></label><br>
      <label id="roiLabel"></label><br>
      <label id="bonusVolatilityLabel"></label><br>
      <label id="positiveProfitLabel"></label><br>
      <label id="nullProfitLabel"></label><br>
      <label id="averageWin"></label><br>
      <label id="betCounter"></label><br><br>

      <label id="topProfitsLabel"></label><br>
      <div id="topProfitsDiv"></div><br>

      <label id="depositMultiplierLabel"></label>
      <div id="depositMultiplierDiv"></div><br>

      <div id="series"></div><br>

      <label id="longestNonProfitLabel"></label><br>
      <label id="longestNonProfitFrequencyLabel"></label><br><br>
      <label id="longestProfitLabel"></label><br>
      <label id="longestProfitFrequencyLabel"></label><br><br>-->
  


<div class="wrap-report-block">
  <div class="multi-title">ИТОГОВЫЙ <span>ОТЧЕТ</span></div>
  <div class="report-block">
    <div class="left">
      <div class="item">
        <div class="item-text-row">
          <div class="title">
            <div class="img">
              <img src="https://bonusev.com/images/i-1.svg" />
            </div>
            <span>Средний бонусный выигрыш</span>
          </div>
          <p class="average-win"></p>
        </div>
      </div>
      <div class="item">
        <div class="item-text-row">
          <div class="title">
            <div class="img">
              <img src="https://bonusev.com/images/i-2.svg" />
            </div>
            <span>Количество спинов </span>
          </div>
          <p class="bet-counter"></p>
        </div>
      </div>
      <div class="item">
        <div class="row-winrate">
          <div class="title">Самые крупные Profit и их вероятности</div>
          <div class="blocks blocks-top">
            <div class="block">
              <span></span><span class="block-title">Сумма</span
              ><span class="block-title">Частота</span>
            </div>
          </div>
        </div>
      </div>
      <div class="item">
        <div class="bonus">
          <div class="title">Дисперсия бонуса</div>
          <div class="block">
            <img src="https://bonusev.com/images/bonus.svg" alt />
            <div class="bonus-speed">
              <img
                src="https://bonusev.com/images/bonus-speed.svg"
                alt
              />
            </div>
            <span class="bonus-volatility"></span>
          </div>
          <div class="info">
            <span>Инфо</span>
            <div
              class="i-info"
              data-type="text"
              data-content="bonusVolatility"
            >
              <img src="https://bonusev.com/images/i-info.svg" />
            </div>
          </div>
        </div>
      </div>
      <div class="item">
        <div class="forms"></div>
      </div>
    </div>
    <div class="right">
      <div class="item item-row">
        <div class="item-text-row-info">
          <div class="wrap">
            <div class="img">
              <img src="https://bonusev.com/images/i-3.svg" />
            </div>
            <span>EV игрока</span>
          </div>
          <div class="price">
            <span class="ev-player"></span>
            <div
              class="i-info"
              data-type="text"
              data-content="evPlayer"
            >
              <img src="https://bonusev.com/images/i-info.svg" />
            </div>
          </div>
        </div>
        <div class="line"></div>
        <div class="item-text-row-info">
          <div class="wrap">
            <div class="img">
              <img src="https://bonusev.com/images/i-3.svg" />
            </div>
            <span>EV казино</span>
          </div>
          <div class="price">
            <span class="ev-casino"></span>
            <div
              class="i-info"
              data-type="text"
              data-content="evCasino"
            >
              <img src="https://bonusev.com/images/i-info.svg" />
            </div>
          </div>
        </div>
      </div>
      <div class="item item-row">
        <div class="item-text-row-info">
          <div class="wrap">
            <div class="img">
              <img src="https://bonusev.com/images/i-4.svg" />
            </div>
            <span>ROI игрока</span>
          </div>
          <div class="price">
            <span class="roi-player"></span>
            <div
              class="i-info"
              data-type="text"
              data-content="roiPlayer"
            >
              <img src="https://bonusev.com/images/i-info.svg" />
            </div>
          </div>
        </div>
        <div class="line"></div>
        <div class="item-text-row-info">
          <div class="wrap">
            <div class="img">
              <img src="https://bonusev.com/images/i-4.svg" />
            </div>
            <span>ROI казино</span>
          </div>
          <div class="price">
            <span class="roi-casino"></span>
            <div
              class="i-info"
              data-type="text"
              data-content="roiCasino"
            >
              <img src="https://bonusev.com/images/i-info.svg" />
            </div>
          </div>
        </div>
      </div>
      <div class="item chart-block-hid">
        <div class="title-info">
          <span> Вероятность увеличить депозит в [X] раз </span>
          <div
            class="i-info"
            data-type="text"
            data-content="depositMultipliers"
          >
            <img src="https://bonusev.com/images/i-info.svg" />
          </div>
        </div>
        <div class="wrap-chart-line">
          <div class="chart-line">
            <div class="chart-line-blocks">
              <div class="block-wrap">
                <div class="block">
                  <div class="top">
                    <span></span>
                    <p>симуляций</p>
                  </div>
                  <div class="bottom">x1 dep</div>
                  <div class="linear"></div>
                </div>
                <div class="block">
                  <div class="top">
                    <span></span>
                    <p>симуляций</p>
                  </div>
                  <div class="bottom">x2 dep</div>
                  <div class="linear"></div>
                </div>
                <div class="block">
                  <div class="top">
                    <span></span>
                    <p>симуляций</p>
                  </div>
                  <div class="bottom">x5 dep</div>
                  <div class="linear"></div>
                </div>
                <div class="block">
                  <div class="top">
                    <span></span>
                    <p>симуляций</p>
                  </div>
                  <div class="bottom">x10 dep</div>
                  <div class="linear"></div>
                </div>
                <div class="block">
                  <div class="top">
                    <span></span>
                    <p>симуляций</p>
                  </div>
                  <div class="bottom">x20 dep</div>
                  <div class="linear"></div>
                </div>
                <div class="block">
                  <div class="top">
                    <span></span>
                    <p>симуляций</p>
                  </div>
                  <div class="bottom">x50 dep</div>
                  <div class="linear"></div>
                </div>
                <div class="block lst">
                  <div class="top">
                    <span></span>
                    <p>симуляций</p>
                  </div>
                  <div class="bottom">x100 dep</div>
                  <div class="linear"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row-item">
        <div class="item">
          <div class="chart-radius">
            <div class="chart">
              <canvas id="chartRadius"></canvas>
            </div>
            <div class="line"></div>
            <div class="text">
              <span>
                Вероятность <br />
                отыграть бонус
              </span>
              <div class="plus profit-probability"></div>
              <div class="zero non-profit-probability"></div>
            </div>
          </div>
        </div>
        <div class="item">
          <div class="item-text-row mod-text">
            <div class="title">
              <div class="img">
                <img src="https://bonusev.com/images/i-5.svg" />
              </div>
              <span> MAX серия выигрышей </span>
            </div>
            <p class="longest-profit-series"></p>
          </div>
          <div class="line-row"></div>
          <div class="item-text-row mod-text">
            <div class="title">
              <div class="img">
                <img src="https://bonusev.com/images/i-6.svg" />
              </div>
              <span> MAX серия проигрышей </span>
            </div>
            <p class="longest-non-profit-series"></p>
          </div>
        </div>
      </div>
      <div class="item">
        <div class="title-info">
          <span> Шансы проиграть [N] депозитов подряд </span>
          <div class="i-info" data-type="text" data-content="series">
            <img src="https://bonusev.com/images/i-info.svg" />
          </div>
        </div>
        <div class="chart-custom non-profit-series">
          <div class="block">
            <div class="fail-row">Частота</div>
            <div class="count count-ser"></div>
            <div class="block-lines">
              <div class="block-lines-item"></div>
            </div>
            <div class="count">5</div>
            <div class="frequency">Проигрыши подряд</div>
          </div>
          <div class="block">
            <div class="fail-row">Частота</div>
            <div class="count count-ser"></div>
            <div class="block-lines">
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
            </div>
            <div class="count">10</div>
            <div class="frequency">Проигрыши подряд</div>
          </div>
          <div class="block">
            <div class="fail-row">Частота</div>
            <div class="count count-ser"></div>
            <div class="block-lines">
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
            </div>
            <div class="count">25</div>
            <div class="frequency">Проигрыши подряд</div>
          </div>
          <div class="block">
            <div class="fail-row">Частота</div>
            <div class="count count-ser"></div>
            <div class="block-lines">
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
            </div>
            <div class="count">50</div>
            <div class="frequency">Проигрыши подряд</div>
          </div>
          <div class="block">
            <div class="fail-row">Частота</div>
            <div class="count count-ser"></div>
            <div class="block-lines">
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
            </div>
            <div class="count">100</div>
            <div class="frequency">Проигрыши подряд</div>
          </div>
          <div class="block">
            <div class="fail-row">Частота</div>
            <div class="count count-ser"></div>
            <div class="block-lines">
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
            </div>
            <div class="count">250</div>
            <div class="frequency">Проигрыши подряд</div>
          </div>
          <div class="block">
            <div class="fail-row">Частота</div>
            <div class="count count-ser"></div>
            <div class="block-lines">
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
              <div class="block-lines-item"></div>
            </div>
            <div class="count">500</div>
            <div class="frequency">Проигрыши подряд</div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--/Report-->
</div>
</div>
        </main>
        <!-- /main -->
  
        <!-- footer -->
        <footer></footer>
        <!-- /footer -->
      </div>
      <div class="pointer" data-pointer="text">
        <div class="logos-row">
          <div class="item-text">
            <span> Подсказка </span>
            <!--<a href="#tarif">
              ВЫБРАТЬ ТАРИФ
              <svg
                width="14"
                height="14"
                viewBox="0 0 14 14"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M1 13L13 1M13 1H1M13 1V13"
                  stroke="#FFB800"
                  stroke-width="2"
                />
              </svg>
            </a>-->
          </div>
        </div>
      </div>
      <!--<script> createGame();</script>-->
      <script>


        
        /*считываем данные из инпута для отправки на сервер*/
        function getSlotInputValues(){
            let providerC = document.getElementById("provider");
            let gameC = document.getElementById("game");
            let depositC = document.getElementById("deposit");
            let bonusC = document.getElementById("bonus");
            let withoutBonusC = document.getElementById("withoutBonus");
            let taxesC = document.getElementById("taxes");
            let providerFeeC = document.getElementById("providerFee");
            let paymentFeeC = document.getElementById("paymentFee");
            let platformFeeC = document.getElementById("platformFee");
            let betC = document.getElementById("bet");
            let maxProfitC = document.getElementById("maxProfit");
            let waggerTypeC = document.getElementById("waggerType");
            let waggerC = document.getElementById("wagger");
            let bonusPartC = document.getElementById("bonusPart");
            let spinLimit = document.getElementById("spinLimit");
            let simC = document.getElementById("sim");
            let manualC = document.getElementById("manual");
            let accuracyC = document.getElementById("accuracy");
            let providerOptions = providerC.options;
            let gameOptions = gameC.options;

            let slotInputFrontO = {
                deposit: depositC.value,
                bonus: bonusC.value,
                withoutBonus: withoutBonusC.checked,
                taxes: taxesC.value,
                providerFee: providerFeeC.value,
                paymentFee: paymentFeeC.value,
                platformFee: platformFeeC.value,
                bet: betC.value,
                profitLimit: maxProfitC.value,
                waggerType: waggerTypeC.value,
                wagger: waggerC.value,
                spinLimit: spinLimit.value,
                bonusParts: bonusPartC.value,
                sim: simC.value,
                manual: manualC.checked,
                provider: providerOptions[providerOptions.selectedIndex].id,
                game: gameOptions[gameOptions.selectedIndex].value,
                accuracy: accuracyC.value,
                adminMode: adminMode,
                performType: document.location.search
            };
            window.slotInputFront = slotInputFrontO;
        }


        /*загрузка выпадающего списка для слотов*/
        const providerSelect = document.querySelector('#provider');
        providerSelect.addEventListener('change', createGame);
    
        function createGame(){
            let provider = document.getElementById("provider");
            let container = document.getElementById("games");
            let gameSelect = document.getElementById("game");
            let gameLabel = container.querySelector('.i-info');

            if (gameSelect !== null){
                gameSelect.remove();
            }
            let options = provider.options;
            $.ajax({
                type: 'POST',
                url: '/SlotApplication/get-games',
                data: { provider: options[options.selectedIndex].id },
                success: function(data){
                    let gamesSelect = document.createElement("select");
                    gamesSelect.setAttribute("id",  "game");
                    gamesSelect.classList.add('drop-top');
                    for(let game of data){
                        let option = document.createElement("option");
                        option.classList.add('item');
                        option.value = game.rawSlot;
                        option.text = game.cleanSlot;
                        gamesSelect.appendChild(option);
                    }
                    container.insertBefore(gamesSelect, gameLabel);
                }
            });
        };
        createGame();

        const formItem = document.querySelectorAll('.required');
        formItem.forEach((item) => {
          item.addEventListener('focus', () =>{
            item.parentNode.classList.remove('warning');
          })
        });



        let adminMode;
        /*загрузка отчета*/
        const performBtn = document.querySelector('#perform');
        performBtn.addEventListener('click', getReport);

        function getReport(){
          if (this.id == 'perform'){
            adminMode = false;
          }
          if(this.id == 'adminPerform'){
            adminMode = true;
          }
          let report = document.querySelector('.report');
                        report.style.display = "none";
                        
        const forms = document.querySelectorAll('.required');
        let arrForms = [];
        forms.forEach((item) => {
          arrForms.push(item.value);
        })
       
        let check = arrForms.every((item) => {
          if(item.length > 0 && !isNaN(item)){
          return true;
          } else {
            return false;
          }
        });
        
        if(check){
      
            hasReport=0;
            let performBtn = document.getElementById('perform');   
            let adminPerform = document.getElementById('adminPerform');
            let evPerform = document.getElementById('evPerform');

            performBtn.setAttribute("disabled", "disabled"); 
            adminPerform ? adminPerform.setAttribute("disabled", "disabled") : null;
            evPerform ? evPerform.setAttribute("disabled", "disabled") : null;

            if(window.idInput !== undefined){
              let bonusWrap = document.querySelector('.bonus-wrap');
              bonusWrap.classList.remove('active');
              let hideLinkBut = document.getElementById("hideLinkButton");
              hideLinkBut.setAttribute("disabled", "disabled");
              let linkBonusBut = document.getElementById("linkBonus");
              linkBonusBut.setAttribute("disabled", "disabled");
                
            }

            getSlotInputValues();
            let expectedTimeFromServer;
            let stopWatchPlace = document.querySelector('.stopwatch-wrapper');
            let coordsStopWatch = stopWatchPlace.getBoundingClientRect();
            window.scrollTo(
              coordsStopWatch.x + window.pageXOffset,
              coordsStopWatch.y + coordsStopWatch.height
            );

            $.ajax({
                type: 'POST',
                url: '/SlotApplication/expectedTime',
                data: window.slotInputFront,
                async: false,
                success: function(data){
                    expectedTimeFromServer = data;
                    if (data !== 0 && hasReport === 0){
                        let lowBorder = Math.floor(data/60);
                        if (document.getElementById("expectedTimeLabel") === null){
                            let expectedTimeLabel = document.createElement("label");
                            expectedTimeLabel.setAttribute("id",  "expectedTimeLabel");
                            if (data < 60) {
                                expectedTimeLabel.innerText = "Ожидаемое время работы программы составляет менее 1 минуты";
                            } else {
                                if (version === 'demo' && data > 180){
                                    expectedTimeLabel.innerText = "Ожидаемое время работы программы составляет 3 минуты";
                                } else {
                                    expectedTimeLabel.innerText = "Ожидаемое время работы программы составляет " + Math.floor(data/60) + "-" + (Math.floor(data/60)+1) + " минут";
                                }
                            }
                            document.getElementById("expectedTime").appendChild(expectedTimeLabel);
                        } else {
                            if (data < 60) {
                                document.getElementById("expectedTimeLabel").innerText = "Ожидаемое время работы программы составляет менее 1 минуты";
                            } else {
                                if (version === 'demo' && data > 180){
                                    expectedTimeLabel.innerText = "Ожидаемое время работы программы составляет 3 минуты";
                                } else {
                                    document.getElementById("expectedTimeLabel").innerText = "Ожидаемое время работы программы составляет " + Math.floor(data/60) + "-" + (Math.floor(data/60)+1) + " минут";
                                }
                            }
                        }
                    }
                }
            });
            let performConfirm;
            if (expectedTimeFromServer > 600){
                performConfirm = confirm("Время ожидания расчета займет более чем " +
                    (Math.floor(expectedTimeFromServer/60)) + " минут. Уверены, что желаете продолжить ?");
            } else {
                performConfirm = true;
            }
        if (performConfirm){
            if (version === 'admin'){
                document.getElementById("reportPlace").classList.remove('hidden');
                document.getElementById("exactEVReport").classList.add('hidden');
            }
            let stopwatchDiv = document.getElementById("stopwatch");
            stopwatchDiv.classList.remove('hidden');
            StartStop();
            $.ajax({
                type: 'POST',
                url: '/SlotApplication/report',
                data: window.slotInputFront,
                success: function(data){
                  
                    
                    let emptyString = document.createElement("br");
                    let reportContainer = document.getElementById("report");
                    let reportPlace = document.getElementById("reportPlace");

                    let waitingToDelete = document.getElementById("waitingLabel");
                    if (waitingToDelete !== null){
                        waitingToDelete.remove();
                    }

                    if (data.error !== null){
                      let performBtn = document.getElementById('perform');
                      performBtn.removeAttribute("disabled", "disabled");
                      let adminPerform = document.getElementById('adminPerform');
                      adminPerform ? adminPerform.removeAttribute("disabled", "disabled") : null;
                      let evPerform = document.getElementById('evPerform');
                      evPerform ? evPerform.removeAttribute("disabled", "disabled") : null;
                      let bonusWrap = document.querySelector('.bonus-wrap');
                      bonusWrap.classList.remove('active');
                      window.idInput = null;
                      let errorMessageDiv = document.getElementById("error-message-div");
                      let errorLabel = document.createElement("label");
                      errorLabel.classList.add('error-message');
                      errorLabel.setAttribute("id", "error-message");
                      errorLabel.innerText = data.error;
                      errorMessageDiv.appendChild(errorLabel);
                    } else {
                      window.idInput = data.slotInput.idInput;
                      let report = document.querySelector('.report');
                      report.style.display = "block";
                      drawReport(data);
                      let bonusWrap = document.querySelector('.bonus-wrap');
                      bonusWrap.classList.add('active');
                        /*let timerLabel= document.getElementById("timer");
                        timerLabel.innerText = "Время работы: " + data.fullTime + "секунд";
                        

                        let slotLabel = document.getElementById("slotLabel");
                        slotLabel.innerText = "Слот: " + data.slotInformation.slot;

                        let providerLabel = document.getElementById("providerLabel");
                        providerLabel.innerText = "Провайдер: " + data.slotInformation.provider;

                        if (version !== "casino"){
                            let profitLabel = document.getElementById("profitLabel");
                            profitLabel.innerText = "PROFIT: " + data.commonSimInformation.profit;
                        }

                        let evLabel = document.getElementById("evLabel");
                        evLabel.innerText = "EV: " + data.commonSimInformation.ev;

                        let roiLabel = document.getElementById("roiLabel");
                        roiLabel.innerText = "ROI: " + data.commonSimInformation.roi;

                        let bonusDispersionLabel = document.getElementById("bonusVolatilityLabel");
                        bonusDispersionLabel.innerText = "Дисперсия бонуса: " + data.commonSimInformation.bonusVolatility;

                        let positiveProfitLabel = document.getElementById("positiveProfitLabel");
                        positiveProfitLabel.innerText = "Вероятность получить Profit больше 0 : " + data.commonSimInformation.profitProbability;

                        let nullProfitLabel = document.getElementById("nullProfitLabel");
                        nullProfitLabel.innerText = "Вероятность получить Profit  0 : " + data.commonSimInformation.nonProfitProbability;

                        let averageWinLabel = document.getElementById("averageWin");
                        if (data.commonSimInformation.averageWin === 0){
                            averageWinLabel.innerText = "";
                        } else {
                            averageWinLabel.innerText = "Средний бонусный выигрыш : " + data.commonSimInformation.averageWin;
                        }

                        let betCounterLabel = document.getElementById("betCounter");
                        if (data.commonSimInformation.betCounter === 0){
                            betCounterLabel.innerText = "";
                        } else {
                            betCounterLabel.innerText = "Количество спинов : " + data.commonSimInformation.betCounter;
                        }

                        let topProfitsLabel = document.getElementById("topProfitsLabel");
                        topProfitsLabel.innerText = "Самые крупные  Profit и их вероятности:";

                        let topProfitsDiv = document.getElementById("topProfitsDiv");
                        while (topProfitsDiv.hasChildNodes()) {
                            topProfitsDiv.removeChild(topProfitsDiv.firstChild);
                        }
                        for(let topProfits of data.topProfits){
                            let topProfitsLabel = document.createElement("label");
                            topProfitsLabel.innerText = "TOP-" + topProfits.name + ": сумма = " +
                                topProfits.profit + "; частота: раз в " + topProfits.frequency + " симуляций";
                            topProfitsDiv.appendChild(topProfitsLabel);
                            let newEmptyString = document.createElement("br");
                            topProfitsDiv.appendChild(newEmptyString);
                        }

                        let depositMultiplierLabel = document.getElementById("depositMultiplierLabel");
                        depositMultiplierLabel.innerText = "Отношение выигрыша к депозиту:";

                        let depositMultiplierDiv = document.getElementById("depositMultiplierDiv");
                        while (depositMultiplierDiv.hasChildNodes()) {
                            depositMultiplierDiv.removeChild(depositMultiplierDiv.firstChild);
                        }
                        for(let depositMultiplier of data.depositMultipliers){
                            let depositMultiplierLabel = document.createElement("label");
                            depositMultiplierLabel.innerText = "Коэффициент выигрыша " + depositMultiplier.depositMultiplier +
                                ": раз в " + depositMultiplier.depositMultiplierFrequency + " симуляций.";
                            depositMultiplierDiv.appendChild(depositMultiplierLabel);
                            let newEmptyString = document.createElement("br");
                            depositMultiplierDiv.appendChild(newEmptyString);
                        }

                        let seriesDiv = document.getElementById("series");
                        while (seriesDiv.hasChildNodes()) {
                            seriesDiv.removeChild(seriesDiv.firstChild);
                        }
                        for(let nonProfitSeries of data.nonProfitSeries){
                            let nonProfitSeriesLabel = document.createElement("label");
                            nonProfitSeriesLabel.innerText = "Серии из  Profit = " + nonProfitSeries.seriesName +
                                " раз подряд: раз в " + nonProfitSeries.series + " симуляций.";
                            seriesDiv.appendChild(nonProfitSeriesLabel);
                            let newEmptyString = document.createElement("br");
                            seriesDiv.appendChild(newEmptyString);
                        }

                        let longestNonProfitLabel = document.getElementById("longestNonProfitLabel");
                        longestNonProfitLabel.innerText = "Самая долгая безвыигрышная серия: " + data.commonSimInformation.longestNonProfitSeries;

                        let longestNonProfitFrequencyLabel = document.getElementById("longestNonProfitFrequencyLabel");
                        longestNonProfitFrequencyLabel.innerText = "Частота самой долгой безвыигрышной серии: " + data.commonSimInformation.longestNonProfitFrequency;

                        let longestProfitLabel = document.getElementById("longestProfitLabel");
                        longestProfitLabel.innerText = "Самая долгая выигрышная серия: " + data.commonSimInformation.longestProfitSeries;

                        let longestProfitFrequencyLabel = document.getElementById("longestProfitFrequencyLabel");
                        longestProfitFrequencyLabel.innerText = "Частота самой долгой выигрышной серии: " + data.commonSimInformation.longestProfitFrequency;
                        */
                    }
                    let coords = reportPlace.getBoundingClientRect();
                    window.scrollTo(
                      coords.x + window.pageXOffset,
                      coords.y + window.pageYOffset
                    );
                    StartStop();
                    ClearСlock();
                    stopwatchDiv.classList.add('hidden');
                    if(document.getElementById("expectedTimeLabel") != null){
                    document.getElementById("expectedTimeLabel").innerText="";
                    }
                    hasReport=1;
                    
                    let hideLinkButton = document.getElementById("hideLinkButton");
                    hideLinkButton.removeAttribute("disabled");
                    let linkBonusButton = document.getElementById("linkBonus");
                    linkBonusButton.removeAttribute("disabled");
                    let performBtn = document.getElementById('perform');
                    performBtn.removeAttribute("disabled");
                    let adminPerform = document.getElementById('adminPerform');
                    adminPerform ? adminPerform.removeAttribute("disabled", "disabled") : null;
                    let evPerform = document.getElementById('evPerform');
                    evPerform ? evPerform.removeAttribute("disabled", "disabled") : null;
                }
            });

            let errorMessageLabel = document.getElementById("error-message");
            if (errorMessageLabel !== null){
                errorMessageLabel.remove();
            }

            let waitingLabel = document.createElement("label");
            waitingLabel.setAttribute("id",  "waitingLabel");
            waitingLabel.innerText = "Происходит расчет. Ожидайте..."
            let successDiv = document.getElementById("message");
            successDiv.appendChild(waitingLabel);
        }

      } else {
        forms.forEach((item) =>{
          if(item.value.length == 0 || isNaN(item.value)) {
            item.parentNode.classList.add('warning');
          }
        });
        
      }
          
      }
      $('body').on('input', '.input-range', function(){
          let value = this.value.replace(/[^0-9]/g, '');
          if (value > $(this).data('max')) {
            $(this).parent().addClass('warning');
            this.value = $(this).data('max');
          } else {
            this.value = value;
            $(this).parent().removeClass('warning');
          }
        });
  
      
       /* function getPrompts(){
            $.ajax({
                type: 'GET',
                url: '/SlotApplication/get-prompts',
                success: function(data){
                  saveData(data);
                }
              });
        }*/


        /*загрузка подсказок*/
        const promptsLink1 = "/SlotApplication/get-prompts";
        fetch(promptsLink1)
        .then((response) => response.json())
        .then((data) => {
          saveData(data);
        })
        .catch((error) => console.error("Ошибка получения данных" + error));

        const promptsLink2 = "/SlotApplication/get-casino-prompts";
      fetch(promptsLink2)
        .then((response) => response.json())
        .then((data) => {
          saveData(data);
        })
        .catch((error) => console.error("Ошибка получения данных" + error));


        const iInfo = document.querySelectorAll(".i-info"),
        pointer = document.querySelector(".pointer");
        let dataPrompts = {};

      function saveData(_data) {
        if(!dataPrompts){
          return dataPrompts = _data;
        } else {
          return dataPrompts = Object.assign(dataPrompts, _data);
        }
      }

      if (iInfo) {
        window.addEventListener("scroll", (e) => {
          const findPointer = document.querySelectorAll("[data-pointer]");

          findPointer.forEach((item) => {
            item.classList.remove("active");
            item.classList.remove("activeLeft");
            item.classList.remove("activeBottom");
          });

          iInfo.forEach((del) => del.classList.remove("active-visible"));
        });
        iInfo.forEach((item) => {
          item.addEventListener("click", () => {
            const rect = item.getBoundingClientRect();

            const findPointer = document.querySelector(
              '[data-pointer="' + item.dataset.type + '"]'
            );
            const findPointerAll = document.querySelectorAll("[data-pointer]");

            let dataContent = item.dataset.content;
            console.log(dataPrompts);
            drawPrompts(dataPrompts);

            function drawPrompts(data) {
              for (key in data) {
                if (key == dataContent) {
                  let pointerContent = pointer.querySelector(".item-text");
                  pointerContent.innerHTML = "<span>" + data[key] + "</span>";
                  pointer.innerHTML =
                    '<div class="logos-row"><div class="item-text"><span>' +
                    data[key] +
                    "</span></div></div>";
                }
              }
            }

            if (findPointer) {
              iInfo.forEach((del) => del.classList.remove("active-visible"));
              item.classList.add("active-visible");

              if (document.body.offsetWidth > 1080) {
                findPointerAll.forEach((item) => {
                  item.classList.remove("active");
                  item.classList.remove("activeLeft");
                });
                if (
                  rect.left + findPointer.offsetWidth + rect.width >
                  document.body.offsetWidth
                ) {
                  findPointer.style.left =
                    rect.right -
                    findPointer.offsetWidth -
                    rect.width -
                    20 +
                    "px";
                  findPointer.classList.add("activeLeft");
                  findPointer.style.top =
                    rect.top -
                    findPointer.offsetHeight / 2 +
                    rect.height / 2 +
                    "px";
                } else {
                  findPointer.style.left = rect.left + rect.width + 20 + "px";
                  findPointer.classList.add("active");

                  findPointer.style.top =
                    rect.top -
                    findPointer.offsetHeight / 2 +
                    rect.height / 2 +
                    "px";
                }
              } else {
                findPointerAll.forEach((item) => {
                  item.classList.remove("activeBottom");
                });
                findPointer.classList.add("activeBottom");
                findPointer.style.top = rect.top + rect.height + 20 + "px";
              }
            }
          });
        });
        document.body.addEventListener("click", (e) => {
          if (!e.target.closest(".i-info")) {
            const findPointer = document.querySelectorAll("[data-pointer]");

            findPointer.forEach((item) => {
              item.classList.remove("active");
              item.classList.remove("activeLeft");
              item.classList.remove("activeBottom");
            });

            iInfo.forEach((del) => del.classList.remove("active-visible"));
          }
        });
      }
                    /*let providerPromptLabel = document.getElementById("providerPrompt");
                    providerPromptLabel.innerText = data.providerPrompt;

                    let gamesPromptLabel = document.getElementById("gamesPrompt");
                    gamesPromptLabel.innerText = data.gamesPrompt;

                    let depositPromptLabel = document.getElementById("depositPrompt");
                    depositPromptLabel.innerText = data.depositPrompt;

                    let bonusPromptLabel = document.getElementById("bonusPrompt");
                    bonusPromptLabel.innerText = data.bonusPrompt;

                    let providerFeePromptLabel = document.getElementById("providerFeePrompt");
                    providerFeePromptLabel.innerText = data.providerFeePrompt;

                    let paymentFeePromptLabel = document.getElementById("paymentFeePrompt");
                    paymentFeePromptLabel.innerText = data.paymentFeePrompt;

                    let platformFeePromptLabel = document.getElementById("platformFeePrompt");
                    platformFeePromptLabel.innerText = data.platformFeePrompt;

                    let betPromptLabel = document.getElementById("betPrompt");
                    betPromptLabel.innerText = data.betPrompt;

                    let waggerTypePromptLabel = document.getElementById("waggerTypePrompt");
                    waggerTypePromptLabel.innerText = data.waggerTypePrompt;

                    let waggerPromptLabel = document.getElementById("waggerPrompt");
                    waggerPromptLabel.innerText = data.waggerPrompt;

                    let simulationsPromptLabel = document.getElementById("simulationsPrompt");
                    simulationsPromptLabel.innerText = data.simulationsPrompt;

                    let withoutBonusPromptLabel = document.getElementById("withoutBonusPrompt");
                    withoutBonusPromptLabel.innerText = data.withoutBonusPrompt;

                    let manualPromptLabel = document.getElementById("manualPrompt");
                    manualPromptLabel.innerText = data.manualPrompt;

                    let taxesPromptLabel = document.getElementById("taxesPrompt");
                    taxesPromptLabel.innerText = data.taxesPrompt;

                    let maxProfitPromptLabel = document.getElementById("maxProfitPrompt");
                    maxProfitPromptLabel.innerText = data.maxProfitPrompt;

                    let bonusPartPromptLabel = document.getElementById("bonusPartPrompt");
                    bonusPartPromptLabel.innerText = data.bonusPartPrompt;

                    let accuracyPromptLabel = document.getElementById("accuracyPrompt");
                    accuracyPromptLabel.innerText = data.accuracyPrompt;*/
                
            
        //getPrompts();

        /*расширенные настройки ограничение максимального выигрыша*/
        function hideMaxProfit(){
            let maxProfitDiv = document.getElementById("maxProfitDiv");
            let hideButton = document.getElementById("hideButton");
            let maxProfit = document.getElementById("maxProfit");
            if (hideButton.innerText === "Добавить ограничение максимального выигрыша"){
                hideButton.innerText = "Убрать ограничение максимального выигрыша";
                maxProfitDiv.style.visibility = 'visible';
            } else {
                if (maxProfit.value !== null){
                    maxProfit.value = 0
                }
                hideButton.innerText = "Добавить ограничение максимального выигрыша";
                maxProfitDiv.style.visibility = 'hidden';
            }
        }



        /*расширенные настройки отыграть бонус частями*/
        function hideBonusPart(){
            let bonusPartDiv = document.getElementById("bonusPartDiv");
            let hideButton = document.getElementById("hideBonusButton");
            let bonusPart = document.getElementById("bonusPart");
            if (hideButton.innerText === "Бонус отыгрывается частями"){
                hideButton.innerText = "Не отыгрывать бонус частями";
                bonusPartDiv.style.visibility = 'visible';
            } else {
                if (bonusPart.value !== null){
                    bonusPart.value = 0
                }
                hideButton.innerText = "Бонус отыгрывается частями";
                bonusPartDiv.style.visibility = 'hidden';
            }
        }


        /*кнопка выпадающего меню для приявязки бонуса к отчету*/
        const hideLinkButton = document.querySelector('#hideLinkButton');
        hideLinkButton.addEventListener('click', hideBonusLink);

        function hideBonusLink(){
            let bonusLinkDiv = document.getElementById("bonusLinkDiv");
            bonusLinkDiv.classList.toggle("hidden");
            if(window.siteStructure === null || window.siteStructure === undefined){
                $.ajax({
                    type: 'GET',
                    url: '/SlotApplication/get-brands-sites',
                    success: function(data){
                        window.siteStructure = data;
                        let brandSelect = document.getElementById("brandSelect");
                        let sitesMap = new Map(Object.entries(data));
                        for(let brand of sitesMap.keys()){
                            let optionBrand = document.createElement("option");
                            optionBrand.text = brand;
                            optionBrand.classList.add('item');
                            brandSelect.appendChild(optionBrand);
                        }
                        let sites = sitesMap.get(brandSelect.value);
                        let siteSelect = document.getElementById("siteSelect");
                        for(let site of sites){
                            let optionSite = document.createElement("option");
                            optionSite.text = site;
                            optionSite.classList.add('item');
                            siteSelect.appendChild(optionSite);
                        }
                    }
                });
            }
        }


        /*выпадающий список сайтов в блоке привязать бонус*/
        const brandSelect = document.querySelector('#brandSelect');
        brandSelect.addEventListener('change', changeSites);

        function changeSites(){
            let brandSelect = document.getElementById("brandSelect");
            let prepareSitesMap = window.siteStructure;
            let sitesMap = new Map(Object.entries(prepareSitesMap));
            let sites = sitesMap.get(brandSelect.value);
            let siteSelect = document.getElementById("siteSelect");
            $("#siteSelect").empty();
            for(let site of sites){
                let optionSite = document.createElement("option");
                optionSite.text = site;
                siteSelect.appendChild(optionSite);
            }
        }


        /*кнопка привязать бонус*/
        const linkBonusBtn = document.querySelector('#linkBonus');
        linkBonusBtn.addEventListener('click', linkBonus);

        function linkBonus(){
            let linkMessage = document.getElementById("linkMessage");
            linkMessage.innerText = "";
            let brandSelect = document.getElementById("brandSelect");
            let siteSelect = document.getElementById("siteSelect");
            let link = { idBonus : window.idInput,
                         brand : brandSelect.value,
                         site : siteSelect.value
                        };
            $.ajax({
                type: 'POST',
                data: link,
                url: '/SlotApplication/create-link',
                success: function(data){
                    linkMessage.innerText = data;
                }
            });
        }
 


        /*таймер*/
        var base = 60;
           var clocktimer,dateObj,dh,dm,ds,ms;
           var readout='';
           var h=1,m=1,tm=1,s=0,ts=0,ms=0,init=0;

           //функция для очистки поля
           function ClearСlock() {
                clearTimeout(clocktimer);
                h=1;m=1;tm=1;s=0;ts=0;ms=0;
                init=0;
                readout='00:00:00';
                document.MyForm.stopwatch.value=readout;
           }

           //функция для старта секундомера
           function StartTIME() {
                var cdateObj = new Date();
                var t = (cdateObj.getTime() - dateObj.getTime())-(s*1000);
                if (t>999) { s++; }
                if (s>=(m*base)) {
                        ts=0;
                        m++;
                } else {
                        ts=parseInt((ms/100)+s);
                        if(ts>=base) { ts=ts-((m-1)*base); }
                }
                if (m>(h*base)) {
                        tm=1;
                        h++;
                } else {
                        tm=parseInt((ms/100)+m);
                        if(tm>=base) { tm=tm-((h-1)*base); }
                }
                ms = Math.round(t/10);
                if (ms>99) {ms=0;}
                if (ms==0) {ms='00';}
                if (ms>0&&ms<=9) { ms = '0'+ms; }
                if (ts>0) { ds = ts; if (ts<10) { ds = '0'+ts; }} else { ds = '00'; }
                dm=tm-1;
                if (dm>0) { if (dm<10) { dm = '0'+dm; }} else { dm = '00'; }
                dh=h-1;
                if (dh>0) { if (dh<10) { dh = '0'+dh; }} else { dh = '00'; }
                readout = dh + ':' + dm + ':' + ds ;
                document.MyForm.stopwatch.value = readout;
                clocktimer = setTimeout("StartTIME()",1000);
           }

           //Функция запуска и остановки
           function StartStop() {
                if (init==0){
                        ClearСlock();
                        dateObj = new Date();
                        StartTIME();
                        init=1;
                } else {
                        clearTimeout(clocktimer);
                        init=0;
                }
           }



/*скрытие элементов на странице в зависимости от версии*/
        function hideTaxes(){
            window.hasReport=0;
            $.ajax({
                type: 'GET',
                url: '/SlotApplication/version',
                data: {path: document.location.search},
                success: function(data){
                  console.log(data);
                    window.version =  data;
                    
                    if (data === 'demo'){
                        document.getElementById("inputSettings").style.display = "none";
                    }
                    if(data === "casino"){
                        document.getElementById("taxesDiv").style.visibility = "hidden";
                        document.getElementById("taxesDiv").disabled = true;
                    } else{
                        document.getElementById("casinoFees").style.visibility = "hidden";
                        document.getElementById("casinoFees").disabled = true;
                        document.getElementById("casinoFees").style.height = "0px";
                    }
                    if (data === 'admin'){
                        document.getElementById("accuracyDiv").classList.remove('hidden');
                        let performsDiv = document.getElementById("performsDiv");
                        let adminPerform = document.createElement("button");
                        adminPerform.id = 'adminPerform';
                        adminPerform.classList.add('btn');
                        adminPerform.innerText = "Выполнить (Админ)";
                        adminPerform.addEventListener('click', getReport);
                        performsDiv.append(adminPerform);
                        let evPerform = document.createElement("button");
                        evPerform.id = 'evPerform';
                        evPerform.classList.add('btn');
                        evPerform.classList.add('btn-ev-perform');
                        evPerform.innerText = "Найти точный EV";
                        evPerform.addEventListener('click', getExactEv);
                        performsDiv.appendChild(evPerform);
                        
                    }
                }
            });
        }


        
        function getExactEv(){
            getSlotInputValues();
            let performBtn = document.getElementById('perform');
            performBtn.setAttribute("disabled", "disabled");
            let adminPerform = document.getElementById('adminPerform');
            adminPerform.setAttribute("disabled", "disabled");
            let evPerform = document.getElementById('evPerform');
            evPerform.setAttribute("disabled", "disabled");
            let stopwatchDiv = document.getElementById("stopwatch");

            document.getElementById("exactEVReport").classList.add('hidden');
            stopwatchDiv.classList.remove('hidden');
            StartStop();

            let coordsStopWatch = stopwatchDiv.getBoundingClientRect();
                    window.scrollTo(
                      coordsStopWatch.x + window.pageXOffset,
                      coordsStopWatch.y + window.pageYOffset
                    );

            let waitingLabel = document.createElement("label");
            waitingLabel.setAttribute("id",  "waitingLabel");
            waitingLabel.innerText = "Происходит расчет. Ожидайте..."
            let successDiv = document.getElementById("message");
            successDiv.appendChild(waitingLabel);

            $.ajax({
                type: 'POST',
                url: '/SlotApplication/exact-EV',
                data: window.slotInputFront,
                success: function(data){

                    document.getElementById("reportPlace").classList.add('hidden');
                    document.getElementById("exactEVReport").classList.remove('hidden');

                    let errorMessageLabel = document.getElementById("error-ev-message");
                    if (errorMessageLabel !== null){
                        errorMessageLabel.remove();
                    }

                    let waitingToDelete = document.getElementById("waitingLabel");
                    if (waitingToDelete !== null){
                        waitingToDelete.remove();
                    }

                    StartStop();
                    ClearСlock();
                    let performBtn = document.getElementById('perform');
                    performBtn.removeAttribute("disabled", "disabled");
                    let adminPerform = document.getElementById('adminPerform');
                    adminPerform.removeAttribute("disabled", "disabled");
                    let evPerform = document.getElementById('evPerform');
                    evPerform.removeAttribute("disabled", "disabled");
                    //stopwatchDiv.style.visibility = 'hidden';
                    stopwatchDiv.classList.add('hidden');
                    document.getElementById("expectedTime").innerText="";

                    let coordsEvReport = document.getElementById("exactEVReport").getBoundingClientRect();
                    window.scrollTo(
                      coordsEvReport.x + window.pageXOffset,
                      coordsEvReport.y + window.pageYOffset
                    );

                    if (data.error !== null){
                      document.querySelectorAll('#exactEVReport .item').forEach((item) => {
                        item.classList.add('hidden');
                      });
                        let errorMessageDiv = document.getElementById("error-ev-div");
                        let errorLabel = document.createElement("label");
                        errorLabel.setAttribute("id", "error-ev-message");
                        errorLabel.innerText = data.error;
                        errorMessageDiv.appendChild(errorLabel);
                    } else {
                      document.querySelectorAll('#exactEVReport .item').forEach((item) => {
                        item.classList.remove('hidden');
                      });
                        document.getElementById("evValue").innerHTML ="Точное EV <span> " + " " + data.ev + " " + "</span>";
                        document.getElementById("simulationsEV").innerHTML ="Количество симуляций для достижения точного EV <span> " + " " + data.simulations + " " + "</span>";
                        document.getElementById("accuracyEV").innerHTML ="Точность <span> " + " " + data.accuracy + " " + "% </span>"
                    }
                }
            });
        }
    </script>
    <!--<script>
        function advanceSettings(){
            let advanceSettingsDiv = document.getElementById("advanceSettings");
            if (advanceSettingsDiv.style.display === 'none' || advanceSettingsDiv.style.display === ''){
                advanceSettingsDiv.style.display = 'block';
            } else {
                advanceSettingsDiv.style.display = 'none';
            }
        }
    </script>-->

<script>
hideTaxes();
// открытие меню навигации
const btnMenu = document.querySelector('.btn-menu')
      btnMenu.addEventListener('click', toggleMenu)

      function toggleMenu() {
        var x = document.getElementById("myTopnav");
        if (x.className === "topnav") {
          x.className += " responsive";
        } else {
          x.className = "topnav";
        }
      }

      //кнопка "Расширенные настройки"
      let btn = document.querySelectorAll('.btn-set')

btn.forEach(elem => {
    elem.addEventListener('click', () => {
        let nextElem = elem.nextElementSibling;
        if(nextElem.className == 'dropdown-settings active'){
            nextElem.classList.remove('active');
        } else {
            nextElem.classList.add('active');
        }
        if(elem.className == 'btn-set active'){
            elem.classList.remove('active');
        } else {
            elem.classList.add('active');
        }
    })
})

let accordions = document.querySelectorAll('.heading');
accordions.forEach(elem => {
    elem.addEventListener('click', () => {
        if(elem.className == 'heading active'){
            elem.classList.remove('active');
        } else {
            elem.classList.add('active');
        }
    });
});

/*отображение отчета*/

const leftBlock = document.querySelector(".left");
      const rightBlock = document.querySelector(".right");
      const averageWin = document.querySelector(".average-win");
      const betCounter = document.querySelector(".bet-counter");
      const topProfits = document.querySelector(".blocks");
      const formInput = document.querySelector(".forms");
      const bonusVolatility = document.querySelector(".bonus-volatility");
      const bonusSpeed = document.querySelector(".bonus-speed");
      const evPlayer = document.querySelector(".ev-player");
      const evCasino = document.querySelector(".ev-casino");
      const roiPlayer = document.querySelector(".roi-player");
      const roiCasino = document.querySelector(".roi-casino");
      const plus = document.querySelector(".plus");
      const zero = document.querySelector(".zero");
      const profitSeries = document.querySelector(".longest-profit-series");
      const nonProfitSeries = document.querySelector(".longest-non-profit-series");
      const nonProfitBlocks = document.querySelectorAll(".non-profit-series .block");
      const nonProfitSeriesSection = document.querySelector(".non-profit-series");

      /*let request = { url: window.location.href };
      $.ajax({
        type: "POST",
        url: '/SlotApplication/report',
        data: window.slotInputFront,
        success: function (data) {
          drawReport(data);
        },
      });*/

      function drawReport(data) {
        console.log(data);
        averageWin.textContent = parseFloat(data.commonSimInformation.averageWin.toFixed(1));//Средний бонусный выигрыш
        betCounter.textContent = parseFloat(data.commonSimInformation.betCounter.toFixed(1));//Количество спинов
        let bonusVolatilityValue = data.commonSimInformation.bonusVolatility;//Дисперсия бонуса
        bonusVolatility.textContent = parseFloat(bonusVolatilityValue.toFixed(2));
        if (bonusVolatilityValue <= 100){
        bonusSpeed.style.transform =
          "rotate(" +
          (-140 +
            (280 / 100) *
              parseFloat(bonusVolatilityValue)) +
          "deg)";
        } else {
          bonusSpeed.style.transform =
          "rotate(" +
          (-140 +
            (280 / 100) *
              100) +
          "deg)";
        }
        evPlayer.textContent = parseFloat(data.commonSimInformation.ev.toFixed(1));//EV игрока
        evCasino.textContent = -parseFloat(data.commonSimInformation.ev.toFixed(1));//EV казино
        roiPlayer.textContent = parseFloat(data.commonSimInformation.roi.toFixed(1));//ROI игрока
        roiCasino.textContent = -parseFloat(data.commonSimInformation.roi.toFixed(1));//ROI казино
        //Вероятность отыграть бонус
        plus.textContent =
          "В плюс " +
          parseFloat(data.commonSimInformation.profitProbability.toFixed(1));
        zero.textContent =
          "В ноль " +
          parseFloat(data.commonSimInformation.nonProfitProbability.toFixed(1));
        profitSeries.textContent =data.commonSimInformation.longestProfitSeries;//MAX серия выигрышей
        nonProfitSeries.textContent = data.commonSimInformation.longestNonProfitSeries;//MAX серия проигрышей

        //Самые крупные Profit и их вероятности
        const blockProfits = topProfits.querySelectorAll('.block');
        //проверяем, если есть уже элементы, то удаляем и создаем заново
          if (blockProfits.length > 1){
            blockProfits.forEach((item) => {
              item.remove();
            });
            let blockTitle = document.createElement('div');
            blockTitle.classList.add('block');
            topProfits.append(blockTitle);
            blockTitle.innerHTML = '<span></span><span class="block-title">Сумма</span><span class="block-title">Частота</span>';
            data.topProfits.forEach((item) => {            
          let block = document.createElement("div");
        block.classList.add("block");
        topProfits.append(block);
        block.innerHTML =
          "<span>Топ-" +
          item.name +
          "</span><p>" +
          item.profit.toLocaleString("ru-RU") +
          '</p><p class="sim">раз в ' +
          item.frequency.toLocaleString("ru-RU") +
          " симуляций</p>";
          });
          } else {
            data.topProfits.forEach((item) => {
          let block = document.createElement("div");
          block.classList.add("block");
          topProfits.append(block);
          block.innerHTML =
            "<span>Топ-" +
            item.name +
            "</span><p>" +
            item.profit.toLocaleString("ru-RU") +
            '</p><p class="sim">раз в ' +
            item.frequency.toLocaleString("ru-RU") +
            " симуляций</p>";
          
        });
      }
        //Входящие данные
        const provider = {
          name: "Провайдер",
          value: data.slotInformation.provider,
        };
        const slot = { name: "Слот", value: data.slotInformation.slot };
        const deposit = {
          name: "Депозит",
          value: data.slotInput.deposit.toLocaleString("ru-RU"),
        };
        const bonus = {
          name: "Бонус",
          value: data.slotInput.bonus.toLocaleString("ru-RU"),
        };
        const bet = {
          name: "Ставка",
          value: data.slotInput.bet.toLocaleString("ru-RU"),
        };
        const waggerType = {
          name: "Wagger Type",
          value: data.slotInput.waggerType,
        };
        const wagger = { name: "Wagger", value: data.slotInput.wagger };
        const sim = {
          name: "Симуляции",
          value: data.slotInput.sim.toLocaleString("ru-RU"),
        };
        const withoutBonus = {
          name: "Результат без учета бонуса",
          value: data.slotInput.withoutBonus == true ? "Да" : "Нет",
        };
        const taxes = {
          name: "Налог",
          value: data.slotInput.taxes.toLocaleString("ru-RU"),
        };
        const profitLimit = {
          name: "Максимальное значение выигрыша одной симуляции",
          value: data.slotInput.profitLimit.toLocaleString("ru-RU"),
        };
        const bonusParts = {
          name: "Количество частей бонуса",
          value: data.slotInput.bonusParts,
        };
        const arrInput = [
          provider,
          slot,
          deposit,
          bonus,
          bet,
          sim,
          wagger,
          waggerType,
          withoutBonus,
          taxes,
          profitLimit,
          bonusParts,
        ];

        let formInputs = formInput.querySelectorAll('.forms-item');
        //проверяем есть элементы с данными, если есть удаляем и создаем заново
        if(formInputs.length > 0){
          formInputs.forEach((item) => {
            item.remove();
          });
          arrInput.forEach((element) => {
          let itemForm = document.createElement("div");
          itemForm.classList.add("forms-item");
          formInput.append(itemForm);
          itemForm.innerHTML =
            "<span>" +
            element.name +
            '</span> <input type="text" value="' +
            element.value +
            '">';
        });
        } else {
          arrInput.forEach((element) => {
          let itemForm = document.createElement("div");
          itemForm.classList.add("forms-item");
          formInput.append(itemForm);
          itemForm.innerHTML =
            "<span>" +
            element.name +
            '</span> <input type="text" value="' +
            element.value +
            '">';
        });
        }


        
        

        //Блок "Шансы проиграть Х депозиоов подряд"

        let arrNonProfit = [];
        data.nonProfitSeries.forEach(item => {
          arrNonProfit.push(item.series);
        });

        const dataNonProfit = [
          {
            ser: arrNonProfit[0],
            line: arrNonProfit[0] ? (arrNonProfit[6] ? arrNonProfit[0] * 40/arrNonProfit[6] : (arrNonProfit[5] ? arrNonProfit[0] * 40/arrNonProfit[5] : (arrNonProfit[4] ? arrNonProfit[0] * 40/arrNonProfit[4] : (arrNonProfit[3] ? arrNonProfit[0] * 40/arrNonProfit[3] : (arrNonProfit[2] ? arrNonProfit[0] * 40/arrNonProfit[2] : (arrNonProfit[1] ? arrNonProfit[0] * 40/arrNonProfit[1] : 40)))))) : undefined,
            count: 5,
          },
          {
            ser: arrNonProfit[1],
            line: arrNonProfit[1] ? (arrNonProfit[6] ? arrNonProfit[1] * 40/arrNonProfit[6] + 1 : (arrNonProfit[5] ? arrNonProfit[1] * 40/arrNonProfit[5] + 1 : (arrNonProfit[4] ? arrNonProfit[1] * 40/arrNonProfit[4] + 1 : (arrNonProfit[3] ? arrNonProfit[1] * 40/arrNonProfit[3] + 1 : (arrNonProfit[2] ? arrNonProfit[1] * 40/arrNonProfit[2] + 1 : 40))))) : undefined,
            count: 10
          },
          {
            ser: arrNonProfit[2],
            line: arrNonProfit[2] ? (arrNonProfit[6] ? arrNonProfit[2] * 40/arrNonProfit[6] + 2: (arrNonProfit[5] ? arrNonProfit[2] * 40/arrNonProfit[5] + 2 : (arrNonProfit[4] ? arrNonProfit[2] * 40/arrNonProfit[4] + 2 : (arrNonProfit[3] ? arrNonProfit[2] * 40/arrNonProfit[3] + 2 : 40)))) : undefined,
            count: 25
          },
          {
            ser: arrNonProfit[3],
            line: arrNonProfit[3] ? (arrNonProfit[6] ? arrNonProfit[3] * 40/arrNonProfit[6] + 3 : (arrNonProfit[5] ? arrNonProfit[3] * 40/arrNonProfit[5] + 3 : (arrNonProfit[4] ? arrNonProfit[3] * 40/arrNonProfit[4] : 40))) : undefined,
            count: 50
          },
          {
            ser: arrNonProfit[4],
            line: arrNonProfit[4] ? (arrNonProfit[6] ? arrNonProfit[4] * 40/arrNonProfit[6] : (arrNonProfit[5] ? arrNonProfit[4] * 40/arrNonProfit[5] : 40)) : undefined,
            count: 100
          },
          {
            ser: arrNonProfit[5],
            line: arrNonProfit[5] ? (arrNonProfit[6] ? arrNonProfit[5] * 40/arrNonProfit[6] : 40) : undefined,
            count: 250
          },
          {
            ser: arrNonProfit[6],
            line: arrNonProfit[6] ? 40 : undefined,
            count: 500
          },
        ]


        const profitLineBlocks = document.querySelector('.non-profit-series');

        const profitLineFunc = (dataBlocks) => {
          if(profitLineBlocks){
            profitLineBlocks.innerHTML = '';

            dataBlocks.forEach((item, index, arr) => {
              if (item.ser){
                let elem = '<div class="block"><div class="fail-row">Частота</div><div class="count count-ser">' 
                  + item.ser 
                  + '</div><div class="block-lines"></div><div class="count">' 
                  + item.count 
                  + '</div><div class="frequency">Проигрыши подряд</div></div>';
                  profitLineBlocks.insertAdjacentHTML('beforeend', elem);
                }
            });
           
            profitLineBlocks.querySelectorAll('.block-lines').forEach((item, index, arr) => {
              
              for (let i=0; i< dataBlocks[index].line; i++){
                
                let elemLine = '<div class="block-lines-item"></div>';
                item.insertAdjacentHTML('beforeend', elemLine);
              }
              
            })
          }
        };

        profitLineFunc(dataNonProfit);

        nonProfitSeriesSection.addEventListener("mouseenter", (e) => {
          nonProfitBlocks.forEach((item) => {
            item.classList.add("activeOpacity");
            item.addEventListener("mouseenter", () => {
              item.classList.add("active");
            });
            item.addEventListener("mouseleave", () => {
              item.classList.remove("active");
            });
          });
        });
        nonProfitSeriesSection.addEventListener("mouseleave", (e) => {
          nonProfitBlocks.forEach((del) => del.classList.remove("active"));
          nonProfitBlocks.forEach((del) =>
            del.classList.remove("activeOpacity")
          );
        });

        //Блок Дисперсия бонуса
        const plusValue = data.commonSimInformation.profitProbability;
        const zeroValue = data.commonSimInformation.nonProfitProbability;

        const myChartRadius = new ChartRadius({
          canvas: myCanvas,
          data: {
            plus: plusValue,
            empty1: 2,
            zero: zeroValue,
            empty2: 2,
          },
          colors: ["#FFEEC2", "#181818", "#FFB800", "#181818"],
          hole: 0.6,
        });

        myChartRadius.draw();


        const blockDM = data.depositMultipliers;
        let dataDM = [];
        
        blockDM.forEach((elem,i,arr) => {
          let DMValue = elem.depositMultiplierFrequency;
          if (DMValue > 0){
            let DMMultiplier = elem.depositMultiplier;
            dataDM.push( {
            sim: DMValue,
            dep: DMValue,
            depLine: DMMultiplier,
            linePos:60
            });
          }
        })
        console.log(dataDM);
        if(dataDM.length > 6){
          for (let i = 0; i<dataDM.length; i++){
          dataDM[i].dep = dataDM[i].dep ? (dataDM[i].dep*100/dataDM[dataDM.length -1].dep) : undefined;
        }
        } else {
          for (let i = 0; i<dataDM.length; i++){
            console.log(dataDM[i].dep);
            console.log(dataDM[dataDM.length -1].dep);
            dataDM[i].dep = dataDM[i].dep ? (dataDM[i].dep*200/dataDM[dataDM.length -1].dep) : undefined;
          }
        }

        const aarDepositMultipliers = [];
        data.depositMultipliers.forEach(item => {
          aarDepositMultipliers.push(item.depositMultiplierFrequency);
        })
        const dataBlocksValue = [
        {
            sim: aarDepositMultipliers[0],
            dep: aarDepositMultipliers[0] ? (aarDepositMultipliers[6] ? aarDepositMultipliers[0]*100/aarDepositMultipliers[6] : (aarDepositMultipliers[5] ? aarDepositMultipliers[0]*100/aarDepositMultipliers[5] : (aarDepositMultipliers[4] ? aarDepositMultipliers[0]*100/aarDepositMultipliers[4] : (aarDepositMultipliers[3] ? aarDepositMultipliers[0]*100/aarDepositMultipliers[3] : (aarDepositMultipliers[2] ? aarDepositMultipliers[0]*100/aarDepositMultipliers[2] : (aarDepositMultipliers[1] ? aarDepositMultipliers[0]*100/aarDepositMultipliers[1] : 120)))))) : undefined,
            depLine: 1,
            linePos: 77,
          },
          {
            sim: aarDepositMultipliers[1],
            dep: aarDepositMultipliers[1] ? (aarDepositMultipliers[6] ? aarDepositMultipliers[1]*100/aarDepositMultipliers[6] +5 : (aarDepositMultipliers[5] ? aarDepositMultipliers[1]*100/aarDepositMultipliers[5] : (aarDepositMultipliers[4] ? aarDepositMultipliers[1]*100/aarDepositMultipliers[4] : (aarDepositMultipliers[3] ? aarDepositMultipliers[1]*100/aarDepositMultipliers[3] : (aarDepositMultipliers[2] ? aarDepositMultipliers[1]*100/aarDepositMultipliers[2] : 120))))) : undefined,
            depLine: 2,
            linePos: 50,
          },
          {
            sim: aarDepositMultipliers[2],
            dep: aarDepositMultipliers[2] ? (aarDepositMultipliers[6] ? aarDepositMultipliers[2]*100/aarDepositMultipliers[6] + 10: (aarDepositMultipliers[5] ? aarDepositMultipliers[2]*100/aarDepositMultipliers[5] : (aarDepositMultipliers[4] ? aarDepositMultipliers[2]*100/aarDepositMultipliers[4] : (aarDepositMultipliers[3] ? aarDepositMultipliers[2]*100/aarDepositMultipliers[3] : 120)))) : undefined,
            depLine: 5,
            linePos: 55,
          },
          {
            sim: aarDepositMultipliers[3],
            dep: aarDepositMultipliers[3] ? (aarDepositMultipliers[6] ? aarDepositMultipliers[3]*100/aarDepositMultipliers[6] +15 : (aarDepositMultipliers[5] ? aarDepositMultipliers[3]*100/aarDepositMultipliers[5] + 10 : (aarDepositMultipliers[4] ? aarDepositMultipliers[3]*100/aarDepositMultipliers[4] + 10 : 120))) : undefined,
            depLine: 10,
            linePos: 55,
          },
          {
            sim: aarDepositMultipliers[4],
            dep: aarDepositMultipliers[4] ? (aarDepositMultipliers[6] ? aarDepositMultipliers[4]*100/aarDepositMultipliers[6] + 20 : (aarDepositMultipliers[5] ? aarDepositMultipliers[4]*100/aarDepositMultipliers[5] + 15 : 120)) : undefined,
            depLine: 20,
            linePos: 60,
          },
          {
            sim: aarDepositMultipliers[5],
            dep: aarDepositMultipliers[5] ? (aarDepositMultipliers[6] ? aarDepositMultipliers[5]*100/aarDepositMultipliers[6] + 25 :  120) : undefined,
            depLine: 50,
            linePos: 65,
          },
          {
            sim: aarDepositMultipliers[6],
            dep: aarDepositMultipliers[6] ? 120 : undefined,
            depLine: 100,
            linePos: 80,
          },
        ];
        charLineFunc(dataDM);
      }

      //canvas для блока "Дисперсия бонуса"
      const myCanvas = document.getElementById("chartRadius");
      myCanvas.width = 220;
      myCanvas.height = 220;

      function drawPieSlice(
        ctx,
        centerX,
        centerY,
        radius,
        startAngle,
        endAngle,
        color
      ) {
        ctx.fillStyle = color;
        ctx.beginPath();
        ctx.moveTo(centerX, centerY);
        ctx.arc(centerX, centerY, radius, startAngle, endAngle);
        ctx.closePath();
        ctx.fill();
      }

      const ChartRadius = function (options) {
        this.options = options;
        this.canvas = options.canvas;
        this.ctx = this.canvas.getContext("2d");
        this.colors = options.colors;

        this.draw = function () {
          let total_value = 0;
          let color_index = 0;
          for (let value in this.options.data) {
            let val = this.options.data[value];
            total_value += val;
          }

          let start_angle = 55;
          for (value in this.options.data) {
            let val = this.options.data[value];
            let slice_angle = (2 * Math.PI * val) / total_value;

            drawPieSlice(
              this.ctx,
              this.canvas.width / 2,
              this.canvas.height / 2,
              Math.min(this.canvas.width / 2, this.canvas.height / 2),
              start_angle,
              start_angle + slice_angle,
              this.colors[color_index % this.colors.length]
            );

            start_angle += slice_angle;
            color_index++;
          }

          if (this.options.hole) {
            drawPieSlice(
              this.ctx,
              this.canvas.width / 2,
              this.canvas.height / 2,
              this.options.hole *
                Math.min(this.canvas.width / 2, this.canvas.height / 2),
              0,
              2 * Math.PI,
              "#181818"
            );
          }
        };
      };

      //Блок "Вероятность увеличить депозит"
      const chartLineBlocks = document.querySelector(
        ".chart-line-blocks .block-wrap"
      );

      const charLineFunc = (dataBlocks) => {
        if (chartLineBlocks) {
          chartLineBlocks.innerHTML = "";

          let countLength = 0;

          dataBlocks.forEach((item, index, arr) => {
            countLength = arr.length - index;

            if (arr.length > 6){
              document.querySelector('.wrap-chart-line').style.minHeight = '274px';
            }
            if(arr.length > 10){
              document.querySelector('.right').style.maxWidth = '890px';
            }
            if(arr.length > 15){
              document.querySelector('.chart-block-hid').style.overflowX = 'scroll';
              document.querySelector('.chart-line-blocks').style.justifyContent = 'flex-start';
            }
            console.log(dataBlocks);
            if (item.sim != undefined){
            const element =
              '<div class="block ' +
              (arr.length - 1 === index ? "lst" : "") + " " +
              (arr.length > 6 && arr.length <= 10 ? "compressed" : "") +
              (arr.length > 10  ? "max-compressed" : "") +
              '" style="z-index:' +
              countLength +
              ';"><div class="top"><span>' +
              (item.sim == 0 || item.sim == "166 666" ? "-" : item.sim) +
              '</span><p>симуляций</p></div><div class="bottom">x' +
              item.depLine +
              'dep</div><div class="linear"></div></div>';
            chartLineBlocks.insertAdjacentHTML("beforeend", element);
              }
          });

          chartLineBlocks
            .querySelectorAll(".block")
            .forEach((item, index, arr) => {
              countLength = arr.length - index;
              const elementPos =
                '<div class="linePos" style="left:' +
                (item.offsetLeft + item.offsetWidth / 2 - 3) +
                "px; bottom: calc((20% + " +
                dataBlocks[index].dep +
                'px) - 2px);"></div>';
              chartLineBlocks.insertAdjacentHTML("beforeend", elementPos);
            });

          const linePosItems = document.querySelectorAll(".linePos");
          const topItems = document.querySelectorAll(".top span");
          const bottomItems = document.querySelectorAll(".bottom");

          chartLineBlocks.querySelectorAll(".block").forEach((el, index) => {
            el.addEventListener("mouseenter", () => {
              linePosItems[index].style.transform = "scale(2)";
              linePosItems[index].style.background = "#FFB800";
              topItems[index].style.color = "#FFB800";
              bottomItems[index].style.color = "#FFB800";
            });
            el.addEventListener("mouseleave", () => {
              linePosItems[index].style.transform = "scale(1)";
              linePosItems[index].style.background = "#fff";
              topItems[index].style.color = "rgba(255, 255, 255, 0.6)";
              bottomItems[index].style.color = "rgba(255, 255, 255, 0.6)";
            });
          });

          linePosItems.forEach((item, index, arr) => {
            let dot1 = linePosItems[index];
            let dot2 = linePosItems[index + 1] ? linePosItems[index + 1] : null;

            if (dot1 && dot2) {
              let choords1 = dot1.getBoundingClientRect();
              let choords2 = dot2.getBoundingClientRect();

              // get choords
              let x1 = choords1.left;
              let y1 = choords1.top;
              let x2 = choords2.left;
              let y2 = choords2.top;

              // line width
              let line = document.querySelectorAll(".linear")[index];
              const calcPos = dataBlocks[index].dep;

              line.style.bottom = "calc(20% + " + calcPos + "px)";
              let hypotenuse = getChoordsWidth(x1, y1, x2, y2);

              let leg = x2 - x1;
              let angle = Math.acos(leg / hypotenuse);

              line.style.width = hypotenuse + "px";
              line.style.transform =
                dot1.offsetTop > dot2.offsetTop
                  ? "rotate(-" + angle + "rad)"
                  : "rotate(" + angle + "rad)";

              function getChoordsWidth(x1, y1, x2, y2) {
                let a1 = Math.pow(x2 - x1, 2);
                let a2 = Math.pow(y2 - y1, 2);
                let res = a1 + a2;

                return Math.sqrt(res);
              }
            }
          });
        }
      };

 

</script>
</body>
</html>