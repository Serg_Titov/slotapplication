<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="by.minsk.miroha.entities.front.SpinnerFront"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html lang="en" pageEncoding="UTF-8">
<head>
    <meta charset="UTF-8">
    <title>Статистика спинов</title>
    <style>
        #menu {
            margin-left: 30%;
            position: absolute;
            top: 2em;
        }
        .menu-li {
            display: inline;
            margin: -2.5;
        }
        .menu-ul {
            list-style: none;
            margin: 0;
            padding-left: 0;
        }
        .menu-a {
            text-decoration: none;
            padding: 1em;
            background: #f5f5f5;
            border: 1px solid #b19891;
            color: #695753;
        }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script>
        function perform(){
            let spinNumberC = document.getElementById("spinNumber");
            let simNumberC = document.getElementById("simNumber");
            let spinnerFront = {
                spinLimit: spinNumberC.value,
                simulations: simNumberC.value,
                result: ""
            };

            $.ajax({
                type: 'POST',
                url: '/SlotApplication/spinner-home',
                data: spinnerFront,
                success: function(data){
                    let waitingToDelete = document.getElementById("waitingLabel");
                    if (waitingToDelete !== null){
                        waitingToDelete.remove();
                    }

                    let resultLabel = document.getElementById("result");
                    resultLabel.innerText = "Результат,% : " + data.result;
                }

            });
            let waitingLabel = document.createElement("label");
            waitingLabel.setAttribute("id",  "waitingLabel");
            waitingLabel.innerText = "Происходит расчет. Ожидайте..."
            let successDiv = document.getElementById("message");
            successDiv.appendChild(waitingLabel);
        }
    </script>
    <script>
        function enabledPerform(){

            $.ajax({
                type: 'GET',
                url: '/SlotApplication/perform-enabled',
                success: function(data){
                    let performButton = document.getElementById("perform");
                    if(data){
                        performButton.disabled  = false;
                    } else {
                        performButton.disabled = true;
                        let performDiv = document.getElementById("performDisabled");
                        let disabledPerform = document.createElement("label");
                        disabledPerform.innerText = "Сперва необходимо загрузить базу из файла";
                        performDiv.appendChild(disabledPerform);
                        let redirectionMenu = document.getElementById("redirection");
                        redirectionMenu.style.visibility = "visible";
                    }
                },
                error: function(error){
                    let performButton = document.getElementById("perform");
                    performButton.style.disabled = true;
                    let performDiv = document.getElementById("performDisabled");
                    let disabledPerform = document.createElement("label");
                    disabledPerform.innerText = "Сперва необходимо загрузить базу из файла";
                    performDiv.appendChild(disabledPerform);
                    let redirectionMenu = document.getElementById("redirection");
                    redirectionMenu.style.visibility = "visible";
                }
            });
        };
    </script>
</head>
<body>
    <div id="menu">
        <nav>
            <ul class="menu-ul">
                <li class="menu-li"><a class="menu-a" href='/SlotApplication/'>Home</a></li>
                <li class="menu-li"><a class="menu-a" href='/SlotApplication/tools'>Tools</a></li>
                <li class="menu-li"><a class="menu-a" href='/SlotApplication/slots'>Slots</a></li>
                <li class="menu-li"><a class="menu-a" href='/SlotApplication/jackpots'>Jackpots</a></li>
            </ul>
        </nav>
    </div>
    <script>enabledPerform();</script>
    <br>
    <label>Количество спинов: </label>
    <input type="text" id="spinNumber" value="${spinnerFront.spinLimit}"/><br>

    <label>Количество симуляций: </label>
    <input type="text" id="simNumber" value="${spinnerFront.simulations}"/><br>

    <br>
    <button onclick="perform()" id="perform">Рассчитать введенный спин</button><br>
    <small>Sim * Spin = 1 000 000 000 будет выполняться около 5 мин</small><br>
    <small>Sim * Spin = 10 000 000 000 будет выполняться около 35 мин</small><br>
    <br>
    <div id="performDisabled"></div>
    <div id="redirection" style="visibility:hidden">
        <button onclick="location.href='/SlotApplication/upload'">Загрузить слот</button>
        <button onclick="location.href='/SlotApplication/calc-home-db'">Рассчитать слот</button>
    </div>
    <label id="result"></label>
    <div id="message"></div>
<br>
<button onclick="location.href='/SlotApplication/'">Вернуться в меню</button>
</body>
</html>