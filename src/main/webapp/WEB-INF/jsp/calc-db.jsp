<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="by.minsk.miroha.entities.databases.SlotName"%>
<%@ page import="java.util.List"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html lang="en" pageEncoding="UTF-8">
<head>
    <meta charset="UTF-8">
    <title>Загрузка информации в базу</title>
    <style>
        #menu {
            margin-left: 30%;
            position: absolute;
            top: 2em;
        }
        .menu-li {
            display: inline;
            margin: -2.5;
        }
        .menu-ul {
            list-style: none;
            margin: 0;
            padding-left: 0;
        }
        .menu-a {
            text-decoration: none;
            padding: 1em;
            background: #f5f5f5;
            border: 1px solid #b19891;
            color: #695753;
        }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script>
        function createGame(){
            let provider = document.getElementById("provider");
            let container = document.getElementById("games");
            let gameSelect = document.getElementById("game");
            if (gameSelect !== null){
                gameSelect.remove();
            }
            $.ajax({
                type: 'POST',
                url: '/SlotApplication/get-games',
                data: { provider: provider.value },
                success: function(data){
                    let reportPlace = document.getElementById("provider");
                    let gamesSelect = document.createElement("select");
                    gamesSelect.setAttribute("id",  "game");
                    for(let game of data){
                        let option = document.createElement("option");
                        option.value = game.rawSlot;
                        option.text = game.cleanSlot;
                        gamesSelect.appendChild(option);
                    }
                    container.appendChild(gamesSelect);
                }
            });
        };
    </script>
    <script>
        function upload(){
            let providerC = document.getElementById("provider");
            let gameC = document.getElementById("game");
            let slotNameO = {
                slotName: gameC.value,
                provider: providerC.value
            };

            $.ajax({
                 type: 'POST',
                 url: '/SlotApplication/calc-db',
                 data: slotNameO,
                 success: function(data){
                    let reportPlace = document.getElementById("report-place");
                    if (data.message !== null){
                        let errorDiv = document.getElementById("error-message-div");
                        let errorLabel = document.createElement("label");
                        errorLabel.innerText = data.message.message;
                        errorDiv.appendChild(errorLabel);
                    } else {
                        let slotLabel = document.getElementById("slotLabel");
                        slotLabel.innerText = "Слот: " + data.slotInformation.slot;

                        let providerLabel = document.getElementById("providerLabel");
                        providerLabel.innerText = "Провайдер: " + data.slotInformation.provider;

                        let totalSpinsLabel = document.getElementById("totalSpins");
                        totalSpinsLabel.innerText = "Total spins in database: " + data.commonSlotInformation.spinAmount;

                        let maxWinLabel = document.getElementById("maxWin");
                        maxWinLabel.innerText = "Max win in database: " + data.commonSlotInformation.maxWin;

                        let rtpLabel = document.getElementById("rtp");
                        rtpLabel.innerText = "RTP: " + data.commonSlotInformation.rtp;

                        let bonusVolatilityLabel = document.getElementById("bonusVolatility");
                        bonusVolatilityLabel.innerText = "Volatility: " + data.commonSlotInformation.volatility;

                        let bonusFrequencyLabel = document.getElementById("bonusFrequency");
                        bonusFrequencyLabel.innerText = "Бонус выпадает раз в " + data.commonSlotInformation.bonusFrequency + " симуляций";

                        let bonusWinsRateLabel = document.getElementById("bonusWinsRate");
                        bonusWinsRateLabel.innerText = "Доля выигрышей с бонусами: " + data.commonSlotInformation.bonusWinsRate;

                        let winsWithoutBonusRateLabel = document.getElementById("winsWithoutBonusRate");
                        winsWithoutBonusRateLabel.innerText = "Доля выигрышей без бонусов: " + data.commonSlotInformation.winsWithoutBonusRate;

                        let averageBonusWinLabel = document.getElementById("averageBonusWin");
                        if (data.commonSlotInformation.averageBonusWin === 0){
                            averageBonusWinLabel.innerText ="";
                        } else {
                            averageBonusWinLabel.innerText = "Средний выигрыш в бонусной игре: " + data.commonSlotInformation.averageBonusWin;
                        }

                        let multipliersFrequencyLabel = document.getElementById("multipliersFrequency");
                        multipliersFrequencyLabel.innerText = "Частота выпадения множителей в базе данных:";

                        let multipliersFrequencyDiv = document.getElementById("multipliersFrequencyDiv");
                        while (multipliersFrequencyDiv.hasChildNodes()) {
                            multipliersFrequencyDiv.removeChild(multipliersFrequencyDiv.firstChild);
                        }
                        for(let multiplier of data.multipliers){
                            let multiplierLabel = document.createElement("label");
                            multiplierLabel.innerText = "Множитель больше " + multiplier.multiplier +
                                 ": раз в " + multiplier.multiplierCount + " симуляций.";
                            multipliersFrequencyDiv.appendChild(multiplierLabel);
                            let newEmptyString = document.createElement("br");
                            multipliersFrequencyDiv.appendChild(newEmptyString);
                        }

                        if (data.slotPoints !== null){
                            let slotPointsLabel = document.getElementById("slotPointsLabel");
                            slotPointsLabel.innerText = "Для данного слота следующие точки: ";

                            let slotPointsDiv = document.getElementById("slotPointsDiv");
                            while (slotPointsDiv.hasChildNodes()) {
                                slotPointsDiv.removeChild(slotPointsDiv.firstChild);
                            }
                            for(let slotPoint of data.slotPoints){
                                let slotPointLabel = document.createElement("label");
                                slotPointLabel.innerText = "SpinLimit = " + slotPoint.spinLimit +
                                    "; Result = " + slotPoint.pointResult;
                                slotPointsDiv.appendChild(slotPointLabel);
                                let newEmptyString = document.createElement("br");
                                slotPointsDiv.appendChild(newEmptyString);
                            }
                        }
                    }

                    let waitingToDelete = document.getElementById("waitingLabel");
                    if (waitingToDelete !== null){
                        waitingToDelete.remove();
                    }

                 }
            });
            let waitingLabel = document.createElement("label");
            waitingLabel.setAttribute("id",  "waitingLabel");
            waitingLabel.innerText = "Происходит расчет. Ожидайте..."
            let successDiv = document.getElementById("message");
            successDiv.appendChild(waitingLabel);
        }
    </script>
</head>
<body>
<%  List<String> providers = (List<String>) request.getAttribute("providers");
%>
    <div id="menu">
        <nav>
            <ul class="menu-ul">
                <li class="menu-li"><a class="menu-a" href='/SlotApplication/'>Home</a></li>
                <li class="menu-li"><a class="menu-a" href='/SlotApplication/tools'>Tools</a></li>
                <li class="menu-li"><a class="menu-a" href='/SlotApplication/slots'>Slots</a></li>
                <li class="menu-li"><a class="menu-a" href='/SlotApplication/jackpots'>Jackpots</a></li>
            </ul>
        </nav>
    </div>
    <label>Выберите провайдера: <label>
    <select id="provider" onchange="createGame()">
<%      for(String provider : providers){
%>          <option><%=provider%></option>
<%      }
%>  </select>
     <br>
     <label>Выберите игру: <label>
     <div id="games"></div>
     <script> createGame();</script>
    <br>
    <br>
    <button id="upload" onclick="upload()">Рассчитать</button>
    <br>
    <br>
    <button onclick="location.href='/SlotApplication/'">Вернуться в меню</button>
    <br>
    <br>
    <div id="message"></div><br><br>
    <div id="report-place">
        <div id="error-message-div"></div>
        <div id="report">
            <label id="slotLabel"></label><br>
            <label id="providerLabel"></label><br><br>
            <label id="totalSpins"></label><br><br>
            <label id="maxWin"></label><br>
            <label id="rtp"></label><br>
            <label id="bonusVolatility"></label><br>
            <label id="bonusFrequency"></label><br>
            <label id="bonusWinsRate"></label><br>
            <label id="winsWithoutBonusRate"></label><br>
            <label id="averageBonusWin"></label><br><br>
            <label id="multipliersFrequency"></label><br>
            <div id="multipliersFrequencyDiv"></div><br><br>

            <label id="slotPointsLabel"></label><br>
            <div id="slotPointsDiv"></div><br>
        </div>
    </div>
</body>
</html>