<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en" pageEncoding="UTF-8">
  <head>
    <meta charset="UTF-8" />
    <title>Jackpots</title>
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
      href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800;900&display=swap"
      rel="stylesheet"
    />
    <link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&family=Montserrat:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <link
      rel="stylesheet"
      href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0"
    />
    <link
      rel="stylesheet"
      type="text/css"
      href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"
    />

    <style>
      html {
        height: 100%;
        scroll-behavior: smooth;
      }
      * {
        box-sizing: border-box;
      }
      body {
        margin: 0;
        font-family: "Montserrat", sans-serif;
        font-size: 14px;
        background-color: #181818;
        color: #f5f5f5;
        background-image: url("https://bonusev.com/images/section-bg.png");
        background-attachment: fixed; /*фиксируем фоновую картинку для эффекта параллакс*/
        background-size: cover;
        background-repeat: no-repeat;
      }

      /*общий grid*/
      .grid {
        display: grid;
        grid-template-columns: 0.5fr 1fr 2fr 1fr 0.5fr;
        grid-template-rows: auto 1fr auto;
        justify-content: center;
        align-content: flex-start;
        justify-items: center;
        box-sizing: border-box;
        min-height: 100%;
        min-width: 320px;
      }
      /*общий flex*/
      .flex {
        display: flex;
        justify-content: center;
        flex-flow: row wrap;
      }
      /*flex-контейнер*/
      .flex-row {
        display: flex;
        flex-flow: row nowrap;
        width: 70%;
        justify-content: space-between;
      }
      /*flex-контейнер*/
      .flex-column {
        display: flex;
        max-width: 1200px; /*ширина контента*/
        flex-flow: column nowrap;
        margin: 0 auto;
        align-items: center;
      }

      /*Стили для Header и меню навигации*/
      header {
        grid-column: 3/4;
        grid-row: 1/2;
        width: 100%;
        height: 100px;
        color: #fff;
      }
      .empty {
        /*пустой div*/
        height: 40px;
      }
      nav {
        width: 100%;
        box-sizing: border-box;
        display: flex;
        flex-flow: row nowrap;
        height: 40px;
        justify-content: space-around;
        transition: background-color 0.5s;
        z-index: 10;
        transition: all 0.8s ease 0s;
      }
      /*список меню навигации*/
      ul.topnav {
        display: flex;
        list-style-type: none;
        width: 100%;
        justify-content: center;
        align-items: center;
        margin: 0;
        padding: 0;
      }
      /*липкий блок с меню*/
      nav.sticky {
        position: fixed;
        top: 0;
        width: 100%;
        background-color: #010101;
      }
      ul.topnav li {
        text-align: center;
        background: #262626;
        border-radius: 2px 2px;
        height: auto;
        perspective: 900px;
        margin-top: -40px;
      }
      /*style for rotate point of menu*/
      .roll-link {
        height: auto;
        display: block;
        width: 100px;
        transform-style: preserve-3d;
      }
      .roll-link .front {
        height: 40px;
        background: rgb(255, 184, 0);
        color: #262626;
      }
      .sticky .roll-link .front {
        background-color: #010101;
        color: #f5f5f5;
      }
      .roll-link .back {
        opacity: 0;
        height: 40px;
        display: block;
        background: #262626;
        color: #f5f5f5;
        transform: translate3d(0, 0, -40px) rotate3d(1, 0, 0, 90deg);
      }
      .topnav li:first-child .roll-link .front,
      .topnav li:first-child .roll-link .back {
        border-radius: 5px 0px 0px 5px;
      }
      .topnav li:last-child .roll-link .front,
      .topnav li:last-child .roll-link .back {
        border-radius: 0px 5px 5px 0px;
      }
      .roll-link > a {
        display: block;
        position: absolute;
        width: 100px;
        text-decoration: none;
        padding: 1em;
        transition: all 0.3s ease-out;
        transform-origin: 50% 0%;
        font-size: 16px;
        line-height: 10px;
      }
      .topnav .roll-link:hover .front {
        transform: translate3d(0, 40px, 0) rotate3d(1, 0, 0, -90deg);
        backface-visibility: hidden;
        perspective: 1000;
        opacity: 0;
      }
      .topnav .roll-link:hover .back {
        transform: rotate3d(1, 0, 0, 0deg);
        backface-visibility: hidden;
        perspective: 1000;
        opacity: 1;
      }
      nav .btn-menu {
        display: none;
      }

      /* Preloader */
      .preloader {
        background-color: transparent;
        color: #fff;
        display: none;
        left: 0;
        position: fixed;
        top: 0;
        height: 100%;
        width: 100%;
        z-index: 10;
      }
      .preloader.active {
        display: block;
      }
      .loader-16 {
        width: 48px;
        height: 48px;
        border-radius: 50%;
        display: inline-block;
        position: relative;
        background: linear-gradient(
          0deg,
          rgba(255, 184, 0, 0.2) 33%,
          rgb(255, 184, 0) 100%
        );
        -webkit-animation: rotation 1s linear infinite;
        animation: rotation 1s linear infinite;
      }
      .loader-16:after {
        content: "";
        position: absolute;
        left: 50%;
        top: 50%;
        transform: translate(-50%, -50%);
        width: 44px;
        height: 44px;
        border-radius: 50%;
        background: #010101;
      }
      @-webkit-keyframes rotation {
        0% {
          transform: rotate(0deg);
        }
        100% {
          transform: rotate(360deg);
        }
      }
      @keyframes rotation {
        0% {
          transform: rotate(0deg);
        }
        100% {
          transform: rotate(360deg);
        }
      }
      .status,
      .load-6 {
        position: absolute;
        left: 50%;
        top: 60%;
        background-repeat: no-repeat;
        background-position: center;
        transform: translate(-50%, -60%);
      }
      .letter-holder {
        padding: 16px;
        color: #ffb800;
      }
      .load-6 {
        margin-top: 50px;
      }

      #button {
        display: inline-block;
        background-color: #ffb800;
        width: 48px;
        height: 48px;
        text-align: center;
        border-radius: 4px;
        position: fixed;
        bottom: 30px;
        right: 30px;
        transition: background-color .3s, 
          opacity .5s, visibility .5s;
        opacity: 0;
        visibility: hidden;
        z-index: 1000;
      }
      #button::after {
        content: "";
        background-image: url('https://img.icons8.com/material-sharp/48/181818/chevron-up.png');
        width: 48px;
        height: 48px;
        position: absolute;
        top: 0;
        left: 0;
      }
      #button:hover {
        cursor: pointer;
        background-color: #ffb800;
      }
      #button:active {
        background-color: #333333;
      }
      #button.show {
        opacity: 1;
        visibility: visible;
      }

      /*основной блок с данными*/
      main {
        grid-column: 2/5;
        grid-row: 2/3;
        width: 100%;
        /*margin-top: 25vh;*/
      }
      main.hide {
        display: none;
      }
      button {
        font-family: "Montserrat", sans-serif;
      }
      .slot-list,
      .first-row,
      .second-row,
      .grow-row,
      .table-row,
      .slot-list-wrapper .title {
        max-width: 1400px;
        min-width: 0;
      }
      .slot-list-wrapper {
        display: flex;
        flex-flow: column;
        justify-content: center;
        align-items: center;
        min-width: 0;
      }
      /*.slot-list-wrapper .title {
        width: 90%;
      }*/
      .slot-list-wrapper .title p {
        font-size: 24px;
        color: #f5f5f5;
        margin-left: 60px;
        margin-bottom: 10px;
        font-family: 'Bebas Neue';
      }
      .slot-list {
        width: 90%;
        display: flex;
        flex-flow: row wrap;
      }
      .slot-list-item {
        z-index: 1;
        position: relative;
        opacity: 0.7;
        cursor: pointer;
        overflow: hidden;
        padding: 10px 5px 5px 5px;
        transition: all .3s ease 0s;
      }
      .slot-list-item:hover {
        opacity: 1;
        transform: translate(0px, -10px);
      }

      .slick-track {
        display: flex;
      }
      .slot-list .slick-arrow {
        position: absolute;
        top: 50%;
        margin-top: -50px;
        z-index: 10;
        font-size: 0;
        width: 30px;
        height: 30px;
        opacity: 0.8;
        transition: opacity 0.1s ease-in-out 0s;
        border: none;
      }
      .slot-list .slick-arrow:hover {
        opacity: 1;
      }
      .slot-list .slick-arrow.slick-prev {
        left: 0;
        background: url("https://img.icons8.com/material-sharp/24/f5f5f5/chevron-left.png")
          no-repeat;
      }
      .slot-list .slick-arrow.slick-next {
        right: 0;
        background: url("https://img.icons8.com/material-sharp/24/f5f5f5/chevron-right.png")
          no-repeat;
      }
      .slot-list .slick-arrow.slick-disabled {
        opacity: 0.2;
      }
      .slot-list .slick-list {
        margin: 0 50px;
        z-index: 1;
        width: 100%;
        background-color: #010101;
        border-radius: 10px;
      }
      .slot-list img {
        width: 100%;
        margin: 0 auto;
        border-radius: 5px;
      }
      .slot-list p {
        transform: translate(0px, -10px);
        padding: 5px;
        color: #cccccc;
      }

      /*блок с данными для каждого слота*/
      .report-block {
        width: 100%;
        display: flex;
        align-items: center;
        justify-content: center;
        flex-direction: column;
        margin-top: 200px;
        position: relative;
        background-color: #181818;
      }
      .slot-title{
        margin-top: -180px;
        color: #f5f5f5;
        z-index: 1;
        font-size: 40px;
        font-family: 'Bebas Neue';
      }
      .top-slant {
        /*стили для повернутого фона */
        content: "";
        display: block;
        position: absolute;
        background: #181818;
        top: -15vh;
        transform-origin: right top;
        transform: skewY(3deg);
        height: 30vh;
        width: 100%;
        opacity: 0.8;
      }
      .btm-slant {
        /*стили для повернутого фона */
        content: "";
        display: block;
        position: absolute;
        background: #181818;
        top: 0;
        transform-origin: left top;
        transform: skewY(-3deg);
        height: 20vh;
        width: 100%;
      }
      .report-block .item {
        padding: 15px 20px;
        background: #181818;
        border: 2px solid #333333;
        border-radius: 10px;
        margin-bottom: 20px;
        min-height: 70px;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
      }
      .report-block .flex-row .item {
        width: 33%;
      }
      .report-block .flex-row.first-row .item,
      .report-block .flex-row.second-row .item {
        width: auto;
        transition: all 0.3s ease 0s;
        transform: translateY(190%);
        opacity: 0;
        border: none;
        background: transparent;
      }
      .report-block .item .title-info {
        margin: 10px 0 15px 0;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        text-align: center;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        width: 100%;
      }
      .report-block .item .title-info span {
        font-weight: 500;
        font-size: 16px;
        line-height: 22px;
        text-align: center;
        color: #ffffff;
      }
      .report-block .item.item-row {
        -webkit-box-orient: horizontal;
        -webkit-box-direction: normal;
        flex-direction: row;
        -webkit-box-align: stretch;
        align-items: stretch;
      }
      .report-block .item .item-text-row {
        width: 100%;
        display: flex;
        justify-content: space-between;
        align-items: center;
      }
      .report-block .item .item-text-row .title,
      .item-text-column .title {
        display: flex;
        align-items: center;
        text-align: center;
        font-size: 20px;
        flex-direction: column;
      }
      .report-block .item .item-text-row .title span {
        font-weight: 500;
        font-size: 16px;
        line-height: 90%;
        color: #ffffff rgba(255, 184, 0, 0.2);
      }

      .report-block .item .item-text-row p,
      .item-text-column p {
        font-weight: 600;
        font-size: 16px;
        line-height: 22px;
        text-align: center;
        color: #ffb800;
        padding-left: 5px;
      }
      .flex-row .item-text-column span {
        line-height: 22px;
        font-size: 16px;
      }
      .item-text-column p:nth-child(2),
      .item-text-column p:nth-child(3) {
        font-weight: 400;
        margin: 10px 0;
        font-size: 14px;
      }
      .report-block .item .item-text-row p span {
        display: block;
      }
      .item-text-column {
        width: 100%;
        min-width: 170px;
        display: flex;
        flex-direction: column;
        justify-content: space-between;
        align-items: center;
        flex-direction: column;
      }
      .report-block .item .row-winrate {
        width: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
      }
      .report-block .item .row-winrate .title {
        margin-bottom: 33px;
        font-weight: 600;
        font-size: 16px;
        line-height: 22px;
        text-align: center;
        color: #ffffff;
      }
      .report-block .item .row-winrate .title span {
        color: #ffb800;
      }
      .report-block .add-margin {
        margin-top: 3vh;
      }
      .first-row {
        /*первый ряд блоков*/
        align-items: center;
        margin-top: 0px;
        width: 70%;
        z-index: 1;
        justify-content: space-evenly;
        transition: all 0.8s ease 0s;
        transform: scale(0.8);
      }
      .flex-row.first-row.active {
        transform: scale(1);
      }
      .report-block .first-row .item-text-column{
        height: 220px;
      }
      .report-block .first-row .item-text-column img {
        margin-bottom: 20px;
      }
      .report-block .first-row .item-text-column p {
        font-size: 40px;
        margin-top: 30px;
        padding: 0;
        font-family: 'Bebas Neue';
      }
      .report-block .first-row .item-text-column span {
        font-size: 18px;
      }
      .report-block .flex-row.first-row .item:nth-child(1) {
        animation: ani-left 1s forwards 0.3s;
      }
      .report-block .flex-row.first-row .item:nth-child(2) {
        animation: ani-left 1s forwards 0.6s;
      }
      .report-block .flex-row.first-row .item:nth-child(3) {
        animation: ani-left 1s forwards 0.9s;
      }
      .report-block .flex-row.second-row .item:nth-child(1) {
        animation: ani-left 1s forwards 0.3s;
      }
      .report-block .flex-row.second-row .item:nth-child(2) {
        animation: ani-left 1s forwards 0.6s;
      }
      .report-block .flex-row.second-row .item:nth-child(3) {
        animation: ani-left 1s forwards 0.9s;
      }
      @keyframes ani-left {
        0% {
          transform: translateY(190%);
          opacity: 0;
        }
        100% {
          transform: translateY(0);
          opacity: 1;
        }
      }
      .flex-row.second-row {
        /*второй ряд блоков*/
        padding: 0 5%;
        transform: scale(0.8);
        transition: all 0.8s ease 0s;
      }
      .second-row.active {
        transform: scale(1);
      }
      .report-block .flex-row.second-row .item {
        background-color: #262626;
        overflow: hidden;
        flex: 1;
        margin: 3vh 1%;
        height: 300px;
      }
      .report-block .flex-row.second-row .item:hover {
        background-color: #333333;
      }
      .report-block .second-row .item-text-column {
        height: 100%;
      }
      .report-block .second-row .item .item-info-top {
        display: flex;
        flex-direction: column;
        height: 100%;
      }
      .second-row .item .item-info-top p {
        font-size: 24px;
        transition: all 0.5s ease 0s;
      }
      .second-row .item:hover .item-info-top p {
        transform: scale(1.2);
      }
      .second-row .item .item-info-top .title {
        border-top: solid 2px #ffb800;
        color: #ffb800;
      }
      .second-row .item .item-info-bottom p {
        color: #f5f5f5;
        line-height: 15px;
      }
      .flex-row.grow-row {
        /*ряд блоков с Топ джекпотов*/
        width: 80%;
      }
      .table-row {
        /*ряд с таблицей джекпотов*/
        width: 70%;
        padding-bottom: 20px;
      }
      .grow-row,
      .table-row {
        transition: all 0.8s ease 0s;
        transform: scale(0.8);
      }
      .grow-row.active,
      .table-row.active {
        transform: scale(1);
      }
      .table-row .item-text-column .title {
        padding: 20px 0;
        color: #f5f5f5;
        width: 100%;
        font-family: 'Bebas Neue';
      }
      .table-row .item-text-column .title p {
        padding: 0;
        color: #f5f5f5;
        font-size: 30px;
      }
      .table-row .item-text-column .title p.reportMessage {
        font-size: 15pt;
        color: red;
      }
      .table-row .item-text-column .title p span {
        color: #ffb800;
      }
      .wrapper {
        /*стили для фона-градиент*/
        background-color: #010101;
        /*padding: 20px;*/
        width: 100%;
        display: flex;
        justify-content: center;
      }
      .wrapper.top {
        background-image: linear-gradient(#181818, #010101);
      }
      .wrapper.bottom {
        background-image: linear-gradient(#010101, #181818);
      }
      .blocks {
        width: 100%;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        flex-direction: column;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        -webkit-box-align: stretch;
        -ms-flex-align: stretch;
        align-items: stretch;
      }
      .blocks .block,
      .blocks .block-top {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        flex-direction: row;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        border-top: 1px solid #333333;
        /*padding: 0 10px;*/
        transition: transform 0.3s ease 0s;
        overflow: hidden;
      }
      .blocks .block {
        transform: translateY(120%);
        opacity: 0;
      }
      .blocks .block-top {
        border: none;
      }
      .blocks .block.hide {
        display: none;
      }
      .blocks .block.hover {
        background-color: #303030;
      }
      .blocks.active .block {
        transform: translateY(0px);
        opacity: 1;
      }
      .blocks.active .block:nth-child(1) {
        transition: all 0.5s ease 0s;
      }
      .blocks.active .block:nth-child(2) {
        transition: all 0.5s ease 0.3s;
      }
      .blocks.active .block:nth-child(3) {
        transition: all 0.5s ease 0.6s;
      }
      .blocks.active .block:nth-child(4) {
        transition: all 0.5s ease 0.9s;
      }
      .blocks.active .block:nth-child(5) {
        transition: all 0.5s ease 1.2s;
      }
      .blocks.active .block:nth-child(6) {
        transition: all 0.5s ease 1.5s;
      }
      .blocks.active .block:nth-child(7) {
        transition: all 0.5s ease 1.8s;
      }
      .blocks.active .block:nth-child(8) {
        transition: all 0.5s ease 2.1s;
      }
      .blocks.active .block:nth-child(9) {
        transition: all 0.5s ease 2.4s;
      }
      .blocks.active .block:nth-child(10) {
        transition: all 0.5s ease 2.7s;
      }
      .blocks.active .block:nth-child(11) {
        transition: all 0.5s ease 3s;
      }
      .blocks.active .block:nth-child(12) {
        transition: all 0.5s ease 3.3s;
      }
      .blocks .block:first-child {
        border: none;
      }
      .blocks .block span {
        display: block;
        font-weight: 500;
        font-size: 16px;
        line-height: 20px;
        text-align: left;
        color: #ffffff;
        /*flex: 1;
            margin-bottom: 10px;*/
        width: 60%;
      }
      .blocks .block span.block-title {
        font-weight: 400;
        font-size: 14px;
        width: 60%;
        margin: 5px 0;
      }
      .blocks .block span.block-title:nth-child(2) {
        width: 30%;
        text-align: center;
      }
      .blocks .block span.block-title:nth-child(3) {
        width: 10%;
      }
      .table-row .blocks .block,
      .table-row .blocks .block-top {
        min-width: 780px;
      }
      .table-row .blocks .block span,
      .table-row .blocks .block-top span {
        font-weight: 400;
        text-align: center;
        margin: 5px 0;
      }
      .table-row .blocks .block span:nth-child(1),
      .table-row .blocks .block-top span:nth-child(1) {
        width: 15%;
        color: #ffb800;
      }
      .table-row .blocks .block span:nth-child(2),
      .table-row .blocks .block-top span:nth-child(2) {
        width: 25%;
        text-align: left;
      }
      .table-row .blocks .block span:nth-child(3),
      .table-row .blocks .block-top span:nth-child(3) {
        width: 25%;
        text-align: left;
      }
      .table-row .blocks .block span:nth-child(4),
      .table-row .blocks .block-top span:nth-child(4) {
        color: #ffb800;
        width: 10%;
      }
      .table-row .blocks .block span:nth-child(5),
      .table-row .blocks .block-top span:nth-child(5) {
        width: 15%;
      }
      .table-row .blocks .block span:nth-child(6),
      .table-row .blocks .block-top span:nth-child(6) {
        width: 10%;
      }
      .report-block .table-row .blocks .block-top span {
        color: #f5f5f5;
      }
      .blocks .block p {
        font-weight: 500;
        font-size: 16px;
        line-height: 20px;
        text-align: center;
        color: #ffb800;
        width: 30%;
      }
      .blocks .block p.sim,
      a.sim {
        font-size: 14px;
        font-weight: 400;
        margin: 5px 0px;
        width: 10%;
        border-radius: 5px;
        text-decoration: none;
        display: block;
        color: #ffb800;
      }
      .grow-row .blocks .block a.sim {
        text-align: right;
        padding-left: 5px;
      }
      .table-row .blocks .block a.sim {
        width: 70%;
        background-color: #333333;
      }
      .table-row .blocks::-webkit-scrollbar {
        width: 12px;
        height: 12px;
      }
      .table-row .blocks::-webkit-scrollbar-thumb {
        background-color: rgb(255, 184, 0);
        border-radius: 10px;
        border: 4px solid #181818;
      }
      .btn {
        display: flex;
        -webkit-box-align: center;
        -webkit-box-pack: center;
        justify-content: center;
        padding: 10px 30px;
        align-items: center;
        font-weight: 600;
        font-size: 14px;
        line-height: 17px;
        text-transform: uppercase;
        color: #ffb800;
        background: #010101;
        transition: all 0.1s ease 0s;
        cursor: pointer;
        margin: 10px 0;
        width: 100%;
        border: none;
      }
      .btn:disabled {
        opacity: 0.3;
      }
      .btn:hover {
        background-color: #303030;
      }

      .brands-filter,
      .btn-to-list {
        width: 97%;
        height: 30px;
        border: none;
        background: #333333;
        outline: none;
        padding: 5px;
        color: #f5f5f5;
        font-family: "Montserrat", sans-serif;
        font-size: 16px;
        display: flex;
        justify-content: space-between;
        min-width: 200px;
      }

      .brands-filter option:hover {
        color: #ffb800;
      }
      .list-row {
        display: flex;
        flex-flow: row wrap;
        width: 100%;
        padding: 0;
        list-style-type: none;
      }
      .list-row li {
        min-width: 200px;
        /*width: 19%;*/
        min-width: 300px;
        margin: 5px;
        box-sizing: border-box;
        font-size: 16px;
        overflow: hidden;
      }
      .list-row a {
        text-decoration: none;
        color: #f5f5f5;
      }
      .list-row li:hover a {
        color: #ffb800;
      }
      .filter-wrap {
        width: 40%;
        margin-top: 20px;
        background-color: #333333;
        padding-left: 1%;
        min-width: 200px;
      }
      a.btn-to-list {
        width: 40%;
        text-decoration: none;
        color: #f5f5f5;
        font-size: 16px;
        font-weight: 400;
        padding: 5px;
        margin-top: 20px;
        min-width: 200px;
      }
      .arrow {
        display: inline-block;
        -webkit-animation: bounce 1.75s infinite;
        animation: bounce 1.75s infinite;
        transition: border 150ms ease-in-out, transform 300ms ease-in;
      }
      .material-symbols-outlined {
        font-size: 18px;
      }
      @-webkit-keyframes bounce {
        0% {
          transform: translateY(0);
        }
        10% {
          transform: translateY(-50%);
        }
        20% {
          transform: translateY(0);
        }
        30% {
          transform: translateY(-25%);
        }
        40% {
          transform: translateY(0);
        }
        50% {
          transform: translateY(-10%);
        }
        60% {
          transform: translateY(0);
        }
        70% {
          transform: translateY(0);
        }
        80% {
          transform: translateY(0);
        }
        100% {
          transform: translateY(0);
        }
      }

      @keyframes bounce {
        0% {
          transform: translateY(0);
        }
        10% {
          transform: translateY(-50%);
        }
        20% {
          transform: translateY(0);
        }
        30% {
          transform: translateY(-25%);
        }
        40% {
          transform: translateY(0);
        }
        50% {
          transform: translateY(-10%);
        }
        60% {
          transform: translateY(0);
        }
        70% {
          transform: translateY(0);
        }
        80% {
          transform: translateY(0);
        }
        100% {
          transform: translateY(0);
        }
      }
      [hidden] { display: none !important}

    

      @media (max-width: 1400px) {
        .flex-row.grow-row {
          width: 90%;
        }
        .report-block .grow-row .item {
          padding: 15px 10px;
        }
        /*.slot-list-item{
          max-width: 223px;
          min-height: 220px;
        }*/
      }
      @media (max-width: 1060px) {
        .flex-row.grow-row,
        .table-row {
          width: 95%;
        }
      }
      @media (max-width: 960px) {
        .flex-row.grow-row {
          width: 95%;
          flex-direction: column;
          align-items: center;
        }
        .report-block .grow-row .item {
          width: 100%;
          max-width: 400px;
        }
        .first-row,
        .second-row {
          width: 95%;
        }
        .table-row .blocks {
          overflow-x: scroll;
        }
        .table-row .flex-row {
          width: 90%;
        }
      }
      @media (max-width: 650px) {
        .first-row,
        .second-row {
          flex-direction: column;
        }
        .table-row .flex-row {
          flex-direction: column;
          align-items: center;
        }
      }

      @media (max-width: 500px) {
        header {
          grid-column: 2/5;
        }
        nav {
          flex-direction: row;
          height: 50px;
          margin-top: 5px;
          justify-content: start;
        }
        nav.sticky {
          top: -5px;
        }
        nav .btn-menu {
          display: block;
          color: #ffb800;
          position: absolute;
          top: 10;
          left: 10;
          z-index: 1;
        }
        ul.topnav {
          display: none;
        }
        ul.topnav li {
          width: 100%;
          height: 40px;
          margin-top: 0px;
        }

        .topnav li a {
          width: 100%;
          padding: 0.5em;
        }
        .topnav li:first-child .roll-link .front,
        .topnav li:first-child .roll-link .back {
          border-radius: 5px 5px 0px 0px;
        }
        .topnav li:last-child .roll-link .front,
        .topnav li:last-child .roll-link .back {
          border-radius: 0px 0px 5px 5px;
        }
        .roll-link {
          width: 100%;
        }
        .topnav.responsive {
          display: block;
        }
        main{
          margin-top: -100px;
        }
        main.responsive{
          margin-top: 100px;
        }
        .empty {
          display: none;
        }
        .first-row {
          margin-top: 0;
        }
      }
      @media (max-width: 1040px) {
        .report-block,
        .report-block .row-item {
          flex-direction: column;
        }
      }
    </style>
  </head>
  <body>
    <!-- Грид-контейенер общий 
        <div class="grid">-->
    <!--Header-->
    <header>
      <div class="empty"></div>
      <!--блок навгации-->
      <nav class="navigation">
        <!-- Кнопка раскрытия меню -->
        <a href="javascript:void(0)" class="btn-menu">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="24"
            height="24"
            viewBox="0 0 24 24"
            style="fill: white"
          >
            <path
              d="M3.5,7 C3.22385763,7 3,6.77614237 3,6.5 C3,6.22385763 3.22385763,6 3.5,6 L20.5,6 C20.7761424,6 21,6.22385763 21,6.5 C21,6.77614237 20.7761424,7 20.5,7 L3.5,7 Z M3.5,12 C3.22385763,12 3,11.7761424 3,11.5 C3,11.2238576 3.22385763,11 3.5,11 L20.5,11 C20.7761424,11 21,11.2238576 21,11.5 C21,11.7761424 20.7761424,12 20.5,12 L3.5,12 Z M3.5,17 C3.22385763,17 3,16.7761424 3,16.5 C3,16.2238576 3.22385763,16 3.5,16 L20.5,16 C20.7761424,16 21,16.2238576 21,16.5 C21,16.7761424 20.7761424,17 20.5,17 L3.5,17 Z"
            />
          </svg>
        </a>
        <!-- список меню -->
        <ul class="topnav" id="myTopnav">
          <li>
            <div class="roll-link">
              <a href="/SlotApplication/" class="front">Home</a>
              <a href="/SlotApplication/" class="back">Home</a>
            </div>
          </li>
          <li>
            <div class="roll-link">
              <a href="/SlotApplication/tools" class="front">Tools</a>
              <a href="/SlotApplication/tools" class="back">Tools</a>
            </div>
          </li>
          <li>
            <div class="roll-link">
              <a href="/SlotApplication/slots" class="front">Slots</a>
              <a href="/SlotApplication/slots" class="back">Slots</a>
            </div>
          </li>
          <li>
            <div class="roll-link">
              <a href="/SlotApplication/jackpots" class="front">Jackpots</a>
              <a href="/SlotApplication/jackpots" class="back">Jackpots</a>
            </div>
          </li>
          <li>
            <div class="roll-link">
              <a href="/SlotApplication/sites" class="front">Sites</a>
              <a href="/SlotApplication/sites" class="back">Sites</a>
            </div>
          </li>
        </ul>
        <!--/ список меню -->
      </nav>
      <!--/ блок навгации-->
    </header>
    <!--/Header-->

    <!--прелоадер на странице-->
    <div class="preloader">
      <div class="status"><span class="loader-16"> </span></div>
      <div class="load-6">
        <div class="letter-holder">Loading</div>
      </div>
    </div>
    <!--/ прелоадер на странице-->

    <!-- Back to top button -->
    <a id="button"></a>

    <!--main-->
    <main>
      <!--блок c меню для выбора слота-->
      <div class="slot-list-wrapper">
        <div class="title">
          <p>Jackpot Slots</p>
        </div>
        <div id="slot-list" class="add-margin slot-list anim-items"></div>
      </div>
      <!--/блок c меню для выбора слота-->
      
      <!--блок с данными для слота-->
      <div class="report-block">

        <div class="slot-title">
          <p class="slot-name"></p>
        </div>

        <!--блок с повернутым фоном-->
        <div class="top-slant"></div>
        
        <!--первый ряд данных-->
        <div class="flex-row first-row anim-items">
          <div class="item">
            <div class="item-text-column">
              <img
                src="https://img.icons8.com/external-kiranshastry-lineal-kiranshastry/64/ffb800/external-jackpot-casino-kiranshastry-lineal-kiranshastry.png"
              />
              <div class="title">
                <span
                  >Уникальных<br />
                  джекпотов</span
                >
              </div>
              <p id="uniqueJackpots" class="numeric">-</p>
            </div>
          </div>

          <div class="item">
            <div class="item-text-column">
              <img
                src="https://img.icons8.com/external-kiranshastry-lineal-kiranshastry/64/ffb800/external-jackpot-casino-kiranshastry-lineal-kiranshastry-1.png"
              />
              <div class="title">
                <span
                  >Число разыгранных<br />
                  джекпотов</span
                >
              </div>
              <p id="payedOutJackpotCount" class="numeric">-</p>
            </div>
          </div>

          <div class="item">
            <div class="item-text-column">
              <img src="https://img.icons8.com/small/64/ffb800/counter.png" />
              <div class="title">
                <span>100 RTP счетчик</span>
              </div>
              <p id="rtpCounter" class="numeric">-</p>
            </div>
          </div>
        </div>
        <!--/первый ряд данных-->

        <!--блок с повернутым фоном-->
        <div class="btm-slant"></div>
        
        <!--второй ряд данных-->
        <div class="flex-row add-margin second-row anim-items">
          <div class="item">
            <div class="item-text-column">
              <div class="item-info-top">
                <p id="maxJackpotValue" class="numeric">-</p>
                <div class="title">
                  <span>Максимальное значение джекпота (сейчас)</span>
                </div>
              </div>
              <div class="item-info-bottom">
                <p id="maxJackpotSite">-</p>
                <p id="maxJackpotBrand">-</p>
              </div>
            </div>
          </div>

          <div class="item">
            <div class="item-text-column">
              <div class="item-info-top">
                <p id="lastPayedOutValue" class="numeric">-</p>
                <div class="title">
                  <span>Последний разыгранный джекпот</span>
                </div>
              </div>
              <div class="item-info-bottom">
                <p id="lastPayedOutSite">-</p>
                <p id="lastPayedOutBrand">-</p>
                <p id="lastPayedOutDate">-</p>
              </div>
            </div>
          </div>

          <div class="item">
            <div class="item-text-column">
              <div class="item-info-top">
                <p id="maxPayedOutJackpotValue" class="numeric">-</p>
                <div class="title">
                  <span>Максимальный разыгранный джекпот</span>
                </div>
              </div>
              <div class="item-info-bottom">
                <p id="maxPayedOutJackpotSite">-</p>
                <p id="maxPayedOutJackpotBrand">-</p>
              </div>
            </div>
          </div>
        </div>
        <!--/второй ряд данных-->

        <!--третий ряд данных с Топ джекпотов-->
        <div class="wrapper top">
          <div class="flex-row add-margin grow-row anim-items">
            <div class="item">
              <div class="row-winrate">
                <div class="title">TOP 5 GROWN <span>24 H</span></div>
                <div
                  class="blocks blocks-top anim-items anim-no-hide"
                  id="growsDay"
                >
                </div>
              </div>
            </div>

            <div class="item">
              <div class="row-winrate">
                <div class="title">TOP 5 GROWN <span>MONTH</span></div>
                <div
                  class="blocks blocks-top anim-items anim-no-hide"
                  id="growsMonth"
                >
                </div>
              </div>
            </div>

            <div class="item">
              <div class="row-winrate">
                <div class="title">TOP 5 GROWN <span>YEAR</span></div>
                <div
                  class="blocks blocks-top anim-items anim-no-hide"
                  id="growsYear"
                >
                </div>
              </div>
            </div>
          </div>
        </div>
        <!--/третий ряд данных с Топ джекпотов-->

        <!--четвертый ряд данных с списком джекпотов-->
        <div class="wrapper bottom">
          <div class="table-row anim-items">
            <div class="item-text-column">
              <div class="title">
                <p>JACKPOT <span>LIST</span></p>
                <div class="flex-row">
                  <div class="filter-wrap">
                    <select class="brands-filter">
                    </select>
                  </div>
                  <a class="btn-to-list" href="#inactiveJL"
                    ><span>Inactive Jackpot List</span>
                    <span class="arrow material-symbols-outlined"
                      >expand_more</span
                    ></a
                  >
                </div>
                <p class="reportMessage" id="reportMessage"></p>
              </div>
              <div
                class="blocks blocks-top anim-items anim-no-hide"
                id="JLTableDiv"
              >
                
              </div>
              <button id="JLshowMore" class="show-more btn">SHOW MORE</button>
            </div>
          </div>
        </div>
        <!--/четвертый ряд данных с списком джекпотов-->

        <!--пятый ряд данных с списком неактивных джекпотов-->
        <div class="table-row anim-items">
          <div class="item-text-column">
            <div class="title">
              <p><span>INACTIVE</span> JACKPOT LIST</p>
            </div>
            <ul class="anim-items anim-no-hide list-row" id="inactiveJL"></ul>
          </div>
        </div>
        <!--/пятый ряд данных с списком неактивных джекпотов-->
      </div>
      <!--/блок с данными для слота-->

    </main>
    <!-- /main -->

    <!-- footer -->
    <footer></footer>
    <!-- /footer -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script
      type="text/javascript"
      src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"
    ></script>
    <script>
      $(".report-block").hide();

      //Собираем элементы списка слотов
      const slotDiv = document.querySelector("#slot-list");
      const slotsList = [
        {
          slot: "megaJoker",
          name: "Mega Joker",
          img: "https://affgambler.ru/wp-content/uploads/2016/11/MegaJoker.jpg",
        },
        {
          slot: "divineFortune",
          name: "Divine Fortune",
          img: "https://affgambler.ru/wp-content/uploads/2017/11/godsoffortune.jpg",
        },
        {
          slot: "mercyOfTheGods",
          name: "Mercy Of The Gods",
          img: "https://affgambler.ru/wp-content/uploads/2019/07/Mercy-of-the-Gods-foto.jpg",
        },
        {
          slot: "vegasNightLive",
          name: "Vegas Night Live",
          img: "https://affgambler.ru/wp-content/uploads/2020/10/Vegas-Night-Life-foto.jpg",
        },
        {
          slot: "IRMega",
          name: "Imperial Riches Mega",
          img: "https://affgambler.ru/wp-content/uploads/2019/09/Imperial-Riches-foto.jpg",
        },
        {
          slot: "IRMidi",
          name: "Imperial Riches Midi",
          img: "https://affgambler.ru/wp-content/uploads/2019/09/Imperial-Riches-foto.jpg",
        },
        {
          slot: "IRMajor",
          name: "Imperial Riches Major",
          img: "https://affgambler.ru/wp-content/uploads/2019/09/Imperial-Riches-foto.jpg",
        },
        {
          slot: "grandSpinSuperpot",
          name: "Grand Spin Superpot",
          img: "https://affgambler.ru/wp-content/uploads/2019/07/Grand-Spinn-foto.jpg",
        },
        {
          slot: "MRHolmes1",
          name: "Mr Holmes 1",
          img: "https://affgambler.ru/wp-content/uploads/2016/12/Holmesand.jpg",
        },
        {
          slot: "MRHolmes2",
          name: "Mr Holmes 2",
          img: "https://affgambler.ru/wp-content/uploads/2016/12/Holmesand.jpg",
        },
        {
          slot: "MRHolmes3",
          name: "Mr Holmes 3",
          img: "https://affgambler.ru/wp-content/uploads/2016/12/Holmesand.jpg",
        },
        {
          slot: "MRHolmes4",
          name: "Mr Holmes 4",
          img: "https://affgambler.ru/wp-content/uploads/2016/12/Holmesand.jpg",
        },
        {
          slot: "MRHolmes5",
          name: "Mr Holmes 5",
          img: "https://affgambler.ru/wp-content/uploads/2016/12/Holmesand.jpg",
        },
        {
          slot: "OzwinsJ1",
          name: "Ozwins Jackpots 1",
          img: "https://affgambler.ru/wp-content/uploads/2018/01/ozwinsjackpots.jpg",
        },
        {
          slot: "OzwinsJ2",
          name: "Ozwins Jackpots 2",
          img: "https://affgambler.ru/wp-content/uploads/2018/01/ozwinsjackpots.jpg",
        },
        {
          slot: "OzwinsJ3",
          name: "Ozwins Jackpots 3",
          img: "https://affgambler.ru/wp-content/uploads/2018/01/ozwinsjackpots.jpg",
        },
        {
          slot: "OzwinsJ4",
          name: "Ozwins Jackpots 4",
          img: "https://affgambler.ru/wp-content/uploads/2018/01/ozwinsjackpots.jpg",
        },
        {
          slot: "OzwinsJ5",
          name: "Ozwins Jackpots 5",
          img: "https://affgambler.ru/wp-content/uploads/2018/01/ozwinsjackpots.jpg",
        },
        {
          slot: "JRaiders1",
          name: "Jackpot Raiders 1",
          img: "https://affgambler.ru/wp-content/uploads/2019/05/Jackpot-Raiders-foto.jpg",
        },  
        {
          slot: "JRaiders2",
          name: "Jackpot Raiders 2",
          img: "https://affgambler.ru/wp-content/uploads/2019/05/Jackpot-Raiders-foto.jpg",
        },  
        {
          slot: "JRaiders3",
          name: "Jackpot Raiders 3",
          img: "https://affgambler.ru/wp-content/uploads/2019/05/Jackpot-Raiders-foto.jpg",
        },  
        {
          slot: "JRaiders4",
          name: "Jackpot Raiders 4",
          img: "https://affgambler.ru/wp-content/uploads/2019/05/Jackpot-Raiders-foto.jpg",
        },  
        {
          slot: "JRaiders5",
          name: "Jackpot Raiders 5",
          img: "https://affgambler.ru/wp-content/uploads/2019/05/Jackpot-Raiders-foto.jpg",
        },  
        /*{
          slot: 
          [
            "MRHolmes1",
            "MRHolmes2",
            "MRHolmes3",
            "MRHolmes4",
            "MRHolmes5",
          ],
          name: "Mr Holmes",
          img: "https://affgambler.ru/wp-content/uploads/2016/12/Holmesand.jpg",
        },   
        {
          slot: 
          [
            "OzwinsJ1",
            "OzwinsJ2",
            "OzwinsJ3",
            "OzwinsJ4",
            "OzwinsJ5",
          ],
          name: "Ozwins Jackpots",
          img: "https://affgambler.ru/wp-content/uploads/2018/01/ozwinsjackpots.jpg",
        },  
        {
          slot: 
          [
            "JRaiders1",
            "JRaiders2",
            "JRaiders3",
            "JRaiders4",
            "JRaiders5",
          ],
          name: "Jackpot Raiders 1",
          img: "https://affgambler.ru/wp-content/uploads/2019/05/Jackpot-Raiders-foto.jpg",
        },*/     
      ];
      //Отрисовываем элементы списка слотов
      slotsList.forEach((item) => {
        let itemSlot = document.createElement("div");
        itemSlot.classList.add("slot-list-item");
        slotDiv.append(itemSlot);
        itemSlot.innerHTML =
          '<img data-slot="' + item.slot + '"src="' + item.img + '"/>';
        itemSlot.innerHTML += "<p>" + item.name + "</p>";
      });

      //Слайдер для списка слотов
      $("#slot-list").slick({
        dots: false,
        infinite: false,
        speed: 500,
        slidesToShow: 8,
        slidesToScroll: 1,
        easing: "ease",
        waitForAnimate: false,
        responsive: [
          {
            breakpoint: 1400,
            settings: {
              slidesToShow: 7,
              slidesToScroll: 1,
            },
          },
          {
            breakpoint: 1060,
            settings: {
              slidesToShow: 6,
              slidesToScroll: 1,
            },
          },
          {
            breakpoint: 800,
            settings: {
              slidesToShow: 4,
              slidesToScroll: 1,
            },
          },
          {
            breakpoint: 700,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 1,
            },
          },
          {
            breakpoint: 550,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 1,
            },
          },
          {
            breakpoint: 400,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
            },
          },
        ],
      });

      localStorage.clear();
      $(".preloader").show();
      requestData("megaJoker", "Mega Joker");//загружаем данные первого слота 
      

      //Вешаем слушатель на слайдер, для загрузки данных по нажатию на слот
      slotDiv.addEventListener("click", getDataSlot);
      function getDataSlot(e) {
        let target = e.target;
        while (target != this) {
          if (target.tagName == "IMG") {
            $(".report-block").hide();
            $(".preloader").show(); //показываем прелоадер
            let slot = target.getAttribute("data-slot");
            slot = slot.split(',');
            let slotName = target.nextSibling.textContent;
            console.log(slotName);
            slot.forEach(item => {
            console.log(item);
            requestData(item, slotName);
            })
            //console.log(slotName);
            //console.log(slots);
            //requestData(slot, slotName);
            return;
          }
          target = target.parentNode;
        }
      }

      function requestData(slot, slotName) {
        let storage = localStorage.getItem(slot) ? JSON.parse(localStorage.getItem(slot)) : null;
        if(storage){  
          console.log(storage);
          drawDataSlot(storage, slot, slotName);
        } else {
          const link = "/SlotApplication/get-jackpot-report";
          const requestData = new FormData();
          requestData.append("request", slot);

          fetch(link, {
            method: "POST",
            mode: "cors",
            body: requestData,
          })
            .then((response) => response.json())
            .then((data) => {
              console.log(data);
              localStorage.setItem(slot,JSON.stringify(data));
              drawDataSlot(data, slot, slotName);
            })
            .catch((error) => console.error("Ошибка получения данных" + error));
        }
      }

      function drawDataSlot(data, slot, slotName) {
        $(".preloader").hide(); //прячем прелоадер
        

        window.rtpTableData = data.topJackpotsRTP;
        window.rtpTablePage = 1;

        //Элементы для заполнения на странице
        let uniqueJackpots = document.getElementById("uniqueJackpots"),
              payedOutJackpotCount = document.getElementById("payedOutJackpotCount"),
              rtpCounter = document.getElementById("rtpCounter"),
              maxJackpotValue = document.getElementById("maxJackpotValue"),
              maxJackpotSite = document.getElementById("maxJackpotSite"),
              maxJackpotBrand = document.getElementById("maxJackpotBrand"),
              maxPayedOutJackpotValue = document.getElementById("maxPayedOutJackpotValue"),
              maxPayedOutJackpotSite = document.getElementById("maxPayedOutJackpotSite"),
              maxPayedOutJackpotBrand = document.getElementById("maxPayedOutJackpotBrand"),
              lastPayedOutValue = document.getElementById("lastPayedOutValue"),
              lastPayedOutSite = document.getElementById("lastPayedOutSite"),
              lastPayedOutBrand = document.getElementById("lastPayedOutBrand"),
              lastPayedOutDate = document.getElementById("lastPayedOutDate"),
              reportMessage = document.getElementById("reportMessage"),
              _slotName = document.querySelector('.slot-name');

        
          //Обнуляем данные в элементах
          uniqueJackpots.textContent = '';
          payedOutJackpotCount.textContent = '';
          rtpCounter.textContent = '';
          maxJackpotValue.textContent = '';
          maxJackpotSite.textContent = '';
          maxJackpotBrand.textContent = '';
          maxPayedOutJackpotValue.textContent = '';
          maxPayedOutJackpotSite.textContent = '';
          maxPayedOutJackpotBrand.textContent = '';
          lastPayedOutValue.textContent = '';
          lastPayedOutSite.textContent = '';
          lastPayedOutBrand.textContent = '';
          lastPayedOutDate.textContent = '';
          reportMessage.textContent = '';
          _slotName.textContent = '';
      
        

        //Заполняем элементы данными
        uniqueJackpots.textContent = !isNaN(data.uniqueJackpots) ? data.uniqueJackpots : "-";
        payedOutJackpotCount.textContent = !isNaN(data.payedOutJackpotsCount) ? data.payedOutJackpotsCount : "-";
        rtpCounter.textContent = !isNaN(data.reach100Rtp) ? data.reach100Rtp : "-";

        maxJackpotValue.textContent = data.maxValueCurrent ? (!isNaN(data.maxValueCurrent.maxValue) ? data.maxValueCurrent.maxValue.toLocaleString("ru-RU") + "€" : "-") : "-";
        maxJackpotSite.textContent = data.maxValueCurrent ? (data.maxValueCurrent.site ? data.maxValueCurrent.site : "-") : "-";
        maxJackpotBrand.textContent = data.maxValueCurrent ? (data.maxValueCurrent.brand ? data.maxValueCurrent.brand : "-") : "-";

        maxPayedOutJackpotValue.textContent = data.maxValuePayedOut ? (!isNaN(data.maxValuePayedOut.maxValue) ? data.maxValuePayedOut.maxValue.toLocaleString("ru-RU") + "€" : "-") : "-";
        maxPayedOutJackpotSite.textContent = data.maxValuePayedOut ? (data.maxValuePayedOut.site ? data.maxValuePayedOut.site : "-") : "-";
        maxPayedOutJackpotBrand.textContent = data.maxValuePayedOut ? (data.maxValuePayedOut.brand ? data.maxValuePayedOut.brand : "-") : "-";

        lastPayedOutValue.textContent = data.lastPayedOut ? (!isNaN(data.lastPayedOut.value) ? data.lastPayedOut.value.toLocaleString("ru-RU") + "€" : "-") : "-";
        lastPayedOutSite.textContent = data.lastPayedOut ? (data.lastPayedOut.site ? (data.lastPayedOut.site.shortUrl ? data.lastPayedOut.site.shortUrl : "-") : "-") : "-";
        lastPayedOutBrand.textContent = data.lastPayedOut ? (data.lastPayedOut.brand ? data.lastPayedOut.brand : "-") : "-";
        lastPayedOutDate.textContent = data.lastPayedOut ? (data.lastPayedOut.date ? data.lastPayedOut.date : "-") : "-";
        
        reportMessage.textContent = data.message ? data.message : "";
        _slotName.textContent = slotName;

        //Top Grown
        if (data != null && data.grownValues != null) {
          for (let grownDataTable of data.grownValues) {
            let periodSuffix = "";
            if (grownDataTable.grownPeriod === "day") {
              periodSuffix = "Day";
            }
            if (grownDataTable.grownPeriod === "month") {
              periodSuffix = "Month";
            }
            if (grownDataTable.grownPeriod === "year") {
              periodSuffix = "Year";
            }

            let growsTable = document.getElementById("grows" + periodSuffix);
            growsTable.innerHTML = '';
    
            let blockTop = document.createElement('div');
            blockTop.classList.add('block');
            growsTable.append(blockTop);
            blockTop.innerHTML = '<span class="block-title">Site</span><span class="block-title">Growth</span><span class="block-title"></span>';
                    

            for (let i = 0; i < grownDataTable.grownValues.length; i++) {
              let block = document.createElement("div");
              block.classList.add("block");
              growsTable.append(block);
              block.innerHTML =
                "<span>" +
                  (grownDataTable.grownValues[i].site ? grownDataTable.grownValues[i].site : '-') +
                "</span><p>" +
                  (grownDataTable.grownValues[i].grownValue ? grownDataTable.grownValues[i].grownValue.toLocaleString(
                  "ru-RU"
                ) : '-') +
                '€</p><a href="#" class="sim"><img src="https://img.icons8.com/sf-ultralight/25/ffb800/details-pane.png"/></a>';
            }
          }
        }

        //Jackpots List
        if (data && data.topJackpotsRTP) {
          let rtpTableLength = data.topJackpotsRTP.length;
          let JLData = data.topJackpotsRTP;
          let JLTable = document.getElementById("JLTableDiv");
          
          JLTable.innerHTML = '';
          let blockTop = document.createElement('div');
          blockTop.classList.add('block-top');
          JLTable.append(blockTop);
          blockTop.innerHTML = '<span class="block-title">Jack Pot</span><span class="block-title">Site</span><span class="block-title">Brand</span><span class="block-title">RTP</span><span class="block-title">Status</span><span class="block-title"></span>';

          for (let i = 0; i < rtpTableLength; i++) {
            let block = document.createElement("div");
            block.classList.add("block");
            JLTable.append(block);
            block.innerHTML =
              "<span>" +
              (JLData[i].valueFirst ? JLData[i].valueFirst.toLocaleString("ru-RU") : '-') +
              "€</span><span>" +
              (JLData[i].site.shortUrl ? JLData[i].site.shortUrl : "-") +
              "</span><span data-" +
              slot +
              ' data-license ="' +
              (JLData[i].licenses ? JLData[i].licenses : '-') +
              '">' +
              (JLData[i].brand ? JLData[i].brand : '-') +
              "</span><span>" +
              (JLData[i].rtp ? JLData[i].rtp.toFixed(1) : '-') +
              "%" +
              "</span><span>" +
              (JLData[i].status ? JLData[i].status : '-') +
              '</span><span><a class="sim" href="/SlotApplication/jackpot?slotName=' +
              slot +
              "&site=" +
              (JLData[i].site ? (JLData[i].site.fullUrl ? JLData[i].site.fullUrl : '-') : '-') +
              '">Details</a></span>';
            if (i > 10) {
              block.classList.add("hide");
            }
          }

          if (data.topJackpotsRTP.length > 10) {
            document.getElementById("JLshowMore").style.display = "block";
          }
        }

        //выпадающий список - фильтр по лицензии
        let arrLicenses = data.licenses;
        let selectLicense = document.querySelector(".brands-filter");
        selectLicense.innerHTML = '';
        let defaultOption = document.createElement('option');
        defaultOption.value = 'default';
        defaultOption.innerText = 'Select license';
        selectLicense.append(defaultOption);
        for (let i = 0; i < arrLicenses.length; i++) {
          let option = document.createElement("option");
          option.value = arrLicenses[i];
          option.innerText = arrLicenses[i];
          selectLicense.append(option);
        }

        //фильтр по лицензии
        selectLicense.addEventListener("change", function () {
          document.getElementById("JLshowMore").style.display = "none";
          let value = selectLicense.value;
          let arrElem = document.querySelectorAll("[data-" + slot + "]");
          let arrData = [];
          let notData = [];

          for (let i = 0; i < arrElem.length; i++) {
            let elemData = arrElem[i].getAttribute("data-license").split(",");
            const chekValue = (elem) => elem == value;

            if (value == "default") {
              arrElem[i].parentNode.classList.remove("hide");
            } else if (elemData.some(chekValue)) {
              arrElem[i].parentNode.classList.remove("hide");
              arrData.push(arrElem[i]);
            } else {
              arrElem[i].parentNode.classList.add("hide");
              notData.push(arrElem[i]);
            }
          }
          console.log(arrData);
        });

        //inactive Jackpot List
        let inactiveJL = document.getElementById("inactiveJL");
        inactiveJL.parentNode.hidden = false;
        inactiveJL.innerHTML = '';
        let inactiveJLdata = data.inactiveJackpots;
        

        if (inactiveJLdata.length > 0){
          for (let i = 0; i < inactiveJLdata.length; i++) {
            let site = inactiveJLdata[i].site
              .replace("https://", "")
              .replace("http://", "")
              .replace("www.", "");
            let item = document.createElement("li");
            inactiveJL.append(item);
            item.innerHTML =
              '<a href="/SlotApplication/jackpot?slotName=' +
              slot +
              "&site=" +
              inactiveJLdata[i].site +
              '">' +
              site +
              "</a>";
          }
        } else {
          inactiveJL.parentNode.hidden = true;
        }
        
        $(".preloader").hide(); //показываем прелоадер
        $(".report-block").show();
        animOnScroll(); //запуск анимации
      }

      //Вешаем слушатель для кнопки Show more списка Джекпотов
      let JLShowMore = document.querySelectorAll(".show-more");
      JLShowMore.forEach((elem) => {
        elem.addEventListener("click", showMore);
      });

      function showMore() {
        let page = window.rtpTablePage;
        let data = window.rtpTableData;
        window.rtpTablePage++;

        let dataLength;
        if (data.length < (page + 1) * 10) {
          document.getElementById("JLshowMore").style.display = "none";
          dataLength = data.length;
        } else {
          dataLength = (page + 1) * 10 + 1;
        }

        let JLTable = document.getElementById("JLTableDiv");
        let JLData = JLTable.querySelectorAll(".block");
        for (let i = page * 10 + 1; i < dataLength; i++) {
          JLData[i].classList.remove("hide");
        }
      }

      //анимация элементов
      const animItems = document.querySelectorAll(".anim-items");

      if (animItems.length > 0) {
        window.addEventListener("scroll", animOnScroll);
        function animOnScroll() {
          for (let i = 0; i < animItems.length; i++) {
            const animItem = animItems[i];
            const animItemHeight = animItem.offsetHeight;
            const animItemOffset = offset(animItem).top;
            const animStart = 8;

            let animItemPoint = window.innerHeight - animItemHeight / animStart;
            if (animItemHeight > window.innerHeight) {
              animItemPoint =
                window.innerHeight - window.innerHeight / animStart;
            }
            if (
              pageYOffset > animItemOffset - animItemPoint &&
              pageYOffset < animItemOffset + animItemHeight
            ) {
              animItem.classList.add("active");
            } else {
              if (!animItem.classList.contains("anim-no-hide")) {
                animItem.classList.remove("active");
              }
            }
          }
        }
        function offset(elem) {
          const rect = elem.getBoundingClientRect(),
            scrollLeft =
              window.pageXOffset || document.documentElement.scrollLeft,
            scrollTop =
              window.pageYOffset || document.documentElement.scrollTop;
          return { top: rect.top + scrollTop, left: rect.left + scrollLeft };
        }
      }
      animOnScroll();

      //форматирование чисел
      $(".numeric").each(function () {
        $this = parseFloat($(this).text()).toLocaleString("ru-RU");
        $(this).text($this);
      });

      $(".roundup").each(function () {
        $this = parseFloat($(this).text()).toFixed(2);
        $(this).text($this);
      });

      // When the user scrolls the page, execute myFunction
      window.onscroll = function () {
        myFunction();
      };

      // Get the navbar
      let navbar = document.querySelector(".navigation");
      //let slotMenu = document.querySelector(".slot-menu");

      // Get the offset position of the navbar
      let stickyNav = navbar.offsetTop;
      //let stickySlot = slotMenu.offsetTop;

      // Add the sticky class to the navbar when you reach its scroll position. Remove "sticky" when you leave the scroll position
      function myFunction() {
        if (window.pageYOffset >= stickyNav) {
          navbar.classList.add("sticky");
        } else {
          navbar.classList.remove("sticky");
        }
        /*if (window.pageYOffset - 100 >= stickySlot) {
          slotMenu.classList.add("sticky");
        } else {
          slotMenu.classList.remove("sticky");
        }*/
      }

      // открытие меню навигации
      const btnMenu = document.querySelector(".btn-menu");
      btnMenu.addEventListener("click", toggleMenu);

      function toggleMenu() {
        let x = document.getElementById("myTopnav");
        let k = document.querySelector('main');
        if (x.className === "topnav") {
          x.className += " responsive";
          k.classList.add("responsive");
        } else {
          x.className = "topnav";
          k.classList.remove("responsive");
        }
      }    
      
      let btnToTop = $('#button');

      $(window).scroll(function() {
        if ($(window).scrollTop() > 300) {
          btnToTop.addClass('show');
        } else {
          btnToTop.removeClass('show');
        }
      });

      btnToTop.on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({scrollTop:0}, '300');
      });
    </script>
  </body>
</html>
