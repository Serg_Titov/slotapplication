<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en" pageEncoding="UTF-8">
  <head>
    <title>Casino Brands</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
      href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800;900&display=swap"
      rel="stylesheet"
    />
  </head>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
  <style>
    html {
      height: 100%;
    }
    * {
      box-sizing: border-box;
    }
    body {
      margin: 0;
      font-family: "Montserrat", sans-serif;
      font-size: 14px;
    }

    /*общий grid*/
    .grid {
      display: grid;
      grid-template-columns: 1fr 0.5fr 2fr 0.5fr 1fr;
      grid-template-rows: auto 1fr auto;
      justify-content: center;
      align-content: flex-start;
      justify-items: center;
      box-sizing: border-box;
      min-height: 100%;
      min-width: 320px;
    }

    /*общий flex*/
    .flex {
      display: flex;
      justify-content: center;
      flex-flow: row wrap;
    }
    /*flex-контейнер*/
    .flex-row {
      display: flex;
      flex-flow: row wrap;
      max-width: 1200px;
      margin: 0 auto;
      justify-content: center;
    }
    /*flex-контейнер*/
    .flex-column {
      display: flex;
      max-width: 1200px; /*ширина контента*/
      flex-flow: column nowrap;
      margin: 0 auto;
      align-items: center;
    }

    header {
      grid-column: 3/4;
      grid-row: 1/2;
      width: 100%;
      /*height: 100vh;*/
      color: #fff;
    }

    /*style for menu navigation*/
    nav {
      width: 100%;
      box-sizing: border-box;
      display: flex;
      flex-flow: row nowrap;
      height: 80px;
      justify-content: space-around;
      transition: background-color 0.5s;
    }

    ul.topnav {
      display: flex;
      list-style-type: none;
      width: 100%;
      justify-content: center;
      align-items: center;
      margin: 0;
      padding: 0;
    }

    ul.topnav li {
      text-align: center;
      background: #f5f5f5;
      border-radius: 2px 2px;
      height: auto;
      perspective: 900px;
    }

    /*style for rotate point of menu*/
    .roll-link {
      height: auto;
      display: block;
      width: 90px;
      transform-style: preserve-3d;
    }

    .roll-link .front {
      height: 40px;
      background: #f5f5f5;
      color: #695753;
    }

    .roll-link .back {
      opacity: 0;
      height: 40px;
      display: block;
      background: #b19891;
      color: #f5f5f5;
      transform: translate3d(0, 0, -40px) rotate3d(1, 0, 0, 90deg);
    }

    .roll-link > a {
      display: block;
      position: absolute;
      width: 90px;
      text-decoration: none;
      padding: 1em;
      transition: all 0.3s ease-out;
      transform-origin: 50% 0%;
    }

    .topnav .roll-link:hover .front {
      transform: translate3d(0, 40px, 0) rotate3d(1, 0, 0, -90deg);
      backface-visibility: hidden;
      perspective: 1000;
      opacity: 0;
    }

    .topnav .roll-link:hover .back {
      transform: rotate3d(1, 0, 0, 0deg);
      backface-visibility: hidden;
      perspective: 1000;
      opacity: 1;
    }

    /*style for button toggle menu*/
    nav .btn-menu {
      display: none;
    }

    /*style for main block*/
    main {
      grid-column: 2/5;
      grid-row: 2/3;
      width: 100%;
    }
    .date-wrap{
      margin-top: 35px;
      display: flex;
      flex-direction: row;
      justify-content: center;
    }

    .top-info{
      display: flex;
      flex-direction: row;
      align-items: flex-end;
      justify-content: space-evenly;
    }
    .brand {
      margin-top: 50px;
      color: #695753;
      font-size: 16px;
      font-weight: bold;
    }

    .date,
    .btn-search{
      min-width: 150px;
      height: 30px;
      border: none;
      background: #f5f5f5;
      outline: none;
      padding: 5px;
      color: #695753;
      font-family: "Montserrat", sans-serif;
      cursor: pointer;
      margin: 0px 10px;
    }
    .date:hover,
    .btn-search:hover{
      color: #f5f5f5;
      background: #b19891;
    }
    .brands-filter option:hover{
      background: #f5f5f5;
      color: #695753;
    }

    .wrapper {
      margin-top: 30px;
    }

    .container-sites {
      flex: 1;
    }
    .container-info {
      flex: 2;
      min-width: 380px;
    }

    .container-sites {
      border-right: solid 2px #b1989130;
    }

    .container-info div {
      margin-top: 5px;
      padding: 5px 5px 5px 30px;
      color: #695753;
    }

    /*style for sites-list*/
    .sites-list {
      align-items: flex-start;
      list-style-type: none;
      margin-right: 5px;
    }

    .sites-list li {
      margin-top: 5px;
      color: #695753;
      cursor: pointer;
      border-radius: 2px 2px;
      padding: 5px;
    }

    .sites-list li:hover {
      background-color: #695753;
      color: #f5f5f5;
    }
    .site a {
      color: #695753;
      text-decoration: none;
    }

    .sites-list li.hide{
      display: none;
    }

    /*style for sites-info*/
    .site-name {
      font-weight: bold;
    }
    .site-name span {
      padding: 5px;
      margin-top: 5px;
      display: block;
    }

    .brands-license .license,
    .jackpot,
    .bonus {
      display: flex;
      flex-flow: row nowrap;
      padding: 0px;
      margin: 0px;
      border: solid 2px #f5f5f5;
      border-top: none;
    }
    .license div {
      padding-right: 5px;
      padding-left: 5px;
      text-align: center;
    }
    .license .brand {
      font-size: 14px;
      font-weight: 400;
      flex: 2;
      min-width: 130px;
      margin: 0px;
    }
    .license .site {
      flex: 1;
      margin: 0px;
    }
    .license .licenses {
      flex: 1;
      margin: 0px;
    }
    .brands-license .license:first-child,
    .brands-license .license:first-child div,
    .jackpot:first-child,
    .bonus:first-child {
      background-color: #f5f5f5;
      border: solid 2px #f5f5f5;
    }

    .jackpots .jackpot,
    .bonuses .bonus {
      margin-top: 0px;
    }
    .jackpot a,
    .bonus a {
      color: #695753;
    }
    .wrap-table{
      display: flex;
      flex-direction: row;
      margin-top: 35px;
    }
    .table{
      flex: 1;
      border: solid 2px #f5f5f5;
    }
    .table-new-sites{
      border-left: solid 2px #f5f5f5;
      border-right: solid 1px #f5f5f5;
    }
    .table-removed-sites{
      border-right: solid 2px #f5f5f5;
      border-left: solid 1px #f5f5f5;
    }
    
    .th{
      margin-bottom: 10px;
      background-color: #f5f5f5;
      padding: 5px;
    }
    .th, .tr{
      display: flex;
      flex-direction: row;
    }
    .th div,
    .tr site{
      width: 100%;
    }
    .tr{
      position: relative;
    }
    .site{
      padding: 5px;
      border-radius: 2px 2px;
    }
    .site-info{
      display: none;
      background-color: #f5f5f5;
      position: absolute;
      width: 90%;
      top: -20%;
      z-index: 1;
      padding: 10px;
      border-radius: 2px 2px;
    }
    .site:hover{
      background-color: #f5f5f5;
    }
    .site:hover + .site-info{
      display: block;
    }
    .site-info span{
      padding-left: 5px;
      font-weight: 600;
    }




/* Preloader */

.preloader {
  background-color: rgba(251, 251, 251, .1);
  color: #fff;
  display: none;
  left: 0;
  position: fixed;
  top: 0;
  height: 100%;
  width: 100%;
}
.preloader.active {
  display: block;
}
#status {
  width: 200px;
  height: 200px;
  position: absolute;
  left: 50%;
  /* centers the loading animation horizontally one the screen */
  top: 50%;
  /* centers the loading animation vertically one the screen */
  background-image: url(https://raw.githubusercontent.com/niklausgerber/PreLoadMe/master/img/status.gif);
  /* path to your loading animation */
  background-repeat: no-repeat;
  background-position: center;
  margin: -100px 0 0 -100px;
  /* is width and height divided by two */
}





    @media (max-width: 500px) {
      .grid {
        grid-template-columns: 0.5fr 0.5fr 2fr 0.5fr 0.5fr;
      }
      header {
        grid-column: 2/5;
      }
      nav {
        flex-direction: row;
        height: auto;
        margin-top: 5px;
        justify-content: start;
      }

      nav .btn-menu {
        display: block;
        color: #695753;
        float: right;
        margin-right: 10px;
      }

      ul.topnav {
        display: none;
      }
      ul.topnav li {
        width: 90%;
        height: 40px;
      }

      .topnav li a {
        width: 100%;
        padding: 0.5em;
      }

      .roll-link {
        width: 100%;
      }

      .topnav.responsive {
        display: block;
      }

      .wrapper {
        flex-direction: column;
      }
      .container-sites,
      .container-info {
        border-left: none;
        border-right: none;
      }

      .container-info {
        border-right: none;
        border-top: solid 2px #b1989130;
        margin-top: 20px;
      }
    }
  </style>
  <body>
    <!-- Грид-контейенер общий -->
    <div class="grid">
      <!--Header-->
      <header>
        <!--блок навгации-->
        <nav class="navigation">
          <!-- Кнопка раскрытия меню -->
          <a href="javascript:void(0)" class="btn-menu">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="24"
              height="24"
              viewBox="0 0 24 24"
            >
              <path
                d="M3.5,7 C3.22385763,7 3,6.77614237 3,6.5 C3,6.22385763 3.22385763,6 3.5,6 L20.5,6 C20.7761424,6 21,6.22385763 21,6.5 C21,6.77614237 20.7761424,7 20.5,7 L3.5,7 Z M3.5,12 C3.22385763,12 3,11.7761424 3,11.5 C3,11.2238576 3.22385763,11 3.5,11 L20.5,11 C20.7761424,11 21,11.2238576 21,11.5 C21,11.7761424 20.7761424,12 20.5,12 L3.5,12 Z M3.5,17 C3.22385763,17 3,16.7761424 3,16.5 C3,16.2238576 3.22385763,16 3.5,16 L20.5,16 C20.7761424,16 21,16.2238576 21,16.5 C21,16.7761424 20.7761424,17 20.5,17 L3.5,17 Z"
              />
            </svg>
          </a>
          <ul class="topnav" id="myTopnav">
            <li>
              <div class="roll-link">
                <a href="/SlotApplication/" class="front">Home</a>
                <a href="/SlotApplication/" class="back">Home</a>
              </div>
            </li>
            <li>
              <div class="roll-link">
                <a href="/SlotApplication/tools" class="front">Tools</a>
                <a href="/SlotApplication/tools" class="back">Tools</a>
              </div>
            </li>
            <li>
              <div class="roll-link">
                <a href="/SlotApplication/slots" class="front">Slots</a>
                <a href="/SlotApplication/slots" class="back">Slots</a>
              </div>
            </li>
            <li>
              <div class="roll-link">
                <a href="/SlotApplication/jackpots" class="front">Jackpots</a>
                <a href="/SlotApplication/jackpots" class="back">Jackpots</a>
              </div>
            </li>
            <li>
              <div class="roll-link">
                <a href="/SlotApplication/sites" class="front">Sites</a>
                <a href="/SlotApplication/sites" class="back">Sites</a>
              </div>
            </li>
          </ul>
        </nav>
        <!--/ блок навгации-->
      </header>
      <!--/Header-->

      <!--main-->
      <main>
        <div class="date-wrap">
          <input type="date" class="date start-date">
          <input type="date" class="date end-date">
          <button class="btn-search">Search</button>
        </div>

        <div class="preloader">
          <div id="status">&nbsp;</div>
        </div>

        <div class="wrap-table">
          <div class="table table-new-sites">

          </div>
          <div class="table table-removed-sites">
            
          </div>

        </div>
      </main>
      <!-- /main -->

      <!-- footer -->
      <footer></footer>
      <!-- /footer -->
    </div>

    <script>
      const startDate = document.querySelector('.start-date');
      const endDate = document.querySelector('.end-date');
      const now = new Date().toISOString().slice(0,10);
      startDate.value = now;
      endDate.value = now;
      startDate.max = now;
      endDate.max = now;
      

      startDate.addEventListener('change', function(){
        console.log(startDate.value);
        endDate.min = startDate.value;
      })
      endDate.addEventListener('change', function(){
        console.log(endDate.value);
      })

      const btnSearch = document.querySelector('.btn-search');
      btnSearch.addEventListener('click', getSites);

      function getSites(){
        $('.preloader').addClass('active');
       let newSitesTable = document.querySelector('.table-new-sites'),
              removedSitesTable = document.querySelector('.table-removed-sites');
              newSitesTable.innerHTML = '';
              removedSitesTable.innerHTML = '';
 
        let requestDate = {
          startDate: startDate.value,
          endDate: endDate.value
        }
        console.log(requestDate);

      /*$.ajax({
        type: "POST",
        url: '/SlotApplication/new-sites',
        data: requestDate,
        success: function (data) {
          console.log(data);
        },
      });*/
        
        const sitesLink = "/SlotApplication/new-sites";
        const requestDataSites = new FormData();
        requestDataSites.append('startDate', startDate.value);
        requestDataSites.append('endDate', endDate.value);

        fetch(sitesLink, {
          method: "POST",
          body: requestDataSites,
        })
          .then((response) => response.json())
          .then((data) => {
            drawTable(data);
          })
          .catch((error) => console.error("Ошибка получения данных" + error));
        }

        function drawTable(data){
          console.log(data);
          $('.preloader').removeClass('active');
          let newSitesData = data.newSites,
              removedSitesData = data.removedSites,
              newSitesTable = document.querySelector('.table-new-sites'),
              removedSitesTable = document.querySelector('.table-removed-sites');
              
          newSitesTable.innerHTML = '<div class="th"><div>New Sites</div></div>';
          removedSitesTable.innerHTML = '<div class="th"><div>Removed Sites</div></div>';

          newSitesData.forEach(elem => {
            let itemSite = document.createElement('div');
            itemSite.classList.add('tr');
            itemSite.innerHTML = '<div class="site">' + elem.site + '</div>';
            newSitesTable.append(itemSite);
            let site = itemSite.querySelector('.site');

            let siteInfo = document.createElement('div');
            siteInfo.classList.add('site-info');
            siteInfo.innerHTML = '<div>brand:<span>' + elem.brand + '</span></div><div>creation date:<span>' + elem.creationDate + '</span></div><div>removing date:<span>' + elem.removingDate + '</span></div>';
            itemSite.append(siteInfo);
            siteInfo.style.left = site.offsetWidth + 20;
          });

          removedSitesData.forEach(elem => {
            let itemSite = document.createElement('div');
            itemSite.classList.add('tr');
            itemSite.innerHTML = '<div class="site">' + elem.site + '</div>';
            removedSitesTable.append(itemSite);
            let site = itemSite.querySelector('.site');

            let siteInfo = document.createElement('div');
            siteInfo.classList.add('site-info');
            siteInfo.innerHTML = '<div>brand:<span>' + elem.brand + '</span></div><div>creation date:<span>' + elem.creationDate + '</span></div><div>removing date:<span>' + elem.removingDate + '</span></div>';
            itemSite.append(siteInfo);
            siteInfo.style.left = site.offsetWidth + 20;
          });
          
         
          
        }



      



      

      const btnMenu = document.querySelector(".btn-menu");
      btnMenu.addEventListener("click", toggleMenu);

      function toggleMenu() {
        var x = document.getElementById("myTopnav");
        if (x.className === "topnav") {
          x.className += " responsive";
        } else {
          x.className = "topnav";
        }
      }
    </script>
  </body>
</html>