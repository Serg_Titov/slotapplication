<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>Bonuses & slots analytics</title>
  </head>
  <style>
    #menu {
        margin-left: 30%;
    }
    .menu-li {
        display: inline;
        margin: -2.5;
    }
    .menu-ul {
        list-style: none;
        margin: 0;
        padding-left: 0;
    }
    .menu-a {
        text-decoration: none;
        padding: 1em;
        background: #f5f5f5;
        border: 1px solid #b19891;
        color: #695753;
    }
  </style>
  <body>
        <div id="menu">
            <nav>
                <ul class="menu-ul">
                  <li class="menu-li"><a class="menu-a" href='/SlotApplication/'>Home</a></li>
                  <li class="menu-li"><a class="menu-a" href='/SlotApplication/tools'>Tools</a></li>
                  <li class="menu-li"><a class="menu-a" href='/SlotApplication/slots'>Slots</a></li>
                  <li class="menu-li"><a class="menu-a" href='/SlotApplication/jackpots'>Jackpots</a></li>
                </ul>
            </nav>
        </div>
        <span>Выберите режим работы:</span> <br>
        <a href="/SlotApplication/start?version=admin">Администратор</a> <br>
        <a href="/SlotApplication/start?version=demo">Демо</a> <br>
        <a href="/SlotApplication/start?version=lite">Lite</a> <br>
        <a href="/SlotApplication/start?version=pro">Pro</a> <br>
        <a href="/SlotApplication/start?version=casino">Casino</a> <br>
  </body>
</html>