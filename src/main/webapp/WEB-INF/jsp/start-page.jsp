<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>Bonuses & slots analytics</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script>
        function createMenu(){
          $.ajax({
              type: 'POST',
              url: '/SlotApplication/getMenu',
              data: {urlParam : document.location.search},
              success: function(data){
                  let menuNav = document.getElementById("menuVertical");
                  let menuUl = document.createElement("ul");
                  menuNav.appendChild(menuUl);
                  for (let [key, value] of Object.entries(data.firstLevel)){
                      let menuItemLi = document.createElement("li");
                      let menuItemA = document.createElement("a");
                      menuItemA.innerText = key;
                      menuItemLi.appendChild(menuItemA);
                      menuUl.appendChild(menuItemLi);
                      let menuSecondUl = document.createElement("ul");
                      menuItemLi.appendChild(menuSecondUl);

                      for(let menuItem of value){
                          let menuItemSecondLi = document.createElement("li");
                          let menuItemSecondA = document.createElement("a");
                          menuItemSecondA.setAttribute("href", menuItem.url);
                          menuItemSecondA.innerText = menuItem.itemName
                          menuItemSecondLi.appendChild(menuItemSecondA);
                          menuSecondUl.appendChild(menuItemSecondLi);
                      }
                  }
                  let documentationLi = document.createElement("li");
                  let documentationA = document.createElement("a");
                  documentationA.innerText = "Документация";
                  documentationA.setAttribute("href", "/SlotApplication/documentation");
                  documentationLi.appendChild(documentationA);
                  menuUl.appendChild(documentationLi);
              },
              error: function(data){
                  let errorContainer = document.getElementById("errorMessage");
                  let errorLabel = document.createElement("label");
                  errorLabel.innerText = data;
                  errorContainer.appendChild(errorLabel);
              }
          });
      }
  </script>
  </head>
  <style>
    #menu {
        margin-left: 30%;
    }
    .menu-li {
        display: inline;
        margin: -2.5;
    }
    .menu-ul {
        list-style: none;
        margin: 0;
        padding-left: 0;
    }
    .menu-a {
        text-decoration: none;
        padding: 1em;
        background: #f5f5f5;
        border: 1px solid #b19891;
        color: #695753;
    }
    .header {
      padding: 20px 20px;
      text-align: left;
    }
    .header h1 {
      font-family: 'Righteous', cursive;
      position: relative;
      display: inline-block;
      border-top: 2px solid;
      border-bottom: 2px solid;
      font-size: 30px;
      padding: 11px 60px;
      margin: 0;
      line-height: 1;
    }
    @media (max-width: 420px) {
      .header h1 {font-size: 2em;}
    }

    #container, header{
	width:1000px;
	margin:0px auto;
    }

    #container{
	box-sizing:border-box;
    }

	#left{
		width:200px;
		height:auto;
		z-index:15;
		margin-left:-220px;
		float:left;
	}

    #menuVertical{
        width:100%;
        height:auto;
    }

	#menuVertical ul{
		display:block;
		width:100%;
		height:auto;
		margin:0px;
		padding:0px;
		list-style:none;
		position:relative;
	}

	#menuVertical ul li{
	    display:block;
	    width:100%;
	    height:auto;
	    position:relative;
	}

	#menuVertical ul li a{
		position:relative;
		display:block;
		width:100%;
		height:auto;
		box-sizing:border-box;
		font-size:13px;
		text-transform:uppercase;
		font-weight:bold;
		color:#FBF7F7;
		line-height:1.2em;
		padding:10px 15px;
		background:#b1b4b5;
		border-top:1px solid #236A92;
		text-decoration:none;
	}

	#menuVertical ul  li:first-child a{
	    border:0px;
	}

	#menuVertical ul li a:hover, #menuVertical ul li:hover a{
		background:#7e7f7f;
	}
	#menuVertical ul li ul{
		position:absolute;
		top:0px;
		left:100%;
		display:none;
		width:auto;
	}

	#menuVertical ul li:hover ul{
	    display:block;
	}

	#menuVertical ul li ul li a{
		white-space:nowrap;
		text-transform:none;
		padding:5px 20px;
	}

	#menuVertical ul li ul li a:hover{
		background:#b1b4b5;
		color:#0A3CC1;
	}

  </style>
  <body>
  <div class="header"><h1>Программа для расчета доходности бонусов</h1></div>
  <br>
  <script>
        createMenu();
  </script>
  <div id="menu">
      <nav>
          <ul class="menu-ul">
              <li class="menu-li"><a class="menu-a" href='/SlotApplication/'>Home</a></li>
              <li class="menu-li"><a class="menu-a" href='/SlotApplication/tools'>Tools</a></li>
              <li class="menu-li"><a class="menu-a" href='/SlotApplication/slots'>Slots</a></li>
              <li class="menu-li"><a class="menu-a" href='/SlotApplication/jackpots'>Jackpots</a></li>
          </ul>
      </nav>
  </div>
  <div id="container">
    <div id="left">
        <nav id="menuVertical">
            <!--<ul>
                <li><a>Рассчитать результат из файла</a>
                    <ul>
                        <li><a href="/SlotApplication/upload">Загрузить слот</a></li>
                	    <li><a href="/SlotApplication/calc-home-db">Рассчитать слот</a></li>
                		<li><a href="/SlotApplication/slot-info">Рассчитать бонус</a></li>
                		<li><a href="/SlotApplication/cashback">Рассчитать кешбек</a></li>
                        <li><a href="/SlotApplication/spinner-home">Шанс сделать n спинов и остаться в плюсе</a></li>
                	</ul>
                </li>
                <li><a>Рассчитать результат из базы</a>
                    <ul>
                         <li><a href="/SlotApplication/calc-db">Рассчитать слот</a></li>
                         <li><a href="/SlotApplication/report">Рассчитать бонус</a></li>
                         <li><a href="/SlotApplication/calc-provider">Рассчитать все слоты провайдера</a></li>
                         <li><a href="/SlotApplication/calc-all-slots">Рассчитать все слоты в базе</a></li>
                         <li><a href="/SlotApplication/spinner-db">Шанс сделать n спинов и остаться в плюсе</a></li>
                    </ul>
                </li>
                <li><a href="/SlotApplication/documentation">Подробное руководство</a></li>
            </ul> -->
        </nav>
    </div>
  </div>
  <div id="errorMessage"></div>
  <br>
  <br>
  <br>
  <% String fileUploaded = (String) request.getAttribute("completed");
      if (fileUploaded != null){ %>
       <br><label>${completed}</label>
     <%}%>
  <br><br>
  <br>
  <% String cashback = (String) request.getAttribute("cashback");
    if (cashback != null){ %>
     <label>${cashback}</label>
   <%}%>
  </body>
</html>