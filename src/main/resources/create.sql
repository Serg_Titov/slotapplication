CREATE TABLE slot_info (
    id_run bigint PRIMARY KEY,
    bid double precision,
    win double precision,
    bonus double precision,
    win_coefficient double precision,
    game VARCHAR(255)
);

CREATE TABLE slot_name (
    id_name bigint PRIMARY KEY,
    name VARCHAR(255),
    provider VARCHAR(255)
);

