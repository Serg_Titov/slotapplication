CREATE TABLE slot_input (
    id_input bigint PRIMARY KEY,
    deposit double precision,
    bonus double precision,
    without_bonus integer,
    tax double precision,
    bet double precision,
    max_profit double precision,
    wagger integer,
    wagger_type VARCHAR(10),
    sim integer,
    game VARCHAR(100),
    version VARCHAR(20),
    provider_fee double precision,
    payment_fee double precision,
    platform_fee double precision,
    spin_limit double precision
);

CREATE TABLE slot_information(
    id_info integer PRIMARY KEY,
    slot_name VARCHAR(50),
    provider VARCHAR(50)
);

CREATE TABLE common_slot_information (
    id_common_info integer PRIMARY KEY,
    amount_spins bigint,
    max_win double precision,
    rtp double precision,
    volatility double precision,
    bonus_freq double precision,
    wins_bonus_rate double precision,
    wins_without_bonus_rate double precision,
    avg_bonus_win double precision,
    game_id integer,
    FOREIGN KEY (game_id) REFERENCES slot_information (id_info)
);

CREATE TABLE common_sim_info (
    id_common_sim integer PRIMARY KEY,
    profit double precision,
    ev double precision,
    casino_ev double precision,
    roi double precision,
    casino_roi double precision,
    profit_prob double precision,
    nonprofit_prob double precision,
    longest_profit_series integer,
    longest_profit_frequency double precision,
    longest_nonprofit_series integer,
    longest_nonprofit_frequency double precision,
    average_win double precision,
    bet_count bigint,
    id_slot_input bigint,
    FOREIGN KEY (id_slot_input) REFERENCES slot_input (id_input)
);

CREATE TABLE nonprofit_series (
    id_series bigint PRIMARY KEY,
    series_name VARCHAR(50),
    series bigint,
    id_slot_input bigint,
    FOREIGN KEY (id_slot_input) REFERENCES slot_input (id_input)
);

CREATE TABLE database_multipliers (
    id_multiplier bigint PRIMARY KEY,
    multiplier integer,
    multiplier_freq double precision,
    game_id integer,
    FOREIGN KEY (game_id) REFERENCES slot_information (id_info)
);

CREATE TABLE top_profits (
    id_top_profits bigint PRIMARY KEY,
    name integer,
    profit double precision,
    frequency double precision,
    id_slot_input bigint,
    FOREIGN KEY (id_slot_input) REFERENCES slot_input (id_input)
);

CREATE TABLE slot_points(
    id_slot_point bigint PRIMARY KEY,
    game VARCHAR(100),
    provider VARCHAR(255),
    spin_limit VARCHAR(100),
    point_result double precision
);

CREATE TABLE deposit_multipliers (
    id_dep_multiplier bigint PRIMARY KEY,
    dep_multiplier integer,
    dep_multiplier_freq double precision,
    id_slot_input bigint,
    FOREIGN KEY (id_slot_input) REFERENCES slot_input (id_input)
);

CREATE TABLE providers(
    provider_name VARCHAR(100) PRIMARY KEY
);

CREATE TABLE users_bonus_audit(
    user_audit_id bigint PRIMARY KEY,
    username VARCHAR(250),
    login_date DATE,
    bonus_count integer
);

CREATE TABLE Slots.slots_brief_info(
    brief_id bigint PRIMARY KEY,
    provider VARCHAR(100),
    slot_name VARCHAR(150),
    spin_amount bigint,
    creation_date DATE
);

CREATE TABLE Slots.users(
    users_id bigint PRIMARY KEY,
    login VARCHAR(30),
    password VARCHAR(250),
    email VARCHAR(100),
    role VARCHAR(20)
);

CREATE TABLE Slots.user_control(
    users_id bigint PRIMARY KEY,
    login VARCHAR(30),
    password VARCHAR(250),
    email VARCHAR(100),
    role VARCHAR(20),
    agreement VARCHAR(1),
    subscription_start TIMESTAMP,
    subscription_days integer
);

CREATE TABLE Slots.casino_sites(
    casino_sites_id bigint PRIMARY KEY,
    casino_site VARCHAR(200),
    casino_brand VARCHAR(150),
    activity VARCHAR(1),
    creation_date DATE,
    removing_date DATE
);

CREATE TABLE Slots.sites_licenses(
    sites_licenses_id bigint  PRIMARY KEY,
    sites_id bigint,
    license VARCHAR(50)
);

CREATE TABLE Slots.sites_bonuses(
    sites_bonuses_id bigint PRIMARY KEY,
    site_id bigint,
    bonus_id bigint
);

CREATE TABLE Slots.jackpot_summary(
    jackpot_summary_id bigint NOT NULL AUTO_INCREMENT,
    unique_jackpots int,
    paid_out_amount int,
    counter_100rtp int,
    slot VARCHAR(50),
    time_check TIMESTAMP,
    PRIMARY KEY (jackpot_summary_id)
);

CREATE TABLE Slots.jackpot_summary_parameters(
    summary_parameter_id bigint NOT NULL AUTO_INCREMENT,
    jackpot_summary_id bigint,
    site VARCHAR(255),
    brand VARCHAR(255),
    slot VARCHAR(50),
    amount double precision,
    parameter_date DATE,
    parameter_name VARCHAR(50),
    PRIMARY KEY (summary_parameter_id),
    FOREIGN KEY (jackpot_summary_id) REFERENCES jackpot_summary (jackpot_summary_id)
);

CREATE TABLE Slots.jackpot_growth(
    jackpot_growth_id bigint NOT NULL AUTO_INCREMENT,
    jackpot_summary_id bigint,
    site VARCHAR(255),
    brand VARCHAR(255),
    growth_rate double precision,
    time_period VARCHAR(10),
    PRIMARY KEY (jackpot_growth_id),
    FOREIGN KEY (jackpot_summary_id) REFERENCES jackpot_summary (jackpot_summary_id)
);

ALTER TABLE deposit_multipliers
DROP COLUMN game;

ALTER TABLE database_multipliers
ADD COLUMN game VARCHAR(100);

ALTER TABLE slot_information
DROP COLUMN id_slot_input;

DROP TABLE common_slot_information;

CREATE TABLE common_slot_information

DROP TABLE database_multipliers;


ALTER TABLE Slots.slot_points
ADD COLUMN provider VARCHAR(255);

ALTER TABLE Slots.common_slot_information
ADD bonus_freq double precision NULL;

ALTER TABLE Slots.common_slot_information
ADD wins_bonus_rate double precision NULL;

ALTER TABLE Slots.common_slot_information
ADD wins_without_bonus_rate double precision NULL;

ALTER TABLE Slots.common_sim_info
ADD bonus_volatility double precision NULL;

ALTER TABLE Slots.slot_input
ADD tax double precision NULL
DEFAULT 0;

ALTER TABLE Slots.slot_input
ADD spin_limit double precision NULL
DEFAULT 0;

ALTER TABLE Slots.slot_input
ADD max_profit double precision NULL
DEFAULT 0;

ALTER TABLE Slots.slot_input
ADD version VARCHAR(20) NULL
DEFAULT 'pro';

ALTER TABLE Slots.slot_input
ADD bonus_parts integer NULL
DEFAULT 0;

ALTER TABLE Slots.slot_input
ADD provider_fee double precision NULL
DEFAULT 0;

ALTER TABLE Slots.slot_input
ADD payment_fee double precision NULL
DEFAULT 0;

ALTER TABLE Slots.slot_input
ADD platform_fee double precision NULL
DEFAULT 0;

ALTER TABLE Slots.common_sim_info
ADD average_win double precision NULL
DEFAULT NULL;

ALTER TABLE Slots.common_slot_information
ADD avg_bonus_win double precision NULL
DEFAULT NULL;

ALTER TABLE Slots.common_sim_info
ADD bet_count bigint NULL
DEFAULT NULL;

ALTER TABLE Slots.common_sim_info
ADD casino_ev double precision NULL
DEFAULT NULL;

ALTER TABLE Slots.common_sim_info
ADD casino_roi double precision NULL
DEFAULT NULL;

ALTER TABLE Slots.casino_sites
ADD activity VARCHAR(1)
DEFAULT 'Y';

ALTER TABLE Slots.casino_sites
ADD COLUMN creation_date DATE
DEFAULT '2022-04-14';

ALTER TABLE Slots.casino_sites
ADD COLUMN removing_date DATE
DEFAULT NULL;

ALTER TABLE Slots.jackpot_summary_parameters
ADD COLUMN parameter_name VARCHAR(50)
DEFAULT NULL;
