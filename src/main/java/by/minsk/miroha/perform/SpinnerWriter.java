package by.minsk.miroha.perform;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import au.com.bytecode.opencsv.CSVWriter;
import by.minsk.miroha.entities.databases.SlotName;
import by.minsk.miroha.entities.report.SlotPoint;
import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.repositories.SlotNameRepository;
import by.minsk.miroha.repositories.impl.SlotNameRepositoryImpl;
import by.minsk.miroha.repositories.report.SlotPointsRepository;
import by.minsk.miroha.repositories.report.impl.SlotPointsRepositoryImpl;

public class SpinnerWriter {

    public static final int MILLION = 1000000;
    public static final int INCREASE_SIM_RATE = 2;
    public static final int SIM_LOOP_LIMIT = 3;

    public static final SlotPointsRepository repository = new SlotPointsRepositoryImpl();
    public SlotNameRepository nameRepository;

    public static final String SM_GOES_WRONG = "Чтото пошло не так...(";

    private String dbName;
    private String tableName;

    public SpinnerWriter() {
        nameRepository = new SlotNameRepositoryImpl();
    }

    public SpinnerWriter(String dbName, String tableName) {
        this.dbName = dbName;
        this.tableName = tableName;
    }

    public void writeToCSV(boolean external){
        SlotName slotName;
        if (external){
            slotName = new SlotName(tableName, dbName);
        } else {
            slotName = nameRepository.getSlotName();
        }
        File file = new File("spinResult.csv");
        try {
            FileWriter writer = new FileWriter(file);
            CSVWriter csvWriter = new CSVWriter(writer);
            String[] header = {"X", "Y"};
            csvWriter.writeNext(header);
            csvWriter.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        double result;
        writeOnePoint(1, MILLION, slotName, external);
        writeOnePoint(2, MILLION, slotName, external);
        writeOnePoint(5, MILLION, slotName, external);
        writeOnePoint(10, MILLION, slotName, external);
        writeOnePoint(20, MILLION, slotName, external);
        writeOnePoint(50, MILLION, slotName, external);
        writeOnePoint(100, MILLION, slotName, external);
        writeOnePoint(200, MILLION, slotName, external);
        writeOnePoint(500, MILLION, slotName, external);
        writeOnePoint(1000, MILLION, slotName, external);
        writeOnePoint(2000, MILLION, slotName, external);
        writeOnePoint(5000, 200000, slotName, external);
        writeOnePoint(10000, 100000, slotName, external);
        writeOnePoint(20000, 100000, slotName, external);
        writeOnePoint(50000, 50000, slotName, external);
        result = writeOnePoint(100000, 50000, slotName, external);
        result = writeLastsPoints(200000, 20000, slotName, external, result);
        result = writeLastsPoints(500000, 20000, slotName, external, result);
        writeLastsPoints(1000000, 20000, slotName, external, result);
    }

    private static double writeOnePoint(Integer spinLimit, int sim, SlotName slotName, boolean external){
        SpinnerPerformer performer;
        if (external){
            performer = new SpinnerPerformer(slotName.getProvider(), slotName.getSlotName());
        } else {
            performer = new SpinnerPerformer();
        }
        double result = performer.getSpinnerResult(spinLimit, sim, external);
        try {
            FileWriter writer = new FileWriter("spinResult.csv", true);
            CSVWriter csvWriter = new CSVWriter(writer);
            String[] header = {spinLimit.toString(), Double.toString(result)};
            csvWriter.writeNext(header);
            csvWriter.close();
            repository.save(new SlotPoint(slotName.getSlotName(), slotName.getProvider(), spinLimit.toString(), result));
            return result;
        }
        catch (IOException e) {
            e.printStackTrace();
            throw new CustomValidation(new String(SM_GOES_WRONG.getBytes(), StandardCharsets.UTF_8));
        }
    }

    private static double writeLastsPoints(Integer spinLimit, int sim, SlotName slotName,
                                           boolean external, double previousResult){
        SpinnerPerformer performer;
        if (external){
            performer = new SpinnerPerformer(slotName.getProvider(), slotName.getSlotName());
        } else {
            performer = new SpinnerPerformer();
        }

        double result = performer.getSpinnerResult(spinLimit, sim, external);
        int loopCounter = 0;
        int newSimValue = sim;
        while (result > previousResult){
            if (loopCounter == SIM_LOOP_LIMIT){
                break;
            }
            newSimValue = newSimValue * INCREASE_SIM_RATE;
            result = performer.getSpinnerResult(spinLimit, newSimValue, external);
            loopCounter++;
        }
        try {
            FileWriter writer = new FileWriter("spinResult.csv", true);
            CSVWriter csvWriter = new CSVWriter(writer);
            String[] header = {spinLimit.toString(), Double.toString(result)};
            csvWriter.writeNext(header);
            csvWriter.close();
            repository.save(new SlotPoint(slotName.getSlotName(), slotName.getProvider(), spinLimit.toString(), result));
            return result;
        }
        catch (IOException e) {
            e.printStackTrace();
            throw new CustomValidation(new String(SM_GOES_WRONG.getBytes(), StandardCharsets.UTF_8));
        }
    }

}
