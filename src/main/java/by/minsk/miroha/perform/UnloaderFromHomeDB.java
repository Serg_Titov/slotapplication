package by.minsk.miroha.perform;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import by.minsk.miroha.entities.PairRandom;
import by.minsk.miroha.entities.enums.UserGrants;
import by.minsk.miroha.repositories.SlotInfoExternalRepository;
import by.minsk.miroha.repositories.impl.SlotInfoExternalRepositoryImpl;
import by.minsk.miroha.services.SlotInfoService;
import by.minsk.miroha.services.impl.SlotInfoHomeServiceImpl;

public class UnloaderFromHomeDB {

    public static final int ONE_PART_UNLOAD_FROM_DB = 1000000;
    public static final int COEFFICIENT_SIZE = 1000000;
    public static final int BORDER_VALUE = 1600000;

    private SlotInfoService slotInfoService;
    private List<Double> allCoefficients;
    private SlotInfoExternalRepository repository;

    public UnloaderFromHomeDB(boolean adminMode) {
        slotInfoService = new SlotInfoHomeServiceImpl();
        if (slotInfoService.countAll() < BORDER_VALUE){
            allCoefficients = slotInfoService.getAllCoefficients();
        }
        if(!adminMode){
            allCoefficients = slotInfoService.getSubsetOfSlotInfo(BORDER_VALUE, 0);
        }
    }

    public UnloaderFromHomeDB(String provider, String slot, boolean adminMode) {
        repository = new SlotInfoExternalRepositoryImpl(provider, slot);
        int dbSize = repository.countAll();
        if (dbSize < BORDER_VALUE){
            allCoefficients = repository.getAllCoef();
        } else if (!adminMode){
            allCoefficients = repository.getSubsetOfSlotInfo(BORDER_VALUE, 0);
        }
    }

    /**
     * Вычисляется размер локальной базы данных (количество строк).
     * Генерируется COEFFICIENT_SIZE (2 000 000) случайных чисел от 1 до
     *  размера локальной БД.
     * Создается структура: массив пар:
     *  Случайное число от  1 до размер базы - коэффициент соответствующий
     *  показателю win/bid, которые берутся в локальной бд в строчке под номером,
     *  который указан в первом показателе.
     *  Метод возвращает только список коэффициентов.
     *
     * @return список коэффициентов.
     */
    public List<Double> getAllCoefficients() {
        if (allCoefficients != null){
            return getCoefInCaseAllCoefs();
        } else {
            int dbSize;
            List<PairRandom> randomIndexes = new ArrayList<>();
            if (slotInfoService != null) {
                dbSize = slotInfoService.countAll();
                for (int counter = 0; counter < COEFFICIENT_SIZE; counter++) {
                    Random random = new Random();
                    PairRandom pairRandom = new PairRandom(random.nextInt(dbSize), -1D);
                    randomIndexes.add(pairRandom);
                }
                fillWinCoefficients(randomIndexes, dbSize, null);
            } else {
                dbSize = repository.countAll();
                for (int counter = 0; counter < COEFFICIENT_SIZE; counter++) {
                    Random random = new Random();
                    PairRandom pairRandom = new PairRandom(random.nextInt(dbSize), -1D);
                    randomIndexes.add(pairRandom);
                }
                fillWinCoefficients(randomIndexes, dbSize, repository);
            }
            return randomIndexes.stream().map(PairRandom::getCoefficient).collect(Collectors.toList());
        }
    }

    private List<Double> getCoefInCaseAllCoefs() {
        List<Double> resultList = new ArrayList<>();
        for (int counter = 0; counter < COEFFICIENT_SIZE; counter++){
            Random random = new Random();
            resultList.add(allCoefficients.get(random.nextInt(allCoefficients.size())));
        }
        return resultList;
    }

    /**
     * Определяется, какое количество раз будут выгружаться данные из локальной БД.
     * Запускается цикл, на выгрузку данных частями из БД.
     * За раз из БД выгружается (ONE_PART_UNLOAD_FROM_DB) элементов.
     *      В первый раз выгрузится с 1 по ONE_PART_UNLOAD_FROM_DB элемент,
     *      во второй раз выгрузится с ONE_PART_UNLOAD_FROM_DB + 1 по 2* ONE_PART_UNLOAD_FROM_DB элемент
     *      и т.д. пока не будут выгружены все эллементы из БД.
     * На вход в метод поступил список структур:
     *      Номер случайной строки в БД - коэффициент win/bid из этой строки.
     * изначально все коэффициенты равны -1, это говорит о том, что коэффициент еще не заполнен.
     * Далее проходим по каждому элементу этой структуры:
     *      если коэффициент еще не заполнен и номер случайно строки попадает в перечень строк,
     *      которые выгружены в текущей итерации цикла, то коэффициент заполняется, иначе переходим
     *      к следующему элементу структуры.
     *
     * @param randomIndexes перечень случайный коэффициентов
     * @param dbSize размер БД
     */
    private void fillWinCoefficients(List<PairRandom> randomIndexes, int dbSize,
                                            SlotInfoExternalRepository repository){
        int countOfCycles = dbSize/ONE_PART_UNLOAD_FROM_DB + 1;
        List<Double> tempCoefficients;
        for (int counter = 0; counter < countOfCycles; counter++){
            if (repository == null) {
                tempCoefficients = slotInfoService.getSubsetOfSlotInfo(ONE_PART_UNLOAD_FROM_DB, counter);
            } else {
                tempCoefficients = repository.getSubsetOfSlotInfo(ONE_PART_UNLOAD_FROM_DB, counter);
            }
            int minIndex = counter*ONE_PART_UNLOAD_FROM_DB;
            int maxIndex = (counter+1)*ONE_PART_UNLOAD_FROM_DB;
            for (PairRandom randomIndex : randomIndexes) {
                if (randomIndex.getCoefficient() == -1 &&
                    randomIndex.getIndex() >= minIndex &&
                    randomIndex.getIndex() <= maxIndex) {
                    int currentIndex = randomIndex.getIndex()%ONE_PART_UNLOAD_FROM_DB;
                    randomIndex.setCoefficient(tempCoefficients.get(currentIndex));
                }
            }
            tempCoefficients.clear();
        }
    }
}
