package by.minsk.miroha.perform;

import by.minsk.miroha.entities.databases.JackpotGrowth;
import by.minsk.miroha.entities.databases.JackpotSummary;
import by.minsk.miroha.entities.databases.SummaryParameter;
import by.minsk.miroha.entities.enums.jackpots.JackpotMultipleSlot;
import by.minsk.miroha.entities.enums.jackpots.JackpotSlot;
import by.minsk.miroha.entities.enums.jackpots.JackpotSummaryParameter;
import by.minsk.miroha.entities.front.report.PaidOutJackpotCounter;
import by.minsk.miroha.entities.front.responses.GrownJackpotResponse;
import by.minsk.miroha.report.jackpot.GrownJackpots;
import by.minsk.miroha.report.jackpot.JackpotsMainReport;
import by.minsk.miroha.report.jackpot.LastPayedOutJackpot;
import by.minsk.miroha.report.jackpot.MaxJackpotValue;
import by.minsk.miroha.repositories.jackpots.JackpotGrowthRepository;
import by.minsk.miroha.repositories.jackpots.JackpotSummaryParameterRepository;
import by.minsk.miroha.repositories.jackpots.JackpotSummaryRepository;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PerformerJackpotSummaries {

    private static final String SLOTS_SUMMARY = "Total";

    private final JackpotMainPerformer performer;
    private final JackpotSummaryRepository summaryRepository;
    private final JackpotSummaryParameterRepository parameterRepository;
    private final JackpotGrowthRepository growthRepository;

    public PerformerJackpotSummaries(JackpotMainPerformer performer, JackpotSummaryRepository summaryRepository,
                                     JackpotSummaryParameterRepository parameterRepository,
                                     JackpotGrowthRepository growthRepository) {
        this.performer = performer;
        this.summaryRepository = summaryRepository;
        this.parameterRepository = parameterRepository;
        this.growthRepository = growthRepository;
    }

    public void prepareJackpotSummaries(){
        List<JackpotsMainReport> allReports = new ArrayList<>();
        for (JackpotSlot slot : JackpotSlot.values()){
            JackpotMultipleSlot multipleSlot = JackpotSlot.getMultipleSlot(slot);
            if(multipleSlot == null){
                JackpotsMainReport report = performer.getReport(slot);
                allReports.add(report);
                saveSingleSummary(report, slot);
            } else {
                JackpotsMainReport report = performer.getMultipleReport(slot);
                allReports.add(report);
                saveMultipleSummary(report, slot);
            }
        }
        saveCommonReport(allReports);
    }

    private void saveSingleSummary(JackpotsMainReport report, JackpotSlot slot){
        JackpotSummary summary = new JackpotSummary();
        summary.setSlot(slot.getSlotName());
        summary.setUniqueJackpots(report.getUniqueJackpots());
        if(report.getPayedOutJackpotsCountList().size() > 0) {
            summary.setPaidOutAmount(report.getPayedOutJackpotsCountList().get(0).getCounter());
        } else {
            summary.setPaidOutAmount(0);
        }
        summary.setCounter100Rtp(report.getReach100Rtp());
        summary.setTimeCheck(new Timestamp(new Date().getTime()));
        JackpotSummary savedSummary = summaryRepository.saveSummary(summary);

        List<SummaryParameter> summaryParameters = new ArrayList<>();
        if(report.getMaxValueCurrentList().size() > 0) {
            SummaryParameter maxCurrentParameter = getMaxValue(report.getMaxValueCurrentList().get(0),
                    JackpotSummaryParameter.CURRENT, slot.getSlotName(), savedSummary.getJackpotSummaryId());
            summaryParameters.add(maxCurrentParameter);
        }

        if(report.getMaxValuePayedOutList().size() > 0) {
            SummaryParameter maxPaidOutValue = getMaxValue(report.getMaxValuePayedOutList().get(0),
                    JackpotSummaryParameter.MAX, slot.getSlotName(), savedSummary.getJackpotSummaryId());
            summaryParameters.add(maxPaidOutValue);
        }

        if(report.getLastPayedOutList().size() > 0) {
            SummaryParameter lastPaidOut = getLastPaidOut(report.getLastPayedOutList().get(0), slot.getSlotName(),
                    savedSummary.getJackpotSummaryId());
            summaryParameters.add(lastPaidOut);
        }

        parameterRepository.saveParameters(summaryParameters);

        saveJackpotGrowth(report.getGrownValues(), savedSummary.getJackpotSummaryId());
    }

    private void saveMultipleSummary(JackpotsMainReport report, JackpotSlot slot){
        List<JackpotSummary> savedSummaries = new ArrayList<>();
        List<JackpotMultipleSlot> multipleSlotList = JackpotMultipleSlot.getListSlots(slot);
        for(JackpotMultipleSlot multipleSlot : multipleSlotList){
            JackpotSummary summary = new JackpotSummary();
            summary.setSlot(multipleSlot.getSlotName());
            summary.setUniqueJackpots(report.getUniqueJackpots());
            for(PaidOutJackpotCounter counter : report.getPayedOutJackpotsCountList()){
                if(multipleSlot.getSlotName().equals(counter.getSlotName())){
                    summary.setPaidOutAmount(counter.getCounter());
                    break;
                }
            }
            summary.setCounter100Rtp(report.getReach100Rtp());
            summary.setTimeCheck(new Timestamp(new Date().getTime()));
            JackpotSummary savedSummary = summaryRepository.saveSummary(summary);
            savedSummaries.add(savedSummary);
        }

        List<SummaryParameter> summaryParameters = new ArrayList<>();
        for(JackpotSummary savedSummary : savedSummaries){
            for(MaxJackpotValue currentValue : report.getMaxValueCurrentList()){
                if(savedSummary.getSlot().equals(currentValue.getSlotName())){
                    SummaryParameter currentMaxValue = getMaxValue(currentValue, JackpotSummaryParameter.CURRENT,
                            savedSummary.getSlot(), savedSummary.getJackpotSummaryId());
                    summaryParameters.add(currentMaxValue);
                }
            }

            for(MaxJackpotValue maxPaid : report.getMaxValuePayedOutList()){
                if(savedSummary.getSlot().equals(maxPaid.getSlotName())){
                    SummaryParameter maxPaidParameter = getMaxValue(maxPaid, JackpotSummaryParameter.MAX,
                            savedSummary.getSlot(), savedSummary.getJackpotSummaryId());
                    summaryParameters.add(maxPaidParameter);
                }
            }

            for(LastPayedOutJackpot lastPaidOut : report.getLastPayedOutList()){
                if(lastPaidOut.getSlot() != null && lastPaidOut.getSlot().equals(savedSummary.getSlot())){
                    SummaryParameter lastPaidOutParameter = getLastPaidOut(lastPaidOut, savedSummary.getSlot(),
                            savedSummary.getJackpotSummaryId());
                    summaryParameters.add(lastPaidOutParameter);
                }
            }
        }

        parameterRepository.saveParameters(summaryParameters);

        saveJackpotGrowth(report.getGrownValues(), savedSummaries.get(0).getJackpotSummaryId());
    }

    private void saveCommonReport(List<JackpotsMainReport> reports){
        JackpotSummary totalSummary = new JackpotSummary();
        totalSummary.setSlot(SLOTS_SUMMARY);

        int uniqueJackpotsCounter = reports.stream().map(JackpotsMainReport::getUniqueJackpots).reduce(0, Integer::sum);
        totalSummary.setUniqueJackpots(uniqueJackpotsCounter);

        int rtp100Counter = reports.stream().map(JackpotsMainReport::getReach100Rtp).reduce(0, Integer::sum);
        totalSummary.setCounter100Rtp(rtp100Counter);

        int paidOutCounter = 0;
        for(JackpotsMainReport report : reports){
            paidOutCounter = paidOutCounter + report.getPayedOutJackpotsCountList().stream()
                    .map(PaidOutJackpotCounter::getCounter).reduce(0, Integer::sum);
        }
        totalSummary.setPaidOutAmount(paidOutCounter);
        totalSummary.setTimeCheck(new Timestamp(new Date().getTime()));
        JackpotSummary savedSummary = summaryRepository.saveSummary(totalSummary);

        List<SummaryParameter> summaryParameters = new ArrayList<>();

        List<MaxJackpotValue> maxPaidJackpots = performer.getMaxPaidOutJackpots();
        for(MaxJackpotValue maxJackpotValue : maxPaidJackpots){
            SummaryParameter parameter = new SummaryParameter();
            parameter.setJackpotSummaryId(savedSummary.getJackpotSummaryId());
            parameter.setSite(maxJackpotValue.getSite());
            parameter.setBrand(maxJackpotValue.getBrand());
            parameter.setParameterName(JackpotSummaryParameter.MAX.getName());
            parameter.setSlot(maxJackpotValue.getSlotName());
            parameter.setAmount(maxJackpotValue.getMaxValue());
            summaryParameters.add(parameter);
        }

        List<LastPayedOutJackpot> lastPaidJackpots = performer.getPaidOutJackpots();
        Collections.sort(lastPaidJackpots);
        List<LastPayedOutJackpot> last5PaidJackpots = lastPaidJackpots.stream().limit(5).collect(Collectors.toList());
        for(LastPayedOutJackpot lastPayedOutJackpot : last5PaidJackpots){
            SummaryParameter parameter = new SummaryParameter();
            parameter.setJackpotSummaryId(savedSummary.getJackpotSummaryId());
            parameter.setParameterName(JackpotSummaryParameter.LAST.getName());
            parameter.setSite(lastPayedOutJackpot.getSite().getShortUrl());
            parameter.setBrand(lastPayedOutJackpot.getBrand());
            parameter.setSlot(lastPayedOutJackpot.getSlot());
            parameter.setAmount(lastPayedOutJackpot.getValue());
            SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
            try {
                Date lastDate = formatter.parse(lastPayedOutJackpot.getDate());
                parameter.setParameterDate(new java.sql.Date(lastDate.getTime()));
            } catch (ParseException ignored) {
            }
            summaryParameters.add(parameter);
        }

        List<LastPayedOutJackpot> firstJackpotEntries = performer.getFirstJackpotsEntries();
        for(LastPayedOutJackpot lastPayedOut : firstJackpotEntries){
            SummaryParameter parameter = new SummaryParameter();
            parameter.setAmount(lastPayedOut.getValue());
            parameter.setSlot(lastPayedOut.getSlot());
            parameter.setParameterName(JackpotSummaryParameter.NEW.getName());
            parameter.setSite(lastPayedOut.getSite().getShortUrl());
            parameter.setBrand(lastPayedOut.getBrand());
            parameter.setJackpotSummaryId(savedSummary.getJackpotSummaryId());
            SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
            try {
                Date lastDate = formatter.parse(lastPayedOut.getDate());
                parameter.setParameterDate(new java.sql.Date(lastDate.getTime()));
            } catch (ParseException ignored) {
            }
            summaryParameters.add(parameter);
        }

        parameterRepository.saveParameters(summaryParameters);
    }

    private SummaryParameter getMaxValue(MaxJackpotValue maxJackpotValue, JackpotSummaryParameter summaryParameter,
                                         String slot, long summaryId){
        SummaryParameter parameter = new SummaryParameter();
        parameter.setParameterName(summaryParameter.getName());
        parameter.setSlot(slot);
        parameter.setSite(maxJackpotValue.getSite());
        parameter.setBrand(maxJackpotValue.getBrand());
        parameter.setAmount(maxJackpotValue.getMaxValue());
        parameter.setJackpotSummaryId(summaryId);
        return parameter;
    }

    private SummaryParameter getLastPaidOut(LastPayedOutJackpot lastPaidOut, String slot, long summaryId){
        SummaryParameter parameter = new SummaryParameter();
        parameter.setJackpotSummaryId(summaryId);
        parameter.setParameterName(JackpotSummaryParameter.LAST.getName());
        parameter.setSlot(slot);
        parameter.setSite(lastPaidOut.getSite().getShortUrl());
        parameter.setBrand(lastPaidOut.getBrand());
        parameter.setAmount(lastPaidOut.getValue());
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
        try {
            Date lastDate = formatter.parse(lastPaidOut.getDate());
            parameter.setParameterDate(new java.sql.Date(lastDate.getTime()));
        } catch (ParseException ignored) {
        }
        return parameter;
    }

    private void saveJackpotGrowth(List<GrownJackpotResponse> growths, long summaryId){
        List<JackpotGrowth> dbGrowthList = new ArrayList<>();
        for(GrownJackpotResponse grownResponse : growths){
            for(GrownJackpots grownJackpots : grownResponse.getGrownValues()){
                JackpotGrowth dbGrowth = new JackpotGrowth();
                dbGrowth.setJackpotSummaryId(summaryId);
                dbGrowth.setSite(grownJackpots.getSite());
                dbGrowth.setBrand(grownJackpots.getBrand());
                dbGrowth.setGrowthRate(grownJackpots.getGrownValue());
                dbGrowth.setTimePeriod(grownResponse.getGrownPeriod());
                dbGrowthList.add(dbGrowth);
            }
        }
        growthRepository.save(dbGrowthList);
    }
}
