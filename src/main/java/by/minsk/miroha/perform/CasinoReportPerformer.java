package by.minsk.miroha.perform;


import by.minsk.miroha.entities.front.CasinoReportFront;
import by.minsk.miroha.report.CasinoReport;
import by.minsk.miroha.repositories.report.CasinoReportRepository;
import by.minsk.miroha.repositories.report.impl.CasinoReportRepositoryImpl;
import by.minsk.miroha.utils.Converters;


import java.util.ArrayList;
import java.util.List;

public class CasinoReportPerformer {

    private final CasinoReportRepository casinoReportRepository = new CasinoReportRepositoryImpl();

    public List<CasinoReportFront> getReport(int limit, int offset, String sortField,
                                             String sortType, List<String> providers, List<String> slots){
        List<CasinoReport> reports = casinoReportRepository.getCasinoReports(limit, offset, sortField, sortType, providers, slots);
        List<CasinoReportFront> reportFronts = new ArrayList<>();
        for(CasinoReport report : reports){
            CasinoReportFront reportFront = Converters.convertCasinoReport(report);
            reportFronts.add(reportFront);
        }
        return reportFronts;
    }
}
