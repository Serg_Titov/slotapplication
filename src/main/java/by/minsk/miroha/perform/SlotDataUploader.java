package by.minsk.miroha.perform;

import by.minsk.miroha.entities.SlotInput;
import by.minsk.miroha.entities.databases.SlotName;
import by.minsk.miroha.entities.front.report.SlotReportFront;
import by.minsk.miroha.entities.report.CommonSlotInformation;
import by.minsk.miroha.entities.report.DatabaseMultipliers;
import by.minsk.miroha.entities.report.Multipliers;
import by.minsk.miroha.entities.report.SlotInformation;
import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.report.ReportPerformer;
import by.minsk.miroha.report.SlotDBReport;
import by.minsk.miroha.repositories.SlotNameRepository;
import by.minsk.miroha.repositories.impl.SlotNameRepositoryImpl;
import by.minsk.miroha.repositories.report.CommonSlotInfoRepository;
import by.minsk.miroha.repositories.report.DatabaseMultipliersRepository;
import by.minsk.miroha.repositories.report.SlotInformationRepository;
import by.minsk.miroha.repositories.report.SlotPointsRepository;
import by.minsk.miroha.repositories.report.impl.CommonSlotInfoRepositoryImpl;
import by.minsk.miroha.repositories.report.impl.DatabaseMultiPliersRepositoryImpl;
import by.minsk.miroha.repositories.report.impl.SlotInformationRepositoryImpl;
import by.minsk.miroha.repositories.report.impl.SlotPointsRepositoryImpl;
import by.minsk.miroha.utils.CommonUtils;
import by.minsk.miroha.utils.Creator;
import by.minsk.miroha.utils.ReportConverter;

import java.util.List;

/**
 * Класс проверяет адекватность данных в локальной БД. В случае,
 * если данные адекватные, рассчитывает и записывает общие параметры для слота.
 */
public class SlotDataUploader {

    private static final double MAX_CORRECT_RTP = 100D;

    public static final String INCORRECT_DATA = "Загруженная информация не корректна! RTP > 100 !";

    private static final SlotInformationRepository slotInformationRepository = new SlotInformationRepositoryImpl();
    private static final CommonSlotInfoRepository commonSlotInfoRepository = new CommonSlotInfoRepositoryImpl();
    private static final DatabaseMultipliersRepository databaseMultipliersRepository = new DatabaseMultiPliersRepositoryImpl();
    private static final SlotPointsRepository slotPointsRepository = new SlotPointsRepositoryImpl();
    private static final SlotNameRepository nameRepository = new SlotNameRepositoryImpl();

    private String provider;

    private String slotName;

    /**
     * После загрузки информации в локальную БД проверяется адекватность загруженных данных.
     * Если РТП больше 100, данные не адекватны, о чем происходит сообщение пользователя.
     * Если же данные адекватные, то сразу происходит запись информации во внешнюю БД
     * рассчетных параметров по данному слоту. Запускатеся две симуляции со стандартным
     * набором данных, а также просчитываются и записываются точки.
     *
     * @param slot название слота.
     * @param provider название провайдера.
     * @param external true - информация тянется с внешней БД (список всех баз),
     *                 false - информация тянется из загруженной вручную БД.
     */
    public SlotDBReport uploadSlotInformation(String slot, String provider, boolean external){
        this.provider = provider;
        this.slotName = slot;
        SlotInformation slotInformationFromDB = slotInformationRepository.getObject(slot, provider);
        if(slotInformationFromDB == null || !slotInformationFromDB.getSlot().getRawSlot().equals(slot) ||
                !slotInformationFromDB.getProvider().getRawProvider().equals(provider)){
            SlotDBReport report = new SlotDBReport();
            DatabaseStatistics databaseStatistics;
            if (external){
                databaseStatistics = new DatabaseStatistics(provider, slot);
            } else {
                databaseStatistics = new DatabaseStatistics();
            }
            double rtp = databaseStatistics.getRTP();
            if (rtp>MAX_CORRECT_RTP){
                throw new CustomValidation(INCORRECT_DATA);
            }
            SlotInformation slotInformation = saveSlotInformation(slot, provider);
            report.setSlotInformation(slotInformation);
            report.setCommonSlotInformation(calculateCommonSlotInformation(databaseStatistics, slotInformation));
            report.setMultipliers(calculateMultipliers(databaseStatistics, slotInformation));
            SpinnerWriter spinnerWriter;
            if (external){
                spinnerWriter = new SpinnerWriter(provider, slot);
            } else {
                spinnerWriter = new SpinnerWriter();
            }
            spinnerWriter.writeToCSV(external);
            report.setSlotPoints(slotPointsRepository.findByGame(slot, provider));
            startSimulations(external);
            return  report;
        } else {
            return getReportFromDB(slotInformationFromDB);
        }
    }

    public SlotReportFront getReportById(int id){
        SlotInformation slotInformationFromDB = slotInformationRepository.getObjectById(id);
        SlotDBReport slotDBReport = slotInformationFromDB == null ? null : getReportFromDB(slotInformationFromDB);
        return slotDBReport == null ? null : ReportConverter.convertSlotReport(slotDBReport);
    }

    private SlotDBReport getReportFromDB(SlotInformation slotInformation){
        SlotDBReport report = new SlotDBReport();
        report.setCommonSlotInformation(commonSlotInfoRepository.getObject(slotInformation.getIdInfo()));
        report.setMultipliers(databaseMultipliersRepository.findByGame(slotInformation.getIdInfo()));
        report.setSlotPoints(slotPointsRepository.findByGame(slotInformation.getSlot().getCleanSlot(), slotInformation.getProvider().getCleanProvider()));
        slotInformation.setProvider(CommonUtils.formatProvider(slotInformation.getProvider().getCleanProvider()));
        slotInformation.setSlot(CommonUtils.formatSlot(slotInformation.getSlot().getCleanSlot()));
        report.setSlotInformation(slotInformation);
        return report;
    }

    /**
     * Сохраняется во внешнюю БД название слота и провайдер.
     *
     * @param slot название слота.
     * @param provider провайдер.
     * @return сохраненное название слота с id во внешней БД.
     */
    private SlotInformation saveSlotInformation(String slot, String provider) {
        SlotInformation slotInformation = new SlotInformation();
        slotInformation.setSlot(slot);
        slotInformation.setProvider(provider);
        return slotInformationRepository.save(slotInformation);
    }

    /**
     * Рассчитываются общие параметры слота и сохранятются во внешнюю БД.
     *
     * @param statistics объект для расчета данных по слоту.
     * @param slotInformation объект с название слота и id во внешней БД.
     */
    private CommonSlotInformation calculateCommonSlotInformation(DatabaseStatistics statistics, SlotInformation slotInformation){
        CommonSlotInformation commonSlotInformation = new CommonSlotInformation();
        commonSlotInformation.setSpinAmount(statistics.amountAllSpins());
        commonSlotInformation.setMaxWin(statistics.getMaxMultiplier());
        commonSlotInformation.setRtp(statistics.getRTP());
        commonSlotInformation.setVolatility(statistics.getVolatility());
        commonSlotInformation.setBonusFrequency(statistics.getBonusFrequency());
        commonSlotInformation.setBonusWinsRate(statistics.getBonusWinsRate());
        commonSlotInformation.setWinsWithoutBonusRate(100-commonSlotInformation.getBonusWinsRate());
        commonSlotInformation.setAverageBonusWin(statistics.getAverageBonusWin());
        commonSlotInformation.setGameId(slotInformation.getIdInfo());

        return commonSlotInfoRepository.save(commonSlotInformation);
    }

    /**
     * Рассчитывается количество мульплееров в загруженной БД и сохраняется в БД.
     *
     * @param statistics объект для расчета данных по слоту.
     * @param slotInformation объект с название слота и id во внешней БД.
     */
    private List<Multipliers> calculateMultipliers(DatabaseStatistics statistics, SlotInformation slotInformation){
        List<Multipliers> multipliersList = statistics.getFrequencyUpperValue();
        List<Multipliers> multipliersInDB = databaseMultipliersRepository.findByGame(slotInformation.getIdInfo());
        if(multipliersInDB.size() == 0){
            for(Multipliers multiplier : multipliersList){
                DatabaseMultipliers databaseMultiplier = new DatabaseMultipliers();
                databaseMultiplier.setMultiplier(multiplier.getMultiplier());
                databaseMultiplier.setMultiplierFreq(multiplier.getMultiplierCount());
                databaseMultiplier.setGameId(slotInformation.getIdInfo());
                databaseMultipliersRepository.save(databaseMultiplier);
            }
        }
        return databaseMultipliersRepository.findByGame(slotInformation.getIdInfo());
    }

    /**
     * Запускает симуляции для стандартного набора входных данных.
     * Результаты сохраняться в БД.
     */
    private void startSimulations(boolean external){
        String game;
        if (external) {
            SlotName slotName = new SlotName(this.slotName, provider);
            game = slotName.getFullName();
        } else {
            game = nameRepository.getSlotName().getFullName();
        }
        SlotInput slotInputFirstStandard = Creator.createFirstStandardSlotInput(game);
        SlotInput slotInputSecondStandard = Creator.createSecondStandardSlotInput(game);
        ReportPerformer reportPerformer;
        if (external){
            reportPerformer = new ReportPerformer(provider, slotName);
        } else {
            reportPerformer = new ReportPerformer();
        }
        reportPerformer.getReport(slotInputFirstStandard, external);
        if (external){
            reportPerformer = new ReportPerformer(provider, slotName);
        } else {
            reportPerformer = new ReportPerformer();
        }
        reportPerformer.getReport(slotInputSecondStandard, external);
    }
}
