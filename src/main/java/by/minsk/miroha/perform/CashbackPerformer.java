package by.minsk.miroha.perform;

import by.minsk.miroha.exceptions.CustomValidation;

import java.util.ArrayList;
import java.util.List;

public class CashbackPerformer {

    private static final int STANDARD_BET = 1;

    /**
     * Массив случайных коэффициентов (win/bid). используется при
     * расчете профита путем умножения на ставку.
     */
    private List<Double> winCoefficients = new ArrayList<>();

    private final List<Double> results = new ArrayList<>();

    private final UnloaderFromHomeDB unloader;

    public CashbackPerformer() {
        unloader = new UnloaderFromHomeDB(false);
    }

    public CashbackPerformer(String dbName, String tableName) {
        unloader = new UnloaderFromHomeDB(dbName, tableName, false);
    }

    /**
     * Если количество симуляций равно нулю, то дальше не вычисляем, кидаем ошибку.
     * Запускаем цикл на количество симуляций.
     * Считаем одну симуляцию (см. ниже). Если результат больше или равно желаемому
     * выигрышу, то добавляем его в список результатов симуляций. иначе
     * результат умножаем на (1-кэшбэк) и добавляем в список результатов симуляций.
     * Суммируем все результаты и делим на количество симуляций - получаем
     * средний профит
     *
     * @param loss допустимый проигрыш.
     * @param maxProfit желаемый выигрыш.
     * @param simulations количество симуляций.
     * @param cashback кэшбэк.
     * @return средний профит.
     */
    public Double getProfit(Double loss, Double maxProfit, Integer simulations, double cashback){
        results.clear();
        double result;
        if (simulations == 0){
            throw new CustomValidation("Simulation count can't be null");
        }
        for (int i = 0; i < simulations; i++){
            result = getOneSimulation(loss, maxProfit);
            if (result >= maxProfit){
                results.add(result);
            } else {
                results.add(result*(1-cashback));
            }
        }
        return results.stream().mapToDouble(Double::doubleValue).sum()/ simulations;
    }

    /**
     * Запускается цикл, пока профит больше допустимого проигрыша и меньше
     * желаемого выигрыша.
     * 1. Проверятеся, выгружены ли случайные коэффициенты, если нет, то выгружаются.
     * 2. Считается профит: профит = профит - ставка + ставка*случайный_коэффициент (выигрыша).
     * 3. Коэффициент выигрыша, который был использован, удаляется из выгрузки коэффициентов.
     *
     * @param loss допустимый проигрыш.
     * @param maxProfit желаемый выигрыш.
     * @return итоговый профит.
     */
    private Double getOneSimulation(Double loss, Double maxProfit){
        double profit = 0D;
        while ((-1)*loss < profit && profit < maxProfit){
            if (winCoefficients.size() == 0){
                winCoefficients = unloader.getAllCoefficients();
            }
            profit = profit - STANDARD_BET + STANDARD_BET*winCoefficients.get(winCoefficients.size()-1);
            winCoefficients.remove(winCoefficients.size()-1);
        }
        return profit;
    }
}
