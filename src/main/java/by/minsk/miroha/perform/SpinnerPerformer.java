package by.minsk.miroha.perform;

import java.util.List;
import java.util.Random;

public class SpinnerPerformer {

    public static final double WIN_VALUE = 1;
    public static final int PERCENT = 100;
    private static final int REFRESH_VALUE = 20;

    private List<Double> winCoefficients;
    private String dbName;
    private String tableName;

    public SpinnerPerformer() {
    }

    public SpinnerPerformer(String dbName, String tableName) {
        this.dbName = dbName;
        this.tableName = tableName;
    }

    public double getSpinnerResult(int spinLimit, int sim, boolean external){
        UnloaderFromHomeDB unloader;
        if (external){
            unloader = new UnloaderFromHomeDB(dbName, tableName, false);
        } else {
            unloader = new UnloaderFromHomeDB(false);
        }
        winCoefficients = unloader.getAllCoefficients();
        int countWin = 0;
        for (int refreshCounter = 0; refreshCounter < REFRESH_VALUE; refreshCounter++){
            int innerCycleLimit = sim/REFRESH_VALUE;
            if (sim <REFRESH_VALUE){
                innerCycleLimit = sim;
                refreshCounter = REFRESH_VALUE;
            }
            for (int simulationCounter=0; simulationCounter < innerCycleLimit; simulationCounter++){
                if(getOneSpinResult(spinLimit)){
                    countWin++;
                }
            }
            winCoefficients = unloader.getAllCoefficients();
        }
        return (double) countWin/sim*PERCENT;
    }

    private boolean getOneSpinResult(int spinLimit){
        double spinWin = 0;
        for (int spinCounter = 0; spinCounter<spinLimit; spinCounter++){
            Random random = new Random();
            spinWin = spinWin + winCoefficients.get(random.nextInt(winCoefficients.size()));
        }
        return (spinWin / spinLimit) > WIN_VALUE;
    }


}
