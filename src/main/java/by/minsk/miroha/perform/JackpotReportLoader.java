package by.minsk.miroha.perform;

import by.minsk.miroha.entities.common.Site;
import by.minsk.miroha.entities.databases.JackpotGrowth;
import by.minsk.miroha.entities.databases.JackpotSummary;
import by.minsk.miroha.entities.databases.SummaryParameter;
import by.minsk.miroha.entities.enums.TimePeriods;
import by.minsk.miroha.entities.enums.jackpots.JackpotMultipleSlot;
import by.minsk.miroha.entities.enums.jackpots.JackpotSlot;
import by.minsk.miroha.entities.enums.jackpots.JackpotSummaryParameter;
import by.minsk.miroha.entities.front.report.PaidOutJackpotCounter;
import by.minsk.miroha.entities.front.responses.GrownJackpotResponse;
import by.minsk.miroha.entities.front.responses.JackpotSummaryResponse;
import by.minsk.miroha.entities.report.SiteWithLicenses;
import by.minsk.miroha.report.jackpot.*;
import by.minsk.miroha.repositories.jackpots.JackpotGrowthRepository;
import by.minsk.miroha.repositories.jackpots.JackpotSummaryParameterRepository;
import by.minsk.miroha.services.SitesService;
import by.minsk.miroha.services.jackpots.JackpotMainService;
import by.minsk.miroha.services.jackpots.JackpotSummaryService;
import by.minsk.miroha.utils.CommonUtils;
import by.minsk.miroha.utils.LicenseUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Comparator;

import java.util.stream.Collectors;

@Service
public class JackpotReportLoader {

    private static final int FIRST_LIST_ITEM = 1;
    private static final String TOTAL_SUMMARY = "Total";

    private static final String NOT_ACTUAL_DATE = "Внимание! Данная информация актуальна на дату ";

    private final JackpotSummaryService summaryService;

    private final JackpotSummaryParameterRepository parameterRepository;

    private final JackpotMainService jackpotService;

    private final JackpotGrowthRepository growthRepository;

    private final SitesService sitesService;

    public JackpotReportLoader(JackpotSummaryService summaryService,
                               JackpotSummaryParameterRepository parameterRepository,
                               JackpotMainService jackpotService,
                               JackpotGrowthRepository growthRepository,
                               SitesService sitesService) {
        this.summaryService = summaryService;
        this.parameterRepository = parameterRepository;
        this.jackpotService = jackpotService;
        this.growthRepository = growthRepository;
        this.sitesService = sitesService;
    }

    public JackpotsMainReport getReport(JackpotSlot slot){
        JackpotsMainReport report;
        JackpotMultipleSlot multipleSlot = JackpotSlot.getMultipleSlot(slot);
        if(multipleSlot == null){
            report = getSingleReport(slot);
        } else {
            report = getMultipleReport(slot);
        }

        return report;
    }

    public JackpotSummaryResponse getTotalReport(){
        JackpotSummaryResponse response = new JackpotSummaryResponse();
        JackpotSummary summary = summaryService.getLastSummary(TOTAL_SUMMARY);
        response.setUniqueJackpots(summary.getUniqueJackpots());
        response.setPaidOutAmount(summary.getPaidOutAmount());
        response.setCounter100Rtp(summary.getCounter100Rtp());

        List<SummaryParameter> parameters = parameterRepository.getParametersBySummaryId(summary.getJackpotSummaryId());
        response.setNewJackpots(extractNewJackpots(parameters));
        response.setLastPaidOut(extractLastPaidOut(parameters));
        response.setMaxValuePaidOut(extractMaxPaidOut(parameters));

        Date lastAvailableDate = jackpotService.getLastDate(null);
        lastAvailableDate = DateUtils.truncate(lastAvailableDate, Calendar.DATE);
        Date currentDate = DateUtils.truncate(new Date(), Calendar.DATE);
        if(lastAvailableDate.before(currentDate)){
            response.setMessage(NOT_ACTUAL_DATE + new SimpleDateFormat("dd/MM/yyyy").format(lastAvailableDate));
        }

        List<JackpotParameter> topRtpTable = jackpotService.getTopRTPSlots(null, lastAvailableDate);
        List<JackpotParameterLicense> tableWithLicence = setJackpotLicense(topRtpTable);
        Collections.sort(tableWithLicence);
        response.setTopJackpotsRTP(tableWithLicence);
        response.setLicenses(LicenseUtils.getLicensesSet(tableWithLicence));

        return response;
    }

    private JackpotsMainReport getSingleReport(JackpotSlot slot){
        JackpotsMainReport report = new JackpotsMainReport();
        JackpotSummary summary = summaryService.getLastSummary(slot.getSlotName());
        report.setUniqueJackpots(summary.getUniqueJackpots());
        report.setReach100Rtp(summary.getCounter100Rtp());
        List<PaidOutJackpotCounter> paidCounterList = new ArrayList<>();
        paidCounterList.add(new PaidOutJackpotCounter(slot.getSlotName(), summary.getPaidOutAmount()));
        report.setPayedOutJackpotsCountList(paidCounterList);


        List<SummaryParameter> parameters = parameterRepository.getParametersBySummaryId(summary.getJackpotSummaryId());
        report.setMaxValueCurrentList(getMaxJackpotValue(parameters, JackpotSummaryParameter.CURRENT));
        report.setMaxValuePayedOutList(getMaxJackpotValue(parameters, JackpotSummaryParameter.MAX));
        report.setLastPayedOutList(getLastPaidOut(parameters));

        List<JackpotGrowth> growthList = growthRepository.getGrowthBySummaryId(summary.getJackpotSummaryId());
        report.setGrownValues(getGrowthList(growthList, slot.getSlotName()));

        Date lastAvailableDate = jackpotService.getLastDate(slot.getSlotName());
        lastAvailableDate = DateUtils.truncate(lastAvailableDate, Calendar.DATE);
        Date currentDate = DateUtils.truncate(new Date(), Calendar.DATE);
        if(lastAvailableDate.before(currentDate)){
            report.setMessage(NOT_ACTUAL_DATE + new SimpleDateFormat("dd/MM/yyyy").format(lastAvailableDate));
        }

        List<JackpotParameter> topRtpTable = jackpotService.getTopRTPSlots(slot, lastAvailableDate);
        List<JackpotParameterLicense> topRtpTableLicense = setJackpotLicense(topRtpTable);
        Collections.sort(topRtpTableLicense);
        report.setTopJackpotsRTP(topRtpTableLicense);
        report.setLicenses(LicenseUtils.getLicensesSet(topRtpTableLicense));

        List<JackpotSite> allJackpots = jackpotService.getAllJackpotsSites(slot.getSlotName());
        report.setInactiveJackpots(getInactiveJackpots(allJackpots, topRtpTable));

        return report;
    }

    private JackpotsMainReport getMultipleReport(JackpotSlot slot){
        JackpotsMainReport report = new JackpotsMainReport();
        List<JackpotMultipleSlot> multipleSlots = JackpotMultipleSlot.getListSlots(slot);
        List<JackpotSummary> summaries = summaryService.getMultipleLastSummary(multipleSlots);
        if(summaries.size() > 0){
            JackpotSummary firstSummary = summaries.get(FIRST_LIST_ITEM);
            report.setUniqueJackpots(firstSummary.getUniqueJackpots());
            report.setReach100Rtp(firstSummary.getCounter100Rtp());
            List<PaidOutJackpotCounter> paidOutJackpotCounter = new ArrayList<>();
            for(JackpotSummary summary : summaries){
                paidOutJackpotCounter.add(new PaidOutJackpotCounter(summary.getSlot(), summary.getPaidOutAmount()));
            }
            report.setPayedOutJackpotsCountList(paidOutJackpotCounter);
        }

        List<Long> summaryIdList = summaries.stream().map(JackpotSummary::getJackpotSummaryId).collect(Collectors.toList());
        List<SummaryParameter> parameters = parameterRepository.getParametersBySummariesId(summaryIdList);
        report.setMaxValueCurrentList(getMaxJackpotValue(parameters, JackpotSummaryParameter.CURRENT));
        report.setMaxValuePayedOutList(getMaxJackpotValue(parameters, JackpotSummaryParameter.MAX));
        report.setLastPayedOutList(getLastPaidOut(parameters));

        List<JackpotGrowth> growthList = growthRepository.getGrowthBySummaryIdList(summaryIdList);
        report.setGrownValues(getGrowthList(growthList, slot.getSlotName()));

        Date lastAvailableDate = jackpotService.getLastDate(slot.getSlotName());
        lastAvailableDate = DateUtils.truncate(lastAvailableDate, Calendar.DATE);
        Date currentDate = DateUtils.truncate(new Date(), Calendar.DATE);
        if(lastAvailableDate.before(currentDate)){
            report.setMessage(NOT_ACTUAL_DATE + new SimpleDateFormat("dd/MM/yyyy").format(lastAvailableDate));
        }

        List<JackpotParameter> topRtpTable = jackpotService.getTopRTPSlots(slot, lastAvailableDate);
        List<JackpotParameterLicense> topRtpTableLicense = setJackpotLicense(topRtpTable);
        Collections.sort(topRtpTableLicense);
        report.setTopJackpotsRTP(topRtpTableLicense);
        report.setLicenses(LicenseUtils.getLicensesSet(topRtpTableLicense));

        List<JackpotSite> allJackpots = jackpotService.getAllJackpotsSites(slot.getSlotName());
        report.setInactiveJackpots(getInactiveJackpots(allJackpots, topRtpTable));

        return report;
    }

    private List<MaxJackpotValue> getMaxJackpotValue(List<SummaryParameter> parameters, JackpotSummaryParameter type){
        List<MaxJackpotValue> maxJackpotValues = new ArrayList<>();
        for(SummaryParameter parameter : parameters){
            if(parameter.getParameterName().equals(type.getName())){
                MaxJackpotValue maxValue = new MaxJackpotValue();
                maxValue.setSite(parameter.getSite());
                maxValue.setBrand(parameter.getBrand());
                maxValue.setSlotName(parameter.getSlot());
                maxValue.setMaxValue(parameter.getAmount());
                maxJackpotValues.add(maxValue);
            }
        }
        Collections.sort(maxJackpotValues);
        return maxJackpotValues;
    }

    private List<LastPayedOutJackpot> getLastPaidOut(List<SummaryParameter> parameters){
        List<LastPayedOutJackpot> lastPayedOutJackpotList = new ArrayList<>();
        for(SummaryParameter parameter : parameters){
            if(parameter.getParameterName().equals(JackpotSummaryParameter.LAST.getName())){
                LastPayedOutJackpot lastPayedOut = new LastPayedOutJackpot();
                lastPayedOut.setSlot(parameter.getSlot());
                lastPayedOut.setSite(new Site(parameter.getSite()));
                lastPayedOut.setBrand(parameter.getBrand());
                lastPayedOut.setValue(parameter.getAmount());
                SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
                lastPayedOut.setDate(formatter.format(parameter.getParameterDate()));
                lastPayedOutJackpotList.add(lastPayedOut);
            }
        }
        Collections.sort(lastPayedOutJackpotList);
        return lastPayedOutJackpotList;
    }

    private List<GrownJackpotResponse> getGrowthList(List<JackpotGrowth> allGrowthList, String slotName){
        List<GrownJackpotResponse> fullGrowthList = new ArrayList<>();
        for(TimePeriods timePeriod : TimePeriods.values()){
            List<GrownJackpots> growthList = new ArrayList<>();
            for(JackpotGrowth growthParameter : allGrowthList){
                if(growthParameter.getTimePeriod().equals(timePeriod.getName())){
                    GrownJackpots response = new GrownJackpots();
                    response.setSite(growthParameter.getSite());
                    response.setBrand(growthParameter.getBrand());
                    response.setSlotName(slotName);
                    response.setGrownValue(growthParameter.getGrowthRate());
                    growthList.add(response);
                }
            }
            growthList = growthList.stream().sorted(GrownJackpots.COMPARE_BY_VALUE).collect(Collectors.toList());
            GrownJackpotResponse response = new GrownJackpotResponse();
            response.setGrownPeriod(timePeriod.getName());
            response.setGrownValues(growthList);
            fullGrowthList.add(response);
        }
        return fullGrowthList;
    }

    private List<JackpotParameterLicense> setJackpotLicense(List<JackpotParameter> parameters){
        List<String> sites = parameters.stream().map(parameter -> CommonUtils.removeSlash(parameter.getSite().getShortUrl())).collect(Collectors.toList());
        List<SiteWithLicenses> siteWithLicenses = sitesService.getJackpotsWithLicense(sites);
        List<JackpotParameterLicense> parameterLicenseList = new ArrayList<>();
        for(JackpotParameter parameter : parameters){
            JackpotParameterLicense jackpotParameterLicense = new JackpotParameterLicense(parameter);
            for (SiteWithLicenses site : siteWithLicenses){
                if (CommonUtils.removeSlash(parameter.getSite().getShortUrl()).equals(site.getSite())){
                    jackpotParameterLicense.setLicenses(site.getLicenses());
                }
            }
            parameterLicenseList.add(jackpotParameterLicense);
        }
        return parameterLicenseList;
    }

    private List<JackpotSite> getInactiveJackpots(List<JackpotSite> allJackpots, List<JackpotParameter> activeJackpots){
        List<JackpotSite> inactiveJackpots = new ArrayList<>();
        for(JackpotSite jackpotSite : allJackpots){
            boolean jackpotActive = false;
            Site site = new Site(jackpotSite.getSite());
            for(JackpotParameter jackpotParameter : activeJackpots){
                if (site.getShortUrl().equals(jackpotParameter.getSite().getShortUrl())) {
                    jackpotActive = true;
                    break;
                }
            }
            if(!jackpotActive){
                inactiveJackpots.add(jackpotSite);
            }
        }
        return inactiveJackpots;
    }

    private List<SummaryParameter> extractNewJackpots(List<SummaryParameter> parameters){
        return parameters.stream().filter(p -> p.getParameterName().equals(JackpotSummaryParameter.NEW.getName()))
                .sorted(Comparator.comparing(SummaryParameter::getParameterDate))
                .collect(Collectors.toList());
    }

    private List<SummaryParameter> extractLastPaidOut(List<SummaryParameter> parameters){
        return parameters.stream().filter(p -> p.getParameterName().equals(JackpotSummaryParameter.LAST.getName()))
                .sorted(Comparator.comparing(SummaryParameter::getParameterDate))
                .collect(Collectors.toList());
    }

    private List<SummaryParameter> extractMaxPaidOut(List<SummaryParameter> parameters){
        return parameters.stream().filter(p -> p.getParameterName().equals(JackpotSummaryParameter.MAX.getName()))
                .sorted(Comparator.comparing(SummaryParameter::getAmount))
                .collect(Collectors.toList());
    }

}
