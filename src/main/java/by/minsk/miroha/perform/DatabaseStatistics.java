package by.minsk.miroha.perform;

import java.util.ArrayList;
import java.util.List;

import by.minsk.miroha.entities.enums.MultiplierValue;
import by.minsk.miroha.entities.report.Multipliers;
import by.minsk.miroha.services.SlotInfoService;
import by.minsk.miroha.services.impl.SlotInfoExternalServiceImpl;
import by.minsk.miroha.services.impl.SlotInfoHomeServiceImpl;

/**
 * Класс предоставляющий информацию по показателям из БД.
 * Для одного и того же слота, показатели одинаковы при каждом запросе.
 */
public class DatabaseStatistics {

    private int allSpins;

    private final SlotInfoService slotInfoService;

    public DatabaseStatistics() {
        slotInfoService = new SlotInfoHomeServiceImpl();
    }

    public DatabaseStatistics(String dbName, String tableName) {
        slotInfoService = new SlotInfoExternalServiceImpl(dbName, tableName);
    }

    /**
     * Возвращается количетсво строк в  БД.
     *
     * @return количество строк данных в БД.
     */
    public int amountAllSpins(){
        allSpins = slotInfoService.countAll();
        return allSpins;
    }

    /**
     * Возвращается максимальный выигрышный коэффициент в БД.
     *
     * @return максимальный выигрышный коэффициент в БД.
     */
    public double getMaxMultiplier(){
        return slotInfoService.getMaxWin();
    }

    /**
     * Считается количество выигрышных коэффициентов в БД выше или равных некоторого значение.
     * (Эти значения задаются в {@link MultiplierValue}.
     * Если такие коэффициенты найдены, то в список помещается параметр:
     * количество записей в БД делить на количество выигрышных коэффициентов выше некоторого
     * значения. Если коэффициенты не найдены, то помещается 0.
     *
     * @return список частот появления в БД выигрышных коэффициентов выше некоторого значения.
     */
    public List<Multipliers> getFrequencyUpperValue(){
        List<Multipliers> frequencyList = new ArrayList<>();
        int allSpins;
        if (this.allSpins > 0){
            allSpins = this.allSpins;
        }
        else {
            allSpins = amountAllSpins();
        }
        for (MultiplierValue multiplier : MultiplierValue.values()){
            int frequency;
            int amountMultipliers = slotInfoService.getAmountCoefficientUpperValue(multiplier.getValue());
            if (amountMultipliers > 0) {
                frequency = allSpins / amountMultipliers;
                frequencyList.add(new Multipliers(multiplier.getValue(), frequency));
            }
        }
        return frequencyList;
    }

    /**
     * Считается РТП слота. Суммируются все win в БД и делятся на сумму всех bid в БД.
     * Если данных нет, то вернется 0.
     *
     * @return ртп слота.
     */
    public Double getRTP(){
        return slotInfoService.getAverageMultiplier();
    }

    /**
     * Считается дисперсия слота. Считается среднее значение выигрышного коэффициента в БД.
     * От каждого значения выигрышного коэффициента в БД вычитается среднее значение.
     * Все это суммируется и делится на количество выигрышных коэффициентов в БД.
     *
     * @return дисперсия выигрышных коэффициентов.
     */
    public Double getVolatility(){
        List<Double> allCoefficients = slotInfoService.getAllCoefficients();
        if (allCoefficients != null && !allCoefficients.isEmpty()) {
            double average = allCoefficients.stream().mapToDouble(Double::doubleValue).average().orElse(0D);
            return allCoefficients.stream().mapToDouble(i -> (i-average)*(i-average)).sum()/ allCoefficients.size();
        }
        else {
            return 0D;
        }
    }

    /**
     * Делим количество всех строк в БД на количество строк,
     * в которых бонус = 1.
     *
     * @return частота выпадения бонуса.
     */
    public Double getBonusFrequency(){
        double bonusCount = slotInfoService.getBonusCount();
        return bonusCount == 0 ? 0 : allSpins/bonusCount;
    }

    /**
     * Считает долю выигрышей с бонусами среди всех выигрышей.
     *
     * @return доля выигрышей с бонусами среди всех выигрышей.
     */
    public double getBonusWinsRate(){
        double allWins = slotInfoService.getSumAllWins();
        double bonusWins = slotInfoService.getWinsWithBonus();
        return bonusWins/allWins*100;
    }

    public double getAverageBonusWin(){
        return slotInfoService.getAverageBonusWin();
    }
}
