package by.minsk.miroha.perform;

import by.minsk.miroha.entities.SlotInput;
import by.minsk.miroha.entities.databases.SlotName;
import by.minsk.miroha.entities.enums.ExactEVSimulations;
import by.minsk.miroha.entities.report.ExactEV;

import java.util.ArrayList;
import java.util.List;

public class ExactEVPerformer {

    private static final int LOOP_LIMIT = 5;

    private SlotName slotName;

    public ExactEVPerformer() {
    }

    public ExactEVPerformer(String provider, String game) {
        this.slotName = new SlotName(game, provider);
    }

    public ExactEV getExactEV(SlotInput slotInput, boolean external, double accuracy){
        ExactEV exactEV = null;
        Performer performer;
        if (external){
            performer = new Performer(slotName.getProvider(), slotName.getSlotName());
        } else {
            performer = new Performer();
        }
        List<Double> profits = new ArrayList<>();
        double errorLimit = (100 - accuracy)/100;
        for (ExactEVSimulations evSimulations : ExactEVSimulations.values()) {
            profits.clear();
            slotInput.setSim(evSimulations.getSimulations());
            for (int i = 0; i < LOOP_LIMIT; i++) {
                profits.add(performer.getProfit(slotInput, external)-slotInput.getDeposit());
                performer.clearInformation();
            }
            double averageProfit = profits.stream().reduce(0D, Double::sum) / profits.size();
            long needMoreSim = profits.stream().filter(profit -> Math.abs((profit / averageProfit - 1)) > errorLimit).count();
            if (needMoreSim == 0){
                exactEV = new ExactEV();
                exactEV.setSimulations(evSimulations.getSimulations());
                exactEV.setEv(averageProfit);
                exactEV.setAccuracy(accuracy);
                break;
            }
        }
        return exactEV;
    }
}
