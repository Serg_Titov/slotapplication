package by.minsk.miroha.perform;

import by.minsk.miroha.entities.common.Site;
import by.minsk.miroha.entities.enums.jackpots.JackpotMultipleSlot;
import by.minsk.miroha.entities.enums.jackpots.JackpotSlot;
import by.minsk.miroha.entities.enums.jackpots.JackpotStatus;
import by.minsk.miroha.entities.enums.TimePeriods;
import by.minsk.miroha.entities.front.responses.GrownSiteResponse;
import by.minsk.miroha.report.jackpot.JackpotParameter;
import by.minsk.miroha.report.jackpot.JackpotSiteReport;
import by.minsk.miroha.report.jackpot.LastPayedOutJackpot;
import by.minsk.miroha.report.jackpot.MaxJackpotValue;
import by.minsk.miroha.services.jackpots.JackpotSiteService;
import by.minsk.miroha.services.jackpots.impl.JackpotSiteServiceImpl;
import org.apache.commons.lang3.time.DateUtils;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

public class JackpotSitePerformer {

    private static final long DAYS_IN_MILLIS = 24*60*60*1000;

    private final JackpotSiteService service = new JackpotSiteServiceImpl();

    private final List<MaxJackpotValue> paidOutJackpots = new ArrayList<>();

    public JackpotSiteReport getReport(JackpotSlot slot, String site){
        JackpotMultipleSlot multipleSlots = JackpotSlot.getMultipleSlot(slot);
        JackpotSiteReport report;
        if(multipleSlots == null){
            report = getSingleReport(slot, site);
        } else {
            report = getMultipleReport(slot, site);
        }
        return report;
    }

    private JackpotSiteReport getSingleReport(JackpotSlot slot, String site){
        JackpotSiteReport report = new JackpotSiteReport();
        report.setSlotName(slot.getSlotName());
        report.setSite(new Site(site));

        List<MaxJackpotValue> currentValues = new ArrayList<>();
        MaxJackpotValue currentValue = new MaxJackpotValue();
        currentValue.setSlotName(slot.getSlotName());
        currentValue.setSite(site);
        currentValue.setMaxValue(service.getCurrentJackpot(slot.getSlotName(), site));
        currentValues.add(currentValue);
        report.setCurrentJackpotList(currentValues);

        List<MaxJackpotValue> maxPaidOutList = new ArrayList<>();
        MaxJackpotValue maxPaidOut = new MaxJackpotValue();
        maxPaidOut.setSlotName(slot.getSlotName());
        maxPaidOut.setSite(site);
        maxPaidOut.setMaxValue(service.getMaxPayedOutJackpot(slot.getSlotName(), site));
        maxPaidOutList.add(maxPaidOut);
        report.setMaxPaidOutJackpotList(maxPaidOutList);

        List<MaxJackpotValue> minPaidOutList = new ArrayList<>();
        MaxJackpotValue minPaidOut = new MaxJackpotValue();
        minPaidOut.setSlotName(slot.getSlotName());
        minPaidOut.setSite(site);
        minPaidOut.setMaxValue(service.getMinPayedOutJackpot(slot.getSlotName(), site));
        minPaidOutList.add(minPaidOut);
        report.setMinPaidOutJackpotList(minPaidOutList);

        List<JackpotParameter> parameters = service.getFullInformation(slot.getSlotName(), site);
        report.setStatus(getSiteStatus(parameters));

        List<LastPayedOutJackpot> lastPayedOutList = new ArrayList<>();
        lastPayedOutList.add(getLastPayedOutJackpot(parameters, slot.getSlotName(), site, false));
        report.setLastPaidOutJackpot(lastPayedOutList);

        List<GrownSiteResponse> responseList = new ArrayList<>();
        for(TimePeriods timePeriod : TimePeriods.values()) {
            GrownSiteResponse response = new GrownSiteResponse();
            response.setTimePeriod(timePeriod.name());
            Double grownValue = getGrownJackpots(timePeriod, parameters);
            response.setGrownValue(String.format("%.0f", grownValue));
            responseList.add(response);
        }
        report.setGrownValues(responseList);

        return report;
    }

    private JackpotSiteReport getMultipleReport(JackpotSlot slot, String site){
        JackpotSiteReport report = new JackpotSiteReport();
        report.setSite(new Site(site));
        report.setSlotName(slot.getFrontName());

        List<JackpotMultipleSlot> multipleSlotList = JackpotMultipleSlot.getListSlots(slot);

        List<MaxJackpotValue> currentValues = new ArrayList<>();
        for(JackpotMultipleSlot multipleSlot : multipleSlotList){
            MaxJackpotValue currentValue = new MaxJackpotValue();
            currentValue.setSlotName(multipleSlot.getSlotName());
            currentValue.setSite(site);
            currentValue.setMaxValue(service.getCurrentJackpot(multipleSlot.getSlotName(), site));
            currentValues.add(currentValue);
        }
        report.setCurrentJackpotList(currentValues);

        Map<JackpotMultipleSlot, List<JackpotParameter>> parameters = new HashMap<>();
        for(JackpotMultipleSlot multipleSlot : multipleSlotList){
            parameters.put(multipleSlot, service.getFullInformation(multipleSlot.getSlotName(), site));
        }

        for(JackpotMultipleSlot multipleSlot : multipleSlotList){
            report.setStatus(getSiteStatus(parameters.get(multipleSlot)));
            break;
        }

        List<MaxJackpotValue> maxPaidOutList = new ArrayList<>();
        List<MaxJackpotValue> minPaidOutList = new ArrayList<>();
        List<LastPayedOutJackpot> lastPayedOutList = new ArrayList<>();
        for(JackpotMultipleSlot multipleSlot : multipleSlotList){
            paidOutJackpots.clear();
            lastPayedOutList.add(getLastPayedOutJackpot(parameters.get(multipleSlot), multipleSlot.getSlotName(), site, true));
            if(paidOutJackpots.size() > 0) {
                Collections.sort(paidOutJackpots);
                maxPaidOutList.add(paidOutJackpots.get(0));
                minPaidOutList.add(paidOutJackpots.get(paidOutJackpots.size()-1));
            }
        }
        report.setLastPaidOutJackpot(lastPayedOutList);
        report.setMaxPaidOutJackpotList(maxPaidOutList);
        report.setMinPaidOutJackpotList(minPaidOutList);

        List<GrownSiteResponse> responseList = new LinkedList<>();
        for(TimePeriods timePeriod : TimePeriods.values()) {
            for(JackpotMultipleSlot multipleSlot : multipleSlotList) {
                GrownSiteResponse response = new GrownSiteResponse();
                response.setSlotName(multipleSlot.getSlotName());
                response.setTimePeriod(timePeriod.name());
                Double grownValue = getGrownJackpots(timePeriod, parameters.get(multipleSlot));
                response.setGrownValue(String.format("%.0f", grownValue));
                responseList.add(response);
            }
        }
        report.setGrownValues(responseList);

        return  report;
    }

    private LastPayedOutJackpot getLastPayedOutJackpot(List<JackpotParameter> parameters, String slotName, String site, boolean multiple){
        LastPayedOutJackpot lastPaid = new LastPayedOutJackpot();
        lastPaid.setSlot(slotName);
        lastPaid.setSite(new Site(site));
        long maxPayedOutTime = 0;
        int lastPayedOutIndex = -1;
        for(int i = 0; i < parameters.size() - 1; i++) {
            JackpotParameter currentParameter = parameters.get(i);
            JackpotParameter nextParameter = parameters.get(i + 1);
            if(currentParameter.getValueFirst() < nextParameter.getValueFirst()){
                if((nextParameter.getValueFirst() - currentParameter.getValueFirst())/currentParameter.getValueFirst() > 0.6){
                    paidOutJackpots.add(createPaidOutJackpot(currentParameter, slotName));
                    if(currentParameter.getDate().getTime()>maxPayedOutTime){
                        lastPayedOutIndex = i;
                        maxPayedOutTime = currentParameter.getDate().getTime();
                    }
                }
            }
        }
        if(lastPayedOutIndex > 0){
            if(multiple){
                if(lastPayedOutIndex < parameters.size() - 2) {
                    lastPaid.setValue(Math.round(parameters.get(lastPayedOutIndex + 1).getValueFirst()));
                } else {
                    lastPaid.setValue(Math.round(parameters.get(lastPayedOutIndex).getValueFirst()));
                }
            } else {
                lastPaid.setValue(Math.round(parameters.get(lastPayedOutIndex).getValueSecond()));
            }
            Date firstDate = new Date(parameters.get(lastPayedOutIndex).getDate().getTime());
            SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
            lastPaid.setDate(formatter.format(firstDate));
        } else {
            if(multiple){
                lastPaid.setValue(Math.round(parameters.get(parameters.size() - 1).getValueFirst()));
            } else {
                lastPaid.setValue(Math.round(parameters.get(parameters.size() - 1).getValueSecond()));
            }
            Date firstDate = new Date(parameters.get(parameters.size()-1).getDate().getTime());
            SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
            lastPaid.setDate(formatter.format(firstDate));
        }
        return lastPaid;

    }

    private String getSiteStatus(List<JackpotParameter> parameters){
        Date targetDate = DateUtils.addDays(new Date(), -92);
        double startJackpot = -1;
        boolean statusSign = true;
        for (JackpotParameter currentParameter : parameters) {
            Date parameterDate = new Date(currentParameter.getDate().getTime());
            if(parameterDate.after(targetDate)) {
                if (startJackpot == -1) {
                    startJackpot = currentParameter.getValueFirst();
                }
                if (currentParameter.getValueFirst() != startJackpot) {
                    statusSign = false;
                    break;
                }
            }
        }
        if (statusSign){
            return JackpotStatus.UNAVAILABLE.getStatus();
        } else {
            return JackpotStatus.AVAILABLE.getStatus();
        }
    }

    private double getGrownJackpots(TimePeriods timePeriod, List<JackpotParameter> parameters){
        if(parameters != null && parameters.size() > 0){
            double grownValue = 0;
            JackpotParameter currentParameter;
            JackpotParameter previousParameter = parameters.get(0);
            Timestamp borderTime = new Timestamp(System.currentTimeMillis() - timePeriod.getDays()* DAYS_IN_MILLIS);
            for(int i = 1; i< parameters.size(); i++){
                currentParameter = parameters.get(i);
                if(currentParameter.getDate().before(borderTime)){
                    break;
                }
                if(previousParameter.getValueFirst() > currentParameter.getValueFirst()) {
                    grownValue = grownValue + previousParameter.getValueFirst() - currentParameter.getValueFirst();
                } else if((currentParameter.getValueFirst() - previousParameter.getValueFirst())/previousParameter.getValueFirst() > 0.6 &&
                            previousParameter.getValueSecond() > 0){
                    grownValue = grownValue + previousParameter.getValueSecond() - currentParameter.getValueFirst();
                }
                previousParameter = currentParameter;
            }
            return grownValue;
        } else {
            return 0;
        }
    }

    private MaxJackpotValue createPaidOutJackpot(JackpotParameter parameter, String slotName){
        MaxJackpotValue maxJackpotValue = new MaxJackpotValue();
        maxJackpotValue.setSlotName(slotName);
        maxJackpotValue.setMaxValue(parameter.getValueFirst());
        return maxJackpotValue;
    }
}
