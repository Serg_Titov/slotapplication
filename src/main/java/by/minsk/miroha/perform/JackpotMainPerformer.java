package by.minsk.miroha.perform;

import by.minsk.miroha.entities.JackpotMultipleValue;
import by.minsk.miroha.entities.enums.jackpots.JackpotMultipleSlot;
import by.minsk.miroha.entities.enums.jackpots.JackpotSlot;
import by.minsk.miroha.entities.enums.jackpots.JackpotStatus;
import by.minsk.miroha.entities.enums.TimePeriods;
import by.minsk.miroha.entities.front.report.PaidOutJackpotCounter;
import by.minsk.miroha.entities.front.responses.GrownJackpotResponse;
import by.minsk.miroha.entities.report.SiteWithLicenses;
import by.minsk.miroha.report.jackpot.*;
import by.minsk.miroha.services.jackpots.JackpotMainService;
import by.minsk.miroha.services.SitesService;
import by.minsk.miroha.utils.CommonUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.stream.Collectors;
import java.util.Collections;

@Service
public class JackpotMainPerformer {

    private static final String NOT_ACTUAL_DATE = "Внимание! Данная информация актуальна на дату ";

    private final JackpotMainService jackpotService;

    private final SitesService sitesService;

    public JackpotMainPerformer(JackpotMainService jackpotService, SitesService sitesService) {
        this.jackpotService = jackpotService;
        this.sitesService = sitesService;
    }

    private final List<PaidOutJackpotCounter> payedOutJackpotCounterList = new ArrayList<>();
    private final List<LastPayedOutJackpot> paidOutJackpots = new ArrayList<>();
    private static final long DAYS_IN_MILLIS = 24*60*60*1000;

    public JackpotsMainReport getReport(JackpotSlot jackpotSlot){
        payedOutJackpotCounterList.clear();
        JackpotsMainReport report = new JackpotsMainReport();
        Date lastAvailableDate = jackpotService.getLastDate(jackpotSlot.getSlotName());
        lastAvailableDate = DateUtils.truncate(lastAvailableDate, Calendar.DATE);
        Date currentDate = DateUtils.truncate(new Date(), Calendar.DATE);
        if(lastAvailableDate.before(currentDate)){
            report.setMessage(NOT_ACTUAL_DATE + new SimpleDateFormat("dd/MM/yyyy").format(lastAvailableDate));
        }
        report.setUniqueJackpots(jackpotService.countUniqueJackpots(jackpotSlot.getSlotName()));

        List<MaxJackpotValue> maxJackpotValuesList = new ArrayList<>();
        maxJackpotValuesList.add(jackpotService.getMaxCurrentJackpotValue(jackpotSlot.getSlotName(), lastAvailableDate));
        report.setMaxValueCurrentList(maxJackpotValuesList);

        List<MaxJackpotValue> maxPaidOutValueList = new ArrayList<>();
        maxPaidOutValueList.add(jackpotService.getMaxPaidOutValue(jackpotSlot.getSlotName()));
        report.setMaxValuePayedOutList(maxPaidOutValueList);

        List<JackpotParameter> parameters = jackpotService.getLastPaidOut(jackpotSlot.getSlotName());
        parameters.forEach(parameter -> parameter.setRtp(JackpotSlot.calculateRtp(jackpotSlot, parameter.getValueFirst())));


        List<LastPayedOutJackpot> lastPayedOutJackpotList = new ArrayList<>();
        lastPayedOutJackpotList.add(getLastPayedOutJackpot(parameters, jackpotSlot.getSlotName(), false));
        report.setLastPayedOutList(lastPayedOutJackpotList);

        List<PaidOutJackpotCounter> paidOutJackpotCounters = new ArrayList<>(payedOutJackpotCounterList);
        report.setPayedOutJackpotsCountList(paidOutJackpotCounters);

        report.setReach100Rtp(reach100RtpTable(parameters));

        List<GrownJackpotResponse> responseList = new ArrayList<>();
        for(TimePeriods timePeriod : TimePeriods.values()){
            List<GrownJackpots> grownJackpots = getGrownJackpots(timePeriod, parameters, jackpotSlot.getSlotName());
            grownJackpots = grownJackpots.stream().sorted(GrownJackpots.COMPARE_BY_VALUE).limit(5).collect(Collectors.toList());
            GrownJackpotResponse response = new GrownJackpotResponse();
            response.setGrownPeriod(timePeriod.getName());
            response.setGrownValues(grownJackpots);
            responseList.add(response);
        }
        report.setGrownValues(responseList);
        parameters.clear();
        return report;
    }

    public JackpotsMainReport getMultipleReport(JackpotSlot slot){
        payedOutJackpotCounterList.clear();
        List<JackpotMultipleSlot> multipleSlots = JackpotMultipleSlot.getListSlots(slot);
        JackpotsMainReport report = new JackpotsMainReport();

        Date lastAvailableDate = jackpotService.getLastDate(slot.getSlotName());
        lastAvailableDate = DateUtils.truncate(lastAvailableDate, Calendar.DATE);
        Date currentDate = DateUtils.truncate(new Date(), Calendar.DATE);
        if(lastAvailableDate.before(currentDate)){
            report.setMessage(NOT_ACTUAL_DATE + new SimpleDateFormat("dd/MM/yyyy").format(lastAvailableDate));
        }
        report.setUniqueJackpots(jackpotService.countUniqueJackpots(slot.getSlotName()));

        List<MaxJackpotValue> maxJackpotValuesList = new ArrayList<>();
        for(JackpotMultipleSlot multipleSlot : multipleSlots) {
            maxJackpotValuesList.add(jackpotService.getMaxCurrentJackpotValue(multipleSlot.getSlotName(), lastAvailableDate));
        }
        report.setMaxValueCurrentList(maxJackpotValuesList);

        Map<JackpotMultipleSlot, List<JackpotParameter>> jackpotParameters = new HashMap<>();
        for(JackpotMultipleSlot multipleSlot : multipleSlots) {
            jackpotParameters.put(multipleSlot, jackpotService.getLastPaidOut(multipleSlot.getSlotName()));
        }
        setMultipleRtp(jackpotParameters, multipleSlots);

        report.setReach100Rtp(reach100RtpTable(jackpotParameters.get(multipleSlots.get(0))));

        List<LastPayedOutJackpot> lastPayedOutJackpotList = new ArrayList<>();
        List<MaxJackpotValue> maxPaidOutValueList = new ArrayList<>();
        for (JackpotMultipleSlot multipleSlot : multipleSlots){
            lastPayedOutJackpotList.add(getLastPayedOutJackpot(jackpotParameters.get(multipleSlot), multipleSlot.getSlotName(), true));
            MaxJackpotValue maxJackpotValue = getMaxPaidOutJackpot(multipleSlot.getSlotName());
            if(maxJackpotValue != null) {
                maxPaidOutValueList.add(maxJackpotValue);
            }
        }
        report.setLastPayedOutList(lastPayedOutJackpotList);
        report.setPayedOutJackpotsCountList(payedOutJackpotCounterList);
        report.setMaxValuePayedOutList(maxPaidOutValueList);

        List<GrownJackpotResponse> grownJackpot = getMultipleGrownValues(jackpotParameters);
        report.setGrownValues(grownJackpot);

        return report;
    }

    public List<LastPayedOutJackpot> getFirstJackpotsEntries(){
        List<LastPayedOutJackpot> lastPayedOutJackpotList = new ArrayList<>();
        List<JackpotParameter> jackpotParameters = jackpotService.getFirstJackpotEntries();
        for(JackpotParameter parameter : jackpotParameters){
            LastPayedOutJackpot lastPayedOut = new LastPayedOutJackpot();
            lastPayedOut.setBrand(parameter.getBrand());
            lastPayedOut.setSite(parameter.getSite());
            lastPayedOut.setSlot(parameter.getSlotName());
            lastPayedOut.setValue(parameter.getValueFirst());
            Date firstDate = new Date(parameter.getDate().getTime());
            SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
            lastPayedOut.setDate(formatter.format(firstDate));
            lastPayedOutJackpotList.add(lastPayedOut);
        }
        return lastPayedOutJackpotList;
    }

    public List<MaxJackpotValue> getMaxPaidOutJackpots(){
        List<JackpotParameter> maxParametersList = jackpotService.getMaxPaidOutJackpots();
        List<MaxJackpotValue> maxJackpotValueList = new ArrayList<>();
        for(JackpotParameter parameter : maxParametersList){
            MaxJackpotValue maxValue = new MaxJackpotValue();
            maxValue.setSlotName(parameter.getSlotName());
            maxValue.setBrand(parameter.getBrand());
            maxValue.setSite(parameter.getSite().getShortUrl());
            maxValue.setMaxValue(parameter.getValueFirst());
            maxJackpotValueList.add(maxValue);
        }
        return maxJackpotValueList;
    }

    public List<LastPayedOutJackpot> getPaidOutJackpots(){
        return paidOutJackpots;
    }

    private LastPayedOutJackpot getLastPayedOutJackpot(List<JackpotParameter> parameters, String slotName, boolean multiple){
        PaidOutJackpotCounter payedOutJackpotCounter = new PaidOutJackpotCounter();
        payedOutJackpotCounter.setSlotName(slotName);
        long maxPayedOutTime = 0;
        int lastPayedOutIndex = -1;
        for(int i = 0; i < parameters.size() - 1; i++){
            JackpotParameter currentParameter = parameters.get(i);
            JackpotParameter nextParameter = parameters.get(i+1);
            if(currentParameter.getSite().equals(nextParameter.getSite()) &&
                    currentParameter.getValueFirst() < nextParameter.getValueFirst()){
                if(multiple){
                    if(currentParameter.getValueFirst() > 0 &&
                            ((nextParameter.getValueFirst() - currentParameter.getValueFirst())/ currentParameter.getValueFirst()) > 0.7){
                        payedOutJackpotCounter.inc();
                        paidOutJackpots.add(extractPaidOutJackpot(nextParameter, slotName, true));
                        if (currentParameter.getDate().getTime() > maxPayedOutTime) {
                            lastPayedOutIndex = i;
                            maxPayedOutTime = currentParameter.getDate().getTime();
                        }
                    }
                }else {
                    if (currentParameter.getValueSecond() > 0 &&
                            (nextParameter.getValueFirst() - currentParameter.getValueFirst()) / currentParameter.getValueSecond() > 0.6) {
                        payedOutJackpotCounter.inc();
                        paidOutJackpots.add(extractPaidOutJackpot(currentParameter, slotName, false));
                        if (currentParameter.getDate().getTime() > maxPayedOutTime) {
                            lastPayedOutIndex = i;
                            maxPayedOutTime = currentParameter.getDate().getTime();
                        }
                    }
                }

            }
        }
        LastPayedOutJackpot parameter = new LastPayedOutJackpot();
        if(lastPayedOutIndex > -1) {
            JackpotParameter payedOutParameter = parameters.get(lastPayedOutIndex);
            parameter.setSite(payedOutParameter.getSite());
            parameter.setBrand(payedOutParameter.getBrand());
            if(multiple){
                parameter.setValue(payedOutParameter.getValueFirst());
            } else {
                parameter.setValue(payedOutParameter.getValueSecond());
            }
            parameter.setSlot(slotName);
            Date payedOutDate = new Date(payedOutParameter.getDate().getTime());
            SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
            parameter.setDate(formatter.format(payedOutDate));
        }
        payedOutJackpotCounterList.add(payedOutJackpotCounter);
        return parameter;
    }

    private int reach100RtpTable(List<JackpotParameter> allInfo){
        int counter100Rtp = 0;
        boolean reach100Rtp = false;
        for(int i = 1; i < allInfo.size(); i++){
            JackpotParameter currentParameter = allInfo.get(i);
            if(currentParameter.getSite().equals(allInfo.get(i-1).getSite())){
                if(reach100Rtp){
                    if(currentParameter.getRtp() < 100) {
                        reach100Rtp = false;
                    }
                } else {
                    if(currentParameter.getRtp() >= 100) {
                        reach100Rtp = true;
                        counter100Rtp++;
                    }
                }
            } else {
                reach100Rtp = false;
            }
        }
        return counter100Rtp;
    }

    private void setJackpotStatus(List<JackpotParameter> rtpTable, List<JackpotParameter> allInfo){
        Date targetDate = DateUtils.addDays(new Date(), -92);
        double startJackpot = -1;
        String currentSite = "";
        boolean statusSign = true;
        for(int i = 1; i < allInfo.size(); i++){
            JackpotParameter currentParameter = allInfo.get(i);
            Date parameterDate = new Date(currentParameter.getDate().getTime());
            if(parameterDate.after(targetDate)){
                if (startJackpot == -1){
                    startJackpot = currentParameter.getValueFirst();
                    currentSite = currentParameter.getSite().getFullUrl();
                }
                if(currentParameter.getSite().getFullUrl().equals(currentSite)){
                    if(currentParameter.getValueFirst() != startJackpot){
                        statusSign = false;
                    }
                } else {
                    for(JackpotParameter siteRtp: rtpTable){
                        if(siteRtp.getSite().getFullUrl().equals(currentSite)){
                            if(statusSign){
                                siteRtp.setStatus(JackpotStatus.UNAVAILABLE.getStatus());
                            } else {
                                siteRtp.setStatus(JackpotStatus.AVAILABLE.getStatus());
                            }
                            siteRtp.setSite(siteRtp.getSite());
                        }
                    }
                    currentSite = currentParameter.getSite().getFullUrl();
                    startJackpot = currentParameter.getValueFirst();
                    statusSign = true;
                }
            }
        }
        for(JackpotParameter siteRtp: rtpTable){
            if(siteRtp.getSite().getFullUrl().equals(currentSite)){
                if(statusSign){
                    siteRtp.setStatus(JackpotStatus.UNAVAILABLE.getStatus());
                } else {
                    siteRtp.setStatus(JackpotStatus.AVAILABLE.getStatus());
                }
                siteRtp.setSite(siteRtp.getSite());
            }
        }
    }

    private List<GrownJackpots> getGrownJackpots(TimePeriods timePeriods, List<JackpotParameter> parameters, String slotName){
        List<GrownJackpots> grownJackpotsList = new ArrayList<>();
        if(parameters != null && parameters.size() > 0) {
            JackpotParameter previousParameter = parameters.get(0);
            String currentSite = previousParameter.getSite().getFullUrl();
            Timestamp borderTime = new Timestamp(System.currentTimeMillis()-timePeriods.getDays()*DAYS_IN_MILLIS);
            double grownValue = 0;
            for (int i = 1; i < parameters.size(); i++){
                JackpotParameter currentParameter = parameters.get(i);
                if (currentSite.equals(currentParameter.getSite().getFullUrl())) {
                    if (currentParameter.getDate().after(borderTime)) {
                        if (previousParameter.getValueFirst() > currentParameter.getValueFirst()) {
                            grownValue = grownValue + previousParameter.getValueFirst() - currentParameter.getValueFirst();
                        } else if ((currentParameter.getValueFirst() - previousParameter.getValueFirst()) / previousParameter.getValueFirst() > 0.6) {
                            grownValue = grownValue + previousParameter.getValueSecond() - currentParameter.getValueFirst();
                        }
                    }
                } else {
                    GrownJackpots grownJackpot = new GrownJackpots();
                    grownJackpot.setSlotName(slotName);
                    grownJackpot.setBrand(previousParameter.getBrand());
                    grownJackpot.setSite(previousParameter.getSite().getShortUrl());
                    grownJackpot.setGrownValue(grownValue);
                    grownJackpotsList.add(grownJackpot);
                    currentSite = currentParameter.getSite().getFullUrl();
                    grownValue = 0;
                }
                previousParameter = currentParameter;
            }
            GrownJackpots grownJackpot = new GrownJackpots();
            grownJackpot.setSlotName(slotName);
            grownJackpot.setBrand(previousParameter.getBrand());
            grownJackpot.setSite(previousParameter.getSite().getShortUrl());
            grownJackpot.setGrownValue(grownValue);
            grownJackpotsList.add(grownJackpot);
        }
        return grownJackpotsList;
    }

    private List<GrownJackpotResponse> getMultipleGrownValues(Map<JackpotMultipleSlot, List<JackpotParameter>> parameters){
        List<GrownJackpotResponse> response = new ArrayList<>();
        for(TimePeriods timePeriod : TimePeriods.values()){
            List<List<GrownJackpots>> grownJackpotsList = new ArrayList<>();
            for(JackpotMultipleSlot multipleSlot : parameters.keySet()) {
                grownJackpotsList.add(getGrownJackpots(timePeriod, parameters.get(multipleSlot), multipleSlot.getSlotName()));
            }
            for(GrownJackpots grownJackpot : grownJackpotsList.get(0)){
                for(int i = 1; i < grownJackpotsList.size(); i++){
                    for(GrownJackpots findGrown : grownJackpotsList.get(i)){
                        if(grownJackpot.getSite().equals(findGrown.getSite()) &&
                                grownJackpot.getBrand().equals(findGrown.getBrand())){
                            grownJackpot.setGrownValue(grownJackpot.getGrownValue()+findGrown.getGrownValue());
                            break;
                        }
                    }
                }
            }
            List<GrownJackpots> grownJackpotsFinal = grownJackpotsList.get(0).stream().sorted(GrownJackpots.COMPARE_BY_VALUE).limit(5).collect(Collectors.toList());
            GrownJackpotResponse grownJackpotResponse = new GrownJackpotResponse();
            grownJackpotResponse.setGrownPeriod(timePeriod.getName());
            grownJackpotResponse.setGrownValues(grownJackpotsFinal);
            response.add(grownJackpotResponse);
        }
        return response;
    }

    public List<JackpotParameterLicense> setJackpotLicense(List<JackpotParameter> parameters){
        List<String> sites = parameters.stream().map(parameter -> CommonUtils.removeSlash(parameter.getSite().getShortUrl())).collect(Collectors.toList());
        List<SiteWithLicenses> siteWithLicenses = sitesService.getJackpotsWithLicense(sites);
        List<JackpotParameterLicense> parameterLicenseList = new ArrayList<>();
        for(JackpotParameter parameter : parameters){
            JackpotParameterLicense jackpotParameterLicense = new JackpotParameterLicense(parameter);
            for (SiteWithLicenses site : siteWithLicenses){
                if (CommonUtils.removeSlash(parameter.getSite().getShortUrl()).equals(site.getSite())){
                    jackpotParameterLicense.setLicenses(site.getLicenses());
                }
            }
            parameterLicenseList.add(jackpotParameterLicense);
        }
        return parameterLicenseList;
    }

    private void setMultipleRtp(Map<JackpotMultipleSlot, List<JackpotParameter>> jackpotParameters, List<JackpotMultipleSlot> multipleSlots){
        for(JackpotParameter parameter : jackpotParameters.get(multipleSlots.get(0))){
            List<JackpotMultipleValue> jackpotValues = new ArrayList<>();
            JackpotMultipleValue multipleValue = new JackpotMultipleValue(multipleSlots.get(0), parameter.getValueFirst());
            jackpotValues.add(multipleValue);
            for(int i = 1; i < multipleSlots.size(); i++){
                for(JackpotParameter findParameter : jackpotParameters.get(multipleSlots.get(i))){
                    if(parameter.getSite().getShortUrl().equals(findParameter.getSite().getShortUrl())
                            && parameter.getBrand().equals(findParameter.getBrand())
                            && parameter.getDate().equals(findParameter.getDate())){
                        JackpotMultipleValue oneMoreMultipleValue = new JackpotMultipleValue(multipleSlots.get(i), findParameter.getValueFirst());
                        jackpotValues.add(oneMoreMultipleValue);
                        break;
                    }
                }
            }
            parameter.setRtp(JackpotMultipleSlot.calculateRtp(multipleSlots.get(0), jackpotValues));
        }

        for(int i = 1; i < multipleSlots.size(); i++){
            for(JackpotParameter parameter : jackpotParameters.get(multipleSlots.get(i))){
                for(JackpotParameter firstSlot: jackpotParameters.get(multipleSlots.get(0))){
                    if(parameter.getSite().getShortUrl().equals(firstSlot.getSite().getShortUrl())
                            && parameter.getBrand().equals(firstSlot.getBrand())
                            && parameter.getDate().equals(firstSlot.getDate())){
                        parameter.setRtp(firstSlot.getRtp());
                        break;
                    }
                }
            }
        }
    }

    private LastPayedOutJackpot extractPaidOutJackpot(JackpotParameter parameter, String slotName, boolean multiple){
        LastPayedOutJackpot lastPayedOut = new LastPayedOutJackpot();
        lastPayedOut.setSlot(slotName);
        lastPayedOut.setSite(parameter.getSite());
        lastPayedOut.setBrand(parameter.getBrand());
        if(multiple){
            lastPayedOut.setValue(parameter.getValueFirst());
        } else {
            lastPayedOut.setValue(parameter.getValueSecond());
        }
        Date payedOutDate = new Date(parameter.getDate().getTime());
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
        lastPayedOut.setDate(formatter.format(payedOutDate));
        return lastPayedOut;
    }

    private MaxJackpotValue getMaxPaidOutJackpot(String slotName){
        List<MaxJackpotValue> slotJackpots = paidOutJackpots.stream().filter(j -> j.getSlot().equals(slotName))
                .map(j -> new MaxJackpotValue(j.getValue(), j.getSite().getShortUrl(), j.getBrand(), j.getSlot()))
                .collect(Collectors.toList());
        if(slotJackpots.size() > 0){
            Collections.sort(slotJackpots);
            return slotJackpots.get(0);
        } else {
            return null;
        }

    }
}
