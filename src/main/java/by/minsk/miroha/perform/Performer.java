package by.minsk.miroha.perform;

import java.util.ArrayList;
import java.util.List;

import by.minsk.miroha.entities.PairProfit;
import by.minsk.miroha.entities.PairSequence;
import by.minsk.miroha.entities.SlotInput;
import by.minsk.miroha.entities.enums.UserGrants;

public class Performer {

    private static final int INITIAL_SEQUENCE_VALUE = 1;
    private static final int TWO_MINUTES = 1000 * 60 + 1000 * 40;

    /**
     * Массив случайных коэффициентов (win/bid). используется при
     * расчете профита путем умножения на ставку.
     */
    private List<Double> winCoefficients = new ArrayList<>();

    /**
     * Массив профитов. Размер массива равен количеству симуляций.
     */
    private final List<Double> results = new ArrayList<>();
    private final List<Double> imitationResults = new ArrayList<>();

    private final List<Integer> nullProfitSequence = new ArrayList<>();
    private final List<Integer> profitSequence = new ArrayList<>();
    private boolean isSequenceProfitSet;
    private final List<Integer> simpleNullProfitSequence =new ArrayList<>();
    private boolean isSimpleNullProfitSet;
    private long betCounter;
    private String provider;
    private String game;
/*    private int count1 = 0;
    private int count2 = 0;
    private int bonusCount1 = 0;
    private int spinCounter = 0;*/

    /**
     * Конструктор.
     */
    public Performer() {
        isSequenceProfitSet = false;
        isSimpleNullProfitSet = false;
        betCounter = 0;
    }

    public Performer(String dbName, String tableName){
        provider = dbName;
        game = tableName;
        isSequenceProfitSet = false;
        isSimpleNullProfitSet = false;
        betCounter = 0;
    }

    /**
     * Устанавливается баланс (депозит + бонус).
     * Если ставка равно нулю, то вычисления заканчиваются и отдается ошибка.
     * Устанавливается баланс с учётом типа вагера для определения минимально необходимого
     *      количество ставок в одной имитации.
     * Определяется минимально необходимое количество ставок для одной имитации:
     *      баланс_с_учётом_типа_вагера * вагер / ставку.
     * Запускается цикл с количество повторений равным необхомому число количеству симуляций:
     *  1. Если в массиве случайных коэффициентов из БД количество элементов меньше
     *      минимально необходимого количества ставок, то массив случайных элементов
     *      дозагружается.
     *  2. Проводится одна симуляций и результат одной симуляции сохранятеся в массив результатов (профитов).
     *      Если необходимо вычитать бонус из результата симуляции, то бонус вычитается
     *          и если результат получается меньше нуля, то результат приравнивается к 0.
     * По окончанию цикла у нас формируется массив (results) с результатами симуляций.
     *  т.е. массив профитов. Размер массива - количество симуляций. Далее этот массив
     *      используется для расчета остальных показателей.
     * Далее суммируются все элементы массива результатов и деляться на количество симуляций -
     *  так получаем средний профит.
     *
     * @param slotInputData параметры слота, вводимые пользователем
     * @return средний профит.
     */
    public double getProfit(SlotInput slotInputData, boolean external) {
        long startTimer = System.currentTimeMillis();
        slotInputData.setBalance();
        double balanceForWagger = slotInputData.getWaggerType().getBalanceForWagger(slotInputData.getDeposit(), slotInputData.getBonus());
        int maxNumberOfBids = (int) (balanceForWagger * slotInputData.getWagger() / slotInputData.getBet());
        UnloaderFromHomeDB unloader;
        if (external){
            if(slotInputData.isAdminMode()){
                unloader = new UnloaderFromHomeDB(provider, game, true);
            } else {
                unloader = new UnloaderFromHomeDB(provider, game, false);
            }
        } else {
            if(slotInputData.isAdminMode()){
                unloader = new UnloaderFromHomeDB(true);
            } else {
                unloader = new UnloaderFromHomeDB(false);
            }
        }
        double addBonusThreshold;
        double addBonusValue;
        if (slotInputData.getBonusParts() > 0){
            addBonusThreshold = (slotInputData.getDeposit() + slotInputData.getBonus()) * slotInputData.getWagger() / slotInputData.getBonusParts();
            addBonusValue = slotInputData.getBonus()/ slotInputData.getBonusParts();
        } else {
            addBonusThreshold = 0;
            addBonusValue = 0;
        }
        for (int i = 0; i< slotInputData.getSim(); i++){
            if (winCoefficients.size()<maxNumberOfBids){
                if (slotInputData.getUserGrants() == UserGrants.DEMO &&
                        (System.currentTimeMillis() - startTimer) > TWO_MINUTES){
                    break;
                }
                winCoefficients = unloader.getAllCoefficients();
            }
            double imitationResult = getOneImitation(slotInputData.getBalance(), slotInputData.getBet(),
                                        maxNumberOfBids, addBonusThreshold, addBonusValue, slotInputData.getSpinLimit());
            if (slotInputData.getProfitLimit() != 0 && imitationResult > slotInputData.getProfitLimit()){
                imitationResult = slotInputData.getProfitLimit();
            }

            if(slotInputData.getUserGrants() == UserGrants.CASINO){
                imitationResults.add(imitationResult);
                imitationResult = toLimitProfitForCasino(slotInputData, imitationResult);
            } else {
                imitationResult = toLimitProfit(slotInputData, imitationResult);
            }
            results.add(imitationResult);
        }
        if(slotInputData.getUserGrants() == UserGrants.CASINO){
            double commonFees = slotInputData.getPlatformFee() + slotInputData.getProviderFee();
            double allBets = betCounter * slotInputData.getBet();
            double userWins = imitationResults.stream().mapToDouble(Double::doubleValue).sum();
            double otherFees = 0;
            if( allBets > userWins){
                otherFees = (allBets - userWins)*commonFees/100;
            }
            double casinoProfit = results.stream().mapToDouble(Double::doubleValue).sum();
            return (casinoProfit - otherFees)/slotInputData.getSim();
        } else {
            return results.stream().mapToDouble(Double::doubleValue).sum() / slotInputData.getSim();
        }
    }

    private double toLimitProfit(SlotInput slotInputData, double imitationResult) {
        if (slotInputData.isWithoutBonus()){
            imitationResult = imitationResult - slotInputData.getBonus()- imitationResult * slotInputData.getTaxes()/100;
            imitationResult = imitationResult < 0 ? 0 : imitationResult;
        } else {
            imitationResult = imitationResult - imitationResult * slotInputData.getTaxes()/100;
        }
        return imitationResult;
    }


    private double toLimitProfitForCasino(SlotInput slotInput, double imitationResult){
        if (imitationResult <= 0){
            imitationResult = slotInput.getDeposit() - slotInput.getDeposit()*slotInput.getPaymentFee()/100;
        } else if(imitationResult >= (slotInput.getDeposit() + slotInput.getBonus())){
            imitationResult = (-1)*(imitationResult + imitationResult*slotInput.getPaymentFee()/100 - slotInput.getDeposit()
                    + slotInput.getDeposit()* slotInput.getPaymentFee()/100);
        } else {
            imitationResult = slotInput.getDeposit() - imitationResult - slotInput.getDeposit()*slotInput.getPaymentFee()/100
                     - imitationResult*slotInput.getPaymentFee()/100;
        }
        return imitationResult;
    }

    /**
     * Пока баланс больше нуля и количество ставок меньше максимального количества
     * выполняются следующие действия:
     *  1. Увеличивается количество ставок на 1 (т.к. сейчас будем делать ставку).
     *  Если баланс больше или равен ставки, то выполняются следующие шаги,
     *  иначе баланс зануляется (выйдем из цикла):
     *      2. из конца массива случайных коэффициентов выбирается один коэффициент
     *      3. Выбранный в массиве коэффициент удаляется из массива.
     *      4. От баланса вычитается ставка и прибавляется ставка*случайный_коэффициент (получаем новый баланс)
     *      5. Увеличивается на 1 счетчик количества сделанных ставок.
     *  Выходим из имитации с итоговым балансом
     *
     * @param slotBalance баланс
     * @param bet ставка
     * @param minNumberOfBids минимально необходимое количество ставок
     * @return итоговый баланс по результатам ставок.
     */
    public double getOneImitation(double slotBalance, double bet, int minNumberOfBids,
                                    double addBonusThreshold, double addBonusValue, double spinLimit) {
        int addBonusCounter = 1;
        double betAmount = 0;
        double balance = slotBalance;
        int counter = 0;
        while (balance > 0 && counter < minNumberOfBids){
            counter++;
            if (balance >= bet) {
                double winCoef = winCoefficients.get(winCoefficients.size()-1);
                winCoefficients.remove(winCoefficients.size()-1);
                if(spinLimit > 0 && winCoef * bet > spinLimit){
                    balance = balance - bet + spinLimit;
                } else {
                    balance = balance - bet + winCoef * bet;
                }
                betCounter = betCounter+1;
                if(addBonusThreshold > 0){
                    betAmount = betAmount + bet;
                    if (betAmount >= addBonusCounter*addBonusThreshold){
                        balance = balance + addBonusValue;
                        addBonusCounter++;
                    }
                }
            }
            else {
                balance = 0;
            }
        }
        return balance;
    }

/*    public double getProfitRace1(int sim){
        long start = System.currentTimeMillis();
        List<Double> profits = new ArrayList<>();
        UnloaderFromHomeDB unloader = new UnloaderFromHomeDB(provider, game, false);
        for(int i = 0; i < sim; i++){
            if (winCoefficients.size() < 100){
                winCoefficients = unloader.getAllCoefficients();
            }
            profits.add(getOneImitationRace1());
        }
        long finish = System.currentTimeMillis();
        System.out.println("Time: " + (finish-start)/1000 + "c ");
        long count100, count200, count400, count600, count800, count1000, count1200, count1400, count1600, count1800;
        long count2000, count2200, count2400, count2600, count2800, count3000;
        count100 = profits.stream().filter(profit -> profit >= 100).count();
        count200 = profits.stream().filter(profit -> profit >= 200).count();
        count400 = profits.stream().filter(profit -> profit >= 400).count();
        count600 = profits.stream().filter(profit -> profit >= 600).count();
        count800 = profits.stream().filter(profit -> profit >= 800).count();
        count1000 = profits.stream().filter(profit -> profit >= 1000).count();
        count1200 = profits.stream().filter(profit -> profit >= 1200).count();
        count1400 = profits.stream().filter(profit -> profit >= 1400).count();
        count1600 = profits.stream().filter(profit -> profit >= 1600).count();
        count1800 = profits.stream().filter(profit -> profit >= 1800).count();
        count2000 = profits.stream().filter(profit -> profit >= 2000).count();
        count2200 = profits.stream().filter(profit -> profit >= 2200).count();
        count2400 = profits.stream().filter(profit -> profit >= 2400).count();
        count2600 = profits.stream().filter(profit -> profit >= 2600).count();
        count2800 = profits.stream().filter(profit -> profit >= 2800).count();
        count3000 = profits.stream().filter(profit -> profit >= 3000).count();
        if(count100 > 0){
            System.out.println("Frequency  more then 100 : " + sim / count100);
        }
        if(count200 > 0){
            System.out.println("Frequency  more then 200 : " + sim / count200);
        }
        if(count400 > 0){
            System.out.println("Frequency  more then 400 : " + sim / count400);
        }
        if(count600 > 0) {
            System.out.println("Frequency  more then 600 : " + sim / count600);
        }
        if(count800 > 0) {
            System.out.println("Frequency  more then 800 : " + sim / count800);
        }
        if(count1000 > 0) {
            System.out.println("Frequency  more then 1000 : " + sim / count1000);
        }
        if(count1200 > 0) {
            System.out.println("Frequency  more then 1200 : " + sim / count1200);
        }
        if(count1400 > 0) {
            System.out.println("Frequency  more then 1400 : " + sim / count1400);
        }
        if(count1600 > 0) {
            System.out.println("Frequency  more then 1600 : " + sim / count1600);
        }
        if (count1800 > 0) {
            System.out.println("Frequency  more then 1800 : " + sim / count1800);
        }
        double averageProfit = profits.stream().mapToDouble(Double::doubleValue).sum()/ sim;
        profits.clear();
        return averageProfit;
    }

    public double getProfitFakeRoulette(int sim){
        long start = System.currentTimeMillis();
        List<Double> profits = new ArrayList<>();
        UnloaderFromHomeDB unloader = new UnloaderFromHomeDB(false);
        for(int i = 0; i < sim; i++){
            if (winCoefficients.size() < 200){
                winCoefficients = unloader.getAllCoefficients();
            }
            profits.add(getOneImitationFakeRoulette());
        }
        long finish = System.currentTimeMillis();
        System.out.println("Time: " + (finish-start)/1000 + "c ");
        long count100, count200, count300, count400, count500, count600, count700, count800, count900, count1000;
        count100 = profits.stream().filter(profit -> profit >= 100).count();
        count200 = profits.stream().filter(profit -> profit >= 200).count();
        count300 = profits.stream().filter(profit -> profit >= 300).count();
        count400 = profits.stream().filter(profit -> profit >= 400).count();
        count500 = profits.stream().filter(profit -> profit >= 500).count();
        count600 = profits.stream().filter(profit -> profit >= 600).count();
        count700 = profits.stream().filter(profit -> profit >= 700).count();
        count800 = profits.stream().filter(profit -> profit >= 800).count();
        count900 = profits.stream().filter(profit -> profit >= 900).count();
        count1000 = profits.stream().filter(profit -> profit >= 1000).count();
        if(count100 > 0){
            System.out.println("Frequency  more then 100 : " + sim / count100);
        }
        if(count200 > 0){
            System.out.println("Frequency  more then 200 : " + sim / count200);
        }
        if(count300 > 0){
            System.out.println("Frequency  more then 300 : " + sim / count300);
        }
        if(count400 > 0) {
            System.out.println("Frequency  more then 400 : " + sim / count400);
        }
        if(count500 > 0) {
            System.out.println("Frequency  more then 500 : " + sim / count500);
        }
        if(count600 > 0) {
            System.out.println("Frequency  more then 600 : " + sim / count600);
        }
        if(count700 > 0) {
            System.out.println("Frequency  more then 700 : " + sim / count700);
        }
        if(count800 > 0) {
            System.out.println("Frequency  more then 800 : " + sim / count800);
        }
        if(count900 > 0) {
            System.out.println("Frequency  more then 900 : " + sim / count900);
        }
        if (count1000 > 0) {
            System.out.println("Frequency  more then 1000 : " + sim / count1000);
        }
        double freqCount1 = (double) spinCounter/count1;
        double freqCount2 = (double) spinCounter/count2;
        double freqBonusCount1 = (double) spinCounter/bonusCount1;
        System.out.println("Count 1 = " + freqCount1);
        System.out.println("Count 2 = " + freqCount2);
        System.out.println("BonusCount 1 = " + freqBonusCount1);
        double averageProfit = profits.stream().mapToDouble(Double::doubleValue).sum()/ sim;
        profits.clear();
        return averageProfit;
    }

    public double getOneImitationRace1(){
        double multipliersSum = 0;
        double coefficient;
        for(int i = 0; i < 100; i++){
            coefficient = winCoefficients.get(winCoefficients.size()-1);
            if(coefficient > 1000){
                multipliersSum = multipliersSum + 1000;
            } else {
                multipliersSum = multipliersSum + coefficient;
            }
            winCoefficients.remove(winCoefficients.size()-1);
        }
        return multipliersSum;
    }

    public double getOneImitationFakeRoulette(){
        double multipliersSum = 0;
        double coefficient;
        for(int i = 0; i < 100; i++){
            spinCounter++;
            coefficient = winCoefficients.get(winCoefficients.size()-1);
            winCoefficients.remove(winCoefficients.size()-1);
            if(coefficient == 1){
                multipliersSum = multipliersSum + 24;
                count1++;
            }
            if(coefficient == 2){
                count2++;
                multipliersSum = multipliersSum + fakeRouletteBonus();
            }
        }
        return multipliersSum;
    }

    public double fakeRouletteBonus(){
        double multipliersSum = 0;
        double coefficient;
        for(int i = 0; i < 2; i++){
            spinCounter++;
            coefficient = winCoefficients.get(winCoefficients.size()-1);
            winCoefficients.remove(winCoefficients.size()-1);
            if(coefficient == 1){
                bonusCount1++;
                multipliersSum = multipliersSum + 120;
            }
            if(coefficient == 2){
                count2++;
                multipliersSum = multipliersSum + fakeRouletteBonus();
            }
        }
        return multipliersSum;
    }
*/
    /**
     * ********** Метод для показателя Отношение выигрыша к депозиту ************
     * Проходим по каждому элементу в массиве с профитами:
     *  из этого массива выбираются только те элементы, которые если их поделить
     *  на депозит, будут больше или равны заданного коэффициента и далее считается
     *  количество таких элементов.
     * Если таких элементов не нашлось, то возвращается ноль,
     *  если элементы нашлись, то количество всех элементов в массиве с профитами
     *  делится на количество найденных элементов.
     *
     * @param times коэффициент, больше или равно которого ищется отношение
     *              выигрыша к депозиту.
     * @param deposit депозит
     * @return частота появления выигрыша превышающего депозит в заданное число раз.
     */
    public int winMoreThanDepositFrequency(int times, double deposit){
        long result = results.stream().filter(i -> i/deposit>=times).count();
        return result == 0 ? 0 : (int) (results.size() / result);
    }

    /**
     * ******* Метод для показателя Вероятность получить Profit больше 0 ***********
     * Проходим по каждому элементу в массиве с профитами:
     *      из этого массива выбираются только элементы строго большие нуля.
     * Считаем количество таких элементов, делим на количество всех элементов
     *  и умножаем на 100%. Получаем вероятность получить профит больше 0.
     *
     * @return вероятность получить профит больше 0
     */
    public double probabilityProfitMoreThanZero(){
        return (double) results.stream().filter(i -> i> 0).count()/results.size()*100;
    }

    /**
     * ******* Метод для показателя Вероятность получить Profit 0 ***********
     * Проходим по каждому элементу в массиве с профитами:
     *      из этого массива выбираются только элементы равные нулю.
     * Считаем количество таких элементов, делим на количество всех элементов
     *  и умножаем на 100%. Получаем вероятность получить профит равный 0.
     *
     * @return вероятность получить профит равный 0
     */
    public double probabilityProfitZero(){
        return (double) results.stream().filter(i -> i==0).count()/results.size()*100;
    }

    public int probabilityNullProfitAnyTimes(int anyTimes){
        if(!isSimpleNullProfitSet) {
            prepareSimpleNullProfitSequence();
        }
        return (int) simpleNullProfitSequence.stream().filter(i -> i > anyTimes).count();
    }

    public PairSequence maxSequenceWithoutProfit(){
        if (!isSequenceProfitSet){
            prepareNullProfitSequence();
        }
        PairSequence pairSequence = new PairSequence();
        pairSequence.setSeries(nullProfitSequence.stream().max(Integer::compare).orElse(nullProfitSequence.size()));
        long longestSeriesCount = nullProfitSequence.stream().filter(i -> i == pairSequence.getSeries()).count();
        long longestSeriesFrequency = (longestSeriesCount == 0 ? 0 :  results.size() / longestSeriesCount);
        pairSequence.setFrequency(longestSeriesFrequency);
        return pairSequence;
    }

    public PairSequence maxSequenceWithProfit(){
        if (!isSequenceProfitSet){
            prepareNullProfitSequence();
        }
        PairSequence pairProfit = new PairSequence();
        pairProfit.setSeries(profitSequence.stream().max(Integer::compare).orElse(0));
        long longestSeriesCount = profitSequence.stream().filter(i -> i == pairProfit.getSeries()).count();
        long longestSeriesFrequency = (longestSeriesCount == 0 ? 0 :  results.size() / longestSeriesCount);
        pairProfit.setFrequency(longestSeriesFrequency);
        return pairProfit;
    }

    /**
     * ********* Метод для показателя Самые крупные  Profit и их вероятности **********
     * 1. ищется самый большой профит в массиве профитов.
     * 2. ищется, сколько в массиве профитов элементов, равных самому большому элементу.
     * 3. Эта пара значений сохраняется с названием 1 (потом преобразуется в TOP-* (TOP-1))
     * 4. Запускается цикл на количество итераций, сколько TOP выигрышей надо -1 (-1 т.к.
     *  самый большой уже нашли).
     *      В цикле с помощью метода getTopBelowValue (описан ниже) получаем структуру:
     *      название топа (цифра) - выигрыш - сколько таких выигрышей в массиве выигрышей.
     * 5. Возвращаем все эти структуры с TOP выигрышами.
     *
     * @param topLimit количество, сколько TOP выигрышей необходимо посчитать.
     * @return массив структур :
     *          название топа (цифра) - выигрыш - сколько таких выигрышей в массиве выигрышей.
     */
    public List<PairProfit> getTop5Profits(int topLimit){
        List<PairProfit> profitsTop = new ArrayList<>();
        PairProfit pairProfitTop1 = new PairProfit();
        pairProfitTop1.setProfit(results.stream().max(Double::compare).orElse(0D));
        pairProfitTop1.setFrequency((double) results.stream().filter(i -> i==pairProfitTop1.getProfit()).count());
        pairProfitTop1.setName(1);
        profitsTop.add(pairProfitTop1);
        for (int counter = 1; counter < topLimit; counter++){
            PairProfit pairProfit = getTopBelowValue(profitsTop.get(profitsTop.size()-1).getProfit());
            pairProfit.setName(counter+1);
            profitsTop.add(pairProfit);
        }
        return profitsTop;
    }

    /**
     * Считаем волатильность бонуса: находим средний выигрыш делёный на депозит.
     * считаем сумму (каждый выигрыш, делёный на депозит, минус средний выигрыш делёный на депозит)
     * в квадрате и делим на количество симуляций.
     *
     * @return волатильность бонуса
     */
    public Double getBonusVolatility(double deposit){
        if (deposit == 0D){
            return 0D;
        }
        double averageWin = (results.stream().reduce(0D, Double::sum)/results.size())/deposit;
        return (results.stream().map(a -> (a/deposit-averageWin)*(a/deposit-averageWin)).
                reduce(0D, Double::sum)/results.size());
    }


    public double getAverageWin(){
        double sumProfit = results.stream().reduce(0D, Double::sum);
        double winFrequency = results.stream().filter(win -> win>0).count();
        return winFrequency == 0 ? 0 : sumProfit/winFrequency;
    }

    public long getBetCount(){
        return betCounter;
    }

    public void clearInformation(){
        results.clear();
        nullProfitSequence.clear();
        profitSequence.clear();
        simpleNullProfitSequence.clear();
    }

    /**
     * из массива профитов исключаются все элементы, которые больше определенного значения.
     *  В полученном массиве ищется максимальный элемент и сохраняется.
     * В массиве всех профитов считается количество элементов равных значению, полученному на предыдущем шаге.
     * Т.е. если нам надо посчитать TOP-3, то мы из массива исключаем элементы
     * большие или равные TOP-2 (TOP-1 и TOP-2). Далее ищем максимальный элемент - он и будет TOP-3.
     *
     * @param maxValue значение, ниже которого ищется максимальное значение.
     * @return структура: значение максимального профита - количество таких профитов
     */
    private PairProfit getTopBelowValue(double maxValue){
        PairProfit pairProfit = new PairProfit();
        pairProfit.setProfit(results.stream().filter(i -> i < maxValue).max(Double::compare).orElse(0D));
        pairProfit.setFrequency((double) results.stream().filter(i -> i==pairProfit.getProfit()).count());
        return pairProfit;
    }


    private void prepareNullProfitSequence(){
        int profitSubSequence;
        int nullProfitSubSequence;
        int subIndex;
        for (int mainIndex = 0; mainIndex < results.size(); mainIndex++){
            profitSubSequence = 0;
            nullProfitSubSequence = 0;
            subIndex = mainIndex + 1;
            if (results.get(mainIndex) == 0){
                nullProfitSubSequence = INITIAL_SEQUENCE_VALUE;
                while (subIndex < results.size() && results.get(subIndex) == 0){
                    nullProfitSubSequence++;
                    subIndex++;
                }
            } else{
                profitSubSequence = INITIAL_SEQUENCE_VALUE;
                while (subIndex < results.size() && results.get(subIndex) > 0){
                    profitSubSequence++;
                    subIndex++;
                }
            }
            nullProfitSequence.add(nullProfitSubSequence);
            profitSequence.add(profitSubSequence);
        }
        isSequenceProfitSet = true;
    }

    private void prepareSimpleNullProfitSequence(){
        int nullCounter = 0;
        for (Double result : results) {
            if (result == 0) {
                nullCounter++;
            } else {
                if (nullCounter > 0) {
                    simpleNullProfitSequence.add(nullCounter);
                    nullCounter = 0;
                }
            }
        }
        if (simpleNullProfitSequence.isEmpty()){
            simpleNullProfitSequence.add(nullCounter);
        }
        isSimpleNullProfitSet= true;
    }

}