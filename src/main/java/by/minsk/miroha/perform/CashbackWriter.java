package by.minsk.miroha.perform;

import by.minsk.miroha.entities.cashback.CashBack;
import by.minsk.miroha.entities.cashback.CashbackValue;
import by.minsk.miroha.entities.databases.SlotName;
import by.minsk.miroha.report.CashbackReport;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class CashbackWriter {

    public static final int INIT_ROW = 1;
    public static final int INIT_COLUMN = 1;
    public static final String SIM_AMOUNT = "Количество симуляций = ";

    List<Double> headerValues;


    public void writeCashbackTable(List<CashBack> cashBackList, double cashbackValue, double bid,
                                   int simulations, SlotName slotName){
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Cashback");
        Row valueRow;
        Cell valueCell;
        int currentRow = INIT_ROW;
        int currentColumn = INIT_COLUMN;
        CashBack maxResult = cashBackList.stream().max(Comparator.comparing(CashBack::getResult)).orElseThrow();
        currentRow = writeCashbackHeader(sheet, currentRow, currentColumn, cashbackValue, maxResult, bid,
                simulations, slotName);
        writeHeader(sheet, currentRow, cashBackList);
        List<Double> losses = cashBackList.stream()
                .map(CashBack::getMaxLoss).collect(Collectors.toSet()).stream().sorted(Comparator.naturalOrder())
                .collect(Collectors.toList());
        int lossIndex = 0;
        for (Double loss: losses){
            currentRow++;
            currentColumn = INIT_COLUMN ;
            valueRow = sheet.createRow(currentRow);
            valueCell = valueRow.createCell(currentColumn);
            valueCell.setCellValue(loss);
            currentColumn = INIT_COLUMN + lossIndex;
            for (Double win : headerValues){
                CashBack value = cashBackList.stream().filter(i ->
                        (i.getMaxLoss().equals(loss)) &&
                                (i.getTargetWin().equals(win))  &&
                                (i.getCashbackValue() == cashbackValue))
                        .findFirst().orElse(new CashBack());
                currentColumn++;
                if (value.getResult() != null) {
                    valueCell = valueRow.createCell(currentColumn);
                    valueCell.setCellValue(value.getResult());
                }
            }
        }
        FileOutputStream outputStream;
        try {
            String fileName = slotName.getFullName() + "_" + simulations + ".xlsx";
            outputStream = new FileOutputStream(fileName);
            workbook.write(outputStream);
            workbook.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeNewCashbackCells(List<CashBack> cashBackList, SlotName slotName, int simulations, int tableBorder){
        try {
            String fileName = slotName.getFullName() + "_" + simulations + ".xlsx";
            FileInputStream file = new FileInputStream(fileName);
            Workbook workbook = new XSSFWorkbook(file);
            Sheet sheet = workbook.getSheet("Cashback");
            Row headerRow = sheet.getRow(INIT_ROW + 1);
            int currentColumn = INIT_COLUMN + tableBorder + 1;
            List<Double> newHeaderValues = cashBackList.stream().map(CashBack::getTargetWin).filter(win -> win > tableBorder)
                    .collect(Collectors.toSet()).stream().sorted(Comparator.naturalOrder())
                    .collect(Collectors.toList());
            headerValues.addAll(newHeaderValues);
            Cell winsHeaderCell;
            for(Double headerValue : newHeaderValues){
                winsHeaderCell = headerRow.createCell(currentColumn);
                winsHeaderCell.setCellValue(headerValue);
                currentColumn++;
            }

            List<Double> losses = cashBackList.stream()
                    .map(CashBack::getMaxLoss).filter(loss -> loss > tableBorder).collect(Collectors.toSet())
                    .stream().sorted(Comparator.naturalOrder()).collect(Collectors.toList());
            Row lossHeaderRow;
            Cell lossHeaderCell;
            currentColumn = INIT_COLUMN;
            int currentRow = INIT_ROW + tableBorder + 1 + 1;
            for(Double initColumn : losses){
                lossHeaderRow = sheet.createRow(currentRow);
                lossHeaderCell = lossHeaderRow.createCell(currentColumn);
                lossHeaderCell.setCellValue(initColumn);
                currentRow++;
            }

            for (CashBack cashBack : cashBackList){
                currentRow = cashBack.getMaxLoss().intValue() + INIT_ROW + 1;
                currentColumn = cashBack.getTargetWin().intValue() + INIT_COLUMN;
                Row cashbackRow = sheet.getRow(currentRow);
                Cell cashbackCell = cashbackRow.createCell(currentColumn);
                cashbackCell.setCellValue(cashBack.getResult());
            }

            FileOutputStream outputStream;
            try {
                outputStream = new FileOutputStream(fileName);
                workbook.write(outputStream);
                workbook.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException fileNotFoundException) {
            fileNotFoundException.printStackTrace();
        }
    }

    public void writeCashbackReport(List<CashBack> cashBackList){
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Cashback");
        Row valueRow;
        Cell valueCell;
        int currentRow = INIT_ROW;
        int currentColumn;
        for (CashbackValue cashbackValue : CashbackValue.values()){
            currentColumn = INIT_COLUMN;
            CashBack maxResult = cashBackList.stream().filter(c -> c.getCashbackValue() == cashbackValue.getValue())
                    .max(Comparator.comparing(CashBack::getResult)).orElseThrow();
            currentRow = writeCashbackHeader(sheet, currentRow, currentColumn, cashbackValue.getValue(), maxResult, 0,
                    0, new SlotName());
            writeHeader(sheet, currentRow, cashBackList);
            List<Double> losses = cashBackList.stream().filter(c -> c.getCashbackValue() == cashbackValue.getValue())
                    .map(CashBack::getMaxLoss).collect(Collectors.toSet()).stream().sorted(Comparator.naturalOrder())
                    .collect(Collectors.toList());
            for (Double loss: losses){
                currentRow++;
                currentColumn = INIT_COLUMN;
                valueRow = sheet.createRow(currentRow);
                valueCell = valueRow.createCell(currentColumn);
                valueCell.setCellValue(loss);
                for (Double win : headerValues){
                    CashBack value = cashBackList.stream().filter(i ->
                            (i.getMaxLoss().equals(loss)) &&
                                    (i.getTargetWin().equals(win))  &&
                                    (i.getCashbackValue() == cashbackValue.getValue()))
                            .findFirst().orElse(new CashBack());
                    currentColumn++;
                    if (value.getResult() != null) {
                        valueCell = valueRow.createCell(currentColumn);
                        valueCell.setCellValue(value.getResult());
                        setBackgroundColor(workbook, valueCell, maxResult.getResult());
                    }
                }
            }
            currentRow = currentRow+3;
        }
        //createAnnotation(workbook,sheet, currentRow);
        FileOutputStream outputStream;
        try {
            outputStream = new FileOutputStream("Cashback.xlsx");
            workbook.write(outputStream);
            workbook.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static int writeCashbackHeader(Sheet sheet, int currentRow, int currentColumn,
                                           double cashbackValue, CashBack max, double bid,
                                           int simulations, SlotName slotName) {
        Row header = sheet.createRow(currentRow);
        Cell headerCell = header.createCell(currentColumn);
        headerCell.setCellValue("Cashback, % = ");
        currentColumn++;
        headerCell = header.createCell(currentColumn);
        headerCell.setCellValue(cashbackValue*100);
        currentColumn++;
        headerCell = header.createCell(currentColumn);
        headerCell.setCellValue("Max = ");
        currentColumn++;
        headerCell = header.createCell(currentColumn);
        headerCell.setCellValue(max.getResult());
        currentColumn++;
        headerCell = header.createCell(currentColumn);
        headerCell.setCellValue("Loss = ");
        currentColumn++;
        headerCell = header.createCell(currentColumn);
        headerCell.setCellValue(max.getMaxLoss());
        currentColumn++;
        headerCell = header.createCell(currentColumn);
        headerCell.setCellValue("Profit = ");
        currentColumn++;
        headerCell = header.createCell(currentColumn);
        headerCell.setCellValue(max.getTargetWin());
        currentColumn++;
        headerCell = header.createCell(currentColumn);
        headerCell.setCellValue("Bid = ");
        currentColumn++;
        headerCell = header.createCell(currentColumn);
        headerCell.setCellValue(bid);
        currentColumn++;
        headerCell = header.createCell(currentColumn);
        headerCell.setCellValue("Sim = ");
        currentColumn++;
        headerCell = header.createCell(currentColumn);
        headerCell.setCellValue(simulations);
        currentColumn++;
        headerCell = header.createCell(currentColumn);
        headerCell.setCellValue("Provider = ");
        currentColumn++;
        headerCell = header.createCell(currentColumn);
        headerCell.setCellValue(slotName.getProvider());
        currentColumn++;
        headerCell = header.createCell(currentColumn);
        headerCell.setCellValue("Slot = ");
        currentColumn++;
        headerCell = header.createCell(currentColumn);
        headerCell.setCellValue(slotName.getSlotName());
        currentRow++;
        return currentRow;
    }

    private void writeHeader(Sheet sheet, int currentRow, List<CashBack> cashBackList) {
        int currentColumn;
        Row winsHeader;
        Cell winsHeaderCell;
        currentColumn = INIT_COLUMN;
        winsHeader = sheet.createRow(currentRow);
        winsHeaderCell = winsHeader.createCell(currentColumn);
        winsHeaderCell.setCellValue("Loss/Profit");
        currentColumn++;
        headerValues = cashBackList.stream().map(CashBack::getTargetWin).collect(Collectors.toSet())
                .stream().sorted(Comparator.naturalOrder())
                .collect(Collectors.toList());
        for (Double wins : headerValues){
            winsHeaderCell = winsHeader.createCell(currentColumn);
            winsHeaderCell.setCellValue(wins);
            currentColumn++;
        }
    }

    private static void setBackgroundColor(Workbook workbook, Cell cell, double maxResult){
        CellStyle firstSimStyle = workbook.createCellStyle();
        firstSimStyle.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
        firstSimStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        firstSimStyle.setWrapText(true);
        CellStyle secondSimStyle = workbook.createCellStyle();
        secondSimStyle.setFillForegroundColor(IndexedColors.BRIGHT_GREEN.getIndex());
        secondSimStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        secondSimStyle.setWrapText(true);
        CellStyle thirdSimStyle = workbook.createCellStyle();
        thirdSimStyle.setFillForegroundColor(IndexedColors.GREEN.getIndex());
        thirdSimStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        thirdSimStyle.setWrapText(true);

        if (cell.getNumericCellValue() == maxResult){
            cell.setCellStyle(secondSimStyle);
        } else {
            cell.setCellStyle(firstSimStyle);
        }

        /*if (simCount == CashbackReport.FIRST_SIMULATION){
            cell.setCellStyle(firstSimStyle);
        } else if(simCount == CashbackReport.SECOND_SIMULATION) {
            cell.setCellStyle(secondSimStyle);
        } else if (simCount == CashbackReport.THIRD_SIMULATION){
            cell.setCellStyle(thirdSimStyle);
        }*/
    }

    private static void createAnnotation(Workbook workbook, Sheet sheet, int currentRow){
        Row annotationRow = sheet.createRow(currentRow);
        Cell annotationCell = annotationRow.createCell(INIT_COLUMN);
        annotationCell.setCellValue(new String((SIM_AMOUNT + CashbackReport.FIRST_SIMULATION).getBytes(), StandardCharsets.UTF_8));
        setBackgroundColor(workbook, annotationCell, CashbackReport.FIRST_SIMULATION);
        currentRow++;
        annotationRow = sheet.createRow(currentRow);
        annotationCell = annotationRow.createCell(INIT_COLUMN);
        annotationCell.setCellValue(new String((SIM_AMOUNT + CashbackReport.SECOND_SIMULATION).getBytes(), StandardCharsets.UTF_8));
        setBackgroundColor(workbook, annotationCell, CashbackReport.SECOND_SIMULATION);
        currentRow++;
        annotationRow = sheet.createRow(currentRow);
        annotationCell = annotationRow.createCell(INIT_COLUMN);
        annotationCell.setCellValue(new String((SIM_AMOUNT + CashbackReport.THIRD_SIMULATION).getBytes(), StandardCharsets.UTF_8));
        setBackgroundColor(workbook, annotationCell, CashbackReport.THIRD_SIMULATION);
    }
}
