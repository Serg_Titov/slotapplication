package by.minsk.miroha.repositories;

import by.minsk.miroha.entities.databases.SlotName;

public interface SlotNameRepository {

    SlotName getSlotName();

    void save(SlotName slotName);

    void clearTable();
}
