package by.minsk.miroha.repositories.audit;

import by.minsk.miroha.entities.databases.UsersBonusAudit;

public interface UsersBonusAuditRepository {

    void save(UsersBonusAudit usersBonusAudit);

    void update(UsersBonusAudit usersBonusAudit);

    UsersBonusAudit getLastLoginByUsername(String username);

    UsersBonusAudit getObjectById(long id);
}
