package by.minsk.miroha.repositories.audit.impl;

import by.minsk.miroha.entities.databases.UsersBonusAudit;
import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.repositories.audit.UsersBonusAuditRepository;
import by.minsk.miroha.utils.DatabaseUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserBonusAuditRepositoryImpl implements UsersBonusAuditRepository {

    public static final String DB_ERROR = "Не удалось подключиться к БД";
    private static final int FIRST_ITEM = 1;
    private static final int SECOND_ITEM = 2;
    private static final int THIRD_ITEM = 3;
    private static final int FOURTH_ITEM = 4;

    private static final String ID = "user_audit_id";
    private static final String USERNAME = "username";
    private static final String LOGIN_DATE = "login_date";
    private static final String BONUS_COUNT = "bonus_count";

    @Override
    public void save(UsersBonusAudit usersBonusAudit) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            int nextId = findMaxId(connection);
            nextId++;
            preparedStatement = connection.prepareStatement(
                    "INSERT INTO Slots.users_bonus_audit (username, login_date, bonus_count, user_audit_id) VALUES (?, ?, ?, ?);");
            fillUserBonusAudit(preparedStatement, usersBonusAudit);
            preparedStatement.setLong(FOURTH_ITEM, nextId);
            preparedStatement.executeUpdate();
        } catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatement(connection, preparedStatement);
        }
    }

    @Override
    public void update(UsersBonusAudit usersBonusAudit) {
        Connection connection = null;
        PreparedStatement preparedStatement =null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("UPDATE Slots.users_bonus_audit" +
                    " SET bonus_count = ? where user_audit_id = ?;");
            preparedStatement.setInt(FIRST_ITEM, usersBonusAudit.getBonusCount());
            preparedStatement.setLong(SECOND_ITEM, usersBonusAudit.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatement(connection, preparedStatement);
        }
    }

    @Override
    public UsersBonusAudit getLastLoginByUsername(String username) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try{
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("select * from Slots.users_bonus_audit" +
                    " where username = ? and login_date = (select max(login_date) " +
                    "from Slots.users_bonus_audit where username = ?)");
            preparedStatement.setString(FIRST_ITEM, username);
            preparedStatement.setString(SECOND_ITEM, username);
            resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                return getUsersBonusAudit(resultSet);
            }
            return null;
        } catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        } finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public UsersBonusAudit getObjectById(long id) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("select * from Slots.users_bonus_audit where user_audit_id = ?");
            preparedStatement.setLong(FIRST_ITEM, id);
            resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                return getUsersBonusAudit(resultSet);
            }
            return null;
        } catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        } finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    private int findMaxId(Connection connection) {
        return DatabaseUtils.getParameterFromQuery("select max(user_audit_id) from Slots.users_bonus_audit", connection);
    }

    private void fillUserBonusAudit(PreparedStatement preparedStatement, UsersBonusAudit usersBonusAudit) throws SQLException {
        preparedStatement.setString(FIRST_ITEM, usersBonusAudit.getUsername());
        preparedStatement.setDate(SECOND_ITEM, usersBonusAudit.getLoginDate());
        preparedStatement.setInt(THIRD_ITEM, usersBonusAudit.getBonusCount());
    }

    private UsersBonusAudit getUsersBonusAudit(ResultSet resultSet) throws SQLException {
        UsersBonusAudit usersBonusAudit = new UsersBonusAudit();
        usersBonusAudit.setId(resultSet.getLong(ID));
        usersBonusAudit.setUsername(resultSet.getString(USERNAME));
        usersBonusAudit.setLoginDate(resultSet.getDate(LOGIN_DATE));
        usersBonusAudit.setBonusCount(resultSet.getInt(BONUS_COUNT));
        return usersBonusAudit;
    }
}
