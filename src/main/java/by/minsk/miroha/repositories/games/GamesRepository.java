package by.minsk.miroha.repositories.games;

import by.minsk.miroha.entities.databases.SlotName;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface GamesRepository {

    Map<String, List<String>> getAllGames(List<String> providers);

    List<String> getGamesByProvider(String provider);

    Set<String> getAllProviders();

    List<SlotName> getAllProvidersWithSlots();

    long getTotalSpinAmount(String slotFullName);

    Date getCreationTableDate(String provider, String slotName);
}
