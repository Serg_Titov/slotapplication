package by.minsk.miroha.repositories.games.impl;

import by.minsk.miroha.entities.databases.SlotBriefInfo;
import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.repositories.games.SlotBriefInfoRepository;
import by.minsk.miroha.utils.DatabaseUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static by.minsk.miroha.repositories.impl.SlotInfoRepositoryImpl.DB_ERROR;

public class SlotBriefInfoRepositoryImpl implements SlotBriefInfoRepository {

    private static final int FIRST_ITEM = 1;
    private static final int SECOND_ITEM = 2;
    private static final int THIRD_ITEM = 3;
    private static final int FOURTH_ITEM = 4;
    private static final int FIFTH_ITEM = 5;
    private static final String BRIEF_ID = "brief_id";
    private static final String PROVIDER = "provider";
    private static final String SLOT_NAME = "slot_name";
    private static final String SPIN_AMOUNT = "spin_amount";
    private static final String CREATION_DATE = "creation_date";

    @Override
    public List<SlotBriefInfo> getSlotBriefInfo() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("SELECT * FROM Slots.slots_brief_info");
            resultSet = preparedStatement.executeQuery();
            List<SlotBriefInfo> slotBriefInfoList  = new ArrayList<>();
            while (resultSet.next()){
                slotBriefInfoList.add(fillSlotBriefInfo(resultSet));
            }
            return slotBriefInfoList;
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        } finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public SlotBriefInfo getObjectByName(String provider, String slotName) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("SELECT * FROM Slots.slots_brief_info\n " +
                    "WHERE provider = ? and slot_name = ?");
            preparedStatement.setString(FIRST_ITEM, provider);
            preparedStatement.setString(SECOND_ITEM, slotName);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return fillSlotBriefInfo(resultSet);
            }
            return null;
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        } finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public void save(List<SlotBriefInfo> list) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            for(SlotBriefInfo slotBriefInfo : list) {
                SlotBriefInfo exist = getObjectByName(slotBriefInfo.getProvider(), slotBriefInfo.getSlotName());
                if(exist == null) {
                    long nextId = findMaxId(connection);
                    nextId++;
                    preparedStatement = connection.prepareStatement("INSERT INTO Slots.slots_brief_info" +
                            " (provider, slot_name, spin_amount, creation_date, brief_id) " +
                            "VALUES (?, ?, ?, ?, ?);");
                    fillPreparedStatementBySlotBriefInfo(preparedStatement, slotBriefInfo);
                    preparedStatement.setLong(FIFTH_ITEM, nextId);
                    preparedStatement.executeUpdate();
                }
            }
        } catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatement(connection, preparedStatement);
        }
    }

    @Override
    public void update(List<SlotBriefInfo> list) {
        Connection connection = null;
        PreparedStatement preparedStatement =null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            for(SlotBriefInfo slotBriefInfo : list) {
                preparedStatement = connection.prepareStatement("UPDATE Slots.slots_brief_info" +
                        " SET provider = ?, slot_name = ?, spin_amount = ?, creation_date = ? where id_slot_input = ?;");
                fillPreparedStatementBySlotBriefInfo(preparedStatement, slotBriefInfo);
                preparedStatement.setLong(FIFTH_ITEM, slotBriefInfo.getBriefId());
                preparedStatement.executeUpdate();
            }
        }
        catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatement(connection, preparedStatement);
        }
    }

    @Override
    public void deleteById(int id) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("Delete from Slots.slots_brief_info " +
                    "where brief_id = ?;");
            preparedStatement.setLong(FIRST_ITEM, id);
            preparedStatement.executeUpdate();
        }
        catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatement(connection, preparedStatement);
        }
    }

    @Override
    public void truncate() {
        DatabaseUtils.truncateTable("Slots.slots_brief_info");
    }

    private int findMaxId(Connection connection) {
        return DatabaseUtils.getParameterFromQuery("select max(brief_id) from Slots.slots_brief_info", connection);
    }

    private SlotBriefInfo fillSlotBriefInfo(ResultSet resultSet) throws SQLException {
        SlotBriefInfo slotBriefInfo = new SlotBriefInfo();
        slotBriefInfo.setBriefId(resultSet.getLong(BRIEF_ID));
        slotBriefInfo.setProvider(resultSet.getString(PROVIDER));
        slotBriefInfo.setSlotName(resultSet.getString(SLOT_NAME));
        slotBriefInfo.setSpinAmount(resultSet.getLong(SPIN_AMOUNT));
        slotBriefInfo.setCreationDate(new Date(resultSet.getDate(CREATION_DATE).getTime()));

        return slotBriefInfo;
    }

    private void fillPreparedStatementBySlotBriefInfo(PreparedStatement preparedStatement, SlotBriefInfo slotBriefInfo) throws SQLException {
        preparedStatement.setString(FIRST_ITEM, slotBriefInfo.getProvider());
        preparedStatement.setString(SECOND_ITEM, slotBriefInfo.getSlotName());
        preparedStatement.setLong(THIRD_ITEM, slotBriefInfo.getSpinAmount());
        preparedStatement.setDate(FOURTH_ITEM, new java.sql.Date(slotBriefInfo.getCreationDate().getTime()));
    }
}
