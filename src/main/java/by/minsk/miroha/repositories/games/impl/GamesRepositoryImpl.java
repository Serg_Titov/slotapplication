package by.minsk.miroha.repositories.games.impl;

import by.minsk.miroha.entities.databases.SlotName;
import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.repositories.games.GamesRepository;
import by.minsk.miroha.utils.DatabaseUtils;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;


import static by.minsk.miroha.repositories.report.impl.TopProfitsRepositoryImpl.DB_ERROR;

@Repository
public class GamesRepositoryImpl implements GamesRepository {

    private static final int FIRST_ITEM = 1;
    private static final int SECOND_ITEM = 2;
    private static final String TABLE_SCHEMA = "table_schema";
    private static final String TABLE_NAME = "table_name";

    @Override
    public Map<String, List<String>> getAllGames(List<String> providers) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            Map<String, List<String>> gameSources = new HashMap<>();
            for (String provider : providers) {
                preparedStatement = connection.prepareStatement("SELECT table_name FROM information_schema.tables\n " +
                        "WHERE table_schema = ?");
                preparedStatement.setString(FIRST_ITEM, provider);
                resultSet = preparedStatement.executeQuery();
                List<String> games = new ArrayList<>();
                while (resultSet.next()) {
                    games.add(resultSet.getString(FIRST_ITEM));
                }
                gameSources.put(provider, games);
            }
            return gameSources;
        }
        catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public List<String> getGamesByProvider(String provider) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("SELECT table_name FROM information_schema.tables\n " +
                        "WHERE table_schema = ?");
            preparedStatement.setString(FIRST_ITEM, provider);
            resultSet = preparedStatement.executeQuery();
            List<String> games = new ArrayList<>();
            while (resultSet.next()) {
                games.add(resultSet.getString(FIRST_ITEM));
            }
            return games;
        }
        catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public Set<String> getAllProviders() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("select TABLE_SCHEMA from INFORMATION_SCHEMA.TABLES" +
                    " WHERE TABLE_TYPE = 'BASE TABLE'");
            resultSet = preparedStatement.executeQuery();
            Set<String> tables = new HashSet<>();
            while (resultSet.next()){
                tables.add(resultSet.getString(FIRST_ITEM));
            }
            return tables;
        }
        catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public List<SlotName> getAllProvidersWithSlots() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("SELECT table_schema, table_name FROM information_schema.tables " +
                    " WHERE TABLE_TYPE = 'BASE TABLE'");
            resultSet = preparedStatement.executeQuery();
            List<SlotName> slotNameList = new ArrayList<>();
            while (resultSet.next()){
                slotNameList.add(extractSlotName(resultSet));
            }
            return slotNameList;
        }
        catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public long getTotalSpinAmount(String slotFullName) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("select count(*) from " + slotFullName);
            resultSet = preparedStatement.executeQuery();
            return DatabaseUtils.getParameterFromResultSet(resultSet);
        } catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        } finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public Date getCreationTableDate(String provider, String slotName) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("SELECT create_time FROM INFORMATION_SCHEMA.TABLES" +
                    " WHERE table_schema = ? AND table_name = ? ") ;
            preparedStatement.setString(FIRST_ITEM, provider);
            preparedStatement.setString(SECOND_ITEM, slotName);
            resultSet = preparedStatement.executeQuery();
            java.sql.Date dateFromDB = DatabaseUtils.getDateParameterFromResultSet(resultSet);
            if (dateFromDB != null) {
                return new Date(dateFromDB.getTime());
            }
            return null;
        } catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        } finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    private SlotName extractSlotName(ResultSet resultSet) throws SQLException{
        SlotName slotName = new SlotName();
        slotName.setProvider(resultSet.getString(TABLE_SCHEMA));
        slotName.setSlotName(resultSet.getString(TABLE_NAME));
        return slotName;
    }
}
