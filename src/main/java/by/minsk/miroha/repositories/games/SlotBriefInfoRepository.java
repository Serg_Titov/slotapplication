package by.minsk.miroha.repositories.games;

import by.minsk.miroha.entities.databases.SlotBriefInfo;

import java.util.List;

public interface SlotBriefInfoRepository {

    List<SlotBriefInfo> getSlotBriefInfo();

    SlotBriefInfo getObjectByName(String provider, String slotName);

    void save(List<SlotBriefInfo> list);

    void update(List<SlotBriefInfo> list);

    void deleteById(int id);

    void truncate();
}
