package by.minsk.miroha.repositories.jackpots.impl;

import by.minsk.miroha.entities.databases.JackpotGrowth;
import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.repositories.jackpots.JackpotGrowthRepository;
import by.minsk.miroha.utils.DatabaseUtils;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static by.minsk.miroha.repositories.report.impl.CommonSimInfoRepositoryImpl.DB_ERROR;

@Repository
public class JackpotGrowthRepositoryImpl implements JackpotGrowthRepository {

    private static final String ID = "jackpot_growth_id";
    private static final String SUMMARY_ID = "jackpot_summary_id";
    private static final String SITE = "site";
    private static final String BRAND = "brand";
    private static final String GROWTH_RATE = "growth_rate";
    private static final String TIME_PERIOD = "time_period";
    private static final int FIRST_ITEM = 1;
    private static final int SECOND_ITEM = 2;
    private static final int THIRD_ITEM = 3;
    private static final int FOURTH_ITEM = 4;
    private static final int FIFTH_ITEM = 5;

    @Override
    public List<JackpotGrowth> getGrowthBySummaryId(long summaryId) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("select * from Slots.jackpot_growth " +
                    " where jackpot_summary_id = ?");
            preparedStatement.setLong(FIRST_ITEM, summaryId);
            resultSet = preparedStatement.executeQuery();
            List<JackpotGrowth> growthList = new ArrayList<>();
            while (resultSet.next()){
                growthList.add(extractJackpotGrowth(resultSet));
            }
            return growthList;
        } catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public List<JackpotGrowth> getGrowthBySummaryIdList(List<Long> summaryIdList) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            List<JackpotGrowth> jackpotGrowthList = new ArrayList<>();
            for(Long summaryId : summaryIdList) {
                preparedStatement = connection.prepareStatement("select * from Slots.jackpot_growth " +
                        " where jackpot_summary_id = ?");
                preparedStatement.setLong(FIRST_ITEM, summaryId);
                resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    jackpotGrowthList.add(extractJackpotGrowth(resultSet));
                }
            }
            return jackpotGrowthList;
        } catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public void save(List<JackpotGrowth> jackpotGrowthList) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            for(JackpotGrowth jackpotGrowth : jackpotGrowthList){
                preparedStatement = connection.prepareStatement("INSERT INTO Slots.jackpot_growth" +
                        " (jackpot_summary_id, site, brand, growth_rate, time_period) " +
                        " VALUES (?, ?, ?, ?, ?);");
                preparedStatement.setLong(FIRST_ITEM, jackpotGrowth.getJackpotSummaryId());
                preparedStatement.setString(SECOND_ITEM, jackpotGrowth.getSite());
                preparedStatement.setString(THIRD_ITEM, jackpotGrowth.getBrand());
                preparedStatement.setDouble(FOURTH_ITEM, jackpotGrowth.getGrowthRate());
                preparedStatement.setString(FIFTH_ITEM, jackpotGrowth.getTimePeriod());
                preparedStatement.executeUpdate();
            }
        } catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatement(connection, preparedStatement);
        }
    }

    private JackpotGrowth extractJackpotGrowth(ResultSet resultSet) throws SQLException{
        JackpotGrowth growth = new JackpotGrowth();
        growth.setJackpotGrowthId(resultSet.getLong(ID));
        growth.setBrand(resultSet.getString(BRAND));
        growth.setSite(resultSet.getString(SITE));
        growth.setGrowthRate(resultSet.getDouble(GROWTH_RATE));
        growth.setTimePeriod(resultSet.getString(TIME_PERIOD));
        growth.setJackpotSummaryId(resultSet.getLong(SUMMARY_ID));
        return growth;
    }
}
