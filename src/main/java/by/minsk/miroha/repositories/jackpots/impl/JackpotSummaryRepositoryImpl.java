package by.minsk.miroha.repositories.jackpots.impl;

import by.minsk.miroha.entities.databases.JackpotSummary;
import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.repositories.jackpots.JackpotSummaryRepository;
import by.minsk.miroha.utils.DatabaseUtils;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static by.minsk.miroha.repositories.report.impl.CommonSimInfoRepositoryImpl.DB_ERROR;

@Repository
public class JackpotSummaryRepositoryImpl implements JackpotSummaryRepository {

    private static final int FIRST_ITEM = 1;
    private static final int SECOND_ITEM = 2;
    private static final int THIRD_ITEM = 3;
    private static final int FOURTH_ITEM = 4;
    private static final int FIFTH_ITEM = 5;
    private static final String ID = "jackpot_summary_id";
    private static final String UNIQUE_JACKPOTS = "unique_jackpots";
    private static final String PAID_OUT_AMOUNT = "paid_out_amount";
    private static final String COUNTER_100_RTP = "counter_100rtp";
    private static final String SLOT = "slot";

    @Override
    public JackpotSummary getLastSummary(String slot) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement(" select * from Slots.jackpot_summary " +
                    " where jackpot_summary_id = (select max(jackpot_summary_id) from Slots.jackpot_summary" +
                    "       where slot = ?) ");
            preparedStatement.setString(FIRST_ITEM, slot);
            resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                return extractJackpotSummary(resultSet);
            } else {
                return null;
            }
        } catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public List<JackpotSummary> getMultipleLastSummary(List<String> slots) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            List<JackpotSummary> summaries = new ArrayList<>();
            for(String slot : slots) {
                preparedStatement = connection.prepareStatement(" select * from Slots.jackpot_summary " +
                        " where jackpot_summary_id = (select max(jackpot_summary_id) from Slots.jackpot_summary" +
                        "       where slot = ?) ");
                preparedStatement.setString(FIRST_ITEM, slot);
                resultSet = preparedStatement.executeQuery();
                if(resultSet.next()){
                     summaries.add(extractJackpotSummary(resultSet));
                }
            }
            return summaries;
        } catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public JackpotSummary saveSummary(JackpotSummary jackpotSummary) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("INSERT INTO Slots.jackpot_summary" +
                    " (unique_jackpots, paid_out_amount, counter_100rtp, slot, time_check) " +
                    " VALUES (?, ?, ?, ?, ?);");
            preparedStatement.setInt(FIRST_ITEM, jackpotSummary.getUniqueJackpots());
            preparedStatement.setInt(SECOND_ITEM, jackpotSummary.getPaidOutAmount());
            preparedStatement.setInt(THIRD_ITEM, jackpotSummary.getCounter100Rtp());
            preparedStatement.setString(FOURTH_ITEM, jackpotSummary.getSlot());
            preparedStatement.setTimestamp(FIFTH_ITEM, jackpotSummary.getTimeCheck());
            preparedStatement.executeUpdate();
            return getLastSummary(jackpotSummary.getSlot());
        } catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatement(connection, preparedStatement);
        }
    }

    private JackpotSummary extractJackpotSummary(ResultSet resultSet) throws SQLException{
        JackpotSummary jackpotSummary = new JackpotSummary();
        jackpotSummary.setJackpotSummaryId(resultSet.getLong(ID));
        jackpotSummary.setUniqueJackpots(resultSet.getInt(UNIQUE_JACKPOTS));
        jackpotSummary.setCounter100Rtp(resultSet.getInt(COUNTER_100_RTP));
        jackpotSummary.setPaidOutAmount(resultSet.getInt(PAID_OUT_AMOUNT));
        jackpotSummary.setSlot(resultSet.getString(SLOT));
        return jackpotSummary;
    }
}
