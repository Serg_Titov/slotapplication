package by.minsk.miroha.repositories.jackpots;

import by.minsk.miroha.entities.databases.JackpotSummary;

import java.util.List;

public interface JackpotSummaryRepository {

    JackpotSummary getLastSummary(String slot);

    List<JackpotSummary> getMultipleLastSummary(List<String> slots);

    JackpotSummary saveSummary(JackpotSummary jackpotSummary);
}
