package by.minsk.miroha.repositories.jackpots.impl;

import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.report.jackpot.JackpotParameter;
import by.minsk.miroha.repositories.jackpots.JackpotSiteRepository;
import by.minsk.miroha.utils.DatabaseUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static by.minsk.miroha.repositories.report.impl.CommonSimInfoRepositoryImpl.DB_ERROR;

public class JackpotSiteRepositoryImpl implements JackpotSiteRepository {

    private static final int FIRST_ITEM = 1;
    private static final int SECOND_ITEM = 2;
    private static final String JACKPOT = "jackpot";
    private static final String LAST_PAYED_OUT = "last_payedout";
    private static final String TIME_CHECK = "time_check";

    @Override
    public Double getCurrentJackpot(String slot, String site) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("select mt.jackpot from jackpot_statistic.statistic mt " +
                    " where mt.id = (select max(sr.id) from jackpot_statistic.statistic sr " +
                    " where sr.slot = ? and sr.site = ?) ");
            preparedStatement.setString(FIRST_ITEM, slot);
            preparedStatement.setString(SECOND_ITEM, site);
            resultSet = preparedStatement.executeQuery();
            return DatabaseUtils.getDoubleParameterFromResultSet(resultSet);
        } catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public Double getMaxPayedOutJackpot(String slot, String site) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("select max(last_payedout) from jackpot_statistic.statistic " +
                    " where slot = ? and site = ? ");
            preparedStatement.setString(FIRST_ITEM, slot);
            preparedStatement.setString(SECOND_ITEM, site);
            resultSet = preparedStatement.executeQuery();
            return DatabaseUtils.getDoubleParameterFromResultSet(resultSet);
        } catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public Double getMinPayedOutJackpot(String slot, String site) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("select min(last_payedout) from jackpot_statistic.statistic " +
                    " where slot = ? and site = ? ");
            preparedStatement.setString(FIRST_ITEM, slot);
            preparedStatement.setString(SECOND_ITEM, site);
            resultSet = preparedStatement.executeQuery();
            return DatabaseUtils.getDoubleParameterFromResultSet(resultSet);
        } catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public List<JackpotParameter> getFullInformation(String slot, String site) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("select jackpot, last_payedout, time_check " +
                    " from jackpot_statistic.statistic where slot = ? and site = ? " +
                    " ORDER BY id DESC");
            preparedStatement.setString(FIRST_ITEM, slot);
            preparedStatement.setString(SECOND_ITEM, site);
            resultSet = preparedStatement.executeQuery();
            List<JackpotParameter> parameters = new ArrayList<>();
            while (resultSet.next()) {
                parameters.add(getParameter(resultSet));
            }
            return parameters;
        } catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    private JackpotParameter getParameter(ResultSet resultSet) throws SQLException{
        JackpotParameter parameter = new JackpotParameter();
        parameter.setValueFirst(resultSet.getDouble(JACKPOT));
        parameter.setValueSecond(resultSet.getDouble(LAST_PAYED_OUT));
        parameter.setDate(resultSet.getTimestamp(TIME_CHECK));
        return parameter;
    }

}
