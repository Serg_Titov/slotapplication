package by.minsk.miroha.repositories.jackpots.impl;

import by.minsk.miroha.entities.databases.SummaryParameter;
import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.repositories.jackpots.JackpotSummaryParameterRepository;
import by.minsk.miroha.utils.DatabaseUtils;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static by.minsk.miroha.repositories.report.impl.CommonSimInfoRepositoryImpl.DB_ERROR;

@Repository
public class JackpotSummaryParameterRepositoryImpl implements JackpotSummaryParameterRepository {

    private static final int FIRST_ITEM = 1;
    private static final int SECOND_ITEM = 2;
    private static final int THIRD_ITEM = 3;
    private static final int FOURTH_ITEM = 4;
    private static final int FIFTH_ITEM = 5;
    private static final int SIXTH_ITEM = 6;
    private static final int SEVENTH_ITEM = 7;
    private static final String SLOT = "slot";
    private static final String SITE = "site";
    private static final String BRAND = "brand";
    private static final String AMOUNT = "amount";
    private static final String DATE = "parameter_date";
    private static final String PARAMETER_NAME = "parameter_name";
    private static final String PARAMETER_ID = "summary_parameter_id";
    private static final String SUMMARY_ID = "jackpot_summary_id";

    @Override
    public List<SummaryParameter> getParametersBySummaryId(long id) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("select * from Slots.jackpot_summary_parameters " +
                    " where jackpot_summary_id = ?");
            preparedStatement.setLong(FIRST_ITEM, id);
            resultSet = preparedStatement.executeQuery();
            List<SummaryParameter> parameters = new ArrayList<>();
            while (resultSet.next()){
                parameters.add(extractSummaryParameters(resultSet));
            }
            return parameters;
        } catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public List<SummaryParameter> getParametersBySummariesId(List<Long> idList) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            List<SummaryParameter> parameters = new ArrayList<>();
            for(Long id : idList) {
                preparedStatement = connection.prepareStatement("select * from Slots.jackpot_summary_parameters " +
                        " where jackpot_summary_id = ?");
                preparedStatement.setLong(FIRST_ITEM, id);
                resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    parameters.add(extractSummaryParameters(resultSet));
                }
            }
            return parameters;
        } catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public void saveParameters(List<SummaryParameter> parameters) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            for(SummaryParameter parameter : parameters) {
                preparedStatement = connection.prepareStatement("INSERT INTO Slots.jackpot_summary_parameters " +
                        " (jackpot_summary_id, site, brand, slot, amount, parameter_date, parameter_name) " +
                        " VALUES (?, ?, ?, ?, ?, ?, ?);");
                preparedStatement.setLong(FIRST_ITEM, parameter.getJackpotSummaryId());
                preparedStatement.setString(SECOND_ITEM, parameter.getSite());
                preparedStatement.setString(THIRD_ITEM, parameter.getBrand());
                preparedStatement.setString(FOURTH_ITEM, parameter.getSlot());
                preparedStatement.setDouble(FIFTH_ITEM, parameter.getAmount());
                preparedStatement.setDate(SIXTH_ITEM, parameter.getParameterDate());
                preparedStatement.setString(SEVENTH_ITEM, parameter.getParameterName());
                preparedStatement.executeUpdate();
            }
        } catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatement(connection, preparedStatement);
        }
    }

    private SummaryParameter extractSummaryParameters(ResultSet resultSet) throws SQLException{
        SummaryParameter parameter = new SummaryParameter();
        parameter.setSlot(resultSet.getString(SLOT));
        parameter.setBrand(resultSet.getString(BRAND));
        parameter.setSite(resultSet.getString(SITE));
        parameter.setParameterName(resultSet.getString(PARAMETER_NAME));
        parameter.setAmount(resultSet.getDouble(AMOUNT));
        parameter.setParameterDate(resultSet.getDate(DATE));
        parameter.setSummaryParameterId(resultSet.getLong(PARAMETER_ID));
        parameter.setJackpotSummaryId(resultSet.getLong(SUMMARY_ID));
        return parameter;
    }
}
