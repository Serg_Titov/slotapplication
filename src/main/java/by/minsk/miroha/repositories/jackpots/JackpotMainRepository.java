package by.minsk.miroha.repositories.jackpots;

import by.minsk.miroha.entities.front.JackpotPath;
import by.minsk.miroha.report.jackpot.GrownJackpots;
import by.minsk.miroha.report.jackpot.JackpotGraphPoint;
import by.minsk.miroha.report.jackpot.JackpotParameter;
import by.minsk.miroha.report.jackpot.JackpotSite;

import java.sql.Timestamp;
import java.sql.Date;
import java.util.List;
import java.util.Set;

public interface JackpotMainRepository {

    int countUniqueJackpots(String slotName);

    Timestamp getLastDate(String slotName);

    List<JackpotParameter> getMaxJackpotValueToday(String slotName, Date lastDate);

    List<JackpotParameter> getLastPaidOut(String slotName);

    JackpotParameter getMaxPaidOut(String slotName);

    List<JackpotParameter> getTopRTPSlots(String slotName, Date lastDate);

    Set<String> getJackpotSites(String slotName);

    List<GrownJackpots> getGrownJackpots(int daysCount, String slotName);

    List<JackpotGraphPoint> getGraphPoint(String slotName, String site, Timestamp dateStart, Timestamp dateEnd);

    List<JackpotPath> getJackpotPath(String siteName);

    List<JackpotSite> getAllJackpotsSites(String slotName);

    List<JackpotParameter> getFirstJackpotsEntry();

    List<JackpotParameter> getMaxPaidOutJackpots();
}
