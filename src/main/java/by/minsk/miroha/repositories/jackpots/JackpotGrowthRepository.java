package by.minsk.miroha.repositories.jackpots;

import by.minsk.miroha.entities.databases.JackpotGrowth;

import java.util.List;

public interface JackpotGrowthRepository {

    List<JackpotGrowth> getGrowthBySummaryId(long summaryId);

    List<JackpotGrowth> getGrowthBySummaryIdList(List<Long> summaryIdList);

    void save(List<JackpotGrowth> jackpotGrowth);
}
