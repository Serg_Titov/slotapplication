package by.minsk.miroha.repositories.jackpots;

import by.minsk.miroha.report.jackpot.JackpotParameter;

import java.util.List;

public interface JackpotSiteRepository {

    Double getCurrentJackpot(String slot, String site);

    Double getMaxPayedOutJackpot(String slot, String site);

    Double getMinPayedOutJackpot(String slot, String site);

    List<JackpotParameter> getFullInformation(String slot, String site);

}
