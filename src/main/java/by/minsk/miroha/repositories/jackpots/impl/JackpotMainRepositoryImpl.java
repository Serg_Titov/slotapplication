package by.minsk.miroha.repositories.jackpots.impl;

import by.minsk.miroha.entities.common.Site;
import by.minsk.miroha.entities.front.JackpotPath;
import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.report.jackpot.GrownJackpots;
import by.minsk.miroha.report.jackpot.JackpotGraphPoint;
import by.minsk.miroha.report.jackpot.JackpotParameter;
import by.minsk.miroha.report.jackpot.JackpotSite;
import by.minsk.miroha.repositories.jackpots.JackpotMainRepository;
import by.minsk.miroha.utils.DatabaseUtils;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static by.minsk.miroha.repositories.report.impl.CommonSimInfoRepositoryImpl.DB_ERROR;

@Repository
public class JackpotMainRepositoryImpl implements JackpotMainRepository {

    private static final int FIRST_ITEM = 1;
    private static final int SECOND_ITEM = 2;
    private static final int THIRD_ITEM = 3;
    private static final int FOURTH_ITEM = 4;
    private static final String SITE = "site";
    private static final String BRAND = "brand";
    private static final String SLOT = "slot";
    private static final String TIME_CHECK = "time_check";
    private static final String JACKPOT = "jackpot";
    private static final String ID = "id";
    private static final String LAST_PAYED_OUT = "last_payedout";

    @Override
    public int countUniqueJackpots(String slotName) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("select count(distinct(st.site)) from jackpot_statistic.statistic st " +
                        " where st.slot = ? and st.time_check >= current_date ");
            preparedStatement.setString(FIRST_ITEM, slotName);
            resultSet = preparedStatement.executeQuery();
            return DatabaseUtils.getParameterFromResultSet(resultSet);
        } catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }

    }

    @Override
    public Timestamp getLastDate(String slotName) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            if(slotName != null) {
                preparedStatement = connection.prepareStatement("select st.id, st.time_check from jackpot_statistic.statistic  st" +
                        " where st.slot = ? and st.id = (select max(s.id) from jackpot_statistic.statistic s where s.slot = ?)");
                preparedStatement.setString(FIRST_ITEM, slotName);
                preparedStatement.setString(SECOND_ITEM, slotName);
            } else {
                preparedStatement = connection.prepareStatement("select st.id, st.time_check from jackpot_statistic.statistic  st" +
                        " where st.id = (select max(s.id) from jackpot_statistic.statistic s )");
            }
            resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                 return resultSet.getTimestamp(TIME_CHECK);
            }
            return null;
        }catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public List<JackpotParameter> getMaxJackpotValueToday(String slotName, Date lastDate) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("select site, brand, time_check, jackpot " +
                    " from jackpot_statistic.statistic " +
                    " where id IN (select max(st.id) from jackpot_statistic.statistic st " +
                        " where st.slot = ? and st.time_check >= ? " +
                        " GROUP BY site, brand ) ");
            preparedStatement.setString(FIRST_ITEM, slotName);
            preparedStatement.setDate(SECOND_ITEM, lastDate);
            resultSet = preparedStatement.executeQuery();
            List<JackpotParameter> parameters = new ArrayList<>();
            while (resultSet.next()){
                parameters.add(getParameterFromResultSet(resultSet, slotName));
            }
            return parameters;
        } catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public List<JackpotParameter> getLastPaidOut(String slotName) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("select site, brand, jackpot, last_payedout, time_check " +
                    " from jackpot_statistic.statistic where slot = ? " +
                    " ORDER BY site, id DESC ");
            preparedStatement.setString(FIRST_ITEM, slotName);
            resultSet = preparedStatement.executeQuery();
            List<JackpotParameter> parameters = new ArrayList<>();
            while (resultSet.next()){
                parameters.add(getFullParameterFromResultSet(resultSet));
            }
            return parameters;
        } catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public JackpotParameter getMaxPaidOut(String slotName) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("select site, brand, time_check, last_payedout " +
                    "from jackpot_statistic.statistic where slot = ? " +
                    " and last_payedout = (select max(st.last_payedout) from jackpot_statistic.statistic st " +
                    " where st.slot = ?  )");
            preparedStatement.setString(FIRST_ITEM, slotName);
            preparedStatement.setString(SECOND_ITEM, slotName);
            resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                return getParameterFromResultSet(resultSet, slotName);
            }
            return null;
        } catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public List<JackpotParameter> getTopRTPSlots(String slotName, Date lastDate) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement(getQueryForRtpTable(slotName));
            preparedStatement.setDate(FIRST_ITEM, lastDate);
            if(slotName != null) {
                preparedStatement.setString(SECOND_ITEM, slotName);
            }
            resultSet = preparedStatement.executeQuery();
            List<JackpotParameter> parameters = new ArrayList<>();
            while (resultSet.next()){
                parameters.add(getRTPParameterFromResultSet(resultSet));
            }
            return parameters;
        } catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public Set<String> getJackpotSites(String slotName) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("select site from jackpot_statistic.statistic " +
                    " where slot = ? and time_check >= SUBDATE(current_date,1) " +
                    " GROUP BY site");
            preparedStatement.setString(FIRST_ITEM, slotName);
            resultSet = preparedStatement.executeQuery();
            Set<String> sites = new HashSet<>();
            while (resultSet.next()){
                sites.add(resultSet.getString(SITE));
            }
            return sites;
        } catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public List<GrownJackpots> getGrownJackpots(int daysCount, String slotName) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("select ff.brand as brand, ff.site as site," +
                    " ff.id as id, ff.jackpot as jackpot from jackpot_statistic.statistic ff " +
                    "where ff.id IN (select min(f.id) as id" +
                    " from jackpot_statistic.statistic f where f.slot = ? " +
                    " and f.time_check >= SUBDATE(current_date, " + daysCount + ") " +
                    " group by f.brand, f.site )" +
                    " union " +
                    " select ss.brand as brand, ss.site as site, ss.id as id, ss.jackpot as jackpot " +
                    " from jackpot_statistic.statistic ss where ss.id IN (select max(s.id) as id " +
                    " from jackpot_statistic.statistic s where s.slot = ? " +
                    " and s.time_check < current_date " +
                    " group by brand, site )");
            preparedStatement.setString(FIRST_ITEM, slotName);
            preparedStatement.setString(SECOND_ITEM, slotName);
            resultSet = preparedStatement.executeQuery();
            List<GrownJackpots> parameterList = new ArrayList<>();
            while (resultSet.next()){
                parameterList.add(getGrownFromResultSet(resultSet, slotName));
            }
            return parameterList;
        } catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public List<JackpotGraphPoint> getGraphPoint(String slotName, String site, Timestamp dateStart, Timestamp dateEnd) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            String sqlStart = "";
            String sqlEnd = "";
            if(dateStart != null){
                sqlStart = " and time_check >= '" + dateStart + "' ";
            }
            if(dateEnd != null){
                sqlEnd = " and time_check <= '" + dateEnd + "' ";
            }
            preparedStatement = connection.prepareStatement("select jackpot, last_payedout, time_check " +
                    " from jackpot_statistic.statistic " +
                    " where slot = ? and site = ? " + sqlStart + sqlEnd +
                    " ORDER BY id ");
            preparedStatement.setString(FIRST_ITEM, slotName);
            preparedStatement.setString(SECOND_ITEM, site);
            resultSet = preparedStatement.executeQuery();
            List<JackpotGraphPoint> points = new ArrayList<>();
            while (resultSet.next()){
                points.add(fillPointsFromResultSet(resultSet, site));
            }
            return points;
        } catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public List<JackpotPath> getJackpotPath(String siteName) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            String sqlQuery = "select distinct slot, site from jackpot_statistic.statistic " +
                    " where site like '%" + siteName + "%' ";
            preparedStatement = connection.prepareStatement(sqlQuery);
            resultSet = preparedStatement.executeQuery();
            List<JackpotPath> paths = new ArrayList<>();
            while (resultSet.next()){
                JackpotPath path = fillJackpotPath(resultSet, siteName);
                if (path != null) {
                    paths.add(path);
                }
            }
            return paths;
        } catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public List<JackpotSite> getAllJackpotsSites(String slotName) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("select distinct site, brand " +
                    " from jackpot_statistic.statistic where slot = ? ");
            preparedStatement.setString(FIRST_ITEM, slotName);
            resultSet = preparedStatement.executeQuery();
            List<JackpotSite> jackpotSites = new ArrayList<>();
            while (resultSet.next()){
                jackpotSites.add(extractJackpotSite(slotName, resultSet));
            }
            return jackpotSites;
        } catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public List<JackpotParameter> getFirstJackpotsEntry() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement(getQueryForFirstEntry());
            resultSet = preparedStatement.executeQuery();
            List<JackpotParameter> parameters = new ArrayList<>();
            while (resultSet.next()){
                parameters.add(extractFirstJackpotEntry(resultSet));
            }
            return parameters;
        } catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public List<JackpotParameter> getMaxPaidOutJackpots() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("select site, brand, slot, max(last_payedout) jackpot " +
                    " from jackpot_statistic.statistic " +
                    " GROUP BY site, brand, slot " +
                    " ORDER BY jackpot DESC LIMIT 5");
            resultSet = preparedStatement.executeQuery();
            List<JackpotParameter> parameters = new ArrayList<>();
            while (resultSet.next()){
                parameters.add(getRTPParameterFromResultSet(resultSet));
            }
            return parameters;
        } catch (SQLException | ClassNotFoundException throwable) {
        throwable.printStackTrace();
        throw new CustomValidation(DB_ERROR);
        }
        finally {
        DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    private JackpotParameter getParameterFromResultSet(ResultSet resultSet, String slotName) throws SQLException{
        JackpotParameter parameter = new JackpotParameter();
        parameter.setSite(new Site(resultSet.getString(SITE)));
        parameter.setBrand(resultSet.getString(BRAND));
        parameter.setDate(resultSet.getTimestamp(TIME_CHECK));
        parameter.setValueFirst(resultSet.getDouble(FOURTH_ITEM));
        parameter.setSlotName(slotName);
        return parameter;
    }

    private JackpotParameter getFullParameterFromResultSet(ResultSet resultSet) throws SQLException{
        JackpotParameter parameter = new JackpotParameter();
        parameter.setSite(new Site(resultSet.getString(SITE)));
        parameter.setBrand(resultSet.getString(BRAND));
        parameter.setValueFirst(resultSet.getDouble(THIRD_ITEM));
        parameter.setValueSecond(resultSet.getDouble(FOURTH_ITEM));
        parameter.setDate(resultSet.getTimestamp(TIME_CHECK));
        return parameter;
    }

    private JackpotParameter getRTPParameterFromResultSet(ResultSet resultSet) throws SQLException{
        JackpotParameter parameter = new JackpotParameter();
        parameter.setSite(new Site(resultSet.getString(SITE)));
        parameter.setBrand(resultSet.getString(BRAND));
        parameter.setValueFirst(resultSet.getDouble(JACKPOT));
        parameter.setSlotName(resultSet.getString(SLOT));
        return parameter;
    }

    private GrownJackpots getGrownFromResultSet(ResultSet resultSet, String slotName) throws SQLException{
        GrownJackpots grownJackpots = new GrownJackpots();
        grownJackpots.setSlotName(slotName);
        grownJackpots.setSite(resultSet.getString(SITE));
        grownJackpots.setBrand(resultSet.getString(BRAND));
        grownJackpots.setGrownValue(resultSet.getDouble(JACKPOT));
        grownJackpots.setId(resultSet.getInt(ID));
        return grownJackpots;
    }

    private JackpotGraphPoint fillPointsFromResultSet(ResultSet resultSet, String site) throws SQLException{
        JackpotGraphPoint point = new JackpotGraphPoint();
        point.setSite(site);
        point.setJackpotValue(resultSet.getDouble(JACKPOT));
        point.setTimestamp(resultSet.getTimestamp(TIME_CHECK));
        point.setLastPayedOut(resultSet.getDouble(LAST_PAYED_OUT));
        return point;
    }

    private JackpotPath fillJackpotPath(ResultSet resultSet, String siteName) throws SQLException{
        Site siteDB = new Site(resultSet.getString(SITE));
        if(siteDB.getShortUrl().equals(siteName)) {
            JackpotPath path = new JackpotPath();
            path.setSlotName(resultSet.getString(SLOT));
            path.setJackpotSite(siteDB);
            return path;
        } else {
            return null;
        }
    }

    private JackpotSite extractJackpotSite(String slotName, ResultSet resultSet) throws SQLException{
        JackpotSite jackpotSite = new JackpotSite();
        jackpotSite.setSlot(slotName);
        jackpotSite.setSite(resultSet.getString(SITE));
        jackpotSite.setBrand(resultSet.getString(BRAND));
        return jackpotSite;
    }

    private JackpotParameter extractFirstJackpotEntry(ResultSet resultSet) throws SQLException{
        JackpotParameter parameter = new JackpotParameter();
        parameter.setSite(new Site(resultSet.getString(SITE)));
        parameter.setBrand(resultSet.getString(BRAND));
        parameter.setSlotName(resultSet.getString(SLOT));
        parameter.setDate(resultSet.getTimestamp(TIME_CHECK));
        parameter.setValueFirst(resultSet.getDouble(JACKPOT));
        return parameter;
    }

    private String getQueryForRtpTable(String slot){
        if(slot == null){
            return "select site, brand, slot, max(jackpot) jackpot " +
                    " from jackpot_statistic.statistic " +
                    " where id IN (select max(st.id) from jackpot_statistic.statistic st " +
                    " where st.time_check >= ? and slot <> 'Dr Fortuno' " +
                    " GROUP BY slot, site, brand )" +
                    " GROUP BY site, brand, slot " +
                    " ORDER BY jackpot DESC";
        } else {
            return "select site, brand, slot, max(jackpot) jackpot " +
                    " from jackpot_statistic.statistic " +
                    " where id IN (select max(st.id) from jackpot_statistic.statistic st " +
                    " where st.time_check >= ? and st.slot = ? " +
                    " GROUP BY site, brand )" +
                    " GROUP BY site, brand, slot " +
                    " ORDER BY jackpot DESC";
        }
    }

    private String getQueryForFirstEntry(){
        return "select site, brand, slot, time_check, jackpot " +
                " from jackpot_statistic.statistic " +
                " where id IN (select min(st.id) from jackpot_statistic.statistic st " +
                " GROUP BY site, brand )" +
                " ORDER BY time_check DESC" +
                " LIMIT 5";
    }
}
