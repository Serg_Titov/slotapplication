package by.minsk.miroha.repositories.jackpots;

import by.minsk.miroha.entities.databases.SummaryParameter;

import java.util.List;

public interface JackpotSummaryParameterRepository {

    List<SummaryParameter> getParametersBySummaryId(long id);

    List<SummaryParameter> getParametersBySummariesId(List<Long> idList);

    void saveParameters(List<SummaryParameter> parameters);
}
