package by.minsk.miroha.repositories.report;

import by.minsk.miroha.entities.report.DepositMultiplier;

import java.util.List;

public interface DepositMultiplierRepository {

    List<DepositMultiplier> findBySlotInput(long idSlotInput);

    void save(DepositMultiplier depositMultiplier);

    void update(DepositMultiplier depositMultiplier);

    void delete(long slotInputId);

    void truncate();
}
