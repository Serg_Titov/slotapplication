package by.minsk.miroha.repositories.report.impl;

import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.report.CasinoReport;
import by.minsk.miroha.repositories.report.CasinoReportRepository;
import by.minsk.miroha.utils.DatabaseUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static by.minsk.miroha.repositories.report.impl.CommonSimInfoRepositoryImpl.DB_ERROR;

public class CasinoReportRepositoryImpl implements CasinoReportRepository {

    private static final int FIRST_ITEM = 1;
    private static final int SECOND_ITEM = 2;
    private static final int THIRD_ITEM = 3;
    private static final int FOURTH_ITEM = 4;
    private static final int FIFTH_ITEM = 5;
    private static final int SIXTH_ITEM = 6;
    private static final int SEVENTH_ITEM = 7;
    private static final int EIGHTH_ITEM = 8;
    private static final int NINTH_ITEM = 9;
    private static final int TENTH_ITEM = 10;
    private static final int ELEVENTH_ITEM = 11;

    @Override
    public List<CasinoReport> getCasinoReports(int limit, int offset, String sortField, String sortType,
                                               List<String> providers, List<String> slots) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try{
            connection = DatabaseUtils.getDatabasesConnection();
            StringBuilder providerFilter = new StringBuilder();
            StringBuilder slotFilter = new StringBuilder();
            boolean where = true;
            if(providers != null){
                for(String provider : providers){
                    if(where){
                        providerFilter = new StringBuilder(" where ( si.provider = '" + provider + "' ");
                        where = false;
                    } else {
                        providerFilter.append(" or ").append(" si.provider = '").append(provider).append("' ");
                    }
                }
                if(providers.size() > 0){
                    providerFilter.append(" ) ");
                }
            }
            if(slots != null){
                if(!where && slots.size() > 0){
                    slotFilter.append(" and ");
                }
                for(String slot : slots){
                    if(where){
                        slotFilter = new StringBuilder(" where ( si.slot_name = '" + slot + "' ");
                        where = false;
                    } else {
                        if(slotFilter.toString().equals(" and ")){
                            slotFilter.append(" ( si.slot_name = '").append(slot).append("' ");
                        } else {
                            slotFilter.append(" or ").append(" si.slot_name = '").append(slot).append("' ");
                        }
                    }
                }
                if(slots.size() > 0){
                    slotFilter.append(" ) ");
                }
            }
            preparedStatement = connection.prepareStatement("select si.id_info, si.provider provider, si.slot_name slot, " +
                    " case when '" + sortField + "' = 'rtp' and '" + sortType + "' = 'ASC' and ISNULL(csi.rtp) then 9999 " +
                    " else csi.rtp end rtp, " +
                    " case when '" + sortField + "' = 'volatility' and '" + sortType + "' = 'ASC' and ISNULL(csi.volatility) then 999999 " +
                    " else csi.volatility end volatility, " +
                    " case when '" + sortField + "' = 'x100' and '" + sortType + "' = 'ASC' and ISNULL(x100.multiplier_freq) then 999999999.9 " +
                    " else x100.multiplier_freq end x100, " +
                    " case when '" + sortField + "' = 'x1000' and '" + sortType + "' = 'ASC' and ISNULL(x1000.multiplier_freq) then 999999999.9 " +
                    " else x1000.multiplier_freq end x1000, " +
                    " case when '" + sortField + "' = 'x5000' and '" + sortType + "' = 'ASC' and ISNULL(x5000.multiplier_freq) then 999999999.9 " +
                    " else x5000.multiplier_freq end x5000, " +
                    " case when '" + sortField + "' = 'x10000' and '" + sortType + "' = 'ASC' and ISNULL(x10000.multiplier_freq) then 999999999.9 " +
                    " else x10000.multiplier_freq end x10000, " +
                    " case when '" + sortField + "' = 'x20000' and '" + sortType + "' = 'ASC' and ISNULL(x20000.multiplier_freq) then 999999999.9 " +
                    " else x20000.multiplier_freq end x20000, " +
                    " case when '" + sortField + "' = 'x50000' and '" + sortType + "' = 'ASC' and ISNULL(x50000.multiplier_freq) then 999999999.9 " +
                    " else x50000.multiplier_freq end x50000 from Slots.slot_information si " +
                    " left join Slots.common_slot_information csi on si.id_info = csi.game_id " +
                    " left join Slots.database_multipliers x100 on si.id_info = x100.game_id and x100.multiplier = 100 " +
                    " left join Slots.database_multipliers x1000 on si.id_info = x1000.game_id and x1000.multiplier = 1000 " +
                    " left join Slots.database_multipliers x5000 on si.id_info = x5000.game_id and x5000.multiplier = 5000 " +
                    " left join Slots.database_multipliers x10000 on si.id_info = x10000.game_id and x10000.multiplier = 10000 " +
                    " left join Slots.database_multipliers x20000 on si.id_info = x20000.game_id and x20000.multiplier = 20000 " +
                    " left join Slots.database_multipliers x50000 on si.id_info = x50000.game_id and x50000.multiplier = 50000 " +
                    providerFilter + slotFilter + " order by " + sortField + " "+ sortType + " limit ?, ?");
            preparedStatement.setInt(FIRST_ITEM, limit*offset);
            preparedStatement.setInt(SECOND_ITEM, limit);
            resultSet = preparedStatement.executeQuery();
            List<CasinoReport> casinoReportList = new ArrayList<>();
            while (resultSet.next()){
                casinoReportList.add(fillReport(resultSet));
            }
            return casinoReportList;
        } catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    private CasinoReport fillReport(ResultSet resultSet) throws SQLException {
        CasinoReport report = new CasinoReport();
        report.setSlotId(resultSet.getInt(FIRST_ITEM));
        report.setProvider(resultSet.getString(SECOND_ITEM));
        report.setSlot(resultSet.getString(THIRD_ITEM));
        report.setRtp(resultSet.getDouble(FOURTH_ITEM));
        if(report.getRtp() == 9999){
            report.setRtp(0);
        }
        report.setVolatility(resultSet.getDouble(FIFTH_ITEM));
        if(report.getVolatility() == 999999){
            report.setVolatility(0);
        }
        report.setX100(resultSet.getDouble(SIXTH_ITEM));
        if(report.getX100() == 999999999.9){
            report.setX100(0);
        }
        report.setX1000(resultSet.getDouble(SEVENTH_ITEM));
        if(report.getX1000() == 999999999.9){
            report.setX1000(0);
        }
        report.setX5000(resultSet.getDouble(EIGHTH_ITEM));
        if(report.getX5000() == 999999999.9){
            report.setX5000(0);
        }
        report.setX10000(resultSet.getDouble(NINTH_ITEM));
        if(report.getX10000() == 999999999.9){
            report.setX10000(0);
        }
        report.setX20000(resultSet.getDouble(TENTH_ITEM));
        if(report.getX20000() == 999999999.9){
            report.setX20000(0);
        }
        report.setX50000(resultSet.getDouble(ELEVENTH_ITEM));
        if(report.getX50000() == 999999999.9){
            report.setX50000(0);
        }
        return report;
    }
}
