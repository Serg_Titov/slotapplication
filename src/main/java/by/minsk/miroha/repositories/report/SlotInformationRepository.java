package by.minsk.miroha.repositories.report;

import by.minsk.miroha.entities.report.SlotInformation;

import java.util.List;

public interface SlotInformationRepository {

    SlotInformation getObject(String name, String provider);

    SlotInformation getObjectById(int objectId);

    SlotInformation save(SlotInformation slotInformation);

    SlotInformation update(SlotInformation slotInformation);

    void delete(int infoId);

    void truncate();

    List<SlotInformation> getSubsetOfSlotInformation(int limit, int offset);

    int countAll();

    List<SlotInformation> getAll();
}
