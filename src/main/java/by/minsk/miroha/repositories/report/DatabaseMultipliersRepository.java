package by.minsk.miroha.repositories.report;

import java.util.List;

import by.minsk.miroha.entities.report.Multipliers;
import by.minsk.miroha.entities.report.DatabaseMultipliers;

public interface DatabaseMultipliersRepository  {

    List<Multipliers> findByGame(int gameId);

    List<DatabaseMultipliers> findListMultipliersByGames(List<Integer> gamesId);

    Multipliers findByID(long id);

    void save(DatabaseMultipliers multipliers);

    void update(DatabaseMultipliers multipliers);

    void deleteBySLotInfoId(int gameId);

    void truncate();
}
