package by.minsk.miroha.repositories.report;

import java.util.List;

import by.minsk.miroha.entities.report.TopProfits;

public interface TopProfitsRepository {

    List<TopProfits> findBySlotInput(long idSlotInput);

    void save(TopProfits topProfits);

    void update(TopProfits topProfits);

    void delete(long slotInputId);

    void truncate();
}
