package by.minsk.miroha.repositories.report;

import by.minsk.miroha.entities.report.CommonSimInformation;

public interface CommonSimInfoRepository {

    CommonSimInformation getObject(long idSlotInput);

    CommonSimInformation save(CommonSimInformation object);

    CommonSimInformation update(CommonSimInformation object);

    void delete(long slotInputId);

    void truncate();
}
