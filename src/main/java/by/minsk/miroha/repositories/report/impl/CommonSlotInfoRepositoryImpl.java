package by.minsk.miroha.repositories.report.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import by.minsk.miroha.entities.report.CommonSlotInformation;
import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.repositories.report.CommonSlotInfoRepository;
import by.minsk.miroha.utils.DatabaseUtils;
import org.springframework.stereotype.Repository;

@Repository
public class CommonSlotInfoRepositoryImpl implements CommonSlotInfoRepository {

    public static final String DB_ERROR = "Не удалось подключиться к БД";
    public static final String ID = "id_common_info";
    public static final String SPINS_AMOUNT = "amount_spins";
    public static final String MAX_WIN = "max_win";
    public static final String RTP = "rtp";
    public static final String VOLATILITY = "volatility";
    public static final String BONUS_FREQUENCY = "bonus_freq";
    public static final String WINS_BONUS_RATE = "wins_bonus_rate";
    public static final String WINS_WITHOUT_BONUS_RATE = "wins_without_bonus_rate";
    public static final String GAME_ID = "game_id";
    public static final String AVERAGE_BONUS_WIN = "avg_bonus_win";

    private static final int FIRST_ITEM = 1;
    private static final int SECOND_ITEM = 2;
    private static final int THIRD_ITEM = 3;
    private static final int FOURTH_ITEM = 4;
    private static final int FIFTH_ITEM = 5;
    private static final int SIXTH_ITEM = 6;
    private static final int SEVENTH_ITEM = 7;
    private static final int EIGHTH_ITEM = 8;
    private static final int NINTH_ITEM = 9;
    private static final int TENTH_ITEM = 10;

    @Override
    public CommonSlotInformation getObject(int gameId) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("select * from Slots.common_slot_information where game_id = ?");
            preparedStatement.setInt(FIRST_ITEM, gameId);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                return fillCommonSlotInformation(resultSet);
            }
            return null;
        }
        catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public CommonSlotInformation getObjectById(int commonSlotInfoId) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("select * from Slots.common_slot_information where id_common_info = ?");
            preparedStatement.setInt(FIRST_ITEM, commonSlotInfoId);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                return fillCommonSlotInformation(resultSet);
            }
            return null;
        }
        catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public List<CommonSlotInformation> getListObjectById(List<Integer> idList) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            List<CommonSlotInformation> commonSlotInformationList = new ArrayList<>();
            for(Integer id : idList){
                preparedStatement = connection.prepareStatement("select * from Slots.common_slot_information where id_common_info = ?");
                preparedStatement.setInt(FIRST_ITEM, id);
                resultSet = preparedStatement.executeQuery();
                if (resultSet.next()){
                    commonSlotInformationList.add(fillCommonSlotInformation(resultSet));
                }
            }
            return commonSlotInformationList;
        } catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public CommonSlotInformation save(CommonSlotInformation object) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            int nextId = findMaxId(connection);
            nextId++;
            preparedStatement = connection.prepareStatement("INSERT INTO Slots.common_slot_information" +
                " (amount_spins, max_win, rtp, volatility, bonus_freq, wins_bonus_rate, " +
                "wins_without_bonus_rate, avg_bonus_win, game_id, id_common_info) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");
            fillPreparedStatementBySlotInfo(preparedStatement, object);
            preparedStatement.setInt(TENTH_ITEM, nextId);
            preparedStatement.executeUpdate();
            return getObject(object.getGameId());
        }
        catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatement(connection, preparedStatement);
        }
    }

    @Override
    public CommonSlotInformation update(CommonSlotInformation object) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("UPDATE Slots.common_slot_information" +
                " SET amount_spins = ?, max_win = ?, rtp = ?, volatility = ?, bonus_freq = ?," +
                "wins_bonus_rate = ?, wins_without_bonus_rate = ?, avg_bonus_win = ?, game_id =? where game_id = ?;");
            fillPreparedStatementBySlotInfo(preparedStatement, object);
            preparedStatement.setLong(TENTH_ITEM, object.getGameId());
            preparedStatement.executeUpdate();
            return getObject(object.getGameId());
        }
        catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatement(connection, preparedStatement);
        }
    }

    @Override
    public void deleteBySlotInfoId(int gameId) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("Delete from Slots.common_slot_information " +
                    "where game_id = ?;");
            preparedStatement.setLong(FIRST_ITEM, gameId);
            preparedStatement.executeUpdate();
        }
        catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatement(connection, preparedStatement);
        }
    }

    @Override
    public void truncate() {
        DatabaseUtils.truncateTable("Slots.common_slot_information");
    }

    private int findMaxId(Connection connection) {
        return DatabaseUtils.getParameterFromQuery("select max(id_common_info) from Slots.common_slot_information", connection);
    }

    private CommonSlotInformation fillCommonSlotInformation (ResultSet resultSet) throws SQLException {
        CommonSlotInformation slotInformation = new CommonSlotInformation();
        slotInformation.setIdCommonInfo(resultSet.getInt(ID));
        slotInformation.setSpinAmount(resultSet.getLong(SPINS_AMOUNT));
        slotInformation.setMaxWin(resultSet.getDouble(MAX_WIN));
        slotInformation.setRtp(resultSet.getDouble(RTP));
        slotInformation.setVolatility(resultSet.getDouble(VOLATILITY));
        slotInformation.setBonusFrequency(resultSet.getDouble(BONUS_FREQUENCY));
        slotInformation.setBonusWinsRate(resultSet.getDouble(WINS_BONUS_RATE));
        slotInformation.setWinsWithoutBonusRate(resultSet.getDouble(WINS_WITHOUT_BONUS_RATE));
        slotInformation.setAverageBonusWin(resultSet.getDouble(AVERAGE_BONUS_WIN));
        slotInformation.setGameId(resultSet.getInt(GAME_ID));
        return slotInformation;
    }

    private void fillPreparedStatementBySlotInfo(PreparedStatement preparedStatement, CommonSlotInformation slotInformation) throws SQLException {
        preparedStatement.setLong(FIRST_ITEM, slotInformation.getSpinAmount());
        preparedStatement.setDouble(SECOND_ITEM, slotInformation.getMaxWin());
        preparedStatement.setDouble(THIRD_ITEM, slotInformation.getRtp());
        preparedStatement.setDouble(FOURTH_ITEM, slotInformation.getVolatility());
        preparedStatement.setDouble(FIFTH_ITEM, slotInformation.getBonusFrequency());
        preparedStatement.setDouble(SIXTH_ITEM, slotInformation.getBonusWinsRate());
        preparedStatement.setDouble(SEVENTH_ITEM, slotInformation.getWinsWithoutBonusRate());
        preparedStatement.setDouble(EIGHTH_ITEM, slotInformation.getAverageBonusWin());
        preparedStatement.setLong(NINTH_ITEM, slotInformation.getGameId());
    }
}
