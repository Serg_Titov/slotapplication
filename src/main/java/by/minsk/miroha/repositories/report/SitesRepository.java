package by.minsk.miroha.repositories.report;

import by.minsk.miroha.entities.databases.BrandPlusLicense;
import by.minsk.miroha.entities.databases.SiteChanging;
import by.minsk.miroha.entities.databases.SitePlusLicense;
import by.minsk.miroha.entities.report.CasinoSite;

import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

public interface SitesRepository {

    List<BrandPlusLicense> getAllBrands();

    Set<String> getLicenses();

    Set<String> getLicensesInsideBrand(String brand);

    List<SitePlusLicense> getSitesByBrand(String brand);

    List<CasinoSite> getCommonSitesInformation(String site);

    List<CasinoSite> getAllCasinoSites();

    CasinoSite getSiteByFields(String brand, String site);

    List<SiteChanging> getRemovedSites(Timestamp startDate, Timestamp endDate);

    List<SiteChanging> getNewSites(Timestamp startDate, Timestamp endDate);

    List<SitePlusLicense> getLicenseForJackpots(List<String> jackpots);

}
