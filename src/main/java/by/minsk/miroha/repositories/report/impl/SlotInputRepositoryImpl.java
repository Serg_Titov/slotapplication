package by.minsk.miroha.repositories.report.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import by.minsk.miroha.entities.SlotInput;
import by.minsk.miroha.entities.enums.UserGrants;
import by.minsk.miroha.entities.enums.WaggerType;
import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.repositories.report.SlotInputRepository;
import by.minsk.miroha.utils.DatabaseUtils;

public class SlotInputRepositoryImpl implements SlotInputRepository {

    private static final String DB_ERROR = "Не удалось подключиться к БД";
    private static final int FIRST_ITEM = 1;
    private static final int SECOND_ITEM = 2;
    private static final int THIRD_ITEM = 3;
    private static final int FOURTH_ITEM = 4;
    private static final int FIFTH_ITEM = 5;
    private static final int SIXTH_ITEM = 6;
    private static final int SEVENTH_ITEM = 7;
    private static final int EIGHTH_ITEM = 8;
    private static final int NINTH_ITEM = 9;
    private static final int TENTH_ITEM = 10;
    private static final int ELEVENTH_ITEM = 11;
    private static final int TWELFTH_ITEM = 12;
    private static final int THIRTEENTH_ITEM = 13;
    private static final int FOURTEENTH_ITEM = 14;
    private static final int FIFTEENTH_ITEM = 15;
    private static final int SIXTEENTH_ITEM = 16;
    private static final int SEVENTEENTH_ITEM = 17;

    private static final String ID_INPUT = "id_input";
    private static final String DEPOSIT = "deposit";
    private static final String BONUS = "bonus";
    private static final String WITHOUT_BONUS = "without_bonus";
    private static final String TAX = "tax";
    private static final String BET = "bet";
    private static final String MAX_PROFIT = "max_profit";
    private static final String WAGGER = "wagger";
    private static final String WAGGER_TYPE = "wagger_type";
    private static final String SIM = "sim";
    private static final String GAME = "game";
    private static final String VERSION = "version";
    private static final String BONUS_PARTS = "bonus_parts";
    private static final String PROVIDER_FEE = "provider_fee";
    private static final String PAYMENT_FEE = "payment_fee";
    private static final String PLATFORM_FEE = "platform_fee";
    private static final String SPIN_LIMIT = "spin_limit";

    @Override
    public SlotInput findById(long idSlotInput) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("select * from Slots.slot_input where id_input = ?");
            preparedStatement.setLong(FIRST_ITEM, idSlotInput);
            resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                return fillSlotInput(resultSet);
            }
            return null;
        }
        catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public List<SlotInput> findByAllFields(SlotInput slotInput) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("select * from Slots.slot_input where deposit = ? and" +
                " bonus = ? and without_bonus = ? and tax = ? and bet = ? and max_profit = ? and wagger = ? and " +
                    "wagger_type = ? and sim = ? and game = ? and bonus_parts = ? and provider_fee = ? " +
                    "and payment_fee = ? and platform_fee = ? and spin_limit = ? ");
            fillPrepareStatementForFindByAllFields(preparedStatement, slotInput);
            resultSet = preparedStatement.executeQuery();
            List<SlotInput> slotInputList = new ArrayList<>();
            while (resultSet.next()){
                slotInputList.add(fillSlotInput(resultSet));
            }
            return slotInputList;
        }
        catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public SlotInput save(SlotInput slotInput) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            int nextId = findMaxId(connection);
            nextId++;
            preparedStatement = connection.prepareStatement("INSERT INTO Slots.slot_input" +
                " (deposit, bonus, without_bonus, tax, bet, max_profit, wagger, wagger_type, sim, game, version, bonus_parts, " +
                    "provider_fee, payment_fee, platform_fee, spin_limit, id_input) " +
                    "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");
            fillPrepareStatementBySlotInput(preparedStatement, slotInput);
            preparedStatement.setLong(SEVENTEENTH_ITEM, nextId);
            preparedStatement.executeUpdate();
            return findById(nextId);
        }
        catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatement(connection, preparedStatement);
        }
    }

    @Override
    public SlotInput update(SlotInput slotInput) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("UPDATE Slots.slot_input" +
                " SET deposit = ?, bonus=?, without_bonus=?, tax=?, bet = ?, max_profit = ?," +
                    " wagger = ?, wagger_type = ?, sim = ?, game =?, version = ?, bonus_parts = ? " +
                    "provider_fee = ?, payment_fee = ?, platform_fee = ?, spin_limit = ? where id_input = ?;");
            fillPrepareStatementBySlotInput(preparedStatement, slotInput);
            preparedStatement.setLong(SEVENTEENTH_ITEM, slotInput.getIdInput());
            preparedStatement.executeUpdate();
            return findById(slotInput.getIdInput());
        }
        catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatement(connection, preparedStatement);
        }

    }

    @Override
    public void deleteById(long idSlotInput) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("delete from Slots.slot_input where id_input = ?");
            preparedStatement.setLong(FIRST_ITEM, idSlotInput);
            preparedStatement.executeUpdate();
        } catch (ClassNotFoundException | SQLException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        } finally {
            DatabaseUtils.closeConnectionPrepStatement(connection, preparedStatement);
        }
    }

    @Override
    public void truncate() {
        DatabaseUtils.truncateTable("Slots.slot_input");
    }

    private int findMaxId(Connection connection) {
        return DatabaseUtils.getParameterFromQuery("select max(id_input) from Slots.slot_input", connection);
    }

    private void fillPrepareStatementBySlotInput (PreparedStatement preparedStatement, SlotInput slotInput) throws SQLException {
        preparedStatement.setDouble(FIRST_ITEM, slotInput.getDeposit());
        preparedStatement.setDouble(SECOND_ITEM, slotInput.getBonus());
        preparedStatement.setInt(THIRD_ITEM, convertBoolean(slotInput.isWithoutBonus()));
        preparedStatement.setDouble(FOURTH_ITEM, slotInput.getTaxes());
        preparedStatement.setDouble(FIFTH_ITEM, slotInput.getBet());
        preparedStatement.setDouble(SIXTH_ITEM, slotInput.getProfitLimit());
        preparedStatement.setInt(SEVENTH_ITEM, slotInput.getWagger());
        preparedStatement.setString(EIGHTH_ITEM, slotInput.getWaggerType().getWaggerName());
        preparedStatement.setInt(NINTH_ITEM, slotInput.getSim());
        preparedStatement.setString(TENTH_ITEM, slotInput.getGame());
        preparedStatement.setString(ELEVENTH_ITEM, slotInput.getUserGrants().getGrants());
        preparedStatement.setInt(TWELFTH_ITEM, slotInput.getBonusParts());
        preparedStatement.setDouble(THIRTEENTH_ITEM, slotInput.getProviderFee());
        preparedStatement.setDouble(FOURTEENTH_ITEM, slotInput.getPaymentFee());
        preparedStatement.setDouble(FIFTEENTH_ITEM, slotInput.getPlatformFee());
        preparedStatement.setDouble(SIXTEENTH_ITEM, slotInput.getSpinLimit());
    }

    private void fillPrepareStatementForFindByAllFields(PreparedStatement preparedStatement, SlotInput slotInput) throws SQLException{
        preparedStatement.setDouble(FIRST_ITEM, slotInput.getDeposit());
        preparedStatement.setDouble(SECOND_ITEM, slotInput.getBonus());
        preparedStatement.setInt(THIRD_ITEM, convertBoolean(slotInput.isWithoutBonus()));
        preparedStatement.setDouble(FOURTH_ITEM, slotInput.getTaxes());
        preparedStatement.setDouble(FIFTH_ITEM, slotInput.getBet());
        preparedStatement.setDouble(SIXTH_ITEM, slotInput.getProfitLimit());
        preparedStatement.setInt(SEVENTH_ITEM, slotInput.getWagger());
        preparedStatement.setString(EIGHTH_ITEM, slotInput.getWaggerType().getWaggerName());
        preparedStatement.setInt(NINTH_ITEM, slotInput.getSim());
        preparedStatement.setString(TENTH_ITEM, slotInput.getGame());
        preparedStatement.setInt(ELEVENTH_ITEM, slotInput.getBonusParts());
        preparedStatement.setDouble(TWELFTH_ITEM, slotInput.getProviderFee());
        preparedStatement.setDouble(THIRTEENTH_ITEM, slotInput.getPaymentFee());
        preparedStatement.setDouble(FOURTEENTH_ITEM, slotInput.getPlatformFee());
        preparedStatement.setDouble(FIFTEENTH_ITEM, slotInput.getSpinLimit());
    }

    private SlotInput fillSlotInput (ResultSet resultSet) throws SQLException {
        SlotInput slotInput = new SlotInput();
        slotInput.setIdInput(resultSet.getLong(ID_INPUT));
        slotInput.setDeposit(resultSet.getDouble(DEPOSIT));
        slotInput.setBonus(resultSet.getDouble(BONUS));
        slotInput.setWithoutBonus(convertInt(resultSet.getInt(WITHOUT_BONUS)));
        slotInput.setTaxes(resultSet.getDouble(TAX));
        slotInput.setBet(resultSet.getDouble(BET));
        slotInput.setProfitLimit(resultSet.getDouble(MAX_PROFIT));
        slotInput.setWagger(resultSet.getInt(WAGGER));
        slotInput.setWaggerType(WaggerType.findByName(resultSet.getString(WAGGER_TYPE)));
        slotInput.setSim(resultSet.getInt(SIM));
        slotInput.setGame(resultSet.getString(GAME));
        slotInput.setUserGrants(UserGrants.findByName(resultSet.getString(VERSION)));
        slotInput.setBonusParts(resultSet.getInt(BONUS_PARTS));
        slotInput.setProviderFee(resultSet.getDouble(PROVIDER_FEE));
        slotInput.setPlatformFee(resultSet.getDouble(PLATFORM_FEE));
        slotInput.setPaymentFee(resultSet.getDouble(PAYMENT_FEE));
        slotInput.setSpinLimit(resultSet.getDouble(SPIN_LIMIT));
        return slotInput;
    }

    private boolean convertInt(int parameter){
        return parameter == 1;
    }

    private int convertBoolean(boolean parameter){
        return parameter ? 1 : 0;
    }
}
