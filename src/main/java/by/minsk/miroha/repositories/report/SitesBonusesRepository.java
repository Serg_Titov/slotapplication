package by.minsk.miroha.repositories.report;


import java.util.List;

public interface SitesBonusesRepository {

    List<Long> getSiteBonus(long siteId);

    void saveBonusLink(long idBonus, long idSite);
}
