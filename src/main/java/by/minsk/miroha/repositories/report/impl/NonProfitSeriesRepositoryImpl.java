package by.minsk.miroha.repositories.report.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import by.minsk.miroha.entities.report.NonProfitSeries;
import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.repositories.report.NonProfitSeriesRepository;
import by.minsk.miroha.utils.DatabaseUtils;

public class NonProfitSeriesRepositoryImpl implements NonProfitSeriesRepository {

    public static final String DB_ERROR = "Не удалось подключиться к БД";

    private static final int FIRST_ITEM = 1;
    private static final int SECOND_ITEM = 2;
    private static final int THIRD_ITEM = 3;
    private static final int FOURTH_ITEM = 4;

    public static final String ID = "id_series";
    public static final String NAME = "series_name";
    public static final String SERIES = "series";

    @Override
    public List<NonProfitSeries> findBySlotInput(long idSlotInput) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("select * from Slots.nonprofit_series where id_slot_input = ?");
            preparedStatement.setLong(FIRST_ITEM, idSlotInput);
            resultSet = preparedStatement.executeQuery();
            List<NonProfitSeries> nonProfitSeries = new ArrayList<>();
            while (resultSet.next()){
                nonProfitSeries.add(fillNonProfitSeries(resultSet));
            }
            return nonProfitSeries;
        }
        catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public void save(NonProfitSeries nonProfitSeries) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            int nextId = findMaxId(connection);
            nextId++;
            preparedStatement = connection.prepareStatement("INSERT INTO Slots.nonprofit_series" +
                " (series_name, series, id_slot_input, id_series) " +
                "VALUES (?, ?, ?, ?);");
            fillPreparedStatementByNonProfitSeries(preparedStatement, nonProfitSeries);
            preparedStatement.setLong(FOURTH_ITEM, nextId);
            preparedStatement.executeUpdate();
        }
        catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatement(connection, preparedStatement);
        }
    }

    @Override
    public void update(NonProfitSeries nonProfitSeries) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("UPDATE Slots.nonprofit_series" +
                " SET series_name = ?, series = ?, id_slot_input = ? where id_series = ?;");
            fillPreparedStatementByNonProfitSeries(preparedStatement, nonProfitSeries);
            preparedStatement.setLong(FOURTH_ITEM, nonProfitSeries.getIdSeries());
            preparedStatement.executeUpdate();
        }
        catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatement(connection, preparedStatement);
        }
    }

    @Override
    public void delete(long slotInputId) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("DELETE from Slots.nonprofit_series where id_slot_input = ?;");
            preparedStatement.setLong(FIRST_ITEM, slotInputId);
            preparedStatement.executeUpdate();
        }
        catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatement(connection, preparedStatement);
        }
    }

    @Override
    public void truncate() {
        DatabaseUtils.truncateTable("Slots.nonprofit_series");
    }

    private int findMaxId(Connection connection) {
        return DatabaseUtils.getParameterFromQuery("select max(id_series) from Slots.nonprofit_series", connection);
    }

    private NonProfitSeries fillNonProfitSeries(ResultSet resultSet) throws SQLException {
        NonProfitSeries nonProfitSeries = new NonProfitSeries();
        nonProfitSeries.setIdSeries(resultSet.getLong(ID));
        nonProfitSeries.setSeriesName(resultSet.getString(NAME));
        nonProfitSeries.setSeries(resultSet.getLong(SERIES));

        return nonProfitSeries;
    }

    private  void fillPreparedStatementByNonProfitSeries(PreparedStatement preparedStatement, NonProfitSeries nonProfitSeries) throws SQLException {
        preparedStatement.setString(FIRST_ITEM, nonProfitSeries.getSeriesName());
        preparedStatement.setLong(SECOND_ITEM, nonProfitSeries.getSeries());
        preparedStatement.setLong(THIRD_ITEM, nonProfitSeries.getSlotInput().getIdInput());
    }
}
