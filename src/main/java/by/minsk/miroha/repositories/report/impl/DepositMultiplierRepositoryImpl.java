package by.minsk.miroha.repositories.report.impl;

import by.minsk.miroha.entities.report.DepositMultiplier;
import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.repositories.report.DepositMultiplierRepository;
import by.minsk.miroha.utils.DatabaseUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DepositMultiplierRepositoryImpl implements DepositMultiplierRepository {

    public static final String DB_ERROR = "Не удалось подключиться к БД";
    private static final int FIRST_ITEM = 1;
    private static final int SECOND_ITEM = 2;
    private static final int THIRD_ITEM = 3;
    private static final int FOURTH_ITEM = 4;

    public static final String ID = "id_dep_multiplier";
    public static final String DEP_MULTIPLIER = "dep_multiplier";
    public static final String DEP_MULTIPLIER_COUNT = "dep_multiplier_freq";

    @Override
    public List<DepositMultiplier> findBySlotInput(long idSlotInput) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try{
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("select * from Slots.deposit_multipliers where id_slot_input = ?");
            preparedStatement.setLong(FIRST_ITEM, idSlotInput);
            resultSet = preparedStatement.executeQuery();
            List<DepositMultiplier> multipliers = new ArrayList<>();
            while (resultSet.next()){
                multipliers.add(fillMultipliers(resultSet));
            }
            return multipliers;
        }
        catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    public List<DepositMultiplier> findAll() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try{
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("select * from Slots.deposit_multipliers");
            resultSet = preparedStatement.executeQuery();
            List<DepositMultiplier> multipliers = new ArrayList<>();
            while (resultSet.next()){
                multipliers.add(fillMultipliers(resultSet));
            }
            return multipliers;
        }
        catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public void save(DepositMultiplier depositMultiplier) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            int nextId = findMaxId(connection);
            nextId++;
            preparedStatement = connection.prepareStatement("INSERT INTO Slots.deposit_multipliers" +
                    " (dep_multiplier, dep_multiplier_freq, id_slot_input, id_dep_multiplier) " +
                    "VALUES (?, ?, ?, ?);");
            fillPreparedStatementByDatabaseMultipliers(preparedStatement, depositMultiplier);
            preparedStatement.setLong(FOURTH_ITEM, nextId);
            preparedStatement.executeUpdate();
        }
        catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatement(connection, preparedStatement);
        }
    }

    @Override
    public void update(DepositMultiplier depositMultiplier) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("UPDATE Slots.deposit_multipliers" +
                    " SET dep_multiplier = ?, dep_multiplier_freq = ?, id_slot_input = ? " +
                    "where id_dep_multiplier = ?;");
            fillPreparedStatementByDatabaseMultipliers(preparedStatement, depositMultiplier);
            preparedStatement.setLong(FOURTH_ITEM, depositMultiplier.getDepositMultiplierId());
            preparedStatement.executeUpdate();
        }
        catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatement(connection, preparedStatement);
        }
    }

    @Override
    public void delete(long slotInputId) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("DELETE from Slots.deposit_multipliers " +
                    "where id_slot_input = ?;");
            preparedStatement.setLong(FIRST_ITEM, slotInputId);
            preparedStatement.executeUpdate();
        }
        catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatement(connection, preparedStatement);
        }
    }

    @Override
    public void truncate() {
        DatabaseUtils.truncateTable("Slots.deposit_multipliers");
    }

    private int findMaxId(Connection connection) {
        return DatabaseUtils.getParameterFromQuery("select max(id_dep_multiplier) from Slots.deposit_multipliers", connection);
    }

    private DepositMultiplier fillMultipliers(ResultSet resultSet) throws SQLException {
        DepositMultiplier multipliers = new DepositMultiplier();
        multipliers.setDepositMultiplierId(resultSet.getLong(ID));
        multipliers.setDepositMultiplier(resultSet.getInt(DEP_MULTIPLIER));
        multipliers.setDepositMultiplierFrequency(resultSet.getInt(DEP_MULTIPLIER_COUNT));
        return multipliers;
    }

    private void fillPreparedStatementByDatabaseMultipliers(PreparedStatement preparedStatement, DepositMultiplier multipliers) throws SQLException {
        preparedStatement.setInt(FIRST_ITEM, multipliers.getDepositMultiplier());
        preparedStatement.setDouble(SECOND_ITEM, multipliers.getDepositMultiplierFrequency());
        preparedStatement.setLong(THIRD_ITEM, multipliers.getSlotInput().getIdInput());
    }
}
