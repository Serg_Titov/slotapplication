package by.minsk.miroha.repositories.report.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import by.minsk.miroha.entities.report.CommonSimInformation;
import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.repositories.report.CommonSimInfoRepository;
import by.minsk.miroha.repositories.report.SlotInputRepository;
import by.minsk.miroha.utils.DatabaseUtils;

public class CommonSimInfoRepositoryImpl implements CommonSimInfoRepository {

    public static final String DB_ERROR = "Не удалось подключиться к БД";
    private static final int FIRST_ITEM = 1;
    private static final int SECOND_ITEM = 2;
    private static final int THIRD_ITEM = 3;
    private static final int FOURTH_ITEM = 4;
    private static final int FIFTH_ITEM = 5;
    private static final int SIXTH_ITEM = 6;
    private static final int SEVENTH_ITEM = 7;
    private static final int EIGHTH_ITEM = 8;
    private static final int NINTH_ITEM = 9;
    private static final int TENTH_ITEM = 10;
    private static final int ELEVENTH_ITEM = 11;
    private static final int TWELFTH_ITEM = 12;
    private static final int THIRTEEN_ITEM = 13;
    private static final int FOURTEEN_ITEM = 14;
    private static final int FIFTEEN_ITEM = 15;
    private static final int SIXTEEN_ITEM = 16;

    private static final String ID = "id_common_sim";
    private static final String PROFIT = "profit";
    private static final String EV = "ev";
    private static final String CASINO_EV = "casino_ev";
    private static final String ROI = "roi";
    private static final String CASINO_ROI = "casino_roi";
    private static final String BONUS_VOLATILITY = "bonus_volatility";
    public static final String PROFIT_PROBABILITY = "profit_prob";
    public static final String NON_PROFIT_PROBABILITY = "nonprofit_prob";
    private static final String LONGEST_PROFIT_SERIES = "longest_profit_series";
    private static final String LONGEST_PROFIT_FREQUENCY = "longest_profit_frequency";
    private static final String LONGEST_NON_PROFIT_SERIES = "longest_nonprofit_series";
    private static final String LONGEST_NON_PROFIT_FREQUENCY = "longest_nonprofit_frequency";
    private static final String AVERAGE_WIN = "average_win";
    private static final String BET_COUNT = "bet_count";
    private static final String ID_SLOT_INPUT = "id_slot_input";

    private static final SlotInputRepository repository = new SlotInputRepositoryImpl();

    @Override
    public CommonSimInformation getObject(long idSlotInput) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("select * from Slots.common_sim_info where id_slot_input = ?");
            preparedStatement.setLong(FIRST_ITEM, idSlotInput);
            resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                return fillCommonSimInformation(resultSet);
            }
            return null;
        } catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public CommonSimInformation save(CommonSimInformation object) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            int nextId = findMaxId(connection);
            nextId++;
            preparedStatement = connection.prepareStatement("INSERT INTO Slots.common_sim_info" +
                " (profit, ev, casino_ev, roi, casino_roi, bonus_volatility, profit_prob, nonprofit_prob, longest_profit_series, longest_profit_frequency, longest_nonprofit_series, " +
                "longest_nonprofit_frequency, average_win, bet_count, id_slot_input, id_common_sim) " +
                    "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");
            fillPreparedStatementByCommonSimInfo(preparedStatement, object);
            preparedStatement.setInt(SIXTEEN_ITEM, nextId);
            preparedStatement.executeUpdate();
            return getObject(object.getSlotInput().getIdInput());
        } catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatement(connection, preparedStatement);
        }
    }

    @Override
    public CommonSimInformation update(CommonSimInformation object) {
        Connection connection = null;
        PreparedStatement preparedStatement =null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("UPDATE Slots.common_sim_info" +
                " SET profit = ?, ev = ?, casino_ev = ? roi = ?, casino_roi = ?,  bonus_volatility = ?, " +
                    " profit_prob = ?, nonprofit_prob = ?, " +
                    " longest_profit_series = ?, longest_profit_frequency = ?, " +
                " longest_nonprofit_series = ?, longest_nonprofit_frequency =?, average_win = ?, " +
                    " bet_count = ?, id_slot_input = ? where id_slot_input = ?;");
            fillPreparedStatementByCommonSimInfo(preparedStatement, object);
            preparedStatement.setLong(SIXTEEN_ITEM, object.getSlotInput().getIdInput());
            preparedStatement.executeUpdate();
            return getObject(object.getSlotInput().getIdInput());
        }
        catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatement(connection, preparedStatement);
        }
    }

    @Override
    public void delete(long slotInputId) {
        Connection connection = null;
        PreparedStatement preparedStatement =null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("DELETE from Slots.common_sim_info " +
                    "where id_slot_input = ?;");
            preparedStatement.setLong(FIRST_ITEM, slotInputId);
            preparedStatement.executeUpdate();
        }
        catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatement(connection, preparedStatement);
        }
    }

    @Override
    public void truncate() {
        DatabaseUtils.truncateTable("Slots.common_sim_info");
    }

    private int findMaxId(Connection connection) {
        return DatabaseUtils.getParameterFromQuery("select max(id_common_sim) from Slots.common_sim_info", connection);
    }

    private void fillPreparedStatementByCommonSimInfo(PreparedStatement preparedStatement, CommonSimInformation commonSimInformation) throws SQLException {
        preparedStatement.setDouble(FIRST_ITEM, commonSimInformation.getProfit());
        preparedStatement.setDouble(SECOND_ITEM, commonSimInformation.getEv());
        preparedStatement.setDouble(THIRD_ITEM, commonSimInformation.getCasinoEv());
        preparedStatement.setDouble(FOURTH_ITEM, commonSimInformation.getRoi());
        preparedStatement.setDouble(FIFTH_ITEM, commonSimInformation.getCasinoRoi());
        preparedStatement.setDouble(SIXTH_ITEM, commonSimInformation.getBonusVolatility());
        preparedStatement.setDouble(SEVENTH_ITEM, commonSimInformation.getProfitProbability());
        preparedStatement.setDouble(EIGHTH_ITEM, commonSimInformation.getNonProfitProbability());
        preparedStatement.setInt(NINTH_ITEM, commonSimInformation.getLongestProfitSeries());
        preparedStatement.setDouble(TENTH_ITEM, commonSimInformation.getLongestProfitFrequency());
        preparedStatement.setInt(ELEVENTH_ITEM, commonSimInformation.getLongestNonProfitSeries());
        preparedStatement.setDouble(TWELFTH_ITEM, commonSimInformation.getLongestNonProfitFrequency());
        preparedStatement.setDouble(THIRTEEN_ITEM, commonSimInformation.getAverageWin());
        preparedStatement.setLong(FOURTEEN_ITEM, commonSimInformation.getBetCounter());
        preparedStatement.setLong(FIFTEEN_ITEM, commonSimInformation.getSlotInput().getIdInput());

    }

    private CommonSimInformation fillCommonSimInformation(ResultSet set) throws SQLException {
        CommonSimInformation commonSimInformation = new CommonSimInformation();
        commonSimInformation.setIdCommonSim(set.getInt(ID));
        commonSimInformation.setProfit(set.getDouble(PROFIT));
        commonSimInformation.setEv(set.getDouble(EV));
        commonSimInformation.setCasinoEv(set.getDouble(CASINO_EV));
        commonSimInformation.setRoi(set.getDouble(ROI));
        commonSimInformation.setCasinoRoi(set.getDouble(CASINO_ROI));
        commonSimInformation.setBonusVolatility(set.getDouble(BONUS_VOLATILITY));
        commonSimInformation.setProfitProbability(set.getDouble(PROFIT_PROBABILITY));
        commonSimInformation.setNonProfitProbability(set.getDouble(NON_PROFIT_PROBABILITY));
        commonSimInformation.setLongestProfitSeries(set.getInt(LONGEST_PROFIT_SERIES));
        commonSimInformation.setLongestProfitFrequency(set.getInt(LONGEST_PROFIT_FREQUENCY));
        commonSimInformation.setLongestNonProfitSeries(set.getInt(LONGEST_NON_PROFIT_SERIES));
        commonSimInformation.setLongestNonProfitFrequency(set.getInt(LONGEST_NON_PROFIT_FREQUENCY));
        commonSimInformation.setAverageWin(set.getDouble(AVERAGE_WIN));
        commonSimInformation.setBetCounter(set.getLong(BET_COUNT));
        commonSimInformation.setSlotInput(repository.findById(set.getLong(ID_SLOT_INPUT)));

        return commonSimInformation;
    }
}
