package by.minsk.miroha.repositories.report.impl;

import by.minsk.miroha.entities.report.SlotInformation;
import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.repositories.report.SlotInformationRepository;
import by.minsk.miroha.utils.DatabaseUtils;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static by.minsk.miroha.repositories.report.impl.CommonSlotInfoRepositoryImpl.DB_ERROR;

@Repository
public class SlotInformationRepositoryImpl implements SlotInformationRepository {

    private static final int FIRST_ITEM = 1;
    private static final int SECOND_ITEM = 2;
    private static final int THIRD_ITEM = 3;
    public static final String ID = "id_info";
    public static final String SLOT_NAME = "slot_name";
    public static final String PROVIDER = "provider";

    @Override
    public SlotInformation getObject(String name, String provider) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("select * from Slots.slot_information " +
                    "where slot_name = ? and provider = ?");
            preparedStatement.setString(FIRST_ITEM, name);
            preparedStatement.setString(SECOND_ITEM, provider);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return fillSlotInformation(resultSet);
            }
            return null;
        } catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        } finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public SlotInformation getObjectById(int objectId) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("select * from Slots.slot_information " +
                    "where id_info = ?");
            preparedStatement.setInt(FIRST_ITEM, objectId);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return fillSlotInformation(resultSet);
            }
            return null;
        } catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        } finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public SlotInformation save(SlotInformation object) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            int nextId = findMaxId(connection);
            nextId++;
            preparedStatement = connection.prepareStatement("INSERT INTO Slots.slot_information" +
                    " (slot_name, provider, id_info) VALUES (?, ?, ?);");
            fillPreparedStatementBySlotInfo(preparedStatement, object);
            preparedStatement.setInt(THIRD_ITEM, nextId);
            preparedStatement.executeUpdate();
            return getObject(object.getSlot().getRawSlot(), object.getProvider().getRawProvider());
        } catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        } finally {
            DatabaseUtils.closeConnectionPrepStatement(connection, preparedStatement);
        }
    }

    @Override
    public SlotInformation update(SlotInformation object) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("UPDATE Slots.slot_information" +
                    " SET slot_name = ?, provider = ? where id_info = ?;");
            fillPreparedStatementBySlotInfo(preparedStatement, object);
            preparedStatement.setLong(THIRD_ITEM, object.getIdInfo());
            preparedStatement.executeUpdate();
            return getObject(object.getSlot().getRawSlot(), object.getProvider().getRawProvider());
        }
        catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatement(connection, preparedStatement);
        }
    }

    @Override
    public void delete(int infoId) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("delete from Slots.slot_information where id_info = ?");
            preparedStatement.setLong(FIRST_ITEM, infoId);
            preparedStatement.executeUpdate();
        } catch (ClassNotFoundException | SQLException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        } finally {
            DatabaseUtils.closeConnectionPrepStatement(connection, preparedStatement);
        }
    }

    @Override
    public void truncate() {
        DatabaseUtils.truncateTable("Slots.slot_information");
    }

    @Override
    public List<SlotInformation> getSubsetOfSlotInformation(int limit, int offset) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("select * from Slots.slot_information order by provider limit ?, ?");
            preparedStatement.setInt(FIRST_ITEM, limit*offset);
            preparedStatement.setInt(SECOND_ITEM, limit);
            resultSet = preparedStatement.executeQuery();
            List<SlotInformation> slotInformationList = new ArrayList<>();
            while (resultSet.next()){
                slotInformationList.add(fillSlotInformation(resultSet));
            }
            return slotInformationList;
        } catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public int countAll() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("select count(*) from Slots.slot_information");
            resultSet = preparedStatement.executeQuery();
            return DatabaseUtils.getParameterFromResultSet(resultSet);
        }
        catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        } finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public List<SlotInformation> getAll() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("select * from Slots.slot_information ");
            resultSet = preparedStatement.executeQuery();
            List<SlotInformation> slotList = new ArrayList<>();
            while (resultSet.next()) {
                slotList.add(fillSlotInformation(resultSet));
            }
            return slotList;
        } catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        } finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    private int findMaxId(Connection connection) {
        return DatabaseUtils.getParameterFromQuery("select max(id_info) from Slots.slot_information", connection);
    }

    private SlotInformation fillSlotInformation(ResultSet resultSet) throws SQLException {
        SlotInformation slotInformation = new SlotInformation();
        slotInformation.setIdInfo(resultSet.getInt(ID));
        slotInformation.setSlot(resultSet.getString(SLOT_NAME));
        slotInformation.setProvider(resultSet.getString(PROVIDER));
        return slotInformation;
    }

    private void fillPreparedStatementBySlotInfo(PreparedStatement preparedStatement, SlotInformation slotInformation) throws SQLException {
        preparedStatement.setString(FIRST_ITEM, slotInformation.getSlot().getRawSlot());
        preparedStatement.setString(SECOND_ITEM, slotInformation.getProvider().getRawProvider());
    }
}