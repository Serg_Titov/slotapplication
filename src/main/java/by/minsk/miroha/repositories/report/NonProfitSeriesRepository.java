package by.minsk.miroha.repositories.report;

import java.util.List;

import by.minsk.miroha.entities.report.NonProfitSeries;

public interface NonProfitSeriesRepository {

    List<NonProfitSeries> findBySlotInput(long idSlotInput);

    void save(NonProfitSeries nonProfitSeries);

    void update(NonProfitSeries nonProfitSeries);

    void delete(long slotInputId);

    void truncate();
}
