package by.minsk.miroha.repositories.report;

import by.minsk.miroha.entities.SlotInput;

import java.util.List;


public interface SlotInputRepository {

    SlotInput findById(long idSlotInput);

    List<SlotInput> findByAllFields(SlotInput slotInput);

    SlotInput save(SlotInput slotInput);

    SlotInput update(SlotInput slotInput);

    void deleteById(long idSlotInput);

    void truncate();
}
