package by.minsk.miroha.repositories.report.impl;

import java.util.ArrayList;
import java.util.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import by.minsk.miroha.entities.report.Multipliers;
import by.minsk.miroha.entities.report.DatabaseMultipliers;
import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.repositories.report.DatabaseMultipliersRepository;
import by.minsk.miroha.utils.DatabaseUtils;
import org.springframework.stereotype.Repository;

@Repository
public class DatabaseMultiPliersRepositoryImpl implements DatabaseMultipliersRepository {

    public static final String DB_ERROR = "Не удалось подключиться к БД";
    private static final int FIRST_ITEM = 1;
    private static final int SECOND_ITEM = 2;
    private static final int THIRD_ITEM = 3;
    private static final int FOURTH_ITEM = 4;

    public static final String ID = "id_multiplier";
    public static final String MULTIPLIER = "multiplier";
    public static final String MULTIPLIER_COUNT = "multiplier_freq";
    public static final String GAME_ID = "game_id";

    @Override
    public List<Multipliers> findByGame(int gameId) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("select * from Slots.database_multipliers where game_id = ?");
            preparedStatement.setInt(FIRST_ITEM, gameId);
            resultSet = preparedStatement.executeQuery();
            List<Multipliers> multipliers = new ArrayList<>();
            while (resultSet.next()){
                multipliers.add(convertDatabaseMultipliers(fillMultipliers(resultSet)));
            }
            return multipliers;
        }
        catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public List<DatabaseMultipliers> findListMultipliersByGames(List<Integer> gamesId) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            List<DatabaseMultipliers> multipliers = new ArrayList<>();
            for (Integer gameId : gamesId){
                preparedStatement = connection.prepareStatement("select * from Slots.database_multipliers where game_id = ?");
                preparedStatement.setInt(FIRST_ITEM, gameId);
                resultSet = preparedStatement.executeQuery();
                while (resultSet.next()){
                    multipliers.add(fillMultipliers(resultSet));
                }
            }
            return multipliers;
        } catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public Multipliers findByID(long id) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("select * from Slots.database_multipliers where id_multiplier = ?");
            preparedStatement.setLong(FIRST_ITEM, id);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                return convertDatabaseMultipliers(fillMultipliers(resultSet));
            }
            return null;
        }
        catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public void save(DatabaseMultipliers multipliers) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            int nextId = findMaxId(connection);
            nextId++;
            preparedStatement = connection.prepareStatement("INSERT INTO Slots.database_multipliers" +
                " (multiplier, multiplier_freq, game_id,  id_multiplier) " +
                "VALUES (?, ?, ?, ?);");
            fillPreparedStatementByDatabaseMultipliers(preparedStatement, multipliers);
            preparedStatement.setLong(FOURTH_ITEM, nextId);
            preparedStatement.executeUpdate();
        }
        catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatement(connection, preparedStatement);
        }
    }

    @Override
    public void update(DatabaseMultipliers multipliers) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("UPDATE Slots.database_multipliers" +
                " SET multiplier = ?, multiplier_freq = ?, game_id = ? " +
                "where id_multiplier = ?;");
            fillPreparedStatementByDatabaseMultipliers(preparedStatement, multipliers);
            preparedStatement.setLong(FOURTH_ITEM, multipliers.getIdMultiplier());
            preparedStatement.executeUpdate();
        }
        catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatement(connection, preparedStatement);
        }
    }

    @Override
    public void deleteBySLotInfoId(int gameId) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("DELETE from Slots.database_multipliers " +
                    "where game_id = ?;");
            preparedStatement.setLong(FIRST_ITEM, gameId);
            preparedStatement.executeUpdate();
        }
        catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatement(connection, preparedStatement);
        }
    }

    @Override
    public void truncate() {
        DatabaseUtils.truncateTable("Slots.database_multipliers");
    }

    private int findMaxId(Connection connection) {
        return DatabaseUtils.getParameterFromQuery("select max(id_multiplier) from Slots.database_multipliers", connection);
    }

    private DatabaseMultipliers fillMultipliers(ResultSet resultSet) throws SQLException {
        DatabaseMultipliers multipliers = new DatabaseMultipliers();
        multipliers.setIdMultiplier(resultSet.getLong(ID));
        multipliers.setMultiplier(resultSet.getInt(MULTIPLIER));
        multipliers.setMultiplierFreq(resultSet.getDouble(MULTIPLIER_COUNT));
        multipliers.setGameId(resultSet.getInt(GAME_ID));

        return multipliers;
    }

    private Multipliers convertDatabaseMultipliers (DatabaseMultipliers databaseMultipliers){
        Multipliers multipliers = new Multipliers();
        multipliers.setMultiplier(databaseMultipliers.getMultiplier());
        multipliers.setMultiplierCount((int) databaseMultipliers.getMultiplierFreq());
        return multipliers;
    }

    private void fillPreparedStatementByDatabaseMultipliers(PreparedStatement preparedStatement, DatabaseMultipliers multipliers) throws SQLException {
        preparedStatement.setInt(FIRST_ITEM, multipliers.getMultiplier());
        preparedStatement.setDouble(SECOND_ITEM, multipliers.getMultiplierFreq());
        preparedStatement.setLong(THIRD_ITEM, multipliers.getGameId());
    }
}
