package by.minsk.miroha.repositories.report.impl;

import by.minsk.miroha.entities.databases.BrandPlusLicense;
import by.minsk.miroha.entities.databases.SiteChanging;
import by.minsk.miroha.entities.databases.SitePlusLicense;
import by.minsk.miroha.entities.report.CasinoSite;
import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.repositories.report.SitesRepository;
import by.minsk.miroha.utils.DatabaseUtils;
import org.springframework.stereotype.Repository;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static by.minsk.miroha.repositories.report.impl.CommonSimInfoRepositoryImpl.DB_ERROR;

@Repository
public class SitesRepositoryImpl implements SitesRepository {

    private static final String CASINO_SITE_ID = "casino_sites_id";
    private static final String CASINO_BRAND = "casino_brand";
    private static final String CASINO_SITE = "casino_site";
    private static final String REMOVING_DATE = "removing_date";
    private static final String CREATION_DATE = "creation_date";
    private static final String LICENSE = "license";
    private static final int FIRST_ITEM = 1;
    private static final int SECOND_ITEM = 2;

    @Override
    public List<BrandPlusLicense> getAllBrands() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("select distinct cs.casino_brand, sl.license from Slots.casino_sites cs \n" +
                    "left outer join Slots.sites_licenses sl\n" +
                    "on cs.casino_sites_id  = sl.sites_id and" +
                    " cs.activity = 'Active' ");
            resultSet = preparedStatement.executeQuery();
            List<BrandPlusLicense> brandPlusLicensesList = new ArrayList<>();
            while (resultSet.next()){
                brandPlusLicensesList.add(extractBrandWithLicense(resultSet));
            }
            return brandPlusLicensesList;
        } catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        } finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public Set<String> getLicenses() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("select distinct license from Slots.sites_licenses ");
            resultSet = preparedStatement.executeQuery();
            Set<String> licenses = new HashSet<>();
            while (resultSet.next()){
                licenses.add(resultSet.getString(LICENSE));
            }
            return licenses;
        } catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        } finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public Set<String> getLicensesInsideBrand(String brand) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("select distinct license from Slots.casino_sites cs" +
                    " inner join Slots.sites_licenses sl " +
                    " on cs.casino_brand = ? and cs.casino_sites_id = sl.sites_id ");
            preparedStatement.setString(FIRST_ITEM, brand);
            resultSet = preparedStatement.executeQuery();
            Set<String> licenses = new HashSet<>();
            while (resultSet.next()){
                licenses.add(resultSet.getString(LICENSE));
            }
            return licenses;
        } catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        } finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public List<SitePlusLicense> getSitesByBrand(String brand) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("select cs.casino_site, sl.license from Slots.casino_sites cs " +
                    " inner join Slots.sites_licenses sl " +
                    " on cs.casino_sites_id = sl.sites_id and cs.casino_brand = ? " +
                    " and cs.activity = 'Active' ");
            preparedStatement.setString(FIRST_ITEM, brand);
            resultSet = preparedStatement.executeQuery();
            List<SitePlusLicense> sites = new ArrayList<>();
            while (resultSet.next()){
                sites.add(extractSiteWithLicense(resultSet));
            }
            return sites;
        } catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        } finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public List<CasinoSite> getCommonSitesInformation(String site) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("select * from Slots.casino_sites " +
                    " where casino_site = ? ");
            preparedStatement.setString(FIRST_ITEM, site);
            resultSet = preparedStatement.executeQuery();
            List<CasinoSite> sites = new ArrayList<>();
            while (resultSet.next()){
                sites.add(extractSite(resultSet));
            }
            return sites;
        } catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        } finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public List<CasinoSite> getAllCasinoSites() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("select * from Slots.casino_sites ");
            resultSet = preparedStatement.executeQuery();
            List<CasinoSite> sites = new ArrayList<>();
            while (resultSet.next()){
                sites.add(extractSiteWithoutLicense(resultSet));
            }
            return sites;
        } catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        } finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public CasinoSite getSiteByFields(String brand, String site) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("select * from Slots.casino_sites " +
                    " where casino_brand = ? and casino_site = ? ");
            preparedStatement.setString(FIRST_ITEM, brand);
            preparedStatement.setString(SECOND_ITEM, site);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                return extractSiteWithoutLicense(resultSet);
            }
            return null;
        } catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        } finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public List<SiteChanging> getRemovedSites(Timestamp startDate, Timestamp endDate) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("select cs.casino_site, cs.casino_brand, " +
                    "cs.removing_date, cs.creation_date from Slots.casino_sites cs " +
                    " WHERE cs.removing_date >= ? and cs.removing_date <= ? ");
            preparedStatement.setTimestamp(FIRST_ITEM, startDate);
            preparedStatement.setTimestamp(SECOND_ITEM, endDate);
            resultSet = preparedStatement.executeQuery();
            List<SiteChanging> siteChangingList = new ArrayList<>();
            while (resultSet.next()){
                siteChangingList.add(extractSiteChanging(resultSet));
            }
            return siteChangingList;
        } catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        } finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public List<SiteChanging> getNewSites(Timestamp startDate, Timestamp endDate) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("select cs.casino_site, cs.casino_brand, " +
                    "cs.removing_date, cs.creation_date from Slots.casino_sites cs " +
                    " WHERE cs.creation_date >= ? and cs.creation_date <= ? ");
            preparedStatement.setTimestamp(FIRST_ITEM, startDate);
            preparedStatement.setTimestamp(SECOND_ITEM, endDate);
            resultSet = preparedStatement.executeQuery();
            List<SiteChanging> newSites = new ArrayList<>();
            while (resultSet.next()){
                newSites.add(extractSiteChanging(resultSet));
            }
            return newSites;
        } catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        } finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public List<SitePlusLicense> getLicenseForJackpots(List<String> jackpots) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try{
            connection = DatabaseUtils.getDatabasesConnection();
            List<SitePlusLicense> sites = new ArrayList<>();
            for(String jackpot : jackpots) {
                preparedStatement = connection.prepareStatement("select cs.casino_site, sl.license " +
                        " from Slots.casino_sites cs left outer join Slots.sites_licenses sl " +
                        " on cs.casino_sites_id = sl.sites_id " +
                        " where cs.casino_site like '%" + jackpot + "%'");
                resultSet = preparedStatement.executeQuery();
                while (resultSet.next()){
                    sites.add(extractSiteWithLicense(resultSet));
                }
            }
            return sites;
        }catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    public List<String> getSiteLicenses(long siteId){
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("select license from Slots.sites_licenses " +
                    " where sites_id = ? ");
            preparedStatement.setLong(FIRST_ITEM, siteId);
            resultSet = preparedStatement.executeQuery();
            List<String> licenses = new ArrayList<>();
            while (resultSet.next()){
                licenses.add(resultSet.getString(LICENSE));
            }
            return licenses;
        } catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        } finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    private CasinoSite extractSite(ResultSet resultSet) throws SQLException{
        CasinoSite site = new CasinoSite();
        site.setSiteId(resultSet.getLong(CASINO_SITE_ID));
        site.setSite(resultSet.getString(CASINO_SITE));
        site.setBrand(resultSet.getString(CASINO_BRAND));
        site.setLicenses(getSiteLicenses(site.getSiteId()));
        return site;
    }
    private CasinoSite extractSiteWithoutLicense(ResultSet resultSet) throws SQLException{
        CasinoSite site = new CasinoSite();
        site.setSiteId(resultSet.getLong(CASINO_SITE_ID));
        site.setSite(resultSet.getString(CASINO_SITE));
        site.setBrand(resultSet.getString(CASINO_BRAND));
        return site;
    }

    private BrandPlusLicense extractBrandWithLicense(ResultSet resultSet) throws  SQLException{
        BrandPlusLicense brandPlusLicense = new BrandPlusLicense();
        brandPlusLicense.setBrand(resultSet.getString(CASINO_BRAND).trim());
        brandPlusLicense.setLicense(resultSet.getString(LICENSE));
        return brandPlusLicense;
    }

    private SitePlusLicense extractSiteWithLicense(ResultSet resultSet) throws SQLException{
        SitePlusLicense sitePlusLicense = new SitePlusLicense();
        sitePlusLicense.setSite(resultSet.getString(CASINO_SITE));
        sitePlusLicense.setLicense(resultSet.getString(LICENSE));
        return sitePlusLicense;
    }

    private SiteChanging extractSiteChanging(ResultSet resultSet) throws SQLException{
        SiteChanging siteChanging = new SiteChanging();
        siteChanging.setSite(resultSet.getString(CASINO_SITE));
        siteChanging.setBrand(resultSet.getString(CASINO_BRAND));
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Timestamp removingDate = resultSet.getTimestamp(REMOVING_DATE);
        if(removingDate != null) {
            siteChanging.setRemovingDate(dateFormat.format(new Date(removingDate.getTime())));
        }
        Timestamp creationDate = resultSet.getTimestamp(CREATION_DATE);
        if(creationDate != null) {
            siteChanging.setCreationDate(dateFormat.format(new Date(creationDate.getTime())));
        }
        return siteChanging;
    }
}
