package by.minsk.miroha.repositories.report;

import java.util.List;

import by.minsk.miroha.entities.report.SlotPoint;

public interface SlotPointsRepository {

    List<SlotPoint> findByGame(String game, String provider);

    void save (SlotPoint slotPoint);

    void update (SlotPoint slotPoint);

    void delete(String game, String provider);

    void truncate();

}
