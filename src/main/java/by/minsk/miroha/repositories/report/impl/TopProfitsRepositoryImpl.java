package by.minsk.miroha.repositories.report.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import by.minsk.miroha.entities.report.TopProfits;
import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.repositories.report.TopProfitsRepository;
import by.minsk.miroha.utils.DatabaseUtils;

public class TopProfitsRepositoryImpl implements TopProfitsRepository {

    public static final String DB_ERROR = "Не удалось подключиться к БД";
    public static final int FIRST_ITEM = 1;
    private static final int SECOND_ITEM = 2;
    private static final int THIRD_ITEM = 3;
    private static final int FOURTH_ITEM = 4;
    private static final int FIFTH_ITEM = 5;

    public static final String ID = "id_top_profits";
    public static final String NAME = "name";
    public static final String PROFIT = "profit";
    public static final String FREQUENCY = "frequency";

    @Override
    public List<TopProfits> findBySlotInput(long idSlotInput) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("select * from Slots.top_profits where id_slot_input = ?");
            preparedStatement.setLong(FIRST_ITEM, idSlotInput);
            resultSet = preparedStatement.executeQuery();
            List<TopProfits> topProfits = new ArrayList<>();
            while (resultSet.next()){
                topProfits.add(fillTopProfits(resultSet));
            }
            return topProfits;
        }
        catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public void save(TopProfits topProfits) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            int nextId = findMaxId(connection);
            nextId++;
            preparedStatement = connection.prepareStatement("INSERT INTO Slots.top_profits" +
                " (name, profit, frequency, id_slot_input, id_top_profits) " +
                "VALUES (?, ?, ?, ?, ?);");
            fillPreparedStatementByTopProfits(preparedStatement, topProfits);
            preparedStatement.setLong(FIFTH_ITEM, nextId);
            preparedStatement.executeUpdate();
        }
        catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatement(connection, preparedStatement);
        }
    }

    @Override
    public void update(TopProfits topProfits) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("UPDATE Slots.top_profits" +
                " SET name = ?, profit = ?, frequency = ?, id_slot_input = ? " +
                "where id_top_profits = ?;");
            fillPreparedStatementByTopProfits(preparedStatement, topProfits);
            preparedStatement.setLong(FIFTH_ITEM, topProfits.getIdTopProfits());
            preparedStatement.executeUpdate();
        }
        catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatement(connection, preparedStatement);
        }

    }

    @Override
    public void delete(long slotInputId) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("DELETE from Slots.top_profits " +
                    "where id_slot_input = ?;");
            preparedStatement.setLong(FIRST_ITEM, slotInputId);
            preparedStatement.executeUpdate();
        }
        catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatement(connection, preparedStatement);
        }
    }

    @Override
    public void truncate() {
        DatabaseUtils.truncateTable("Slots.top_profits");
    }

    private int findMaxId(Connection connection) {
        return DatabaseUtils.getParameterFromQuery("select max(id_top_profits) from Slots.top_profits", connection);
    }

    private TopProfits fillTopProfits(ResultSet resultSet) throws SQLException {
        TopProfits topProfits = new TopProfits();
        topProfits.setName(resultSet.getInt(NAME));
        topProfits.setProfit(resultSet.getDouble(PROFIT));
        topProfits.setFrequency(resultSet.getDouble(FREQUENCY));
        topProfits.setIdTopProfits(resultSet.getLong(ID));

        return topProfits;
    }

    private void fillPreparedStatementByTopProfits(PreparedStatement preparedStatement, TopProfits topProfits) throws SQLException {
        preparedStatement.setInt(FIRST_ITEM, topProfits.getName());
        preparedStatement.setDouble(SECOND_ITEM, topProfits.getProfit());
        preparedStatement.setDouble(THIRD_ITEM, topProfits.getFrequency());
        preparedStatement.setLong(FOURTH_ITEM, topProfits.getSlotInput().getIdInput());
    }
}
