package by.minsk.miroha.repositories.report;

import by.minsk.miroha.report.CasinoReport;

import java.util.List;

public interface CasinoReportRepository {

    List<CasinoReport> getCasinoReports(int limit, int offset, String sortField,
                                        String sortType, List<String> providers, List<String> slots);
}
