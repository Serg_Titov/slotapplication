package by.minsk.miroha.repositories.report;

import by.minsk.miroha.entities.report.CommonSlotInformation;

import java.util.List;

public interface CommonSlotInfoRepository  {

    CommonSlotInformation getObject(int gameId);

    CommonSlotInformation getObjectById(int commonslotInfoId);

    List<CommonSlotInformation> getListObjectById(List<Integer> idList);

    CommonSlotInformation save(CommonSlotInformation commonSlotInformation);

    CommonSlotInformation update(CommonSlotInformation commonSlotInformation);

    void deleteBySlotInfoId(int gameId);

    void truncate();

}
