package by.minsk.miroha.repositories.report.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import by.minsk.miroha.entities.report.SlotPoint;
import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.repositories.report.SlotPointsRepository;
import by.minsk.miroha.utils.DatabaseUtils;
import org.springframework.stereotype.Repository;

@Repository
public class SlotPointsRepositoryImpl implements SlotPointsRepository {

    private static final String DB_ERROR = "Не удалось подключиться к БД";
    public static final String ID_SLOT_POINT = "id_slot_point";
    public static final String GAME = "game";
    public static final String PROVIDER = "provider";
    public static final String SPIN_LIMIT = "spin_limit";
    public static final String POINT_RESULT = "point_result";

    private static final int FIRST_ITEM = 1;
    private static final int SECOND_ITEM = 2;
    private static final int THIRD_ITEM = 3;
    private static final int FOURTH_ITEM = 4;
    private static final int FIFTH_ITEM = 5;

    @Override
    public List<SlotPoint> findByGame(String game, String provider) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("select * from Slots.slot_points " +
                    "where game = ? and provider = ?");
            preparedStatement.setString(FIRST_ITEM, game);
            preparedStatement.setString(SECOND_ITEM, provider);
            resultSet = preparedStatement.executeQuery();
            List<SlotPoint> points = new ArrayList<>();
            while (resultSet.next()){
                points.add(fillSlotPoint(resultSet));
            }
            return points;
        }
        catch (ClassNotFoundException | SQLException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public void save(SlotPoint slotPoint) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            int nextId = findMaxId(connection);
            nextId++;
            preparedStatement = connection.prepareStatement("INSERT INTO Slots.slot_points" +
                " (game, provider,  spin_limit, point_result, id_slot_point) VALUES (?, ?, ?, ?, ?);");
            fillPreparedStatementBySlotPoint(preparedStatement, slotPoint);
            preparedStatement.setLong(FIFTH_ITEM, nextId);
            preparedStatement.executeUpdate();
        }
        catch (ClassNotFoundException | SQLException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatement(connection, preparedStatement);
        }
    }

    @Override
    public void update(SlotPoint slotPoint) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("UPDATE Slots.slot_points" +
                " SET game = ?, provider = ?, spin_limit = ?, point_result = ? " +
                "where id_slot_point = ?;");
            fillPreparedStatementBySlotPoint(preparedStatement, slotPoint);
            preparedStatement.setLong(FIFTH_ITEM, slotPoint.getIdSlotPoint());
            preparedStatement.executeUpdate();
        }
        catch (ClassNotFoundException | SQLException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatement(connection, preparedStatement);
        }
    }

    @Override
    public void delete(String game, String provider) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("delete from Slots.slot_points " +
                    "where game = ? and provider=? ");
            preparedStatement.setString(FIRST_ITEM, game);
            preparedStatement.setString(SECOND_ITEM, provider);
            preparedStatement.executeUpdate();
        } catch (ClassNotFoundException | SQLException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        } finally {
            DatabaseUtils.closeConnectionPrepStatement(connection, preparedStatement);
        }
    }

    @Override
    public void truncate() {
        DatabaseUtils.truncateTable("Slots.slot_points");
    }

    private int findMaxId(Connection connection) {
        return DatabaseUtils.getParameterFromQuery("select max(id_slot_point) from Slots.slot_points", connection);
    }

    private SlotPoint fillSlotPoint(ResultSet resultSet) throws SQLException {
        SlotPoint slotPoint = new SlotPoint();
        slotPoint.setIdSlotPoint(resultSet.getLong(ID_SLOT_POINT));
        slotPoint.setGame(resultSet.getString(GAME));
        slotPoint.setProvider(resultSet.getString(PROVIDER));
        slotPoint.setSpinLimit(resultSet.getString(SPIN_LIMIT));
        slotPoint.setPointResult(resultSet.getDouble(POINT_RESULT));
        return slotPoint;
    }

    private void fillPreparedStatementBySlotPoint(PreparedStatement preparedStatement, SlotPoint slotPoint) throws SQLException {
        preparedStatement.setString(FIRST_ITEM, slotPoint.getGame());
        preparedStatement.setString(SECOND_ITEM, slotPoint.getProvider());
        preparedStatement.setString(THIRD_ITEM, slotPoint.getSpinLimit());
        preparedStatement.setDouble(FOURTH_ITEM, slotPoint.getPointResult());
    }
}
