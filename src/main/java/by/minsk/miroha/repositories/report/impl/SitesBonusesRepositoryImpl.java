package by.minsk.miroha.repositories.report.impl;

import by.minsk.miroha.entities.common.Site;
import by.minsk.miroha.entities.report.SitesBonuses;
import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.repositories.report.SitesBonusesRepository;
import by.minsk.miroha.utils.DatabaseUtils;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static by.minsk.miroha.repositories.report.impl.CommonSimInfoRepositoryImpl.DB_ERROR;

@Repository
public class SitesBonusesRepositoryImpl implements SitesBonusesRepository {

    private static final int FIRST_ITEM = 1;
    private static final int SECOND_ITEM = 2;
    private static final int THIRD_ITEM = 3;
    private static final String BONUS_ID = "bonus_id";

    @Override
    public List<Long> getSiteBonus(long siteId) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("select bonus_id from Slots.sites_bonuses " +
                    " where site_id = ? ");
            preparedStatement.setLong(FIRST_ITEM, siteId);
            resultSet = preparedStatement.executeQuery();
            List<Long> bonuses = new ArrayList<>();
            while (resultSet.next()){
                bonuses.add(resultSet.getLong(BONUS_ID));
            }
            return bonuses;
        } catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public void saveBonusLink(long idBonus, long idSite) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            int nextId = findMaxId(connection);
            nextId++;
            preparedStatement = connection.prepareStatement("INSERT INTO Slots.sites_bonuses" +
                    " (sites_bonuses_id, site_id, bonus_id) VALUES (?, ?, ?);");
            preparedStatement.setLong(FIRST_ITEM, nextId);
            preparedStatement.setLong(SECOND_ITEM, idSite);
            preparedStatement.setLong(THIRD_ITEM, idBonus);
            preparedStatement.executeUpdate();
        } catch (SQLException | ClassNotFoundException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatement(connection, preparedStatement);
        }
    }

    private int findMaxId(Connection connection) {
        return DatabaseUtils.getParameterFromQuery("select max(sites_bonuses_id) from Slots.sites_bonuses", connection);
    }
}
