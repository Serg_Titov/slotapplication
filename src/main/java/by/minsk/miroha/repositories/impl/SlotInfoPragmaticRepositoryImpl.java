package by.minsk.miroha.repositories.impl;

import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.plaf.InsetsUIResource;

import by.minsk.miroha.entities.databases.SlotInfoPragmatic;
import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.utils.DatabaseUtils;

public class SlotInfoPragmaticRepositoryImpl {

    private static final int FIRST_ITEM = 1;
    private static final String ID_SLOT_INFO = "id";
    private static final String BID = "bid";
    private static final String WIN = "win";
    private static final String CREDIT = "credit";
    private static final String BONUS = "bonus";
    private static final String IP = "ip";
    private static final String GAME = "game";
    private static final String DB_ERROR = "Не удалось подключиться к БД";

    public int countAll(){
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("select count(*) from pragmatic.maintbl");
            resultSet = preparedStatement.executeQuery();
            return DatabaseUtils.getParameterFromResultSet(resultSet);
        }
        catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
            throw new CustomValidation(new String(DB_ERROR.getBytes(), StandardCharsets.UTF_8));
        }finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    public int countAllInGame(String game){
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try{
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("select count(*) from pragmatic.maintbl where game = ?");
            preparedStatement.setString(FIRST_ITEM, game);
            resultSet = preparedStatement.executeQuery();
            return DatabaseUtils.getParameterFromResultSet(resultSet);
        }
        catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
            throw new CustomValidation(new String(DB_ERROR.getBytes(), StandardCharsets.UTF_8));
        } finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    public Set<String> allGames(){
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("select distinct game from pragmatic.maintbl");
            resultSet = preparedStatement.executeQuery();
            Set<String> games = new HashSet<>();
            while (resultSet.next()){
                games.add(DatabaseUtils.getStringFromResultSet(resultSet));
            }
            return games;
        }
        catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
            throw new CustomValidation(new String(DB_ERROR.getBytes(), StandardCharsets.UTF_8));
        } finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    public List<Double> getAllCoefInGame(String game){
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try{
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("select win, bid from pragmatic.maintbl where game = ?");
            preparedStatement.setString(FIRST_ITEM, game);
            resultSet = preparedStatement.executeQuery();
            List<Double> allCoef = new ArrayList<>();
            while (resultSet.next()){
                allCoef.add(DatabaseUtils.getDoubleCoefFromResultSet(resultSet));
            }
            return allCoef;
        }
        catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
            throw new CustomValidation(new String(DB_ERROR.getBytes(), StandardCharsets.UTF_8));
        } finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    public List<SlotInfoPragmatic> findAllInGame(String game){
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("select * from pragmatic.maintbl where game = ?");
            preparedStatement.setString(FIRST_ITEM, game);
            resultSet = preparedStatement.executeQuery();
            List<SlotInfoPragmatic> slotInfoList = new ArrayList<>();
            while (resultSet.next()){
                SlotInfoPragmatic slotInfoPragmatic = fillSlotInfoPragmatic(resultSet);
                slotInfoList.add(slotInfoPragmatic);
            }
            return slotInfoList;
        }
        catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
            throw new CustomValidation(new String(DB_ERROR.getBytes(), StandardCharsets.UTF_8));
        } finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    public SlotInfoPragmatic findById(int id) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        SlotInfoPragmatic slotInfo = new SlotInfoPragmatic();
        try{
            connection = DatabaseUtils.getDatabasesConnection();
            preparedStatement = connection.prepareStatement("select * from pragmatic.maintbl where id = ?");
            preparedStatement.setInt(FIRST_ITEM, id);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                slotInfo = fillSlotInfoPragmatic(resultSet);
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
        return slotInfo;
    }

    private SlotInfoPragmatic fillSlotInfoPragmatic(ResultSet resultSet) throws SQLException {
        SlotInfoPragmatic slotInfo = new SlotInfoPragmatic();
        if(resultSet.next()) {
            slotInfo.setIdPragmatic(resultSet.getInt(ID_SLOT_INFO));
            slotInfo.setBid(resultSet.getDouble(BID));
            slotInfo.setWin(resultSet.getDouble(WIN));
            slotInfo.setCredit(resultSet.getDouble(CREDIT));
            slotInfo.setBonus(resultSet.getString(BONUS));
            slotInfo.setIp(resultSet.getString(IP));
            slotInfo.setGame(resultSet.getString(GAME));
        }
        return slotInfo;
    }
}
