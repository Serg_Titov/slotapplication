package by.minsk.miroha.repositories.impl;

import by.minsk.miroha.entities.databases.SlotName;
import by.minsk.miroha.repositories.SlotNameRepository;
import by.minsk.miroha.utils.DatabaseUtils;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Repository
public class SlotNameRepositoryImpl implements SlotNameRepository {

    public static final long ID = 1L;
    private static final int FIRST_ITEM = 1;
    private static final int SECOND_ITEM = 2;
    private static final int THIRD_ITEM = 3;
    private static final String NAME = "name";
    private static final String PROVIDER = "provider";

    @Override
    public SlotName getSlotName() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        SlotName slotName = new SlotName();
        try{
            connection = DatabaseUtils.getHSQLConnection();
            preparedStatement = connection.prepareStatement("select * from slot_name where id_name = ?");
            preparedStatement.setLong(FIRST_ITEM, ID);
            resultSet = preparedStatement.executeQuery();
            slotName = fillSlotName(resultSet);

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
        return slotName;
    }

    @Override
    public void save(SlotName slotName) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DatabaseUtils.getHSQLConnection();
            preparedStatement = connection.prepareStatement(
                        "INSERT INTO slot_name (id_name, name, provider) VALUES (?, ?, ?);");
            setSlotNameIntoStatement(slotName, preparedStatement);
            preparedStatement.executeUpdate();
        } catch (SQLException| ClassNotFoundException  e) {
            e.printStackTrace();
        } finally {
            DatabaseUtils.closeConnectionPrepStatement(connection, preparedStatement);
        }
    }

    @Override
    public void clearTable() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DatabaseUtils.getHSQLConnection();
            preparedStatement = connection.prepareStatement("truncate table slot_name");
            preparedStatement.executeUpdate();
        }
        catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatement(connection, preparedStatement);
        }
    }

    private void setSlotNameIntoStatement(SlotName slotName, PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setLong(FIRST_ITEM, ID);
        preparedStatement.setString(SECOND_ITEM, slotName.getSlotName());
        preparedStatement.setString(THIRD_ITEM, slotName.getProvider());
    }

    private SlotName fillSlotName(ResultSet resultSet) throws SQLException {
        SlotName slotName = new SlotName();
        if(resultSet.next()) {
            slotName.setSlotName(resultSet.getString(NAME));
            slotName.setProvider(resultSet.getString(PROVIDER));
        }
        return slotName;
    }
}
