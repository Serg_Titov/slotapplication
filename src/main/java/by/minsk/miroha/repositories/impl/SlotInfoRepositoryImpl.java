package by.minsk.miroha.repositories.impl;

import by.minsk.miroha.entities.databases.SlotInfoHome;
import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.repositories.SlotInfoRepository;
import by.minsk.miroha.utils.DatabaseUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SlotInfoRepositoryImpl implements SlotInfoRepository {

    public static final String DB_ERROR = "Не удалось подключиться к БД slot_info";

    public static final int FIRST_ITEM = 1;
    public static final int SECOND_ITEM = 2;
    private static final int THIRD_ITEM = 3;
    private static final int FOURTH_ITEM = 4;
    private static final int FIFTH_ITEM = 5;
    private static final int SIXTH_ITEM = 6;
    private static final String ID_SLOT_INFO = "id_run";
    private static final String BID = "bid";
    private static final String WIN = "win";
    private static final String GAME = "game";
    private static final String BONUS = "bonus";
    private static final String WIN_COEFFICIENT = "win_coefficient";

    public List<SlotInfoHome> findAll() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<SlotInfoHome> allSlotInfo = new ArrayList<>();
        try{
            connection = DatabaseUtils.getHSQLConnection();
            preparedStatement = connection.prepareStatement("select * from slot_info");
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                SlotInfoHome slotInfo = fillSlotInfo(resultSet);
                allSlotInfo.add(slotInfo);
            }
        } catch (SQLException |ClassNotFoundException e) {
            e.printStackTrace();
        }finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
        return allSlotInfo;
    }

    @Override
    public List<Double> getSubsetOfSlotInfo(int limit, int offset) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getHSQLConnection();
            preparedStatement = connection.prepareStatement("select win_coefficient from slot_info order by id_run limit ?, ?");
            preparedStatement.setInt(FIRST_ITEM, limit*offset);
            preparedStatement.setInt(SECOND_ITEM, limit);
            resultSet = preparedStatement.executeQuery();
            List<Double> partOfCoef = new ArrayList<>();
            while (resultSet.next()){
                partOfCoef.add(resultSet.getDouble(WIN_COEFFICIENT));
            }
            return partOfCoef;
        } catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public int countAll() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getHSQLConnection();
            preparedStatement = connection.prepareStatement("select count(*) from slot_info");
            resultSet = preparedStatement.executeQuery();
            return DatabaseUtils.getParameterFromResultSet(resultSet);
        }
        catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        } finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    public List<Double> getAllCoef() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Double> allCoef = new ArrayList<>();
        try {
            connection = DatabaseUtils.getHSQLConnection();
            preparedStatement = connection.prepareStatement("select win_coefficient from slot_info");
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                allCoef.add(resultSet.getDouble(WIN_COEFFICIENT));
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
        return allCoef;
    }

    @Override
    public double getMaxWin() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getHSQLConnection();
            preparedStatement = connection.prepareStatement("select max(win_coefficient) from slot_info");
            resultSet = preparedStatement.executeQuery();
            return DatabaseUtils.getDoubleParameterFromResultSet(resultSet);
        }
        catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        } finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public double getAverageMultiplier() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getHSQLConnection();
            preparedStatement = connection.prepareStatement("select avg(win_coefficient) from slot_info");
            resultSet = preparedStatement.executeQuery();
            return DatabaseUtils.getDoubleParameterFromResultSet(resultSet);
        }
        catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        } finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public int getAmountCoefficientUpperValue(double value) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try{
            connection = DatabaseUtils.getHSQLConnection();
            preparedStatement = connection.prepareStatement("select count(win_coefficient) from slot_info where win_coefficient >= ?");
            preparedStatement.setDouble(FIRST_ITEM, value);
            resultSet = preparedStatement.executeQuery();
            return DatabaseUtils.getParameterFromResultSet(resultSet);
        }catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        } finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public SlotInfoHome findById(long id) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        SlotInfoHome slotInfo = new SlotInfoHome();
        try{
            connection = DatabaseUtils.getHSQLConnection();
            preparedStatement = connection.prepareStatement("select * from slot_info where id_run = ?");
            preparedStatement.setLong(FIRST_ITEM, id);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                slotInfo = fillSlotInfo(resultSet);
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
        return slotInfo;
    }

    @Override
    public void save(List<SlotInfoHome> slotInfos) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DatabaseUtils.getHSQLConnection();
            for (SlotInfoHome slotInfo: slotInfos) {
                preparedStatement = connection.prepareStatement(
                        "INSERT INTO slot_info (bid, win, bonus, win_coefficient, " +
                                "game, id_run)"+ " VALUES (?, ?, ?, ?, ?, ?);");
                setSlotInfoIntoStatement(slotInfo, preparedStatement);
                preparedStatement.executeUpdate();
            }
        } catch (SQLException| ClassNotFoundException  e) {
            e.printStackTrace();
        } finally {
            DatabaseUtils.closeConnectionPrepStatement(connection, preparedStatement);
        }
    }

    public void update(SlotInfoHome slotInfo) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try{
            connection = DatabaseUtils.getHSQLConnection();
            preparedStatement = connection.prepareStatement("UPDATE slot_info SET " +
                    "bid = ? , win = ? , win_coefficient = ? , game = ? " +
                    "where id_run = ?");
            setSlotInfoIntoStatement(slotInfo, preparedStatement);
            preparedStatement.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            DatabaseUtils.closeConnectionPrepStatement(connection, preparedStatement);
        }
    }

    @Override
    public void delete(long id) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DatabaseUtils.getHSQLConnection();
            preparedStatement = connection.prepareStatement("delete from slot_info where id_run = ?");
            preparedStatement.setLong(FIRST_ITEM, id);
            preparedStatement.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            DatabaseUtils.closeConnectionPrepStatement(connection, preparedStatement);
        }
    }

    @Override
    public void clearTable() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DatabaseUtils.getHSQLConnection();
            preparedStatement = connection.prepareStatement("truncate table slot_info");
            preparedStatement.executeUpdate();
        }
        catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatement(connection, preparedStatement);
        }
    }

    @Override
    public double getSumAllWins() {
        Connection connection = null;
        try {
            connection = DatabaseUtils.getHSQLConnection();
            return DatabaseUtils.getSum("win", "slot_info", connection );
        } catch (ClassNotFoundException | SQLException throwables) {
            throw new CustomValidation(DB_ERROR);
        } finally {
            DatabaseUtils.closeResource(connection);
        }
    }

    @Override
    public double getSumAllBids() {
        Connection connection = null;
        try {
            connection = DatabaseUtils.getHSQLConnection();
            return DatabaseUtils.getSum("bid", "slot_info", connection );
        } catch (ClassNotFoundException | SQLException throwables) {
            throw new CustomValidation(DB_ERROR);
        } finally {
            DatabaseUtils.closeResource(connection);
        }
    }

    @Override
    public double getBonusAmount() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try{
            connection = DatabaseUtils.getHSQLConnection();
            preparedStatement = connection.prepareStatement("select count(*) from slot_info where bonus = 1");
            resultSet = preparedStatement.executeQuery();
            return DatabaseUtils.getDoubleParameterFromResultSet(resultSet);
        } catch (ClassNotFoundException | SQLException throwables) {
            throw new CustomValidation(DB_ERROR);
        } finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public double getWinsWithBonus() {
        Connection connection = null;
        try {
            connection = DatabaseUtils.getHSQLConnection();
            return DatabaseUtils.getSum("win", "slot_info where bonus = 1", connection);
        } catch (ClassNotFoundException | SQLException throwables) {
            throw new CustomValidation(DB_ERROR);
        } finally {
            DatabaseUtils.closeResource(connection);
        }
    }

    @Override
    public double getAverageBonusWin() {
        Connection connection = null;
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            connection = DatabaseUtils.getHSQLConnection();
            preparedStatement = connection.prepareStatement("select avg(win) from slot_info where bonus = 1");
            resultSet = preparedStatement.executeQuery();
            return DatabaseUtils.getDoubleParameterFromResultSet(resultSet);
        } catch (ClassNotFoundException | SQLException throwables){
            throw new CustomValidation(DB_ERROR);
        } finally {
            DatabaseUtils.closeResource(connection);
        }
    }

    @Override
    public double getBid() {
        Connection connection = null;
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            connection = DatabaseUtils.getHSQLConnection();
            preparedStatement = connection.prepareStatement("select distinct bid from slot_info");
            resultSet = preparedStatement.executeQuery();
            return DatabaseUtils.getDoubleParameterFromResultSet(resultSet);
        } catch (ClassNotFoundException | SQLException throwables){
            throw new CustomValidation(DB_ERROR);
        } finally {
            DatabaseUtils.closeResource(connection);
        }
    }

    private SlotInfoHome fillSlotInfo(ResultSet resultSet) throws SQLException {
        SlotInfoHome slotInfo = new SlotInfoHome();
        slotInfo.setIdSlotInfo(resultSet.getLong(ID_SLOT_INFO));
        slotInfo.setBid(resultSet.getDouble(BID));
        slotInfo.setWin(resultSet.getDouble(WIN));
        slotInfo.setBonus(resultSet.getDouble(BONUS));
        slotInfo.setWinCoefficient(resultSet.getDouble(WIN_COEFFICIENT));
        slotInfo.setGame(resultSet.getString(GAME));

        return slotInfo;
    }

    private void setSlotInfoIntoStatement(SlotInfoHome slotInfo, PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setDouble(FIRST_ITEM, slotInfo.getBid());
        preparedStatement.setDouble(SECOND_ITEM, slotInfo.getWin());
        preparedStatement.setDouble(THIRD_ITEM, slotInfo.getBonus());
        preparedStatement.setDouble(FOURTH_ITEM, slotInfo.getWinCoefficient());
        if(slotInfo.getGame() != null){
            preparedStatement.setString(FIFTH_ITEM, slotInfo.getGame());
        }
        else {
            preparedStatement.setString(FIFTH_ITEM, "");
        }
        preparedStatement.setLong(SIXTH_ITEM, slotInfo.getIdSlotInfo());
    }
}