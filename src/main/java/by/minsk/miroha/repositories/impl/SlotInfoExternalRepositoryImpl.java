package by.minsk.miroha.repositories.impl;

import by.minsk.miroha.entities.databases.SlotInfoHome;
import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.repositories.SlotInfoExternalRepository;
import by.minsk.miroha.utils.DatabaseUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static by.minsk.miroha.repositories.impl.SlotInfoRepositoryImpl.DB_ERROR;
import static by.minsk.miroha.repositories.impl.SlotInfoRepositoryImpl.SECOND_ITEM;
import static by.minsk.miroha.repositories.report.impl.TopProfitsRepositoryImpl.FIRST_ITEM;

public class SlotInfoExternalRepositoryImpl implements SlotInfoExternalRepository {

    private final String dbName;

    private final String tableName;

    private static final String ID_SLOT_INFO = "id";
    private static final String BID = "bid";
    private static final String WIN = "win";
    private static final String BONUS = "bonus";

    public SlotInfoExternalRepositoryImpl(String dbName, String tableName) {
        this.dbName = dbName;
        this.tableName = tableName;
    }

    @Override
    public SlotInfoHome findById(long id) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        SlotInfoHome slotInfo = new SlotInfoHome();
        try{
            connection = DatabaseUtils.getDatabasesConnection();
            String query = "select * from " + getTableName() + " where id = ?";
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(FIRST_ITEM, id);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                slotInfo = fillSlotInfo(resultSet);
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
        return slotInfo;
    }

    @Override
    public int countAll() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            String query = "select count(*) from " + getTableName() ;
            preparedStatement = connection.prepareStatement(query);
            resultSet = preparedStatement.executeQuery();
            return DatabaseUtils.getParameterFromResultSet(resultSet);
        }
        catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        } finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public List<Double> getSubsetOfSlotInfo(int limit, int offset) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            String query = "select win/bid from " + getTableName() + " order by id limit ?, ?";
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(FIRST_ITEM, limit*offset);
            preparedStatement.setInt(SECOND_ITEM, limit);
            resultSet = preparedStatement.executeQuery();
            List<Double> partOfCoef = new ArrayList<>();
            while (resultSet.next()){
                partOfCoef.add(resultSet.getDouble(FIRST_ITEM));
            }
            return partOfCoef;
        }
        catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        }
        finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public List<Double> getAllCoef() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Double> allCoef = new ArrayList<>();
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            String query = "select win/bid from " + getTableName();
            preparedStatement = connection.prepareStatement(query);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                allCoef.add(resultSet.getDouble(FIRST_ITEM));
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
        return allCoef;
    }

    @Override
    public double getMaxWin() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            String query = "select max(win/bid) from " + getTableName();
            preparedStatement = connection.prepareStatement(query);
            resultSet = preparedStatement.executeQuery();
            return DatabaseUtils.getDoubleParameterFromResultSet(resultSet);
        }
        catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        } finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public int getAmountCoefficientUpperValue(double value) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try{
            connection = DatabaseUtils.getDatabasesConnection();
            String query = "select count(win/bid) from " + getTableName() + " where win/bid >= ?";
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setDouble(FIRST_ITEM, value);
            resultSet = preparedStatement.executeQuery();
            return DatabaseUtils.getParameterFromResultSet(resultSet);
        }catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        } finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public double getAverageMultiplier() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            String query = "select avg(win/bid) from " + getTableName();
            preparedStatement = connection.prepareStatement(query);
            resultSet = preparedStatement.executeQuery();
            return DatabaseUtils.getDoubleParameterFromResultSet(resultSet);
        }
        catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
            throw new CustomValidation(DB_ERROR);
        } finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public double getBonusAmount() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try{
            connection = DatabaseUtils.getDatabasesConnection();
            String query = "select count(*) from " + getTableName() + " where bonus = 1";
            preparedStatement = connection.prepareStatement(query);
            resultSet = preparedStatement.executeQuery();
            return DatabaseUtils.getDoubleParameterFromResultSet(resultSet);
        } catch (ClassNotFoundException | SQLException throwables) {
            throw new CustomValidation(DB_ERROR);
        } finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public double getSumAllWins() {
        Connection connection = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            return DatabaseUtils.getSum("win", getTableName(), connection );
        } catch (ClassNotFoundException | SQLException throwables) {
            throw new CustomValidation(DB_ERROR);
        } finally {
            DatabaseUtils.closeResource(connection);
        }
    }

    @Override
    public double getSumAllBids() {
        Connection connection = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            return DatabaseUtils.getSum("bid", getTableName(), connection );
        } catch (ClassNotFoundException | SQLException throwables) {
            throw new CustomValidation(DB_ERROR);
        } finally {
            DatabaseUtils.closeResource(connection);
        }
    }

    @Override
    public double getWinsWithBonus() {
        Connection connection = null;
        try {
            connection = DatabaseUtils.getDatabasesConnection();
            String tableName = getTableName() + " where bonus = 1";
            return DatabaseUtils.getSum("win", tableName, connection);
        } catch (ClassNotFoundException | SQLException throwables) {
            throw new CustomValidation(DB_ERROR);
        } finally {
            DatabaseUtils.closeResource(connection);
        }
    }

    @Override
    public double getAverageBonusWin() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try{
            connection = DatabaseUtils.getDatabasesConnection();
            String query = "select avg(win/bid) from " + getTableName() + " where bonus = 1";
            preparedStatement = connection.prepareStatement(query);
            resultSet = preparedStatement.executeQuery();
            return DatabaseUtils.getDoubleParameterFromResultSet(resultSet);
        } catch (ClassNotFoundException | SQLException throwables) {
            throw new CustomValidation(DB_ERROR);
        } finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public double getBid() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try{
            connection = DatabaseUtils.getDatabasesConnection();
            String query = "select distinct bid from " + getTableName() + " ";
            preparedStatement = connection.prepareStatement(query);
            resultSet = preparedStatement.executeQuery();
            return DatabaseUtils.getDoubleParameterFromResultSet(resultSet);
        } catch (ClassNotFoundException | SQLException throwables) {
            throw new CustomValidation(DB_ERROR);
        } finally {
            DatabaseUtils.closeConnectionPrepStatementResultSet(connection, preparedStatement, resultSet);
        }
    }

    private SlotInfoHome fillSlotInfo(ResultSet resultSet) throws SQLException {
        SlotInfoHome slotInfo = new SlotInfoHome();
        slotInfo.setIdSlotInfo(resultSet.getLong(ID_SLOT_INFO));
        slotInfo.setBid(resultSet.getDouble(BID));
        slotInfo.setWin(resultSet.getDouble(WIN));
        slotInfo.setBonus(resultSet.getDouble(BONUS));
        slotInfo.setWinCoefficient(slotInfo.getWin()/ slotInfo.getBid());
        slotInfo.setGame(getTableName());

        return slotInfo;
    }

    private String getTableName(){
        return dbName + "."  + tableName;
    }
}
