package by.minsk.miroha.services;

import by.minsk.miroha.entities.databases.UsersBonusAudit;

public interface UsersBonusAuditService {

    void save(UsersBonusAudit usersBonusAudit);

    void update(UsersBonusAudit usersBonusAudit);

    int getBonusPerformCount(String username);

    UsersBonusAudit getObjectByUsername(String username);

    UsersBonusAudit getObjectById(long id);
}
