package by.minsk.miroha.services;

import by.minsk.miroha.entities.front.responses.SiteChangingReport;
import by.minsk.miroha.entities.report.CasinoSite;
import by.minsk.miroha.entities.report.SiteWithLicenses;
import by.minsk.miroha.report.entities.BrandsList;
import by.minsk.miroha.report.entities.SiteList;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

public interface SitesService {

    BrandsList getAllBrands();

    SiteList getSitesByBrand(String brand);

    List<CasinoSite> getCommonSitesInformation(String site);

    Map<String, List<String>> getBrandsWithSites();

    SiteChangingReport getSiteChanging(Timestamp startDate, Timestamp endDate);

    List<SiteWithLicenses> getJackpotsWithLicense(List<String> jackpots);

}
