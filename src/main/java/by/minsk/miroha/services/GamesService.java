package by.minsk.miroha.services;

import by.minsk.miroha.entities.common.Provider;
import by.minsk.miroha.entities.common.ProviderWithSlots;
import by.minsk.miroha.entities.common.Slot;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface GamesService {

    Map<String, List<String>> getAllGames(List<String> providers);

    List<String> getGamesByProvider(String provider);

    List<Slot> getFrontGamesByProvider(String provider);

    List<String> getAllProviders();

    List<Provider> getAllFrontProviders();

    void calculateAllProviders();

    long getSpinAmount(String slotFullName);

    Date getCreationTableDate(String provider, String slotName);

    List<ProviderWithSlots> getProvidersWithSlots();
}
