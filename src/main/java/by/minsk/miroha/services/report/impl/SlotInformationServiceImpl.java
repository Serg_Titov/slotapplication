package by.minsk.miroha.services.report.impl;

import by.minsk.miroha.entities.report.SlotInformation;
import by.minsk.miroha.entities.report.SlotPoint;
import by.minsk.miroha.perform.SpinnerWriter;
import by.minsk.miroha.repositories.report.CommonSlotInfoRepository;
import by.minsk.miroha.repositories.report.DatabaseMultipliersRepository;
import by.minsk.miroha.repositories.report.SlotInformationRepository;
import by.minsk.miroha.repositories.report.SlotPointsRepository;
import by.minsk.miroha.services.report.SlotInformationService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SlotInformationServiceImpl implements SlotInformationService {

    private static final int POINTS_AMOUNT = 19;

    private final SlotInformationRepository repository;

    private final SlotPointsRepository pointsRepository;

    private final CommonSlotInfoRepository commonSlotInfoRepository;

    private final DatabaseMultipliersRepository databaseMultipliersRepository;

    public SlotInformationServiceImpl(SlotInformationRepository repository,
                                      SlotPointsRepository pointsRepository,
                                      CommonSlotInfoRepository commonSlotInfoRepository,
                                      DatabaseMultipliersRepository databaseMultipliersRepository) {
        this.repository = repository;
        this.pointsRepository = pointsRepository;
        this.commonSlotInfoRepository = commonSlotInfoRepository;
        this.databaseMultipliersRepository = databaseMultipliersRepository;
    }

    @Override
    public Map<String, List<String>> getCalculatedSlotInformation() {
        List<SlotInformation> allList = repository.getAll();
        Map<String, List<String>> resultMap = new HashMap<>();
        for(SlotInformation slotInfo : allList){
            if(resultMap.get(slotInfo.getProvider().getRawProvider()) == null){
                List<String> slots = new ArrayList<>();
                slots.add(slotInfo.getSlot().getRawSlot());
                resultMap.put(slotInfo.getProvider().getRawProvider(), slots);
            } else {
                List<String> existingSlots = resultMap.get(slotInfo.getProvider().getRawProvider());
                existingSlots.add(slotInfo.getSlot().getRawSlot());
            }
        }

        return resultMap;
    }

    @Override
    public List<SlotInformation> getAllSlots() {
        return repository.getAll();
    }

    @Override
    public void fullRemovingSlotInformationById(SlotInformation slotInformation) {
        commonSlotInfoRepository.deleteBySlotInfoId(slotInformation.getIdInfo());
        databaseMultipliersRepository.deleteBySLotInfoId(slotInformation.getIdInfo());
        pointsRepository.delete(slotInformation.getSlot().getRawSlot(), slotInformation.getProvider().getRawProvider());
        repository.delete(slotInformation.getIdInfo());
    }

    @Override
    public void recalculateSlotsPoints() {
        List<SlotInformation> slots = repository.getAll();
        for(SlotInformation slot : slots){
            List<SlotPoint> slotsPoints = pointsRepository.findByGame(slot.getSlot().getRawSlot(), slot.getProvider().getRawProvider());
            if(slotsPoints.size() < POINTS_AMOUNT){
                pointsRepository.delete(slot.getSlot().getRawSlot(), slot.getProvider().getRawProvider());
                SpinnerWriter spinnerWriter = new SpinnerWriter(slot.getProvider().getRawProvider(), slot.getSlot().getRawSlot());
                spinnerWriter.writeToCSV(true);
            }
        }
    }
}
