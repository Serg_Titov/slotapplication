package by.minsk.miroha.services.report.impl;

import by.minsk.miroha.entities.report.CasinoSite;
import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.repositories.report.SitesBonusesRepository;
import by.minsk.miroha.repositories.report.SitesRepository;
import by.minsk.miroha.services.report.SitesBonusesService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SitesBonusesServiceImpl implements SitesBonusesService {

    private static final String SITE_NOT_EXIST = "Такого сайта в базе нет";

    private final SitesBonusesRepository repository;

    private final SitesRepository sitesRepository;

    public SitesBonusesServiceImpl(SitesBonusesRepository repository, SitesRepository sitesRepository) {
        this.repository = repository;
        this.sitesRepository = sitesRepository;
    }

    @Override
    public List<Long> getSitesBonuses(List<CasinoSite> sites) {
        List<Long> bonusesId = new ArrayList<>();
        for (CasinoSite site : sites){
            bonusesId.addAll(repository.getSiteBonus(site.getSiteId()));
        }
        return bonusesId;
    }

    @Override
    public void saveBonusLink(long idBonus, String brand, String site) {
        CasinoSite siteFromDB = sitesRepository.getSiteByFields(brand, site);
        if(siteFromDB != null){
            repository.saveBonusLink(idBonus, siteFromDB.getSiteId());
        } else {
            throw new CustomValidation(SITE_NOT_EXIST);
        }
    }
}
