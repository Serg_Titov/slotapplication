package by.minsk.miroha.services.report;

import by.minsk.miroha.entities.report.CasinoSite;

import java.util.List;

public interface SitesBonusesService {

    List<Long> getSitesBonuses(List<CasinoSite> sites);

    void saveBonusLink(long idBonus, String brand, String site);
}
