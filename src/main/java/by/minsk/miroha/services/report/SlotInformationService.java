package by.minsk.miroha.services.report;

import by.minsk.miroha.entities.report.SlotInformation;

import java.util.List;
import java.util.Map;

public interface SlotInformationService {

    Map<String, List<String>> getCalculatedSlotInformation();

    List<SlotInformation> getAllSlots();

    void fullRemovingSlotInformationById(SlotInformation slotInformation);

    void recalculateSlotsPoints();
}
