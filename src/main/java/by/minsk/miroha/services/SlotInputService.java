package by.minsk.miroha.services;

import by.minsk.miroha.entities.SlotInput;

public interface SlotInputService {

    SlotInput findById(long idSlotInput);

    SlotInput findByAllFields(SlotInput slotInput);

    SlotInput save(SlotInput slotInput);

    SlotInput update(SlotInput slotInput);

    void deleteById(long idSlotInput);

    void truncate();

}
