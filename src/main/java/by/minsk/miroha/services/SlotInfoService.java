package by.minsk.miroha.services;

import java.util.List;

public interface SlotInfoService {

    int countAll();
    List<Double> getSubsetOfSlotInfo(int limit, int offset);
    List<Double> getAllCoefficients();

    double getMaxWin();
    int getAmountCoefficientUpperValue(double value);

    /**
     * Возвращает сумму всех значений в столбце win из БД.
     *
     * @return сумма всех значений в столбце win из БД.
     */
    double getSumAllWins();

    /**
     * Возвращает сумму всех значений в столбце bid из БД.
     *
     * @return сумма всех значений в столбце bid из БД.
     */
    double getSumAllBids();

    /**
     * Возвращает количество строк в БД, в которых bonus=1.
     *
     * @return количество бонусных строк в БД.
     */
    double getBonusCount();

    /**
     * Возвращает среднее значение всех множителей (win/bid) в БД.
     *
     * @return среднее значение всех множителей (win/bid) в БД.
     */
    double getAverageMultiplier();

    /**
     * Возвращает сумму выигрышей в строках, где бонус равен 1.
     *
     * @return сумма выигрышей в строках, где бонус равен 1.
     */
    double getWinsWithBonus();

    /**
     * Возвращает средний выигрыш среди бонусных игр.
     *
     * @return средний выигрыш среди бонусных игр.
     */
    double getAverageBonusWin();

    double getBid();
}
