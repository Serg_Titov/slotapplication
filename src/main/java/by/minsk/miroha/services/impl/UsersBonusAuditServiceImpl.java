package by.minsk.miroha.services.impl;

import by.minsk.miroha.entities.databases.UsersBonusAudit;
import by.minsk.miroha.repositories.audit.UsersBonusAuditRepository;
import by.minsk.miroha.repositories.audit.impl.UserBonusAuditRepositoryImpl;
import by.minsk.miroha.services.UsersBonusAuditService;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class UsersBonusAuditServiceImpl implements UsersBonusAuditService {

    private final UsersBonusAuditRepository repository = new UserBonusAuditRepositoryImpl();

    @Override
    public void save(UsersBonusAudit usersBonusAudit) {
        repository.save(usersBonusAudit);
    }

    @Override
    public void update(UsersBonusAudit usersBonusAudit) {
        usersBonusAudit.setBonusCount(usersBonusAudit.getBonusCount()+1);
        repository.update(usersBonusAudit);
    }

    @Override
    public int getBonusPerformCount(String username) {
        UsersBonusAudit usersBonusAudit = repository.getLastLoginByUsername(username);
        if (usersBonusAudit == null){
            return 0;
        }

        Calendar currentDate = new GregorianCalendar();
        currentDate.setTime(new Date());

        Calendar bonusDate = new GregorianCalendar();
        bonusDate.setTime(new Date(usersBonusAudit.getLoginDate().getTime()));

        if (currentDate.get(Calendar.YEAR) == bonusDate.get(Calendar.YEAR) &&
                currentDate.get(Calendar.MONTH) == bonusDate.get(Calendar.MONTH) &&
                currentDate.get(Calendar.DATE) == bonusDate.get(Calendar.DATE)){
            return usersBonusAudit.getBonusCount();
        }
        return 0;
    }

    @Override
    public UsersBonusAudit getObjectByUsername(String username) {
        UsersBonusAudit usersBonusAudit = repository.getLastLoginByUsername(username);
        if(usersBonusAudit == null){
            return null;
        }
        Calendar currentDate = new GregorianCalendar();
        currentDate.setTime(new Date());

        Calendar bonusDate = new GregorianCalendar();
        bonusDate.setTime(new Date(usersBonusAudit.getLoginDate().getTime()));

        if (currentDate.get(Calendar.YEAR) == bonusDate.get(Calendar.YEAR) &&
                currentDate.get(Calendar.MONTH) == bonusDate.get(Calendar.MONTH) &&
                currentDate.get(Calendar.DATE) == bonusDate.get(Calendar.DATE)){
            return usersBonusAudit;
        }
        return null;
    }

    @Override
    public UsersBonusAudit getObjectById(long id) {
        return repository.getObjectById(id);
    }
}
