package by.minsk.miroha.services.impl;

import by.minsk.miroha.entities.databases.BrandPlusLicense;
import by.minsk.miroha.entities.databases.SiteChanging;
import by.minsk.miroha.entities.databases.SitePlusLicense;
import by.minsk.miroha.entities.enums.Licenses;
import by.minsk.miroha.entities.front.responses.SiteChangingReport;
import by.minsk.miroha.entities.report.BrandWithLicenses;
import by.minsk.miroha.entities.report.CasinoSite;
import by.minsk.miroha.entities.report.SiteWithLicenses;
import by.minsk.miroha.report.entities.BrandsList;
import by.minsk.miroha.report.entities.SiteList;
import by.minsk.miroha.repositories.report.SitesRepository;
import by.minsk.miroha.services.SitesService;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class SitesServiceImpl implements SitesService {

    private final SitesRepository repository;

    public SitesServiceImpl(SitesRepository repository) {
        this.repository = repository;
    }

    @Override
    public BrandsList getAllBrands() {
        BrandsList brandsList = new BrandsList();
        Set<String> dbLicenses = repository.getLicenses();
        List<String> licenses = new ArrayList<>();
        for(String license : dbLicenses){
            licenses.add(Licenses.convertLicense(license));
        }
        brandsList.setLicenses(licenses);
        List<BrandPlusLicense> brandSet = repository.getAllBrands();
        List<BrandWithLicenses> brandWithLicenses = convertBrandList(brandSet);
        Collections.sort(brandWithLicenses);
        brandsList.setBrands(brandWithLicenses);
        return brandsList;
    }

    @Override
    public SiteList getSitesByBrand(String brand) {
        SiteList siteList = new SiteList();
        Set<String> shortLicense = repository.getLicensesInsideBrand(brand);
        List<String> licenses = new ArrayList<>();
        for(String license : shortLicense){
            licenses.add(Licenses.convertLicense(license));
        }
        siteList.setLicenses(licenses);
        List<SitePlusLicense> sitesSet = repository.getSitesByBrand(brand);
        List<SiteWithLicenses> siteWithLicensesList = convertSiteList(sitesSet);
        siteList.setSites( siteWithLicensesList);
        return siteList;
    }

    @Override
    public List<CasinoSite> getCommonSitesInformation(String site) {
        return repository.getCommonSitesInformation(site);
    }

    @Override
    public Map<String, List<String>> getBrandsWithSites() {
        List<CasinoSite> sites = repository.getAllCasinoSites();
        sites = sites.stream().sorted(Comparator.comparing(CasinoSite::getBrand)).collect(Collectors.toList());
        Map<String, List<String>> sitesStructure = new LinkedHashMap<>();
        for (CasinoSite site: sites){
            if(sitesStructure.containsKey(site.getBrand())){
                List<String> sitesForBrand = sitesStructure.get(site.getBrand());
                sitesForBrand.add(site.getSite());
            } else {
                List<String> sitesForBrand = new ArrayList<>();
                sitesForBrand.add(site.getSite());
                sitesStructure.put(site.getBrand(), sitesForBrand);
            }
        }

        return sitesStructure;
    }

    @Override
    public SiteChangingReport getSiteChanging(Timestamp startDate, Timestamp endDate) {
        List<SiteChanging> removedSites = repository.getRemovedSites(startDate, endDate);
        removedSites = removedSites.stream().sorted(Comparator.comparing(SiteChanging::getSite)).collect(Collectors.toList());
        List<SiteChanging> newSites = repository.getNewSites(startDate, endDate);
        newSites = newSites.stream().sorted(Comparator.comparing(SiteChanging::getSite)).collect(Collectors.toList());
        SiteChangingReport report = new SiteChangingReport();
        report.setRemovedSites(removedSites);
        report.setNewSites(newSites);
        return report;
    }

    @Override
    public List<SiteWithLicenses> getJackpotsWithLicense(List<String> jackpots) {
        List<SitePlusLicense> allSitesPlusLicense = repository.getLicenseForJackpots(jackpots);
        Map<String, Set<String>> mapSitesPlusLicense = new HashMap<>();
        for(SitePlusLicense sitePlusLicense : allSitesPlusLicense){
            if(sitePlusLicense.getLicense() != null){
                if(mapSitesPlusLicense.containsKey(sitePlusLicense.getSite())){
                    mapSitesPlusLicense.get(sitePlusLicense.getSite()).add(Licenses.convertLicense(sitePlusLicense.getLicense()));
                } else {
                    Set<String> licenses = new HashSet<>();
                    licenses.add(Licenses.convertLicense(sitePlusLicense.getLicense()));
                    mapSitesPlusLicense.put(sitePlusLicense.getSite(), licenses);
                }
            }
        }
        List<SiteWithLicenses> siteWithLicenses = new ArrayList<>();
        for(String mapKey : mapSitesPlusLicense.keySet()){
            SiteWithLicenses siteWithLicense = new SiteWithLicenses();
            siteWithLicense.setSite(mapKey);
            siteWithLicense.setLicenses(new ArrayList<>(mapSitesPlusLicense.get(mapKey)));
            siteWithLicenses.add(siteWithLicense);
        }
        return siteWithLicenses;
    }

    private List<BrandWithLicenses> convertBrandList(List<BrandPlusLicense> brandPlusLicenses){
        Map<String, List<String>> mapBrandList = new HashMap<>();
        for (BrandPlusLicense brandPlusLicense : brandPlusLicenses){
            if(brandPlusLicense.getLicense() != null) {
                if (mapBrandList.containsKey(brandPlusLicense.getBrand())) {
                    mapBrandList.get(brandPlusLicense.getBrand()).add(Licenses.convertLicense(brandPlusLicense.getLicense()));
                } else {
                    List<String> licenses = new ArrayList<>();
                    licenses.add(Licenses.convertLicense(brandPlusLicense.getLicense()));
                    mapBrandList.put(brandPlusLicense.getBrand(), licenses);
                }
            }
        }
        List<BrandWithLicenses> brandWithLicenses = new ArrayList<>();
        for(String mapKey : mapBrandList.keySet()){
            if(mapKey != null && !mapKey.equals("")) {
                BrandWithLicenses brandWithLicense = new BrandWithLicenses();
                brandWithLicense.setBrand(mapKey.substring(0, 1).toUpperCase() + mapKey.substring(1));
                brandWithLicense.setLicenses(mapBrandList.get(mapKey));
                brandWithLicenses.add(brandWithLicense);
            }
        }
        return brandWithLicenses;
    }

    private List<SiteWithLicenses> convertSiteList(List<SitePlusLicense> sitePlusLicenses){
        Map<String, List<String>> mapSiteList = new HashMap<>();
        for (SitePlusLicense sitePlusLicense : sitePlusLicenses){
            if(sitePlusLicense.getLicense() != null) {
                if (mapSiteList.containsKey(sitePlusLicense.getSite())) {
                    mapSiteList.get(sitePlusLicense.getSite()).add(Licenses.convertLicense(sitePlusLicense.getLicense()));
                } else {
                    List<String> licenses = new ArrayList<>();
                    licenses.add(Licenses.convertLicense(sitePlusLicense.getLicense()));
                    mapSiteList.put(sitePlusLicense.getSite(), licenses);
                }
            }
        }
        List<SiteWithLicenses> siteWithLicenses = new ArrayList<>();
        for(String mapKey : mapSiteList.keySet()){
            SiteWithLicenses siteWithLicense = new SiteWithLicenses();
            siteWithLicense.setSite(mapKey);
            siteWithLicense.setLicenses(mapSiteList.get(mapKey));
            siteWithLicenses.add(siteWithLicense);
        }
        return siteWithLicenses;
    }
}
