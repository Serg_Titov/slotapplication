package by.minsk.miroha.services.impl;

import java.nio.charset.StandardCharsets;
import java.util.List;

import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.repositories.SlotInfoRepository;
import by.minsk.miroha.repositories.impl.SlotInfoRepositoryImpl;
import by.minsk.miroha.services.SlotInfoService;
import org.springframework.stereotype.Service;

import static by.minsk.miroha.utils.DatabaseUtils.ZERO;

@Service
public class SlotInfoHomeServiceImpl implements SlotInfoService {

    public static final String EMPTY_DB = "База данных пустая! Ее необходимо наполнить!";

    private static final SlotInfoRepository repository = new SlotInfoRepositoryImpl();

    @Override
    public int countAll() {
        int result = repository.countAll();
        if (result == ZERO){
            throw new CustomValidation(EMPTY_DB);
        }
        return result;
    }

    @Override
    public List<Double> getSubsetOfSlotInfo(int limit, int offset) {
        List<Double> subset = repository.getSubsetOfSlotInfo(limit, offset);
        if (subset.isEmpty()){
            throw new CustomValidation(new String(EMPTY_DB.getBytes(), StandardCharsets.UTF_8));
        }
        return subset;
    }

    @Override
    public List<Double> getAllCoefficients() {
        List<Double> allCoefficients = repository.getAllCoef();
        if (allCoefficients.isEmpty()){
            throw new CustomValidation(EMPTY_DB);
        }
        return allCoefficients;
    }

    @Override
    public double getMaxWin() {
        return repository.getMaxWin();
    }

    @Override
    public double getAverageMultiplier() {
        return repository.getAverageMultiplier();
    }

    @Override
    public double getSumAllWins() {
        return repository.getSumAllWins();
    }

    @Override
    public double getSumAllBids() {
        return repository.getSumAllBids();
    }

    @Override
    public double getBonusCount() {
        return repository.getBonusAmount();
    }

    @Override
    public double getWinsWithBonus() {
        return repository.getWinsWithBonus();
    }

    @Override
    public double getAverageBonusWin() {
        return repository.getAverageBonusWin();
    }

    public int getAmountCoefficientUpperValue(double value){
        return repository.getAmountCoefficientUpperValue(value);
    }

    @Override
    public double getBid() {
        return repository.getBid();
    }
}
