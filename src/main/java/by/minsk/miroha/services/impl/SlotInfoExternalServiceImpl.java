package by.minsk.miroha.services.impl;

import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.repositories.SlotInfoExternalRepository;
import by.minsk.miroha.repositories.impl.SlotInfoExternalRepositoryImpl;
import by.minsk.miroha.services.SlotInfoService;

import java.util.List;

import static by.minsk.miroha.services.impl.SlotInfoHomeServiceImpl.EMPTY_DB;
import static by.minsk.miroha.utils.DatabaseUtils.ZERO;

public class SlotInfoExternalServiceImpl implements SlotInfoService {

    private final SlotInfoExternalRepository repository;

    public SlotInfoExternalServiceImpl(String dbName, String tableName) {
        repository = new SlotInfoExternalRepositoryImpl(dbName, tableName);
    }

    @Override
    public int countAll() {
        int result = repository.countAll();
        if (result == ZERO){
            throw new CustomValidation(EMPTY_DB);
        }
        return result;
    }

    @Override
    public List<Double> getSubsetOfSlotInfo(int limit, int offset) {
        List<Double> subset = repository.getSubsetOfSlotInfo(limit, offset);
        if (subset.isEmpty()){
            throw new CustomValidation(EMPTY_DB);
        }
        return subset;
    }

    @Override
    public List<Double> getAllCoefficients() {
        List<Double> allCoefficients = repository.getAllCoef();
        if (allCoefficients.isEmpty()){
            throw new CustomValidation(EMPTY_DB);
        }
        return allCoefficients;
    }

    @Override
    public double getMaxWin() {
        return repository.getMaxWin();
    }

    @Override
    public int getAmountCoefficientUpperValue(double value) {
        return repository.getAmountCoefficientUpperValue(value);
    }

    @Override
    public double getAverageMultiplier() {
        return repository.getAverageMultiplier();
    }

    @Override
    public double getSumAllWins() {
        return repository.getSumAllWins();
    }

    @Override
    public double getSumAllBids() {
        return repository.getSumAllBids();
    }

    @Override
    public double getBonusCount() {
        return repository.getBonusAmount();
    }

    @Override
    public double getWinsWithBonus() {
        return repository.getWinsWithBonus();
    }

    @Override
    public double getAverageBonusWin() {
        return repository.getAverageBonusWin();
    }

    @Override
    public double getBid() {
        return repository.getBid();
    }
}
