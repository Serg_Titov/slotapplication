package by.minsk.miroha.services.impl;

import by.minsk.miroha.entities.SlotInput;
import by.minsk.miroha.entities.enums.UserGrants;
import by.minsk.miroha.repositories.report.SlotInputRepository;
import by.minsk.miroha.repositories.report.impl.SlotInputRepositoryImpl;
import by.minsk.miroha.services.SlotInputService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SlotInputServiceImpl implements SlotInputService {

    private final SlotInputRepository repository = new SlotInputRepositoryImpl();

    @Override
    public SlotInput findById(long idSlotInput) {
        return repository.findById(idSlotInput);
    }

    @Override
    public SlotInput findByAllFields(SlotInput slotInput) {
        List<SlotInput> slotInputList = repository.findByAllFields(slotInput);
        if(slotInputList.size() == 0) {
            return null;
        }
        for(SlotInput slotInputFromList : slotInputList){
            if (slotInput.getUserGrants() == UserGrants.CASINO &&
                    slotInputFromList.getUserGrants() == UserGrants.CASINO){
                return  slotInputFromList;
            }
            if (slotInput.getUserGrants() == UserGrants.DEMO &&
                    slotInputFromList.getUserGrants() != UserGrants.CASINO){
                return slotInputFromList;
            }
            if ((slotInput.getUserGrants() == UserGrants.ADMIN ||
                    slotInput.getUserGrants() == UserGrants.PRO ||
                    slotInput.getUserGrants() == UserGrants.LITE) &&
                    (slotInputFromList.getUserGrants() != UserGrants.CASINO &&
                    slotInputFromList.getUserGrants() != UserGrants.DEMO)){
                return slotInputFromList;
            }
        }
        return null;
    }

    @Override
    public SlotInput save(SlotInput slotInput) {
        return repository.save(slotInput);
    }

    @Override
    public SlotInput update(SlotInput slotInput) {
        return repository.update(slotInput);
    }

    @Override
    public void deleteById(long idSlotInput) {
        repository.deleteById(idSlotInput);
    }

    @Override
    public void truncate() {
        repository.truncate();
    }
}
