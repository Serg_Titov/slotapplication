package by.minsk.miroha.services.impl;

import by.minsk.miroha.entities.common.Provider;
import by.minsk.miroha.entities.common.ProviderWithSlots;
import by.minsk.miroha.entities.common.Slot;
import by.minsk.miroha.entities.databases.SlotName;
import by.minsk.miroha.entities.enums.SystemFolders;
import by.minsk.miroha.perform.SlotDataUploader;
import by.minsk.miroha.repositories.games.GamesRepository;
import by.minsk.miroha.repositories.games.impl.GamesRepositoryImpl;
import by.minsk.miroha.services.GamesService;
import org.springframework.stereotype.Service;


import java.util.*;
import java.util.stream.Collectors;

@Service
public class GamesServiceImpl implements GamesService {

    private final GamesRepository repository = new GamesRepositoryImpl();

    @Override
    public Map<String, List<String>> getAllGames(List<String> providers) {
        return repository.getAllGames(providers);
    }

    @Override
    public List<String> getGamesByProvider(String provider) {
        return repository.getGamesByProvider(provider);
    }

    @Override
    public List<Slot> getFrontGamesByProvider(String provider) {
        List<String> games = repository.getGamesByProvider(provider);
        games = games.stream().sorted().collect(Collectors.toList());
        return games.stream().map(Slot::new).collect(Collectors.toList());
    }

    @Override
    public List<String> getAllProviders() {
        Set<String> tables = repository.getAllProviders();
        List<String> resultTables = removeSystemFolders(tables);
        resultTables = resultTables.stream().sorted().collect(Collectors.toList());
        return resultTables;
    }

    @Override
    public void calculateAllProviders() {
        List<String> providers = getAllProviders();
        for(String provider: providers) {
            List<String> games = getGamesByProvider(provider);
            for (String game : games) {
                SlotDataUploader dataUploader = new SlotDataUploader();
                dataUploader.uploadSlotInformation(game, provider, true);
            }
        }
    }

    @Override
    public List<Provider> getAllFrontProviders() {
        Set<String> tables = repository.getAllProviders();
        List<String> resultTables = removeSystemFolders(tables);
        resultTables = resultTables.stream().sorted().collect(Collectors.toList());
        return resultTables.stream().map(Provider::new).collect(Collectors.toList());
    }

    @Override
    public long getSpinAmount(String slotFullName) {
        return repository.getTotalSpinAmount(slotFullName);
    }

    @Override
    public Date getCreationTableDate(String provider, String slotName) {
        return repository.getCreationTableDate(provider, slotName);
    }

    private List<String> removeSystemFolders(Set<String> allTables){
        List<String> tables = new ArrayList<>();
        for (String table : allTables){
            if (SystemFolders.findByName(table) == null){
                tables.add(table);
            }
        }
        return tables;
    }

    @Override
    public List<ProviderWithSlots> getProvidersWithSlots() {
        List<SlotName> slotNameList = repository.getAllProvidersWithSlots();
        Map<String, List<Slot>> providerSlotsMap = new HashMap<>();
        for(SlotName slotName : slotNameList){
            if(SystemFolders.findByName(slotName.getProvider()) == null){
                if(providerSlotsMap.containsKey(slotName.getProvider())){
                    providerSlotsMap.get(slotName.getProvider()).add(new Slot(slotName.getSlotName()));
                } else {
                    List<Slot> slotNames = new ArrayList<>();
                    slotNames.add(new Slot(slotName.getSlotName()));
                    providerSlotsMap.put(slotName.getProvider(), slotNames);
                }
            }
        }
        List<ProviderWithSlots> providerWithSlotsList = new ArrayList<>();
        for(String provider : providerSlotsMap.keySet()){
            ProviderWithSlots providerWithSlots = new ProviderWithSlots();
            providerWithSlots.setProvider(new Provider(provider));
            List<Slot> slots = providerSlotsMap.get(provider);
            Collections.sort(slots);
            providerWithSlots.setSlots(slots);
            providerWithSlotsList.add(providerWithSlots);
        }
        Collections.sort(providerWithSlotsList);
        return providerWithSlotsList;
    }
}
