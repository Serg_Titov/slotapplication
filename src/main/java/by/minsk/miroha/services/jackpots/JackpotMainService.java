package by.minsk.miroha.services.jackpots;

import by.minsk.miroha.entities.common.Site;
import by.minsk.miroha.entities.enums.IncreasePeriod;
import by.minsk.miroha.entities.enums.jackpots.JackpotSlot;
import by.minsk.miroha.entities.enums.TimePeriods;
import by.minsk.miroha.entities.front.JackpotPath;
import by.minsk.miroha.entities.front.responses.JackpotGraphResponse;
import by.minsk.miroha.report.jackpot.*;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

public interface JackpotMainService {

    int countUniqueJackpots(String slotName);

    Date getLastDate(String slotName);

    MaxJackpotValue getMaxCurrentJackpotValue(String slotName, Date lastDate);

    List<JackpotParameter> getLastPaidOut(String slotName);

    MaxJackpotValue getMaxPaidOutValue(String slotName);

    List<JackpotParameter> getTopRTPSlots(JackpotSlot slot, Date lastDate);

    List<Site> getJackpotsSites(String slotName);

    List<GrownJackpots> getGrownJackpots(TimePeriods timePeriods, String slotName);

    JackpotGraphResponse getGraphPoints(JackpotSlot slotName, String site, Timestamp dateStart, Timestamp dateEnd, IncreasePeriod increasePeriod);

    List<JackpotPath> getJackpotPaths(String siteName);

    List<JackpotSite> getAllJackpotsSites(String slotName);

    List<JackpotParameter> getFirstJackpotEntries();

    List<JackpotParameter> getMaxPaidOutJackpots();
}
