package by.minsk.miroha.services.jackpots;

import by.minsk.miroha.entities.databases.JackpotSummary;
import by.minsk.miroha.entities.enums.jackpots.JackpotMultipleSlot;

import java.util.List;

public interface JackpotSummaryService {

    JackpotSummary getLastSummary(String slot);

    List<JackpotSummary> getMultipleLastSummary(List<JackpotMultipleSlot> slots);

    JackpotSummary saveSummary(JackpotSummary jackpotSummary);

}
