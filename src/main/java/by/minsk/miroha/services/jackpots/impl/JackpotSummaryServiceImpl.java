package by.minsk.miroha.services.jackpots.impl;

import by.minsk.miroha.entities.databases.JackpotSummary;
import by.minsk.miroha.entities.enums.jackpots.JackpotMultipleSlot;
import by.minsk.miroha.repositories.jackpots.JackpotSummaryRepository;
import by.minsk.miroha.services.jackpots.JackpotSummaryService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class JackpotSummaryServiceImpl implements JackpotSummaryService {

    private final JackpotSummaryRepository summaryRepository;

    public JackpotSummaryServiceImpl(JackpotSummaryRepository summaryRepository) {
        this.summaryRepository = summaryRepository;
    }

    @Override
    public JackpotSummary getLastSummary(String slot) {
        return summaryRepository.getLastSummary(slot);
    }

    @Override
    public List<JackpotSummary> getMultipleLastSummary(List<JackpotMultipleSlot> slots) {
        List<String> slotNames = slots.stream().map(JackpotMultipleSlot::getSlotName).collect(Collectors.toList());
        return summaryRepository.getMultipleLastSummary(slotNames);
    }

    @Override
    public JackpotSummary saveSummary(JackpotSummary jackpotSummary) {
        return summaryRepository.saveSummary(jackpotSummary);
    }

}
