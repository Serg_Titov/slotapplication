package by.minsk.miroha.services.jackpots.impl;

import by.minsk.miroha.entities.JackpotMultipleValue;
import by.minsk.miroha.entities.common.Site;
import by.minsk.miroha.entities.enums.IncreasePeriod;
import by.minsk.miroha.entities.enums.jackpots.JackpotMultipleSlot;
import by.minsk.miroha.entities.enums.jackpots.JackpotSlot;
import by.minsk.miroha.entities.enums.TimePeriods;
import by.minsk.miroha.entities.front.JackpotPath;
import by.minsk.miroha.entities.front.responses.JackpotGraphResponse;
import by.minsk.miroha.entities.front.responses.SlotPoints;
import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.report.jackpot.*;
import by.minsk.miroha.repositories.jackpots.JackpotMainRepository;
import by.minsk.miroha.services.jackpots.JackpotMainService;
import by.minsk.miroha.utils.CommonUtils;
import by.minsk.miroha.utils.Converters;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class JackpotMainServiceImpl implements JackpotMainService {

    public static final long DAY_IN_MILLIS = 24*60*60*1000;
    public static final long HOUR_IN_MILLIS = 60*60*1000;

    private final JackpotMainRepository repository;

    public JackpotMainServiceImpl(JackpotMainRepository repository) {
        this.repository = repository;
    }

    @Override
    public int countUniqueJackpots(String slotName) {
        return repository.countUniqueJackpots(slotName);
    }

    @Override
    public Date getLastDate(String slotName) {
        Timestamp ts = repository.getLastDate(slotName);
        return new Date(ts.getTime()-3*HOUR_IN_MILLIS);
    }

    @Override
    public MaxJackpotValue getMaxCurrentJackpotValue(String slotName, Date lastDate) {
        java.sql.Date sqlDate = new java.sql.Date(lastDate.getTime());
        List<JackpotParameter> parameters = repository.getMaxJackpotValueToday(slotName, sqlDate);
        double maxValue = parameters.stream().map(JackpotParameter::getValueFirst).reduce(Double::max).orElse(0D);
        for(JackpotParameter parameter : parameters){
            if(parameter.getValueFirst() == maxValue){
                return fillMaxJackpotValue(parameter ,slotName);
            }
        }
        return null;
    }

    @Override
    public List<JackpotParameter> getLastPaidOut(String slotName) {
        return repository.getLastPaidOut(slotName);
    }

    @Override
    public MaxJackpotValue getMaxPaidOutValue(String slotName) {
        return fillMaxJackpotValue(repository.getMaxPaidOut(slotName), slotName);
    }

    @Override
    public List<JackpotParameter> getTopRTPSlots(JackpotSlot slot, Date lastDate) {
        java.sql.Date sqlDate = new java.sql.Date(lastDate.getTime());
        List<JackpotParameter> jackpotParameters = new ArrayList<>();
        if(slot == null){
            jackpotParameters = repository.getTopRTPSlots(null, sqlDate);
            jackpotParameters = calculateTotalRtp(jackpotParameters);
        } else {
            JackpotMultipleSlot multipleSlot = JackpotSlot.getMultipleSlot(slot);
            if (multipleSlot == null) {
                jackpotParameters = repository.getTopRTPSlots(slot.getSlotName(), sqlDate);
                jackpotParameters.forEach(parameter -> parameter.setRtp(JackpotSlot.calculateRtp(slot, parameter.getValueFirst())));
            } else {
                for (JackpotMultipleSlot jackpotSlot : JackpotMultipleSlot.getListSlots(slot)) {
                    jackpotParameters.addAll(repository.getTopRTPSlots(jackpotSlot.getSlotName(), sqlDate));
                }
                jackpotParameters = calculateMultipleRTP(slot, jackpotParameters, multipleSlot);
            }
        }
        return jackpotParameters;
    }

    @Override
    public List<Site> getJackpotsSites(String slotName) {
        Set<String> sites = repository.getJackpotSites(slotName);
        return sites.stream().map(Site::new).sorted(Site.COMPARE_BY_SITE).collect(Collectors.toList());
    }

    @Override
    public List<GrownJackpots> getGrownJackpots(TimePeriods timePeriods, String slotName) {
        List<GrownJackpots> minMaxList = repository.getGrownJackpots(timePeriods.getDays(), slotName);
        List<GrownJackpots> resultList = new ArrayList<>();
        while(minMaxList.size() > 0){
            GrownJackpots firstJackpots = minMaxList.get(0);
            for(int i = 1; i < minMaxList.size(); i++){
                GrownJackpots secondJackpots = minMaxList.get(i);
                if(firstJackpots.getBrand().equals(secondJackpots.getBrand()) &&
                        firstJackpots.getSite().equals(secondJackpots.getSite())){
                    GrownJackpots resultJackpot = new GrownJackpots();
                    resultJackpot.setSlotName(firstJackpots.getSlotName());
                    resultJackpot.setBrand(firstJackpots.getBrand());
                    resultJackpot.setSite(CommonUtils.cleanURLPath(firstJackpots.getSite()));
                    if(firstJackpots.getId() > secondJackpots.getId()){
                        resultJackpot.setGrownValue(firstJackpots.getGrownValue()- secondJackpots.getGrownValue());
                    } else {
                        resultJackpot.setGrownValue(secondJackpots.getGrownValue()- firstJackpots.getGrownValue());
                    }
                    if(resultJackpot.getGrownValue()<0){
                        resultJackpot.setGrownValue(0);
                    }
                    resultList.add(resultJackpot);
                    minMaxList.remove(i);
                    break;
                }
            }
            minMaxList.remove(0);
        }
        return resultList.stream().sorted(GrownJackpots.COMPARE_BY_VALUE).limit(5).collect(Collectors.toList());
    }

    @Override
    public JackpotGraphResponse getGraphPoints(JackpotSlot slot, String site, Timestamp dateStart, Timestamp dateEnd, IncreasePeriod increasePeriod) {
        JackpotMultipleSlot multipleSlot = JackpotSlot.getMultipleSlot(slot);
        if(multipleSlot == null){
            return getSingleSlotResponse(slot, site, dateStart, dateEnd,increasePeriod);
        } else {
            return getMultipleSlotResponse(slot, site, dateStart, dateEnd, increasePeriod);
        }
    }

    @Override
    public List<JackpotPath> getJackpotPaths(String siteName) {
        List<JackpotPath> paths = repository.getJackpotPath(siteName);
        paths.forEach(path -> path.setSlotName(Converters.convertJackpotSlotName(path.getSlotName())));
        return paths;
    }

    @Override
    public List<JackpotSite> getAllJackpotsSites(String slotName) {
        return repository.getAllJackpotsSites(slotName);
    }

    @Override
    public List<JackpotParameter> getFirstJackpotEntries() {
        return repository.getFirstJackpotsEntry();
    }

    @Override
    public List<JackpotParameter> getMaxPaidOutJackpots() {
        return repository.getMaxPaidOutJackpots();
    }

    private MaxJackpotValue fillMaxJackpotValue(JackpotParameter parameter, String slotName){
        MaxJackpotValue maxValue = new MaxJackpotValue();
        maxValue.setMaxValue(parameter.getValueFirst());
        maxValue.setSite(CommonUtils.cleanURLPath(parameter.getSite().getFullUrl()));
        maxValue.setBrand(parameter.getBrand());
        maxValue.setSlotName(slotName);
        return maxValue;
    }

    private List<JackpotParameter> calculateMultipleRTP(JackpotSlot slot, List<JackpotParameter> parameters,
                                                        JackpotMultipleSlot jackpotMultipleSlot){
        List<JackpotParameter> resultList = new ArrayList<>();
        for(JackpotParameter parameter : parameters){
            if(parameter.getSlotName().equals(slot.getSlotName())){
                List<JackpotMultipleValue> jackpotValues = new ArrayList<>();
                JackpotMultipleValue multipleValue = new JackpotMultipleValue(jackpotMultipleSlot, parameter.getValueFirst());
                jackpotValues.add(multipleValue);
                for (JackpotParameter findValue : parameters){
                    if(findValue.getSite().equals(parameter.getSite())
                            && findValue.getBrand().equals(parameter.getBrand())
                            && !(findValue.getSlotName().equals(parameter.getSlotName()))){
                        JackpotMultipleValue jackpotMultipleValue =
                                new JackpotMultipleValue(JackpotMultipleSlot.findByName(findValue.getSlotName()),
                                        findValue.getValueFirst());
                        jackpotValues.add(jackpotMultipleValue);

                    }
                }
                parameter.setRtp(JackpotMultipleSlot.calculateRtp(jackpotMultipleSlot, jackpotValues));
                resultList.add(parameter);
            }
        }
        return resultList;
    }

    private List<JackpotParameter> calculateTotalRtp(List<JackpotParameter> parameters){
        List<JackpotParameter> resultList = new ArrayList<>();
        for(JackpotParameter parameter : parameters){
            try {
                JackpotSlot slot = JackpotSlot.findByName(parameter.getSlotName());
                parameter.setRtp(JackpotSlot.calculateRtp(slot, parameter.getValueFirst()));
            } catch (CustomValidation ex) {
                JackpotSlot slot = JackpotSlot.getMultipleSlot(parameter.getSlotName());
                JackpotMultipleSlot multipleSlot = JackpotMultipleSlot.findByName(parameter.getSlotName());
                List<String> multipleSlots = JackpotMultipleSlot.getListSlots(slot).stream()
                        .map(JackpotMultipleSlot::getSlotName).collect(Collectors.toList());
                List<JackpotMultipleValue> jackpotValues = new ArrayList<>();
                JackpotMultipleValue multipleValue = new JackpotMultipleValue(multipleSlot, parameter.getValueFirst());
                jackpotValues.add(multipleValue);
                for(JackpotParameter findParameter : parameters){
                    if(findParameter.getSite().equals(parameter.getSite())
                            && findParameter.getBrand().equals(parameter.getBrand())
                            && !(findParameter.getSlotName().equals(parameter.getSlotName()))
                            && multipleSlots.contains(findParameter.getSlotName())){
                        JackpotMultipleValue jackpotMultipleValue =
                                new JackpotMultipleValue(JackpotMultipleSlot.findByName(findParameter.getSlotName()),
                                        findParameter.getValueFirst());
                        jackpotValues.add(jackpotMultipleValue);
                    }
                }
                parameter.setRtp(JackpotMultipleSlot.calculateRtp(multipleSlot, jackpotValues));
            }
            resultList.add(parameter);
        }
        resultList.sort((o1, o2) -> Double.compare(o2.getValueFirst(), o1.getValueFirst()));
        return resultList;
    }

    private JackpotGraphResponse getSingleSlotResponse(JackpotSlot slot, String site, Timestamp dateStart,
                                                       Timestamp dateEnd, IncreasePeriod increasePeriod){
        JackpotGraphResponse response = new JackpotGraphResponse();
        response.setSite(site);
        List<JackpotGraphPoint> points = repository.getGraphPoint(slot.getSlotName(), site, dateStart, dateEnd);
        JackpotGraphPoint firstPoint = new JackpotGraphPoint();
        double increaseValue = 0;
        List<JackpotGraphPointFront> frontPoints = new ArrayList<>();
        List<JackpotGraphPointFront> increasePoints = new ArrayList<>();
        if(points.size() > 0){
            firstPoint = points.get(0);

            frontPoints.add(createGraphPoint(firstPoint));
        }
        for(int i = 1; i < points.size()-1; i++){
            JackpotGraphPoint currentPoint = points.get(i);
            JackpotGraphPoint previousPoint = points.get(i-1);
            SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");

            if(((previousPoint.getJackpotValue()- currentPoint.getJackpotValue())/currentPoint.getJackpotValue() > 0.6) &&
                    (currentPoint.getLastPayedOut() != 0)){
                frontPoints.add(createAdditionalPoint(currentPoint));
                increaseValue = increaseValue + currentPoint.getLastPayedOut() - previousPoint.getJackpotValue();
            } else {
                increaseValue = increaseValue + currentPoint.getJackpotValue() - previousPoint.getJackpotValue();
            }

            frontPoints.add(createGraphPoint(currentPoint));

            if(currentPoint.getTimestamp().getTime() - firstPoint.getTimestamp().getTime()
                    > increasePeriod.getDays()*DAY_IN_MILLIS){
                JackpotGraphPointFront increasePoint = new JackpotGraphPointFront();
                if(increaseValue > 0) {
                    increasePoint.setJackpotValue(String.format("%.1f", increaseValue));
                } else {
                    increasePoint.setJackpotValue("0");
                }

                increasePoint.setDate(formatter.format(new Date(currentPoint.getTimestamp().getTime())));
                increasePoints.add(increasePoint);
                firstPoint = currentPoint;
                increaseValue = 0;
            }
        }

        SlotPoints slotPoints = new SlotPoints();
        slotPoints.setSlotName(slot.getSlotName());
        slotPoints.setGraphPoints(frontPoints);
        List<SlotPoints> slotPointsList = new ArrayList<>();
        slotPointsList.add(slotPoints);

        response.setSlotPoints(slotPointsList);
        response.setIncreasePoints(increasePoints);
        return response;
    }

    private JackpotGraphResponse getMultipleSlotResponse(JackpotSlot slot, String site, Timestamp dateStart,
                                                         Timestamp dateEnd, IncreasePeriod increasePeriod){
        JackpotGraphResponse response = new JackpotGraphResponse();
        response.setSite(site);

        List<JackpotMultipleSlot> multipleSlots = JackpotMultipleSlot.getListSlots(slot);
        List<SlotPoints> slotPointsList = new ArrayList<>();
        List<JackpotGraphPointFront> increasePoints = new ArrayList<>();
        for(JackpotMultipleSlot multipleSlot : multipleSlots){
            List<JackpotGraphPoint> graphPoints = repository.getGraphPoint(multipleSlot.getSlotName(), site, dateStart, dateEnd);
            SlotPoints slotPoints = new SlotPoints();
            slotPoints.setSlotName(multipleSlot.getSlotName());

            JackpotGraphPoint firstPoint = new JackpotGraphPoint();
            double increaseValue = 0;
            List<JackpotGraphPointFront> frontPoints = new ArrayList<>();
            if(graphPoints.size() > 0){
                firstPoint = graphPoints.get(0);
                frontPoints.add(createGraphPoint(firstPoint));
            }

            for(int i = 1; i < graphPoints.size()-1; i++){
                JackpotGraphPoint currentPoint = graphPoints.get(i);
                JackpotGraphPoint previousPoint = graphPoints.get(i-1);
                SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");

                if(((previousPoint.getJackpotValue()- currentPoint.getJackpotValue())/currentPoint.getJackpotValue() > 0.6) &&
                        (currentPoint.getLastPayedOut() != 0)){
                    frontPoints.add(createAdditionalPoint(currentPoint));
                    increaseValue = increaseValue + currentPoint.getLastPayedOut() - previousPoint.getJackpotValue();
                } else if(!((previousPoint.getJackpotValue()- currentPoint.getJackpotValue())/currentPoint.getJackpotValue() > 0.6)) {
                    increaseValue = increaseValue + currentPoint.getJackpotValue() - previousPoint.getJackpotValue();
                }

                frontPoints.add(createGraphPoint(currentPoint));

                if(currentPoint.getTimestamp().getTime() - firstPoint.getTimestamp().getTime()
                        > increasePeriod.getDays()*DAY_IN_MILLIS){
                    String incDate = formatter.format(new Date(currentPoint.getTimestamp().getTime()));
                    boolean findInc = false;
                    for(JackpotGraphPointFront incPoint : increasePoints){
                        if(incPoint.getDate().equals(incDate)){
                            double incValue;
                            try {
                                incValue = Double.parseDouble(incPoint.getJackpotValue());
                            } catch (NumberFormatException ex){
                                incValue = 0;
                            }
                            incValue = incValue + increaseValue;
                            incPoint.setJackpotValue(String.format("%.1f", incValue));
                            findInc = true;
                        }
                    }
                    if(!findInc) {
                        JackpotGraphPointFront increasePoint = new JackpotGraphPointFront();
                        if(increaseValue > 0) {
                            increasePoint.setJackpotValue(String.format("%.1f", increaseValue));
                        } else {
                            increasePoint.setJackpotValue("0");
                        }
                        increasePoint.setDate(incDate);
                        increasePoints.add(increasePoint);
                    }
                    firstPoint = currentPoint;
                    increaseValue = 0;
                }
            }

            slotPoints.setGraphPoints(frontPoints);
            slotPointsList.add(slotPoints);
        }

        response.setSlotPoints(slotPointsList);
        response.setIncreasePoints(increasePoints);
        return response;
    }

    private JackpotGraphPointFront createAdditionalPoint(JackpotGraphPoint currentPoint){
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        JackpotGraphPointFront additionalPoint = new JackpotGraphPointFront();
        additionalPoint.setDate(formatter.format(new Date(currentPoint.getTimestamp().getTime())));
        additionalPoint.setJackpotValue(String.format("%.1f", currentPoint.getLastPayedOut()));
        return additionalPoint;
    }

    private JackpotGraphPointFront createGraphPoint(JackpotGraphPoint currentPoint){
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        JackpotGraphPointFront frontPoint = new JackpotGraphPointFront();
        frontPoint.setJackpotValue(String.format("%.1f", currentPoint.getJackpotValue()));
        frontPoint.setDate(formatter.format(new Date(currentPoint.getTimestamp().getTime())));
        return frontPoint;
    }
}
