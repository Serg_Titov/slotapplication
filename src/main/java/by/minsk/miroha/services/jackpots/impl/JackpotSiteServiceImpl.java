package by.minsk.miroha.services.jackpots.impl;

import by.minsk.miroha.report.jackpot.JackpotParameter;
import by.minsk.miroha.repositories.jackpots.JackpotSiteRepository;
import by.minsk.miroha.repositories.jackpots.impl.JackpotSiteRepositoryImpl;
import by.minsk.miroha.services.jackpots.JackpotSiteService;

import java.util.List;

public class JackpotSiteServiceImpl implements JackpotSiteService {

    private final JackpotSiteRepository repository = new JackpotSiteRepositoryImpl();

    @Override
    public long getCurrentJackpot(String slot, String site) {
        Double currentJackpot = repository.getCurrentJackpot(slot, site);
        return currentJackpot == null ? 0 : Math.round(currentJackpot);
    }

    @Override
    public long getMaxPayedOutJackpot(String slot, String site) {
        Double maxPayedOutJackpot = repository.getMaxPayedOutJackpot(slot, site);
        return maxPayedOutJackpot == null ? 0 : Math.round(maxPayedOutJackpot);
    }

    @Override
    public long getMinPayedOutJackpot(String slot, String site) {
        Double minPayedOutJackpot = repository.getMinPayedOutJackpot(slot, site);
        return minPayedOutJackpot == null ? 0 : Math.round(minPayedOutJackpot);
    }

    @Override
    public List<JackpotParameter> getFullInformation(String slot, String site) {
        return repository.getFullInformation(slot, site);
    }

}
