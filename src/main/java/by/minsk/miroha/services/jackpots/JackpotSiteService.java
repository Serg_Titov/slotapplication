package by.minsk.miroha.services.jackpots;

import by.minsk.miroha.report.jackpot.JackpotParameter;

import java.util.List;

public interface JackpotSiteService {

    long getCurrentJackpot(String slot, String site);

    long getMaxPayedOutJackpot(String slot, String site);

    long getMinPayedOutJackpot(String slot, String site);

    List<JackpotParameter> getFullInformation(String slot, String site);

}
