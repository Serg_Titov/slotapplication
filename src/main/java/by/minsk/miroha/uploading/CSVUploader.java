package by.minsk.miroha.uploading;

import au.com.bytecode.opencsv.CSVReader;
import by.minsk.miroha.entities.databases.SlotInfoHome;
import by.minsk.miroha.repositories.SlotInfoRepository;
import by.minsk.miroha.repositories.impl.SlotInfoRepositoryImpl;

import java.util.ArrayList;
import java.util.List;
import java.io.FileReader;
import java.io.IOException;

import static java.lang.Double.*;

/**
 * Класс для считывания информации об результатах работы слота и
 * записи в локальную БД.
 */
public class CSVUploader {

    private static final int ID_RUN_INDEX = 0;
    private static final int BID_INDEX = 1;
    private static final int WIN_INDEX = 2;
    private static final int CREDIT_INDEX = 3;
    private static final int BONUS = 4;
    private static final int IP = 5;
    private static final int GAME = 6;
    private static final int CONTAINER_SIZE = 100000;
    private static final int CYCLE_INITIAL_VALUE = 0;

    private static final SlotInfoRepository repository = new SlotInfoRepositoryImpl();

    /**
     * Построчно считывает информацию из файла. Когда считает CONTAINER_SIZE строк,
     * записывает их в локальную базу. В случае "битой" строки в файле, данная строка
     * игнорируется.
     *
     * @throws IOException ошибка чтения из файла
     */
    public static void uploadSlotInfoToHSQLDB() throws IOException {
        CSVReader reader = new CSVReader(new FileReader("maintbl.csv"), ';', '"',1);
        String[] nextLine;
        int cycleCounter = CYCLE_INITIAL_VALUE;
        List<SlotInfoHome> slotInfos = new ArrayList<>();
        while ((nextLine = reader.readNext()) != null){
            try{
                SlotInfoHome slotInfoHome = getSlotInfoHome(nextLine);
                slotInfos.add(slotInfoHome);
            } catch (Exception ignored){
            }
            if (cycleCounter == CONTAINER_SIZE){
                repository.save(slotInfos);
                slotInfos.clear();
                cycleCounter = CYCLE_INITIAL_VALUE;
            }
            else {
                cycleCounter++;
            }
        }
        repository.save(slotInfos);
        reader.close();
    }

    /**
     * В зависимости от количества столбцов в файле, выбирает нужный метод
     * для заполнения объекта результата работы одного оборота слота.
     *
     * @param nextLine строка из файла
     * @return заполененный объект информации об одном обороте слота
     */
    private static SlotInfoHome getSlotInfoHome(String[] nextLine) {
        SlotInfoHome slotInfoHome;
        switch (nextLine.length) {
            case 4 : slotInfoHome = fillShortSlotInfoFromCSV(nextLine); break;
            case 5 : slotInfoHome = fillSlotInfoBonusFromCSV(nextLine); break;
            case 6 : slotInfoHome = fillSlotInfoIpFromCSV(nextLine); break;
            case 7 : slotInfoHome = fillFullSlotInfoFromCSV(nextLine); break;
            default : slotInfoHome = new SlotInfoHome();
        }
        return slotInfoHome;
    }

    /**
     * Считывает строку информации об результате одного оборота слота
     * в случае если в файле первые 4 столбца.
     *
     * @param nextLine строка с информацией
     * @return заполененный объект информации об одном обороте слота
     */
    private static SlotInfoHome fillShortSlotInfoFromCSV(String[] nextLine) {
        SlotInfoHome slotInfo = new SlotInfoHome();
        double winCoefficient;
        slotInfo.setIdSlotInfo(Long.parseLong(nextLine[ID_RUN_INDEX]));
        slotInfo.setBid(parseDouble(nextLine[BID_INDEX].replace(",",".")));
        slotInfo.setWin(parseDouble(nextLine[WIN_INDEX].replace(",",".")));
        slotInfo.setCredit(parseDouble(nextLine[CREDIT_INDEX].replace(",",".")));
        if(slotInfo.getBid()!=0){
            winCoefficient = slotInfo.getWin()/slotInfo.getBid();
        }else {
            winCoefficient= 0D;
        }
        slotInfo.setWinCoefficient(winCoefficient);
        return slotInfo;
    }

    /**
     * Считывает строку информации об результате одного оборота слота
     * в случае если в файле первые 5 столбцов.
     *
     * @param nextLine строка с информацией
     * @return заполененный объект информации об одном обороте слота
     */
    private static SlotInfoHome fillSlotInfoBonusFromCSV(String[] nextLine){
        SlotInfoHome slotInfo = fillShortSlotInfoFromCSV(nextLine);
        slotInfo.setBonus(parseDouble(nextLine[BONUS].replace(",", ".")));
        return slotInfo;
    }

    /**
     * Считывает строку информации об результате одного оборота слота
     * в случае если в файле первые 6 столбцов.
     *
     * @param nextLine строка с информацией
     * @return заполененный объект информации об одном обороте слота
     */
    private static SlotInfoHome fillSlotInfoIpFromCSV(String[] nextLine){
        SlotInfoHome slotInfoHome = fillSlotInfoBonusFromCSV(nextLine);
        slotInfoHome.setIp(nextLine[IP]);
        return slotInfoHome;
    }

    /**
     * Считывает строку информации об результате одного оборота слота
     * в случае если в файле первые 7 столбцов.
     *
     * @param nextLine строка с информацией
     * @return заполененный объект информации об одном обороте слота
     */
    private static SlotInfoHome fillFullSlotInfoFromCSV(String[] nextLine){
        SlotInfoHome slotInfoHome = fillSlotInfoIpFromCSV(nextLine);
        slotInfoHome.setGame(nextLine[GAME]);
        return slotInfoHome;
    }
}