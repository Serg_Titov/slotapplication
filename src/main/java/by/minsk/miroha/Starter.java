package by.minsk.miroha;

import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Set;

import by.minsk.miroha.entities.SlotInput;
import by.minsk.miroha.entities.cashback.CashBack;
import by.minsk.miroha.entities.databases.SlotInfoHome;
import by.minsk.miroha.entities.databases.SlotName;
import by.minsk.miroha.entities.enums.WaggerType;
import by.minsk.miroha.entities.report.Multipliers;
import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.perform.CashbackPerformer;
import by.minsk.miroha.perform.Performer;
import by.minsk.miroha.perform.SpinnerPerformer;
import by.minsk.miroha.perform.UnloaderFromHomeDB;
import by.minsk.miroha.report.CashbackReport;
import by.minsk.miroha.repositories.SlotNameRepository;
import by.minsk.miroha.repositories.impl.SlotNameRepositoryImpl;
import by.minsk.miroha.repositories.report.*;
import by.minsk.miroha.repositories.SlotInfoRepository;
import by.minsk.miroha.repositories.report.impl.*;
import by.minsk.miroha.repositories.impl.SlotInfoPragmaticRepositoryImpl;
import by.minsk.miroha.repositories.impl.SlotInfoRepositoryImpl;
import by.minsk.miroha.uploading.CSVUploader;
import by.minsk.miroha.utils.DatabaseUtils;

import static by.minsk.miroha.repositories.report.impl.CommonSlotInfoRepositoryImpl.DB_ERROR;


public class Starter {

    static SlotInfoRepository repository = new SlotInfoRepositoryImpl();

    public static void main(String[] args) {
        
        //testHomeDB();
        //testCashbackPerformer();
        //requestToDatabase();
        deleteLine();
        showTables();
        //downloadFromCSV();
        //connectToMySql();
        //testPragmaticProfit();
        //testCoef();
        //testPerformance();
        //testUnloadCoefficients();
        //testHomePerformer();
        //testArrayPerformance();
        //testDispersion();
        //testSpinnerPerformer();
        //testSlotInputRepository();
    }

    private static void showTables(){
        SlotInformationRepository repository = new SlotInformationRepositoryImpl();
        for (int i = 50; i< 100 ; i++){
            System.out.println(repository.getObjectById(i));
        }
        /*SlotInputRepository repository1 = new SlotInputRepositoryImpl();
        System.out.println(repository1.findById(4));
        SlotInput slotInput = new SlotInput();
        slotInput.setIdInput(4L);
        slotInput.setDeposit(100);
        slotInput.setBonus(100);
        slotInput.setWithoutBonus(false);
        slotInput.setBet(5);
        slotInput.setWagger(35);
        slotInput.setWaggerType(WaggerType.DB);
        slotInput.setSim(1000000);
        slotInput.setProfit(0);
        slotInput.setBalance();
        slotInput.setManual(false);
        slotInput.setGame("Syncronite Yggdrasil");
        repository1.update(slotInput);
        System.out.println(repository1.findById(4));
        for (int i = 200; i < 230; i++){
            System.out.println(repository1.findById(i));
        }*/
    }



    private static void deleteLine(){
        int id = 87;
        /*CommonSimInfoRepository repository = new CommonSimInfoRepositoryImpl();
        repository.delete(id);
        NonProfitSeriesRepository repository1 = new NonProfitSeriesRepositoryImpl();
        repository1.delete(id);
        TopProfitsRepository repository2 = new TopProfitsRepositoryImpl();
        repository2.delete(id);
        DepositMultiplierRepository repository3 = new DepositMultiplierRepositoryImpl();
        repository3.delete(id);
        SlotPointsRepository repository4 = new SlotPointsRepositoryImpl();
        repository4.delete("Test_delete1", "Test_delete1");
        SlotInputRepository repository7 = new SlotInputRepositoryImpl();
        repository7.deleteById(id);*/
        CommonSlotInfoRepository repository4 = new CommonSlotInfoRepositoryImpl();
        repository4.delete(id);
        DatabaseMultipliersRepository repository5 = new DatabaseMultiPliersRepositoryImpl();
        repository5.delete(id);
        SlotInformationRepository repository6 = new SlotInformationRepositoryImpl();
        repository6.delete(id);
    }

    private static void requestToDatabase(){
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DatabaseUtils.getMySqlConnection();
            //preparedStatement = connection.prepareStatement(" ;");
            preparedStatement = connection.prepareStatement("ALTER TABLE Slots.common_sim_info\n" +
                    "ADD bonus_volatility double precision NULL;");
            preparedStatement.executeUpdate();
        } catch (ClassNotFoundException | SQLException throwable) {
            throwable.printStackTrace();
            throw new CustomValidation(new String(DB_ERROR.getBytes(), StandardCharsets.UTF_8));
        } finally {
            DatabaseUtils.closeConnectionPrepStatement(connection, preparedStatement);
        }
    }

    private static void testSpinnerPerformer(){
        SpinnerPerformer performer = new SpinnerPerformer();
        //System.out.println(performer.getSpinnerResult(100000, 10));
        SlotInfoRepository repository = new SlotInfoRepositoryImpl();
        System.out.println(repository.getAmountCoefficientUpperValue(10D));
    }

    private static void testPragmaticProfit() {
        SlotInfoPragmaticRepositoryImpl repository = new SlotInfoPragmaticRepositoryImpl();
        System.out.println(repository.countAll());
    }

    public static void connectToMySql(){
        SlotInfoPragmaticRepositoryImpl repository = new SlotInfoPragmaticRepositoryImpl();
        System.out.println(repository.findById(1));
        //System.out.println(repository.countAll());
        //System.out.println(repository.countAllInGame("vswayswerewolf"));
        /*for (String game: repository.allGames()){
            System.out.println(game + ": ");
            System.out.println(repository.countAllInGame(game));
        }*/
        Set<String> games = repository.allGames();
        for (String game : games){
            System.out.println(game);
        }
    }

    public static void downloadFromCSV(){
        try {
            CSVUploader.uploadSlotInfoToHSQLDB();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void testCoef(){
        SlotInfoPragmaticRepositoryImpl repository = new SlotInfoPragmaticRepositoryImpl();
        /*List<Double> allcoef = repository.getAllCoefInGame("vs4096bufking");
        Double bigSum = allcoef.stream().mapToDouble(Double::doubleValue).sum();
        double coefCount = allcoef.size();
        System.out.println("Avg coef:" + bigSum/coefCount);*/
        //System.out.println("Win: "+repository.countAllInGame("vs4096bufking"));

    }

    public static void testPerformance(){
        SlotInfoRepository repository = new SlotInfoRepositoryImpl();
        int size = repository.countAll();
        long start = System.currentTimeMillis();
        for (int i = 0; i< 10; i++){
            Random random = new Random();
            SlotInfoHome slotInfo = repository.findById(random.nextInt(size));
        }
        long finish = System.currentTimeMillis();
        System.out.println(finish-start);
    }

    public static void testHomePerformer(){
        SlotInput slotInput = new SlotInput();
        slotInput.setDeposit(200);
        slotInput.setBonus(100);
        slotInput.setWagger(20);
        slotInput.setBet(20);
        slotInput.setSim(100000);
        Performer performer = new Performer();
        System.out.println(performer.getProfit(slotInput));
    }

    public static void testCashbackPerformer(){
        SlotInfoRepository repository = new SlotInfoRepositoryImpl();
        List<Double> allCoef = repository.getAllCoef();
        allCoef.forEach(System.out::println);
        /*CashbackReport report = new CashbackReport();
        List<CashBack> cashBackList = report.getReport();
        for(CashBack cashBack : cashBackList){
            System.out.println(cashBack);
        }*/
    }

    public static void testHomeDB(){
        SlotInfoRepository repository = new SlotInfoRepositoryImpl();
        SlotInfoHome slotInfo = new SlotInfoHome();
        slotInfo.setIdSlotInfo(1L);
        slotInfo.setBid(1);
        slotInfo.setWin(1);
        slotInfo.setWinCoefficient(1);
        slotInfo.setIp("111");
        slotInfo.setBonus(1D);
        slotInfo.setCredit(111);
        slotInfo.setGame("game");
        List<SlotInfoHome> slotInfoList = new ArrayList<>();
        slotInfoList.add(slotInfo);
        repository.save(slotInfoList);
    }
}
