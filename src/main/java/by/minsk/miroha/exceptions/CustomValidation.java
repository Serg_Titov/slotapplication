package by.minsk.miroha.exceptions;

public class CustomValidation extends RuntimeException {

    public CustomValidation(String message) {
        super(message);
    }
}