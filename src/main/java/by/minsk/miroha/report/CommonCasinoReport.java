package by.minsk.miroha.report;

import by.minsk.miroha.entities.front.ProviderAmount;

import java.util.List;

public class CommonCasinoReport {

    private int totalSlots;

    private int providers;

    private long totalSpins;

    private List<ProviderAmount> topProviders;

    private List<String> newSlots;

    public int getTotalSlots() {
        return totalSlots;
    }

    public void setTotalSlots(int totalSlots) {
        this.totalSlots = totalSlots;
    }

    public int getProviders() {
        return providers;
    }

    public void setProviders(int providers) {
        this.providers = providers;
    }

    public long getTotalSpins() {
        return totalSpins;
    }

    public void setTotalSpins(long totalSpins) {
        this.totalSpins = totalSpins;
    }

    public List<ProviderAmount> getTopProviders() {
        return topProviders;
    }

    public void setTopProviders(List<ProviderAmount> topProviders) {
        this.topProviders = topProviders;
    }

    public List<String> getNewSlots() {
        return newSlots;
    }

    public void setNewSlots(List<String> newSlots) {
        this.newSlots = newSlots;
    }
}
