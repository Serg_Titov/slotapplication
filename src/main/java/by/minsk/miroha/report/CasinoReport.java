package by.minsk.miroha.report;

public class CasinoReport {

    private String provider;

    private String slot;

    private double rtp;

    private double volatility;

    private double x100;

    private double x1000;

    private double x5000;

    private double x10000;

    private double x20000;

    private double x50000;

    private int slotId;

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getSlot() {
        return slot;
    }

    public void setSlot(String slot) {
        this.slot = slot;
    }

    public double getRtp() {
        return rtp;
    }

    public void setRtp(double rtp) {
        this.rtp = rtp;
    }

    public double getVolatility() {
        return volatility;
    }

    public void setVolatility(double volatility) {
        this.volatility = volatility;
    }

    public double getX100() {
        return x100;
    }

    public void setX100(double x100) {
        this.x100 = x100;
    }

    public double getX1000() {
        return x1000;
    }

    public void setX1000(double x1000) {
        this.x1000 = x1000;
    }

    public double getX5000() {
        return x5000;
    }

    public void setX5000(double x5000) {
        this.x5000 = x5000;
    }

    public int getSlotId() {
        return slotId;
    }

    public void setSlotId(int slotId) {
        this.slotId = slotId;
    }

    public double getX10000() {
        return x10000;
    }

    public void setX10000(double x10000) {
        this.x10000 = x10000;
    }

    public double getX20000() {
        return x20000;
    }

    public void setX20000(double x20000) {
        this.x20000 = x20000;
    }

    public double getX50000() {
        return x50000;
    }

    public void setX50000(double x50000) {
        this.x50000 = x50000;
    }

    @Override
    public String toString() {
        return "CasinoReport{" +
                "provider='" + provider + '\'' +
                ", slot='" + slot + '\'' +
                ", rtp=" + rtp +
                ", volatility=" + volatility +
                ", x100=" + x100 +
                ", x1000=" + x1000 +
                ", x5000=" + x5000 +
                ", x10000=" + x10000 +
                ", x20000=" + x20000 +
                ", x50000=" + x50000 +
                ", slotId=" + slotId +
                '}';
    }
}
