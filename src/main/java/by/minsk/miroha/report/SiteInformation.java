package by.minsk.miroha.report;

import by.minsk.miroha.entities.front.JackpotPath;
import by.minsk.miroha.entities.report.CasinoSite;

import java.util.List;

public class SiteInformation {

    private List<CasinoSite> licenses;

    private List<JackpotPath> jackpotPaths;

    private List<Long> bonuses;

    public List<CasinoSite> getLicenses() {
        return licenses;
    }

    public void setLicenses(List<CasinoSite> licenses) {
        this.licenses = licenses;
    }

    public List<JackpotPath> getJackpotPaths() {
        return jackpotPaths;
    }

    public void setJackpotPaths(List<JackpotPath> jackpotPaths) {
        this.jackpotPaths = jackpotPaths;
    }

    public List<Long> getBonuses() {
        return bonuses;
    }

    public void setBonuses(List<Long> bonuses) {
        this.bonuses = bonuses;
    }
}
