package by.minsk.miroha.report.jackpot;

import by.minsk.miroha.entities.common.Site;
import by.minsk.miroha.entities.front.responses.GrownSiteResponse;

import java.util.List;

public class JackpotSiteReport {

    private List<MaxJackpotValue> currentJackpotList;

    private List<MaxJackpotValue> maxPaidOutJackpotList;

    private List<MaxJackpotValue> minPaidOutJackpotList;

    private String status;

    private List<LastPayedOutJackpot> lastPaidOutJackpot;

    private String slotName;

    private Site site;

    private List<GrownSiteResponse> grownValues;

    public List<MaxJackpotValue> getCurrentJackpotList() {
        return currentJackpotList;
    }

    public void setCurrentJackpotList(List<MaxJackpotValue> currentJackpotList) {
        this.currentJackpotList = currentJackpotList;
    }

    public List<MaxJackpotValue> getMaxPaidOutJackpotList() {
        return maxPaidOutJackpotList;
    }

    public void setMaxPaidOutJackpotList(List<MaxJackpotValue> maxPaidOutJackpotList) {
        this.maxPaidOutJackpotList = maxPaidOutJackpotList;
    }

    public List<MaxJackpotValue> getMinPaidOutJackpotList() {
        return minPaidOutJackpotList;
    }

    public void setMinPaidOutJackpotList(List<MaxJackpotValue> minPaidOutJackpotList) {
        this.minPaidOutJackpotList = minPaidOutJackpotList;
    }

    public List<LastPayedOutJackpot> getLastPaidOutJackpot() {
        return lastPaidOutJackpot;
    }

    public void setLastPaidOutJackpot(List<LastPayedOutJackpot> lastPaidOutJackpot) {
        this.lastPaidOutJackpot = lastPaidOutJackpot;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSlotName() {
        return slotName;
    }

    public void setSlotName(String slotName) {
        this.slotName = slotName;
    }

    public Site getSite() {
        return site;
    }

    public void setSite(Site site) {
        this.site = site;
    }

    public List<GrownSiteResponse> getGrownValues() {
        return grownValues;
    }

    public void setGrownValues(List<GrownSiteResponse> grownValues) {
        this.grownValues = grownValues;
    }
}
