package by.minsk.miroha.report.jackpot;

import java.sql.Timestamp;

public class JackpotGraphPoint {

    private String site;

    private Double jackpotValue;

    private Double lastPayedOut;

    private String date;

    private Timestamp timestamp;

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public Double getJackpotValue() {
        return jackpotValue;
    }

    public void setJackpotValue(Double jackpotValue) {
        this.jackpotValue = jackpotValue;
    }

    public Double getLastPayedOut() {
        return lastPayedOut;
    }

    public void setLastPayedOut(Double lastPayedOut) {
        this.lastPayedOut = lastPayedOut;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }
}
