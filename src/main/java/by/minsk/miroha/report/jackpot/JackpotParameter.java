package by.minsk.miroha.report.jackpot;

import by.minsk.miroha.entities.common.Site;

import java.sql.Timestamp;

public class JackpotParameter {

    private Site site;

    private String brand;

    private Timestamp date;

    private double valueFirst;

    private double valueSecond;

    private String slotName;

    private double rtp;

    private String status;

    public JackpotParameter() {
    }

    public Site getSite() {
        return site;
    }

    public void setSite(Site site) {
        this.site = site;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public double getValueFirst() {
        return valueFirst;
    }

    public void setValueFirst(double valueFirst) {
        this.valueFirst = valueFirst;
    }

    public double getValueSecond() {
        return valueSecond;
    }

    public void setValueSecond(double valueSecond) {
        this.valueSecond = valueSecond;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public double getRtp() {
        return rtp;
    }

    public void setRtp(double rtp) {
        this.rtp = rtp;
    }

    public String getSlotName() {
        return slotName;
    }

    public void setSlotName(String slotName) {
        this.slotName = slotName;
    }
}
