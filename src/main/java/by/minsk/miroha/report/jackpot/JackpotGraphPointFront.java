package by.minsk.miroha.report.jackpot;

public class JackpotGraphPointFront {

    private String jackpotValue;

    private String date;

    public String getJackpotValue() {
        return jackpotValue;
    }

    public void setJackpotValue(String jackpotValue) {
        this.jackpotValue = jackpotValue;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}
