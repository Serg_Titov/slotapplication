package by.minsk.miroha.report.jackpot;

import by.minsk.miroha.entities.front.report.PaidOutJackpotCounter;
import by.minsk.miroha.entities.front.responses.GrownJackpotResponse;

import java.util.List;
import java.util.Set;

public class JackpotsMainReport {

    private int uniqueJackpots;

    private List<PaidOutJackpotCounter> payedOutJackpotsCountList;

    private List<MaxJackpotValue> maxValueCurrentList;

    private List<LastPayedOutJackpot> lastPayedOutList;

    private List<MaxJackpotValue> maxValuePayedOutList;

    private List<JackpotParameterLicense> topJackpotsRTP;

    private int reach100Rtp;

    private List<GrownJackpotResponse> grownValues;

    private Set<String> licenses;

    private List<JackpotSite> inactiveJackpots;

    private String message;

    public int getUniqueJackpots() {
        return uniqueJackpots;
    }

    public void setUniqueJackpots(int uniqueJackpots) {
        this.uniqueJackpots = uniqueJackpots;
    }

    public List<PaidOutJackpotCounter> getPayedOutJackpotsCountList() {
        return payedOutJackpotsCountList;
    }

    public void setPayedOutJackpotsCountList(List<PaidOutJackpotCounter> payedOutJackpotsCountList) {
        this.payedOutJackpotsCountList = payedOutJackpotsCountList;
    }

    public List<MaxJackpotValue> getMaxValueCurrentList() {
        return maxValueCurrentList;
    }

    public void setMaxValueCurrentList(List<MaxJackpotValue> maxValueCurrentList) {
        this.maxValueCurrentList = maxValueCurrentList;
    }

    public List<LastPayedOutJackpot> getLastPayedOutList() {
        return lastPayedOutList;
    }

    public void setLastPayedOutList(List<LastPayedOutJackpot> lastPayedOutList) {
        this.lastPayedOutList = lastPayedOutList;
    }

    public List<MaxJackpotValue> getMaxValuePayedOutList() {
        return maxValuePayedOutList;
    }

    public void setMaxValuePayedOutList(List<MaxJackpotValue> maxValuePayedOutList) {
        this.maxValuePayedOutList = maxValuePayedOutList;
    }

    public List<JackpotParameterLicense> getTopJackpotsRTP() {
        return topJackpotsRTP;
    }

    public void setTopJackpotsRTP(List<JackpotParameterLicense> topJackpotsRTP) {
        this.topJackpotsRTP = topJackpotsRTP;
    }

    public int getReach100Rtp() {
        return reach100Rtp;
    }

    public void setReach100Rtp(int reach100Rtp) {
        this.reach100Rtp = reach100Rtp;
    }

    public List<GrownJackpotResponse> getGrownValues() {
        return grownValues;
    }

    public void setGrownValues(List<GrownJackpotResponse> grownValues) {
        this.grownValues = grownValues;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<JackpotSite> getInactiveJackpots() {
        return inactiveJackpots;
    }

    public void setInactiveJackpots(List<JackpotSite> inactiveJackpots) {
        this.inactiveJackpots = inactiveJackpots;
    }

    public Set<String> getLicenses() {
        return licenses;
    }

    public void setLicenses(Set<String> licenses) {
        this.licenses = licenses;
    }
}
