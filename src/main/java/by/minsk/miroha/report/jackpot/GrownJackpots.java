package by.minsk.miroha.report.jackpot;

import java.util.Comparator;

public class GrownJackpots {

    private String slotName;

    private String brand;

    private String site;

    private Double grownValue;

    private int id;

    public String getSlotName() {
        return slotName;
    }

    public void setSlotName(String slotName) {
        this.slotName = slotName;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public Double getGrownValue() {
        return grownValue;
    }

    public void setGrownValue(double grownValue) {
        this.grownValue = grownValue;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "GrownJackpots{" +
                "slotName='" + slotName + '\'' +
                ", brand='" + brand + '\'' +
                ", site='" + site + '\'' +
                ", grownValue=" + grownValue +
                ", id=" + id +
                '}';
    }

    public static final Comparator<GrownJackpots> COMPARE_BY_SITE = Comparator.comparing(GrownJackpots::getSite);

    public static final Comparator<GrownJackpots> COMPARE_BY_VALUE = (o1, o2) -> o2.getGrownValue().compareTo(o1.getGrownValue());
}
