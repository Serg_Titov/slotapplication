package by.minsk.miroha.report.jackpot;

import by.minsk.miroha.entities.common.Site;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class LastPayedOutJackpot implements Comparable<LastPayedOutJackpot>{

    private Site site;

    private String brand;

    private double value;

    private String date;

    private String slot;

    public Site getSite() {
        return site;
    }

    public void setSite(Site site) {
        this.site = site;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSlot() {
        return slot;
    }

    public void setSlot(String slot) {
        this.slot = slot;
    }

    @Override
    public int compareTo(LastPayedOutJackpot o) {
        try {
            if(o.getDate() == null){
                return -1;
            }
            if(date == null){
                return 1;
            }
            SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
            Date objectLastDate = formatter.parse(o.getDate());
            Date currentLastDate = formatter.parse(date);
            if(objectLastDate.before(currentLastDate)){
                return -1;
            } else {
                return 1;
            }
        } catch (ParseException ignored) {
            return 0;
        }
    }
}
