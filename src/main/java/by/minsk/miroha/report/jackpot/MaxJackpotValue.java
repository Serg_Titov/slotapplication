package by.minsk.miroha.report.jackpot;

public class MaxJackpotValue implements Comparable<MaxJackpotValue>{

    private double maxValue;

    private String site;

    private String brand;

    private String slotName;

    public MaxJackpotValue() {
    }

    public MaxJackpotValue(double maxValue, String site, String brand, String slotName) {
        this.maxValue = maxValue;
        this.site = site;
        this.brand = brand;
        this.slotName = slotName;
    }

    public double getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(double maxValue) {
        this.maxValue = maxValue;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getSlotName() {
        return slotName;
    }

    public void setSlotName(String slotName) {
        this.slotName = slotName;
    }

    @Override
    public int compareTo(MaxJackpotValue o) {
        if(o.getMaxValue() == maxValue){
            return 0;
        }
        return  o.getMaxValue() > maxValue ? 1 : -1;
    }

    @Override
    public String toString() {
        return "MaxJackpotValue{" +
                "maxValue=" + maxValue +
                ", site='" + site + '\'' +
                ", brand='" + brand + '\'' +
                '}';
    }
}
