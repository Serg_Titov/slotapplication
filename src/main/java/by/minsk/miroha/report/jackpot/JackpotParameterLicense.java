package by.minsk.miroha.report.jackpot;

import java.util.List;

public class JackpotParameterLicense extends JackpotParameter implements Comparable<JackpotParameterLicense>{

    private List<String> licenses;

    public JackpotParameterLicense() {
    }

    public JackpotParameterLicense(JackpotParameter jackpotParameter){
        this.setBrand(jackpotParameter.getBrand());
        this.setSite(jackpotParameter.getSite());
        this.setDate(jackpotParameter.getDate());
        this.setRtp(jackpotParameter.getRtp());
        this.setStatus(jackpotParameter.getStatus());
        this.setValueFirst(jackpotParameter.getValueFirst());
        this.setValueSecond(jackpotParameter.getValueSecond());
        this.setSlotName(jackpotParameter.getSlotName());
    }

    public List<String> getLicenses() {
        return licenses;
    }

    public void setLicenses(List<String> licenses) {
        this.licenses = licenses;
    }

    @Override
    public int compareTo(JackpotParameterLicense jackpotParameter) {
        if(this.getRtp() == jackpotParameter.getRtp()){
            return 0;
        }
        return this.getRtp() - jackpotParameter.getRtp() > 0 ? -1 : 1;
    }


}
