package by.minsk.miroha.report;

import by.minsk.miroha.entities.databases.SlotBriefInfo;
import by.minsk.miroha.entities.front.ProviderAmount;
import by.minsk.miroha.repositories.games.SlotBriefInfoRepository;
import by.minsk.miroha.repositories.games.impl.SlotBriefInfoRepositoryImpl;
import by.minsk.miroha.services.GamesService;
import by.minsk.miroha.services.impl.GamesServiceImpl;
import by.minsk.miroha.utils.CommonUtils;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CommonCasinoReportPerformer {

    private static final int TOP3 = 3;

    private final GamesService gamesService = new GamesServiceImpl();
    private final SlotBriefInfoRepository briefInfoRepository = new SlotBriefInfoRepositoryImpl();

    private Map<String, List<String>> allGamesWithProvider;
    private List<SlotBriefInfo> slotBriefInfoList;
    private int slotAmount;

    public CommonCasinoReport getReport() {
        List<String> providers = gamesService.getAllProviders();
        allGamesWithProvider = gamesService.getAllGames(providers);
        CommonCasinoReport report = new CommonCasinoReport();

        report.setProviders(providers.size());
        report.setTotalSlots(getTotalSlots());
        report.setTopProviders(getTop3BiggestProviders());
        report.setTotalSpins(getTotalSpinAmount(allGamesWithProvider));
        report.setNewSlots(getNewSlots(slotBriefInfoList));

        return report;
    }

    private int getTotalSlots() {
        int gamesCount = 0;
        for(String provider : allGamesWithProvider.keySet()){
            gamesCount = gamesCount + allGamesWithProvider.get(provider).size();
        }
        slotAmount = gamesCount;
        return gamesCount;
    }

    private List<ProviderAmount> getTop3BiggestProviders(){
        List<Integer> spinCounts = new ArrayList<>();
        for(String provider : allGamesWithProvider.keySet()){
            spinCounts.add(allGamesWithProvider.get(provider).size());
        }
        spinCounts.sort(Collections.reverseOrder());
        spinCounts = spinCounts.stream().limit(TOP3).collect(Collectors.toList());
        List<ProviderAmount> topProviders = new ArrayList<>();
        int providerCount = 0;
        for(Integer top : spinCounts) {
            for (String provider : allGamesWithProvider.keySet()) {
                if(allGamesWithProvider.get(provider).size() == top){
                    if(providerCount < TOP3) {
                        ProviderAmount providerAmount = new ProviderAmount(provider, top);
                        topProviders.add(providerAmount);
                        providerCount++;
                    }
                    if(providerCount == TOP3){
                        break;
                    }
                }
            }
            if (providerCount == TOP3){
                break;
            }
        }
        return topProviders;
    }

    private long getTotalSpinAmount(Map<String, List<String>> games){
        slotBriefInfoList = briefInfoRepository.getSlotBriefInfo();
        if (slotBriefInfoList.size() >= slotAmount){
            long totalSpinAmount = 0;
            for(SlotBriefInfo slotBriefInfo : slotBriefInfoList){
                totalSpinAmount = totalSpinAmount + slotBriefInfo.getSpinAmount();
            }
            return totalSpinAmount;
        } else {
            List<SlotBriefInfo> briefInfoToAdd = new ArrayList<>();
            for(String provider: games.keySet()){
                for(String slot: games.get(provider)){
                    boolean instanceFound = false;
                    for(SlotBriefInfo slotBriefInfo: slotBriefInfoList){
                        if(provider.equals(slotBriefInfo.getProvider()) && slot.equals(slotBriefInfo.getSlotName())){
                            instanceFound = true;
                            break;
                        }
                    }
                    if (!instanceFound){
                        briefInfoToAdd.add(createSlotBriefInfo(provider, slot));
                    }
                }
            }
            briefInfoRepository.save(briefInfoToAdd);
            return getTotalSpinAmount(games);
        }
    }

    private SlotBriefInfo createSlotBriefInfo(String provider, String slotName){
        SlotBriefInfo slotBriefInfo = new SlotBriefInfo();
        slotBriefInfo.setProvider(provider);
        slotBriefInfo.setSlotName(slotName);
        slotBriefInfo.setSpinAmount(gamesService.getSpinAmount(CommonUtils.getDatabaseName(provider, slotName)));
        slotBriefInfo.setCreationDate(gamesService.getCreationTableDate(provider, slotName));

        return slotBriefInfo;
    }

    private List<String> getNewSlots(List<SlotBriefInfo> briefInfoList){
        Collections.sort(briefInfoList);
        return briefInfoList.stream().map(brief -> brief.getProvider() + " " + brief.getSlotName()).limit(TOP3).collect(Collectors.toList());
    }

}