package by.minsk.miroha.report.entities;

import by.minsk.miroha.entities.report.BrandWithLicenses;

import java.util.List;

public class BrandsList {

    private List<String> licenses;

    private List<BrandWithLicenses> brands;

    public List<String> getLicenses() {
        return licenses;
    }

    public void setLicenses(List<String> licenses) {
        this.licenses = licenses;
    }

    public List<BrandWithLicenses> getBrands() {
        return brands;
    }

    public void setBrands(List<BrandWithLicenses> brands) {
        this.brands = brands;
    }

    @Override
    public String toString() {
        return "BrandsList{" +
                "licenses=" + licenses +
                ", brands=" + brands +
                '}';
    }
}
