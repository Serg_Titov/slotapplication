package by.minsk.miroha.report.entities;

import by.minsk.miroha.entities.report.SiteWithLicenses;

import java.util.List;

public class SiteList {

    private List<String> licenses;

    private List<SiteWithLicenses> sites;

    public List<String> getLicenses() {
        return licenses;
    }

    public void setLicenses(List<String> licenses) {
        this.licenses = licenses;
    }

    public List<SiteWithLicenses> getSites() {
        return sites;
    }

    public void setSites(List<SiteWithLicenses> sites) {
        this.sites = sites;
    }

    @Override
    public String toString() {
        return "SiteList{" +
                "licenses=" + licenses +
                ", sites=" + sites +
                '}';
    }
}
