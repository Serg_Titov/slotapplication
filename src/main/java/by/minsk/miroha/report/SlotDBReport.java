package by.minsk.miroha.report;

import by.minsk.miroha.entities.front.Message;
import by.minsk.miroha.entities.report.*;

import java.util.List;

public class SlotDBReport {

    private SlotInformation slotInformation;

    private CommonSlotInformation commonSlotInformation;

    private List<Multipliers> multipliers;

    private List<SlotPoint> slotPoints;

    private Message message;

    public SlotInformation getSlotInformation() {
        return slotInformation;
    }

    public void setSlotInformation(SlotInformation slotInformation) {
        this.slotInformation = slotInformation;
    }

    public CommonSlotInformation getCommonSlotInformation() {
        return commonSlotInformation;
    }

    public void setCommonSlotInformation(CommonSlotInformation commonSlotInformation) {
        this.commonSlotInformation = commonSlotInformation;
    }

    public List<SlotPoint> getSlotPoints() {
        return slotPoints;
    }

    public void setSlotPoints(List<SlotPoint> slotPoints) {
        this.slotPoints = slotPoints;
    }

    public List<Multipliers> getMultipliers() {
        return multipliers;
    }

    public void setMultipliers(List<Multipliers> multipliers) {
        this.multipliers = multipliers;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }
}
