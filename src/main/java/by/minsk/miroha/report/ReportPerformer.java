package by.minsk.miroha.report;

import java.util.ArrayList;
import java.util.List;

import by.minsk.miroha.entities.PairProfit;
import by.minsk.miroha.entities.SlotInput;
import by.minsk.miroha.entities.databases.SlotName;
import by.minsk.miroha.entities.enums.MultiplierDeposit;
import by.minsk.miroha.entities.enums.NonProfitSeriesValue;
import by.minsk.miroha.entities.enums.UserGrants;
import by.minsk.miroha.entities.report.*;
import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.perform.Performer;
import by.minsk.miroha.repositories.SlotNameRepository;
import by.minsk.miroha.repositories.impl.SlotNameRepositoryImpl;
import by.minsk.miroha.repositories.report.*;
import by.minsk.miroha.repositories.report.impl.*;
import by.minsk.miroha.services.SlotInputService;
import by.minsk.miroha.services.impl.SlotInputServiceImpl;


public class ReportPerformer {

    public static final String DB_NOT_UPLOADED = "База из файла не загружена!";

    private static final TopProfitsRepository topProfitsRepository = new TopProfitsRepositoryImpl();
    private static final NonProfitSeriesRepository nonProfitSeriesRepository = new NonProfitSeriesRepositoryImpl();
    private static final CommonSimInfoRepository commonSimInformationRepository = new CommonSimInfoRepositoryImpl();
    private static final SlotInputService slotInputService = new SlotInputServiceImpl();
    private static final DepositMultiplierRepository depositMultiplierRepository = new DepositMultiplierRepositoryImpl();
    private static final SlotNameRepository nameRepository = new SlotNameRepositoryImpl();

    public static final int TOP_PROFIT_AMOUNT = 5;

    private boolean toUpdate;

    private final SlotName slotName;

    public ReportPerformer() {
        slotName = nameRepository.getSlotName();
        if (slotName.getSlotName() == null){
            throw new CustomValidation(DB_NOT_UPLOADED);
        }

    }

    public ReportPerformer(String provider, String game){
        slotName = new SlotName(game, provider);
    }

    public SlotInputReport getReport(SlotInput slotInput, boolean external){
        SlotInput slotInputFromDB = slotInputService.findByAllFields(slotInput);
        if (!slotInput.isManual()){
            if(slotInputFromDB == null){
                toUpdate = false;
                return calculateResult(slotInput, external);
            }
            else {
                toUpdate = true;
                return calculateResult(slotInputFromDB, external);
            }
        }
        else {
            if (slotInputFromDB != null) {
                return getResultFromDatabase(slotInputFromDB);
            } else {
                return calculateResult(slotInput, external);
            }
        }
    }

    public SlotInputReport getResultFromDatabase(SlotInput slotInput){
        SlotInputReport report = new SlotInputReport();
        report.setTopProfits(topProfitsRepository.findBySlotInput(slotInput.getIdInput()));
        report.setSlotInformation(new SlotInformation(slotName.getSlotName(), slotName.getProvider()));
        report.setNonProfitSeries(nonProfitSeriesRepository.findBySlotInput(slotInput.getIdInput()));
        report.setDepositMultipliers(depositMultiplierRepository.findBySlotInput(slotInput.getIdInput()));
        report.setCommonSimInformation(commonSimInformationRepository.getObject(slotInput.getIdInput()));
        report.setSlotInput(slotInput);
        return report;
    }

    private SlotInputReport calculateResult(SlotInput slotInput, boolean external){
        Performer performer;
        SlotInputReport report = new SlotInputReport();
        if (external){
            performer = new Performer(slotName.getProvider(), slotName.getSlotName());
        } else {
            performer = new Performer();
        }
        SlotInput savedSlotInput;
        if (!toUpdate) {
            savedSlotInput = slotInputService.save(slotInput);
        } else {
            savedSlotInput = slotInput;
        }

        report.setSlotInformation(new SlotInformation(slotName.getSlotName(), slotName.getProvider()));
        report.setCommonSimInformation(calculateCommonSimInformation(savedSlotInput, performer, external));
        report.setDepositMultipliers(calculateDepositMultipliers(savedSlotInput, performer));
        report.setNonProfitSeries(calculateNonProfitSeries(savedSlotInput, performer));
        report.setTopProfits(calculateTopProfits(savedSlotInput, performer));
        report.setSlotInput(savedSlotInput);
        return report;
    }

    private CommonSimInformation calculateCommonSimInformation(SlotInput slotInput, Performer performer, boolean external){
        CommonSimInformation commonSimInformation = new CommonSimInformation();
        commonSimInformation.setProfit(performer.getProfit(slotInput, external));
        if (slotInput.getUserGrants() == UserGrants.CASINO){
            commonSimInformation.setCasinoEv(commonSimInformation.getProfit());
        } else {
            commonSimInformation.setEv(commonSimInformation.getProfit() - slotInput.getDeposit());
        }
        if(slotInput.getDeposit() != 0) {
            commonSimInformation.setRoi(commonSimInformation.getEv() / slotInput.getDeposit() * 100);
        }
        commonSimInformation.setBonusVolatility(performer.getBonusVolatility(slotInput.getDeposit()));
        commonSimInformation.setProfitProbability(performer.probabilityProfitMoreThanZero());
        commonSimInformation.setNonProfitProbability(performer.probabilityProfitZero());
        commonSimInformation.setLongestProfitSeries(performer.maxSequenceWithProfit().getSeries());
        commonSimInformation.setLongestProfitFrequency(performer.maxSequenceWithProfit().getFrequency());
        commonSimInformation.setLongestNonProfitSeries(performer.maxSequenceWithoutProfit().getSeries());
        commonSimInformation.setLongestNonProfitFrequency(performer.maxSequenceWithoutProfit().getFrequency());
        commonSimInformation.setAverageWin(performer.getAverageWin());
        commonSimInformation.setBetCounter(performer.getBetCount());
        commonSimInformation.setSlotInput(slotInput);

        if (!toUpdate){
            commonSimInformationRepository.save(commonSimInformation);
        }
        return commonSimInformation;
    }



    private List<NonProfitSeries> calculateNonProfitSeries(SlotInput slotInput, Performer performer){
        List<NonProfitSeries> nonProfitSeriesList = new ArrayList<>();
        for (NonProfitSeriesValue nonProfitSeries : NonProfitSeriesValue.values()){
            int series = performer.probabilityNullProfitAnyTimes(nonProfitSeries.getValue());
            int frequency = series == 0 ? 0 : slotInput.getSim() / series;
            NonProfitSeries savedNonProfitSeries = new NonProfitSeries(nonProfitSeries.getName(), frequency, slotInput);
            nonProfitSeriesList.add(savedNonProfitSeries);
            if (!toUpdate) {
                nonProfitSeriesRepository.save(savedNonProfitSeries);
            }
        }
        return nonProfitSeriesList;
    }

    private List<TopProfits> calculateTopProfits(SlotInput slotInput, Performer performer){
        List<TopProfits> topProfitsList = new ArrayList<>();
        for (PairProfit pairProfit : performer.getTop5Profits(TOP_PROFIT_AMOUNT)){
            TopProfits topProfits = new TopProfits(pairProfit.getName(), pairProfit.getProfit(),
                (slotInput.getSim() / pairProfit.getFrequency()), slotInput);
            topProfitsList.add(topProfits);
            if (!toUpdate) {
                topProfitsRepository.save(topProfits);
            }
        }
        return topProfitsList;
    }

    private List<DepositMultiplier> calculateDepositMultipliers(SlotInput slotInput, Performer performer){
        List<DepositMultiplier> depositMultiplierList = new ArrayList<>();
        for(MultiplierDeposit multiplier: MultiplierDeposit.values()){
            DepositMultiplier depositMultiplier = new DepositMultiplier();
            depositMultiplier.setDepositMultiplier(multiplier.getValue());
            depositMultiplier.setDepositMultiplierFrequency(performer.winMoreThanDepositFrequency(multiplier.getValue(), slotInput.getDeposit()));
            depositMultiplier.setSlotInput(slotInput);
            depositMultiplierList.add(depositMultiplier);
            if (!toUpdate) {
                depositMultiplierRepository.save(depositMultiplier);
            }
        }
        return depositMultiplierList;
    }

}
