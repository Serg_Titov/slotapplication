package by.minsk.miroha.report;

import by.minsk.miroha.entities.cashback.CashBack;
import by.minsk.miroha.entities.cashback.CashbackValue;
import by.minsk.miroha.entities.cashback.LossWinValues;
import by.minsk.miroha.entities.cashback.RangeValues;
import by.minsk.miroha.entities.databases.SlotName;
import by.minsk.miroha.perform.CashbackPerformer;
import by.minsk.miroha.perform.CashbackWriter;
import by.minsk.miroha.services.SlotInfoService;
import by.minsk.miroha.services.impl.SlotInfoExternalServiceImpl;
import by.minsk.miroha.services.impl.SlotInfoHomeServiceImpl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CashbackReport {

    public static final int FIRST_SIMULATION = 1000;
    public static final int SECOND_SIMULATION = 500000;
    public static final int THIRD_SIMULATION = 100000;
    public static final int LIMIT_LOSS_WIN = 1;

    private static final List<Double> FIRST_LOSS_WIN_VALUES = Arrays.asList(1D, 10D, 50D, 100D, 150D, 200D, 250D, 300D, 350D, 400D);

    private SlotName slotName;
    private final CashbackWriter cashbackWriter = new CashbackWriter();

    public CashbackReport() {
    }

    public CashbackReport(SlotName slotName) {
        this.slotName = slotName;
    }

    public List<CashBack> getReport(double cashbackValue, int simulations){
        CashbackPerformer performer;
        if (slotName == null) {
            performer = new CashbackPerformer();
        } else {
            performer = new CashbackPerformer(slotName.getProvider(), slotName.getSlotName());
        }
        List<CashBack> cashBackList = new ArrayList<>();
        int tableBorder = LIMIT_LOSS_WIN;
        for(double loss = 1; loss <= tableBorder; loss++){
            for(double win = 1; win <= tableBorder; win++){
                CashBack cashBack = new CashBack();
                cashBack.setMaxLoss(loss);
                cashBack.setTargetWin(win);
                cashBack.setSimulationAmount(simulations);
                cashBack.setCashbackValue(cashbackValue);
                cashBack.setResult(performer.getProfit(loss, win, simulations, cashbackValue));
                System.out.println(cashBack);
                cashBackList.add(cashBack);
            }
        }
        /*writeStartTable(cashBackList, cashbackValue, simulations);
        boolean maxFound = false;
        while (!maxFound || tableBorder > 200){
            CashBack maxCashback = cashBackList.stream().max(Comparator.comparing(CashBack::getResult)).orElseThrow();
            if(maxCashback.getMaxLoss() >= tableBorder - 5 || maxCashback.getTargetWin() >= tableBorder - 5){
                List<CashBack> newCashbackList = new ArrayList<>();
                int newTableBorder = tableBorder + 5;
                for(double loss =  1; loss <= newTableBorder; loss++){
                    for(double win = tableBorder + 1; win <= newTableBorder; win++){
                        CashBack cashBack = new CashBack();
                        cashBack.setMaxLoss(loss);
                        cashBack.setTargetWin(win);
                        cashBack.setSimulationAmount(simulations);
                        cashBack.setCashbackValue(cashbackValue);
                        cashBack.setResult(performer.getProfit(loss, win, simulations, cashbackValue));
                        System.out.println(cashBack);
                        newCashbackList.add(cashBack);
                    }
                }
                for(double loss =  tableBorder + 1; loss <= newTableBorder; loss++){
                    for(double win = 1; win <= newTableBorder; win++){
                        CashBack cashBack = new CashBack();
                        cashBack.setMaxLoss(loss);
                        cashBack.setTargetWin(win);
                        cashBack.setSimulationAmount(simulations);
                        cashBack.setCashbackValue(cashbackValue);
                        cashBack.setResult(performer.getProfit(loss, win, simulations, cashbackValue));
                        System.out.println(cashBack);
                        newCashbackList.add(cashBack);
                    }
                }
                cashBackList.addAll(newCashbackList);
                cashbackWriter.writeNewCashbackCells(newCashbackList, slotName, simulations, tableBorder);
                tableBorder = newTableBorder;
                System.out.println("NEW BORDER!!!!! "+ tableBorder);
            } else {
                maxFound = true;
            }
        }*/
        return cashBackList;
    }

    public List<CashBack> getReportOld(){
        /*CashbackPerformer performer;
        if (slotName == null) {
            performer = new CashbackPerformer();
        } else {
            performer = new CashbackPerformer(slotName.getProvider(), slotName.getSlotName());
        }
        List<CashBack> cashBackList = new ArrayList<>();
        List<LossWinValues> firstLossWinsValues = new ArrayList<>();
        for(Double lossValue : FIRST_LOSS_WIN_VALUES) {
            for (Double winValue : FIRST_LOSS_WIN_VALUES) {
                firstLossWinsValues.add(new LossWinValues(lossValue, winValue));
            }
        }
        for (CashbackValue cashbackValue: CashbackValue.values()) {
            List<CashBack> firstSimulation = performSimulation(performer, cashbackValue, firstLossWinsValues, FIRST_SIMULATION);
            CashBack firstCashBackMax = firstSimulation.stream().filter(cashBack -> cashBack.getCashbackValue() == cashbackValue.getValue())
                    .max(Comparator.comparing(CashBack::getResult)).orElseThrow();
            List<LossWinValues> secondLossWinValues = getSecondLossWinValues(firstCashBackMax, FIRST_LOSS_WIN_VALUES);
            List<CashBack> secondSimulation = performSimulation(performer, cashbackValue,
                    secondLossWinValues, SECOND_SIMULATION);
            CashBack secondCashbackMax = secondSimulation.stream().filter(cashBack -> cashBack.getCashbackValue() == cashbackValue.getValue())
                    .max(Comparator.comparing(CashBack::getResult)).orElseThrow();
            List<LossWinValues> lastLossWinsValues = getLastLossWinsValues(secondCashbackMax);
            List<CashBack> lastSimulation = performSimulation(performer, cashbackValue, lastLossWinsValues, THIRD_SIMULATION);
            List<CashBack> allCashback = unionAllCashback(secondSimulation, lastSimulation);
            unionAllCashback(firstSimulation, allCashback);
            cashBackList.addAll(allCashback);
        }
        return cashBackList;*/
        return new ArrayList<>();
    }

    private List<LossWinValues> getSecondLossWinValues(CashBack cashBack, List<Double> lossWinRange){
        RangeValues lossValues = new RangeValues();
        RangeValues winValues = new RangeValues();
        for (Double lossWinValue: lossWinRange){
            if (lossWinValue.equals(cashBack.getMaxLoss())){
                if(lossWinRange.indexOf(lossWinValue)==0){
                    lossValues.setMin(lossWinValue);
                    lossValues.setMax(lossWinRange.get(1));
                } else if(lossWinRange.indexOf(lossWinValue)==(lossWinRange.size()-1)){
                    lossValues.setMax(lossWinValue);
                    lossValues.setMin(lossWinRange.get(lossWinRange.size()-2));
                } else {
                    lossValues.setMin(lossWinRange.get(lossWinRange.indexOf(lossWinValue)-1));
                    lossValues.setMax(lossWinRange.get(lossWinRange.indexOf(lossWinValue)+1));
                }
            }
            if (lossWinValue.equals(cashBack.getTargetWin())){
                if(lossWinRange.indexOf(lossWinValue)==0){
                    winValues.setMin(lossWinValue);
                    winValues.setMax(lossWinRange.get(1));
                } else if(lossWinRange.indexOf(lossWinValue)==(lossWinRange.size()-1)){
                    winValues.setMax(lossWinValue);
                    winValues.setMin(lossWinRange.get(lossWinRange.size()-2));
                } else {
                    winValues.setMin(lossWinRange.get(lossWinRange.indexOf(lossWinValue)-1));
                    winValues.setMax(lossWinRange.get(lossWinRange.indexOf(lossWinValue)+1));
                }
            }
        }
        List<Double> losses = getRange(lossValues);
        List<Double> wins = getRange(winValues);
        List<LossWinValues> lossWinValues = new ArrayList<>();
        for (Double lossValue: losses){
            for (Double winValue: wins){
                lossWinValues.add(new LossWinValues(lossValue, winValue));
            }
        }
        return lossWinValues;
    }

    private List<LossWinValues> getLastLossWinsValues(CashBack cashBack){
        double startLoss = cashBack.getMaxLoss() < 5 ? 1D : cashBack.getMaxLoss();
        double startWin = cashBack.getTargetWin() <5 ? 1D : cashBack.getTargetWin();
        List<LossWinValues> lossWinValues = new ArrayList<>();
        for (double losses = startLoss; losses < startLoss+11; losses++){
            for (double wins = startWin; wins < startWin+11; wins++){
                lossWinValues.add(new LossWinValues(losses, wins));
            }
        }
        return lossWinValues;
    }

    private List<CashBack> performSimulation(CashbackPerformer performer, CashbackValue cashbackValue,
                                            List<LossWinValues> lossWinValues, int simulationsAmount){
        List<CashBack> cashBacks = new ArrayList<>();
        for (LossWinValues values : lossWinValues){
            CashBack cashBack = new CashBack();
            cashBack.setMaxLoss(values.getLoss());
            cashBack.setTargetWin(values.getWin());
            cashBack.setSimulationAmount(simulationsAmount);
            cashBack.setCashbackValue(cashbackValue.getValue());
            cashBack.setResult(performer.getProfit(values.getLoss(), values.getWin(),
                    simulationsAmount, cashbackValue.getValue()));
            cashBacks.add(cashBack);
        }
        return cashBacks;
    }

    private List<Double> getRange(RangeValues values) {
        List<Double> rangeValues = new ArrayList<>();
        if (values.getMin() == 1D && values.getMax() == 10D){
            for (double d = values.getMin(); d<= values.getMax(); d++){
                rangeValues.add(d);
            }
        }
        if (values.getMin() == 1D && values.getMax() == 50D){
            rangeValues.add(values.getMin());
            for (double d = 5D; d<= values.getMax(); d=d+5){
                rangeValues.add(d);
            }
        }
        if (values.getMin() == 350D){
            for (double d = values.getMin(); d<= values.getMax(); d=d+5){
                rangeValues.add(d);
            }
        }
        if (values.getMin() == 10D || values.getMin() == 50D || values.getMin() == 100D ||
                values.getMin() == 150 || values.getMin() == 200 || values.getMin() == 250 ||
                values.getMin() == 300){
            for (double d = values.getMin(); d<= values.getMax(); d=d+10){
                rangeValues.add(d);
            }
        }
        return rangeValues;
    }

    private List<CashBack> unionAllCashback(List<CashBack> addList, List<CashBack> baseList){
        int hasSameCashback;
        for (CashBack cashBackAdd : addList){
            hasSameCashback = 0;
            for (CashBack baseCashback: baseList){
                if (cashBackAdd.getMaxLoss().equals(baseCashback.getMaxLoss()) &&
                        cashBackAdd.getTargetWin().equals(baseCashback.getTargetWin()) &&
                        cashBackAdd.getCashbackValue() == baseCashback.getCashbackValue()) {
                    hasSameCashback = 1;
                    break;
                }
            }
            if (hasSameCashback == 0){
                baseList.add(cashBackAdd);
            }
        }
        return baseList;
    }

    private void writeStartTable(List<CashBack> cashBackList, double cashbackValue, int simulations){
        SlotInfoService slotInfoService;
        SlotName writtenSlot;
        if(slotName != null) {
            slotInfoService = new SlotInfoExternalServiceImpl(slotName.getProvider(), slotName.getSlotName());
            writtenSlot = slotName;
        } else {
            slotInfoService = new SlotInfoHomeServiceImpl();
            writtenSlot = new SlotName("Local", "Local");
        }
        double bid = slotInfoService.getBid();

        cashbackWriter.writeCashbackTable(cashBackList, cashbackValue, bid, simulations, writtenSlot);
    }

}
