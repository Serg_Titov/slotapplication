package by.minsk.miroha.report;

import java.util.List;

import by.minsk.miroha.entities.SlotInput;
import by.minsk.miroha.entities.report.*;

public class SlotInputReport {

    private List<TopProfits> topProfits;

    private SlotInformation slotInformation;

    private List<NonProfitSeries> nonProfitSeries;

    private List<DepositMultiplier> depositMultipliers;

    private CommonSimInformation commonSimInformation;

    private SlotInput slotInput;

    private String error;

    private long fullTime;

    public SlotInputReport() {
    }

    public List<TopProfits> getTopProfits() {
        return topProfits;
    }

    public void setTopProfits(List<TopProfits> topProfits) {
        this.topProfits = topProfits;
    }

    public SlotInformation getSlotInformation() {
        return slotInformation;
    }

    public void setSlotInformation(SlotInformation slotInformation) {
        this.slotInformation = slotInformation;
    }

    public List<NonProfitSeries> getNonProfitSeries() {
        return nonProfitSeries;
    }

    public void setNonProfitSeries(List<NonProfitSeries> nonProfitSeries) {
        this.nonProfitSeries = nonProfitSeries;
    }

    public CommonSimInformation getCommonSimInformation() {
        return commonSimInformation;
    }

    public void setCommonSimInformation(CommonSimInformation commonSimInformation) {
        this.commonSimInformation = commonSimInformation;
    }

    public SlotInput getSlotInput() {
        return slotInput;
    }

    public void setSlotInput(SlotInput slotInput) {
        this.slotInput = slotInput;
    }

    public List<DepositMultiplier> getDepositMultipliers() {
        return depositMultipliers;
    }

    public void setDepositMultipliers(List<DepositMultiplier> depositMultipliers) {
        this.depositMultipliers = depositMultipliers;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public long getFullTime() {
        return fullTime;
    }

    public void setFullTime(long fullTime) {
        this.fullTime = fullTime;
    }

}
