package by.minsk.miroha.utils;

import by.minsk.miroha.report.jackpot.JackpotParameterLicense;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class LicenseUtils {

    public static Set<String> getLicensesSet(List<JackpotParameterLicense> jackpotParameters){
        Set<String> licenses = new HashSet<>();
        for (JackpotParameterLicense jackpotParameter : jackpotParameters){
            if(jackpotParameter.getLicenses() != null) {
                licenses.addAll(jackpotParameter.getLicenses());
            }
        }
        return licenses;
    }
}
