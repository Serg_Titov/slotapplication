package by.minsk.miroha.utils;

import by.minsk.miroha.entities.SlotInput;
import by.minsk.miroha.entities.enums.UserGrants;
import by.minsk.miroha.entities.enums.WaggerType;
import by.minsk.miroha.entities.front.Menu;
import by.minsk.miroha.entities.front.MenuItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Creator {

    private static final String FILE_MENU = "Рассчитать результат из файла";
    private static final String DATABASE_MENU = "Рассчитать результат из базы";
    private static final String UPLOAD_FILE = "Загрузить свой слот";
    private static final String CALCULATE_SLOT = "Рассчитать слот";
    private static final String CALCULATE_BONUS = "Рассчитать бонус";
    private static final String PROBABILITY_HAS_PROFIT = "Вероятность сделать N спинов и остаться в плюсе";
    private static final String CALCULATE_CASHBACK = "Рассчитать кэшбэк";
    private static final String MULTI_CALCULATE = "Массовый расчет бонусов";
    private static final String CALCULATE_ALL_PROVIDERS_SLOTS = "Рассчитать все слоты провайдера";
    private static final String CALCULATE_ALL_SLOTS = "Рассчитать все слоты в базе";


    public static SlotInput createFirstStandardSlotInput(String game){
        SlotInput slotInput = new SlotInput();
        slotInput.setDeposit(100);
        slotInput.setBonus(100);
        slotInput.setWithoutBonus(false);
        slotInput.setTaxes(0);
        slotInput.setBet(5);
        slotInput.setWagger(40);
        slotInput.setWaggerType(WaggerType.B);
        slotInput.setSim(1000000);
        slotInput.setGame(game);
        slotInput.setManual(false);
        slotInput.setUserGrants(UserGrants.PRO);
        slotInput.setPlatformFee(0);
        slotInput.setProviderFee(0);
        slotInput.setPaymentFee(0);
        slotInput.setBonusParts(0);
        return slotInput;
    }

    public static SlotInput createSecondStandardSlotInput(String game){
        SlotInput slotInput = new SlotInput();
        slotInput.setDeposit(100);
        slotInput.setBonus(100);
        slotInput.setWithoutBonus(false);
        slotInput.setTaxes(0);
        slotInput.setBet(5);
        slotInput.setWagger(30);
        slotInput.setWaggerType(WaggerType.DB);
        slotInput.setSim(1000000);
        slotInput.setGame(game);
        slotInput.setManual(false);
        slotInput.setUserGrants(UserGrants.PRO);
        slotInput.setPlatformFee(0);
        slotInput.setProviderFee(0);
        slotInput.setPaymentFee(0);
        slotInput.setBonusParts(0);
        return slotInput;
    }

    public static Menu createMenu(UserGrants grants){
        Menu menu = null;
        switch (grants){
            case PRO :
                menu = getProMenu();
                break;
            case DEMO :
                menu = getDemoMenu();
                break;
            case LITE :
                menu = getLiteMenu();
                break;
            case ADMIN :
                menu = getAdminMenu();
                break;
            case CASINO:
                menu = getCasinoMenu();
                break;
        }
        return menu;
    }

    private static Menu getDemoMenu(){
        MenuItem firstItem = new MenuItem("/SlotApplication/report?version=demo", CALCULATE_BONUS);
        List<MenuItem> menuItems = new ArrayList<>();
        menuItems.add(firstItem);
        Map<String, List<MenuItem>> menuMap = new HashMap<>();
        menuMap.put(DATABASE_MENU, menuItems);
        Menu menu = new Menu();
        menu.setVersion(UserGrants.DEMO.getGrants());
        menu.setFirstLevel(menuMap);
        return menu;
    }

    private static Menu getLiteMenu(){
        MenuItem firstItem = new MenuItem("/SlotApplication/report?version=lite", CALCULATE_BONUS);
        MenuItem secondItem = new MenuItem("/SlotApplication/spinner-db", PROBABILITY_HAS_PROFIT);
        List<MenuItem> menuItems = new ArrayList<>();
        menuItems.add(firstItem);
        menuItems.add(secondItem);
        Map<String, List<MenuItem>> menuMap = new HashMap<>();
        menuMap.put(DATABASE_MENU, menuItems);
        Menu menu = new Menu();
        menu.setVersion(UserGrants.LITE.getGrants());
        menu.setFirstLevel(menuMap);
        return menu;
    }

    private static Menu getProMenu(){
        MenuItem firstItem = new MenuItem("/SlotApplication/upload", UPLOAD_FILE);
        MenuItem secondItem = new MenuItem("/SlotApplication/slot-info?version=pro", CALCULATE_BONUS);
        MenuItem thirdItem = new MenuItem("/SlotApplication/cashback", CALCULATE_CASHBACK);
        MenuItem fourthItem = new MenuItem("/SlotApplication/spinner-home", PROBABILITY_HAS_PROFIT);
        List<MenuItem> firstMenuItems = new ArrayList<>();
        firstMenuItems.add(firstItem);
        firstMenuItems.add(secondItem);
        firstMenuItems.add(thirdItem);
        firstMenuItems.add(fourthItem);

        MenuItem fifthItem = new MenuItem("/SlotApplication/report?version=pro", CALCULATE_BONUS);
        MenuItem sixthItem = new MenuItem("/SlotApplication/spinner-db", PROBABILITY_HAS_PROFIT);
        MenuItem seventhItem = new MenuItem("/SlotApplication/db-cashback", CALCULATE_CASHBACK);
        List<MenuItem> secondMenuItems = new ArrayList<>();
        secondMenuItems.add(fifthItem);
        secondMenuItems.add(sixthItem);
        secondMenuItems.add(seventhItem);

        Map<String, List<MenuItem>> menuMap = new HashMap<>();
        menuMap.put(DATABASE_MENU, secondMenuItems);
        menuMap.put(FILE_MENU, firstMenuItems);
        Menu menu = new Menu();
        menu.setVersion(UserGrants.PRO.getGrants());
        menu.setFirstLevel(menuMap);
        return menu;
    }

    private static Menu getAdminMenu(){
        MenuItem firstItem = new MenuItem("/SlotApplication/upload", UPLOAD_FILE);
        MenuItem secondItem = new MenuItem("/SlotApplication/calc-home-db", CALCULATE_SLOT);
        MenuItem thirdItem = new MenuItem("/SlotApplication/slot-info?version=admin", CALCULATE_BONUS);
        MenuItem fourthItem = new MenuItem("/SlotApplication/cashback", CALCULATE_CASHBACK);
        MenuItem fifthItem = new MenuItem("/SlotApplication/spinner-home", PROBABILITY_HAS_PROFIT);
        List<MenuItem> firstMenuItems = new ArrayList<>();
        firstMenuItems.add(firstItem);
        firstMenuItems.add(secondItem);
        firstMenuItems.add(thirdItem);
        firstMenuItems.add(fourthItem);
        firstMenuItems.add(fifthItem);

        MenuItem sixthItem = new MenuItem("/SlotApplication/calc-db", CALCULATE_SLOT);
        MenuItem seventhItem = new MenuItem("/SlotApplication/report?version=admin", CALCULATE_BONUS);
        MenuItem eighthItem = new MenuItem("/SlotApplication/calc-provider", CALCULATE_ALL_PROVIDERS_SLOTS);
        MenuItem ninthItem = new MenuItem("/SlotApplication/calc-all-slots", CALCULATE_ALL_SLOTS);
        MenuItem tenthItem = new MenuItem("/SlotApplication/spinner-db", PROBABILITY_HAS_PROFIT);
        MenuItem eleventhItem = new MenuItem("/SlotApplication/db-cashback", CALCULATE_CASHBACK);
        MenuItem twelfthItem = new MenuItem("/SlotApplication/multi-report", MULTI_CALCULATE);
        List<MenuItem> secondMenuItems = new ArrayList<>();
        secondMenuItems.add(sixthItem);
        secondMenuItems.add(seventhItem);
        secondMenuItems.add(eighthItem);
        secondMenuItems.add(ninthItem);
        secondMenuItems.add(tenthItem);
        secondMenuItems.add(eleventhItem);
        secondMenuItems.add(twelfthItem);

        Map<String, List<MenuItem>> menuMap = new HashMap<>();
        menuMap.put(FILE_MENU, firstMenuItems);
        menuMap.put(DATABASE_MENU, secondMenuItems);
        Menu menu = new Menu();
        menu.setVersion(UserGrants.ADMIN.getGrants());
        menu.setFirstLevel(menuMap);
        return menu;
    }

    private static Menu getCasinoMenu(){
        MenuItem firstItem = new MenuItem("/SlotApplication/slot-info?version=casino", CALCULATE_BONUS);
        List<MenuItem> firstMenuItems = new ArrayList<>();
        firstMenuItems.add(firstItem);

        MenuItem secondItem = new MenuItem("/SlotApplication/report?version=casino", CALCULATE_BONUS);
        List<MenuItem> secondMenuItems = new ArrayList<>();
        secondMenuItems.add(secondItem);

        Map<String, List<MenuItem>> menuMap = new HashMap<>();
        menuMap.put(FILE_MENU, firstMenuItems);
        menuMap.put(DATABASE_MENU, secondMenuItems);

        Menu menu = new Menu();
        menu.setVersion(UserGrants.CASINO.getGrants());
        menu.setFirstLevel(menuMap);
        return menu;
    }
}
