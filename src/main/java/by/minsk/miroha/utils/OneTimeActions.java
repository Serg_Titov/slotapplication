package by.minsk.miroha.utils;

import by.minsk.miroha.entities.SlotInput;
import by.minsk.miroha.entities.databases.SlotName;
import by.minsk.miroha.entities.enums.UserGrants;
import by.minsk.miroha.entities.enums.WaggerType;
import by.minsk.miroha.entities.report.CommonSlotInformation;
import by.minsk.miroha.entities.report.SlotInformation;
import by.minsk.miroha.perform.Performer;
import by.minsk.miroha.repositories.report.CommonSlotInfoRepository;
import by.minsk.miroha.services.SlotInfoService;
import by.minsk.miroha.services.impl.SlotInfoExternalServiceImpl;
import by.minsk.miroha.services.report.SlotInformationService;
import org.springframework.context.annotation.Configuration;
//import org.springframework.scheduling.annotation.Scheduled;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Configuration
public class OneTimeActions {

    private final SlotInformationService slotService;

    private final CommonSlotInfoRepository commonSlotInfoRepository;

    public OneTimeActions(SlotInformationService slotService, CommonSlotInfoRepository commonSlotInfoRepository) {
        this.slotService = slotService;
        this.commonSlotInfoRepository = commonSlotInfoRepository;
    }

    //@Scheduled(fixedDelayString = "P7D", initialDelay = 1000)
    public void correctBonusAverageWin(){
        List<SlotInformation> slotInformationList = slotService.getAllSlots();
        for(SlotInformation slot : slotInformationList){
            SlotInfoService slotInfoService = new SlotInfoExternalServiceImpl(slot.getProvider().getRawProvider(), slot.getSlot().getRawSlot());
            CommonSlotInformation commonSlotInformation = commonSlotInfoRepository.getObject(slot.getIdInfo());
            double averageWin = slotInfoService.getAverageBonusWin();
            if(averageWin != 0) {
                commonSlotInformation.setAverageBonusWin(averageWin);
                commonSlotInfoRepository.update(commonSlotInformation);
            }
        }
    }

    public static void distributionGraph(){
        double deposit = 100;
        SlotInput slotInput = new SlotInput();
        slotInput.setGame("the_dog_house pragmatic");
        slotInput.setDeposit(deposit);
        slotInput.setBonus(100);
        slotInput.setPaymentFee(0);
        slotInput.setProviderFee(0);
        slotInput.setPlatformFee(0);
        slotInput.setAdminMode(false);
        slotInput.setUserGrants(UserGrants.ADMIN);
        slotInput.setSpinLimit(0);
        slotInput.setBet(10);
        slotInput.setWagger(40);
        slotInput.setWaggerType(WaggerType.B);
        slotInput.setWithoutBonus(false);
        slotInput.setTaxes(0);
        slotInput.setProfitLimit(0);
        slotInput.setBonusParts(0);
        slotInput.setSim(1000000);
        slotInput.setManual(true);
        SlotName slotName = new SlotName("the_dog_house", "pragmatic");
        Performer performer = new Performer(slotName.getProvider(), slotName.getSlotName());
        double profit = performer.getProfit(slotInput, true);
        List<Double> notNullResults = performer.getNotNullResults();
        double minWin = notNullResults.stream().min(Double::compareTo).orElseThrow();
        double maxWin = notNullResults.stream().max(Double::compareTo).orElseThrow();
        int parts = 500;
        int resultsCount = notNullResults.size();
        Map<Double, Integer> buckets = new HashMap<>();
        double step = (maxWin - minWin)/parts;
        double middleStep = step / 2;
        double firstPoint = minWin + middleStep;
        for (double i = 0; i < parts; i++){
            buckets.put(i, 0);
        }
        for(Double result : notNullResults){
            for(double i = 0; i < parts; i++){
                if(result >= minWin + i*step && result <= minWin + (i+1)*step){
                    buckets.put(i, buckets.get(i) + 1);
                    break;
                }
            }
        }
        /*for(Double result : notNullResults){
            if(!buckets.containsKey(result)){
                buckets.put(result, 1);
            } else {
                buckets.put(result, buckets.get(result) + 1);
            }
        }*/
        Map<Long, Double> resultBuckets = new HashMap<>();
        for(double i = 0; i < parts; i++){
            Long point = Math.round(firstPoint + i * step);
            resultBuckets.put(point, (double) buckets.get(i)/resultsCount);
        }
        System.out.println(notNullResults.size());
        System.out.println("Min - " + minWin);
        System.out.println("Max - " + maxWin);
        for (Long point : resultBuckets.keySet()){
            System.out.println(point + ", " + resultBuckets.get(point));
        }
        /*try(FileWriter writer = new FileWriter("notes3.txt", false))
        {

            for(Double point : buckets.keySet()){
                writer.write(point + ", " + buckets.get(point));
                writer.append('\n');
            }
            writer.flush();
        } catch(IOException ex){

            System.out.println(ex.getMessage());
        }*/
    }

    public static void main(String[] args) {
        distributionGraph();
    }
}
