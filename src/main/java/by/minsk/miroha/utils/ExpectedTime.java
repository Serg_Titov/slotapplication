package by.minsk.miroha.utils;

import by.minsk.miroha.entities.SlotInput;

public class ExpectedTime {

    public static final int SIM_LOW_BORDER = 70000;
    public static final int SIM_LOW_BORDER_VALUE = -1;
    public static final int SIM_HIGH_BORDER = 130000;
    public static final int SIM_HIGH_BORDER_VALUE = 1;
    public static final int SIM_COMMON_VALUE = 0;

    public static final int WAGGER_LOW_BORDER = 25;
    public static final int WAGGER_LOW_BORDER_VALUE = -1;
    public static final int WAGGER_HIGH_BORDER = 70;
    public static final int WAGGER_HIGH_BORDER_VALUE = 2;
    public static final int WAGGER_COMMON_VALUE = 1;

    public static final double BET_LOW_BORDER = 0.02;
    public static final double BET_LOW_BORDER_VALUE = -1;
    public static final double BET_HIGH_BORDER = 0.03;
    public static final double BET_HIGH_BORDER_VALUE = 1;
    public static final double BET_COMMON_VALUE = 0;

    public static final double START_EXPECTED_TIME = 5.5;
    public static final double UNLOADER_TIME = 5;

    public static double getExpectedTime(SlotInput slotInput){
        slotInput.setBalance();
        double maxSim = slotInput.getBalance()/slotInput.getBet()*slotInput.getWagger()*slotInput.getSim();
        double simCorrectionFactor = getSimulationCorrectionFactor(slotInput.getSim());
        double waggerCorrectionFactor = getWaggerCorrectionFactor(slotInput.getWagger());
        double betCorrectionFactor = getBetCorrectionFactor(slotInput.getBet(), slotInput.getBalance());
        double expectedSimulationsPerSecond = (START_EXPECTED_TIME + simCorrectionFactor + waggerCorrectionFactor
                + betCorrectionFactor)*1000000;
        return maxSim/expectedSimulationsPerSecond + UNLOADER_TIME;
    }

    private static double getSimulationCorrectionFactor(int simulations){
        if(simulations < SIM_LOW_BORDER){
            return SIM_LOW_BORDER_VALUE;
        } else if(simulations > SIM_HIGH_BORDER) {
            return SIM_HIGH_BORDER_VALUE;
        } else {
            return SIM_COMMON_VALUE;
        }
    }

    private static double getWaggerCorrectionFactor(int wagger){
        if(wagger <= WAGGER_LOW_BORDER){
            return WAGGER_LOW_BORDER_VALUE;
        } else if(wagger > WAGGER_HIGH_BORDER) {
            return WAGGER_HIGH_BORDER_VALUE;
        } else {
            return WAGGER_COMMON_VALUE;
        }
    }

    private static double getBetCorrectionFactor(double bet, double balance){
        if(balance/bet <= BET_LOW_BORDER){
            return BET_LOW_BORDER_VALUE;
        } else if(balance/bet > BET_HIGH_BORDER) {
            return BET_HIGH_BORDER_VALUE;
        } else {
            return BET_COMMON_VALUE;
        }
    }
}
