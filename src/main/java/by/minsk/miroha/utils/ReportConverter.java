package by.minsk.miroha.utils;

import by.minsk.miroha.entities.front.report.CommonSLotInformationFront;
import by.minsk.miroha.entities.front.report.SlotPointFront;
import by.minsk.miroha.entities.front.report.SlotReportFront;
import by.minsk.miroha.entities.report.CommonSlotInformation;
import by.minsk.miroha.entities.report.SlotPoint;
import by.minsk.miroha.report.SlotDBReport;

import java.util.ArrayList;
import java.util.List;

public class ReportConverter {

    public static SlotPointFront convertSlotPoint(SlotPoint slotPoint){
        SlotPointFront pointFront = new SlotPointFront();
        pointFront.setIdSlotPoint(slotPoint.getIdSlotPoint());
        pointFront.setProvider(slotPoint.getProvider());
        pointFront.setGame(slotPoint.getGame());
        pointFront.setSpinLimit(slotPoint.getSpinLimit());
        pointFront.setPointResult(String.format("%.2f", slotPoint.getPointResult()));
        return pointFront;
    }

    public static CommonSLotInformationFront convertCommonSlotInformation(CommonSlotInformation commonSlotInformation){
        CommonSLotInformationFront informationFront = new CommonSLotInformationFront();
        informationFront.setRtp(String.format("%.2f", commonSlotInformation.getRtp()*100));
        informationFront.setVolatility(String.format("%.0f", commonSlotInformation.getVolatility()));
        informationFront.setIdCommonInfo(commonSlotInformation.getIdCommonInfo());
        informationFront.setBonusFrequency(String.format("%.0f", commonSlotInformation.getBonusFrequency()));
        informationFront.setGameId(commonSlotInformation.getGameId());
        informationFront.setAverageBonusWin(commonSlotInformation.getAverageBonusWin());
        informationFront.setMaxWin(commonSlotInformation.getMaxWin());
        informationFront.setSpinAmount(commonSlotInformation.getSpinAmount());
        informationFront.setBonusWinsRate(String.format("%.1f",commonSlotInformation.getBonusWinsRate()));
        informationFront.setWinsWithoutBonusRate(String.format("%.1f",commonSlotInformation.getWinsWithoutBonusRate()));
        return informationFront;
    }

    public static SlotReportFront convertSlotReport(SlotDBReport report){
        SlotReportFront reportFront = new SlotReportFront();
        reportFront.setMessage(report.getMessage());
        reportFront.setSlotInformation(report.getSlotInformation());
        reportFront.setMultipliers(report.getMultipliers());
        reportFront.setCommonSlotInformation(convertCommonSlotInformation(report.getCommonSlotInformation()));

        List<SlotPointFront> pointFronts = new ArrayList<>();
        for(SlotPoint point : report.getSlotPoints()){
            pointFronts.add(ReportConverter.convertSlotPoint(point));
        }
        reportFront.setSlotPoints(pointFronts);
        return reportFront;
    }
}
