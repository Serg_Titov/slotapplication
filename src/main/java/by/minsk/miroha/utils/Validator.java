package by.minsk.miroha.utils;


import by.minsk.miroha.entities.SlotInput;
import by.minsk.miroha.entities.enums.UserGrants;
import by.minsk.miroha.entities.enums.WaggerType;
import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.services.UsersBonusAuditService;
import by.minsk.miroha.services.impl.UsersBonusAuditServiceImpl;

import java.util.regex.Pattern;

import static by.minsk.miroha.controllers.spring.file.SlotInputController.NOT_NULL_SIM;
import static by.minsk.miroha.controllers.spring.file.SlotInputController.WAGGER_BONUS;
import static by.minsk.miroha.utils.DatabaseUtils.ZERO;

public class Validator {

    private static final String NOT_INPUT_PARAMETER = "Не введен параметр: ";
    private static final String NOT_EMPTY_PARAMETER = "Параметр %s не может быть пустым";
    private static final String INCORRECT_PARAMETER = "Не корректно введен параметр: ";
    private static final String CANT_BE_NEGATIVE = " не может быть отрицательным";
    private static final String EMPTY_STRING = "Поле %s не может быть пустым";
    private static final String BET_MORE_THAN_BALANCE = "Ставка не может быть больше суммарного баланса";
    private static final String SIMULATIONS_MAX = "Ваша версия программы позволяет рассчитывать бонус с количеством симуляций не более %d";
    private static final String BONUS_PERFORM_MAX = "Ваша версия программы не позволяет рассчитывать бонусы более %d раз";
    private static final String PRO_SIM_MAX_MESSAGE = "Количество симуляций не должно превышать 1 000 000";
    private static final String BONUS_PARTS_MAX_MESSAGE = "Бонус не может разбиваться больше, чем на 100 частей";
    private static final String WAGGER_MAX_MESSAGE = "Ваггер не может быть больше 200";
    private static final String ACCURACY_RANGE_MESSAGE = "Точность расчета должна быть числом от 0 до 100 (не включительно)";
    private static final String ACCURACY_PARAMETER = "Точность расчета EV";
    private static final String LOGIN_VALIDATION = "Логин должен содержать латинские буквы или цифры. Быть длиной не менее 5 символов и не более 30 символов";
    private static final String PASSWORD_VALIDATION = "Пароль должен содержать строчные и прописные латинские буквы, цифры, спецсимволы. Быть длиной не менее 8 символов и не более 30 символов";
    private static final String CONFIRM_PASSWORD = "Пароли должны совпадать";
    private static final String EMAIL_VALIDATION = "Введите корректный Email";
    private static final String FROM_0_TO_1 = " должно принимать значения от 0 до 1 (включительно)";
    private static final long DEMO_SIM_MAX = 50000;
    private static final int DEMO_BONUS_PERFORM_MAX = 50;
    private static final long LITE_SIM_MAX = 100000;
    private static final int LITE_BONUS_PERFORM_MAX = 2;
    private static final long PRO_SIM_MAX = 1000000;
    private static final int BONUS_PARTS_MAX = 100;
    private static final int WAGGER_MAX_VALUE = 200;
    private static final double MIN_BET = 0.1;
    private static final double RELATION_BALANCE_BET = 500;
    private static final double BASIC_ACCURACY = 95;

    public static final UsersBonusAuditService bonusAuditService = new UsersBonusAuditServiceImpl();

    public static double convertStringToDouble(String parameter, String nameOfParameter){
        try {
            if (parameter==null){
                String errorNull = NOT_INPUT_PARAMETER + nameOfParameter;
                throw new CustomValidation(errorNull);
            }
            double result = Double.parseDouble(parameter);
            if (result < 0){
                String errorNegative = nameOfParameter + CANT_BE_NEGATIVE;
                throw new CustomValidation(errorNegative);
            }
            return result;
        } catch (NumberFormatException e){
            String errorMessage = INCORRECT_PARAMETER + nameOfParameter;
            throw new CustomValidation(errorMessage);
        }
    }

    public static double convertStringToDoubleNull(String parameter, String nameOfParameter){
        try {
            if (parameter == null || parameter.equals("")){
                return 0D;
            }
            double result = Double.parseDouble(parameter);
            if (result < 0){
                String errorNegative = nameOfParameter + CANT_BE_NEGATIVE;
                throw new CustomValidation(errorNegative);
            }
            return result;
        } catch (NumberFormatException e){
            String errorMessage = INCORRECT_PARAMETER + nameOfParameter;
            throw new CustomValidation(errorMessage);
        }
    }

    public static int convertStringToInt(String parameter, String nameOfParameter){
        try {
            if (parameter==null){
                String errorMessage = NOT_INPUT_PARAMETER + nameOfParameter;
                throw new CustomValidation(errorMessage);
            }
            int result = Integer.parseInt(parameter);
            if (result < 0){
                String errorNegative = nameOfParameter + CANT_BE_NEGATIVE;
                throw new CustomValidation(errorNegative);
            }
            return result;
        } catch (NumberFormatException e){
            String errorMessage = INCORRECT_PARAMETER + nameOfParameter;
            throw new CustomValidation(errorMessage);
        }
    }

    public static long convertStringToLong(String parameter, String nameOfParameter){
        try {
            if (parameter==null || parameter.equals("")){
                String errorMessage = String.format(NOT_EMPTY_PARAMETER,nameOfParameter);
                throw new CustomValidation(errorMessage);
            }
            long result = Long.parseLong(parameter);
            if (result < 0){
                String errorNegative = nameOfParameter + CANT_BE_NEGATIVE;
                throw new CustomValidation(errorNegative);
            }
            return result;
        } catch (NumberFormatException e){
            String errorMessage = INCORRECT_PARAMETER + nameOfParameter;
            throw new CustomValidation(errorMessage);
        }
    }

    public static double convertAccuracy(String accuracy){
        try {
            if (accuracy == null || accuracy.equals("")){
                return BASIC_ACCURACY;
            }
            double result = Double.parseDouble(accuracy);
            if (result < 0 || result >= 100){
                throw new CustomValidation(ACCURACY_RANGE_MESSAGE);
            }
            return result;
        } catch (NumberFormatException e){
            String errorMessage = INCORRECT_PARAMETER + ACCURACY_PARAMETER;
            throw new CustomValidation(errorMessage);
        }
    }

    public static int convertStringToIntNull(String parameter, String nameOfParameter){
        try {
            if (parameter == null || parameter.equals("")){
                return 0;
            }
            int result = Integer.parseInt(parameter);
            if (result < 0){
                String errorNegative = nameOfParameter + CANT_BE_NEGATIVE;
                throw new CustomValidation(errorNegative);
            }
            return result;
        } catch (NumberFormatException e){
            String errorMessage = INCORRECT_PARAMETER + nameOfParameter;
            throw new CustomValidation(errorMessage);
        }
    }

    public static double convertStringToDoubleMaxOne(String value, String nameOfParameter){
        try{
            if(value == null || value.equals("")){
                return 0;
            }
            double result = Double.parseDouble(value);
            if(result < 0 || result > 1){
                String errorMessage = nameOfParameter + FROM_0_TO_1;
                throw  new CustomValidation(errorMessage);
            }
            return result;
        } catch (NumberFormatException e){
            String errorMessage = INCORRECT_PARAMETER + nameOfParameter;
            throw new CustomValidation(errorMessage);
        }
    }

    public static boolean convertCheckbox(String checkBox){
        return checkBox == null;
    }

    public static String validateSize(String str, int size){
        if (str.length()<=size){
            return str;
        } else {
            String message = "Длина параметра не должна превышать " + size + " символов.";
            throw new CustomValidation(message);
        }
    }

    public static void validateSlotInput(SlotInput slotInput){
        if (slotInput.getSim() == ZERO){
            throw  new CustomValidation(NOT_NULL_SIM);
        }
        if (slotInput.getWaggerType() == WaggerType.B && slotInput.getBonus() == ZERO){
            throw  new CustomValidation(WAGGER_BONUS);
        }
        slotInput.setBalance();
        if (slotInput.getBet() > slotInput.getBalance()){
            throw new CustomValidation(BET_MORE_THAN_BALANCE);
        }
        if (slotInput.getWagger() > WAGGER_MAX_VALUE && slotInput.getUserGrants() != UserGrants.ADMIN){
            throw new CustomValidation(WAGGER_MAX_MESSAGE);
        }
        if (slotInput.getBonusParts() > BONUS_PARTS_MAX){
            throw new CustomValidation(BONUS_PARTS_MAX_MESSAGE);
        }
        if (slotInput.getBet() == 0){
            throw new CustomValidation("Ставка не может быть нулевой!");
        }
        if (slotInput.getBet() < MIN_BET){
            throw new CustomValidation(String.format("Ставка не может быть меньше %1.1f", MIN_BET));
        }
        if (slotInput.getUserGrants() != UserGrants.ADMIN &&
                slotInput.getBalance()/slotInput.getBet() > RELATION_BALANCE_BET){
            throw new CustomValidation(String.format("Ставка не может быть меньше баланса в %3.0f раз. Увеличьте размер ставки или уменьшите размер баланса.", RELATION_BALANCE_BET));
        }
        switch (slotInput.getUserGrants()) {
            case DEMO :
                checkMode(slotInput, DEMO_SIM_MAX, DEMO_BONUS_PERFORM_MAX);
                break;
            case LITE :
                checkMode(slotInput, LITE_SIM_MAX, LITE_BONUS_PERFORM_MAX);
                break;
            case PRO :
                if (slotInput.getSim() > PRO_SIM_MAX){
                    throw new CustomValidation(PRO_SIM_MAX_MESSAGE);
                }
                break;
        }
    }

    private static void checkMode(SlotInput slotInput, long simMax, int bonusCount){
        if(slotInput.getSim() > simMax){
            throw new CustomValidation(String.format(SIMULATIONS_MAX, simMax));
        }
        int bonusPerformCount = bonusAuditService.getBonusPerformCount("test_user");
        if (bonusPerformCount >= bonusCount){
            throw new CustomValidation(String.format(BONUS_PERFORM_MAX, bonusCount));
        }
    }

    public static String validateLogin(String login){
        if(login == null || login.equals("")){
            throw new CustomValidation(LOGIN_VALIDATION);
        }
        if (!Pattern.matches("^[A-Za-z0-9]{5,30}$", login)){
            throw new CustomValidation(LOGIN_VALIDATION);
        }
        return login;
    }

    public static String validatePassword(String password){
        if(password == null || password.equals("") || password.length() > 30){
            throw new CustomValidation(PASSWORD_VALIDATION);
        }
        if(!Pattern.matches("(?=^.{8,}$)((?=.*\\d)|(?=.*\\W+))(?![.\\n])(?=.*[A-Z])(?=.*[a-z]).*$",password)){
            throw new CustomValidation(PASSWORD_VALIDATION);
        }
        return password;
    }

    public static void validateConfirmPassword(String confirmPassword, String password){
        if(!confirmPassword.equals(password)){
            throw new CustomValidation(CONFIRM_PASSWORD);
        }
    }

    public static String validateEmail(String email){
        if(email == null || email.equals("") || email.length() > 100){
            throw new CustomValidation(EMAIL_VALIDATION);
        }
        if(!Pattern.matches("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$",email)){
            throw new CustomValidation(EMAIL_VALIDATION);
        }
        return email;
    }

    public static void validateEmptyString(String string, String parameterName){
        if(string == null || string.equals("")){
            throw new CustomValidation(String.format(EMPTY_STRING, parameterName));
        }
    }
}