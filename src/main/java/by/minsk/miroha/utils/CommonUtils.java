package by.minsk.miroha.utils;

import by.minsk.miroha.entities.databases.SlotName;
import by.minsk.miroha.exceptions.CustomValidation;

import java.util.Arrays;

public class CommonUtils {

    public static String getUrlParam(String path){
        try {
            int startParam = path.indexOf("=")+1;
            String rawParam =  path.substring(startParam);
            return rawParam.replace("%20", " ");
        } catch (Exception ex){
            throw new CustomValidation("Невозможно определить версию программы");
        }
    }

    public static String getDatabaseName(String schema, String table){
        return schema + "." + table;
    }

    public static String cleanURLPath(String site){
        site = site.replace("www.", "");
        site = site.replace("http://", "");
        site = site.replace("https://", "");
        site = removeLastSlash(site);
        return site;
    }

    public static String formatProvider(String provider){
        if(provider != null && provider.length() > 0) {
            provider = provider.replace("_", " ");
            return provider.substring(0, 1).toUpperCase() + provider.substring(1);
        } else {
            return "";
        }
    }

    public static String formatSlot(String slot){
        if(slot != null && slot.length() > 0) {
            String[] words = slot.split("_");
            for (int i = 0; i < words.length; i++) {
                if (i == 0) {
                    words[i] = words[i].substring(0, 1).toUpperCase() + words[i].substring(1);
                }
                if (i > 0 && !words[i].equals("of") && !words[i].equals("the") && !words[i].equals("and")){
                    words[i] = words[i].substring(0, 1).toUpperCase() + words[i].substring(1);
                }
            }
            return Arrays.stream(words).reduce((word1, word2) -> word1 + " " + word2).orElse("");
        } else {
            return "";
        }
    }

    public static SlotName getSlotName(String game){
        String[] names = game.split(" ");
        if(names.length == 2){
            return new SlotName(names[0], names[1]);
        } else {
            return new SlotName();
        }
    }

    public static String removeSlash(String str){
        int slashIndex = str.indexOf("/");
        if(slashIndex == -1){
            return str;
        } else {
            return str.substring(0, slashIndex);
        }
    }

    public static String removeLastSlash(String str){
        if(str == null || str.length() == 0){
            return str;
        }
        int length = str.length();
        char lastChar = str.charAt(length-1);
        if(lastChar == '/'){
            return str.substring(0, length-1);
        } else {
            return str;
        }
    }

}
