package by.minsk.miroha.utils;

import by.minsk.miroha.entities.SlotInput;
import by.minsk.miroha.entities.SlotInputError;
import by.minsk.miroha.entities.common.Slot;
import by.minsk.miroha.entities.enums.JackpotSlot;
import by.minsk.miroha.entities.enums.TimePeriods;
import by.minsk.miroha.entities.enums.UserGrants;
import by.minsk.miroha.entities.enums.WaggerType;
import by.minsk.miroha.entities.front.CasinoReportFront;
import by.minsk.miroha.entities.front.SlotInputFront;
import by.minsk.miroha.entities.front.SlotInputStartCasino;
import by.minsk.miroha.entities.front.requests.UserRegistrationRequest;
import by.minsk.miroha.entities.users.User;
import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.report.CasinoReport;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static by.minsk.miroha.controllers.spring.file.SlotInputController.*;

public class Converters {

    private static final String INCORRECT_JACKPOT_SLOT = "Выбран несуществующий слот!";
    private static final String INCORRECT_TIME_PERIOD = "Выбран не корректный период времени!";
    private static final String INCORRECT_DATE_FORMAT = "Некорректный формат даты!";

    public static SlotInput convert(SlotInputFront slotInputFront, UserGrants role) throws CustomValidation {
        SlotInput slotInput = new SlotInput();
        slotInput.setDeposit(Validator.convertStringToDouble(slotInputFront.getDeposit(), DEPOSIT));
        slotInput.setBonus(Validator.convertStringToDouble(slotInputFront.getBonus(), BONUS));
        slotInput.setWithoutBonus(slotInputFront.isWithoutBonus());
        slotInput.setTaxes(Validator.convertStringToDoubleNull(slotInputFront.getTaxes(), TAXES));
        slotInput.setPaymentFee(Validator.convertStringToDoubleNull(slotInputFront.getPaymentFee(), PAYMENT_FEE));
        slotInput.setPlatformFee(Validator.convertStringToDoubleNull(slotInputFront.getPlatformFee(), PLATFORM_FEE));
        slotInput.setProviderFee(Validator.convertStringToDoubleNull(slotInputFront.getProviderFee(), PROVIDER_FEE));
        slotInput.setBet(Validator.convertStringToDouble(slotInputFront.getBet(), BET));
        slotInput.setProfitLimit(Validator.convertStringToDoubleNull(slotInputFront.getProfitLimit(), MAX_PROFIT));
        slotInput.setWaggerType(WaggerType.findByName(slotInputFront.getWaggerType()));
        slotInput.setWagger(Validator.convertStringToInt(slotInputFront.getWagger(),WAGGER));
        slotInput.setBonusParts(Validator.convertStringToIntNull(slotInputFront.getBonusParts(), BONUS_PARTS));
        slotInput.setSim(Validator.convertStringToInt(slotInputFront.getSim(), SIM));
        slotInput.setManual(slotInputFront.isManual());
        slotInput.setSpinLimit(Validator.convertStringToDoubleNull(slotInputFront.getSpinLimit(), SPIN_LIMIT));
        slotInput.setAdminMode(slotInput.isAdminMode());
        slotInput.setUserGrants(role);

        Validator.validateSlotInput(slotInput);
        return slotInput;
    }

    public static SlotInputError convertError(SlotInputFront slotInputFront){
        SlotInputError errorInput = new SlotInputError();
        errorInput.setDeposit(slotInputFront.getDeposit());
        errorInput.setBonus(slotInputFront.getBonus());
        errorInput.setTaxes(slotInputFront.getTaxes());
        errorInput.setBet(slotInputFront.getBet());
        errorInput.setWaggerType(slotInputFront.getWaggerType());
        errorInput.setWagger(slotInputFront.getWagger());
        errorInput.setSim(slotInputFront.getSim());
        return errorInput;
    }

    public static SlotInput convertStartCasino(SlotInputStartCasino slotInputStartCasino){
        SlotInput slotInput = new SlotInput();
        slotInput.setDeposit(Validator.convertStringToDouble(slotInputStartCasino.getDeposit(), DEPOSIT));
        double bonus = Validator.convertStringToDouble(slotInputStartCasino.getBonus(), BONUS) / 100;
        slotInput.setBonus(slotInput.getDeposit()*bonus);
        slotInput.setWithoutBonus(false);
        slotInput.setTaxes(0D);
        slotInput.setPaymentFee(0D);
        slotInput.setProviderFee(0D);
        slotInput.setPlatformFee(0D);
        slotInput.setBet(5D);
        slotInput.setProfitLimit(0D);
        slotInput.setWaggerType(WaggerType.B);
        slotInput.setWagger(45);
        slotInput.setBonusParts(0);
        slotInput.setSim(1000000);
        slotInput.setManual(true);
        slotInput.setUserGrants(UserGrants.PRO);
        return slotInput;
    }

    public static String convertSortType(String sortType){
        if(sortType != null && sortType.equals("desc")){
            return "DESC";
        } else {
            return "ASC";
        }
    }

    public static String convertSortField(String sortField){
        switch (sortField){
            case "provider" : return "provider";
            case "slot" : return "slot";
            case "rtp" : return "rtp";
            case "volatility" : return "volatility";
            case "x100" : return "x100";
            case "x1000" : return "x1000";
            case "x5000" : return "x5000";
            case "x10000" : return "x10000";
            case "x20000" : return "x20000";
            case "x50000" : return "x50000";
            default: return "provider";
        }
    }

    public static JackpotSlot convertJackpotSlot(String jackpotSlot){
        switch (jackpotSlot){
            case "megaJoker" : return JackpotSlot.MEGA_JOKER;
            case "divineFortune" : return JackpotSlot.DIVINE_FORTUNE;
            case "mercyOfTheGods" : return JackpotSlot.MERCY_OF_THE_GODS;
            case "vegasNightLive" : return JackpotSlot.VEGAS_NIGHT_LIVE;
            case "grandSpinSuperpot" : return JackpotSlot.GRAND_SPINN_SUPERPOT;
            default: throw new CustomValidation(INCORRECT_JACKPOT_SLOT);
        }
    }

    public static String convertJackpotSlotName(String slotName){
        switch (slotName){
            case "Mega joker" : return "megaJoker";
            case "Divine fortune" : return "divineFortune";
            case "Mercy of the Gods" : return "mercyOfTheGods";
            case "Vegas Night Life" : return "vegasNightLive";
            case "Grand Spinn Superpot" : return "grandSpinSuperpot";
            default: return "";
        }
    }

    public static TimePeriods convertTimePeriod(String timePeriod){
        switch (timePeriod){
            case "day" : return TimePeriods.DAY;
            case "month" : return TimePeriods.MONTH;
            case "year" : return TimePeriods.YEAR;
            default: throw new CustomValidation(INCORRECT_TIME_PERIOD);
        }
    }

    public static User convertUserRequest(UserRegistrationRequest userRequest){
        User user = new User();
        user.setLogin(Validator.validateLogin(userRequest.getLogin()));
        user.setEmail(Validator.validateEmail(userRequest.getEmail()));
        user.setPassword(Validator.validatePassword(userRequest.getPassword()));
        user.setRole(UserGrants.PRO);
        Validator.validateConfirmPassword(userRequest.getConfirmPassword(), userRequest.getPassword());
        return user;
    }

    public static CasinoReportFront convertCasinoReport(CasinoReport report){
        CasinoReportFront reportFront = new CasinoReportFront();
        reportFront.setSlot(new Slot(report.getSlot()));
        reportFront.setProvider(CommonUtils.formatProvider(report.getProvider()));
        reportFront.setRtp(String.format("%.2f", report.getRtp()*100));
        reportFront.setVolatility(String.format("%.0f", report.getVolatility()));
        reportFront.setX100(report.getX100());
        reportFront.setX1000(report.getX1000());
        reportFront.setX5000(report.getX5000());
        reportFront.setX10000(report.getX10000());
        reportFront.setX20000(report.getX20000());
        report.setX50000(report.getX50000());
        reportFront.setSlotId(report.getSlotId());
        return reportFront;
    }

    public static Timestamp convertStringDate(String stringDate){
        if(stringDate == null || stringDate.equals("")){
            return null;
        }
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date = formatter.parse(stringDate);
            return new Timestamp(date.getTime());
        } catch (ParseException e) {
            throw new CustomValidation(INCORRECT_DATE_FORMAT);
        }
    }
}