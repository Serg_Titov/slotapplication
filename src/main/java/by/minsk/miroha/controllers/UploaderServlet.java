package by.minsk.miroha.controllers;

import by.minsk.miroha.entities.databases.SlotName;
import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.repositories.SlotInfoRepository;
import by.minsk.miroha.repositories.SlotNameRepository;
import by.minsk.miroha.repositories.impl.SlotInfoRepositoryImpl;
import by.minsk.miroha.repositories.impl.SlotNameRepositoryImpl;
import by.minsk.miroha.uploading.CSVUploader;
import by.minsk.miroha.utils.Validator;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static by.minsk.miroha.controllers.spring.file.UploaderController.*;

public class UploaderServlet extends HttpServlet {





    private static final SlotNameRepository nameRepository = new SlotNameRepositoryImpl();
    private static final SlotInfoRepository infoRepository = new SlotInfoRepositoryImpl();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("upload-ldb.jsp");
        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try{
            if (req.getParameter("slotName") == null || "".equals(req.getParameter("slotName"))){
                throw new CustomValidation(new String(NAME_NOT_NULL.getBytes(),StandardCharsets.UTF_8));
            }
            req.setAttribute("slotName", req.getParameter("slotName"));
            if (req.getParameter("provider") == null || "".equals(req.getParameter("provider"))){
                throw new CustomValidation(new String(PROVIDER_NOT_NULL.getBytes(), StandardCharsets.UTF_8));
            }
            SlotName slotName = new SlotName();
            slotName.setSlotName(Validator.validateSize(req.getParameter("slotName"), SIZE_SLOT_NAME));
            slotName.setProvider(Validator.validateSize(req.getParameter("provider"), SIZE_PROVIDER));
            nameRepository.save(slotName);
            CSVUploader.uploadSlotInfoToHSQLDB();
            String uploadedMessage = "В базу загружено " + infoRepository.countAll() + " строк.";
            /*try {
                SlotDataUploader dataUploader = new SlotDataUploader();
                dataUploader.uploadSlotInformation(slotName.getSlotName(), slotName.getProvider());
            } catch (CustomValidation e){
                req.setAttribute("incorrectData", e.getMessage());
            }*/
            req.setAttribute("uploaded", new String(uploadedMessage.getBytes(), StandardCharsets.UTF_8));
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("start-page.jsp");
            requestDispatcher.forward(req, resp);
        } catch (CustomValidation e){
            if (req.getParameter("provider") != null && !("".equals(req.getParameter("provider")))) {
                req.setAttribute("provider", req.getParameter("slotName"));
            }
            req.setAttribute("error", e.getMessage());
            doGet(req, resp);
        }
    }
}