package by.minsk.miroha.controllers;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.minsk.miroha.entities.SlotInput;
import by.minsk.miroha.entities.SlotInputError;
import by.minsk.miroha.entities.enums.WaggerType;
import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.report.ReportPerformer;
import by.minsk.miroha.report.SlotInputReport;
import by.minsk.miroha.repositories.SlotNameRepository;
import by.minsk.miroha.repositories.impl.SlotNameRepositoryImpl;
import by.minsk.miroha.utils.Validator;

import static by.minsk.miroha.controllers.spring.file.SlotInputController.*;


public class SlotInputServlet extends HttpServlet {

    private static final int ZERO = 0;

    private static final SlotNameRepository nameRepository = new SlotNameRepositoryImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("slot-info.jsp");
        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            SlotInput slotInput = new SlotInput();
            slotInput.setDeposit(Validator.convertStringToDouble(req.getParameter("deposit"), DEPOSIT));
            slotInput.setBonus(Validator.convertStringToDouble(req.getParameter("bonus"), BONUS));
            slotInput.setWithoutBonus(!Validator.convertCheckbox(req.getParameter("withoutBonus")));
            slotInput.setBet(Validator.convertStringToDouble(req.getParameter("bet"), BET));
            slotInput.setWaggerType(WaggerType.findByName(req.getParameter("waggerType")));
            slotInput.setWagger(Validator.convertStringToInt(req.getParameter("wagger"),WAGGER));
            slotInput.setSim(Validator.convertStringToInt(req.getParameter("sim"), SIM));
            slotInput.setManual(Validator.convertCheckbox(req.getParameter("manual")));
            slotInput.setGame(nameRepository.getSlotName().getFullName());
            if (slotInput.getSim() == ZERO){
                throw  new CustomValidation(new String(NOT_NULL_SIM.getBytes(), StandardCharsets.UTF_8));
            }
            if (slotInput.getWaggerType() == WaggerType.B && slotInput.getBonus() == 0){
                throw  new CustomValidation(new String(WAGGER_BONUS.getBytes(), StandardCharsets.UTF_8));
            }
            ReportPerformer reportPerformer = new ReportPerformer();
            SlotInputReport report = reportPerformer.getReport(slotInput, false);
            req.setAttribute("report", report);

            doGet(req, resp);
        } catch (CustomValidation e){
            SlotInputError errorInput = new SlotInputError();
            errorInput.setDeposit(req.getParameter("deposit"));
            errorInput.setBonus(req.getParameter("bonus"));
            errorInput.setBet(req.getParameter("bet"));
            errorInput.setWaggerType(req.getParameter("waggerType"));
            errorInput.setWagger(req.getParameter("wagger"));
            errorInput.setSim(req.getParameter("sim"));
            req.setAttribute("errorInput", errorInput);
            req.setAttribute("errorMessage", e.getMessage());
            doGet(req, resp);
        }

    }
}