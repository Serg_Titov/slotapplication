package by.minsk.miroha.controllers;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.minsk.miroha.repositories.SlotInfoRepository;
import by.minsk.miroha.repositories.SlotNameRepository;
import by.minsk.miroha.repositories.impl.SlotInfoRepositoryImpl;
import by.minsk.miroha.repositories.impl.SlotNameRepositoryImpl;

import static by.minsk.miroha.controllers.spring.DeleteOwnDBController.DB_CLEARED;

public class DeleteHomeServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        SlotInfoRepository infoRepository = new SlotInfoRepositoryImpl();
        infoRepository.clearTable();
        SlotNameRepository nameRepository = new SlotNameRepositoryImpl();
        nameRepository.clearTable();
        req.setAttribute("deleted", new String(DB_CLEARED.getBytes(), StandardCharsets.UTF_8));
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("start-page.jsp");
        requestDispatcher.forward(req, resp);
    }
}
