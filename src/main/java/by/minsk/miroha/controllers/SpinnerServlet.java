package by.minsk.miroha.controllers;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.perform.SpinnerPerformer;
import by.minsk.miroha.utils.Validator;

public class SpinnerServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("spin-home.jsp");
        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            int spinLimit = Validator.convertStringToInt(req.getParameter("spinLimit"), "Количество спинов");
            int sim = Validator.convertStringToInt(req.getParameter("sim"), "Количество симуляций");
            if (spinLimit < 1){
                throw new CustomValidation(new String("Не корректно введено количество спинов!".getBytes(), StandardCharsets.UTF_8));
            }
            if (sim < 1){
                throw new CustomValidation(new String("Не корректно введено количество симуляций!".getBytes(), StandardCharsets.UTF_8));
            }
            SpinnerPerformer spinnerPerformer = new SpinnerPerformer();
            double spinResult = spinnerPerformer.getSpinnerResult(spinLimit, sim, false);
            req.setAttribute("spinLimit", req.getParameter("spinLimit"));
            req.setAttribute("sim", req.getParameter("sim"));
            req.setAttribute("result", spinResult);
        } catch (CustomValidation e){
            req.setAttribute("spinError", req.getParameter("spinLimit"));
            req.setAttribute("simError", req.getParameter("sim"));
            req.setAttribute("errorMessage", e.getMessage());
        }
        doGet(req, resp);
    }
}
