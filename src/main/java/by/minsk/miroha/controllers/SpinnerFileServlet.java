package by.minsk.miroha.controllers;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.minsk.miroha.perform.SpinnerWriter;

import static by.minsk.miroha.controllers.spring.file.SpinnerFileController.COMPLETED;

public class SpinnerFileServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        SpinnerWriter spinnerWriter = new SpinnerWriter();
        spinnerWriter.writeToCSV(false);
        req.setAttribute("completed", new String(COMPLETED.getBytes(), StandardCharsets.UTF_8));
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("start-page.jsp");
        requestDispatcher.forward(req, resp);
    }
}
