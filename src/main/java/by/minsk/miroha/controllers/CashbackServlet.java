package by.minsk.miroha.controllers;

import by.minsk.miroha.entities.cashback.CashBack;
import by.minsk.miroha.perform.CashbackWriter;
import by.minsk.miroha.report.CashbackReport;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

import static by.minsk.miroha.controllers.spring.file.CashbackController.CASHBACK;

public class CashbackServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        CashbackReport report = new CashbackReport();
        List<CashBack> cashBackList = report.getReportOld();
        CashbackWriter cashbackWriter = new CashbackWriter();
        cashbackWriter.writeCashbackReport(cashBackList);
        req.setAttribute("cashback", new String(CASHBACK.getBytes(), StandardCharsets.UTF_8));
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("start-page.jsp");
        requestDispatcher.forward(req, resp);
    }
}
