package by.minsk.miroha.controllers.spring.jackpots;

import by.minsk.miroha.entities.front.responses.JackpotSummaryResponse;
import by.minsk.miroha.perform.JackpotReportLoader;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@CrossOrigin
public class JackpotSummaryController {

    private final JackpotReportLoader loader;

    public JackpotSummaryController(JackpotReportLoader loader) {
        this.loader = loader;
    }

    @GetMapping("/jackpot-summary")
    public @ResponseBody JackpotSummaryResponse getSummaryReport(){
        return loader.getTotalReport();
    }
}
