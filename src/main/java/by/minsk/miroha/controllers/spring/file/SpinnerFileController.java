package by.minsk.miroha.controllers.spring.file;

import by.minsk.miroha.perform.SpinnerWriter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@CrossOrigin
public class SpinnerFileController {

    public static final String COMPLETED  = "информация в файл загружена";

    @GetMapping(value = "/spinner-home-file")
    public String performSpinner(Model model){
        SpinnerWriter spinnerWriter = new SpinnerWriter();
        spinnerWriter.writeToCSV(false);
        model.addAttribute("completed", COMPLETED);
        return "index";
    }
}
