package by.minsk.miroha.controllers.spring.sites;

import by.minsk.miroha.entities.configs.SwaggerConfig;
import by.minsk.miroha.entities.front.requests.LinkBonusRequest;
import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.services.report.SitesBonusesService;
import by.minsk.miroha.utils.Validator;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@CrossOrigin
@Api(tags = {SwaggerConfig.TO_LINK_BONUS})
public class LinkBonusController {

    private static final String BONUS_ID = "Id бонуса";
    private static final String BRAND = "Бренд";
    private static final String SITE = "Сайт";

    private final SitesBonusesService service;

    public LinkBonusController(SitesBonusesService service) {
        this.service = service;
    }

    @PostMapping(value = "/create-link")
    @ApiOperation(value = "Привязать рассчитанный бонус по его id к сайту казино по его названию бренда и сайта. При крайней необходимости не тестировать!!!")
    public @ResponseBody
    String getBonus (@ModelAttribute("link") LinkBonusRequest linkBonusRequest) {
        try {
            long idBonus = Validator.convertStringToLong(linkBonusRequest.getIdBonus(), BONUS_ID);
            Validator.validateEmptyString(linkBonusRequest.getBrand(), BRAND);
            Validator.validateEmptyString(linkBonusRequest.getSite(), SITE);
            service.saveBonusLink(idBonus, linkBonusRequest.getBrand(), linkBonusRequest.getSite());
            return "Success";
        } catch (CustomValidation ex){
            return ex.getMessage();
        }
    }
}
