package by.minsk.miroha.controllers.spring.common;

import by.minsk.miroha.utils.CommonUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@CrossOrigin
public class GetVersionController {

    @GetMapping(value = "/version")
    public @ResponseBody String getMethod(@ModelAttribute("path") String path){
        return CommonUtils.getUrlParam(path);
    }
}
