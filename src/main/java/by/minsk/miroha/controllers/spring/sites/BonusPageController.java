package by.minsk.miroha.controllers.spring.sites;

import by.minsk.miroha.entities.configs.SwaggerConfig;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@CrossOrigin
@Api(tags = {SwaggerConfig.COMPLETED_BONUS_PAGE})
public class BonusPageController {

    @GetMapping(value = "/bonus")
    @ApiOperation(value = "Получение страницы для отображения информации о рассчитанном ранее бонусе.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Страница успено передана"),
            @ApiResponse(code = 403, message = "Недостаточно прав"),
            @ApiResponse(code = 404, message = "Произошла ошибка при получении страницы"),
            @ApiResponse(code = 500, message = "Произошла ошибка при получении страницы")
    })
    public String getBonusPage (){
        return "bonus";
    }
}
