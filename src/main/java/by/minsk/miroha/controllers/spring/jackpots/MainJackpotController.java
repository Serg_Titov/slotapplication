package by.minsk.miroha.controllers.spring.jackpots;


import by.minsk.miroha.entities.configs.SwaggerConfig;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@CrossOrigin
@Api(tags = {SwaggerConfig.JACKPOTS_PAGE})
public class MainJackpotController {

    @ApiOperation(value = "Получение страницы для отображения информации о джекпотах. Страница не будет наполнена данными. Данные необходимо получать отдельными запросами")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Страница успено передана"),
            @ApiResponse(code = 403, message = "Недостаточно прав"),
            @ApiResponse(code = 404, message = "Произошла ошибка при получении страницы"),
            @ApiResponse(code = 500, message = "Произошла ошибка при получении страницы")
    })
    @GetMapping("/jackpots")
    public String getJackpotsPage (){
        return "main-jackpot";
    }

}
