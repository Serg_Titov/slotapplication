package by.minsk.miroha.controllers.spring.database;

import by.minsk.miroha.entities.configs.SwaggerConfig;
import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.services.GamesService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import static by.minsk.miroha.controllers.spring.database.CalcProviderSlotsController.UPLOADED;

@Controller
@CrossOrigin
@Api(tags = {SwaggerConfig.CALC_ALL_SLOTS})
public class CalcAllSlotsController {

    private final GamesService gamesService;

    public CalcAllSlotsController(GamesService gamesService) {
        this.gamesService = gamesService;
    }

    @GetMapping(value = "/calc-all-slots")
    @ApiOperation(value = "Получение страницы для расчета всех слотов (ранее не рассчитанных)")
    public String getSLotInfo(){
        return "calc-all-slots";
    }

    @PostMapping(value = "/calc-all-slots")
    @ApiOperation(value = "Рассчитать все ранее не рассчитанные слоты")
    public @ResponseBody
    String postMethod(){
        try {
            gamesService.calculateAllProviders();
            return UPLOADED;
        } catch (CustomValidation e){
            return e.getMessage();
        }
    }
}
