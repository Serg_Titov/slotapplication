package by.minsk.miroha.controllers.spring.jackpots;

import by.minsk.miroha.entities.configs.SwaggerConfig;
import by.minsk.miroha.entities.enums.jackpots.JackpotSlot;
import by.minsk.miroha.entities.front.requests.JackpotSiteRequest;
import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.perform.JackpotSitePerformer;
import by.minsk.miroha.report.jackpot.JackpotSiteReport;
import by.minsk.miroha.utils.Converters;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ModelAttribute;

@Controller
@CrossOrigin
@Api(tags = {SwaggerConfig.JACKPOT_PAGE})
public class SiteJackpotController {

    @GetMapping(value = "/jackpot")
    @ApiOperation(value = "Получение страницы для отображения информации о выбранном джекпоте. Информация передается с помощью POST запроса.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Страница успено передана"),
            @ApiResponse(code = 403, message = "Недостаточно прав"),
            @ApiResponse(code = 404, message = "Произошла ошибка при получении страницы"),
            @ApiResponse(code = 500, message = "Произошла ошибка при получении страницы")
    })
    public String getJackpotsPage (){
        return "site-jackpot";
    }

    @PostMapping(value = "/jackpot")
    @ApiOperation(value = "Получение информации о выбранном джекпоте. Информация для графиков передается отдельным контроллером.")
    public @ResponseBody
    JackpotSiteReport postMethod(@ModelAttribute("request") JackpotSiteRequest jackpotSiteRequest){
        try {
            JackpotSlot jackpotSlot = Converters.convertJackpotSlot(jackpotSiteRequest.getSlot());
            String site = jackpotSiteRequest.getSite();
            JackpotSitePerformer performer = new JackpotSitePerformer();
            return performer.getReport(jackpotSlot, site);
        } catch (CustomValidation ex){
            return new JackpotSiteReport();
        }
    }
}
