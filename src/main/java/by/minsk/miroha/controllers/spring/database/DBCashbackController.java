package by.minsk.miroha.controllers.spring.database;

import by.minsk.miroha.entities.configs.SwaggerConfig;
import by.minsk.miroha.entities.databases.SlotName;
import by.minsk.miroha.entities.front.requests.CashbackRequest;
import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.report.CashbackReport;
import by.minsk.miroha.services.GamesService;
import by.minsk.miroha.utils.Validator;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.List;

import static by.minsk.miroha.controllers.spring.file.CashbackController.CASHBACK;
import static by.minsk.miroha.controllers.spring.file.UploaderController.*;

@Controller
@CrossOrigin
@Api(tags = {SwaggerConfig.CASHBACK_DB})
public class DBCashbackController {

    private static final String CASHBACK_PARAMETER = "Кэшбек ";
    private static final String SIMULATIONS = "Количество симуляций ";

    private final GamesService gamesService;

    public DBCashbackController(GamesService gamesService) {
        this.gamesService = gamesService;
    }

    @GetMapping(value = "/db-cashback")
    @ApiOperation(value = "Получить страницу для расчета кэшбека на основании данных из внешней БД")
    public String getCashback(Model model){
        List<String> providers = gamesService.getAllProviders();
        model.addAttribute("providers", providers);
        return "db-cashback";
    }

    @PostMapping(value = "/db-cashback")
    @ApiOperation(value = "Рассчитать кэшбек на основании БД название, которой передано в параметрах")
    public @ResponseBody String performCashback(@ModelAttribute("cashbackRequest") CashbackRequest cashbackRequest){
        try {
            if (cashbackRequest.getSlotName() == null || "".equals(cashbackRequest.getSlotName())) {
                throw new CustomValidation(NAME_NOT_NULL);
            }
            if (cashbackRequest.getProvider() == null || "".equals(cashbackRequest.getProvider())) {
                throw new CustomValidation(PROVIDER_NOT_NULL);
            }
            SlotName verifiedSlotName = new SlotName();
            verifiedSlotName.setSlotName(Validator.validateSize(cashbackRequest.getSlotName(), SIZE_SLOT_NAME));
            verifiedSlotName.setProvider(Validator.validateSize(cashbackRequest.getProvider(), SIZE_PROVIDER));
            double cashback = Validator.convertStringToDoubleMaxOne(cashbackRequest.getCashbackValue(), CASHBACK_PARAMETER);
            int simulations = Validator.convertStringToInt(cashbackRequest.getSimAmount(), SIMULATIONS);
            CashbackReport report = new CashbackReport(verifiedSlotName);
            report.getReport(cashback, simulations);
            return CASHBACK;
        }catch (CustomValidation e){
            return e.getMessage();
        }
    }
}
