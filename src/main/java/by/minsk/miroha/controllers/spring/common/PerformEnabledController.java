package by.minsk.miroha.controllers.spring.common;

import by.minsk.miroha.entities.configs.SwaggerConfig;
import by.minsk.miroha.repositories.SlotInfoRepository;
import by.minsk.miroha.repositories.impl.SlotInfoRepositoryImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@CrossOrigin
@Api(tags = {SwaggerConfig.PERFORM_ENABLED})
public class PerformEnabledController {

    private static final SlotInfoRepository slotInfoRepository = new SlotInfoRepositoryImpl();

    @GetMapping(value = "/perform-enabled")
    @ApiOperation(value = "Если локальная БД наполнена данными - расчет бонуса или спинов возможен. Если локальная БД пустая, то нет.")
    public @ResponseBody
    boolean getMethod(){
        try {
            return slotInfoRepository.countAll() > 0;
        } catch (Exception e){
            return false;
        }

    }
}
