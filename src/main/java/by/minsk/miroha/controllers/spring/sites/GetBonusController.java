package by.minsk.miroha.controllers.spring.sites;

import by.minsk.miroha.entities.SlotInput;
import by.minsk.miroha.entities.configs.SwaggerConfig;
import by.minsk.miroha.entities.databases.SlotName;
import by.minsk.miroha.entities.front.requests.IdAttribute;
import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.report.ReportPerformer;
import by.minsk.miroha.report.SlotInputReport;
import by.minsk.miroha.services.SlotInputService;
import by.minsk.miroha.utils.CommonUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@CrossOrigin
@Api(tags = {SwaggerConfig.CALCULATED_BONUS})
public class GetBonusController {

    private final SlotInputService service;

    public GetBonusController(SlotInputService service) {
        this.service = service;
    }

    @PostMapping(value = "/get-bonus")
    @ApiOperation(value = "Получение отчета с рассчитанным ранее бонусом по его id в БД")
    public @ResponseBody
    SlotInputReport getBonus (@ModelAttribute("url") IdAttribute idAttribute){
        try{
            String idStr = CommonUtils.getUrlParam(idAttribute.getUrl());
            long bonusId = Long.parseLong(idStr);
            SlotInput slotInput = service.findById(bonusId);
            SlotName slotName = CommonUtils.getSlotName(slotInput.getGame());
            ReportPerformer performer = new ReportPerformer(slotName.getProvider(), slotName.getSlotName());
            return performer.getResultFromDatabase(slotInput);
        } catch (CustomValidation ex){
            return new SlotInputReport();
        }

    }
}
