package by.minsk.miroha.controllers.spring.sites;

import by.minsk.miroha.entities.configs.SwaggerConfig;
import by.minsk.miroha.report.entities.BrandsList;
import by.minsk.miroha.services.SitesService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@CrossOrigin
@Api(tags = {SwaggerConfig.BRANDS_LIST})
public class GetAllBrandsController {

    private final SitesService service;

    public GetAllBrandsController(SitesService service) {
        this.service = service;
    }

    @GetMapping(value = "/get-all-brands")
    @ApiOperation(value = "Получение списка всех брендов сайтов казино в виде списка строк, отсортированного в алфавитном порядке")
    public @ResponseBody
    BrandsList getAllBrands (){
        return service.getAllBrands();
    }

}
