package by.minsk.miroha.controllers.spring.database;

import by.minsk.miroha.entities.configs.SwaggerConfig;
import by.minsk.miroha.entities.front.SpinnerDBFront;
import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.perform.SpinnerPerformer;
import by.minsk.miroha.services.GamesService;
import by.minsk.miroha.services.impl.GamesServiceImpl;
import by.minsk.miroha.utils.Validator;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.List;

import static by.minsk.miroha.controllers.spring.file.SpinnerController.SIMULATIONS;
import static by.minsk.miroha.controllers.spring.file.SpinnerController.SPIN_LIMIT;

@Controller
@CrossOrigin
@Api(tags = {SwaggerConfig.CALC_SPIN})
public class SpinnerDatabaseController {

    private final GamesService gamesService = new GamesServiceImpl();

    @GetMapping(value = "/spinner-db")
    @ApiOperation(value = "Получить страницу для расчета Вероятности сделать N спинов и остаться в плюсе")
    public String getMethod(Model model){
        List<String> providers = gamesService.getAllProviders();
        model.addAttribute("providers", providers);
        return "spin-db";
    }

    @PostMapping(value = "/spinner-db")
    @ApiOperation(value = "Рассчитать вероятность сделать N спинов и остаться в плюсе на данных выбранного слота")
    public @ResponseBody
    SpinnerDBFront postMethod(@ModelAttribute("spinnerFront") SpinnerDBFront spinnerFront){
        try {
            int spinLimit = Validator.convertStringToInt(spinnerFront.getSpinLimit(), SPIN_LIMIT);
            int sim = Validator.convertStringToInt(spinnerFront.getSimulations(), SIMULATIONS );
            if (spinLimit < 1){
                throw new CustomValidation("Не корректно введено количество спинов!");
            }
            if (sim < 1){
                throw new CustomValidation("Не корректно введено количество симуляций!");
            }
            SpinnerPerformer spinnerPerformer = new SpinnerPerformer(spinnerFront.getProvider(), spinnerFront.getGame());
            double spinResult = spinnerPerformer.getSpinnerResult(spinLimit, sim, true);
            spinnerFront.setResult(Double.toString(spinResult));
            return spinnerFront;
        } catch (CustomValidation e){
            spinnerFront.setResult(e.getMessage());
            return spinnerFront;
        }
    }

}
