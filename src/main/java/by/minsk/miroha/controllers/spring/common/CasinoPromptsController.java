package by.minsk.miroha.controllers.spring.common;

import by.minsk.miroha.entities.CasinoPrompts;
import by.minsk.miroha.entities.configs.CasinoPromptsConfig;
import by.minsk.miroha.entities.configs.SwaggerConfig;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@CrossOrigin
@Api(tags = {SwaggerConfig.BONUS_PROMPTS})
public class CasinoPromptsController {

    @Autowired
    private CasinoPromptsConfig casinoPromptsConfig;

    @GetMapping(value = "/get-casino-prompts")
    @ApiOperation(value = "Получить подсказки для страницы рассчитанного бонуса в виде объекта")
    public @ResponseBody
    CasinoPrompts postMethod(){
        return casinoPromptsConfig.getCasinoPrompts();
    }
}
