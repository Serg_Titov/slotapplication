package by.minsk.miroha.controllers.spring;

import by.minsk.miroha.entities.configs.SwaggerConfig;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@CrossOrigin
@Api(tags = {SwaggerConfig.DOCUMENTATION})
public class DocumentationController {

    @GetMapping(value = "/documentation")
    @ApiOperation(value = "Получить страницу с документацией приложения")
    public String getMethod(){
        return "documentation";
    }
}
