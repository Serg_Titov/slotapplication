package by.minsk.miroha.controllers.spring.sites;

import by.minsk.miroha.entities.configs.SwaggerConfig;
import by.minsk.miroha.entities.front.requests.NewSitesRequest;
import by.minsk.miroha.entities.front.responses.SiteChangingReport;
import by.minsk.miroha.services.SitesService;
import by.minsk.miroha.utils.Converters;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


import java.sql.Timestamp;

@Controller
@CrossOrigin
@Api(tags = {SwaggerConfig.NEW_SITES_PAGE})
public class NewSitesController {

    private final SitesService service;

    public NewSitesController(SitesService service) {
        this.service = service;
    }

    @PostMapping(value = "/new-sites", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Получение списка новых сайтов и удаленных сайтов за указанный промежуток времени")
    public @ResponseBody
    SiteChangingReport postMethod(@ModelAttribute("url") NewSitesRequest url){
        Timestamp startDate = Converters.convertStringDate(url.getStartDate());
        Timestamp endDate = Converters.convertStringDate(url.getEndDate());
        return service.getSiteChanging(startDate, endDate);
    }

    @GetMapping(value = "/new-sites")
    @ApiOperation(value = "Получение страницы для отображения информации об изменении списка сайтов.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Страница успено передана"),
            @ApiResponse(code = 403, message = "Недостаточно прав"),
            @ApiResponse(code = 404, message = "Произошла ошибка при получении страницы"),
            @ApiResponse(code = 500, message = "Произошла ошибка при получении страницы")
    })
    public String getNewSitesPage(){
        return "new-sites";
    }
}
