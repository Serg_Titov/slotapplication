package by.minsk.miroha.controllers.spring;

import by.minsk.miroha.entities.configs.SwaggerConfig;
import by.minsk.miroha.entities.databases.SlotName;
import by.minsk.miroha.repositories.SlotInfoRepository;
import by.minsk.miroha.repositories.SlotNameRepository;
import by.minsk.miroha.repositories.impl.SlotInfoRepositoryImpl;
import by.minsk.miroha.repositories.impl.SlotNameRepositoryImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@CrossOrigin
@Api(tags = {SwaggerConfig.CLEAR_LOCAL_DB})
public class DeleteOwnDBController {

    public static final String DB_CLEARED = "База данных очищена";

    @GetMapping(value = "/deleteHome")
    @ApiOperation(value = "Очистить локальную базу данных и вернуться на страницу загрузки данных в локальную базу данных")
    public String deleteDB(Model model){
        SlotInfoRepository infoRepository = new SlotInfoRepositoryImpl();
        infoRepository.clearTable();
        SlotNameRepository nameRepository = new SlotNameRepositoryImpl();
        nameRepository.clearTable();
        model.addAttribute("deleted", DB_CLEARED);
        model.addAttribute("deleteDisabled", "disabled");
        model.addAttribute("uploadDisabled", "visible");
        model.addAttribute("slotName", new SlotName());
        return "upload-ldb";
    }
}
