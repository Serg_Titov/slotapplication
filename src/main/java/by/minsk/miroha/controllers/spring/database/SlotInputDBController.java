package by.minsk.miroha.controllers.spring.database;

import by.minsk.miroha.audit.AuditUtils;
import by.minsk.miroha.entities.common.Provider;
import by.minsk.miroha.entities.SlotInput;
//import by.minsk.miroha.entities.enums.UserGrants;
import by.minsk.miroha.entities.enums.WaggerType;
import by.minsk.miroha.entities.front.SlotInputFront;
import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.report.ReportPerformer;
import by.minsk.miroha.report.SlotInputReport;
//import by.minsk.miroha.repositories.UserSecurityRepository;
//import by.minsk.miroha.security.utils.PrincipalUtils;
import by.minsk.miroha.services.GamesService;
import by.minsk.miroha.utils.Converters;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ModelAttribute;

//import java.security.Principal;
import java.util.List;



@Controller
@CrossOrigin
public class SlotInputDBController {

    private final GamesService gamesService;
    //private final UserSecurityRepository userRepository;

    public SlotInputDBController(GamesService gamesService/*, UserSecurityRepository userRepository*/) {
        this.gamesService = gamesService;
        //this.userRepository = userRepository;
    }

    @GetMapping(value = "/report")
    public String getSLotInfo(Model model){
        List<Provider> providers = gamesService.getAllFrontProviders();
        model.addAttribute("providers", providers);
        model.addAttribute("waggerTypes", WaggerType.getNames());
        model.addAttribute("slotInputFront", new SlotInputFront());
        return "slot-info-db";
    }

    @PostMapping(value = "/report")
    public  @ResponseBody
    SlotInputReport perform(@ModelAttribute("slotInputFront") SlotInputFront slotInputFront/*, Principal principal*/){
        try{
            long startTimer = System.currentTimeMillis();
            /*String login = PrincipalUtils.getUsername(principal);
            UserGrants role = userRepository.getUserByLogin(login).getRole();*/
            SlotInput slotInput = Converters.convert(slotInputFront/*, role*/);
            slotInput.setGame(slotInputFront.getGame()+ " " + slotInputFront.getProvider());
            ReportPerformer reportPerformer = new ReportPerformer(slotInputFront.getProvider(), slotInputFront.getGame());
            SlotInputReport report = reportPerformer.getReport(slotInput, true);
            AuditUtils.savePerformBonus();
            long finishTimer = System.currentTimeMillis();
            report.setFullTime((finishTimer-startTimer)/1000);
            return report;
        } catch (CustomValidation e){
            SlotInputReport slotInputReport = new SlotInputReport();
            slotInputReport.setError(e.getMessage());
            return slotInputReport;
        }
    }
}