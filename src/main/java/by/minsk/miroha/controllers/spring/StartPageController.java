package by.minsk.miroha.controllers.spring;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@CrossOrigin
public class StartPageController {

    @GetMapping(value = {"/", "/index"})
    public String getStartPage (){
        return "index";
    }
}
