package by.minsk.miroha.controllers.spring;

import by.minsk.miroha.entities.enums.UserGrants;
import by.minsk.miroha.entities.front.Menu;
import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.utils.Creator;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@CrossOrigin
public class MenuProviderController {

    public static final String EMPTY_VERSION = "Что-то пошло не так... Не указана версия программы";

    @PostMapping(value = "/getMenu")
    public @ResponseBody Menu getMenu(@ModelAttribute("urlParam") String urlParam){
        if (urlParam != null){
            int startParam = urlParam.indexOf("=")+1;
            String version = urlParam.substring(startParam);
            UserGrants userGrants = UserGrants.findByName(version);
            return Creator.createMenu(userGrants);
        } else {
            throw new CustomValidation(EMPTY_VERSION);
        }
    }
}
