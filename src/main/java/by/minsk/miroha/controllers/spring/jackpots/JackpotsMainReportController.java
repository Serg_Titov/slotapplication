package by.minsk.miroha.controllers.spring.jackpots;

import by.minsk.miroha.entities.common.SlotNameRequest;
import by.minsk.miroha.entities.configs.SwaggerConfig;
import by.minsk.miroha.entities.enums.jackpots.JackpotSlot;
import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.perform.JackpotReportLoader;
import by.minsk.miroha.report.jackpot.JackpotsMainReport;
import by.minsk.miroha.utils.Converters;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.ModelAttribute;

@RestController
@RequestMapping("/get-jackpot-report")
@Api(tags = {SwaggerConfig.JACKPOTS_REPORT})
public class JackpotsMainReportController {

    private final JackpotReportLoader loader;

    public JackpotsMainReportController(JackpotReportLoader loader) {
        this.loader = loader;
    }

    @CrossOrigin
    @PostMapping
    @ApiOperation(value = "Получение отчета с общей информацией о джекпотах. Есть два слота: Mega Joker и Divine Fortune. Для каждого слота необходимо делать отдельный запрос")
    public JackpotsMainReport postMethod(@ModelAttribute("request") SlotNameRequest jackpotSlotName){
        try {
            JackpotSlot jackpotSlot = Converters.convertJackpotSlot(jackpotSlotName.getRequest());
            return loader.getReport(jackpotSlot);
        } catch (CustomValidation ex){
            return new JackpotsMainReport();
        }
    }
}
