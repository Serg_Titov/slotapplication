package by.minsk.miroha.controllers.spring.common;

import by.minsk.miroha.entities.SlotInputPrompts;
import by.minsk.miroha.entities.configs.SlotInputPromptsConfig;
import by.minsk.miroha.entities.configs.SwaggerConfig;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@CrossOrigin
@Api(tags = {SwaggerConfig.REPORT_PROMPTS})
public class SlotInputPromptsController {

    @Autowired
    private SlotInputPromptsConfig slotInputPromptsConfig;

    @GetMapping(value = "/get-prompts")
    @ApiOperation(value = "Получить подсказки для страницы расчета бонуса")
    public @ResponseBody
    SlotInputPrompts postMethod(){
        return slotInputPromptsConfig.getPrompts();
    }

}
