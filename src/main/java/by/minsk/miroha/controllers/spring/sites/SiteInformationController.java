package by.minsk.miroha.controllers.spring.sites;


import by.minsk.miroha.entities.configs.SwaggerConfig;
import by.minsk.miroha.entities.front.requests.SiteRequest;
import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.report.SiteInformation;
import by.minsk.miroha.services.jackpots.JackpotMainService;
import by.minsk.miroha.services.SitesService;
import by.minsk.miroha.services.report.SitesBonusesService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@CrossOrigin
@Api(tags = {SwaggerConfig.SITE_INFORMATION})
public class SiteInformationController {

    private final SitesService sitesService;

    private final SitesBonusesService bonusesService;

    private final JackpotMainService jackpotService;

    public SiteInformationController(SitesService sitesService, SitesBonusesService bonusesService, JackpotMainService jackpotService) {
        this.sitesService = sitesService;
        this.bonusesService = bonusesService;
        this.jackpotService = jackpotService;
    }

    @PostMapping(value = "/get-site-info")
    @ApiOperation(value = "Получение информации о сайте казино: лицензии, джекпоты, привязанные бонусы")
    public @ResponseBody
    SiteInformation getBonus(@ModelAttribute("url") SiteRequest url){
        try{
            String site = url.getUrl();
            if(site == null || site.equals("")){
                throw new CustomValidation("Incorrect Site!");
            }
            SiteInformation siteInformation = new SiteInformation();
            siteInformation.setLicenses(sitesService.getCommonSitesInformation(site));
            siteInformation.setJackpotPaths(jackpotService.getJackpotPaths(site));
            siteInformation.setBonuses(bonusesService.getSitesBonuses(siteInformation.getLicenses()));
            return siteInformation;
        } catch (CustomValidation ex){
            return new SiteInformation();
        }
    }
}
