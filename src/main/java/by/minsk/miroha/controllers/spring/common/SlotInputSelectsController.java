package by.minsk.miroha.controllers.spring.common;

import by.minsk.miroha.entities.configs.SwaggerConfig;
import by.minsk.miroha.entities.enums.WaggerType;
import by.minsk.miroha.entities.front.SlotInputSelects;
import by.minsk.miroha.services.GamesService;
import by.minsk.miroha.services.impl.GamesServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@CrossOrigin
@Api(tags = {SwaggerConfig.BONUS_SELECTS})
public class SlotInputSelectsController {

    private final GamesService gamesService = new GamesServiceImpl();

    @GetMapping(value = "/get-selects")
    @ApiOperation(value = "Получить опции для селектов провайдера и типа ваггера при множественном расчете бонусов")
    public @ResponseBody
    SlotInputSelects getSLotInputSelects(){
        SlotInputSelects inputSelects = new SlotInputSelects();
        inputSelects.setProviders(gamesService.getAllProviders());
        inputSelects.setWaggerTypes(WaggerType.getNames());
        return inputSelects;
    }
}
