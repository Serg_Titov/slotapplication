package by.minsk.miroha.controllers.spring;

import by.minsk.miroha.entities.configs.SwaggerConfig;
import by.minsk.miroha.entities.databases.SlotName;
import by.minsk.miroha.repositories.SlotInfoRepository;
import by.minsk.miroha.repositories.SlotNameRepository;
import by.minsk.miroha.repositories.impl.SlotInfoRepositoryImpl;
import by.minsk.miroha.repositories.impl.SlotNameRepositoryImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;

import static by.minsk.miroha.controllers.spring.DeleteOwnDBController.DB_CLEARED;

@Controller
@CrossOrigin
@Api(tags = {SwaggerConfig.CLEAR_LOCAL_DB})
public class DeleteHomeDBController {

    @ApiOperation(value = "Очистить локальную БД и вернуться на страницу полного расчета локальной базы данных")
    @GetMapping(value = "/DeleteHome")
    public String deleteDB(Model model){
        SlotInfoRepository infoRepository = new SlotInfoRepositoryImpl();
        infoRepository.clearTable();
        SlotNameRepository nameRepository = new SlotNameRepositoryImpl();
        nameRepository.clearTable();
        model.addAttribute("deleted", DB_CLEARED);
        model.addAttribute("slotName", new SlotName());
        model.addAttribute("deleteDisabled", "disabled");
        model.addAttribute("uploadDisabled", "visible");
        return "calc-home-db";
    }
}
