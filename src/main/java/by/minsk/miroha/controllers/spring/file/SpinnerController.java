package by.minsk.miroha.controllers.spring.file;

import by.minsk.miroha.entities.configs.SwaggerConfig;
import by.minsk.miroha.entities.front.SpinnerFront;
import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.perform.SpinnerPerformer;
import by.minsk.miroha.utils.Validator;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ModelAttribute;

@Controller
@CrossOrigin
@Api(tags = {SwaggerConfig.CALC_SPIN_LOCAL})
public class SpinnerController {

    public static final String SPIN_LIMIT = "Количество спинов";
    public static final String SIMULATIONS = "Количество симуляций";

    @GetMapping(value = "/spinner-home")
    @ApiOperation(value = "Получить страницу для расчета Вероятности сделать N спинов и остаться в плюсе на основании данных из локальной БД")
    public String getMethod(){
        return "spin-home";
    }

    @PostMapping(value = "/spinner-home")
    @ApiOperation(value = "Рассчитать Вероятность сделать N спинов и остаться в плюсе на основании данных из локальной базы данных")
    public @ResponseBody SpinnerFront postMethod(@ModelAttribute("spinnerFront") SpinnerFront spinnerFront){
        try {
            int spinLimit = Validator.convertStringToInt(spinnerFront.getSpinLimit(), SPIN_LIMIT);
            int sim = Validator.convertStringToInt(spinnerFront.getSimulations(), SIMULATIONS );
            if (spinLimit < 1){
                throw new CustomValidation("Не корректно введено количество спинов!");
            }
            if (sim < 1){
                throw new CustomValidation("Не корректно введено количество симуляций!");
            }
            SpinnerPerformer spinnerPerformer = new SpinnerPerformer();
            double spinResult = spinnerPerformer.getSpinnerResult(spinLimit, sim, false);
            spinnerFront.setResult(Double.toString(spinResult));
            return spinnerFront;
        } catch (CustomValidation e){
            spinnerFront.setResult(e.getMessage());
            return spinnerFront;
        }
    }
}
