package by.minsk.miroha.controllers.spring.common;

import by.minsk.miroha.entities.SlotInput;
import by.minsk.miroha.entities.configs.SwaggerConfig;
import by.minsk.miroha.entities.enums.UserGrants;
import by.minsk.miroha.entities.front.SlotInputFront;
import by.minsk.miroha.entities.report.ExactEV;
import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.perform.ExactEVPerformer;
import by.minsk.miroha.repositories.SlotNameRepository;
import by.minsk.miroha.utils.Converters;
import by.minsk.miroha.utils.Validator;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@CrossOrigin
@Api(tags = {SwaggerConfig.EXACT_EV})
public class ExactEVController {

    private final SlotNameRepository nameRepository;
    private static final String NEED_MORE_SIMULATIONS = "Для расчета точного EV требуется количество симуляций более 50 000 000";

    public ExactEVController(SlotNameRepository nameRepository) {
        this.nameRepository = nameRepository;
    }

    @PostMapping(value = "/exact-EV")
    @ApiOperation(value = "Рассчет точного значения EV бонуса с определенной точностью")
    public @ResponseBody
    ExactEV postMethod(@ModelAttribute("slotInputFront") SlotInputFront slotInputFront){
        try{
            SlotInput slotInput = Converters.convert(slotInputFront, UserGrants.ADMIN);
            double accuracy = Validator.convertAccuracy(slotInputFront.getAccuracy());
            ExactEVPerformer evPerformer;
            boolean external;
            if(slotInputFront.getGame() == null || slotInputFront.getGame().equals("")){
                slotInput.setGame(nameRepository.getSlotName().getFullName());
                evPerformer = new ExactEVPerformer();
                external = false;
            } else {
                slotInput.setGame(slotInputFront.getGame()+ " " + slotInputFront.getProvider());
                evPerformer = new ExactEVPerformer(slotInputFront.getProvider(), slotInputFront.getGame());
                external = true;
            }
            ExactEV ev = evPerformer.getExactEV(slotInput, external, accuracy);
            if (ev == null){
                throw new CustomValidation(NEED_MORE_SIMULATIONS);
            }
            return ev;
        } catch (CustomValidation ex){
            ExactEV evError = new ExactEV();
            evError.setError(ex.getMessage());
            return evError;
        }
    }
}
