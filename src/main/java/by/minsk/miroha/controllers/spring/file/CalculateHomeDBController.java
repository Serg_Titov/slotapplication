package by.minsk.miroha.controllers.spring.file;

import by.minsk.miroha.entities.configs.SwaggerConfig;
import by.minsk.miroha.entities.databases.SlotName;
import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.perform.SlotDataUploader;
import by.minsk.miroha.report.SlotDBReport;
import by.minsk.miroha.repositories.SlotInfoRepository;
import by.minsk.miroha.repositories.SlotNameRepository;
import by.minsk.miroha.repositories.impl.SlotInfoRepositoryImpl;
import by.minsk.miroha.repositories.impl.SlotNameRepositoryImpl;
import by.minsk.miroha.uploading.CSVUploader;
import by.minsk.miroha.utils.Validator;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import springfox.documentation.annotations.ApiIgnore;

import java.io.IOException;

@Controller
@CrossOrigin
@Api(tags = {SwaggerConfig.CALC_LOCAL_DB})
public class CalculateHomeDBController {

    public static final String NAME_NOT_NULL = "Название слота должно быть заполнено!";
    public static final String PROVIDER_NOT_NULL = "Провайдер должен быть заполнен!";
    public static final int SIZE_SLOT_NAME = 255;
    public static final int SIZE_PROVIDER = 255;

    private static final SlotNameRepository nameRepository = new SlotNameRepositoryImpl();
    private static final SlotInfoRepository infoRepository = new SlotInfoRepositoryImpl();

    @GetMapping(value = "/calc-home-db")
    @ApiOperation(value = "Получить страницу для загрузки данных в локальную БД из файла с дальнейшим автоматическим расчетом показателей")
    public String getMethod(Model model){
        if(infoRepository.countAll() ==0 ){
            model.addAttribute("deleteDisabled", "disabled");
            model.addAttribute("uploadDisabled", "visible");
        } else {
            model.addAttribute("uploadDisabled", "hidden");
        }
        model.addAttribute("slotName", new SlotName());
        return "calc-home-db";
    }

    @PostMapping(value = "/calc-home-db")
    @ApiOperation(value = "Загрузить данные в локальную БД из файла с дальнейшим автоматическим расчетом показателей")
    public String postMethod(@ModelAttribute("slotName") SlotName slotName, @ApiIgnore Model model){
        try {
            if (slotName.getSlotName() == null || "".equals(slotName.getSlotName())){
                throw new CustomValidation(NAME_NOT_NULL);
            }
            if (slotName.getProvider() == null || "".equals(slotName.getProvider())){
                throw new CustomValidation(PROVIDER_NOT_NULL);
            }
            model.addAttribute("slotName", slotName);
            SlotName verifiedSlotName = new SlotName();
            verifiedSlotName.setSlotName(Validator.validateSize(slotName.getSlotName(), SIZE_SLOT_NAME));
            verifiedSlotName.setProvider(Validator.validateSize(slotName.getProvider(), SIZE_PROVIDER));
            nameRepository.save(verifiedSlotName);
            CSVUploader.uploadSlotInfoToHSQLDB();
            String uploadedMessage = "В базу загружено " + infoRepository.countAll() + " строк.";
            try {
                SlotDataUploader dataUploader = new SlotDataUploader();
                SlotDBReport report = dataUploader.uploadSlotInformation(slotName.getSlotName(), slotName.getProvider(), false);
                model.addAttribute("report", report);
            } catch (CustomValidation e){
                model.addAttribute("incorrectData", e.getMessage());
            }
            model.addAttribute("uploadDisabled", "hidden");
            model.addAttribute("uploaded", uploadedMessage);
        } catch (CustomValidation e){
            if (slotName.getProvider() != null && !("".equals(slotName.getProvider()))) {
                model.addAttribute("slotName", slotName);
            }
            model.addAttribute("error", e.getMessage());
            model.addAttribute("uploadDisabled", "visible");
            return "calc-home-db";
        } catch (IOException e){
            model.addAttribute("error", e.getMessage());
            model.addAttribute("uploadDisabled", "visible");
            return "calc-home-db";
        }
        return "calc-home-db";
    }
}
