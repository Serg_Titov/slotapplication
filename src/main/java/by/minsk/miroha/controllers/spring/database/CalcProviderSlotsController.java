package by.minsk.miroha.controllers.spring.database;

import by.minsk.miroha.entities.configs.SwaggerConfig;
import by.minsk.miroha.entities.front.requests.ProviderRequest;
import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.perform.SlotDataUploader;
import by.minsk.miroha.services.GamesService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ModelAttribute;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

@Controller
@CrossOrigin
@Api(tags = {SwaggerConfig.CALC_PROVIDERS_SLOTS})
public class CalcProviderSlotsController {

    private final GamesService gamesService;

    public CalcProviderSlotsController(GamesService gamesService) {
        this.gamesService = gamesService;
    }

    public static final String UPLOADED = "Все слоты указанного провайдера рассчитаны!";

    @GetMapping(value = "/calc-provider")
    @ApiOperation(value = "Получить страницу для расчета всех слотов (ранее не рассчитанных) выбранного провайдера")
    public String getSLotInfo(@ApiIgnore Model model){
        List<String> providers = gamesService.getAllProviders();
        model.addAttribute("providers", providers);
        return "calc-provider";
    }

    @PostMapping(value = "/calc-provider")
    @ApiOperation(value = "Рассчитать все слоты провайдера (ранее не рассчитанные), переданного в параметрах")
    public @ResponseBody
    String postMethod(@ModelAttribute("provider") ProviderRequest provider){
        try {
            List<String> games = gamesService.getGamesByProvider(provider.getProvider());
            for (String game: games){
                SlotDataUploader dataUploader = new SlotDataUploader();
                dataUploader.uploadSlotInformation(game, provider.getProvider(), true);
            }
            return UPLOADED;
        } catch (CustomValidation e){
            return e.getMessage();
        }
    }

}
