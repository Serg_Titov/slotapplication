package by.minsk.miroha.controllers.spring.slots;

import by.minsk.miroha.entities.configs.SwaggerConfig;
import by.minsk.miroha.report.CommonCasinoReport;
import by.minsk.miroha.report.CommonCasinoReportPerformer;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@CrossOrigin
@Api(tags = {SwaggerConfig.CASINO_COMMON_REPORT})
public class CasinoCommonReportController {

    @ApiOperation(value = "Получение страницы с общей информацией о слотах. Получение таблицы слотов будет происходить отдельным контроллером")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Страница успено передана"),
            @ApiResponse(code = 403, message = "Недостаточно прав"),
            @ApiResponse(code = 404, message = "Произошла ошибка при получении страницы"),
            @ApiResponse(code = 500, message = "Произошла ошибка при получении страницы")
    })
    @GetMapping(value = "/slots")
    public @ResponseBody
    CommonCasinoReport getCasinoCommonReportPage (){
        CommonCasinoReportPerformer reportPerformer = new CommonCasinoReportPerformer();
        return reportPerformer.getReport();
    }

}
