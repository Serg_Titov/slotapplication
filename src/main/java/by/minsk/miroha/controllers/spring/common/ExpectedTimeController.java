package by.minsk.miroha.controllers.spring.common;

import by.minsk.miroha.entities.SlotInput;
import by.minsk.miroha.entities.front.SlotInputFront;
import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.repositories.SlotInfoExternalRepository;
import by.minsk.miroha.repositories.impl.SlotInfoExternalRepositoryImpl;
import by.minsk.miroha.services.SlotInfoService;
import by.minsk.miroha.services.impl.SlotInfoHomeServiceImpl;
import by.minsk.miroha.utils.Converters;
import by.minsk.miroha.utils.ExpectedTime;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import static by.minsk.miroha.perform.UnloaderFromHomeDB.BORDER_VALUE;

@Controller
@CrossOrigin
public class ExpectedTimeController {

    @PostMapping(value = "/expectedTime")
    public @ResponseBody
    Double postMethod(@ModelAttribute("slotInputFront") SlotInputFront slotInputFront){
        long spinAmount;
        SlotInput slotInput;
        try {
            slotInput = Converters.convert(slotInputFront);
            if(slotInputFront.getGame() == null || slotInputFront.getGame().equals("")){
                SlotInfoService internalService = new SlotInfoHomeServiceImpl();
                spinAmount = internalService.countAll();
            }else {
                SlotInfoExternalRepository externalRepository = new SlotInfoExternalRepositoryImpl(slotInputFront.getProvider(), slotInputFront.getGame());
                spinAmount = externalRepository.countAll();
            }
            if (spinAmount<BORDER_VALUE){
                return ExpectedTime.getExpectedTime(slotInput);
            } else {
                return 0D;
            }
        }catch (CustomValidation ex){
            return 0D;
        }

    }

}
