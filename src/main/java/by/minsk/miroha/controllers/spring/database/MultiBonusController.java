package by.minsk.miroha.controllers.spring.database;

import by.minsk.miroha.entities.configs.SwaggerConfig;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;


@Controller
@CrossOrigin
@Api(tags = {SwaggerConfig.MULTI_BONUS_PAGE})
public class MultiBonusController {

    @GetMapping(value = "/multi-report")
    @ApiOperation(value = "Получить страницу для расчета нескольких бонусов")
    public String getReport(){
        return "multi-bonus";
    }

}
