package by.minsk.miroha.controllers.spring.sites;

import by.minsk.miroha.entities.configs.SwaggerConfig;
import by.minsk.miroha.entities.front.requests.UrlRequest;
import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.report.entities.SiteList;
import by.minsk.miroha.services.SitesService;
import by.minsk.miroha.utils.CommonUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@CrossOrigin
@Api(tags = {SwaggerConfig.BRANDS_SITES_LIST})
public class GetSitesController {

    private final SitesService service;

    public GetSitesController(SitesService service) {
        this.service = service;
    }

    @PostMapping(value = "/get-sites")
    @ApiOperation(value = "Отсортированный в алфавитном порядке список сайтов, относящихся к бренду, переданному в параметрах")
    public @ResponseBody
    SiteList getJackpotsPage (@ModelAttribute("url") UrlRequest fullUrl){
        try{
            String brand = CommonUtils.getUrlParam(fullUrl.getUrl());
            return service.getSitesByBrand(brand);
        } catch (CustomValidation ex){
            return new SiteList();
        }

    }
}
