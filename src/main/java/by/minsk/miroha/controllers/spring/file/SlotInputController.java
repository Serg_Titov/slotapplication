package by.minsk.miroha.controllers.spring.file;

import by.minsk.miroha.audit.AuditUtils;
import by.minsk.miroha.entities.SlotInput;
import by.minsk.miroha.entities.front.SlotInputFront;
import by.minsk.miroha.entities.enums.WaggerType;
import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.report.ReportPerformer;
import by.minsk.miroha.report.SlotInputReport;
import by.minsk.miroha.repositories.SlotNameRepository;
import by.minsk.miroha.repositories.impl.SlotNameRepositoryImpl;
import by.minsk.miroha.utils.Converters;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@CrossOrigin
public class SlotInputController {

    public static final String DEPOSIT = "Депозит";
    public static final String BONUS = "Бонус";
    public static final String TAXES = "Налог";
    public static final String PAYMENT_FEE = "Payment fee";
    public static final String PLATFORM_FEE = "Platform fee";
    public static final String PROVIDER_FEE = "Provider fee";
    public static final String BET = "Ставка";
    public static final String MAX_PROFIT = "Максимальный выигрыш";
    public static final String WAGGER = "Оборот";
    public static final String BONUS_PARTS = "Количество частей бонуса";
    public static final String SIM = "Количество симуляций";
    public static final String NOT_NULL_SIM = "Количество симуляций должно быть больше 0";
    public static final String WAGGER_BONUS = "Бонус не может быть равен 0 при типе Wagger = B";
    private static final int ZERO = 0;

    private static final SlotNameRepository nameRepository = new SlotNameRepositoryImpl();

    @GetMapping(value = "/slot-info")
    public String getSLotInfo(Model model){
        model.addAttribute("waggerTypes", WaggerType.getNames());
        model.addAttribute("slotInputFront", new SlotInputFront());
        return "slot-info";
    }

    @PostMapping(value = "/slot-info")
    public @ResponseBody
    SlotInputReport perform(@ModelAttribute("slotInputFront") SlotInputFront slotInputFront, Model model){
        try{
            SlotInput slotInput = Converters.convert(slotInputFront);
            slotInput.setManual(false);
            slotInput.setGame(nameRepository.getSlotName().getFullName());
            ReportPerformer reportPerformer = new ReportPerformer();
            SlotInputReport report = reportPerformer.getReport(slotInput, false);
            AuditUtils.savePerformBonus();
            return report;
        } catch (CustomValidation e){
            SlotInputReport slotInputReport = new SlotInputReport();
            slotInputReport.setError(e.getMessage());
            return slotInputReport;
        }
    }


}
