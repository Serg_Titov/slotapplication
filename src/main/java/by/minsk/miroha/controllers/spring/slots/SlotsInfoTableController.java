package by.minsk.miroha.controllers.spring.slots;

import by.minsk.miroha.entities.configs.SwaggerConfig;
import by.minsk.miroha.entities.front.requests.SlotTableRequest;
import by.minsk.miroha.entities.front.responses.SlotsTableResponse;
import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.perform.CasinoReportPerformer;
import by.minsk.miroha.services.report.SlotInformationService;
import by.minsk.miroha.utils.Converters;
import by.minsk.miroha.utils.Validator;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

@Controller
@CrossOrigin
@Api(tags = {SwaggerConfig.SLOTS_TABLE})
public class SlotsInfoTableController {

    private static final int LIMIT = 20;
    private static final int FIRST_PAGE = 0;
    private static final String PAGE = "Номер страницы";

    private final SlotInformationService slotInformationService;

    public SlotsInfoTableController(SlotInformationService slotInformationService) {
        this.slotInformationService = slotInformationService;
    }

    @ApiOperation(value = "Получение таблицы рассчитанных слотов. Поля 'providerFilter' и 'slotFilter' необязательны. Значения для этих полей приходят вместе с таблицей в части 'slots'. За один раз приходит 20 слотов. Для следующей порции слотов необходимо делать отдельный запрос со  следующим значением поля 'page'. Т.е. первоначально page = 0 выгрузит первые 20 слотов. Далее page = 1 - следующие 20 слотов и т.д.")
    @PostMapping(value = "/get-slots-table")
    public @ResponseBody
    SlotsTableResponse postMethod(@ModelAttribute("request") SlotTableRequest request){
        CasinoReportPerformer performer = new CasinoReportPerformer();
        String sortType = Converters.convertSortType(request.getType());
        String sortField = Converters.convertSortField(request.getField());
        SlotsTableResponse response = new SlotsTableResponse();
        try {
            Map<String, List<String>> providers = slotInformationService.getCalculatedSlotInformation();
            response.setSlots(providers);
            int page = Validator.convertStringToIntNull(request.getPage(), PAGE);
            response.setTable(performer.getReport(LIMIT, page, sortField, sortType, request.getProviderFilter(), request.getSlotFilter()));
        } catch (CustomValidation ex){
            response.setTable(performer.getReport(LIMIT, FIRST_PAGE, sortField, sortType, null, null));
        }
        return response;
    }
}
