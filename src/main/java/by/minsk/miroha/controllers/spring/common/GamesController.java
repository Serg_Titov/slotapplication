package by.minsk.miroha.controllers.spring.common;

import by.minsk.miroha.entities.common.ProviderWithSlots;
import by.minsk.miroha.entities.configs.SwaggerConfig;
import by.minsk.miroha.services.GamesService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


@Controller
@CrossOrigin
@Api(tags = {SwaggerConfig.GAMES_LIST})
public class GamesController {

    private final GamesService gamesService;

    public GamesController(GamesService gamesService) {
        this.gamesService = gamesService;
    }

    @ApiOperation(value = "Список слотов, относящихся к провайдеру, переданному в параметрах")
    @GetMapping(value = "/get-games")
    public @ResponseBody
    List<ProviderWithSlots> postMethod(){
        return gamesService.getProvidersWithSlots();
    }
}
