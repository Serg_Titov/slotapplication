package by.minsk.miroha.controllers.spring.sites;

import by.minsk.miroha.entities.configs.SwaggerConfig;
import by.minsk.miroha.services.SitesService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

@Controller
@CrossOrigin
@Api(tags = {SwaggerConfig.BRANDS_WITH_SITES})
public class GetBrandsWithSitesController {

    private final SitesService service;

    public GetBrandsWithSitesController(SitesService service) {
        this.service = service;
    }

    @GetMapping(value = "/get-brands-sites")
    @ApiOperation(value = "Список всех брендов и соответствующих им сайтов казино передается в виде Map. Каждому бренду соответсвует список сайтов")
    public @ResponseBody
    Map<String, List<String>> getSites () {
        return service.getBrandsWithSites();
    }
}
