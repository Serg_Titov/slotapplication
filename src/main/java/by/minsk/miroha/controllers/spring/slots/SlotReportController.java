package by.minsk.miroha.controllers.spring.slots;

import by.minsk.miroha.entities.configs.SwaggerConfig;
import by.minsk.miroha.entities.front.report.SlotReportFront;
import by.minsk.miroha.perform.SlotDataUploader;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ApiParam;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@CrossOrigin
@Api(tags = {SwaggerConfig.SLOT_REPORT_PAGE})
public class SlotReportController {

    @ApiOperation(value = "Получение готовой страницы с предварительно рассчитанной информацией о слоте. Информаций выгружается на основании id полученного в параметрах URL")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Страница успено передана"),
            @ApiResponse(code = 403, message = "Недостаточно прав"),
            @ApiResponse(code = 404, message = "Произошла ошибка при получении страницы"),
            @ApiResponse(code = 500, message = "Произошла ошибка при получении страницы")
    })
    @GetMapping(value = "/slot")
    public @ResponseBody
    SlotReportFront getCasinoCommonReportPage (@ApiParam("Id слота. К примеру id = 1.") @RequestParam String id){
        int slotId;
        try {
            slotId = Integer.parseInt(id);
        } catch (Exception e){
            slotId = 0;
        }
        SlotDataUploader uploader = new SlotDataUploader();
        return uploader.getReportById(slotId);
    }
}
