package by.minsk.miroha.controllers.spring;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@CrossOrigin
public class StartPageWithModeController {

    @GetMapping(value = "/tools")
    public String getStartPage (){
        return "start-page";
    }
}
