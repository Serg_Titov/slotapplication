package by.minsk.miroha.controllers.spring.database;

import by.minsk.miroha.entities.configs.SwaggerConfig;
import by.minsk.miroha.entities.databases.SlotName;
import by.minsk.miroha.entities.front.Message;
import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.perform.SlotDataUploader;
import by.minsk.miroha.report.SlotDBReport;
import by.minsk.miroha.services.GamesService;
import by.minsk.miroha.services.impl.GamesServiceImpl;
import by.minsk.miroha.utils.Validator;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

import static by.minsk.miroha.controllers.spring.file.UploaderController.NAME_NOT_NULL;
import static by.minsk.miroha.controllers.spring.file.UploaderController.PROVIDER_NOT_NULL;
import static by.minsk.miroha.controllers.spring.file.UploaderController.SIZE_PROVIDER;
import static by.minsk.miroha.controllers.spring.file.UploaderController.SIZE_SLOT_NAME;


@Controller
@CrossOrigin
@Api(tags = {SwaggerConfig.CALC_SLOT})
public class CalculateDatabaseController {

    private final GamesService gamesService = new GamesServiceImpl();

    @GetMapping(value = "/calc-db")
    @ApiOperation(value = "Получить страницу для расчета слота")
    public String getSLotInfo(Model model){
        List<String> providers = gamesService.getAllProviders();
        model.addAttribute("providers", providers);
        return "calc-db";
    }

    @PostMapping(value = "/calc-db")
    @ApiOperation(value = "Рассчитать выбранный слот")
    public @ResponseBody
    SlotDBReport postMethod(@ModelAttribute("slotName") SlotName slotName){
        try {
            if (slotName.getSlotName() == null || "".equals(slotName.getSlotName())){
                throw new CustomValidation(NAME_NOT_NULL);
            }
            if (slotName.getProvider() == null || "".equals(slotName.getProvider())){
                throw new CustomValidation(PROVIDER_NOT_NULL);
            }
            SlotName verifiedSlotName = new SlotName();
            verifiedSlotName.setSlotName(Validator.validateSize(slotName.getSlotName(), SIZE_SLOT_NAME));
            verifiedSlotName.setProvider(Validator.validateSize(slotName.getProvider(), SIZE_PROVIDER));
            try {
                SlotDataUploader dataUploader = new SlotDataUploader();
                return dataUploader.uploadSlotInformation(slotName.getSlotName(), slotName.getProvider(), true);
            } catch (CustomValidation e){
                SlotDBReport report = new SlotDBReport();
                report.setMessage(new Message(e.getMessage()));
                return report;
            }
        } catch (CustomValidation e){
            SlotDBReport report = new SlotDBReport();
            report.setMessage(new Message(e.getMessage()));
            return report;
        }
    }
}
