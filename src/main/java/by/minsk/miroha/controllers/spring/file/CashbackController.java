package by.minsk.miroha.controllers.spring.file;

import by.minsk.miroha.entities.cashback.CashBack;
import by.minsk.miroha.entities.configs.SwaggerConfig;
import by.minsk.miroha.entities.databases.SlotName;
import by.minsk.miroha.perform.CashbackWriter;
import by.minsk.miroha.report.CashbackReport;
import by.minsk.miroha.services.SlotInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
@CrossOrigin
@Api(tags = {SwaggerConfig.CASHBACK_LOCAL})
public class CashbackController {

    public static final String CASHBACK = "информация загружена в файл";

    @GetMapping(value = "/cashback")
    @ApiOperation(value = "Рассчитать кэшбек на основании данных из локальной БД и записать результат в файл")
    public String performCashback(Model model){
        CashbackReport report = new CashbackReport();
        int simulations = 50000000;
        report.getReport(0.10, simulations);
        model.addAttribute("cashback", CASHBACK);
        return "start-page";
    }
}
