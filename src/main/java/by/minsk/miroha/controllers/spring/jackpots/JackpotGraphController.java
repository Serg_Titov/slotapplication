package by.minsk.miroha.controllers.spring.jackpots;

import by.minsk.miroha.entities.configs.SwaggerConfig;
import by.minsk.miroha.entities.enums.IncreasePeriod;
import by.minsk.miroha.entities.enums.jackpots.JackpotSlot;
import by.minsk.miroha.entities.front.requests.JackpotGraphRequest;
import by.minsk.miroha.entities.front.responses.JackpotGraphResponse;
import by.minsk.miroha.exceptions.CustomValidation;
import by.minsk.miroha.services.jackpots.JackpotMainService;
import by.minsk.miroha.utils.Converters;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.sql.Timestamp;

@Controller
@CrossOrigin
@Api(tags = {SwaggerConfig.JACKPOT_GRAPH})
public class JackpotGraphController {

    private final JackpotMainService jackpotService;

    public JackpotGraphController(JackpotMainService jackpotService) {
        this.jackpotService = jackpotService;
    }

    @PostMapping(value = "/get-jackpot-graph")
    @ApiOperation(value = "Получение данных для построения графиков роста и прироста джекпота.")
    public @ResponseBody
    JackpotGraphResponse postMethod(@ModelAttribute("request") JackpotGraphRequest request){
        try {
            JackpotSlot jackpotSlot = Converters.convertJackpotSlot(request.getSlotName());
            Timestamp dateStart = Converters.convertStringDate(request.getDateStart());
            Timestamp dateEnd = Converters.convertStringDate(request.getDateEnd());
            IncreasePeriod increasePeriod = IncreasePeriod.findByName(request.getIncreaseType());
            return jackpotService.getGraphPoints(jackpotSlot, request.getSite(), dateStart, dateEnd, increasePeriod);
        } catch (CustomValidation ex){
            JackpotGraphResponse response = new JackpotGraphResponse();
            response.setSite(request.getSite());
            return response;
        }
    }
}
