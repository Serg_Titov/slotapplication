package by.minsk.miroha;

import by.minsk.miroha.entities.configs.SwaggerConfig;
import org.apache.catalina.core.Constants;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableScheduling;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;
import java.util.stream.Stream;

@EnableSwagger2
@EnableScheduling
@Import({SwaggerConfig.class})
@SpringBootApplication
public class Runner extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(Runner.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(Runner.class);
    }

    @Bean
    public ServletContextInitializer preCompileJspsAtStartup() {
        return servletContext -> getDeepResourcePaths(servletContext, "/WEB-INF/jsp/").forEach(jspPath -> {
            ServletRegistration.Dynamic reg = servletContext.addServlet(jspPath, Constants.JSP_SERVLET_CLASS);
            reg.setInitParameter("jspFile", jspPath);
            reg.setLoadOnStartup(99);
            reg.addMapping(jspPath);
        });
    }

    private static Stream<String> getDeepResourcePaths(ServletContext servletContext, String path) {
        return (path.endsWith("/")) ? servletContext.getResourcePaths(path).stream().flatMap(p -> getDeepResourcePaths(servletContext, p))
                : Stream.of(path);
    }


}
