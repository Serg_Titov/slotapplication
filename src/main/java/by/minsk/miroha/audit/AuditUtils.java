package by.minsk.miroha.audit;

import by.minsk.miroha.entities.databases.UsersBonusAudit;
import by.minsk.miroha.services.UsersBonusAuditService;
import by.minsk.miroha.services.impl.UsersBonusAuditServiceImpl;

import java.sql.Date;

public class AuditUtils {

    private static final UsersBonusAuditService bonusAuditService = new UsersBonusAuditServiceImpl();

    public static void savePerformBonus(){
        UsersBonusAudit usersBonusAudit = bonusAuditService.getObjectByUsername("test_user");
        if(usersBonusAudit == null){
            UsersBonusAudit newUsersBonusAudit = new UsersBonusAudit();
            newUsersBonusAudit.setUsername("test_user");
            newUsersBonusAudit.setLoginDate(new Date(new java.util.Date().getTime()));
            newUsersBonusAudit.setBonusCount(1);
            bonusAuditService.save(newUsersBonusAudit);
        } else {
            bonusAuditService.update(usersBonusAudit);
        }
    }
}
