package by.minsk.miroha.entities;

import by.minsk.miroha.entities.enums.UserGrants;
import by.minsk.miroha.entities.enums.WaggerType;

public class SlotInput {

    private Long idInput;

    private double deposit;

    private double bonus;

    private boolean withoutBonus;

    private double taxes;

    private double bet;

    private double profitLimit;

    private int wagger;

    private WaggerType waggerType;

    private int bonusParts;

    private int sim;

    private double profit;

    private String game;

    private double balance;

    private boolean manual;

    private double providerFee;

    private double paymentFee;

    private double platformFee;

    private double spinLimit;

    private UserGrants userGrants;

    private boolean adminMode;

    public SlotInput() {
    }

    public Long getIdInput() {
        return idInput;
    }

    public void setIdInput(Long idInput) {
        this.idInput = idInput;
    }

    public double getDeposit() {
        return deposit;
    }

    public void setDeposit(double deposit) {
        this.deposit = deposit;
    }

    public double getBonus() {
        return bonus;
    }

    public void setBonus(double bonus) {
        this.bonus = bonus;
    }

    public boolean isWithoutBonus() {
        return withoutBonus;
    }

    public void setWithoutBonus(boolean withoutBonus) {
        this.withoutBonus = withoutBonus;
    }

    public double getBet() {
        return bet;
    }

    public void setBet(double bet) {
        this.bet = bet;
    }

    public double getProfitLimit() {
        return profitLimit;
    }

    public void setProfitLimit(double profitLimit) {
        this.profitLimit = profitLimit;
    }

    public double getProfit() {
        return profit;
    }

    public void setProfit(double profit) {
        this.profit = profit;
    }

    public int getWagger() {
        return wagger;
    }

    public void setWagger(int wagger) {
        this.wagger = wagger;
    }

    public int getBonusParts() {
        return bonusParts;
    }

    public void setBonusParts(int bonusParts) {
        this.bonusParts = bonusParts;
    }

    public int getSim() {
        return sim;
    }

    public void setSim(int sim) {
        this.sim = sim;
    }

    public String getGame() {
        return game;
    }

    public void setGame(String game) {
        this.game = game;
    }

    public void setBalance(){
        if (this.bonusParts == 0) {
            this.balance = this.deposit + this.bonus;
        } else {
            this.balance = this.deposit + this.bonus/this.bonusParts;
        }
    }

    public double getBalance() {
        return balance;
    }

    public WaggerType getWaggerType() {
        return waggerType;
    }

    public void setWaggerType(WaggerType waggerType) {
        this.waggerType = waggerType;
    }

    public boolean isManual() {
        return manual;
    }

    public void setManual(boolean manual) {
        this.manual = manual;
    }

    public double getTaxes() {
        return taxes;
    }

    public void setTaxes(double taxes) {
        this.taxes = taxes;
    }

    public double getProviderFee() {
        return providerFee;
    }

    public void setProviderFee(double providerFee) {
        this.providerFee = providerFee;
    }

    public double getPaymentFee() {
        return paymentFee;
    }

    public void setPaymentFee(double paymentFee) {
        this.paymentFee = paymentFee;
    }

    public double getPlatformFee() {
        return platformFee;
    }

    public void setPlatformFee(double platformFee) {
        this.platformFee = platformFee;
    }

    public UserGrants getUserGrants() {
        return userGrants;
    }

    public void setUserGrants(UserGrants userGrants) {
        this.userGrants = userGrants;
    }

    public double getSpinLimit() {
        return spinLimit;
    }

    public void setSpinLimit(double spinLimit) {
        this.spinLimit = spinLimit;
    }

    public boolean isAdminMode() {
        return adminMode;
    }

    public void setAdminMode(boolean adminMode) {
        this.adminMode = adminMode;
    }

    @Override
    public String toString() {
        return "SlotInput{" +
                "idInput=" + idInput +
                ", deposit=" + deposit +
                ", bonus=" + bonus +
                ", withoutBonus=" + withoutBonus +
                ", taxes=" + taxes +
                ", bet=" + bet +
                ", profitLimit=" + profitLimit +
                ", wagger=" + wagger +
                ", waggerType=" + waggerType +
                ", bonusParts=" + bonusParts +
                ", sim=" + sim +
                ", profit=" + profit +
                ", game='" + game + '\'' +
                ", balance=" + balance +
                ", manual=" + manual +
                ", providerFee=" + providerFee +
                ", paymentFee=" + paymentFee +
                ", platformFee=" + platformFee +
                ", spinLimit=" + spinLimit +
                ", userGrants=" + userGrants +
                '}';
    }
}