package by.minsk.miroha.entities.configs;

import by.minsk.miroha.entities.CasinoPrompts;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties("casino-prompts")
public class CasinoPromptsConfig {

    private String evPlayer;

    private String evCasino;

    private String roiPlayer;

    private String roiCasino;

    private String bonusVolatility;

    private String depositMultipliers;

    private String series;

    public CasinoPrompts getCasinoPrompts(){
        CasinoPrompts casinoPrompts = new CasinoPrompts();
        casinoPrompts.setEvPlayer(evPlayer);
        casinoPrompts.setEvCasino(evCasino);
        casinoPrompts.setRoiPlayer(roiPlayer);
        casinoPrompts.setRoiCasino(roiCasino);
        casinoPrompts.setBonusVolatility(bonusVolatility);
        casinoPrompts.setDepositMultipliers(depositMultipliers);
        casinoPrompts.setSeries(series);
        return casinoPrompts;
    }

    public String getEvPlayer() {
        return evPlayer;
    }

    public void setEvPlayer(String evPlayer) {
        this.evPlayer = evPlayer;
    }

    public String getEvCasino() {
        return evCasino;
    }

    public void setEvCasino(String evCasino) {
        this.evCasino = evCasino;
    }

    public String getRoiPlayer() {
        return roiPlayer;
    }

    public void setRoiPlayer(String roiPlayer) {
        this.roiPlayer = roiPlayer;
    }

    public String getRoiCasino() {
        return roiCasino;
    }

    public void setRoiCasino(String roiCasino) {
        this.roiCasino = roiCasino;
    }

    public String getBonusVolatility() {
        return bonusVolatility;
    }

    public void setBonusVolatility(String bonusVolatility) {
        this.bonusVolatility = bonusVolatility;
    }

    public String getDepositMultipliers() {
        return depositMultipliers;
    }

    public void setDepositMultipliers(String depositMultipliers) {
        this.depositMultipliers = depositMultipliers;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }
}
