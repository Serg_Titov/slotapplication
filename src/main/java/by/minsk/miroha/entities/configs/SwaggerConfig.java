package by.minsk.miroha.entities.configs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.sql.Timestamp;
import java.util.ArrayList;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    public static final String CASINO_COMMON_REPORT = "Страница с общей информацией о слотах";
    public static final String SLOT_REPORT_PAGE = "Страница с подробной информацией о слоте";
    public static final String JACKPOTS_PAGE = "Страница с общей информацией о джекпотах";
    public static final String JACKPOT_PAGE = "Страница с информацией о джекпоте конкретного сайта";
    public static final String SITE_PAGE = "Страница с информацией о сайте казино";
    public static final String COMPLETED_BONUS_PAGE = "Страница с информацией о рассчитанном ранее бонусе";
    public static final String SLOTS_TABLE = "Таблица с перечнем рассчитанных слотов";
    public static final String JACKPOTS_REPORT = "Отчет с общей информацией о джекпотах";
    public static final String JACKPOT_GRAPH = "Графики роста и прироста джекпота";
    public static final String BRANDS_PAGE = "Страница со списком всех брендов";
    public static final String GAMES_LIST = "Список слотов, относящихся к определенному провайдеру";
    public static final String BRANDS_LIST = "Список всех брендов";
    public static final String CALCULATED_BONUS = "Отчет по рассчитанному ранее бонусе";
    public static final String BRANDS_WITH_SITES = "Список всех брендов с относящихся к ним сайтами казино";
    public static final String BRANDS_SITES_LIST = "Список сайтов, относящихся к бренду";
    public static final String TO_LINK_BONUS = "Привязать бонус к сайту";
    public static final String SITE_INFORMATION = "Информация о сайте казино";
    public static final String BONUS_PROMPTS = "Подсказки для страницы рассчитанного бонуса";
    public static final String REPORT_PROMPTS = "Подсказки для страницы расчета бонуса";
    public static final String EXACT_EV = "Рассчет точного значения EV бонуса";
    public static final String BONUS_SELECTS = "Опции Select-ов для рассчета бонуса";
    public static final String PERFORM_ENABLED = "Возможность расчета бонуса (и спинов) из локальной БД";
    public static final String CALC_ALL_SLOTS = "Рассчитать все слоты";
    public static final String CALC_PROVIDERS_SLOTS = "Рассчитать все слоты провайдера";
    public static final String CALC_SLOT = "Рассчитать слот";
    public static final String CALC_SPIN = "Рассчитать спины из базы данных";
    public static final String CALC_SPIN_LOCAL = "Рассчитать спины из локальной базы данных";
    public static final String MULTI_BONUS_PAGE = "Страница расчета множественных бонусов";
    public static final String CASHBACK_DB = "Расчет кэшбека на основании данных из внешней БД";
    public static final String CLEAR_LOCAL_DB = "Очистить локальную базу данных";
    public static final String DOCUMENTATION = "Документация";
    public static final String UPLOAD_LOCAL_DB = "Загрузка данных в локальную базу данных без дальнейшего расчета";
    public static final String CALC_LOCAL_DB = "Загрузка данных в локальную БД с дальнейшим автоматическим расчетом показателей";
    public static final String CASHBACK_LOCAL = "Кэшбек из локальной БД";
    public static final String REPORT_DB = "Расчет бонуса";
    public static final String REPORT_LOCAL = "Расчет бонуса из локальной базы данных";
    public static final String EXPECTED_BONUS_TIME = "Оценка времени расчета бонуса";
    public static final String NEW_SITES_PAGE = "Страница изменений списка сайтов";

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build().apiInfo(generateApiInfo()).directModelSubstitute(Timestamp.class, Long.class)
                .tags(new Tag(CASINO_COMMON_REPORT, "Получение HTML страницы /SlotApplication/slots, которая предварительно заполнена общей информацией о слотах"))
                .tags(new Tag(SLOT_REPORT_PAGE, "Получение готовой HTML страницы /SlotApplication/slot, которая предварительно заполнена подробной информацией о рассчитанном слоте."))
                .tags(new Tag(JACKPOTS_PAGE, "Полученией HTML страницы /SlotApplication/jackpots. Данную страницу необходимо наполнять информацией, с помощью других контроллеров"))
                .tags(new Tag(COMPLETED_BONUS_PAGE, "Получение HTML страницы /SlotApplication/bonus. Данную страницу необходимо наполнять информацией, с помощью других контроллеров"))
                .tags(new Tag(BRANDS_PAGE, "Получение HTML страницы /SlotApplication/sites. Данная страница наполняется информацией с помощью других контроллеров"))
                .tags(new Tag(SITE_PAGE, "Получение HTML страницы /SlotApplication/brand. Данная страница наполняется информацией с помощью других контроллеров"))
                .tags(new Tag(JACKPOT_PAGE, "Получение HTML страницы /SlotApplication/jackpot и частичное ее наполенение информацией о выбранном джекпоте. Информация о графиках предоставляется отдельным контроллером."))
                .tags(new Tag(SLOTS_TABLE, "URL - /SlotApplication/get-slots-table. Получение/сортировка таблицы с перечнем рассчитанных слотов."))
                .tags(new Tag(JACKPOTS_REPORT, "URL - /SlotApplication/get-jackpot-report. Отчет, который заполняет информацию на странице со списком джекпотов"))
                .tags(new Tag(JACKPOT_GRAPH, "URL - /SlotApplication/get-jackpot-graph. Данные для построения графиков роста и прироста джекпота"))
                .tags(new Tag(BRANDS_LIST, "URL - /SlotApplication/get-all-brands. Список всех брендов с сайтами казино"))
                .tags(new Tag(GAMES_LIST, "URL - /SlotApplication/get-games. В ответе список всех слотов, которые относятся к провайдеру, переданному в запросе."))
                .tags(new Tag(CALCULATED_BONUS, "URL - /SlotApplication/get-bonus. Получение отчета с рассчитанном ранее бонусом"))
                .tags(new Tag(BRANDS_WITH_SITES, "URL - /SlotApplication/get-brands-sites. Получение списка брендов с соответсвующими сайтами казино"))
                .tags(new Tag(BRANDS_SITES_LIST, "URL - /SlotApplication/get-sites. Получение списка сайтов, относящихся к выбранному бренду"))
                .tags(new Tag(TO_LINK_BONUS, "URL - /SlotApplication/create-link. Привязать рассчитанный бонус к сайту казино"))
                .tags(new Tag(SITE_INFORMATION, "URL - /SlotApplication/get-sites. Получить информацию о сайте казино"))
                .tags(new Tag(BONUS_PROMPTS, "URL - /SlotApplication/get-casino-prompts. Подсказки для страницы рассчитанного бонуса в виде объекта"))
                .tags(new Tag(EXACT_EV, "URL - /SlotApplication/exact-EV. Рассчет точного значения EV бонуса с определенной точностью"))
                .tags(new Tag(BONUS_SELECTS, "URL - /SlotApplication/get-selects. Получить опции селектов при множественном расчете бонуса"))
                .tags(new Tag(REPORT_PROMPTS, "URL - /SlotApplication/get-prompts. Получить подсказки для страницы расчета бонуса"))
                .tags(new Tag(PERFORM_ENABLED, "URL - /SlotApplication/perform-enabled. Проверить возможность расчета бонуса (или спинов) из локальной базы данных"))
                .tags(new Tag(CALC_ALL_SLOTS, "URL - /SlotApplication/calc-all-slots. Страница для расчета всех ранее не рассчитанных слотов"))
                .tags(new Tag(CALC_PROVIDERS_SLOTS, "URL - /SlotApplication/calc-provider. Страница для расчета всех ранее не рассчитанных слотов провайдера"))
                .tags(new Tag(CALC_SLOT, "URL - /SlotApplication/calc-db. Страница для расчета слота"))
                .tags(new Tag(CALC_SPIN, "URL - /SlotApplication/spinner-db. Страница для расчта спинов из базы данных"))
                .tags(new Tag(MULTI_BONUS_PAGE, "URL - /SlotApplication/multi-report. Получить страницу для расчета нескольких бонусов"))
                .tags(new Tag(CASHBACK_DB, "URL - /SlotApplication/db-cashback. Рассчитать кэшбек для слота из внешней базы данных"))
                .tags(new Tag(CLEAR_LOCAL_DB, "URL - /SlotApplication/deleteHome. Очистить локальную базу данных"))
                .tags(new Tag(DOCUMENTATION, "URL - /SlotApplication/documentation. Страница с документацией"))
                .tags(new Tag(UPLOAD_LOCAL_DB, "URL - /SlotApplication/upload. Страница для загрузки данных в локальную базу данных без дальнейшего автоматического расчета"))
                .tags(new Tag(CALC_LOCAL_DB, "URL - /SlotApplication/calc-home-db. Страница для загрузки данных в локальную базу данных с дальнейшим автоматическим расчетом показателей"))
                .tags(new Tag(CALC_SPIN_LOCAL, "URL - /SlotApplication/spinner-home. Страница для расчета спинов из локальной базы данных"))
                .tags(new Tag(CASHBACK_LOCAL, "URL - /SlotApplication/cashback. Рассчитать кэшбек на основании данных из локальной БД"))
                .tags(new Tag(REPORT_DB, "URL - /SlotApplication/report. Страница расчета бонусов"))
                .tags(new Tag(REPORT_LOCAL, "URL - /SlotApplication/slot-info. Страница расчета бонусов из локальной базы данных"))
                .tags(new Tag(EXPECTED_BONUS_TIME, "URL - /SlotApplication/expectedTime. Расчет приблизительного времени выполнения бонуса"))
                .tags(new Tag(NEW_SITES_PAGE, " URL - /SlotApplication/new-sites. Страница для отображения новых сайтов и удаленных сайтов"))
                ;

    }

    private ApiInfo generateApiInfo() {
        return new ApiInfo(                "demo", "demo.", "Version 1.0", "urn:tos", ApiInfo.DEFAULT_CONTACT, "Apache 2.0",
                "http://www.apache.org/licenses/LICENSE-2.0", new ArrayList<>());
    }

    private ApiInfo apiEndPointsInfo() {
        return new ApiInfoBuilder().title("Slot Application REST API")
                .description("Slot Application REST API")
                .contact(new Contact("Titov Sergej", "www.bonusev.com", "sergey.titov@servpoint.org"))
                .license("Apache 2.0")
                .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html")
                .version("1.0.0")
                .build();
    }
}
