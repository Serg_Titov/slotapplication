package by.minsk.miroha.entities;

public class PairRandom {

    private Integer index;
    private Double coefficient;

    public PairRandom(Integer index, Double coefficient) {
        this.index = index;
        this.coefficient = coefficient;
    }

    public Integer getIndex() {
        return index;
    }

    public Double getCoefficient() {
        return coefficient;
    }

    public void setCoefficient(Double coefficient) {
        this.coefficient = coefficient;
    }
}
