package by.minsk.miroha.entities;

import by.minsk.miroha.entities.enums.jackpots.JackpotMultipleSlot;

public class JackpotMultipleValue {

    private JackpotMultipleSlot multipleSlot;

    private Double jackpotValue;

    public JackpotMultipleValue(JackpotMultipleSlot multipleSlot, Double jackpotValue) {
        this.multipleSlot = multipleSlot;
        this.jackpotValue = jackpotValue;
    }

    public JackpotMultipleSlot getMultipleSlot() {
        return multipleSlot;
    }

    public void setMultipleSlot(JackpotMultipleSlot multipleSlot) {
        this.multipleSlot = multipleSlot;
    }

    public Double getJackpotValue() {
        return jackpotValue;
    }

    public void setJackpotValue(Double jackpotValue) {
        this.jackpotValue = jackpotValue;
    }
}
