package by.minsk.miroha.entities;

public class SlotInputError {

    private String deposit;
    private String bonus;
    private String taxes;
    private String bet;
    private String waggerType;
    private String wagger;
    private String sim;
    private String game;

    public String getDeposit() {
        return deposit;
    }

    public void setDeposit(String deposit) {
        this.deposit = deposit;
    }

    public String getBonus() {
        return bonus;
    }

    public void setBonus(String bonus) {
        this.bonus = bonus;
    }

    public String getBet() {
        return bet;
    }

    public void setBet(String bet) {
        this.bet = bet;
    }

    public String getWagger() {
        return wagger;
    }

    public void setWagger(String wagger) {
        this.wagger = wagger;
    }

    public String getSim() {
        return sim;
    }

    public void setSim(String sim) {
        this.sim = sim;
    }

    public String getGame() {
        return game;
    }

    public void setGame(String game) {
        this.game = game;
    }

    public String getWaggerType() {
        return waggerType;
    }

    public void setWaggerType(String waggerType) {
        this.waggerType = waggerType;
    }

    public String getTaxes() {
        return taxes;
    }

    public void setTaxes(String taxes) {
        this.taxes = taxes;
    }
}
