package by.minsk.miroha.entities;

public class PairProfit {

    private int name;
    private double profit;
    private double frequency;

    public int getName() {
        return name;
    }

    public void setName(int name) {
        this.name = name;
    }

    public double getProfit() {
        return profit;
    }

    public void setProfit(double profit) {
        this.profit = profit;
    }

    public double getFrequency() {
        return frequency;
    }

    public void setFrequency(double frequency) {
        this.frequency = frequency;
    }
}