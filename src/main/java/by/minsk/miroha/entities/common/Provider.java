package by.minsk.miroha.entities.common;

import by.minsk.miroha.utils.CommonUtils;

public class Provider {

    private String rawProvider;

    private String cleanProvider;

    public Provider(String rawProvider) {
        this.rawProvider = rawProvider;
        this.cleanProvider = CommonUtils.formatProvider(rawProvider);
    }

    public String getRawProvider() {
        return rawProvider;
    }

    public void setRawProvider(String rawProvider) {
        this.rawProvider = rawProvider;
    }

    public String getCleanProvider() {
        return cleanProvider;
    }

    public void setCleanProvider(String cleanProvider) {
        this.cleanProvider = cleanProvider;
    }
}
