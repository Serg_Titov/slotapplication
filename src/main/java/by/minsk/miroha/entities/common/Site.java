package by.minsk.miroha.entities.common;

import by.minsk.miroha.utils.CommonUtils;

import java.util.Comparator;
import java.util.Objects;

public class Site {

    private String fullUrl;

    private String shortUrl;

    public Site(String fullUrl) {
        this.fullUrl = fullUrl;
        this.shortUrl = CommonUtils.cleanURLPath(fullUrl);
    }

    public String getFullUrl() {
        return fullUrl;
    }

    public void setFullUrl(String fullUrl) {
        this.fullUrl = fullUrl;
    }

    public String getShortUrl() {
        return shortUrl;
    }

    public void setShortUrl(String shortUrl) {
        this.shortUrl = CommonUtils.cleanURLPath(shortUrl);
    }

    public static final Comparator<Site> COMPARE_BY_SITE = Comparator.comparing(Site::getShortUrl);

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Site site = (Site) o;
        return Objects.equals(fullUrl, site.fullUrl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fullUrl);
    }

    @Override
    public String toString() {
        return "Site{" +
                "fullUrl='" + fullUrl + '\'' +
                ", shortUrl='" + shortUrl + '\'' +
                '}';
    }
}
