package by.minsk.miroha.entities.common;

import by.minsk.miroha.utils.CommonUtils;

public class Slot implements Comparable<Slot>{

    private final String rawSlot;

    private final String cleanSlot;

    public Slot(String rawSLot) {
        this.rawSlot = rawSLot;
        cleanSlot = CommonUtils.formatSlot(rawSLot);
    }

    public String getRawSlot() {
        return rawSlot;
    }

    public String getCleanSlot() {
        return cleanSlot;
    }

    @Override
    public int compareTo(Slot o) {
        return rawSlot.compareTo(o.rawSlot);
    }
}
