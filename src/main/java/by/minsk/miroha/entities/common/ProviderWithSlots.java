package by.minsk.miroha.entities.common;

import java.util.List;

public class ProviderWithSlots implements Comparable<ProviderWithSlots> {

    private Provider provider;

    private List<Slot> slots;

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public List<Slot> getSlots() {
        return slots;
    }

    public void setSlots(List<Slot> slots) {
        this.slots = slots;
    }

    @Override
    public int compareTo(ProviderWithSlots o) {
        return provider.getRawProvider().compareTo(o.getProvider().getRawProvider());
    }
}
