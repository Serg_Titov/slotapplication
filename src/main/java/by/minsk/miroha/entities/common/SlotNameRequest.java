package by.minsk.miroha.entities.common;

import io.swagger.annotations.ApiModelProperty;

public class SlotNameRequest {

    @ApiModelProperty(name = "request", value = "Название слота. Возможные варианты: megaJoker, divineFortune",
    example = "megaJoker", required = true)
    private String request;

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }
}
