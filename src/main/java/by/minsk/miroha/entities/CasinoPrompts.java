package by.minsk.miroha.entities;

public class CasinoPrompts {

    private String evPlayer;

    private String evCasino;

    private String roiPlayer;

    private String roiCasino;

    private String bonusVolatility;

    private String depositMultipliers;

    private String series;

    public String getEvPlayer() {
        return evPlayer;
    }

    public void setEvPlayer(String evPlayer) {
        this.evPlayer = evPlayer;
    }

    public String getEvCasino() {
        return evCasino;
    }

    public void setEvCasino(String evCasino) {
        this.evCasino = evCasino;
    }

    public String getRoiPlayer() {
        return roiPlayer;
    }

    public void setRoiPlayer(String roiPlayer) {
        this.roiPlayer = roiPlayer;
    }

    public String getRoiCasino() {
        return roiCasino;
    }

    public void setRoiCasino(String roiCasino) {
        this.roiCasino = roiCasino;
    }

    public String getBonusVolatility() {
        return bonusVolatility;
    }

    public void setBonusVolatility(String bonusVolatility) {
        this.bonusVolatility = bonusVolatility;
    }

    public String getDepositMultipliers() {
        return depositMultipliers;
    }

    public void setDepositMultipliers(String depositMultipliers) {
        this.depositMultipliers = depositMultipliers;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }
}
