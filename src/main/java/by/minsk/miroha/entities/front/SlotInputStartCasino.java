package by.minsk.miroha.entities.front;

public class SlotInputStartCasino {

    private String deposit;

    private String bonus;

    public String getDeposit() {
        return deposit;
    }

    public void setDeposit(String deposit) {
        this.deposit = deposit;
    }

    public String getBonus() {
        return bonus;
    }

    public void setBonus(String bonus) {
        this.bonus = bonus;
    }
}
