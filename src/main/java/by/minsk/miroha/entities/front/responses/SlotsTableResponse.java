package by.minsk.miroha.entities.front.responses;

import by.minsk.miroha.entities.front.CasinoReportFront;

import java.util.List;
import java.util.Map;

public class SlotsTableResponse {

    private List<CasinoReportFront> table;

    private Map<String, List<String>> slots;

    public List<CasinoReportFront> getTable() {
        return table;
    }

    public void setTable(List<CasinoReportFront> table) {
        this.table = table;
    }

    public Map<String, List<String>> getSlots() {
        return slots;
    }

    public void setSlots(Map<String, List<String>> slots) {
        this.slots = slots;
    }
}
