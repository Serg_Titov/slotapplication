package by.minsk.miroha.entities.front;

public class MenuItem {

    private String url;
    private String itemName;

    public MenuItem(String url, String itemName) {
        this.url = url;
        this.itemName = itemName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }
}
