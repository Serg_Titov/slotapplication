package by.minsk.miroha.entities.front.responses;

import by.minsk.miroha.report.jackpot.JackpotGraphPointFront;

import java.util.List;

public class JackpotGraphResponse {

    private String site;

    private List<SlotPoints> slotPoints;

    private List<JackpotGraphPointFront> increasePoints;

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public List<SlotPoints> getSlotPoints() {
        return slotPoints;
    }

    public void setSlotPoints(List<SlotPoints> slotPoints) {
        this.slotPoints = slotPoints;
    }

    public List<JackpotGraphPointFront> getIncreasePoints() {
        return increasePoints;
    }

    public void setIncreasePoints(List<JackpotGraphPointFront> increasePoints) {
        this.increasePoints = increasePoints;
    }
}
