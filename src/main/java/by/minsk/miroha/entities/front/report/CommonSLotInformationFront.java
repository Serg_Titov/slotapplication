package by.minsk.miroha.entities.front.report;

public class CommonSLotInformationFront {

    private Integer idCommonInfo;

    private long spinAmount;

    private double maxWin;

    private String rtp;

    private String volatility;

    private String bonusFrequency;

    private String bonusWinsRate;

    private String winsWithoutBonusRate;

    private double averageBonusWin;

    private int gameId;

    public Integer getIdCommonInfo() {
        return idCommonInfo;
    }

    public void setIdCommonInfo(Integer idCommonInfo) {
        this.idCommonInfo = idCommonInfo;
    }

    public long getSpinAmount() {
        return spinAmount;
    }

    public void setSpinAmount(long spinAmount) {
        this.spinAmount = spinAmount;
    }

    public double getMaxWin() {
        return maxWin;
    }

    public void setMaxWin(double maxWin) {
        this.maxWin = maxWin;
    }

    public String getRtp() {
        return rtp;
    }

    public void setRtp(String rtp) {
        this.rtp = rtp;
    }

    public String getVolatility() {
        return volatility;
    }

    public void setVolatility(String volatility) {
        this.volatility = volatility;
    }

    public String getBonusFrequency() {
        return bonusFrequency;
    }

    public void setBonusFrequency(String bonusFrequency) {
        this.bonusFrequency = bonusFrequency;
    }

    public String getBonusWinsRate() {
        return bonusWinsRate;
    }

    public void setBonusWinsRate(String bonusWinsRate) {
        this.bonusWinsRate = bonusWinsRate;
    }

    public String getWinsWithoutBonusRate() {
        return winsWithoutBonusRate;
    }

    public void setWinsWithoutBonusRate(String winsWithoutBonusRate) {
        this.winsWithoutBonusRate = winsWithoutBonusRate;
    }

    public double getAverageBonusWin() {
        return averageBonusWin;
    }

    public void setAverageBonusWin(double averageBonusWin) {
        this.averageBonusWin = averageBonusWin;
    }

    public int getGameId() {
        return gameId;
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }
}
