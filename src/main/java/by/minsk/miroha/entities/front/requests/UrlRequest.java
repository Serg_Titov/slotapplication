package by.minsk.miroha.entities.front.requests;

import io.swagger.annotations.ApiModelProperty;

public class UrlRequest {

    @ApiModelProperty(value = "Название бренда", example = "32Red Limited", required = true)
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
