package by.minsk.miroha.entities.front.requests;

import io.swagger.annotations.ApiModelProperty;

public class SiteRequest {

    @ApiModelProperty(value = "Название сайта", example = "32red.com", required = true)
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
