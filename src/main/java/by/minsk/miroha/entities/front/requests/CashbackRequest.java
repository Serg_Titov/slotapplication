package by.minsk.miroha.entities.front.requests;

import io.swagger.annotations.ApiModelProperty;

public class CashbackRequest {

    @ApiModelProperty(value = "Название слота", example = "buffalo_rising_megaways", required = true)
    private String slotName;

    @ApiModelProperty(value = "Название провайдера", example = "blueprint", required = true)
    private String provider;

    @ApiModelProperty(value = "Размер кэшбека", example = "0.15", required = true)
    private String cashbackValue;

    @ApiModelProperty(value = "Количество симуляций", example = "1000", required = true)
    private String simAmount;

    public String getSlotName() {
        return slotName;
    }

    public void setSlotName(String slotName) {
        this.slotName = slotName;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getCashbackValue() {
        return cashbackValue;
    }

    public void setCashbackValue(String cashbackValue) {
        this.cashbackValue = cashbackValue;
    }

    public String getSimAmount() {
        return simAmount;
    }

    public void setSimAmount(String simAmount) {
        this.simAmount = simAmount;
    }
}
