package by.minsk.miroha.entities.front.report;

public class SlotPointFront {

    private Long idSlotPoint;

    private String game;

    private String provider;

    private String spinLimit;

    private String pointResult;

    public Long getIdSlotPoint() {
        return idSlotPoint;
    }

    public void setIdSlotPoint(Long idSlotPoint) {
        this.idSlotPoint = idSlotPoint;
    }

    public String getGame() {
        return game;
    }

    public void setGame(String game) {
        this.game = game;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getSpinLimit() {
        return spinLimit;
    }

    public void setSpinLimit(String spinLimit) {
        this.spinLimit = spinLimit;
    }

    public String getPointResult() {
        return pointResult;
    }

    public void setPointResult(String pointResult) {
        this.pointResult = pointResult;
    }
}
