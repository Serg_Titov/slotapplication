package by.minsk.miroha.entities.front;


import io.swagger.annotations.ApiModelProperty;

public class SlotInputFront {

    @ApiModelProperty(value = "Депозит (не отрицательное число)", example = "100", required = true)
    private String deposit;

    @ApiModelProperty(value = "Бонус (не отрицательное число)", example = "100", required = true)
    private String bonus;

    @ApiModelProperty(value = "Расчет без учета бонуса", example = "false", required = true)
    private boolean withoutBonus;

    @ApiModelProperty(value = "Налог (не отрицательное число). Для версий игрока", example = "0")
    private String taxes;

    @ApiModelProperty(value = "Ставка (не отрицательное число)", example = "200", required = true)
    private String bet;

    @ApiModelProperty(value = "Ограничение выигрыша (не отрицательное число)", example = "0")
    private String profitLimit;

    @ApiModelProperty(value = "Тип Вагера (возможны два значения - 'D+B', 'B')", example = "B", required = true)
    private String waggerType;

    @ApiModelProperty(value = "Вагер (целое положительное число)", example = "20", required = true)
    private String wagger;

    @ApiModelProperty(value = "Количество частей бонуса (целое неотрицательное число)", example = "0")
    private String bonusParts;

    @ApiModelProperty(value = "Количество симуляций (целое положительное число)", example = "100000", required = true)
    private String sim;

    @ApiModelProperty(value = "True - будет всегда производиться расчет, false - будет происходить поиск готового расчета в БД", example = "false", required = true)
    private boolean manual;

    @ApiModelProperty(value = "Название провайдера", example = "blueprint", required = true)
    private String provider;

    @ApiModelProperty(value = "Название слота", example = "buffalo_rising_megaways", required = true)
    private String game;

    @ApiModelProperty(value = "Будет удалено", example = "ADMIN")
    private String performType;

    @ApiModelProperty(value = "Provider fee (не отрицательное число). Для версии казино", example = "10")
    private String providerFee;

    @ApiModelProperty(value = "Payment fee (не отрицательное число). Для версии казино", example = "10")
    private String paymentFee;

    @ApiModelProperty(value = "Platform fee (не отрицательное число). Для версии казино", example = "10")
    private String platformFee;

    @ApiModelProperty(value = "Ограничение выигрыша одного спина", example = "500")
    private String spinLimit;

    @ApiModelProperty(value = "Точность расчета EV в % (число от 0 до 100)", example = "90")
    private String accuracy;

    @ApiModelProperty(value = "Режим расчета бонуса. На полном наборе данных БД передавать true. Для сокращенного - false", example = "false", required = true)
    private boolean adminMode;

    public String getDeposit() {
        return deposit;
    }

    public void setDeposit(String deposit) {
        this.deposit = deposit;
    }

    public String getBonus() {
        return bonus;
    }

    public void setBonus(String bonus) {
        this.bonus = bonus;
    }

    public boolean isWithoutBonus() {
        return withoutBonus;
    }

    public void setWithoutBonus(boolean withoutBonus) {
        this.withoutBonus = withoutBonus;
    }

    public String getTaxes() {
        return taxes;
    }

    public void setTaxes(String taxes) {
        this.taxes = taxes;
    }

    public String getBet() {
        return bet;
    }

    public void setBet(String bet) {
        this.bet = bet;
    }

    public String getProfitLimit() {
        return profitLimit;
    }

    public void setProfitLimit(String profitLimit) {
        this.profitLimit = profitLimit;
    }

    public String getWaggerType() {
        return waggerType;
    }

    public void setWaggerType(String waggerType) {
        this.waggerType = waggerType;
    }

    public String getWagger() {
        return wagger;
    }

    public void setWagger(String wagger) {
        this.wagger = wagger;
    }

    public String getBonusParts() {
        return bonusParts;
    }

    public void setBonusParts(String bonusParts) {
        this.bonusParts = bonusParts;
    }

    public String getSim() {
        return sim;
    }

    public void setSim(String sim) {
        this.sim = sim;
    }

    public boolean isManual() {
        return manual;
    }

    public void setManual(boolean manual) {
        this.manual = manual;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getGame() {
        return game;
    }

    public void setGame(String game) {
        this.game = game;
    }

    public String getPerformType() {
        return performType;
    }

    public void setPerformType(String performType) {
        this.performType = performType;
    }

    public String getProviderFee() {
        return providerFee;
    }

    public void setProviderFee(String providerFee) {
        this.providerFee = providerFee;
    }

    public String getPaymentFee() {
        return paymentFee;
    }

    public void setPaymentFee(String paymentFee) {
        this.paymentFee = paymentFee;
    }

    public String getPlatformFee() {
        return platformFee;
    }

    public void setPlatformFee(String platformFee) {
        this.platformFee = platformFee;
    }

    public String getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(String accuracy) {
        this.accuracy = accuracy;
    }

    public String getSpinLimit() {
        return spinLimit;
    }

    public void setSpinLimit(String spinLimit) {
        this.spinLimit = spinLimit;
    }

    public boolean isAdminMode() {
        return adminMode;
    }

    public void setAdminMode(boolean adminMode) {
        this.adminMode = adminMode;
    }
}
