package by.minsk.miroha.entities.front;

import io.swagger.annotations.ApiModelProperty;

public class SpinnerFront {

    @ApiModelProperty(value = "Количество спинов", example = "1000", required = true)
    private String spinLimit;

    @ApiModelProperty(value = "Количество симуляций", example = "10000", required = true)
    private String simulations;

    @ApiModelProperty(value = "Не заполнять, параметр не влияет на расчет")
    private String result;

    public String getSpinLimit() {
        return spinLimit;
    }

    public void setSpinLimit(String spinLimit) {
        this.spinLimit = spinLimit;
    }

    public String getSimulations() {
        return simulations;
    }

    public void setSimulations(String simulations) {
        this.simulations = simulations;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
