package by.minsk.miroha.entities.front.requests;

import io.swagger.annotations.ApiModelProperty;

public class JackpotGraphRequest {

    @ApiModelProperty(value = "Название слота. Возможные варианты: megaJoker, divineFortune. Значение берется из URL",
            example = "divineFortune", required = true)
    private String slotName;

    @ApiModelProperty(value = "Название сайта. Значение берется из URL",
            example = "https://www.catcasino.com/", required = true)
    private String site;

    @ApiModelProperty(value = "Фильтр для графика снизу. Даты, которые ранее данной даты, в график попадать не будут. Формат даты - 'yyyy-MM-dd'",
            example = "2022-03-02")
    private String dateStart;

    @ApiModelProperty(value = "Фильтр для графика сверху. Даты, которые похже данной даты, в график попадать не будут. Формат даты - 'yyyy-MM-dd'",
            example = "2022-04-15")
    private String dateEnd;

    @ApiModelProperty(value = "Тип прироста джепота. Возможные варианты: days, weeks, months.",
            example = "weeks", required = true)
    private String increaseType;

    public String getSlotName() {
        return slotName;
    }

    public void setSlotName(String slotName) {
        this.slotName = slotName;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getDateStart() {
        return dateStart;
    }

    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getIncreaseType() {
        return increaseType;
    }

    public void setIncreaseType(String increaseType) {
        this.increaseType = increaseType;
    }
}
