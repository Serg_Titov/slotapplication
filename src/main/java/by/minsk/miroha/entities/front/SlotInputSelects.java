package by.minsk.miroha.entities.front;

import java.util.List;

public class SlotInputSelects {

    private List<String> providers;

    private List<String> waggerTypes;

    public List<String> getProviders() {
        return providers;
    }

    public void setProviders(List<String> providers) {
        this.providers = providers;
    }

    public List<String> getWaggerTypes() {
        return waggerTypes;
    }

    public void setWaggerTypes(List<String> waggerTypes) {
        this.waggerTypes = waggerTypes;
    }
}
