package by.minsk.miroha.entities.front.requests;

import io.swagger.annotations.ApiModelProperty;

import java.util.List;

public class SlotTableRequest {

    @ApiModelProperty(value = "Номер отображаемой страницы. Целые числа от 0 и далее", example = "0", required = true)
    private String page;

    @ApiModelProperty(value = "Поле, по которому происходит сортировка. Значение хранится в id названия столбца таблицы.", example = "provider", required = true)
    private String field;

    @ApiModelProperty(value = "Тип сортировки. asc - по возрастанию. desc - по убыванию. По умолчанию всегда будет asc", example = "asc", required = true)
    private String type;

    @ApiModelProperty(value = "Список названий провайдеров, по которому будет фильтроваться таблица. Данный список приходит при изначальной загрузке таблицы. Массив строк", example = "Blueprint, сore_gaming")
    private List<String> providerFilter;

    @ApiModelProperty(value = "Список названий слотов, по которому будет фильтроваться таблица. Данный список приходит при изначальной загрузке таблицы. Массив строк", example = "buffalo_rising_megaways, cobra_cash, crazy_veg")
    private List<String> slotFilter;

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getProviderFilter() {
        return providerFilter;
    }

    public void setProviderFilter(List<String> providerFilter) {
        this.providerFilter = providerFilter;
    }

    public List<String> getSlotFilter() {
        return slotFilter;
    }

    public void setSlotFilter(List<String> slotFilter) {
        this.slotFilter = slotFilter;
    }
}
