package by.minsk.miroha.entities.front.responses;

import by.minsk.miroha.report.jackpot.JackpotGraphPointFront;

import java.util.List;

public class SlotPoints {

    private String slotName;

    private List<JackpotGraphPointFront> graphPoints;

    public String getSlotName() {
        return slotName;
    }

    public void setSlotName(String slotName) {
        this.slotName = slotName;
    }

    public List<JackpotGraphPointFront> getGraphPoints() {
        return graphPoints;
    }

    public void setGraphPoints(List<JackpotGraphPointFront> graphPoints) {
        this.graphPoints = graphPoints;
    }
}
