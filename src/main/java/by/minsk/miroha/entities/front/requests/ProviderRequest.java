package by.minsk.miroha.entities.front.requests;

import io.swagger.annotations.ApiModelProperty;

public class ProviderRequest {

    @ApiModelProperty(value = "Наименование провайдера", example = "blueprint", required = true)
    private String provider;

    public ProviderRequest(String provider) {
        this.provider = provider;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }
}
