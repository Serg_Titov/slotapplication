package by.minsk.miroha.entities.front;

import by.minsk.miroha.entities.common.Slot;

public class CasinoReportFront {

    private String provider;

    private Slot slot;

    private String rtp;

    private String volatility;

    private double x100;

    private double x1000;

    private double x5000;

    private double x10000;

    private double x20000;

    private double x50000;

    private int slotId;

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public Slot getSlot() {
        return slot;
    }

    public void setSlot(Slot slot) {
        this.slot = slot;
    }

    public String getRtp() {
        return rtp;
    }

    public void setRtp(String rtp) {
        this.rtp = rtp;
    }

    public String getVolatility() {
        return volatility;
    }

    public void setVolatility(String volatility) {
        this.volatility = volatility;
    }

    public double getX100() {
        return x100;
    }

    public void setX100(double x100) {
        this.x100 = x100;
    }

    public double getX1000() {
        return x1000;
    }

    public void setX1000(double x1000) {
        this.x1000 = x1000;
    }

    public double getX5000() {
        return x5000;
    }

    public void setX5000(double x5000) {
        this.x5000 = x5000;
    }

    public double getX10000() {
        return x10000;
    }

    public void setX10000(double x10000) {
        this.x10000 = x10000;
    }

    public double getX20000() {
        return x20000;
    }

    public void setX20000(double x20000) {
        this.x20000 = x20000;
    }

    public double getX50000() {
        return x50000;
    }

    public void setX50000(double x50000) {
        this.x50000 = x50000;
    }

    public int getSlotId() {
        return slotId;
    }

    public void setSlotId(int slotId) {
        this.slotId = slotId;
    }
}
