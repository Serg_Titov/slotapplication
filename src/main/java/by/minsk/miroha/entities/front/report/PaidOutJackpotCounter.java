package by.minsk.miroha.entities.front.report;

public class PaidOutJackpotCounter {

    private String slotName;

    private int counter;

    public PaidOutJackpotCounter() {
        counter = 0;
    }

    public PaidOutJackpotCounter(String slotName, int counter) {
        this.slotName = slotName;
        this.counter = counter;
    }

    public void inc(){
        counter++;
    }

    public String getSlotName() {
        return slotName;
    }

    public void setSlotName(String slotName) {
        this.slotName = slotName;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

}
