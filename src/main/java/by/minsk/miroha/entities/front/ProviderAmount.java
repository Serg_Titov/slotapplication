package by.minsk.miroha.entities.front;

public class ProviderAmount {

    private String provider;

    private int slots;

    public ProviderAmount(String provider, int slots) {
        this.provider = provider;
        this.slots = slots;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public int getSlots() {
        return slots;
    }

    public void setSlots(int slots) {
        this.slots = slots;
    }
}
