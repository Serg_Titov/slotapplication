package by.minsk.miroha.entities.front.requests;


import io.swagger.annotations.ApiModelProperty;

public class NewSitesRequest {

    @ApiModelProperty(value = "Начало временного промежутка", example = "2022-07-01", required = true)
    private String startDate;

    @ApiModelProperty(value = "Конец временного промежутка", example = "2022-08-25", required = true)
    private String endDate;

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "NewSitesRequest{" +
                "startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                '}';
    }
}
