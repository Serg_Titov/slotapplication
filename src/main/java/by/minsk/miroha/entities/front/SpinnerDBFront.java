package by.minsk.miroha.entities.front;

import io.swagger.annotations.ApiModelProperty;

public class SpinnerDBFront {

    @ApiModelProperty(value = "Название провайдера", example = "blueprint", required = true)
    private String provider;

    @ApiModelProperty(value = "Название слота", example = "buffalo_rising_megaways", required = true)
    private String game;

    @ApiModelProperty(value = "Количество спинов", example = "1000", required = true)
    private String spinLimit;

    @ApiModelProperty(value = "Количество симуляций", example = "10000", required = true)
    private String simulations;

    @ApiModelProperty(value = "Не участвует в расчетах, не заполнять")
    private String result;

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getGame() {
        return game;
    }

    public void setGame(String game) {
        this.game = game;
    }

    public String getSpinLimit() {
        return spinLimit;
    }

    public void setSpinLimit(String spinLimit) {
        this.spinLimit = spinLimit;
    }

    public String getSimulations() {
        return simulations;
    }

    public void setSimulations(String simulations) {
        this.simulations = simulations;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
