package by.minsk.miroha.entities.front.requests;

import io.swagger.annotations.ApiModelProperty;

public class LinkBonusRequest {

    @ApiModelProperty(value = "Id рассчитанного бонуса в БД", example = "1", required = true)
    private String idBonus;

    @ApiModelProperty(value = "Название бренда, к которому принадлежит сайт", example = "Winners Malta Operations Limited", required = true)
    private String brand;

    @ApiModelProperty(value = "Название сайта", example = "winners.bet", required = true)
    private String site;

    public String getIdBonus() {
        return idBonus;
    }

    public void setIdBonus(String idBonus) {
        this.idBonus = idBonus;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }
}
