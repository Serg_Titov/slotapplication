package by.minsk.miroha.entities.front.report;

import by.minsk.miroha.entities.front.Message;
import by.minsk.miroha.entities.report.Multipliers;
import by.minsk.miroha.entities.report.SlotInformation;

import java.util.List;

public class SlotReportFront {

    private SlotInformation slotInformation;

    private CommonSLotInformationFront commonSlotInformation;

    private List<Multipliers> multipliers;

    private List<SlotPointFront> slotPoints;

    private Message message;

    public SlotInformation getSlotInformation() {
        return slotInformation;
    }

    public void setSlotInformation(SlotInformation slotInformation) {
        this.slotInformation = slotInformation;
    }

    public CommonSLotInformationFront getCommonSlotInformation() {
        return commonSlotInformation;
    }

    public void setCommonSlotInformation(CommonSLotInformationFront commonSlotInformation) {
        this.commonSlotInformation = commonSlotInformation;
    }

    public List<Multipliers> getMultipliers() {
        return multipliers;
    }

    public void setMultipliers(List<Multipliers> multipliers) {
        this.multipliers = multipliers;
    }

    public List<SlotPointFront> getSlotPoints() {
        return slotPoints;
    }

    public void setSlotPoints(List<SlotPointFront> slotPoints) {
        this.slotPoints = slotPoints;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }
}
