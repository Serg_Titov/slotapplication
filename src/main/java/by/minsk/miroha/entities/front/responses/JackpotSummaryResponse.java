package by.minsk.miroha.entities.front.responses;

import by.minsk.miroha.entities.databases.SummaryParameter;
import by.minsk.miroha.report.jackpot.JackpotParameterLicense;

import java.util.List;
import java.util.Set;

public class JackpotSummaryResponse {

    private int uniqueJackpots;

    private int paidOutAmount;

    private int counter100Rtp;

    private List<SummaryParameter> newJackpots;

    private List<SummaryParameter> lastPaidOut;

    private List<SummaryParameter> maxValuePaidOut;

    private List<JackpotParameterLicense> topJackpotsRTP;

    private Set<String> licenses;

    private String message;

    public int getUniqueJackpots() {
        return uniqueJackpots;
    }

    public void setUniqueJackpots(int uniqueJackpots) {
        this.uniqueJackpots = uniqueJackpots;
    }

    public int getPaidOutAmount() {
        return paidOutAmount;
    }

    public void setPaidOutAmount(int paidOutAmount) {
        this.paidOutAmount = paidOutAmount;
    }

    public int getCounter100Rtp() {
        return counter100Rtp;
    }

    public void setCounter100Rtp(int counter100Rtp) {
        this.counter100Rtp = counter100Rtp;
    }

    public List<SummaryParameter> getNewJackpots() {
        return newJackpots;
    }

    public void setNewJackpots(List<SummaryParameter> newJackpots) {
        this.newJackpots = newJackpots;
    }

    public List<SummaryParameter> getLastPaidOut() {
        return lastPaidOut;
    }

    public void setLastPaidOut(List<SummaryParameter> lastPaidOut) {
        this.lastPaidOut = lastPaidOut;
    }

    public List<SummaryParameter> getMaxValuePaidOut() {
        return maxValuePaidOut;
    }

    public void setMaxValuePaidOut(List<SummaryParameter> maxValuePaidOut) {
        this.maxValuePaidOut = maxValuePaidOut;
    }

    public List<JackpotParameterLicense> getTopJackpotsRTP() {
        return topJackpotsRTP;
    }

    public void setTopJackpotsRTP(List<JackpotParameterLicense> topJackpotsRTP) {
        this.topJackpotsRTP = topJackpotsRTP;
    }

    public Set<String> getLicenses() {
        return licenses;
    }

    public void setLicenses(Set<String> licenses) {
        this.licenses = licenses;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
