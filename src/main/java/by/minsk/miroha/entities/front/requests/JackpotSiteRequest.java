package by.minsk.miroha.entities.front.requests;

import io.swagger.annotations.ApiModelProperty;

public class JackpotSiteRequest {

    @ApiModelProperty(value = "Название слота. Варианты = 'divineFortune', 'megaJoker'", example = "megaJoker", required = true)
    private String slot;

    @ApiModelProperty(value = "Название сайта.", example = "https://www.paf.com/", required = true)
    private String site;

    public String getSlot() {
        return slot;
    }

    public void setSlot(String slot) {
        this.slot = slot;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }
}
