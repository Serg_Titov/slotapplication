package by.minsk.miroha.entities.front;

import java.util.List;
import java.util.Map;

public class Menu {

    private Map<String, List<MenuItem>> firstLevel;

    private String version;

    public Map<String, List<MenuItem>> getFirstLevel() {
        return firstLevel;
    }

    public void setFirstLevel(Map<String, List<MenuItem>> firstLevel) {
        this.firstLevel = firstLevel;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

}
