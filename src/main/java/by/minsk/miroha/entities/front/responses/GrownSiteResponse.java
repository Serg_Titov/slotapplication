package by.minsk.miroha.entities.front.responses;

public class GrownSiteResponse {

    private String timePeriod;

    private String grownValue;

    private String slotName;

    public String getTimePeriod() {
        return timePeriod;
    }

    public void setTimePeriod(String timePeriod) {
        this.timePeriod = timePeriod;
    }

    public String getGrownValue() {
        return grownValue;
    }

    public void setGrownValue(String grownValue) {
        this.grownValue = grownValue;
    }

    public String getSlotName() {
        return slotName;
    }

    public void setSlotName(String slotName) {
        this.slotName = slotName;
    }
}
