package by.minsk.miroha.entities.front.requests;

import io.swagger.annotations.ApiModelProperty;

public class IdAttribute {

    @ApiModelProperty(value = "Id объекта в базе данных", example = "1", required = true)
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
