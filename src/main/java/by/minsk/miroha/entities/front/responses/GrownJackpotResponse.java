package by.minsk.miroha.entities.front.responses;

import by.minsk.miroha.report.jackpot.GrownJackpots;

import java.util.List;

public class GrownJackpotResponse {

    private String grownPeriod;

    private List<GrownJackpots> grownValues;

    public String getGrownPeriod() {
        return grownPeriod;
    }

    public void setGrownPeriod(String grownPeriod) {
        this.grownPeriod = grownPeriod;
    }

    public List<GrownJackpots> getGrownValues() {
        return grownValues;
    }

    public void setGrownValues(List<GrownJackpots> grownValues) {
        this.grownValues = grownValues;
    }
}
