package by.minsk.miroha.entities.front.responses;

import by.minsk.miroha.entities.databases.SiteChanging;

import java.util.List;

public class SiteChangingReport {

    private List<SiteChanging> removedSites;

    private List<SiteChanging> newSites;

    public List<SiteChanging> getRemovedSites() {
        return removedSites;
    }

    public void setRemovedSites(List<SiteChanging> removedSites) {
        this.removedSites = removedSites;
    }

    public List<SiteChanging> getNewSites() {
        return newSites;
    }

    public void setNewSites(List<SiteChanging> newSites) {
        this.newSites = newSites;
    }

    @Override
    public String toString() {
        return "SiteChangingReport{" +
                "removedSites=" + removedSites +
                ", newSites=" + newSites +
                '}';
    }
}
