package by.minsk.miroha.entities.front;

import by.minsk.miroha.entities.common.Site;

public class JackpotPath {

    private String slotName;

    private Site jackpotSite;

    public String getSlotName() {
        return slotName;
    }

    public void setSlotName(String slotName) {
        this.slotName = slotName;
    }

    public Site getJackpotSite() {
        return jackpotSite;
    }

    public void setJackpotSite(Site jackpotSite) {
        this.jackpotSite = jackpotSite;
    }

    @Override
    public String toString() {
        return "JackpotPath{" +
                "slotName='" + slotName + '\'' +
                ", jackpotSite=" + jackpotSite +
                '}';
    }
}
