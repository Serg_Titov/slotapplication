package by.minsk.miroha.entities.enums.jackpots;

public enum JackpotStatus {

    AVAILABLE ("Available"),

    UNAVAILABLE ("Unavailable");

    private final String status;

    JackpotStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

}
