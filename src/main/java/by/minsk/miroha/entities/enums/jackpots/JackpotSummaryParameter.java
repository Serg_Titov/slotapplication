package by.minsk.miroha.entities.enums.jackpots;

public enum JackpotSummaryParameter {

    NEW ("New jackpots"),

    LAST ("Last paid out"),

    MAX ("Max paid out"),

    CURRENT("Max current");

    private final String name;

    JackpotSummaryParameter(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
