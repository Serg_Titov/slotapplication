package by.minsk.miroha.entities.enums;

public enum TimePeriods {

    DAY( "day", 1 ),

    MONTH ( "month", 31 ),

    YEAR ( "year", 366 );

    private final String name;

    private final int days;

    TimePeriods(String name, int days) {
        this.name = name;
        this.days = days;
    }

    public int getDays() {
        return days;
    }

    public String getName() {
        return name;
    }
}
