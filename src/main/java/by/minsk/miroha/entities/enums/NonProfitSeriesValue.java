package by.minsk.miroha.entities.enums;

public enum NonProfitSeriesValue {

    FIVE_TIMES ( 5, "0 - 5" ),

    TEN_TIMES ( 10, "0  - 10" ),

    TWENTY_FIVE_TIMES ( 25, "0  - 25" ),

    FIFTY_TIMES ( 50, "0  - 50" ),

    HUNDRED_TIMES ( 100, "0  - 100" ),

    TWO_HUNDREDS_FIFTY_TIMES ( 250, "0  - 250" ),

    FIVE_HUNDREDS_TIMES ( 500, "0  - 500" );

    private final int value;
    private final String name;

    NonProfitSeriesValue(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public String getName() {
        return name;
    }
}
