package by.minsk.miroha.entities.enums.jackpots;

import by.minsk.miroha.entities.JackpotMultipleValue;
import by.minsk.miroha.exceptions.CustomValidation;

import java.util.ArrayList;
import java.util.List;

public enum JackpotMultipleSlot {

    IMPERIAL_RICHES_MAJOR ("Imperial Riches Major"),

    IMPERIAL_RICHES_MEGA ("Imperial Riches Mega"),

    IMPERIAL_RICHES_MIDI ("Imperial Riches Midi"),

    MR_HOLMES_1 ("Mr Holmes 1"),

    MR_HOLMES_2 ("Mr Holmes 2"),

    MR_HOLMES_3 ("Mr Holmes 3"),

    MR_HOLMES_4 ("Mr Holmes 4"),

    MR_HOLMES_5 ("Mr Holmes 5"),

    OZWINS_J1 ("Ozwins Jackpots 1"),

    OZWINS_J2 ("Ozwins Jackpots 2"),

    OZWINS_J3 ("Ozwins Jackpots 3"),

    OZWINS_J4("Ozwins Jackpots 4"),

    OZWINS_J5("Ozwins Jackpots 5"),

    J_RAIDERS_1("Jackpot Raiders 1"),

    J_RAIDERS_2("Jackpot Raiders 2"),

    J_RAIDERS_3("Jackpot Raiders 3"),

    J_RAIDERS_4("Jackpot Raiders 4"),

    J_RAIDERS_5("Jackpot Raiders 5");

    private final String slotName;

    JackpotMultipleSlot(String slotName) {
        this.slotName = slotName;
    }

    public String getSlotName() {
        return slotName;
    }

    public static double calculateRtp(JackpotMultipleSlot jackpotSlot, List<JackpotMultipleValue> jackpotValues){
        double rtp = 0;
        switch (jackpotSlot){
            case IMPERIAL_RICHES_MAJOR:
            case IMPERIAL_RICHES_MEGA:
            case IMPERIAL_RICHES_MIDI:
                rtp = rtp + 0.9114;
                break;
            case MR_HOLMES_1:
            case MR_HOLMES_2:
            case MR_HOLMES_3:
            case MR_HOLMES_4:
            case MR_HOLMES_5:
                rtp = rtp + 0.934;
                break;
            case OZWINS_J1:
            case OZWINS_J2:
            case OZWINS_J3:
            case OZWINS_J4:
            case OZWINS_J5:
                rtp = rtp + 0.933;
                break;
            case J_RAIDERS_1:
            case J_RAIDERS_2:
            case J_RAIDERS_3:
            case J_RAIDERS_4:
            case J_RAIDERS_5:
                rtp = rtp + 0.925;
                break;
            default: rtp = 0;
        }
        for(JackpotMultipleValue jackpotValue : jackpotValues){
            switch (jackpotValue.getMultipleSlot()){
                case IMPERIAL_RICHES_MIDI:
                    rtp = rtp + (jackpotValue.getJackpotValue() - 500) * 0.00000285714285714286;
                    break;
                case IMPERIAL_RICHES_MAJOR:
                    rtp = rtp + (jackpotValue.getJackpotValue() - 1000) * 0.0000012;
                    break;
                case IMPERIAL_RICHES_MEGA:
                    rtp = rtp + (jackpotValue.getJackpotValue() - 4500) * 0.000000397905759162304;
                    break;
                case MR_HOLMES_1:
                    rtp = rtp + (jackpotValue.getJackpotValue() - 40) * 0.000102941176470588;
                    break;
                case MR_HOLMES_2:
                    rtp = rtp + (jackpotValue.getJackpotValue() - 200) * 0.0000233333333333333;
                    break;
                case MR_HOLMES_3:
                    rtp = rtp + (jackpotValue.getJackpotValue() - 600) * 0.000006;
                    break;
                case MR_HOLMES_4:
                    rtp = rtp + (jackpotValue.getJackpotValue() - 1500) * 0.0000024;
                    break;
                case MR_HOLMES_5:
                    rtp = rtp + (jackpotValue.getJackpotValue() - 10000) * 0.0000004;
                    break;
                case OZWINS_J1:
                    rtp = rtp + (jackpotValue.getJackpotValue() - 40) * 0.000102040816326531;
                    break;
                case OZWINS_J2:
                    rtp = rtp + (jackpotValue.getJackpotValue() - 200) * 0.0000233644859813084;
                    break;
                case OZWINS_J3:
                    rtp = rtp + (jackpotValue.getJackpotValue() - 600) * 0.00000600240096038415;
                    break;
                case OZWINS_J4:
                    rtp = rtp + (jackpotValue.getJackpotValue() - 1000) * 0.00000357142857142857;
                    break;
                case OZWINS_J5:
                    rtp = rtp + (jackpotValue.getJackpotValue() - 2500) * 0.00000160919540229885;
                    break;
                case J_RAIDERS_1:
                    rtp = rtp + (jackpotValue.getJackpotValue() - 40) * 0.000107692307692308;
                    break;
                case J_RAIDERS_2:
                    rtp = rtp + (jackpotValue.getJackpotValue() - 200) * 0.0000233333333333333;
                    break;
                case J_RAIDERS_3:
                    rtp = rtp + (jackpotValue.getJackpotValue() - 600) * 0.000006;
                    break;
                case J_RAIDERS_4:
                    rtp = rtp + (jackpotValue.getJackpotValue() - 1500) * 0.0000024;
                    break;
                case J_RAIDERS_5:
                    rtp = rtp + (jackpotValue.getJackpotValue() - 15000) * 0.000000141176470588235;
                    break;
                default: throw new CustomValidation("Такого слота не существует");
            }
        }
        return rtp * 100;
    }

    public static List<JackpotMultipleSlot> getListSlots(JackpotSlot slot){
        List<JackpotMultipleSlot> slots = new ArrayList<>();
        switch (slot){
            case IMPERIAL_RICHES:
                slots.add(JackpotMultipleSlot.IMPERIAL_RICHES_MAJOR);
                slots.add(JackpotMultipleSlot.IMPERIAL_RICHES_MIDI);
                slots.add(JackpotMultipleSlot.IMPERIAL_RICHES_MEGA);
                break;
            case MR_HOLMES:
                slots.add(JackpotMultipleSlot.MR_HOLMES_1);
                slots.add(JackpotMultipleSlot.MR_HOLMES_2);
                slots.add(JackpotMultipleSlot.MR_HOLMES_3);
                slots.add(JackpotMultipleSlot.MR_HOLMES_4);
                slots.add(JackpotMultipleSlot.MR_HOLMES_5);
                break;
            case OZWINS:
                slots.add(JackpotMultipleSlot.OZWINS_J1);
                slots.add(JackpotMultipleSlot.OZWINS_J2);
                slots.add(JackpotMultipleSlot.OZWINS_J3);
                slots.add(JackpotMultipleSlot.OZWINS_J4);
                slots.add(JackpotMultipleSlot.OZWINS_J5);
                break;
            case J_RAIDERS:
                slots.add(JackpotMultipleSlot.J_RAIDERS_1);
                slots.add(JackpotMultipleSlot.J_RAIDERS_2);
                slots.add(JackpotMultipleSlot.J_RAIDERS_3);
                slots.add(JackpotMultipleSlot.J_RAIDERS_4);
                slots.add(JackpotMultipleSlot.J_RAIDERS_5);
                break;
        }
        return slots;
    }

    public static JackpotMultipleSlot findByName(String slotName){
        for(JackpotMultipleSlot slot : JackpotMultipleSlot.values()){
            if(slot.getSlotName().equals(slotName)){
                return slot;
            }
        }
        throw new CustomValidation("Такого слота нет!");
    }
}
