package by.minsk.miroha.entities.enums;

public enum MultiplierDeposit {

    MULTIPLIER_ONE (1),

    MULTIPLIER_TWO (2),

    MULTIPLIER_FIVE (5),

    MULTIPLIER_TEN (10),

    MULTIPLIER_TWENTY (20),

    MULTIPLIER_FIFTY (50),

    MULTIPLIER_HUNDRED (100);

    private final int value;

    MultiplierDeposit(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
