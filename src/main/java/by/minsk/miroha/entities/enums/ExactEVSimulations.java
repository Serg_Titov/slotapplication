package by.minsk.miroha.entities.enums;

public enum ExactEVSimulations {

    FIRST_LEVEL (100000),

    SECOND_LEVEL (250000),

    THIRD_LEVEL (500000),

    FOURTH_LEVEL (1000000),

    FIFTH_LEVEL (2500000),

    SIXTH_LEVEL (5000000),

    SEVENTH_LEVEL (10000000),

    EIGHTH_LEVEL (20000000),

    NINTH_LEVEL (40000000);

    private final int simulations;

    ExactEVSimulations(int simulations) {
        this.simulations = simulations;
    }

    public int getSimulations() {
        return simulations;
    }
}
