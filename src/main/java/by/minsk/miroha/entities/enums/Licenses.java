package by.minsk.miroha.entities.enums;

public enum Licenses {

    UK( "uk", "UK"),

    MGA( "mga", "Malta"),

    SE( "se", "Sweden"),

    DK( "dk", "Denmark"),

    ES( "es", "Spain"),

    EE( "ee", "Estonia"),

    NJ("nj", "New Jersey"),

    ALD( "ald", "Alderney"),

    CUR("cur", "Curacao"),

    GR("gr", "Greece"),

    MAN("im", "Isle of Man");

    private final String shortName;

    private final String fullName;

    Licenses(String shortName, String fullName) {
        this.shortName = shortName;
        this.fullName = fullName;
    }

    public String getShortName() {
        return shortName;
    }

    public String getFullName() {
        return fullName;
    }

    public static String convertLicense(String dbLicense){
        for (Licenses license : Licenses.values()){
            if (license.getShortName().equals(dbLicense)){
                return license.getFullName();
            }
        }
        return "Other license";
    }
}
