package by.minsk.miroha.entities.enums;

import by.minsk.miroha.exceptions.CustomValidation;

public enum IncreasePeriod {

    DAY( "days", 1 ),

    WEEK("weeks", 7),

    MONTH ( "months", 30 );

    private final String name;

    private final int days;

    IncreasePeriod(String name, int days) {
        this.name = name;
        this.days = days;
    }

    public static IncreasePeriod findByName(String periodName){
        for (IncreasePeriod increasePeriod : IncreasePeriod.values()){
            if (increasePeriod.getName().equals(periodName)){
                return increasePeriod;
            }
        }
        throw new CustomValidation("Такого периода прироста нет!");
    }

    public int getDays() {
        return days;
    }

    public String getName() {
        return name;
    }
}
