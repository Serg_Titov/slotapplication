package by.minsk.miroha.entities.enums;

import by.minsk.miroha.exceptions.CustomValidation;

import java.util.ArrayList;
import java.util.List;

public enum WaggerType {

    DB ("D+B"),
    B ("B");

    private final String waggerName;

    WaggerType(String waggerName) {
        this.waggerName = waggerName;
    }

    public String getWaggerName() {
        return waggerName;
    }

    public static WaggerType findByName(String waggerName){
        for (WaggerType waggerType : WaggerType.values()){
            if (waggerType.getWaggerName().equals(waggerName)){
                return waggerType;
            }
        }
        throw new CustomValidation("Такого типа Wagger нет!");
    }

    public double getBalanceForWagger(double deposit, double bonus){
        if (this.equals(WaggerType.DB)){
            return deposit + bonus;
        }
        else{
            return bonus;
        }
    }

    public static List<String> getNames(){
        List<String> waggerTypes = new ArrayList<>();
        for(WaggerType waggerType: WaggerType.values()){
            waggerTypes.add(waggerType.getWaggerName());
        }
        return waggerTypes;
    }
}
