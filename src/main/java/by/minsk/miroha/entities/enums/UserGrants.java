package by.minsk.miroha.entities.enums;

public enum UserGrants {

    ADMIN ("admin"),

    LITE("lite"),

    PRO ("pro"),

    DEMO("demo"),

    CASINO("casino");

    private final String grants;

    UserGrants(String grants) {
        this.grants = grants;
    }

    public String getGrants() {
        return grants;
    }

    public static UserGrants findByName(String userGrants){
        for (UserGrants userGrant : UserGrants.values()){
            if (userGrant.getGrants().equals(userGrants)){
                return userGrant;
            }
        }
        return null;
    }
}
