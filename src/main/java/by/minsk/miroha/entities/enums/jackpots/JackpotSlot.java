package by.minsk.miroha.entities.enums.jackpots;

import by.minsk.miroha.exceptions.CustomValidation;

public enum JackpotSlot {

    MEGA_JOKER("Mega joker", "Mega joker"),

    DIVINE_FORTUNE ("Divine fortune", "Divine fortune"),

    MERCY_OF_THE_GODS ("Mercy of the Gods", "Mercy of the Gods"),

    VEGAS_NIGHT_LIVE ( "Vegas Night Life", "Vegas Night Life"),

    GRAND_SPINN_SUPERPOT ( "Grand Spinn Superpot", "Grand Spinn Superpot"),

    IMPERIAL_RICHES ("Imperial Riches Mega", "Imperial Riches"),

    MR_HOLMES ("Mr Holmes 1", "Mr Holmes"),

    OZWINS ("Ozwins Jackpots 1", "Ozwins Jackpots"),

    J_RAIDERS("Jackpot Raiders 1", "Jackpot Raiders");

    private final String slotName;

    private final String frontName;

    JackpotSlot(String slotName, String frontName) {
        this.slotName = slotName;
        this.frontName = frontName;
    }

    public String getSlotName() {
        return slotName;
    }

    public String getFrontName() {
        return frontName;
    }

    public static double calculateRtp(JackpotSlot slot, double jackpot){
        switch (slot){
            case MEGA_JOKER : return (0.96 + (jackpot - 2000)*1/500000)*100;
            case DIVINE_FORTUNE : return (0.9289 + (jackpot - 10000)*1/2685400)*100;
            case MERCY_OF_THE_GODS : return (0.9294 + (jackpot - 10000) * 0.000000337850177143066)*100;
            case VEGAS_NIGHT_LIVE : return (0.9293 + (jackpot - 1000) * 0.00000371784565916399)*100;
            case GRAND_SPINN_SUPERPOT : return (0.9351 + (jackpot - 5000) * 0.000000595895735019733)*100;
            default: throw new CustomValidation("Такого слота не существует");
        }
    }

    public static JackpotMultipleSlot getMultipleSlot(JackpotSlot slot){
        switch (slot){
            case IMPERIAL_RICHES: return JackpotMultipleSlot.IMPERIAL_RICHES_MEGA;
            case MR_HOLMES : return JackpotMultipleSlot.MR_HOLMES_1;
            case OZWINS : return JackpotMultipleSlot.OZWINS_J1;
            case J_RAIDERS : return JackpotMultipleSlot.J_RAIDERS_1;
            default : return null;
        }
    }

    public static JackpotSlot getMultipleSlot(String slotName){
        switch (slotName){
            case "Imperial Riches Major":
            case "Imperial Riches Mega":
            case "Imperial Riches Midi":
                return JackpotSlot.IMPERIAL_RICHES;
            case "Mr Holmes 1":
            case "Mr Holmes 2":
            case "Mr Holmes 3":
            case "Mr Holmes 4":
            case "Mr Holmes 5":
                return JackpotSlot.MR_HOLMES;
            case "Ozwins Jackpots 1":
            case "Ozwins Jackpots 2":
            case "Ozwins Jackpots 3":
            case "Ozwins Jackpots 4":
            case "Ozwins Jackpots 5":
                return JackpotSlot.OZWINS;
            case "Jackpot Raiders 1":
            case "Jackpot Raiders 2":
            case "Jackpot Raiders 3":
            case "Jackpot Raiders 4":
            case "Jackpot Raiders 5":
                return J_RAIDERS;
            default:
                throw new CustomValidation("Такого слота не существует");
        }
    }

    public static JackpotSlot findByName(String slotName){
        for (JackpotSlot jackpotSlot : JackpotSlot.values()){
            if (jackpotSlot.getSlotName().equals(slotName)){
                return jackpotSlot;
            }
        }
        throw new CustomValidation("Такого слота не существует");
    }

}
