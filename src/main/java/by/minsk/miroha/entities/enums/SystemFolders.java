package by.minsk.miroha.entities.enums;

public enum SystemFolders {

    SYS ("sys"),

    MYSQL ("mysql"),

    PERFORMANCE_SCHEMA ("performance_schema"),

    JACKPOT_STATISTIC ("jackpot_statistic"),

    JACKPOTS ("JackPots"),

    SLOTS ("Slots"),

    SLOT_STATISTIC ("slot_statistic"),

    CR_DEBUG ("cr_debug"),

    TEST_STATISTICS ("test_statistic"),

    TEST("test");

    private final String tableName;

    SystemFolders(String tableName) {
        this.tableName = tableName;
    }

    public String getTableName(){
        return tableName;
    }

    public static SystemFolders findByName(String tableName){
        for (SystemFolders systemFolder : SystemFolders.values()){
            if (systemFolder.getTableName().equals(tableName)){
                return systemFolder;
            }
        }
        return null;
    }
}
