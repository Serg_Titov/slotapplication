package by.minsk.miroha.entities;

public class SlotInputPrompts {

    private String providerPrompt;

    private String gamesPrompt;

    private String depositPrompt;

    private String bonusPrompt;

    private String providerFeePrompt;

    private String paymentFeePrompt;

    private String platformFeePrompt;

    private String betPrompt;

    private String waggerTypePrompt;

    private String waggerPrompt;

    private String simulationsPrompt;

    private String withoutBonusPrompt;

    private String manualPrompt;

    private String taxesPrompt;

    private String maxProfitPrompt;

    private String bonusPartPrompt;

    private String accuracyPrompt;

    public String getProviderPrompt() {
        return providerPrompt;
    }

    public void setProviderPrompt(String providerPrompt) {
        this.providerPrompt = providerPrompt;
    }

    public String getGamesPrompt() {
        return gamesPrompt;
    }

    public void setGamesPrompt(String gamesPrompt) {
        this.gamesPrompt = gamesPrompt;
    }

    public String getDepositPrompt() {
        return depositPrompt;
    }

    public void setDepositPrompt(String depositPrompt) {
        this.depositPrompt = depositPrompt;
    }

    public String getBonusPrompt() {
        return bonusPrompt;
    }

    public void setBonusPrompt(String bonusPrompt) {
        this.bonusPrompt = bonusPrompt;
    }

    public String getProviderFeePrompt() {
        return providerFeePrompt;
    }

    public void setProviderFeePrompt(String providerFeePrompt) {
        this.providerFeePrompt = providerFeePrompt;
    }

    public String getPaymentFeePrompt() {
        return paymentFeePrompt;
    }

    public void setPaymentFeePrompt(String paymentFeePrompt) {
        this.paymentFeePrompt = paymentFeePrompt;
    }

    public String getPlatformFeePrompt() {
        return platformFeePrompt;
    }

    public void setPlatformFeePrompt(String platformFeePrompt) {
        this.platformFeePrompt = platformFeePrompt;
    }

    public String getBetPrompt() {
        return betPrompt;
    }

    public void setBetPrompt(String betPrompt) {
        this.betPrompt = betPrompt;
    }

    public String getWaggerTypePrompt() {
        return waggerTypePrompt;
    }

    public void setWaggerTypePrompt(String waggerTypePrompt) {
        this.waggerTypePrompt = waggerTypePrompt;
    }

    public String getWaggerPrompt() {
        return waggerPrompt;
    }

    public void setWaggerPrompt(String waggerPrompt) {
        this.waggerPrompt = waggerPrompt;
    }

    public String getSimulationsPrompt() {
        return simulationsPrompt;
    }

    public void setSimulationsPrompt(String simulationsPrompt) {
        this.simulationsPrompt = simulationsPrompt;
    }

    public String getWithoutBonusPrompt() {
        return withoutBonusPrompt;
    }

    public void setWithoutBonusPrompt(String withoutBonusPrompt) {
        this.withoutBonusPrompt = withoutBonusPrompt;
    }

    public String getManualPrompt() {
        return manualPrompt;
    }

    public void setManualPrompt(String manualPrompt) {
        this.manualPrompt = manualPrompt;
    }

    public String getTaxesPrompt() {
        return taxesPrompt;
    }

    public void setTaxesPrompt(String taxesPrompt) {
        this.taxesPrompt = taxesPrompt;
    }

    public String getMaxProfitPrompt() {
        return maxProfitPrompt;
    }

    public void setMaxProfitPrompt(String maxProfitPrompt) {
        this.maxProfitPrompt = maxProfitPrompt;
    }

    public String getBonusPartPrompt() {
        return bonusPartPrompt;
    }

    public void setBonusPartPrompt(String bonusPartPrompt) {
        this.bonusPartPrompt = bonusPartPrompt;
    }

    public String getAccuracyPrompt() {
        return accuracyPrompt;
    }

    public void setAccuracyPrompt(String accuracyPrompt) {
        this.accuracyPrompt = accuracyPrompt;
    }
}
