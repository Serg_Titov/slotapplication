package by.minsk.miroha.entities.report;

import by.minsk.miroha.entities.common.Provider;
import by.minsk.miroha.entities.common.Slot;

public class SlotInformation {

    private int idInfo;

    private Slot slot;

    private Provider provider;

    public SlotInformation() {
    }

    public SlotInformation(String slot, String provider) {
        this.slot = new Slot(slot);
        this.provider = new Provider(provider);
    }

    public int getIdInfo() {
        return idInfo;
    }

    public void setIdInfo(int idInfo) {
        this.idInfo = idInfo;
    }

    public Slot getSlot() {
        return slot;
    }

    public void setSlot(String slot) {
        this.slot = new Slot(slot);
    }

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = new Provider(provider);
    }

    @Override
    public String toString() {
        return "SlotInformation{" +
                "idInfo=" + idInfo +
                ", slot='" + slot + '\'' +
                ", provider='" + provider + '\'' +
                '}';
    }
}
