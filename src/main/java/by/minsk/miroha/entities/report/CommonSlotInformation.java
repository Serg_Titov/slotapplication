package by.minsk.miroha.entities.report;

public class CommonSlotInformation {

    private Integer idCommonInfo;

    private long spinAmount;

    private double maxWin;

    private double rtp;

    private double volatility;

    private double bonusFrequency;

    private double bonusWinsRate;

    private double winsWithoutBonusRate;

    private double averageBonusWin;

    private int gameId;

    public Integer getIdCommonInfo() {
        return idCommonInfo;
    }

    public void setIdCommonInfo(Integer idCommonInfo) {
        this.idCommonInfo = idCommonInfo;
    }

    public long getSpinAmount() {
        return spinAmount;
    }

    public void setSpinAmount(long spinAmount) {
        this.spinAmount = spinAmount;
    }

    public double getMaxWin() {
        return maxWin;
    }

    public void setMaxWin(double maxWin) {
        this.maxWin = maxWin;
    }

    public double getRtp() {
        return rtp;
    }

    public void setRtp(double rtp) {
        this.rtp = rtp;
    }

    public double getVolatility() {
        return volatility;
    }

    public void setVolatility(double volatility) {
        this.volatility = volatility;
    }

    public double getBonusFrequency() {
        return bonusFrequency;
    }

    public void setBonusFrequency(double bonusFrequency) {
        this.bonusFrequency = bonusFrequency;
    }

    public double getBonusWinsRate() {
        return bonusWinsRate;
    }

    public void setBonusWinsRate(double bonusWinsRate) {
        this.bonusWinsRate = bonusWinsRate;
    }

    public double getWinsWithoutBonusRate() {
        return winsWithoutBonusRate;
    }

    public void setWinsWithoutBonusRate(double winsWithoutBonusRate) {
        this.winsWithoutBonusRate = winsWithoutBonusRate;
    }

    public double getAverageBonusWin() {
        return averageBonusWin;
    }

    public void setAverageBonusWin(double avarageBonusWin) {
        this.averageBonusWin = avarageBonusWin;
    }

    public int getGameId() {
        return gameId;
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }

    @Override
    public String toString() {
        return "CommonSlotInformation{" +
                "idCommonInfo=" + idCommonInfo +
                ", spinAmount=" + spinAmount +
                ", maxWin=" + maxWin +
                ", rtp=" + rtp +
                ", volatility=" + volatility +
                ", bonusFrequency=" + bonusFrequency +
                ", bonusWinsRate=" + bonusWinsRate +
                ", winsWithoutBonusRate=" + winsWithoutBonusRate +
                ", avarageBonusWin=" + averageBonusWin +
                ", gameId=" + gameId +
                '}';
    }
}