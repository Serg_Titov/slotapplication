package by.minsk.miroha.entities.report;

public class SitesBonuses {

    private long sitesBonusesId;

    private long sitesId;

    private long bonusId;

    public long getSitesBonusesId() {
        return sitesBonusesId;
    }

    public void setSitesBonusesId(long sitesBonusesId) {
        this.sitesBonusesId = sitesBonusesId;
    }

    public long getSitesId() {
        return sitesId;
    }

    public void setSitesId(long sitesId) {
        this.sitesId = sitesId;
    }

    public long getBonusId() {
        return bonusId;
    }

    public void setBonusId(long bonusId) {
        this.bonusId = bonusId;
    }
}
