package by.minsk.miroha.entities.report;

import by.minsk.miroha.entities.SlotInput;

public class DepositMultiplier {

    private long depositMultiplierId;
    private int depositMultiplier;
    private int depositMultiplierFrequency;
    private SlotInput slotInput;

    public DepositMultiplier() {
    }

    public int getDepositMultiplier() {
        return depositMultiplier;
    }

    public void setDepositMultiplier(int depositMultiplier) {
        this.depositMultiplier = depositMultiplier;
    }

    public int getDepositMultiplierFrequency() {
        return depositMultiplierFrequency;
    }

    public void setDepositMultiplierFrequency(int depositMultiplierFrequency) {
        this.depositMultiplierFrequency = depositMultiplierFrequency;
    }

    public long getDepositMultiplierId() {
        return depositMultiplierId;
    }

    public void setDepositMultiplierId(long depositMultiplierId) {
        this.depositMultiplierId = depositMultiplierId;
    }

    public SlotInput getSlotInput() {
        return slotInput;
    }

    public void setSlotInput(SlotInput slotInput) {
        this.slotInput = slotInput;
    }

    @Override
    public String toString() {
        return "DepositMultiplier{" +
                "depositMultiplierId=" + depositMultiplierId +
                ", depositMultiplier=" + depositMultiplier +
                ", depositMultiplierFrequency=" + depositMultiplierFrequency +
                ", slotInput=" + slotInput +
                '}';
    }
}
