package by.minsk.miroha.entities.report;

public class ExactEV {

    private double ev;

    private double simulations;

    private double accuracy;

    private String error;

    public ExactEV(double ev, double simulations) {
        this.ev = ev;
        this.simulations = simulations;
    }

    public ExactEV() {
    }

    public double getEv() {
        return ev;
    }

    public void setEv(double ev) {
        this.ev = ev;
    }

    public double getSimulations() {
        return simulations;
    }

    public void setSimulations(double simulations) {
        this.simulations = simulations;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public double getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(double accuracy) {
        this.accuracy = accuracy;
    }
}
