package by.minsk.miroha.entities.report;

import java.util.List;

public class BrandWithLicenses implements Comparable<BrandWithLicenses> {

    private String brand;

    private List<String> licenses;

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public List<String> getLicenses() {
        return licenses;
    }

    public void setLicenses(List<String> licenses) {
        this.licenses = licenses;
    }

    @Override
    public int compareTo(BrandWithLicenses o) {
        return this.brand.compareTo(o.getBrand());
    }

    @Override
    public String toString() {
        return "BrandWithLicenses{" +
                "brand='" + brand + '\'' +
                ", licenses=" + licenses +
                '}';
    }
}
