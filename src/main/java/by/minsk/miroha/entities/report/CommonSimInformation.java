package by.minsk.miroha.entities.report;

import by.minsk.miroha.entities.SlotInput;

public class CommonSimInformation {

    private Integer idCommonSim;

    private double profit;

    private double ev;

    private double casinoEv;

    private double roi;

    private double casinoRoi;

    private double bonusVolatility;

    private double profitProbability;

    private double nonProfitProbability;

    private int longestProfitSeries;

    private double longestProfitFrequency;

    private int longestNonProfitSeries;

    private double longestNonProfitFrequency;

    private double averageWin;

    private long betCounter;

    private SlotInput slotInput;

    public Integer getIdCommonSim() {
        return idCommonSim;
    }

    public void setIdCommonSim(Integer idCommonSim) {
        this.idCommonSim = idCommonSim;
    }

    public double getProfit() {
        return profit;
    }

    public void setProfit(double profit) {
        this.profit = profit;
    }

    public double getEv() {
        return ev;
    }

    public void setEv(double ev) {
        this.ev = ev;
    }

    public double getRoi() {
        return roi;
    }

    public void setRoi(double roi) {
        this.roi = roi;
    }

    public double getBonusVolatility() {
        return bonusVolatility;
    }

    public void setBonusVolatility(double bonusVolatility) {
        this.bonusVolatility = bonusVolatility;
    }

    public int getLongestProfitSeries() {
        return longestProfitSeries;
    }

    public void setLongestProfitSeries(int longestProfitSeries) {
        this.longestProfitSeries = longestProfitSeries;
    }

    public double getLongestProfitFrequency() {
        return longestProfitFrequency;
    }

    public void setLongestProfitFrequency(double longestProfitFrequency) {
        this.longestProfitFrequency = longestProfitFrequency;
    }

    public int getLongestNonProfitSeries() {
        return longestNonProfitSeries;
    }

    public void setLongestNonProfitSeries(int longestNonProfitSeries) {
        this.longestNonProfitSeries = longestNonProfitSeries;
    }

    public double getLongestNonProfitFrequency() {
        return longestNonProfitFrequency;
    }

    public void setLongestNonProfitFrequency(double longestNonProfitFrequency) {
        this.longestNonProfitFrequency = longestNonProfitFrequency;
    }

    public SlotInput getSlotInput() {
        return slotInput;
    }

    public void setSlotInput(SlotInput slotInput) {
        this.slotInput = slotInput;
    }

    public double getProfitProbability() {
        return profitProbability;
    }

    public void setProfitProbability(double profitProbability) {
        this.profitProbability = profitProbability;
    }

    public double getNonProfitProbability() {
        return nonProfitProbability;
    }

    public void setNonProfitProbability(double nonProfitProbability) {
        this.nonProfitProbability = nonProfitProbability;
    }

    public double getAverageWin() {
        return averageWin;
    }

    public void setAverageWin(double averageWin) {
        this.averageWin = averageWin;
    }

    public long getBetCounter() {
        return betCounter;
    }

    public void setBetCounter(long betCounter) {
        this.betCounter = betCounter;
    }

    public double getCasinoEv() {
        return casinoEv;
    }

    public void setCasinoEv(double casinoEv) {
        this.casinoEv = casinoEv;
    }

    public double getCasinoRoi() {
        return casinoRoi;
    }

    public void setCasinoRoi(double casinoRoi) {
        this.casinoRoi = casinoRoi;
    }

}

