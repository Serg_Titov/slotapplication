package by.minsk.miroha.entities.report;

import by.minsk.miroha.entities.SlotInput;

public class NonProfitSeries {

    private long idSeries;

    private String seriesName;

    private long series;

    private SlotInput slotInput;

    public NonProfitSeries() {
    }

    public NonProfitSeries(String seriesName, long series, SlotInput slotInput) {
        this.seriesName = seriesName;
        this.series = series;
        this.slotInput = slotInput;
    }

    public long getIdSeries() {
        return idSeries;
    }

    public void setIdSeries(long idSeries) {
        this.idSeries = idSeries;
    }

    public String getSeriesName() {
        return seriesName;
    }

    public void setSeriesName(String seriesName) {
        this.seriesName = seriesName;
    }

    public long getSeries() {
        return series;
    }

    public void setSeries(long series) {
        this.series = series;
    }

    public SlotInput getSlotInput() {
        return slotInput;
    }

    public void setSlotInput(SlotInput slotInput) {
        this.slotInput = slotInput;
    }
}