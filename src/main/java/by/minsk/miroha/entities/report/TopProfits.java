package by.minsk.miroha.entities.report;

import by.minsk.miroha.entities.SlotInput;

public class TopProfits {

    private long idTopProfits;

    private int name;

    private double profit;

    private double frequency;

    private SlotInput slotInput;

    public TopProfits() {
    }

    public TopProfits(int name, double profit, double frequency, SlotInput slotInput) {
        this.name = name;
        this.profit = profit;
        this.frequency = frequency;
        this.slotInput = slotInput;
    }

    public long getIdTopProfits() {
        return idTopProfits;
    }

    public void setIdTopProfits(long idTopProfits) {
        this.idTopProfits = idTopProfits;
    }

    public int getName() {
        return name;
    }

    public void setName(int name) {
        this.name = name;
    }

    public double getProfit() {
        return profit;
    }

    public void setProfit(double profit) {
        this.profit = profit;
    }

    public double getFrequency() {
        return frequency;
    }

    public void setFrequency(double frequency) {
        this.frequency = frequency;
    }

    public SlotInput getSlotInput() {
        return slotInput;
    }

    public void setSlotInput(SlotInput slotInput) {
        this.slotInput = slotInput;
    }

    @Override
    public String toString() {
        return "TopProfits{" +
                "idTopProfits=" + idTopProfits +
                ", name=" + name +
                ", profit=" + profit +
                ", frequency=" + frequency +
                ", slotInput=" + slotInput +
                '}';
    }
}