package by.minsk.miroha.entities.report;

public class DatabaseMultipliers {

    private long idMultiplier;

    private int multiplier;

    private double multiplierFreq;

    private int gameId;

    public long getIdMultiplier() {
        return idMultiplier;
    }

    public void setIdMultiplier(long idMultiplier) {
        this.idMultiplier = idMultiplier;
    }

    public int getMultiplier() {
        return multiplier;
    }

    public void setMultiplier(int multiplier) {
        this.multiplier = multiplier;
    }

    public double getMultiplierFreq() {
        return multiplierFreq;
    }

    public void setMultiplierFreq(double multiplierFreq) {
        this.multiplierFreq = multiplierFreq;
    }

    public int getGameId() {
        return gameId;
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }
}

