package by.minsk.miroha.entities.report;

public class Multipliers {

    private int multiplier;
    private int multiplierCount;

    public Multipliers(int multiplier, int multiplierCount) {
        this.multiplier = multiplier;
        this.multiplierCount = multiplierCount;
    }

    public Multipliers() {
    }

    public int getMultiplier() {
        return multiplier;
    }

    public void setMultiplier(int multiplier) {
        this.multiplier = multiplier;
    }

    public int getMultiplierCount() {
        return multiplierCount;
    }

    public void setMultiplierCount(int multiplierCount) {
        this.multiplierCount = multiplierCount;
    }

    @Override
    public String toString() {
        return "multiplier = " + multiplier + ", multiplierCount = " + multiplierCount + ". ";
    }
}
