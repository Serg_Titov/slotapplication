package by.minsk.miroha.entities.report;

public class SlotPoint {

    private Long idSlotPoint;

    private String game;

    private String provider;

    private String spinLimit;

    private Double pointResult;

    public SlotPoint() {
    }

    public SlotPoint(String game, String provider, String spinLimit, Double pointResult) {
        this.game = game;
        this.provider = provider;
        this.spinLimit = spinLimit;
        this.pointResult = pointResult;
    }

    public Long getIdSlotPoint() {
        return idSlotPoint;
    }

    public void setIdSlotPoint(Long idSlotPoint) {
        this.idSlotPoint = idSlotPoint;
    }

    public String getGame() {
        return game;
    }

    public void setGame(String game) {
        this.game = game;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getSpinLimit() {
        return spinLimit;
    }

    public void setSpinLimit(String spinLimit) {
        this.spinLimit = spinLimit;
    }

    public Double getPointResult() {
        return pointResult;
    }

    public void setPointResult(Double pointResult) {
        this.pointResult = pointResult;
    }

    @Override
    public String toString() {
        return "SlotPoint{" +
                "idSlotPoint=" + idSlotPoint +
                ", game='" + game + '\'' +
                ", provider='" + provider + '\'' +
                ", spinLimit='" + spinLimit + '\'' +
                ", pointResult=" + pointResult +
                '}';
    }
}
