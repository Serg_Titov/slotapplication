package by.minsk.miroha.entities.report;

import java.util.List;

public class CasinoSite {

    private long siteId;

    private String site;

    private String brand;

    private List<String> licenses;

    public long getSiteId() {
        return siteId;
    }

    public void setSiteId(long siteId) {
        this.siteId = siteId;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public List<String> getLicenses() {
        return licenses;
    }

    public void setLicenses(List<String> licenses) {
        this.licenses = licenses;
    }

    @Override
    public String toString() {
        return "CasinoSite{" +
                "siteId=" + siteId +
                ", site='" + site + '\'' +
                ", brand='" + brand + '\'' +
                ", licenses=" + licenses +
                '}';
    }
}
