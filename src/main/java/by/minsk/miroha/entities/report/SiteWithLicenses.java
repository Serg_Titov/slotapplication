package by.minsk.miroha.entities.report;

import java.util.List;

public class SiteWithLicenses {

    private String site;

    private List<String> licenses;

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public List<String> getLicenses() {
        return licenses;
    }

    public void setLicenses(List<String> licenses) {
        this.licenses = licenses;
    }

    @Override
    public String toString() {
        return "SiteWithLicenses{" +
                "site='" + site + '\'' +
                ", licenses=" + licenses +
                '}';
    }
}
