package by.minsk.miroha.entities.cashback;

public class RangeValues {

    private double min;

    private double max;

    public RangeValues() {
        max = 0;
        min = 0;
    }

    public RangeValues(double minValue, double maxValue) {
        this.min = minValue;
        this.max = maxValue;
    }

    public double getMin() {
        return min;
    }

    public void setMin(double minValue) {
        this.min = minValue;
    }

    public double getMax() {
        return max;
    }

    public void setMax(double maxValue) {
        this.max = maxValue;
    }
}
