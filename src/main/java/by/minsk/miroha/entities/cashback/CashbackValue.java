package by.minsk.miroha.entities.cashback;

public enum CashbackValue {

    TEN (0.1),

    FIFTEEN (0.15),

    TWENTY (0.2),

    TWENTY_FIVE (0.25);

    private final double value;

    CashbackValue(double value) {
        this.value = value;
    }

    public double getValue() {
        return value;
    }
}
