package by.minsk.miroha.entities.cashback;

public class LossWinValues {

    private Double loss;

    private Double win;

    public LossWinValues() {
        this.loss = 0D;
        this.win = 0D;
    }

    public LossWinValues(Double loss, Double win) {
        this.loss = loss;
        this.win = win;
    }

    public Double getLoss() {
        return loss;
    }

    public void setLoss(Double loss) {
        this.loss = loss;
    }

    public Double getWin() {
        return win;
    }

    public void setWin(Double win) {
        this.win = win;
    }
}
