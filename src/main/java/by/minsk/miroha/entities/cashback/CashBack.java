package by.minsk.miroha.entities.cashback;

public class CashBack implements Comparable<CashBack>{

    private Double maxLoss;

    private Double targetWin;

    private int simulationAmount;

    private Double result;

    private double cashbackValue;

    @Override
    public int compareTo(CashBack o) {
        if (result == null || o.getResult() == null){
            return 0;
        } else {
            return -1*result.compareTo(o.getResult());
        }
    }

    public Double getMaxLoss() {
        return maxLoss;
    }

    public void setMaxLoss(Double maxLoss) {
        this.maxLoss = maxLoss;
    }

    public Double getTargetWin() {
        return targetWin;
    }

    public void setTargetWin(Double targetWin) {
        this.targetWin = targetWin;
    }

    public int getSimulationAmount() {
        return simulationAmount;
    }

    public void setSimulationAmount(int simulationAmount) {
        this.simulationAmount = simulationAmount;
    }

    public Double getResult() {
        return result;
    }

    public void setResult(Double result) {
        this.result = result;
    }

    public double getCashbackValue() {
        return cashbackValue;
    }

    public void setCashbackValue(double cashbackValue) {
        this.cashbackValue = cashbackValue;
    }

    @Override
    public String toString() {
        return "CashBack{" +
                "maxLoss=" + maxLoss +
                ", targetWin=" + targetWin +
                ", simulationAmount=" + simulationAmount +
                ", result=" + result +
                ", cashbackValue=" + cashbackValue +
                '}';
    }
}
