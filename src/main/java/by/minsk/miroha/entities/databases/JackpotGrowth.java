package by.minsk.miroha.entities.databases;

public class JackpotGrowth {

    private long jackpotGrowthId;

    private long jackpotSummaryId;

    private String site;

    private String brand;

    private double growthRate;

    private String timePeriod;

    public long getJackpotGrowthId() {
        return jackpotGrowthId;
    }

    public void setJackpotGrowthId(long jackpotGrowthId) {
        this.jackpotGrowthId = jackpotGrowthId;
    }

    public long getJackpotSummaryId() {
        return jackpotSummaryId;
    }

    public void setJackpotSummaryId(long jackpotSummaryId) {
        this.jackpotSummaryId = jackpotSummaryId;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public double getGrowthRate() {
        return growthRate;
    }

    public void setGrowthRate(double growthRate) {
        this.growthRate = growthRate;
    }

    public String getTimePeriod() {
        return timePeriod;
    }

    public void setTimePeriod(String timePeriod) {
        this.timePeriod = timePeriod;
    }
}
