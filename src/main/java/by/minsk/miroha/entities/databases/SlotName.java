package by.minsk.miroha.entities.databases;

import io.swagger.annotations.ApiModelProperty;

public class SlotName {

    @ApiModelProperty(value = "Название слота", example = "buffalo_rising_megaways", required = true)
    private String slotName;

    @ApiModelProperty(value = "Название провайдера", example = "blueprint", required = true)
    private String provider;

    public SlotName() {
    }

    public SlotName(String slotName, String provider) {
        this.slotName = slotName;
        this.provider = provider;
    }

    public String getSlotName() {
        return slotName;
    }

    public void setSlotName(String slotName) {
        this.slotName = slotName;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getFullName(){
        return slotName + " " + provider;
    }

    @Override
    public String toString() {
        return "SlotName{" +
                "slotName='" + slotName + '\'' +
                ", provider='" + provider + '\'' +
                '}';
    }
}
