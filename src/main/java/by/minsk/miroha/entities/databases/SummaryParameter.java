package by.minsk.miroha.entities.databases;

import java.sql.Date;

public class SummaryParameter {

    private long summaryParameterId;

    private long jackpotSummaryId;

    private String site;

    private String brand;

    private String slot;

    private double amount;

    private Date parameterDate;

    private String parameterName;

    public long getSummaryParameterId() {
        return summaryParameterId;
    }

    public void setSummaryParameterId(long summaryParameterId) {
        this.summaryParameterId = summaryParameterId;
    }

    public long getJackpotSummaryId() {
        return jackpotSummaryId;
    }

    public void setJackpotSummaryId(long jackpotSummaryId) {
        this.jackpotSummaryId = jackpotSummaryId;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getSlot() {
        return slot;
    }

    public void setSlot(String slot) {
        this.slot = slot;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Date getParameterDate() {
        return parameterDate;
    }

    public void setParameterDate(Date parameterDate) {
        this.parameterDate = parameterDate;
    }

    public String getParameterName() {
        return parameterName;
    }

    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }
}
