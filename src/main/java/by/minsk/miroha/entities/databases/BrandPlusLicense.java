package by.minsk.miroha.entities.databases;

public class BrandPlusLicense {

    private String brand;

    private String license;

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }
}
