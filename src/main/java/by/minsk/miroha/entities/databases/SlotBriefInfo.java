package by.minsk.miroha.entities.databases;

import java.util.Date;

public class SlotBriefInfo implements Comparable<SlotBriefInfo>{

    private long briefId;

    private String provider;

    private String slotName;

    private long spinAmount;

    private Date creationDate;

    public long getBriefId() {
        return briefId;
    }

    public void setBriefId(long briefId) {
        this.briefId = briefId;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getSlotName() {
        return slotName;
    }

    public void setSlotName(String slotName) {
        this.slotName = slotName;
    }

    public long getSpinAmount() {
        return spinAmount;
    }

    public void setSpinAmount(long spinAmount) {
        this.spinAmount = spinAmount;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public int compareTo(SlotBriefInfo o) {
        return (-1)*getCreationDate().compareTo(o.getCreationDate());
    }
}
