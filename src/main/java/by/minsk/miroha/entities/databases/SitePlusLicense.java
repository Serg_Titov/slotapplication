package by.minsk.miroha.entities.databases;

public class SitePlusLicense {

    private String site;

    private String license;

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    @Override
    public String toString() {
        return "SitePlusLicense{" +
                "site='" + site + '\'' +
                ", license='" + license + '\'' +
                '}';
    }
}
