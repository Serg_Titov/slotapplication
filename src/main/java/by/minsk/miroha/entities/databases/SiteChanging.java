package by.minsk.miroha.entities.databases;

public class SiteChanging {

    private String site;

    private String brand;

    private String removingDate;

    private String creationDate;

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getRemovingDate() {
        return removingDate;
    }

    public void setRemovingDate(String removingDate) {
        this.removingDate = removingDate;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public String toString() {
        return "SiteChanging{" +
                "site='" + site + '\'' +
                ", brand='" + brand + '\'' +
                ", removingDate='" + removingDate + '\'' +
                ", creationDate='" + creationDate + '\'' +
                '}';
    }
}
