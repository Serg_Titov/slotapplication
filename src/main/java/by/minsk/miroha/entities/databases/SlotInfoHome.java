package by.minsk.miroha.entities.databases;

import java.util.Objects;

/**
 * Класс - информация об результате одного оборота слота.
 */
public class SlotInfoHome {

    /**
     * ID.
     */
    private long idSlotInfo;

    /**
     * Ставка.
     */
    private double bid;

    /**
     * Выйгрыш.
     */
    private double win;

    /**
     * Баланс.
     */
    private double credit;

    /**
     * Бонус.
     */
    private Double bonus;

    /**
     * IP.
     */
    private String ip;

    /**
     * Название слота.
     */
    private String game;

    /**
     * Выйгрышный коэффициент.
     */
    private double winCoefficient;

    public SlotInfoHome() {
    }

    public long getIdSlotInfo() {
        return idSlotInfo;
    }

    public void setIdSlotInfo(long idSlotInfo) {
        this.idSlotInfo = idSlotInfo;
    }

    public double getBid() {
        return bid;
    }

    public void setBid(double bid) {
        this.bid = bid;
    }

    public double getWin() {
        return win;
    }

    public void setWin(double win) {
        this.win = win;
    }

    public double getCredit() {
        return credit;
    }

    public void setCredit(double credit) {
        this.credit = credit;
    }

    public double getWinCoefficient() {
        return winCoefficient;
    }

    public void setWinCoefficient(double winCoefficient) {
        this.winCoefficient = winCoefficient;
    }

    public Double getBonus() {
        return bonus;
    }

    public void setBonus(Double bonus) {
        this.bonus = bonus;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getGame() {
        return game;
    }

    public void setGame(String game) {
        this.game = game;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        if (!super.equals(object)) return false;
        SlotInfoHome slotInfo = (SlotInfoHome) object;
        return idSlotInfo == slotInfo.idSlotInfo &&
                bid == slotInfo.bid &&
                win == slotInfo.win &&
                credit == slotInfo.credit &&
                bonus.equals(slotInfo.bonus) &&
                ip.equals(slotInfo.ip) &&
                game.equals(slotInfo.game) &&
                java.lang.Double.compare(slotInfo.winCoefficient, winCoefficient) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), idSlotInfo, bid, win, credit, bonus, ip, game, winCoefficient);
    }

    @Override
    public String toString() {
        return "id_run = " + this.idSlotInfo + ", bid = " + this.bid + ", win = " + this.win +
                ", credit = " + this.credit + ", bonus = " + this.bonus + ", ip = " + this.ip
                + ", game = " + this.game + ", win coefficient = " + this.winCoefficient;
    }
}