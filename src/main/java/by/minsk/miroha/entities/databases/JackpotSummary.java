package by.minsk.miroha.entities.databases;

import java.sql.Timestamp;

public class JackpotSummary {

    private long jackpotSummaryId;

    private int uniqueJackpots;

    private int paidOutAmount;

    private int counter100Rtp;

    private String slot;

    private Timestamp timeCheck;

    public long getJackpotSummaryId() {
        return jackpotSummaryId;
    }

    public void setJackpotSummaryId(long jackpotSummaryId) {
        this.jackpotSummaryId = jackpotSummaryId;
    }

    public int getUniqueJackpots() {
        return uniqueJackpots;
    }

    public void setUniqueJackpots(int uniqueJackpots) {
        this.uniqueJackpots = uniqueJackpots;
    }

    public int getPaidOutAmount() {
        return paidOutAmount;
    }

    public void setPaidOutAmount(int paidOutAmount) {
        this.paidOutAmount = paidOutAmount;
    }

    public int getCounter100Rtp() {
        return counter100Rtp;
    }

    public void setCounter100Rtp(int counter100Rtp) {
        this.counter100Rtp = counter100Rtp;
    }

    public String getSlot() {
        return slot;
    }

    public void setSlot(String slot) {
        this.slot = slot;
    }

    public Timestamp getTimeCheck() {
        return timeCheck;
    }

    public void setTimeCheck(Timestamp timeCheck) {
        this.timeCheck = timeCheck;
    }
}
