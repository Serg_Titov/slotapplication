package by.minsk.miroha.entities.databases;

import java.util.Objects;

public class SlotInfoPragmatic {

    private int idPragmatic;
    private double bid;
    private double win;
    private double credit;
    private String bonus;
    private String ip;
    private String game;

    public int getIdPragmatic() {
        return idPragmatic;
    }

    public void setIdPragmatic(int idPragmatic) {
        this.idPragmatic = idPragmatic;
    }

    public double getBid() {
        return bid;
    }

    public void setBid(double bid) {
        this.bid = bid;
    }

    public double getWin() {
        return win;
    }

    public void setWin(double win) {
        this.win = win;
    }

    public double getCredit() {
        return credit;
    }

    public void setCredit(double credit) {
        this.credit = credit;
    }

    public String getBonus() {
        return bonus;
    }

    public void setBonus(String bonus) {
        this.bonus = bonus;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getGame() {
        return game;
    }

    public void setGame(String game) {
        this.game = game;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SlotInfoPragmatic that = (SlotInfoPragmatic) o;
        return idPragmatic == that.idPragmatic &&
            Double.compare(that.bid, bid) == 0 &&
            Double.compare(that.win, win) == 0 &&
            Double.compare(that.credit, credit) == 0 &&
            Objects.equals(bonus, that.bonus) &&
            Objects.equals(ip, that.ip) &&
            Objects.equals(game, that.game);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idPragmatic, bid, win, credit, bonus, ip, game);
    }

    @Override
    public String toString() {
        return "SlotInfoPragmatic{" +
            "idPragmatic=" + idPragmatic +
            ", bid=" + bid +
            ", win=" + win +
            ", credit=" + credit +
            ", bonus='" + bonus + '\'' +
            ", ip='" + ip + '\'' +
            ", game='" + game + '\'' +
            '}';
    }
}
