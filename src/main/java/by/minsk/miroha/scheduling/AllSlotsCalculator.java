package by.minsk.miroha.scheduling;

import by.minsk.miroha.services.GamesService;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
public class AllSlotsCalculator {

    private final GamesService gamesService;

    public AllSlotsCalculator(GamesService gamesService) {
        this.gamesService = gamesService;
    }

    //@Scheduled(fixedDelayString = "P7D", initialDelay = 10000)
    public void runCalculateAllSlots(){
        gamesService.calculateAllProviders();
    }
}
