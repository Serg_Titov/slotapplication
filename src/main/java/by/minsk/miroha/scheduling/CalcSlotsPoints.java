package by.minsk.miroha.scheduling;

import by.minsk.miroha.services.report.SlotInformationService;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
public class CalcSlotsPoints {

    private final SlotInformationService service;

    public CalcSlotsPoints(SlotInformationService service) {
        this.service = service;
    }

    //@Scheduled(fixedDelayString = "P7D", initialDelay = 10000)
    public void recalculatePoints(){
        service.recalculateSlotsPoints();
    }
}
