package by.minsk.miroha.scheduling;

import by.minsk.miroha.perform.PerformerJackpotSummaries;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
public class PrepareJackpotSummaries {

    private final PerformerJackpotSummaries performer;

    public PrepareJackpotSummaries(PerformerJackpotSummaries performer) {
        this.performer = performer;
    }

    @Scheduled(fixedDelay = 10800000, initialDelay = 1000)//(fixedDelayString = "PT03H", initialDelay = 10000)
    public void prepareJackpotSummaries(){
        System.out.println("!!!");
        performer.prepareJackpotSummaries();
    }
}
