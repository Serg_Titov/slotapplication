package by.minsk.miroha.scheduling;

import by.minsk.miroha.entities.report.SlotInformation;
import by.minsk.miroha.services.GamesService;
import by.minsk.miroha.services.report.SlotInformationService;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.List;
import java.util.Map;

@Configuration
public class SlotInformationDeleter {

    private final GamesService gamesService;

    private final SlotInformationService slotInformationService;

    public SlotInformationDeleter(GamesService gamesService, SlotInformationService slotInformationService) {
        this.gamesService = gamesService;
        this.slotInformationService = slotInformationService;
    }

    //@Scheduled(fixedDelayString = "P1D", initialDelay = 1000)
    public void deleteSlotInformation(){
        List<String> providers = gamesService.getAllProviders();
        Map<String, List<String>> existsSlots = gamesService.getAllGames(providers);
        List<SlotInformation> calculatedSlots = slotInformationService.getAllSlots();
        for(SlotInformation slotInformation : calculatedSlots){
            String provider = slotInformation.getProvider().getRawProvider();
            if(!existsSlots.containsKey(provider)){
                slotInformationService.fullRemovingSlotInformationById(slotInformation);
            } else {
                List<String> slots = existsSlots.get(provider);
                if(!slots.contains(slotInformation.getSlot().getRawSlot())){
                    slotInformationService.fullRemovingSlotInformationById(slotInformation);
                }
            }
        }
    }
}
